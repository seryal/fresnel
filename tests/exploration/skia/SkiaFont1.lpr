program SkiaFont1;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Classes, SysUtils,
  Fresnel, Fresnel.SkiaRenderer, System.Skia, // this includes the Fresnel widgetset
  Fresnel.Forms,
  Fresnel.Classes, Fresnel.DOM, Fresnel.Controls;

{$R *.res}

type

  { TFresnelForm1 }

  TFresnelForm1 = class(TFresnelForm)
  private

  public
    procedure WSDraw; override;
    constructor CreateNew(AOwner: TComponent); override;
  end;

var
  FresnelForm1: TFresnelForm1;

{ TFresnelForm1 }

procedure TFresnelForm1.WSDraw;
var
  SkRenderer: TFresnelSkiaRenderer;
  C: ISkCanvas;
  aFontDesc: TFresnelFontDesc;
  aFont: IFresnelFont;
begin
  inherited WSDraw;

  SkRenderer:=Renderer as TFresnelSkiaRenderer;
  C:=SkRenderer.Canvas;

  with aFontDesc do begin
    Family:='arial';
    Kerning:=fckAuto;
    Size:=20; // in pixel
    Style:='normal';
    Weight:=400; // 100..750
    Width:=1; // 1 = default
  end;
  aFont:=AllocateFont(aFontDesc);

  writeln('TFresnelForm1.WSDraw ');
end;

constructor TFresnelForm1.CreateNew(AOwner: TComponent);
begin
  inherited CreateNew(AOwner);
  FormBounds:=BoundsRectFre(100,100,500,300);
end;

begin
  Application.HookFresnelLog:=true;
  Application.Initialize;
  Application.CreateForm(TFresnelForm1,FresnelForm1);
  Application.Run;
end.

