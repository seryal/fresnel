program SkiaCmdLine1;

{$IFDEF Windows}
{$APPTYPE CONSOLE}
{$ENDIF}

{ $R *.res}

uses
  SysUtils, System.UITypes, System.Skia;

procedure TestSkSurface;
var
  LSurface: ISkSurface;
begin
  // Creating a solid red png image using SkSurface
  LSurface := TSkSurface.MakeRaster(200, 200);
  LSurface.Canvas.Clear(TAlphaColors.Red); // $FFFF0000
  LSurface.MakeImageSnapshot.EncodeToFile('test.png');
end;

begin
  try
    TestSkSurface;
    WriteLn('Finished!');
    ReadLn;
  except
    on E: Exception do
      Writeln(E.ClassName, ': ', E.Message);
  end;
end.
