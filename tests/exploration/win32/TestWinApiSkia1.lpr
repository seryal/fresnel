program TestWinApiSkia1;

uses Classes, Windows, Types, JwaWinGDI, System.UITypes, System.Skia;

const
  WndClassName1 = 'MainForm';

var
  MainFormHandle: HWND;
  DrawBuffer: HBITMAP;
  DrawBufferData: Pointer;
  DrawBufferStride: integer;

procedure CreateBuffer(const aMemDC: HDC; const aWidth, aHeight: Integer;
  out aBuffer: HBITMAP; out aData: Pointer; out aStride: Integer);
const
  {%H-}ColorMasks: array[0..2] of DWord = ($ff0000, $00ff00, $0000ff);
var
  aBitmapInfo: PBITMAPINFO;
begin
  aStride:=aWidth*4;
  aBuffer:=0;
  aData:=nil;
  aBitmapInfo:=AllocMem(SizeOf(TBITMAPINFOHEADER));
  try
    with aBitmapInfo^.bmiHeader do
    begin
      biSize:=SizeOf(TBITMAPINFOHEADER);
      biWidth:=aWidth;
      biHeight:=-aHeight;
      biPlanes:=1;
      biBitCount:=32;
      biCompression:=BI_BITFIELDS;
      biSizeImage:=aStride*aHeight;
    end;
    System.Move(ColorMasks[0],aBitmapInfo^.bmiColors[0],SizeOf(ColorMasks));
    aBuffer:=CreateDIBSection(aMemDC,aBitmapInfo,DIB_RGB_COLORS, @aData,0,0);
    if aBuffer<>0 then
      GdiFlush;
  finally
    Freemem(aBitmapInfo);
  end;
end;

procedure DeleteBuffers;
begin
  if DrawBuffer=0 then exit;
  DeleteObject(DrawBuffer);
  DrawBuffer:=0;
  DrawBufferData:=nil;
end;

procedure DrawMainForm(dc: HDC);
const
  BlendFunction: TBlendFunction = (
    BlendOp: AC_SRC_OVER;
    BlendFlags: 0;
    SourceConstantAlpha: 255;
    AlphaFormat: AC_SRC_ALPHA
    );
var
  r: TRect;
  SkSurface: ISkSurface;
  SkCanvas: ISkCanvas;
  OldBmp: HGDIOBJ;
  BufDC: HDC;
  SkPaint: ISkPaint;
  Box: TRectF;
begin
  r:=Default(TRect);
  GetClientRect(MainFormHandle,r);
  if (r.Width<=0) or (r.Height<=0) then exit;

  BufDC:=CreateCompatibleDC(0);
  if BufDC=0 then exit;
  try
    if DrawBuffer=0 then
      CreateBuffer(BufDC,r.Width,r.Height,DrawBuffer,DrawBufferData,DrawBufferStride);
    if DrawBuffer=0 then
      exit;
    OldBmp:=SelectObject(BufDC,DrawBuffer);
    try
      SkSurface:=TSkSurface.MakeRasterDirect(TSkImageInfo.Create(r.Width,r.Height),
        DrawBufferData,DrawBufferStride);
      SkCanvas:=SkSurface.Canvas;
      SkCanvas.Clear(TAlphaColors.White);

      SkPaint:=TSkPaint.Create(TSkPaintStyle.Stroke);
      SkPaint.SetColor(TAlphaColors.Red);
      SkCanvas.DrawLine(10,10,100,30,SkPaint);

      SkPaint.SetColor(TAlphaColors.Blue);
      Box.Left:=r.Right-6;
      Box.Top:=r.Bottom-6;
      Box.Right:=r.Right-1;
      Box.Bottom:=r.Bottom-1;
      SkCanvas.DrawRect(Box,SkPaint);

      AlphaBlend(dc,0,0,r.Width,r.Height,BufDC,0,0,r.Width,r.Height,BlendFunction);
    finally
      if OldBmp<>0 then
        SelectObject(BufDC,OldBmp);
    end;
  finally
    DeleteDC(BufDC);
  end;
end;

function WindowProc(aHandle: HWND; aMsg: UINT; aWParam: WPARAM; aLParam: LPARAM): LRESULT; stdcall;

  function HandlePaintMsg: LRESULT;
  var
    ps: TPAINTSTRUCT;
    dc: HDC;
  begin
    ps:=Default(TPAINTSTRUCT);
    dc:=BeginPaint(aHandle,ps);
    try
      DrawMainForm(dc);
    finally
      EndPaint(aHandle,ps);
    end;
    Result:=0;
  end;

var
  r: TRect;
begin
  case aMsg of
  WM_PAINT:
    begin
      Result:=HandlePaintMsg;
      exit;
    end;
  WM_SIZE:
    begin
      DeleteBuffers;
      InvalidateRect(aHandle,nil,false);
      r:=Default(TRect);
      if GetWindowRect(aHandle,r) then
      begin
        writeln('WindowProc IsMainForm=',aHandle=MainFormHandle,' l=',r.Left,',t=',r.Top,',w=',r.Width,',h=',r.Height);
      end;
    end;
  WM_CLOSE:
    begin
      PostQuitMessage(0);
      exit(0);
    end;
  end;

  Result:=DefWindowProc(aHandle,aMsg,aWParam,aLParam);
end;

procedure RegisterWindowClass(const aClassName: PChar);
var
  wc: TWNDCLASS;
begin
  wc:=Default(TWNDCLASS);
  wc.lpfnWndProc:=@WindowProc;
  wc.hInstance:=HINSTANCE;
  wc.hbrBackground:=GetStockObject(WHITE_BRUSH);
  wc.lpszClassName:=aClassName;
  wc.hCursor:=LoadCursor(0,IDC_ARROW);
  RegisterClass(wc);
end;

procedure CreateForms;
begin
  RegisterWindowClass(WndClassName1);

  MainFormHandle:= CreateWindowEx(0,WndClassName1,'MainForm',
    WS_OVERLAPPEDWINDOW,
    100,100,400,300, 0,0, HINSTANCE, nil);
  ShowWindow(MainFormHandle,SW_SHOW);
end;

var
  aMsg: TMsg;
begin
  CreateForms;
  repeat
    aMsg:=Default(TMsg);
    if not GetMessage(aMsg,0,0,0) then break;
    TranslateMessage(aMsg);
    DispatchMessage(aMsg);
  until false;
  DeleteBuffers;
end.

