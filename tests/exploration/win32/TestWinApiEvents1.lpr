program TestWinApiEvents1;

uses Classes, Windows;

const
  WndClassName1 = 'MainForm';

var
  MainFormHandle: HWND;

procedure DrawMainForm(dc: HDC);
const
  aText = 'Hello Fresnel!';
var
  Pen: HPEN;
  r: TRect;
  Brush: HBRUSH;
begin
  r:=Default(TRect);
  GetClientRect(MainFormHandle,r);

  // clear background
  Brush:=CreateSolidBrush(RGB(255,255,255));
  FillRect(dc,r,Brush);
  DeleteObject(Brush);

  // draw rectangle in right, bottom corner
  Pen:=CreatePen(PS_SOLID,1,RGB(0,0,255));
  SelectObject(dc,Pen);
  Brush:=CreateSolidBrush(RGB(255,0,255));
  SelectObject(dc,Brush);
  Rectangle(dc,r.Right-5,r.Bottom-5,r.Right,r.Bottom);
  DeleteObject(Pen);
  DeleteObject(Brush);

  // draw line
  Pen:=CreatePen(PS_SOLID,2,RGB(255,0,0));
  SelectObject(dc,Pen);
  MoveToEx(dc,10,10,nil);
  LineTo(dc,100,30);
  DeleteObject(Pen);

  // draw text
  SetTextColor(dc, RGB(0,0,255));
  TextOut(dc, 20,50,aText,length(aText));

  //
end;

function WindowProc(aHandle: HWND; aMsg: UINT; aWParam: WPARAM; aLParam: LPARAM): LRESULT; stdcall;

  function HandlePaintMsg: LRESULT;
  var
    ps: TPAINTSTRUCT;
    dc: HDC;
  begin
    ps:=Default(TPAINTSTRUCT);
    dc:=BeginPaint(aHandle,ps);
    try
      DrawMainForm(dc);
    finally
      EndPaint(aHandle,ps);
    end;
    Result:=0;
  end;

var
  r: TRect;
  XPos, YPos: LongInt;
begin
  case aMsg of
  WM_PAINT:
    begin
      Result:=HandlePaintMsg;
      exit;
    end;
  WM_SIZE:
    begin
      InvalidateRect(aHandle,nil,false);
      r:=Default(TRect);
      if GetWindowRect(aHandle,r) then
      begin
        writeln('WindowProc IsMainForm=',aHandle=MainFormHandle,' l=',r.Left,',t=',r.Top,',w=',r.Width,',h=',r.Height);
      end;
    end;
  WM_KEYDOWN:
    begin
      writeln('KeyDown: ',aWParam);
      if aWParam=VK_ESCAPE then
        PostQuitMessage(0);
      exit(0);
    end;
  WM_KEYUP:
    begin
      writeln('KeyUp: ',aWParam);
      exit(0);
    end;
  WM_LBUTTONDOWN:
    begin
      XPos := GET_X_LPARAM(aLParam);
      YPos := GET_Y_LPARAM(aLParam);
      writeln('LButtonDown ',XPos,',',YPos);
      exit(0);
    end;
  WM_CLOSE:
    begin
      PostQuitMessage(0);
      exit(0);
    end;
  end;

  Result:=DefWindowProc(aHandle,aMsg,aWParam,aLParam);
end;

procedure RegisterWindowClass(const aClassName: PChar);
var
  wc: TWNDCLASS;
begin
  wc:=Default(TWNDCLASS);
  wc.lpfnWndProc:=@WindowProc;
  wc.hInstance:=HINSTANCE;
  wc.hbrBackground:=GetStockObject(WHITE_BRUSH);
  wc.lpszClassName:=aClassName;
  wc.hCursor:=LoadCursor(0,IDC_ARROW);
  RegisterClass(wc);
end;

procedure CreateForms;
begin
  RegisterWindowClass(WndClassName1);

  MainFormHandle:= CreateWindowEx(0,WndClassName1,'MainForm',
    WS_OVERLAPPEDWINDOW,
    100,100,400,300, 0,0, HINSTANCE, nil);
  ShowWindow(MainFormHandle,SW_SHOW);
  //UpdateWindow(MainFormHandle);
end;

var
  aMsg: TMsg;
begin
  CreateForms;
  repeat
    aMsg:=Default(TMsg);
    if not GetMessage(aMsg,0,0,0) then break;
    TranslateMessage(aMsg);
    DispatchMessage(aMsg);
  until false;
end.

