{
  This is the MainForm of the LCL application.
  Once it is shown, it automatically opens the fresnel form.
}
unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, FreLabelFragsForm;

type

  { TMainForm }

  TMainForm = class(TForm)
    procedure FormPaint(Sender: TObject);
  private
    FQueued: boolean;
    procedure ShowFresnelForm({%H-}Data: PtrInt);
  public
  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormPaint(Sender: TObject);
begin
  if FQueued then exit;
  FQueued:=true;
  Application.QueueAsyncCall(@ShowFresnelForm,0)
end;

procedure TMainForm.ShowFresnelForm(Data: PtrInt);
begin
  LabelFragmentsTestForm:=TLabelFragmentsTestForm.Create(Self);
end;

end.

