unit TCTextLayout;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpcunit, testregistry, fpimage, fresnel.textlayouter;

const
  cHeight = 15;
  cWidth  = 10;
  cFontSize = 12;
  cFontName = 'Arial';
  cFontAttr : TFontAttributes = [];
  cDelta  = 0.01;
  cLineSpacing = 2;

type

  { TTestLayouter }

  TTestLayouter = class(TTextLayouter)
  public
    class function CreateMeasurer(aLayouter: TTextLayouter): TTextMeasurer; override;
    class function CreateSPlitter(aLayouter: TTextLayouter): TTextSplitter; override;
  end;

  { TFixedListSplitter }

  TFixedListSplitter = class(TTextSplitter)
  public
    Function GetNextSplitPoint(const aText : TTextString; aStartPos : SizeInt; aAllowHyphen : Boolean) : TTextSplitPoint; override;
  end;

  { TLayoutTestCase }

  TLayoutTestCase = class(TTestCase)
  private
    FLayouter: TTextLayouter;
    FFont : TTextFont;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
    function CreateTextBlock(const aOffset, aLength: SizeInt): TTextBlock;
    class procedure AssertEquals(aMsg : String; aExpected,aActual : TTextSplitPoint); overload;
    property Layouter : TTextLayouter Read FLayouter;
    Property Font : TTextFont Read FFont;
  end;

  { TTestSplitter }

  TTestSplitter= class(TLayoutTestCase)
  private
    FSplitter: TTextSplitter;
  protected
    procedure SetUp; override;
    procedure TearDown; override;
    procedure CheckPoints(Msg : String; aRes : TTextSplitPointArray; aPoints : Array of TTextSplitPoint);
    procedure TestSplit(Msg : String; aText : TTextString; aStartPos : SizeInt; aPoints : Array of TTextSplitPoint);
    procedure TestNewLineSplit(Msg : String; aText : TTextString; aStartPos : SizeInt; aPoints : Array of TTextSplitPoint);
    Property Splitter : TTextSplitter Read FSplitter;
  published
    procedure TestHookUp;
    procedure TestOneWord;
    procedure Test2Words;
    procedure Test3Words;
    procedure Test4Words;
    procedure TestLineBreak;
    procedure Test2LineBreaks;
    procedure Test2LineBreakChars;
  end;

  { TTestTextBlock }

  TTestTextBlock = class(TLayoutTestCase)
  private
    FTextBlock: TTextBlock;
    procedure FakeLayoutBlock(aBlock: TTextBlock);
  Protected
    Procedure SetUp; override;
    Procedure TearDown; override;
    Property Block : TTextBlock Read FTextBlock;
  Published
    procedure TestHookUp;
    procedure TestAssign;
    procedure TestSplit;
    procedure TestSplit2;
  end;

  { TTestTextLayouter }

  TTestTextLayouter = class(TLayoutTestCase)
  private
    class procedure AssertBlock(Msg: String; aBlock: TTextBlock; atext: String; aPosX, aPosY, aWidth, aHeight: TTextUnits; aLineBreak : Boolean = False);
    procedure AssertBlock(Msg: String; Idx: Integer; atext: String; aPosX, aPosY, aWidth, aHeight: TTextUnits; aLineBreak : Boolean = False);
  published
    procedure TestHookUp;
    procedure TestSimpleText;
    procedure TestSimpleMultiLineText;
    procedure TestSimpleMultiLineTextCRLF;
    procedure TestSimpleMultiLineTextWhiteSpace;
    procedure TestSimpleMultiLineTextWhiteSpaceCRLF;
    procedure TestSimpleWrapText;
    procedure TestSimpleWrapTextHyphen;
    procedure TestSimpleWrapTextOnewordOverflow;
    procedure TestSimpleWrapTextOnewordTruncate;
    procedure TestSimpleWrapTextOnewordTruncateEmpty;
    procedure TestSimpleWrapTextOnewordEllipsis;
    procedure TestSimpleWrapTextOnewordAsterisk;
    procedure TestRangeOverlap;
    procedure TestRangeOverlapFit;
    procedure TestRangeText;
    procedure TestRangeTextOffset;
    procedure TestRangeTextOffset2;
    procedure TestRangeTextOffset3;
  end;




implementation

{ TTestLayouter }

class function TTestLayouter.CreateMeasurer(aLayouter: TTextLayouter): TTextMeasurer;

var
  M : TFixedSizeTextMeasurer;


begin
  M:=TFixedSizeTextMeasurer.Create(aLayouter);
  // For easier calculation
  M.CharWidth:=cWidth;
  M.CharHeight:=cHeight;
  Result:=M;
end;

class function TTestLayouter.CreateSPlitter(aLayouter: TTextLayouter): TTextSplitter;
begin
  Result:=TFixedListSplitter.Create(aLayouter);
end;

{ TFixedListSplitter }

function TFixedListSplitter.GetNextSplitPoint(const aText : TTextString; aStartPos : SizeInt; aAllowHyphen : Boolean) : TTextSplitPoint;

Const
  SplText = 'spaces.';

begin
  if not aAllowHyphen then
    Result:=Inherited GetNextSplitPoint(aText,aStartPos,aAllowHyphen)
  else
    if (Copy(aText,aStartPos,Length(splText))=SplText) then
      Result:=SplitPoint(aStartPos+2,0)
    else
      Result:=Inherited GetNextSplitPoint(aText,aStartPos,aAllowHyphen)

end;


{ TLayoutTestCase }

procedure TLayoutTestCase.SetUp;
begin
  inherited SetUp;
  FLayouter:=TTestLayouter.Create(nil);
  FFont:=TTextFont.Create(Nil);
  Font.Name:=cFontName;
  Font.Size:=cFontSize;
  Font.Attrs:=cFontAttr;
  FLayouter.Font:=FFont;
  FLayouter.LineSpacing:=cLineSpacing;
end;

procedure TLayoutTestCase.TearDown;
begin
  FreeAndNil(FLayouter);
  FreeAndNil(FFont);
  inherited TearDown;
end;

function TLayoutTestCase.CreateTextBlock(const aOffset,aLength : SizeInt): TTextBlock;
begin
  Result:=TTextBlock.Create(Layouter,aOffset,aLength);
end;

class procedure TLayoutTestCase.AssertEquals(aMsg: String; aExpected, aActual: TTextSplitPoint);
begin
  AssertEquals(aMsg+': offset',aExpected.offset,aActual.Offset);
  AssertEquals(aMsg+': whitespace',aExpected.WhiteSpace,aActual.WhiteSpace);
end;

procedure TTestSplitter.TestHookUp;
begin
  AssertNotNull('Have splitter',Splitter);
end;

procedure TTestSplitter.TestOneWord;
begin
  TestSplit('one word','one',1,[SplitPoint(3,1)]);
end;

procedure TTestSplitter.Test2Words;
begin
  TestSplit('two words','one two',1,[SplitPoint(3,1),SplitPoint(7,1)]);
end;

procedure TTestSplitter.Test3Words;
begin
  //                       0    5    10   15
  TestSplit('three words','one  two   three ',1,[SplitPoint(3,2),SplitPoint(8,3),SplitPoint(16,1)]);
end;

procedure TTestSplitter.Test4Words;
begin
  // Pos (=offset+1)      1   5    10   15
  TestSplit('Four words','the  cat saw   me',1,[SplitPoint(3,2),SplitPoint(8,1),SplitPoint(12,3),SplitPoint(17,1)]);
  //                         ^    ^   ^    ^
end;

procedure TTestSplitter.TestLineBreak;
begin
  // Pos (=offset+1)            1   5        10   15
  TestNewLineSplit('One split','the cat'#10'saw me',1,[SplitPoint(7,1)]);
  //                                        ^
end;

procedure TTestSplitter.Test2LineBreaks;
begin
  // Pos (=offset+1)            1   5        10   15
  TestNewLineSplit('Two split','the cat'#10'saw me'#10'and ran',1,[SplitPoint(7,1),SplitPoint(14,1)]);
  //                                        ^
end;

procedure TTestSplitter.Test2LineBreakChars;
begin
  // Pos (=offset+1)            1   5          10   15
  TestNewLineSplit('Two split','the cat'#13#10'saw me'#10'and ran',1,[SplitPoint(7,2),SplitPoint(15,1)]);
  //                                        ^
end;


procedure TTestSplitter.SetUp;
begin
  Inherited;
  FSplitter:=TTextSplitter.Create(Layouter);
end;

procedure TTestSplitter.TearDown;
begin
  FreeAndNil(FSplitter);
  Inherited;
end;

procedure TTestSplitter.CheckPoints(Msg : String; aRes: TTextSplitPointArray; aPoints: array of TTextSplitPoint);

var
  I : integer;

begin
  AssertEquals(Msg+': Splitpoint count',Length(aPoints),Length(aRes));
  For I:=0 to Length(aRes)-1 do
    AssertEquals(Msg+Format(': Element %d',[I]),aPoints[i],aRes[I]);
end;

procedure TTestSplitter.TestSplit(Msg : String; aText: TTextString; aStartPos: SizeInt; aPoints: array of TTextSplitPoint);

var
  Res : TTextSplitPointArray;

begin
  Res:=Splitter.SplitText(aText,aStartPos,False);
  CheckPoints(Msg,Res,aPoints);
end;

procedure TTestSplitter.TestNewLineSplit(Msg: String; aText: TTextString; aStartPos: SizeInt; aPoints: array of TTextSplitPoint);
var
  Res : TTextSplitPointArray;
begin
  Res:=Splitter.SplitLines(aText,aStartPos,False);
  CheckPoints(Msg,Res,aPoints);
end;

{ TTestTextBlock }

procedure TTestTextBlock.SetUp;
begin
  inherited SetUp;
  Layouter.Text:='this text';
  FTextBlock:=CreateTextBlock(0,4);
  FTextBlock.Font:=FFont;
  Block.Font.Name:='Arial';
  Block.Font.Size:=12;
  Block.Font.Attrs:=[faBold];
  Block.Font.FPColor:=colBlack;
end;

procedure TTestTextBlock.TearDown;
begin
  FreeAndNil(FTextBlock);
  inherited TearDown;
end;

procedure TTestTextBlock.TestHookUp;
begin
  AssertNotNull('Have block',Block);
  AssertEquals('block offset',0,Block.TextOffset);
  AssertEquals('block length',4,Block.TextLen);
  AssertEquals('block font name','Arial',Block.Font.Name);
  AssertEquals('block font Size',12,Block.Font.Size);
  AssertTrue('block font attrs',[faBold]=Block.Font.Attrs);
  AssertTrue('block font color',Block.Font.FPColor=colBlack);
  AssertEquals('Get text','this',Block.Text);
end;

procedure TTestTextBlock.FakeLayoutBlock(aBlock : TTextBlock);

begin
  With aBlock do
    begin
    ForceNewLine:=True;
    LayoutPos:=TTextPoint.Create(2,3);
    Size.Width:=12.3;
    Size.Height:=8.9;
    Size.Descender:=2.3;
    Font:=Self.FFont;
    end;
end;

procedure TTestTextBlock.TestAssign;

var
  Blk : TTextBlock;

begin
  FakeLayoutBlock(Block);

  Blk:=CreateTextBlock(0,0);
  try
    Blk.Assign(Block);
    AssertEquals('block offset',Block.TextOffset,Blk.TextOffset);
    AssertEquals('block length',Block.TextLen,Blk.TextLen);
    AssertEquals('block forcenewline',Block.ForceNewLine,Blk.ForceNewLine);

    AssertEquals('block font name',Block.Font.Name,Blk.Font.Name);
    AssertEquals('block font Size',12,Block.Font.Size,Blk.Font.Size);
    AssertTrue('block font attrs',Block.Font.Attrs=Blk.Font.Attrs);
    AssertTrue('block font color',FTextBlock.Font.Color=Blk.Font.Color);
    AssertEquals('block layout pos X',Block.LayoutPos.X,Blk.LayoutPos.X);
    AssertEquals('block layout pos Y',Block.LayoutPos.Y,Blk.LayoutPos.Y);
    AssertEquals('block width',Block.Width,Blk.Width);
    AssertEquals('block height',Block.Height,Blk.Height);
    AssertEquals('block descender',Block.Descender,Blk.Descender);
  finally
    Blk.Free;
  end;
end;

procedure TTestTextBlock.TestSplit;
var
  Blk : TTextBlock;

begin
  FakeLayoutBlock(Block);

  Blk:=Block.Split(2);
  try
    AssertEquals('old block offset',0,Block.TextOffset);
    AssertEquals('old block length',2,Block.TextLen);
    AssertEquals('new block offset',2,Blk.TextOffset);
    AssertEquals('new block length',2,Blk.TextLen);
    AssertEquals('block font name',Block.Font.Name,Blk.Font.Name);
    AssertEquals('block font Size',12,Block.Font.Size,Blk.Font.Size);
    AssertTrue('block font attrs',Block.Font.Attrs=Blk.Font.Attrs);
    AssertTrue('block font color',FTextBlock.Font.Color=Blk.Font.Color);
    AssertEquals('block layout pos X',0,Blk.LayoutPos.X);
    AssertEquals('block layout pos Y',0,Blk.LayoutPos.Y);
    AssertEquals('block width',0,Blk.Width);
    AssertEquals('block height',0,Blk.Height);
    AssertEquals('block descender',0,Blk.Descender);
    AssertEquals('block Forcenewline',False,Blk.ForceNewLine);
  finally
    Blk.Free;
  end;
end;

procedure TTestTextBlock.TestSplit2;
var
  Blk : TTextBlock;

begin
  Layouter.Text:='one for the road';
  Block.TextOffset:=4;
  Block.TextLen:=7;
  AssertEquals('Orig block Text','for the',Block.Text);
  FakeLayoutBlock(Block);
  Blk:=Block.Split(4);
  try
    AssertEquals('old block offset',4,Block.TextOffset);
    AssertEquals('old block length',4,Block.TextLen);
    AssertEquals('old block Text','for ',Block.Text);
    AssertEquals('new block offset',8,Blk.TextOffset);
    AssertEquals('new block length',3,Blk.TextLen);
    AssertEquals('new block Text','the',Blk.Text);
    AssertEquals('block font name',Block.Font.Name,Blk.Font.Name);
    AssertEquals('block font Size',12,Block.Font.Size,Blk.Font.Size);
    AssertTrue('block font attrs',Block.Font.Attrs=Blk.Font.Attrs);
    AssertTrue('block font color',FTextBlock.Font.Color=Blk.Font.Color);
    AssertEquals('block layout pos X',0,Blk.LayoutPos.X);
    AssertEquals('block layout pos Y',0,Blk.LayoutPos.Y);
    AssertEquals('block width',0,Blk.Width);
    AssertEquals('block height',0,Blk.Height);
    AssertEquals('block descender',0,Blk.Descender);
    AssertEquals('block Forcenewline',False,Blk.ForceNewLine);
  finally
    Blk.Free;
  end;
end;

{ TTestTextLayouter }

procedure TTestTextLayouter.TestHookUp;
begin
  AssertNotNull('Have layouter',Layouter);
  AssertEquals('blocks',0,Layouter.TextBlockCount);
  AssertEquals('bounds height',0.0,Layouter.Bounds.Height,0.01);
  AssertEquals('bounds width',0.0,Layouter.Bounds.Width,0.01);
  AssertEquals('Font name','Arial',cFontName);
  AssertEquals('Font size',12,cFontSize);
  AssertEquals('Line spacing',cLineSpacing,FLayouter.LineSpacing);
  AssertTrue('Font attrs',[]=cFontAttr);
end;


class procedure TTestTextLayouter.AssertBlock(Msg: String; aBlock : TTextBlock; atext : String; aPosX,aPosY,aWidth,aHeight : TTextUnits; aLineBreak: Boolean = False);

begin
  Msg:=Msg+': ';
  AssertNotNull(Msg+'Have block',aBlock);
  AssertEquals(Msg+'Text',aText,aBlock.Text);
  AssertEquals(Msg+'Pos.X',aPosX,aBlock.LayoutPos.X,cDelta);
  AssertEquals(Msg+'Pos.Y',aPosY,aBlock.LayoutPos.Y,cDelta);
  AssertEquals(Msg+'width',aWidth,aBlock.Width,cDelta);
  AssertEquals(Msg+'height',aHeight,aBlock.Height,cDelta);
  AssertEquals(Msg+'linebreak',aLineBreak,aBlock.ForceNewLine);
end;

procedure TTestTextLayouter.AssertBlock(Msg: String; Idx : Integer; atext : String; aPosX,aPosY,aWidth,aHeight : TTextUnits; aLineBreak: Boolean = False);

begin
  AssertTrue(Msg+Format('Block index %d OK',[Idx]),(Idx>=0) and (Idx<Layouter.TextBlockCount));
  AssertBlock(Msg,Layouter.TextBlocks[Idx],aText,aPosX,aPosY,aWidth,aHeight,aLineBreak);
end;

procedure TTestTextLayouter.TestSimpleText;

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:='Some text with spaces.';
  MyWidth:=Length(Layouter.Text)*cWidth;
  MyHeight:=cHeight;
  Layouter.Bounds.Width:=myWidth*1.1;
  Layouter.Bounds.Height:=cHeight*1.1;
  AssertEquals('block count',1,Layouter.Execute);
  AssertBlock('Block',0,Layouter.Text,0,0,MyWidth,MyHeight);
  AssertEquals('TotalWidth',MyWidth,Layouter.GetTotalWidth);
  AssertEquals('TotalHeight',MyHeight,Layouter.GetTotalHeight);
end;

procedure TTestTextLayouter.TestSimpleMultiLineText;

const
  cLine1 = 'Some text';
  cLine2 = 'with spaces.';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1+#10+cLine2;
  MyWidth:=Length(Layouter.Text)*cWidth;
  MyHeight:=(cHeight+cLineSpacing)*2;
  Layouter.Bounds.Width:=myWidth*1.1;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',2,Layouter.Execute);
  // First block
  MyWidth:=Length(cLine1)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block',0,cLine1,0,0,MyWidth,MyHeight);
  // Second block
  MyWidth:=Length(cLine2)*cWidth;
  AssertBlock('Block',1,cLine2,0,cHeight+cLineSpacing,MyWidth,MyHeight,True);
  // Second block is larger
  AssertEquals('TotalWidth',MyWidth,Layouter.GetTotalWidth);
  AssertEquals('TotalHeight',MyHeight*2+cLineSpacing,Layouter.GetTotalHeight);
end;

procedure TTestTextLayouter.TestSimpleMultiLineTextCRLF;

const
  cLine1 = 'Some text';
  cLine2 = 'with spaces.';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1+#13#10+cLine2;
  MyWidth:=Length(Layouter.Text)*cWidth;
  MyHeight:=(cHeight+cLineSpacing)*2;
  Layouter.Bounds.Width:=myWidth*1.1;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',2,Layouter.Execute);
  // First block
  MyWidth:=Length(cLine1)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block',0,cLine1,0,0,MyWidth,MyHeight);
  // Second block
  MyWidth:=Length(cLine2)*cWidth;
  AssertBlock('Block',1,cLine2,0,cHeight+cLineSpacing,MyWidth,MyHeight,True);
end;

procedure TTestTextLayouter.TestSimpleMultiLineTextWhiteSpace;

const
  cLine1 = 'Some text';
  cLine2 = 'with spaces.';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1+' '#10+cLine2;
  MyWidth:=Length(Layouter.Text)*cWidth;
  MyHeight:=(cHeight+cLineSpacing)*2;
  Layouter.Bounds.Width:=myWidth*1.1;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',2,Layouter.Execute);
  // First block
  MyWidth:=Length(cLine1)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block',0,cLine1,0,0,MyWidth,MyHeight);
  // Second block
  MyWidth:=Length(cLine2)*cWidth;
  AssertBlock('Block',1,cLine2,0,cHeight+cLineSpacing,MyWidth,MyHeight,True);
end;

procedure TTestTextLayouter.TestSimpleMultiLineTextWhiteSpaceCRLF;
const
  cLine1 = 'Some text';
  cLine2 = 'with spaces.';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1+'  '#13#10+cLine2;
  MyWidth:=Length(Layouter.Text)*cWidth;
  MyHeight:=(cHeight+cLineSpacing)*2;
  Layouter.Bounds.Width:=myWidth*1.1;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',2,Layouter.Execute);
  // First block
  MyWidth:=Length(cLine1)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block',0,cLine1,0,0,MyWidth,MyHeight);
  // Second block
  MyWidth:=Length(cLine2)*cWidth;
  AssertBlock('Block',1,cLine2,0,cHeight+cLineSpacing,MyWidth,MyHeight,True);
end;

procedure TTestTextLayouter.TestSimpleWrapText;
const
  cLine1 = 'Some text';
  cLine2 = 'with spaces.';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1+' '+cLine2;
  Layouter.WordWrap:=True;
  MyWidth:=(Length(cLine2)+0.5)*cWidth;
  MyHeight:=(cHeight+cLineSpacing)*2;
  Layouter.Bounds.Width:=myWidth;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',2,Layouter.Execute);
  // First block
  MyWidth:=Length(cLine1)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block 1',0,cLine1,0,0,MyWidth,MyHeight);
  // Second block
  MyWidth:=Length(cLine2)*cWidth;
  AssertBlock('Block 2',1,cLine2,0,cHeight+cLineSpacing,MyWidth,MyHeight,True);
end;

procedure TTestTextLayouter.TestSimpleWrapTextHyphen;

const
  cLine1 = 'Some text';
  cLine2 = 'with spaces.';
  cSplit1 = 'Some text with spa-';
  cSplit2 = 'ces.';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1+' '+cLine2;
  Layouter.WordWrap:=True;
  Layouter.AllowHyphenation:=True;
  MyWidth:=(Length(cSplit1)+0.5)*cWidth;
  MyHeight:=(cHeight+cLineSpacing)*2;
  Layouter.Bounds.Width:=myWidth;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',2,Layouter.Execute);
  // First block
  MyWidth:=Length(cSplit1)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block 1',0,cSplit1,0,0,MyWidth,MyHeight);
  // Second block
  MyWidth:=Length(cSplit2)*cWidth;
  AssertBlock('Block 2',1,cSplit2,0,cHeight+cLineSpacing,MyWidth,MyHeight,True);
end;

procedure TTestTextLayouter.TestSimpleWrapTextOnewordOverflow;
// One word, too long for wordwrap.
// Overflow
const
  cLine1 = 'longevity';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1;
  Layouter.WordWrap:=True;
  Layouter.WordOverflow:=woOverflow;
  MyWidth:=5*cWidth;
  MyHeight:=(cHeight+cLineSpacing);
  Layouter.Bounds.Width:=myWidth*1.1;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',1,Layouter.Execute);
  MyWidth:=Length(cLine1)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block 1',0,cLine1,0,0,MyWidth,MyHeight);
end;

procedure TTestTextLayouter.TestSimpleWrapTextOnewordTruncate;
// One word, too long for wordwrap.
// Explicitly set truncate (although it is the default)


const
  cLine1 = 'longevity';
  cRes   = 'longe';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1;
  Layouter.WordOverflow:=woTruncate;
  MyWidth:=5.5*cWidth;
  MyHeight:=(cHeight+cLineSpacing);
  Layouter.Bounds.Width:=myWidth;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',1,Layouter.Execute);
  MyWidth:=Length(cRes)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block 1',0,cRes,0,0,MyWidth,MyHeight);
end;

procedure TTestTextLayouter.TestSimpleWrapTextOnewordTruncateEmpty;
// One word, too long for wordwrap.
// Explicitly set truncate (although it is the default)

const
  cLine1 = 'longevity';

var
  myHeight,myWidth : TTextUnits;

begin
  Layouter.Text:=cLine1;
  Layouter.WordOverflow:=woTruncate;
  MyWidth:=0.5*cWidth;
  MyHeight:=(cHeight+cLineSpacing);
  Layouter.Bounds.Width:=myWidth;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',1,Layouter.Execute);
  MyWidth:=0;
  MyHeight:=cHeight;
  AssertBlock('Block 1',0,'',0,0,MyWidth,MyHeight);
end;

procedure TTestTextLayouter.TestSimpleWrapTextOnewordEllipsis;
// One word, too long for wordwrap.

const
  cLine1 = 'longevity';

var
  myHeight,myWidth : TTextUnits;
  cRes : String;

begin
  Layouter.Text:=cLine1;
  Layouter.WordOverflow:=woEllipsis;
  MyWidth:=5.5*cWidth;
  MyHeight:=(cHeight+cLineSpacing);
  Layouter.Bounds.Width:=myWidth;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',1,Layouter.Execute);
  cRes:='lo'+UTF8Encode(cEllipsis); // 3 characters in UTF8
  MyWidth:=Length(cRes)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block 1',0,cRes,0,0,MyWidth,MyHeight);
end;

procedure TTestTextLayouter.TestSimpleWrapTextOnewordAsterisk;
// One word, too long for wordwrap.

const
  cLine1 = 'longevity';

var
  myHeight,myWidth : TTextUnits;
  cRes : String;

begin
  Layouter.Text:=cLine1;
  Layouter.WordOverflow:=woAsterisk;
  MyWidth:=5.5*cWidth;
  MyHeight:=(cHeight+cLineSpacing);
  Layouter.Bounds.Width:=myWidth;
  Layouter.Bounds.Height:=myHeight*1.1;
  AssertEquals('block count',1,Layouter.Execute);
  cRes:='long*';
  MyWidth:=Length(cRes)*cWidth;
  MyHeight:=cHeight;
  AssertBlock('Block 1',0,cRes,0,0,MyWidth,MyHeight);
end;

procedure TTestTextLayouter.TestRangeOverlap;
begin
  Layouter.Text:='Some text with spaces';
  Layouter.Ranges.AddRange(0,10,Font);
  Layouter.Ranges.AddRange(9,Length(Layouter.Text)-9,Font);
  AssertException('Ranges cannot overlap',ETextLayout,@Layouter.CheckRanges);
end;

procedure TTestTextLayouter.TestRangeOverlapFit;

var
  I : Integer;

begin
  Layouter.Text:='Some text with spaces';
  Layouter.Ranges.AddRange(9,Length(Layouter.Text)-9,Font);
  Layouter.Ranges.AddRange(0,10,Font);
  Layouter.OverlappingRangesAction:=oraFit;
  Layouter.CheckRanges;
  For I:=0 to Layouter.Ranges.Count-1 do
    Writeln(i,' : ',Layouter.Ranges[i].ToString);
  With Layouter.Ranges[0] do
    begin
    AssertEquals('Range 0 offset',0,CharOffset);
    AssertEquals('Range 0 length',9,CharLength);
    end;
  With Layouter.Ranges[1] do
    begin
    AssertEquals('Range 1 offset',9,CharOffset);
    AssertEquals('Range 1 length',Length(Layouter.Text)-9,CharLength);
    end;
end;

procedure TTestTextLayouter.TestRangeText;

Const
  P1 = 'Some text';
  P2 = ' with spaces.';

var

  myHeight1,myHeight2,myWidth1,myWidth2 : TTextUnits;
  F2 : TTextFont;

begin
  Layouter.Text:=P1+P2;
  F2:=Font.Clone;
  try
    F2.Size:=24;
    // 'Some text' in large font.
    Layouter.Ranges.AddRange(0,Length(P1),F2);
  finally
    F2.Free;
  end;
  MyWidth1:=Length(P1)*(cWidth*2);
  MyWidth2:=Length(P2)*(cWidth);
  MyHeight1:=cHeight*2;
  MyHeight2:=cHeight;
  Layouter.Bounds.Width:=(myWidth1+MyWidth2)*1.1;
  Layouter.Bounds.Height:=MyHeight2*1.1;
  AssertEquals('block count',2,Layouter.Execute);
  AssertBlock('Block 1',0,P1,0,0,MyWidth1,MyHeight1);
  AssertBlock('Block 2',1,P2,MyWidth1,0,MyWidth2,MyHeight2);
end;

procedure TTestTextLayouter.TestRangeTextOffset;
Const
  P1 = 'Some text';
  P2 = ' with spaces.';

var

  myHeight1,myHeight2,myWidth1,myWidth2 : TTextUnits;
  F2 : TTextFont;

begin
  Layouter.Text:=P1+P2;
  F2:=Font.Clone;
  try
    F2.Size:=24;
    // ' with spaces' in large font.
    Layouter.Ranges.AddRange(Length(P1),Length(P2),F2);
  finally
    F2.Free;
  end;
  MyWidth1:=Length(P1)*(cWidth);
  MyWidth2:=Length(P2)*(cWidth*2);
  MyHeight1:=cHeight;
  MyHeight2:=cHeight*2;
  Layouter.Bounds.Width:=(myWidth1+MyWidth2)*1.1;
  Layouter.Bounds.Height:=MyHeight2*1.1;
  AssertEquals('block count',2,Layouter.Execute);
  AssertBlock('Block 1',0,P1,0,0,MyWidth1,MyHeight1);
  AssertBlock('Block 2',1,P2,MyWidth1,0,MyWidth2,MyHeight2);
end;

procedure TTestTextLayouter.TestRangeTextOffset2;

Const
  P1 = 'Some text ';
  P2 = 'with';
  P3 = ' spaces.';

var
  myHeight1,myHeight2,myHeight3,
  myWidth1,myWidth2,myWidth3 : TTextUnits;
  F2 : TTextFont;

begin
  // 'with' in regular font, 'Some text ' and ' spaces.' in large font
  Layouter.Text:=P1+P2+P3;
  F2:=Font.Clone;
  try
    F2.Size:=24;
    Layouter.Ranges.AddRange(0,Length(P1),F2);
    Layouter.Ranges.AddRange(Length(P1)+Length(P2),Length(P3),F2);
  finally
    F2.Free;
  end;
  MyWidth1:=Length(P1)*(cWidth*2);
  MyWidth2:=Length(P2)*(cWidth);
  MyWidth3:=Length(P3)*(cWidth*2);
  MyHeight1:=cHeight*2;
  MyHeight2:=cHeight;
  MyHeight3:=cHeight*2;
  Layouter.Bounds.Width:=(myWidth1+MyWidth2+myWidth3)*1.1;
  Layouter.Bounds.Height:=MyHeight3*1.1;
  AssertEquals('block count',3,Layouter.Execute);
  AssertBlock('Block 1',0,P1,0,0,MyWidth1,MyHeight1);
  AssertBlock('Block 2',1,P2,MyWidth1,0,MyWidth2,MyHeight2);
  AssertBlock('Block 3',2,P3,MyWidth1+MyWidth2,0,MyWidth3,MyHeight3);
end;

procedure TTestTextLayouter.TestRangeTextOffset3;
begin

end;

initialization

  RegisterTests([TTestSplitter,TTestTextBlock,TTestTextLayouter]);
end.

