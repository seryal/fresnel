unit TCFresnelImages;

{$mode ObjFPC}{$H+}
{$IF FPC_FULLVERSION>30300}
  {$DEFINE HasIOUtils}
{$ENDIF}

interface

uses
  Classes, SysUtils, fpcunit, fresnel.images, testregistry, fpimage;

Type

  { TImageTest }

  TImageTest = Class(TTestCase)
  private
    FImagesDir : String;
    procedure ReadConfig;
  Public
    Function TestImagesDir : String;
    Function GetStdImage(aSize : integer) : String;
  end;

  { TTestImageConfig }

  TTestImageConfig = Class(TImageTest)
  private
    FConfig: TImagesConfig;
  Public
    Procedure Setup; override;
    Procedure TearDown; override;
    Property Config : TImagesConfig read FConfig;
  Published
    Procedure TestHookup;
    Procedure TestSetImageDir;
    Procedure TestImageExtension;
    Procedure TestImageFileNameDefault;
    Procedure TestImageFileNameResolution;
    Procedure TestImageFileNameSizeDir;
    Procedure TestImageSizedFileNameDefault;
    Procedure TestImageSizedFileNameResolution;
    Procedure TestImageSizedFileNameSizeDir;
    Procedure TestImageSizedFileNameResolutionAndSizeDir;
  end;

  { TTestResolutionList }

  TTestResolutionList = class(TImageTest)
  private
    FList: TResolutionList;
  Public
    Procedure SetUp; override;
    Procedure TearDown; override;
    Property List : TResolutionList Read FList;
  Published
    Procedure TestHookup;
    Procedure TestAddResolution;
    Procedure TestFindDefaultResolution;
    Procedure TestIndexOfResolution;
  end;

  { TTestImageList }

  TTestImageList = Class(TImageTest)
  private
    FImageList: TImageList;
  Public
    Procedure Setup; override;
    Procedure TearDown; override;
    Property List : TImageList Read FImageList;
  Published
    Procedure TestHookup;
    Procedure TestLoadImage;
    Procedure TestIsImageIndexValid;
    Procedure TestGetRawImage;
    Procedure TestGetImageAsResult;
    Procedure TestGetImageArgument;
  end;

  { TTestImageStore }

  TTestImageStore = Class(TImageTest)
  private
    FFreeImg: TFPCustomImage;
    FStore: TImageStore;
    FDir : string;
  Public
    Procedure CreateImages;
    Procedure Setup; override;
    Procedure TearDown; override;
    Property Store : TImageStore Read FStore;
    Property FreeImg: TFPCustomImage Read FFreeImg;
  Published
    Procedure TestHookup;
    procedure TestGetImage;
    procedure TestSetResolution;
    procedure TestSetSize;
    procedure TestNoSize;
    procedure TestNoResolution;
  end;

  { TTestFresnelImageData }

  TTestFresnelImageData = Class(TImageTest)
  private
    FData: TImageData;
  Public
    Procedure Setup; override;
    Procedure TearDown; override;
    Property Data : TImageData Read FData Write FData;
  Published
    Procedure TestHookup;
  end;

procedure DeleteDir(Dir: string);
procedure CopyFile(Src, Dest: string; Overwrite: boolean = false);

implementation

uses inifiles,
  {$IFDEF HasIOUtils}
  System.IOUtils,
  {$ENDIF}
  fpreadpng, fpwritepng;

procedure DeleteDir(Dir: string);
{$IFDEF HasIOUtils}
{$ELSE}
var
  Info: TRawByteSearchRec;
  List: TStringList;
  i: Integer;
  Filename: String;
{$ENDIF}
begin
  {$IFDEF HasIOUtils}
  TDirectory.Delete(Dir,True);
  {$ELSE}
  if not DirectoryExists(Dir) then exit;
  Info:=Default(TRawByteSearchRec);
  List:=TStringList.Create;
  try
    if FindFirst(Dir+PathDelim+AllFilesMask,faAnyFile,Info)=0 then
      repeat
        if (Info.Name='.') or (Info.Name='..') then continue;
        List.Add(Info.Name);
      until FindNext(Info)<>0;
    for i:=0 to List.Count-1 do
      begin
      Filename:=Dir+PathDelim+List[i];
      if not DeleteFile(Filename) then
        raise Exception.Create('20240906122331 DeleteDir failed '+Filename);
      end;
  finally
    FindClose(Info);
  end;
  {$ENDIF}
end;

procedure CopyFile(Src, Dest: string; Overwrite: boolean);
{$IFDEF HasIOUtils}
{$ELSE}
var
  SrcFS, DestFS: TFileStream;
{$ENDIF}
begin
  {$IFDEF HasIOUtils}
  TFile.Copy(Src,Dest,Overwrite);
  {$ELSE}
  if Overwrite and FileExists(Dest) then
    begin
    if not DeleteFile(Dest) then
      raise Exception.Create('20240906122205 failed to delete file "'+Dest+'"');
    end;

  SrcFS:=TFileStream.Create(Src,fmOpenRead or fmShareDenyNone);
  try
    DestFS:=TFileStream.Create(Dest,fmCreate or fmShareDenyNone);
    try
      DestFS.CopyFrom(SrcFS,SrcFS.Size);
    finally
      DestFS.Free;
    end;
  finally
    SrcFS.Free;
  end;
  {$ENDIF}
end;

{ TImageTest }
Procedure TImageTest.ReadConfig;

var
  Ini : TMemIniFile;

begin
  Ini:=TMemIniFile.Create(ChangeFileExt(ParamStr(0),'.ini'));
  try
    FImagesDir:=Ini.ReadString('Images','Dir','');
  finally
    Ini.Free;
  end;
end;

function TImageTest.TestImagesDir: String;

begin
  ReadConfig;
  Result:=FImagesDir;
  if Result='' then
    Result:=ExtractFilePath(ParamStr(0))+'images';
  Result:=IncludeTrailingPathDelimiter(Result);
end;

function TImageTest.GetStdImage(aSize: integer): String;
begin
  Result:=TestImagesDir+IntToStr(asize)+'.png';
end;

{ TTestImageConfig }

procedure TTestImageConfig.Setup;
begin
  inherited Setup;
  FConfig:=TImagesConfig.Create;
end;

procedure TTestImageConfig.TearDown;
begin
  FreeAndNil(FConfig);
  inherited TearDown;
end;

procedure TTestImageConfig.TestHookup;
begin
  AssertNotNull('Have config',Config);
  AssertEquals('Default image dir',ExtractFilePath(ParamStr(0)),Config.ImageDir);
  AssertEquals('Default image extension',DefaultImageExtension,Config.ImageExtension);
  AssertEquals('Default image resolution',DefaultImageResolution,Config.Resolution);
  AssertEquals('Default icon size',DefaultIconSIze,Config.IconSize);
  AssertTrue('Default options',Config.Options=[]);
end;

procedure TTestImageConfig.TestSetImageDir;
begin
  Config.ImageDir:='/tmp/';
  AssertEquals('Correct value','/tmp/',Config.ImageDir);
  Config.ImageDir:='/tmp/something';
  AssertEquals('Correct value','/tmp/something/',Config.ImageDir);
end;

procedure TTestImageConfig.TestImageExtension;
begin
  Config.ImageExtension:='';
  AssertEquals('Reset default image extension',DefaultImageExtension,Config.ImageExtension);
  Config.ImageExtension:='bmp';
  AssertEquals('Extension has initial dot','.bmp',Config.ImageExtension);
end;

procedure TTestImageConfig.TestImageFileNameDefault;
begin
  AssertEquals('Default filename',Config.ImageDir+'so'+DefaultImageExtension,Config.GetImageFileName('so'));
  AssertEquals('Resolution filename',Config.ImageDir+'so_96'+DefaultImageExtension,Config.GetImageFileName('so',96));
end;

procedure TTestImageConfig.TestImageFileNameResolution;
begin
  Config.Options:=Config.Options+[icoResolutionDir];
  AssertEquals('Default resolution filename',Config.ImageDir+'96/so'+DefaultImageExtension,Config.GetImageFileName('so',96));
end;

procedure TTestImageConfig.TestImageFileNameSizeDir;
begin
  Config.Options:=Config.Options+[icoSizeDir];
  AssertEquals('Default filename',Config.ImageDir+'so'+DefaultImageExtension,Config.GetImageFileName('so'));
  AssertEquals('Default resolution filename',Config.ImageDir+'so_96'+DefaultImageExtension,Config.GetImageFileName('so',96));
end;

procedure TTestImageConfig.TestImageSizedFileNameDefault;
begin
  AssertEquals('Default filename (no resolution)',Config.ImageDir+'so_48x48'+DefaultImageExtension,Config.GetSizedImageFileName('so',48));
  AssertEquals('Default filename (no resolution)',Config.ImageDir+'so_24x24'+DefaultImageExtension,Config.GetSizedImageFileName('so'));
  AssertEquals('Default filename (resolution)',Config.ImageDir+'so_96_48x48'+DefaultImageExtension,Config.GetSizedImageFileName('so',48,96));
  Config.Options:=Config.Options+[icoSizeFirst];
  AssertEquals('Default filename (resolution, size first)',Config.ImageDir+'so_48x48_96'+DefaultImageExtension,Config.GetSizedImageFileName('so',48,96));
end;

procedure TTestImageConfig.TestImageSizedFileNameResolution;
begin
  Config.Options:=Config.Options+[icoResolutionDir];
  AssertEquals('Default filename (no resolution)',Config.ImageDir+'so_48x48'+DefaultImageExtension,Config.GetSizedImageFileName('so',48));
  AssertEquals('Default filename (resolution)',Config.ImageDir+'96/so_48x48'+DefaultImageExtension,Config.GetSizedImageFileName('so',48,96));
  Config.Options:=Config.Options+[icoSizeFirst];
  AssertEquals('Default filename (resolution, size first)',Config.ImageDir+'96/so_48x48'+DefaultImageExtension,Config.GetSizedImageFileName('so',48,96));

end;

procedure TTestImageConfig.TestImageSizedFileNameSizeDir;
begin
  Config.Options:=Config.Options+[icoSizeDir];
  AssertEquals('Default filename (no resolution)',Config.ImageDir+'48x48/so'+DefaultImageExtension,Config.GetSizedImageFileName('so',48));
  AssertEquals('Default filename (resolution)',Config.ImageDir+'48x48/so_96'+DefaultImageExtension,Config.GetSizedImageFileName('so',48,96));
  Config.Options:=Config.Options+[icoSizeFirst];
  AssertEquals('Default filename (resolution, size first)',Config.ImageDir+'48x48/so_96'+DefaultImageExtension,Config.GetSizedImageFileName('so',48,96));

end;

procedure TTestImageConfig.TestImageSizedFileNameResolutionAndSizeDir;
begin
  Config.Options:=Config.Options+[icoSizeDir,icoResolutionDir];
  AssertEquals('Default filename (no resolution)',Config.ImageDir+'48x48/so'+DefaultImageExtension,Config.GetSizedImageFileName('so',48));
  AssertEquals('Default filename (resolution)',Config.ImageDir+'96/48x48/so'+DefaultImageExtension,Config.GetSizedImageFileName('so',48,96));
  Config.Options:=Config.Options+[icoSizeFirst];
  AssertEquals('Default filename (resolution, size first)',Config.ImageDir+'48x48/96/so'+DefaultImageExtension,Config.GetSizedImageFileName('so',48,96));
end;

{ TTestResolutionList }

procedure TTestResolutionList.SetUp;
begin
  inherited SetUp;
  FList:=TResolutionList.Create(Nil,TResolution);
end;

procedure TTestResolutionList.TearDown;
begin
  inherited TearDown;
  FreeAndNil(FList);
end;

procedure TTestResolutionList.TestHookup;
begin
  AssertNotNull('Have list',List);
  AssertEquals('No resolutions',0,List.Count);
end;

procedure TTestResolutionList.TestAddResolution;

  procedure AddData;

  begin
    List.AddResolution(96);
  end;

var
  R : TResolution;

begin
  R:=List.AddResolution(96);
  AssertNotNull('Have result',R);
  AssertEquals('Correct resolution',96,R.Resolution);
  AssertFalse('Default',R.Default);
  {$IFDEF HasFunctionReferences}
  AssertException('No second resolution',EImageData,@AddData);
  {$ENDIF}
end;

procedure TTestResolutionList.TestFindDefaultResolution;
var
  R : TResolution;

begin
  R:=List.AddResolution(96);
  R.Default:=True;
  List.AddResolution(72);
  AssertSame('Correct default',R,List.FindDefaultResolution);
end;

procedure TTestResolutionList.TestIndexOfResolution;
begin
  List.AddResolution(96);
  List.AddResolution(72);
  List.AddResolution(128);
  List.AddResolution(224);
  AssertEquals('Index of 72',1,List.IndexOfResolution(72));

end;

{ TTestImageList }

procedure TTestImageList.Setup;
begin
  inherited Setup;
  FImageList:=TImageList.Create(NIl);
end;

procedure TTestImageList.TearDown;
begin
  FreeandNil(FImageList);
  inherited TearDown;
end;

procedure TTestImageList.TestHookup;
begin
  AssertNotNull('Have list',List);
  AssertEquals('Empty',0,List.ImageCount);
  AssertEquals('Resolution',1,List.Resolutions.Count);
end;

procedure TTestImageList.TestLoadImage;

var
  Img : TFPCustomImage;

begin
  Img:=List.LoadImageFromFile(0,0,GetStdImage(10));
  AssertEquals('Image count',1,List.ImageCount);
  AssertNotNull('Have image',Img);
  AssertEquals('Width',10,Img.Width);
end;

procedure TTestImageList.TestIsImageIndexValid;
begin
  List.LoadImageFromFile(0,0,GetStdImage(10));
  AssertTrue('Correct index',List.IsValidImageIndex(0));
  AssertFalse('Incorrect negative index',List.IsValidImageIndex(-1));
  AssertFalse('Incorrect index',List.IsValidImageIndex(1));
end;

procedure TTestImageList.TestGetRawImage;

var
  Img : TFPCustomImage;

begin
  List.LoadImageFromFile(0,0,GetStdImage(10));
  AssertNull('Negative Index',List.GetRawImage(-1,0));
  AssertNull('Non-existing resolution',List.GetRawImage(0,400));
  AssertNull('Non-existing Index',List.GetRawImage(1,0));
  Img:=List.GetRawImage(0,0);
  AssertNotNull('Have image',Img);
  AssertEquals('Width',10,Img.Width);
end;

procedure TTestImageList.TestGetImageAsResult;
var
  Img : TFPCustomImage;
begin
  Img:=List.GetImage(0,0);
  AssertNotNull('Have image',Img);
  AssertEquals('Width',24,Img.Width);
  Img.Free;
  List.LoadImageFromFile(0,0,GetStdImage(10));
  Img:=List.GetImage(0,0);
  AssertNotNull('Have image',Img);
  AssertEquals('Width',10,Img.Width);
  Img.Free;
end;

procedure TTestImageList.TestGetImageArgument;
var
  Img : TFPCustomImage;
begin
  Img:=TFPMemoryImage.Create(0,0);
  try
    assertFalse('No such image',List.GetImage(0,0,Img));
    AssertEquals('Width',0,Img.Width);
    List.LoadImageFromFile(0,0,GetStdImage(10));
    assertTrue('have image',List.GetImage(0,0,Img));
    AssertEquals('Width',10,Img.Width);
  finally
    Img.Free;
  end;
end;

{ TTestImageStore }

procedure TTestImageStore.CreateImages;
begin
  CopyFile(GetStdImage(10),ImagesConfig.GetSizedImageFileName('img'));
  CopyFile(GetStdImage(20),ImagesConfig.GetSizedImageFileName('img',ImagesConfig.IconSize*2));
  CopyFile(GetStdImage(30),ImagesConfig.GetSizedImageFileName('img',ImagesConfig.IconSize,192));
end;

procedure TTestImageStore.Setup;
begin
  inherited Setup;
  FStore:=TImageStore.Create(Nil);
  FDir:=GetTempDir(False)+HexStr(Random(Maxint),8);
  AssertTrue(ForceDirectories(FDir));
  ImagesConfig.ImageDir:=FDir;
end;

procedure TTestImageStore.TearDown;
begin
  FreeAndNil(FFreeImg);
  DeleteDir(FDir);
  FreeAndNil(FStore);
  inherited TearDown;
end;

procedure TTestImageStore.TestHookup;
begin
  AssertNotNull('Have store',Store);
  AssertEquals('Store empty',0,Store.CachedImageCount);
  AssertEquals('Default size',ImagesConfig.IconSize,Store.Size);
  AssertEquals('Default resolution',ImagesConfig.Resolution,Store.Resolution);
end;

procedure TTestImageStore.TestGetImage;

begin
  CreateImages;
  Store.GetImageData('img',FFreeImg,True);
  AssertNotNull('Have image',FreeImg);
  AssertEquals('Have correct image',10,FreeImg.Width);
  FreeAndNil(FFreeImg);
  AssertEquals('Image in store',1,Store.CachedImageCount);
  Store.GetImageData('img',FFreeImg,True);
  AssertEquals('Image only once in store',1,Store.CachedImageCount);
end;

procedure TTestImageStore.TestSetResolution;
begin
  CreateImages;
  Store.GetImageData('img',FFreeImg,True);
  AssertNotNull('Have image',FreeImg);
  AssertEquals('Image in store',1,Store.CachedImageCount);
  FreeAndNil(FFreeImg);
  Store.Resolution:=192;
  AssertEquals('Cache cleared',0,Store.CachedImageCount);
  Store.GetImageData('img',FFreeImg,True);
  AssertNotNull('Have image',FreeImg);
  AssertEquals('Have correct image',30,FreeImg.Width);
end;

procedure TTestImageStore.TestSetSize;
begin
  CreateImages;
  Store.GetImageData('img',FFreeImg,True);
  AssertNotNull('Have image',FreeImg);
  AssertEquals('Image in store',1,Store.CachedImageCount);
  FreeAndNil(FFreeImg);
  Store.Size:=Store.Size*2;
  AssertEquals('Cache cleared',0,Store.CachedImageCount);
  Store.GetImageData('img',FFreeImg,True);
  AssertNotNull('Have image',FreeImg);
  AssertEquals('Have correct image',20,FreeImg.Width);
end;

procedure TTestImageStore.TestNoSize;
begin
  CreateImages;
  CopyFile(GetStdImage(30),ImagesConfig.ImageDir+'img_96.png');
  Store.Size:=0;
  Store.GetImageData('img',FFreeImg,True);
  AssertNotNull('Have image',FreeImg);
  AssertEquals('Have correct image',30,FreeImg.Width);
  AssertEquals('Image in store',1,Store.CachedImageCount);
end;

procedure TTestImageStore.TestNoResolution;
begin
  CreateImages;
  CopyFile(GetStdImage(30),ImagesConfig.ImageDir+'img_24x24.png',true);
  Store.Size:=24;
  Store.Resolution:=0;
  Store.GetImageData('img',FFreeImg,True);
  AssertNotNull('Have image',FreeImg);
  AssertEquals('Have correct image',30,FreeImg.Width);
  AssertEquals('Image in store',1,Store.CachedImageCount);
end;

{ TTestFresnelImageData }

procedure TTestFresnelImageData.Setup;
begin
  inherited Setup;
  FData:=TImageData.Create(Nil);
end;

procedure TTestFresnelImageData.TearDown;
begin
  FreeAndNil(FData);
  inherited TearDown;
end;

procedure TTestFresnelImageData.TestHookup;
begin
  AssertNotNull('Have data',Data)
end;


initialization
  Randomize;
  RegisterTests([TTestImageConfig,TTestFresnelImageData,TTestResolutionList,TTestImageList,TTestImageStore])
end.

