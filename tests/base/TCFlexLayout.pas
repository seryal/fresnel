unit TCFlexLayout;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, testregistry, TCFresnelCSS, Fresnel.Controls, Fresnel.DOM;

type

  { TTestFlexLayout }

  TTestFlexLayout = class(TCustomTestFresnelCSS)
  published
    procedure TestFlexLayout_Empty;
    procedure TestFlexLayout_Empty_FlexInline;
    procedure TestFlexLayout_Row_OneItem;
    procedure TestFlexLayout_Row_OneItem_NoGrow;
    procedure TestFlexLayout_Row_OneItem_Grow;
    procedure TestFlexLayout_Row_OneItem_Shrink;
    procedure TestFlexLayout_Row_TwoItems_Grow;
    procedure TestFlexLayout_Row_TwoItems_JustifyCenter;
    // todo: test flex-direction:row, flex-wrap:nowrap
    // todo: test flex-direction:row-reverse, flex-wrap:nowrap
    // todo: test flex-direction:row, flex-wrap:wrap
    // todo: test flex-direction:row-reverse, flex-wrap:wrap
    // todo: test flex-direction:row, flex-wrap:wrap-reverse
    // todo: test flex-direction:row-reverse, flex-wrap:wrap-reverse
    // todo: test flex-direction:column, flex-wrap:nowrap
    // todo: test flex-direction:column-reverse, flex-wrap:nowrap
    // todo: test flex-direction:column, flex-wrap:wrap
    // todo: test flex-direction:column-reverse, flex-wrap:wrap
    // todo: test flex-direction:column, flex-wrap:wrap-reverse
    // todo: test flex-direction:column-reverse, flex-wrap:wrap-reverse
    // todo: test child visibility:collapse
    // todo: test child visibility:hidden
    // todo: test child position:relative
    // todo: test child position:absolute
    // todo: test child position:fixed
    // todo: test child position:sticky
    // todo: test justify-content: left, right, start, end, flex-start, flex-end, center, space-around, space-between, space-evenly
    // todo: test align-items: stretch, normal, left, right, start, end, flex-start, flex-end, center, baseline, first baseline, last baseline
    // todo: test column-gap, row-gap
    // todo: test padding-left,right,top,bottom percentage uses container's width
    // todo: test margin-left,right,top,bottom percentage uses container's width
  end;

implementation

{ TTestFlexLayout }

procedure TTestFlexLayout.TestFlexLayout_Empty;
var
  FlexDiv: TDiv;
begin
  FlexDiv:=TDiv.Create(Viewport);
  FlexDiv.Name:='FlexDiv';
  FlexDiv.Parent:=Viewport;

  Viewport.Stylesheet.Text:=LinesToStr([
  '#FlexDiv { width: 100px; height: 100px; display: flex; }'
  ]);

  Viewport.Draw;
  AssertEquals('FlexDiv.Rendered',true,FlexDiv.Rendered);
  AssertEquals('FlexDiv.GetComputedString(fcaWidth)','100px',FlexDiv.GetComputedString(fcaWidth));
  AssertEquals('FlexDiv.GetComputedString(fcaDisplay)','flex',FlexDiv.GetComputedString(fcaDisplay));
  AssertEquals('FlexDiv.ComputedDisplayInside',CSSRegistry.Keywords[CSSRegistry.kwFlex],CSSRegistry.Keywords[FlexDiv.ComputedDisplayInside]);
  AssertEquals('FlexDiv.ComputedDisplayOutside',CSSRegistry.Keywords[CSSRegistry.kwBlock],CSSRegistry.Keywords[FlexDiv.ComputedDisplayOutside]);
end;

procedure TTestFlexLayout.TestFlexLayout_Empty_FlexInline;
var
  FlexDiv: TDiv;
begin
  FlexDiv:=TDiv.Create(Viewport);
  FlexDiv.Name:='FlexDiv';
  FlexDiv.Parent:=Viewport;

  Viewport.Stylesheet.Text:=LinesToStr([
  '#FlexDiv { width: 100px; height: 100px; display: inline flex; }'
  ]);

  Viewport.Draw;
  AssertEquals('FlexDiv.Rendered',true,FlexDiv.Rendered);
  AssertEquals('FlexDiv.GetComputedString(fcaWidth)','100px',FlexDiv.GetComputedString(fcaWidth));
  AssertEquals('FlexDiv.GetComputedString(fcaDisplay)','inline flex',FlexDiv.GetComputedString(fcaDisplay));
  AssertEquals('FlexDiv.ComputedDisplayInside',CSSRegistry.Keywords[CSSRegistry.kwFlex],CSSRegistry.Keywords[FlexDiv.ComputedDisplayInside]);
  AssertEquals('FlexDiv.ComputedDisplayOutside',CSSRegistry.Keywords[CSSRegistry.kwInline],CSSRegistry.Keywords[FlexDiv.ComputedDisplayOutside]);
end;

procedure TTestFlexLayout.TestFlexLayout_Row_OneItem;
var
  FlexDiv, Div1: TDiv;
  Body: TBody;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  FlexDiv:=TDiv.Create(Viewport);
  FlexDiv.Name:='FlexDiv';
  FlexDiv.Parent:=Body;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=FlexDiv;

  Viewport.Stylesheet.Text:=LinesToStr([
  'body { margin: 0; }',
  '#FlexDiv { display: flex; }',
  '#Div1 { width: 30px; height: 20px; }'
  ]);

  Viewport.Draw;
  AssertEquals('FlexDiv.Rendered',true,FlexDiv.Rendered);
  AssertEquals('FlexDiv.GetComputedString(fcaDisplay)','flex',FlexDiv.GetComputedString(fcaDisplay));

  AssertEquals('Div1.UsedContentBox.Left',0,Div1.UsedContentBox.Left);
  AssertEquals('Div1.UsedContentBox.Top',0,Div1.UsedContentBox.Top);
  AssertEquals('Div1.UsedContentBox.Width',30,Div1.UsedContentBox.Width);
  AssertEquals('Div1.UsedContentBox.Height',20,Div1.UsedContentBox.Height);

  AssertEquals('FlexDiv.UsedContentBox.Left',0,FlexDiv.UsedContentBox.Left);
  AssertEquals('FlexDiv.UsedContentBox.Top',0,FlexDiv.UsedContentBox.Top);
  AssertEquals('FlexDiv.UsedContentBox.Width',800,FlexDiv.UsedContentBox.Width);
  AssertEquals('FlexDiv.UsedContentBox.Height',20,FlexDiv.UsedContentBox.Height);
end;

procedure TTestFlexLayout.TestFlexLayout_Row_OneItem_NoGrow;
var
  FlexDiv, Div1: TDiv;
  Body: TBody;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  FlexDiv:=TDiv.Create(Viewport);
  FlexDiv.Name:='FlexDiv';
  FlexDiv.Parent:=Body;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=FlexDiv;

  Viewport.Stylesheet.Text:=LinesToStr([
  'body { margin: 0; }',
  '#FlexDiv { display: flex; width: 200px; }',
  '#Div1 { width: 30px; height: 20px; }'
  ]);

  Viewport.Draw;
  AssertEquals('FlexDiv.Rendered',true,FlexDiv.Rendered);
  AssertEquals('FlexDiv.GetComputedString(fcaDisplay)','flex',FlexDiv.GetComputedString(fcaDisplay));
  AssertEquals('Div1.GetComputedString(fcaFlexGrow)','0',Div1.GetComputedString(fcaFlexGrow));

  AssertEquals('Div1.UsedContentBox.Left',0,Div1.UsedContentBox.Left);
  AssertEquals('Div1.UsedContentBox.Top',0,Div1.UsedContentBox.Top);
  AssertEquals('Div1.UsedContentBox.Width',30,Div1.UsedContentBox.Width);
  AssertEquals('Div1.UsedContentBox.Height',20,Div1.UsedContentBox.Height);

  AssertEquals('FlexDiv.UsedContentBox.Left',0,FlexDiv.UsedContentBox.Left);
  AssertEquals('FlexDiv.UsedContentBox.Top',0,FlexDiv.UsedContentBox.Top);
  AssertEquals('FlexDiv.UsedContentBox.Width',200,FlexDiv.UsedContentBox.Width);
  AssertEquals('FlexDiv.UsedContentBox.Height',20,FlexDiv.UsedContentBox.Height);
end;

procedure TTestFlexLayout.TestFlexLayout_Row_OneItem_Grow;
var
  FlexDiv, Div1: TDiv;
  Body: TBody;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  FlexDiv:=TDiv.Create(Viewport);
  FlexDiv.Name:='FlexDiv';
  FlexDiv.Parent:=Body;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=FlexDiv;

  Viewport.Stylesheet.Text:=LinesToStr([
  'body { margin: 0; }',
  '#FlexDiv { display: flex; width: 200px; }',
  '#Div1 { width: 30px; height: 20px; flex-grow: 1; }'
  ]);

  Viewport.Draw;
  AssertEquals('FlexDiv.Rendered',true,FlexDiv.Rendered);
  AssertEquals('FlexDiv.GetComputedString(fcaDisplay)','flex',FlexDiv.GetComputedString(fcaDisplay));
  AssertEquals('Div1.GetComputedString(fcaFlexGrow)','1',Div1.GetComputedString(fcaFlexGrow));

  AssertEquals('Div1.UsedContentBox.Left',0,Div1.UsedContentBox.Left);
  AssertEquals('Div1.UsedContentBox.Top',0,Div1.UsedContentBox.Top);
  AssertEquals('Div1.UsedContentBox.Width',200,Div1.UsedContentBox.Width);
  AssertEquals('Div1.UsedContentBox.Height',20,Div1.UsedContentBox.Height);

  AssertEquals('FlexDiv.UsedContentBox.Left',0,FlexDiv.UsedContentBox.Left);
  AssertEquals('FlexDiv.UsedContentBox.Top',0,FlexDiv.UsedContentBox.Top);
  AssertEquals('FlexDiv.UsedContentBox.Width',200,FlexDiv.UsedContentBox.Width);
  AssertEquals('FlexDiv.UsedContentBox.Height',20,FlexDiv.UsedContentBox.Height);
end;

procedure TTestFlexLayout.TestFlexLayout_Row_OneItem_Shrink;
var
  FlexDiv, Div1: TDiv;
  Body: TBody;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  FlexDiv:=TDiv.Create(Viewport);
  FlexDiv.Name:='FlexDiv';
  FlexDiv.Parent:=Body;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=FlexDiv;

  Viewport.Stylesheet.Text:=LinesToStr([
  'body { margin: 0; }',
  '#FlexDiv { display: flex; width: 10px; }',
  '#Div1 { width: 30px; height: 20px; }'
  ]);

  Viewport.Draw;
  AssertEquals('FlexDiv.Rendered',true,FlexDiv.Rendered);
  AssertEquals('FlexDiv.GetComputedString(fcaDisplay)','flex',FlexDiv.GetComputedString(fcaDisplay));

  AssertEquals('Div1.UsedContentBox.Left',0,Div1.UsedContentBox.Left);
  AssertEquals('Div1.UsedContentBox.Top',0,Div1.UsedContentBox.Top);
  AssertEquals('Div1.UsedContentBox.Width',10,Div1.UsedContentBox.Width);
  AssertEquals('Div1.UsedContentBox.Height',20,Div1.UsedContentBox.Height);

  AssertEquals('FlexDiv.UsedContentBox.Left',0,FlexDiv.UsedContentBox.Left);
  AssertEquals('FlexDiv.UsedContentBox.Top',0,FlexDiv.UsedContentBox.Top);
  AssertEquals('FlexDiv.UsedContentBox.Width',10,FlexDiv.UsedContentBox.Width);
  AssertEquals('FlexDiv.UsedContentBox.Height',20,FlexDiv.UsedContentBox.Height);
end;

procedure TTestFlexLayout.TestFlexLayout_Row_TwoItems_Grow;
var
  FlexDiv, Div1, Div2: TDiv;
  Body: TBody;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  FlexDiv:=TDiv.Create(Viewport);
  FlexDiv.Name:='FlexDiv';
  FlexDiv.Parent:=Body;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=FlexDiv;

  Div2:=TDiv.Create(Viewport);
  Div2.Name:='Div2';
  Div2.Parent:=FlexDiv;

  Viewport.Stylesheet.Text:=LinesToStr([
  'body { margin: 0; }',
  '#FlexDiv { display: flex; width: 200px; }',
  '#Div1 { width: 30px; height: 20px; flex-grow: 3; }',
  '#Div2 { width: 70px; height: 20px; flex-grow: 2; }'
  ]);

  Viewport.Draw;
  AssertEquals('FlexDiv.Rendered',true,FlexDiv.Rendered);
  AssertEquals('FlexDiv.GetComputedString(fcaDisplay)','flex',FlexDiv.GetComputedString(fcaDisplay));
  AssertEquals('Div1.GetComputedString(fcaFlexGrow)','3',Div1.GetComputedString(fcaFlexGrow));
  AssertEquals('Div2.GetComputedString(fcaFlexGrow)','2',Div2.GetComputedString(fcaFlexGrow));

  AssertEquals('FlexDiv.UsedContentBox.Left',0,FlexDiv.UsedContentBox.Left);
  AssertEquals('FlexDiv.UsedContentBox.Top',0,FlexDiv.UsedContentBox.Top);
  AssertEquals('FlexDiv.UsedContentBox.Width',200,FlexDiv.UsedContentBox.Width);
  AssertEquals('FlexDiv.UsedContentBox.Height',20,FlexDiv.UsedContentBox.Height);

  AssertEquals('Div1.UsedContentBox.Left',0,Div1.UsedContentBox.Left);
  AssertEquals('Div1.UsedContentBox.Top',0,Div1.UsedContentBox.Top);
  AssertEquals('Div1.UsedContentBox.Width',90,Div1.UsedContentBox.Width);
  AssertEquals('Div1.UsedContentBox.Height',20,Div1.UsedContentBox.Height);

  AssertEquals('Div2.UsedContentBox.Left',90,Div2.UsedContentBox.Left);
  AssertEquals('Div2.UsedContentBox.Top',0,Div2.UsedContentBox.Top);
  AssertEquals('Div2.UsedContentBox.Width',110,Div2.UsedContentBox.Width);
  AssertEquals('Div2.UsedContentBox.Height',20,Div2.UsedContentBox.Height);
end;

procedure TTestFlexLayout.TestFlexLayout_Row_TwoItems_JustifyCenter;
var
  FlexDiv, Div1, Div2: TDiv;
  Body: TBody;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  FlexDiv:=TDiv.Create(Viewport);
  FlexDiv.Name:='FlexDiv';
  FlexDiv.Parent:=Body;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=FlexDiv;

  Div2:=TDiv.Create(Viewport);
  Div2.Name:='Div2';
  Div2.Parent:=FlexDiv;

  Viewport.Stylesheet.Text:=LinesToStr([
  'body { margin: 0; }',
  '#FlexDiv { display: flex; width: 200px; justify-content: center; }',
  '#Div1 { width: 30px; height: 20px; }',
  '#Div2 { width: 70px; height: 20px; }'
  ]);

  Viewport.Draw;
  AssertEquals('FlexDiv.Rendered',true,FlexDiv.Rendered);
  AssertEquals('FlexDiv.GetComputedString(fcaDisplay)','flex',FlexDiv.GetComputedString(fcaDisplay));
  AssertEquals('FlexDiv.GetComputedString(fcaJustifyContent)','center',FlexDiv.GetComputedString(fcaJustifyContent));

  AssertEquals('FlexDiv.UsedContentBox.Left',0,FlexDiv.UsedContentBox.Left);
  AssertEquals('FlexDiv.UsedContentBox.Top',0,FlexDiv.UsedContentBox.Top);
  AssertEquals('FlexDiv.UsedContentBox.Width',200,FlexDiv.UsedContentBox.Width);
  AssertEquals('FlexDiv.UsedContentBox.Height',20,FlexDiv.UsedContentBox.Height);

  AssertEquals('Div1.UsedContentBox.Left',50,Div1.UsedContentBox.Left);
  AssertEquals('Div1.UsedContentBox.Top',0,Div1.UsedContentBox.Top);
  AssertEquals('Div1.UsedContentBox.Width',30,Div1.UsedContentBox.Width);
  AssertEquals('Div1.UsedContentBox.Height',20,Div1.UsedContentBox.Height);

  AssertEquals('Div2.UsedContentBox.Left',80,Div2.UsedContentBox.Left);
  AssertEquals('Div2.UsedContentBox.Top',0,Div2.UsedContentBox.Top);
  AssertEquals('Div2.UsedContentBox.Width',70,Div2.UsedContentBox.Width);
  AssertEquals('Div2.UsedContentBox.Height',20,Div2.UsedContentBox.Height);
end;

Initialization
  RegisterTests([TTestFlexLayout]);
end.

