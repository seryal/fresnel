{
 *****************************************************************************
  This file is part of Fresnel.

  See the file COPYING.modifiedLGPL.txt, included in this distribution,
  for details about the license.
 *****************************************************************************
}
program TestFresnelBase;

{$mode objfpc}{$H+}

uses
  Classes, consoletestrunner, TCFresnelCSS, TCFresnelBaseEvents, TCFresnelImages, TCTextLayout,
  TCFlowLayout, TCFlexLayout;

type

  TMyTestRunner = class(TTestRunner)
  protected
  end;

var
  Application: TMyTestRunner;

begin
  DefaultFormat:=fPlain;
  DefaultRunAllTests:=True;
  Application := TMyTestRunner.Create(nil);
  Application.Initialize;
  Application.Title := 'Fresnel console tests';
  Application.Run;
  Application.Free;
end.

