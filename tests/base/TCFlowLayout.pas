unit TCFlowLayout;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, testregistry, TCFresnelCSS, Fresnel.Controls, Fresnel.DOM, Fresnel.Classes;

type

  { TTestFlowLayout }

  TTestFlowLayout = class(TCustomTestFresnelCSS)
  published
    procedure TestFlowLayout_BodyDiv;
    procedure TestFlowLayout_Slider_WithoutRangePoint;
    procedure TestFlowLayout_SliderRangePoint;

    procedure TestMarginPercentage;
    procedure TestPaddingPercentage;
    procedure TestPositionAbsolute_Right_WidthAuto;
    procedure TestPositionAbsolute_DivDefaultPosBehindStatic;
    procedure TestPositionAbsolute_DivDefaultPosBehindRelative;
    // todo procedure TestPositionAbsolute_DivTop100Percent;
    // todo: test break line
  end;


implementation

{ TTestFlowLayout }

procedure TTestFlowLayout.TestFlowLayout_BodyDiv;
var
  Body: TBody;
  Div1: TDiv;
  r: TFresnelRect;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=Body;

  Viewport.Stylesheet.Text:=LinesToStr([
  '#Div1 { width: 20px; height: 10px; }'
  ]);

  Viewport.Draw;

  // body
  AssertEquals('Body.Rendered',true,Body.Rendered);
  AssertEquals('Body.GetComputedString(fcaBoxSizing)','content-box',Body.GetComputedString(fcaBoxSizing));
  AssertEquals('Body.GetComputedString(fcaDisplay)','block',Body.GetComputedString(fcaDisplay));
  AssertEquals('Body.GetComputedString(fcaFloat)','none',Body.GetComputedString(fcaFloat));
  AssertEquals('Body.GetComputedString(fcaLineHeight)','normal',Body.GetComputedString(fcaLineHeight));
  AssertEquals('Body.GetComputedString(fcaPosition)','static',Body.GetComputedString(fcaPosition));
  AssertEquals('Body.GetComputedString(fcaZIndex)','auto',Body.GetComputedString(fcaZIndex));
  AssertEquals('Body.GetComputedString(fcaMarginLeft)','8px',Body.GetComputedString(fcaMarginLeft));
  AssertEquals('Body.GetComputedString(fcaMarginTop)','8px',Body.GetComputedString(fcaMarginTop));

  r:=Body.UsedBorderBox;
  AssertEquals('Body.UsedBorderBox.Left',8,r.Left);
  AssertEquals('Body.UsedBorderBox.Top',8,r.Top);
  AssertEquals('Body.UsedBorderBox.Right',792,r.Right);
  AssertEquals('Body.UsedBorderBox.Bottom',18,r.Bottom);

  // div1
  AssertEquals('Div1.Rendered',true,Div1.Rendered);
  AssertEquals('Div1.GetComputedString(fcaWidth)','20px',Div1.GetComputedString(fcaWidth));
  AssertEquals('Div1.GetComputedString(fcaHeight)','10px',Div1.GetComputedString(fcaHeight));

  AssertEquals('Div1.GetComputedString(fcaBoxSizing)','content-box',Div1.GetComputedString(fcaBoxSizing));
  AssertEquals('Div1.GetComputedString(fcaDisplay)','block',Div1.GetComputedString(fcaDisplay));
  AssertEquals('Div1.GetComputedString(fcaFloat)','none',Div1.GetComputedString(fcaFloat));
  AssertEquals('Div1.GetComputedString(fcaLineHeight)','normal',Div1.GetComputedString(fcaLineHeight));
  AssertEquals('Div1.GetComputedString(fcaPosition)','static',Div1.GetComputedString(fcaPosition));
  AssertEquals('Div1.GetComputedString(fcaZIndex)','auto',Div1.GetComputedString(fcaZIndex));

  r:=Div1.UsedBorderBox;
  AssertEquals('Div1.UsedBorderBox.Left',0,r.Left);
  AssertEquals('Div1.UsedBorderBox.Top',0,r.Top);
  AssertEquals('Div1.UsedBorderBox.Right',20,r.Right);
  AssertEquals('Div1.UsedBorderBox.Bottom',10,r.Bottom);
end;

procedure TTestFlowLayout.TestFlowLayout_Slider_WithoutRangePoint;
var
  Body: TBody;
  SliderDiv: TDiv;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  SliderDiv:=TDiv.Create(Viewport);
  SliderDiv.Name:='SliderDiv';
  SliderDiv.Parent:=Body;

  Viewport.Stylesheet.Text:=LinesToStr([
    '#SliderDiv {',
    '  margin: 7px 0 5px;',
    '  position: relative;',
    '  border: 1px solid #5080e0;',
    '  height: 9px;',
    '  width: 100%;',
    '}']);

  Viewport.Draw;
  AssertEquals('SliderDiv.Rendered',true,SliderDiv.Rendered);
  AssertEquals('SliderDiv.GetComputedString(fcaPosition)','relative',SliderDiv.GetComputedString(fcaPosition));
  AssertEquals('SliderDiv.RenderedBorderBox.Left',0,SliderDiv.RenderedBorderBox.Left);
  AssertEquals('SliderDiv.RenderedBorderBox.Top',7,SliderDiv.RenderedBorderBox.Top);
  AssertEquals('SliderDiv.RenderedBorderBox.Right',786,SliderDiv.RenderedBorderBox.Right);
  AssertEquals('SliderDiv.RenderedBorderBox.Bottom',18,SliderDiv.RenderedBorderBox.Bottom);
end;

procedure TTestFlowLayout.TestFlowLayout_SliderRangePoint;
var
  Body: TBody;
  SliderDiv, RangeDiv, PointDiv: TDiv;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  SliderDiv:=TDiv.Create(Viewport);
  SliderDiv.Name:='SliderDiv';
  SliderDiv.Parent:=Body;

  RangeDiv:=TDiv.Create(Viewport);
  RangeDiv.Name:='RangeDiv';
  RangeDiv.Parent:=SliderDiv;

  PointDiv:=TDiv.Create(Viewport);
  PointDiv.Name:='PointDiv';
  PointDiv.Parent:=SliderDiv;

  Viewport.Stylesheet.Text:=LinesToStr([
    '#SliderDiv {',
    '  margin: 7px 0 5px;',
    '  position: relative;',
    '  border: 1px solid #5080e0;',
    '  height: 9px;',
    '  width: 100%;',
    '}',
    '#RangeDiv {',
    '  display: block;',
    '  position: absolute;',
    '  z-index: 1;',
    '  font-size: .7em;',
    '  border: 0;',
    '  background-color: #5ca0cc;',
    '  top: 0;',
    '  height: 100%;',
    '  width: 50%;',
    '}',
    '#PointDiv {',
    '  position: absolute;',
    '  z-index: 2;',
    '  width: 15px;',
    '  height: 15px;',
    '  border: 1px solid #385590;',
    '  border-radius: 50%;',
    '  background-color: #fff;',
    '  top: -.3em;',
    '  margin-left: -.6em;',
    '  left: 50%;',
    '}']);

  Viewport.Draw;
  AssertEquals('SliderDiv.Rendered',true,SliderDiv.Rendered);

  // first check computed values
  AssertEquals('SliderDiv.GetComputedString(fcaPosition)','relative',SliderDiv.GetComputedString(fcaPosition));
  AssertEquals('SliderDiv.GetComputedString(fcaWidth)','100%',SliderDiv.GetComputedString(fcaWidth));
  AssertEquals('SliderDiv.GetComputedString(fcaHeight)','9px',SliderDiv.GetComputedString(fcaHeight));
  AssertEquals('SliderDiv.GetComputedString(fcaMarginTop)','7px',SliderDiv.GetComputedString(fcaMarginTop));
  AssertEquals('SliderDiv.GetComputedString(fcaMarginRight)','0',SliderDiv.GetComputedString(fcaMarginRight));
  AssertEquals('SliderDiv.GetComputedString(fcaMarginBottom)','5px',SliderDiv.GetComputedString(fcaMarginBottom));
  AssertEquals('SliderDiv.GetComputedString(fcaMarginLeft)','0',SliderDiv.GetComputedString(fcaMarginLeft));

  AssertEquals('RangeDiv.GetComputedString(fcaDisplay)','block',RangeDiv.GetComputedString(fcaDisplay));
  AssertEquals('RangeDiv.GetComputedString(fcaPosition)','absolute',RangeDiv.GetComputedString(fcaPosition));
  AssertEquals('RangeDiv.GetComputedString(fcaZIndex)','1',RangeDiv.GetComputedString(fcaZIndex));
  AssertEquals('RangeDiv.Font.GetSize',7,RangeDiv.Font.GetSize);
  AssertEquals('RangeDiv.GetComputedString(fcaHeight)','100%',RangeDiv.GetComputedString(fcaHeight));
  AssertEquals('RangeDiv.GetComputedString(fcaWidth)','50%',RangeDiv.GetComputedString(fcaWidth));

  AssertEquals('PointDiv.GetComputedString(fcaPosition)','absolute',PointDiv.GetComputedString(fcaPosition));
  AssertEquals('PointDiv.GetComputedString(fcaZIndex)','2',PointDiv.GetComputedString(fcaZIndex));
  AssertEquals('PointDiv.GetComputedString(fcaWidth)','15px',PointDiv.GetComputedString(fcaWidth));
  AssertEquals('PointDiv.GetComputedString(fcaHeight)','15px',PointDiv.GetComputedString(fcaHeight));
  AssertEquals('PointDiv.GetComputedString(fcaTop)','-0.3em',PointDiv.GetComputedString(fcaTop));
  AssertEquals('PointDiv.GetComputedString(fcaLeft)','50%',PointDiv.GetComputedString(fcaLeft));
  AssertEquals('PointDiv.GetComputedString(fcaMarginLeft)','-0.6em',PointDiv.GetComputedString(fcaMarginLeft));

  // then check layout values
  //writeln('TTestFlowLayout.TestFlowLayout_SliderRangePoint Body.RenderedBorderBox=',Body.RenderedBorderBox.ToString);
  AssertEquals('Body.RenderedBorderBox.Left',8,Body.RenderedBorderBox.Left);
  AssertEquals('Body.RenderedBorderBox.Top',8,Body.RenderedBorderBox.Top);
  AssertEquals('Body.RenderedContentBox.Width',784,Body.RenderedContentBox.Width);
  AssertEquals('Body.RenderedBorderBox.Right',792,Body.RenderedBorderBox.Right); // 800-8
  AssertEquals('Body.RenderedBorderBox.Bottom',31,Body.RenderedBorderBox.Bottom); // 8(body margin)+Slider:7(margin)+1(border)+9(height)+1(border)+5(margin)

  //writeln('TTestFlowLayout.TestFlowLayout_SliderRangePoint SliderDiv.RenderedBorderBox=',SliderDiv.RenderedBorderBox.ToString);
  AssertEquals('SliderDiv.RenderedBorderBox.Left',0,SliderDiv.RenderedBorderBox.Left);
  AssertEquals('SliderDiv.RenderedBorderBox.Top',7,SliderDiv.RenderedBorderBox.Top);
  AssertEquals('SliderDiv.RenderedBorderBox.Right',786,SliderDiv.RenderedBorderBox.Right); // 784(width:100%) + 2*1(border) + 0(margin)
  AssertEquals('SliderDiv.RenderedBorderBox.Bottom',18,SliderDiv.RenderedBorderBox.Bottom);
  AssertEquals('SliderDiv.RenderedContentBox.Width',784,SliderDiv.RenderedContentBox.Width);
  AssertEquals('SliderDiv.RenderedContentBox.Left',1,SliderDiv.RenderedContentBox.Left);
  AssertEquals('SliderDiv.RenderedContentBox.Top',8,SliderDiv.RenderedContentBox.Top);
  AssertEquals('SliderDiv.RenderedContentBox.Right',785,SliderDiv.RenderedContentBox.Right);
  AssertEquals('SliderDiv.RenderedContentBox.Bottom',17,SliderDiv.RenderedContentBox.Bottom);

  //writeln('TTestFlowLayout.TestFlowLayout_SliderRangePoint RangeDiv.RenderedBorderBox=',RangeDiv.RenderedBorderBox.ToString);
  AssertEquals('RangeDiv.Rendered',true,RangeDiv.Rendered);
  AssertEquals('RangeDiv.RenderedBorderBox.Left',0,RangeDiv.RenderedBorderBox.Left);
  AssertEquals('RangeDiv.RenderedBorderBox.Top',0,RangeDiv.RenderedBorderBox.Top);
  AssertEquals('RangeDiv.RenderedBorderBox.Right',392,RangeDiv.RenderedBorderBox.Right);
  AssertEquals('RangeDiv.RenderedBorderBox.Bottom',9,RangeDiv.RenderedBorderBox.Bottom);
  AssertEquals('RangeDiv.RenderedContentBox.Left',0,RangeDiv.RenderedContentBox.Left);
  AssertEquals('RangeDiv.RenderedContentBox.Top',0,RangeDiv.RenderedContentBox.Top);
  AssertEquals('RangeDiv.RenderedContentBox.Right',392,RangeDiv.RenderedContentBox.Right);
  AssertEquals('RangeDiv.RenderedContentBox.Bottom',9,RangeDiv.RenderedContentBox.Bottom);

  AssertEquals('PointDiv.Rendered',true,PointDiv.Rendered);
  AssertEquals('PointDiv.LayoutNode.MarginLeft',-6,PointDiv.LayoutNode.MarginLeft);
  AssertEquals('PointDiv.LayoutNode.Width',15,PointDiv.LayoutNode.Width);
  AssertEquals('PointDiv.LayoutNode.Height',15,PointDiv.LayoutNode.Height);
  AssertEquals('PointDiv.LayoutNode.Top',-3,PointDiv.LayoutNode.Top);
  AssertEquals('PointDiv.LayoutNode.Left',392,PointDiv.LayoutNode.Left);
  AssertEquals('PointDiv.RenderedBorderBox.Left',386,PointDiv.RenderedBorderBox.Left);
  AssertEquals('PointDiv.RenderedBorderBox.Top',-3,PointDiv.RenderedBorderBox.Top);
  AssertEquals('PointDiv.RenderedBorderBox.Right',403,PointDiv.RenderedBorderBox.Right);
  AssertEquals('PointDiv.RenderedBorderBox.Bottom',14,PointDiv.RenderedBorderBox.Bottom);
  AssertEquals('PointDiv.RenderedContentBox.Left',387,PointDiv.RenderedContentBox.Left);
  AssertEquals('PointDiv.RenderedContentBox.Top',-2,PointDiv.RenderedContentBox.Top);
  AssertEquals('PointDiv.RenderedContentBox.Right',402,PointDiv.RenderedContentBox.Right);
  AssertEquals('PointDiv.RenderedContentBox.Bottom',13,PointDiv.RenderedContentBox.Bottom);
end;

procedure TTestFlowLayout.TestMarginPercentage;
var
  Body: TBody;
  Div1: TDiv;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=Body;

  Viewport.Stylesheet.Text:=LinesToStr([
    'body {',
    '  margin: 0;',
    '}',
    'div {',
    '  margin: 5% 10% 15% 20%;',
    '  height: 10px;',
    '}']);

  Viewport.Draw;
  AssertEquals('Body.RenderedContentBox.Width',800,Body.RenderedContentBox.Width);

  AssertEquals('Div1.Rendered',true,Div1.Rendered);

  //writeln('TTestFlowLayout.TestPaddingPercentage ',Div1.RenderedBorderBox.ToString);
  AssertEquals('Div1.LayoutNode.MarginLeft',160,Div1.LayoutNode.MarginLeft);
  AssertEquals('Div1.LayoutNode.MarginTop',40,Div1.LayoutNode.MarginTop);
  AssertEquals('Div1.LayoutNode.MarginRight',80,Div1.LayoutNode.MarginRight);
  AssertEquals('Div1.LayoutNode.MarginBottom',120,Div1.LayoutNode.MarginBottom);
  AssertEquals('Div1.RenderedBorderBox.Left',160,Div1.RenderedBorderBox.Left);
  AssertEquals('Div1.RenderedBorderBox.Top',40,Div1.RenderedBorderBox.Top);
  AssertEquals('Div1.RenderedBorderBox.Right',720,Div1.RenderedBorderBox.Right);
  AssertEquals('Div1.RenderedBorderBox.Bottom',50,Div1.RenderedBorderBox.Bottom);
end;

procedure TTestFlowLayout.TestPaddingPercentage;
var
  Body: TBody;
  Div1: TDiv;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=Body;

  Viewport.Stylesheet.Text:=LinesToStr([
    'body {',
    '  margin: 0;',
    '}',
    'div {',
    '  padding: 5% 10% 15% 20%;',
    '  height: 10px;',
    '}']);

  Viewport.Draw;
  AssertEquals('Body.RenderedContentBox.Width',800,Body.RenderedContentBox.Width);

  AssertEquals('Div1.Rendered',true,Div1.Rendered);

  //writeln('TTestFlowLayout.TestPaddingPercentage ',Div1.RenderedBorderBox.ToString);
  AssertEquals('Div1.LayoutNode.PaddingLeft',160,Div1.LayoutNode.PaddingLeft);
  AssertEquals('Div1.LayoutNode.PaddingTop',40,Div1.LayoutNode.PaddingTop);
  AssertEquals('Div1.LayoutNode.PaddingRight',80,Div1.LayoutNode.PaddingRight);
  AssertEquals('Div1.LayoutNode.PaddingBottom',120,Div1.LayoutNode.PaddingBottom);
  AssertEquals('Div1.RenderedContentBox.Left',160,Div1.RenderedContentBox.Left);
  AssertEquals('Div1.RenderedContentBox.Top',40,Div1.RenderedContentBox.Top);
  AssertEquals('Div1.RenderedContentBox.Right',720,Div1.RenderedContentBox.Right);
  AssertEquals('Div1.RenderedContentBox.Bottom',50,Div1.RenderedContentBox.Bottom);
end;

procedure TTestFlowLayout.TestPositionAbsolute_Right_WidthAuto;
var
  Body: TBody;
  Div1, Div2, Div3: TDiv;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=Body;

  Div2:=TDiv.Create(Viewport);
  Div2.Name:='Div2';
  Div2.Parent:=Div1;

  Div3:=TDiv.Create(Viewport);
  Div3.Name:='Div3';
  Div3.Parent:=Div2;

  Viewport.Stylesheet.Text:=LinesToStr([
    'body {',
    '  margin: 0;', // content width = 800
    '}',
    '#Div1 {',
    '  position: absolute;',
    '  height: 120px;', // content height = 120
    '  right: 30px;',
    // content-width is on top-down run 0, and on second run max-content, which is width of Div3, so 100px
    '}',
    '#Div2 {',
    '  margin: 10%;', // 10% of 0 on top-down run!
    '  padding: 10%;', // 10% of 0 on top-down run!
    '  box-sizing: border-box;',
    '  width: 80%;', // 30% of 360 = 108, content width = 108 - 2*padding = 88
    '  height: 60%;', // same as width 30%, content height = 88
    '}',
    '#Div3 {',
    '  width: 100px;',
    '  height: 30px;',
    '}']);

  Viewport.Draw;
  AssertEquals('Body.RenderedContentBox.Width',800,Body.RenderedContentBox.Width);

  AssertEquals('Div3.Rendered',true,Div3.Rendered);

  //writeln('TTestFlowLayout.TestPositionAbsolute_Right_WidthAuto Div3.RenderedBorderBox=',Div3.RenderedBorderBox.ToString);
  AssertEquals('Div3.LayoutNode.Width',100,Div3.LayoutNode.Width);
  AssertEquals('Div3.LayoutNode.Height',30,Div3.LayoutNode.Height);

  //writeln('TTestFlowLayout.TestPositionAbsolute_Right_WidthAuto Div1.RenderedBorderBox=',Div1.RenderedBorderBox.ToString);
  AssertEquals('Div1.GetComputedString(fcaPosition)','absolute',Div1.GetComputedString(fcaPosition));
  AssertEquals('Div1.GetComputedString(fcaBoxSizing)','content-box',Div1.GetComputedString(fcaBoxSizing));
  AssertEquals('Div1.GetComputedString(fcaWidth)','auto',Div1.GetComputedString(fcaWidth));
  AssertEquals('Div1.LayoutNode.Height',120,Div1.LayoutNode.Height);
  AssertEquals('Div1.LayoutNode.Width',100,Div1.LayoutNode.Width);
  AssertEquals('Div1.LayoutNode.Right',30,Div1.LayoutNode.Right);
  AssertEquals('Div1.LayoutNode.Left',670,Div1.LayoutNode.Left); // 800-30-100 = 670

  //writeln('TTestFlowLayout.TestPositionAbsolute_Right_WidthAuto Div2.RenderedBorderBox=',Div2.RenderedBorderBox.ToString);
  AssertEquals('Div2.GetComputedString(fcaPosition)','static',Div2.GetComputedString(fcaPosition));
  AssertEquals('Div2.GetComputedString(fcaBoxSizing)','border-box',Div2.GetComputedString(fcaBoxSizing));
  AssertEquals('Div2.GetComputedString(fcaWidth)','80%',Div2.GetComputedString(fcaWidth));
  AssertEquals('Div2.GetComputedString(fcaHeight)','60%',Div2.GetComputedString(fcaHeight));
  AssertEquals('Div2.GetComputedString(fcaMarginLeft)','10%',Div2.GetComputedString(fcaMarginLeft));
  AssertEquals('Div2.GetComputedString(fcaMarginTop)','10%',Div2.GetComputedString(fcaMarginTop));
  AssertEquals('Div2.GetComputedString(fcaMarginRight)','10%',Div2.GetComputedString(fcaMarginRight));
  AssertEquals('Div2.GetComputedString(fcaMarginBottom)','10%',Div2.GetComputedString(fcaMarginBottom));
  AssertEquals('Div2.GetComputedString(fcaPaddingLeft)','10%',Div2.GetComputedString(fcaPaddingLeft));
  AssertEquals('Div2.GetComputedString(fcaPaddingTop)','10%',Div2.GetComputedString(fcaPaddingTop));
  AssertEquals('Div2.GetComputedString(fcaPaddingRight)','10%',Div2.GetComputedString(fcaPaddingRight));
  AssertEquals('Div2.GetComputedString(fcaPaddingBottom)','10%',Div2.GetComputedString(fcaPaddingBottom));
  AssertEquals('Div2.LayoutNode.MarginLeft',10,Div2.LayoutNode.MarginLeft); // 10% of Div2.width 100
  AssertEquals('Div2.LayoutNode.MarginRight',10,Div2.LayoutNode.MarginRight);
  AssertEquals('Div2.LayoutNode.MarginTop',10,Div2.LayoutNode.MarginTop); // 10% of Div2.width 100
  AssertEquals('Div2.LayoutNode.MarginBottom',10,Div2.LayoutNode.MarginBottom);
  AssertEquals('Div2.LayoutNode.PaddingLeft',10,Div2.LayoutNode.PaddingLeft); // 10% of Div2.width 100
  AssertEquals('Div2.LayoutNode.PaddingRight',10,Div2.LayoutNode.PaddingRight);
  AssertEquals('Div2.LayoutNode.PaddingTop',10,Div2.LayoutNode.PaddingTop); // 10% of Div2.width 100
  AssertEquals('Div2.LayoutNode.PaddingBottom',10,Div2.LayoutNode.PaddingBottom);
  AssertEquals('Div2.LayoutNode.Width',60,Div2.LayoutNode.Width);
  AssertEquals('Div2.LayoutNode.Height',52,Div2.LayoutNode.Height);
end;

procedure TTestFlowLayout.TestPositionAbsolute_DivDefaultPosBehindStatic;
var
  Body: TBody;
  Div1: TDiv;
  Label1, Label2: TLabel;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  Label1:=TLabel.Create(Viewport);
  Label1.Name:='Label1';
  Label1.Caption:='Label1';
  Label1.Parent:=Body;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=Body;

  Label2:=TLabel.Create(Viewport);
  Label2.Name:='Label2';
  Label2.Caption:='Label2';
  Label2.Parent:=Body;

  Viewport.Stylesheet.Text:=LinesToStr([
    'body {',
    '  margin: 0;', // content width = 800
    '  font-size: 20px;',
    '}',
    '#Div1 {',
    '  position: absolute;',
    '  width: 30px; height: 20px;',
    '}']);

  // Div1 is absolute to the Viewport, because Body position is static
  // Div1 default position is below Label1, because Div1 display is block
  // Label2 is right behind Label1
  // Body width/height includes Label1 and Label2, but not Div1

  Viewport.Draw;
  AssertEquals('Body.RenderedContentBox.Width',800,Body.RenderedContentBox.Width);

  AssertEquals('Div1.Rendered',true,Div1.Rendered);

  //writeln('TTestFlowLayout.TestPositionAbsolute_DivDefaultPos Label1.RenderedBorderBox=',Label1.RenderedBorderBox.ToString);
  AssertEquals('Label1.GetComputedString(fcaPosition)','static',Label1.GetComputedString(fcaPosition));
  AssertEquals('Label1.GetComputedString(fcaDisplay)','inline flow',Label1.GetComputedString(fcaDisplay));
  AssertEquals('Label1.GetComputedString(fcaWidth)','auto',Label1.GetComputedString(fcaWidth));
  AssertEquals('Label1.RenderedBorderBox.Top',0,Label1.RenderedBorderBox.Left);
  AssertEquals('Label1.RenderedBorderBox.Left',0,Label1.RenderedBorderBox.Left);
  AssertEquals('Label1.RenderedBorderBox.Height',23,Label1.RenderedBorderBox.Height);

  //writeln('TTestFlowLayout.TestPositionAbsolute_DivDefaultPos Label2.RenderedBorderBox=',Label2.RenderedBorderBox.ToString);
  AssertEquals('Label2.GetComputedString(fcaPosition)','static',Label2.GetComputedString(fcaPosition));
  AssertEquals('Label2.GetComputedString(fcaDisplay)','inline flow',Label2.GetComputedString(fcaDisplay));
  AssertEquals('Label2.GetComputedString(fcaWidth)','auto',Label2.GetComputedString(fcaWidth));
  AssertEquals('Label2.RenderedBorderBox.Height',23,Label2.RenderedBorderBox.Height);
  AssertEquals('Label2.RenderedBorderBox.Top',0,Label2.RenderedBorderBox.Top);
  AssertEquals('Label2.RenderedBorderBox.Left',Label1.RenderedBorderBox.Right,Label2.RenderedBorderBox.Left);

  //writeln('TTestFlowLayout.TestPositionAbsolute_DivDefaultPos Div1.RenderedBorderBox=',Div1.RenderedBorderBox.ToString);
  AssertEquals('Div1.RenderedBorderBox.Width',30,Div1.RenderedBorderBox.Width);
  AssertEquals('Div1.RenderedBorderBox.Height',20,Div1.RenderedBorderBox.Height);
  AssertEquals('Div1.RenderedBorderBox.Left',0,Div1.RenderedBorderBox.Left);
  AssertEquals('Div1.RenderedBorderBox.Top',23,Div1.RenderedBorderBox.Top);
end;

procedure TTestFlowLayout.TestPositionAbsolute_DivDefaultPosBehindRelative;
var
  Body: TBody;
  Div1: TDiv;
  Label1: TLabel;
begin
  Body:=TBody.Create(Viewport);
  Body.Name:='Body';
  Body.Parent:=Viewport;

  Label1:=TLabel.Create(Viewport);
  Label1.Name:='Label1';
  Label1.Caption:='Label1';
  Label1.Parent:=Body;

  Div1:=TDiv.Create(Viewport);
  Div1.Name:='Div1';
  Div1.Parent:=Body;

  Viewport.Stylesheet.Text:=LinesToStr([
    'body {',
    '  margin: 0;', // content width = 800
    '  font-size: 20px;',
    '}',
    '#Label1 {',
    '  position: relative; top:10px;',
    '}',
    '#Div1 {',
    '  position: absolute;',
    '  width: 30px; height: 20px;',
    '}']);

  // Div1 is absolute to the Viewport, because Body position is static
  // Div1 default position is below Label1 static position, because Div1 display is block
  // Body width/height includes Label1 static bounds, and not Div1

  Viewport.Draw;
  AssertEquals('Body.RenderedContentBox.Width',800,Body.RenderedContentBox.Width);

  AssertEquals('Div1.Rendered',true,Div1.Rendered);

  //writeln('TTestFlowLayout.TestPositionAbsolute_DivDefaultPos Label1.RenderedBorderBox=',Label1.RenderedBorderBox.ToString);
  AssertEquals('Label1.GetComputedString(fcaPosition)','relative',Label1.GetComputedString(fcaPosition));
  AssertEquals('Label1.GetComputedString(fcaDisplay)','inline flow',Label1.GetComputedString(fcaDisplay));
  AssertEquals('Label1.GetComputedString(fcaWidth)','auto',Label1.GetComputedString(fcaWidth));
  AssertEquals('Label1.GetComputedString(fcaTop)','10px',Label1.GetComputedString(fcaTop));
  AssertEquals('Label1.RenderedBorderBox.Top',10,Label1.RenderedBorderBox.Top);
  AssertEquals('Label1.RenderedBorderBox.Left',0,Label1.RenderedBorderBox.Left);
  AssertEquals('Label1.RenderedBorderBox.Height',23,Label1.RenderedBorderBox.Height);

  AssertEquals('Body.RenderedBorderBox.Height',23,Body.RenderedBorderBox.Height);

  //writeln('TTestFlowLayout.TestPositionAbsolute_DivDefaultPos Div1.RenderedBorderBox=',Div1.RenderedBorderBox.ToString);
  AssertEquals('Div1.RenderedBorderBox.Width',30,Div1.RenderedBorderBox.Width);
  AssertEquals('Div1.RenderedBorderBox.Height',20,Div1.RenderedBorderBox.Height);
  AssertEquals('Div1.RenderedBorderBox.Left',0,Div1.RenderedBorderBox.Left);
  AssertEquals('Div1.RenderedBorderBox.Top',23,Div1.RenderedBorderBox.Top);
end;

Initialization
  RegisterTests([TTestFlowLayout]);
end.

