{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Select a widgetset

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel;

{$IF FPC_FULLVERSION < 30301}
  { $ERROR Fresnel requires at least FPC 3.3.1}
{$ENDIF}

interface

uses
  Classes, Fresnel.Forms, Fresnel.App,
  {$IFDEF FresnelCocoa}
  Fresnel.Cocoa
  {$ENDIF}
  {$IFDEF FresnelGtk3}
  Fresnel.Gtk3
  {$ENDIF}
  {$IFDEF FresnelWin32}
  Fresnel.Win32
  {$ENDIF}
  ;

implementation

initialization
  FresnelApplication:=TFresnelApplication.Create(nil);
finalization
  FresnelApplication.Free; // will nil itself

end.
