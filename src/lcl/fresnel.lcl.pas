{
  The Fresnel-LCL widgetset and renderer.
}
unit Fresnel.LCL;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Types, Math, FPImage, lclType, fresnel.keys,
  AvgLvlTree, LazLoggerBase, GraphType,
  Graphics, Controls, LCLIntf, Forms, LResources, IntfGraphics,
  Fresnel.Classes, Fresnel.Events, Fresnel.DOM,
  Fresnel.Renderer, Fresnel.Images, Fresnel.WidgetSet,
  {$IFDEF FresnelSkia}
  System.Skia, Fresnel.SkiaRenderer,
  {$ENDIF}
  Fresnel.Forms;

type
  TFresnelLCLFontEngine = class;

  {$IFDEF FresnelSkia}
  {$ELSE}
  { TFresnelLCLImageData }

  TFresnelLCLImageData = class(TImageData)
  protected
    class function CreateData(aWidth, aHeight: Word): TFPCustomImage; override;
  end;
  {$ENDIF}

  { TFresnelLCLFont }

  TFresnelLCLFont = class(TInterfacedObject,IFresnelFont)
  public
    Engine: TFresnelLCLFontEngine;
    Family: string;
    Kerning: TFresnelCSSKerning;
    Size: TFresnelLength;
    Style: string;
    Weight: TFresnelLength;
    Width: TFresnelLength;
    LCLFont: TFont;
    destructor Destroy; override;
    function GetAlternates: string;
    function GetCaps: TFresnelCSSFontVarCaps;
    function GetEastAsians: TFresnelCSSFontVarEastAsians;
    function GetEmoji: TFresnelCSSFontVarEmoji;
    function GetFamily: string;
    function GetKerning: TFresnelCSSKerning;
    function GetLigatures: TFresnelCSSFontVarLigaturesSet;
    function GetNumerics: TFresnelCSSFontVarNumerics;
    function GetPosition: TFresnelCSSFontVarPosition;
    function GetSize: TFresnelLength;
    function GetStyle: string;
    function GetWeight: TFresnelLength;
    function GetWidth: TFresnelLength;
    function TextSize(const aText: string): TFresnelPoint; virtual;
    function TextSizeMaxWidth(const aText: string; MaxWidth: TFresnelLength
      ): TFresnelPoint; virtual;
    function GetTool: TObject;
    function GetDescription: String;
  end;

  { TFresnelLCLFontEngine }

  TFresnelLCLFontEngine = class(TFresnelFontEngine)
  private
    FCanvas: TCanvas;
    FFonts: TAvgLvlTree; // tree of TFresnelLCLFont sorted with CompareFresnelLCLFont
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function FindFont(const Desc: TFresnelFontDesc): TFresnelLCLFont; virtual;
    function Allocate(const Desc: TFresnelFontDesc): IFresnelFont; override;
    function TextSize(aFont: TFresnelLCLFont; const aText: string): TPoint; virtual;
    function TextSizeMaxWidth(aFont: TFresnelLCLFont; const aText: string; MaxWidth: integer): TPoint; virtual;
    function NeedLCLFont(aFont: TFresnelLCLFont): TFont; virtual;
    property Canvas: TCanvas read FCanvas write FCanvas;
  end;

  { TFresnelLCLRenderer }

  TFresnelLCLRenderer = class(TFresnelRenderer)
  private
    FCanvas: TCanvas;
  public
    function RadToLCLAngle16(Angle: TFresnelLength): integer;
    procedure Arc(const aColor: TFPColor; const aCenter, aRadii: TFresnelPoint;
      aStartAngle: TFresnelLength = 0; aStopAngle: TFresnelLength = DoublePi); override;
    procedure FillRect(const aColor: TFPColor; const aRect: TFresnelRect); override;
    procedure Line(const aColor: TFPColor; const x1, y1, x2, y2: TFresnelLength); override;
    procedure RoundRect(const aColor: TFPColor; const aRect: TFresnelRoundRect;
      Fill: Boolean); override;
    procedure TextOut(const aLeft, aTop: TFresnelLength;
      const aFont: IFresnelFont; const aColor: TFPColor;
      const aText: string); override;
    procedure DrawImage(const aLeft, aTop, aWidth, aHeight: TFresnelLength;
      const aImage: TFPCustomImage); override;

    property Canvas: TCanvas read FCanvas write FCanvas;
  end;

  TLCLWSForm = class;

  { TFresnelLCLForm }

  TFresnelLCLForm = class(TForm)
  private
    FFresnelForm: TFresnelCustomForm;
  public
    property FresnelForm: TFresnelCustomForm read FFresnelForm;
  end;

  { TLCLWSForm }

  TLCLWSForm = class(TFresnelWSForm)
  private
    FFresnelForm: TFresnelCustomForm;
    FLCLForm: TFresnelLCLForm;
    {$IFDEF FresnelSkia}
    FIntfImg: TLazIntfImage;
    {$ENDIF}
    procedure LCLChangeBounds(Sender: TObject);
    procedure LCLKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure LCLMouseDown(Sender: TObject; Button: Controls.TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LCLMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure LCLMouseUp(Sender: TObject; Button: Controls.TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure LCLPaint(Sender: TObject);
    procedure LCLKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure LCLUTF8Key(Sender: TObject; var UTF8Key: TUTF8Char);
    procedure SetFresnelForm(const AValue: TFresnelCustomForm);
  protected
    function GetCaption: TFresnelCaption; override;
    function GetFormBounds: TFresnelRect; override;
    function GetVisible: boolean; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure SetCaption(AValue: TFresnelCaption); override;
    procedure SetFormBounds(const AValue: TFresnelRect); override;
    procedure SetVisible(const AValue: boolean); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    Class procedure InitMouseXYEvent(out EvtInit: TFresnelMouseEventInit;
      Shift: TShiftState; X, Y: Integer;
      Button: Controls.TMouseButton = Controls.mbLeft);

    function GetClientSize: TFresnelPoint; override;
    procedure Invalidate; override;
    procedure InvalidateRect(const aRect: TFresnelRect); override;
    function CreateLCLForm: TForm; virtual;
    property LCLForm: TFresnelLCLForm read FLCLForm;

    property FresnelForm: TFresnelCustomForm read FFresnelForm write SetFresnelForm;
  end;

  { TFresnelLCLWidgetSet }

  TFresnelLCLWidgetSet = class(TFresnelWidgetSet)
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure AppProcessMessages; override; // called by user
    procedure AppTerminate; override; // called by user
    procedure AppWaitMessage; override; // called by user
    procedure CreateWSForm(aFresnelForm: TFresnelComponent); override;
  end;

var
  FresnelLCLWidgetSet: TFresnelLCLWidgetSet;

function CompareFresnelLCLFont(Item1, Item2: Pointer): integer;
function CompareFresnelFontDescWithLCLFont(Key, Item: Pointer): integer;
procedure FresnelRectToRect(const Src: TFresnelRect; out Dest: TRect);
procedure FresnelPointsToPoints(const Src: TFresnelPointArray; out Dest: TPointArray);

implementation

uses utf8utils;

function CompareFresnelLCLFont(Item1, Item2: Pointer): integer;
var
  Font1: TFresnelLCLFont absolute Item1;
  Font2: TFresnelLCLFont absolute Item2;
begin
  Result:=CompareText(Font1.Family,Font2.Family);
  if Result<>0 then exit;
  Result:=CompareValue(Font1.Size,Font2.Size);
  if Result<>0 then exit;
  Result:=CompareText(Font1.Style,Font2.Style);
  if Result<>0 then exit;
  Result:=CompareValue(Font1.Weight,Font2.Weight);
  if Result<>0 then exit;
  Result:=CompareValue(Font1.Width,Font2.Width);
  if Result<>0 then exit;
  Result:=CompareValue(ord(Font1.Kerning),ord(Font2.Kerning));
end;

function CompareFresnelFontDescWithLCLFont(Key, Item: Pointer): integer;
var
  Desc: PFresnelFontDesc absolute Key;
  aFont: TFresnelLCLFont absolute Item;
begin
  Result:=CompareText(Desc^.Family,aFont.Family);
  if Result<>0 then exit;
  Result:=CompareValue(Desc^.Size,aFont.Size);
  if Result<>0 then exit;
  Result:=CompareText(Desc^.Style,aFont.Style);
  if Result<>0 then exit;
  Result:=CompareValue(Desc^.Weight,aFont.Weight);
  if Result<>0 then exit;
  Result:=CompareValue(Desc^.Width,aFont.Width);
  if Result<>0 then exit;
  Result:=CompareValue(ord(Desc^.Kerning),ord(aFont.Kerning));
end;

procedure FresnelRectToRect(const Src: TFresnelRect; out Dest: TRect);
begin
  Dest.Left:=floor(Src.Left);
  Dest.Top:=floor(Src.Top);
  Dest.Right:=ceil(Src.Right);
  Dest.Bottom:=ceil(Src.Bottom);
end;

procedure FresnelPointsToPoints(const Src: TFresnelPointArray; out
  Dest: TPointArray);
var
  l, i: Integer;
begin
  l:=length(Src);
  SetLength(Dest{%H-},l);
  for i:=0 to l-1 do
    Dest[i]:=Src[i].GetPoint;
end;

{ TFresnelLCLRenderer }

function TFresnelLCLRenderer.RadToLCLAngle16(Angle: TFresnelLength): integer;
// Fresnel is in Rad (0..2*pi) starting right going clockwise
// LCL is in 16th degree (0..5760=16*360) starting right going counter-clockwise
const
  aFactor = TFresnelLength(-16*180)/TFresnelLength(pi);
begin
  Result:=round(Angle*aFactor);
end;

procedure TFresnelLCLRenderer.Arc(const aColor: TFPColor; const aCenter,
  aRadii: TFresnelPoint; aStartAngle: TFresnelLength; aStopAngle: TFresnelLength
  );
var
  aLeft, aTop, aRight, aBottom, AngleStart, AngleSweep: integer;
begin
  Canvas.Pen.FPColor:=aColor;
  Canvas.Pen.Style:=psSolid;
  aLeft:=round(aCenter.X-aRadii.X);
  aTop:=round(aCenter.Y-aRadii.Y);
  aRight:=round(aCenter.X+aRadii.X);
  aBottom:=round(aCenter.Y+aRadii.Y);
  AngleStart:=RadToLCLAngle16(aStartAngle);
  AngleSweep:=RadToLCLAngle16(aStopAngle-aStartAngle);

  Canvas.Arc(aLeft,aTop,aRight,aBottom,AngleStart,AngleSweep);
end;

procedure TFresnelLCLRenderer.FillRect(const aColor: TFPColor;
  const aRect: TFresnelRect);
begin
  Canvas.Brush.FPColor:=aColor;
  Canvas.Brush.Style:=bsSolid;
  Canvas.FillRect(Rect(floor(Origin.X+aRect.Left),floor(Origin.Y+aRect.Top),
                       ceil(Origin.X+aRect.Right),ceil(Origin.Y+aRect.Bottom)));
end;

procedure TFresnelLCLRenderer.Line(const aColor: TFPColor; const x1, y1, x2,
  y2: TFresnelLength);
begin
  Canvas.Pen.FPColor:=aColor;
  Canvas.Pen.Style:=psSolid;
  Canvas.Line(round(Origin.X+x1),round(Origin.Y+y1),round(Origin.X+x2),round(Origin.Y+y2));
end;

procedure TFresnelLCLRenderer.RoundRect(const aColor: TFPColor;
  const aRect: TFresnelRoundRect; Fill: Boolean);
var
  R: TRect;
  c: TFresnelCSSCorner;
  Radii: array[TFresnelCSSCorner] of TPoint;
  SameRadii: Boolean;
  W, H, MaxRadiusX, MaxRadiusY: Integer;
  p: TFresnelPointArray;
  Points: TPointArray;
begin
  FresnelRectToRect(aRect.Box,R);
  W:=R.Width;
  H:=R.Height;
  if (W<=0) or (H<=0) then exit;

  MaxRadiusX:=W div 2;
  MaxRadiusY:=H div 2;

  for c in TFresnelCSSCorner do
  begin
    Radii[c].X:=Min(Max(0,round(aRect.Radii[c].X)),MaxRadiusX);
    Radii[c].Y:=Min(Max(0,round(aRect.Radii[c].Y)),MaxRadiusY);
    if Radii[c].X=0 then
      Radii[c].Y:=0
    else if Radii[c].Y=0 then
      Radii[c].X:=0;
  end;

  SameRadii:=true;
  for c:=fcsTopRight to fcsBottomRight do
    if (Radii[c].X<>Radii[fcsTopLeft].X)
        or (Radii[c].Y<>Radii[fcsTopLeft].Y) then
      SameRadii:=false;

  if Fill then
  begin
    Canvas.Brush.FPColor:=aColor;
    Canvas.Brush.Style:=bsSolid;
    Canvas.Pen.Style:=psClear;
  end else begin
    Canvas.Pen.FPColor:=aColor;
    Canvas.Pen.Style:=psSolid;
    Canvas.Brush.Style:=bsClear;
  end;

  if SameRadii then
    Canvas.RoundRect(R,Radii[fcsTopLeft].X,Radii[fcsTopLeft].Y)
  else begin
    p:=RoundRect2Polygon(aRect);
    if length(p)=0 then exit;
    FresnelPointsToPoints(p,Points);

    if Fill then
      Canvas.Polyline(Points)
    else
      Canvas.Polygon(Points);
  end;
end;

procedure TFresnelLCLRenderer.TextOut(const aLeft, aTop: TFresnelLength;
  const aFont: IFresnelFont; const aColor: TFPColor; const aText: string
  );
var
  ts: TTextStyle;
  aFresnelFont: TFresnelLCLFont;
begin
  Canvas.Brush.Style:=bsClear;
  aFresnelFont:=aFont.GetTool as TFresnelLCLFont;
  Canvas.Font:=aFresnelFont.LCLFont;
  Canvas.Font.Color:=FPColorToTColor(aColor);
  ts:=Canvas.TextStyle;
  ts.Opaque:=false;
  Canvas.TextStyle:=ts;
  Canvas.TextOut(round(Origin.X+aLeft),round(Origin.Y+aTop),aText);
end;

procedure TFresnelLCLRenderer.DrawImage(const aLeft, aTop, aWidth, aHeight: TFresnelLength;
  const aImage: TFPCustomImage);
var
  Img : TBitmap;
  R : TRect;
begin
  R.Left:=Round(Origin.X+aLeft);
  R.Top:=Round(Origin.Y+aTop);
  R.Right:=Round(Origin.X+aLeft+aWidth);
  R.Bottom:=Round(Origin.Y+aTop+aHeight);
  if R.IsEmpty then exit;

  Img:=TBitmap.Create;
  try
    Img.PixelFormat:=pf32bit;
    if aImage is TLazIntfImage then
      Img.LoadFromIntfImage(TLazIntfImage(aImage))
    else
      Img.Assign(aImage);
    Canvas.StretchDraw(R,Img);
  finally
    Img.Free;
  end;
end;

{ TLCLWSForm }

procedure TLCLWSForm.SetFresnelForm(const AValue: TFresnelCustomForm);
begin
  if FFresnelForm=AValue then Exit;
  FFresnelForm:=AValue;
  if FFresnelForm<>nil then
    FreeNotification(FFresnelForm);
end;

class procedure TLCLWSForm.InitMouseXYEvent(out EvtInit: TFresnelMouseEventInit;
  Shift: TShiftState; X, Y: Integer; Button: Controls.TMouseButton);

begin
  EvtInit:=Default(TFresnelMouseEventInit);
  evtInit.Button:=Fresnel.Events.TMouseButton(Button);
  if ssLeft in Shift then
    Include(EvtInit.Buttons,mbLeft);
  if ssMiddle in Shift then
    Include(EvtInit.Buttons,mbMiddle);
  if ssRight in Shift then
    Include(EvtInit.Buttons,mbRight);
  if ssExtra1 in Shift then
    Include(EvtInit.Buttons,mbExtra1);
  if ssExtra2 in Shift then
    Include(EvtInit.Buttons,mbExtra2);
  EvtInit.ScreenPos.SetLocation(Controls.Mouse.CursorPos);
  EvtInit.PagePos.X:=X;
  EvtInit.PagePos.Y:=Y;
  EvtInit.Shiftstate:=Shift;
end;

procedure TLCLWSForm.LCLMouseDown(Sender: TObject; Button: Controls.TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  EvtInit: TFresnelMouseEventInit;
begin
  InitMouseXYEvent(EvtInit,Shift,X,Y,Button);
  FresnelForm.WSMouseXY(EvtInit,evtMouseDown);
end;

procedure TLCLWSForm.LCLChangeBounds(Sender: TObject);
begin
  FresnelForm.WSResize(GetFormBounds,LCLForm.ClientWidth,LCLForm.ClientHeight);
end;


procedure TLCLWSForm.LCLMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
var
  EvtInit: TFresnelMouseEventInit;
begin
  InitMouseXYEvent(EvtInit,Shift,X,Y);
  FresnelForm.WSMouseXY(EvtInit,evtMouseMove);
end;

procedure TLCLWSForm.LCLMouseUp(Sender: TObject; Button: Controls.TMouseButton;
  Shift: TShiftState; X, Y: Integer);
var
  EvtInit: TFresnelMouseEventInit;
begin
  InitMouseXYEvent(EvtInit,Shift,X,Y,Button);
  FresnelForm.WSMouseXY(EvtInit,evtMouseUp);
end;

procedure TLCLWSForm.LCLPaint(Sender: TObject);

  {$IFDEF FresnelSkia}
  procedure DrawSkia;
  var
    W, H: Integer;
    SkiaRenderer: TFresnelSkiaRenderer;
    SkSurface: ISkSurface;
    SkCanvas: ISkCanvas;
    Desc: TRawImageDescription;
    Bmp: TBitmap;
  begin
    W:=LCLForm.ClientWidth;
    H:=LCLForm.ClientHeight;
    if (W<1) or (H<1) then exit;
    SkiaRenderer:=Renderer as TFresnelSkiaRenderer;

    if FIntfImg=nil then
    begin
      if SkNative32ColorType=TSkColorType.BGRA8888 then
        Desc.Init_BPP32_B8G8R8A8_BIO_TTB(W,H)
      else
        Desc.Init_BPP32_R8G8B8A8_BIO_TTB(W,H);
      FIntfImg:=TLazIntfImage.Create(0,0);
      FIntfImg.DataDescription:=Desc;
      FIntfImg.SetSize(W,H);
    end else if (FIntfImg.Width<>W) or (FIntfImg.Height<>H) then
    begin
      FIntfImg.SetSize(W,H);
    end;

    SkSurface := TSkSurface.MakeRasterDirect(TSkImageInfo.Create(W,H),
                     FIntfImg.PixelData, FIntfImg.DataDescription.BytesPerLine);
    SkCanvas:=SkSurface.Canvas;
    SkiaRenderer.Canvas:=SkCanvas;
    try
      FresnelForm.WSDraw;
    finally
      SkiaRenderer.Canvas:=SkCanvas;
    end;

    Bmp:=TBitmap.Create;
    try
      Bmp.LoadFromIntfImage(FIntfImg);
      LCLForm.Canvas.Draw(0,0,Bmp);
    finally
      Bmp.Free;
    end;
  end;
  {$ENDIF}

begin
  {$IFDEF FresnelSkia}
  DrawSkia;
  {$ELSE}
  FresnelForm.WSDraw;
  {$ENDIF}
end;

{$i fresnel.wintofresnelkey.inc}

procedure TLCLWSForm.LCLKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);

var
  lInfo : TFresnelKeyEventInit;
  lInput : TFresnelInputEventInit;

begin
  lInfo:=Default(TFresnelKeyEventInit);
  lInfo.ShiftState:=Shift;
  lInfo.NumKey:=WinKeyToFresnelKey(Key,0);
  if not FresnelForm.WSKey(lInfo,evtKeyDown) then
    if (lInfo.NumKey<0) then
      begin
      lInput:=Default(TFresnelInputEventInit);
      if NumKeyToInputType(lInfo.NumKey,lInfo.ShiftState,lInput.InputType) then
        FresnelForm.WSInput(lInput);
      end;
end;

procedure TLCLWSForm.LCLKeyUp(Sender: TObject; var Key: Word; Shift: TShiftState);

var
  lInfo : TFresnelKeyEventInit;

begin
  lInfo:=Default(TFresnelKeyEventInit);
  lInfo.ShiftState:=Shift;
  lInfo.NumKey:=WinKeyToFresnelKey(Key,0);
  FresnelForm.WSKey(lInfo,evtKeyUp);
end;

procedure TLCLWSForm.LCLUTF8Key(Sender: TObject; var UTF8Key: TUTF8Char);

var
  lInfo : TFresnelInputEventInit;

begin
  lInfo:=Default(TFresnelInputEventInit);
  case UTF8Key of
    #8: Exit; // Already handled in keydown.
  else
    lInfo.Data:=UTF8Key;
    lInfo.InputType:=fitInsertText;
  end;
  FresnelForm.WSInput(lInfo);
end;

function TLCLWSForm.GetFormBounds: TFresnelRect;
begin
  Result.SetRect(LCLForm.BoundsRect);
end;

function TLCLWSForm.GetCaption: TFresnelCaption;
begin
  Result:=LCLForm.Caption;
end;

function TLCLWSForm.GetVisible: boolean;
begin
  Result:=LCLForm.Visible;
end;

procedure TLCLWSForm.Notification(AComponent: TComponent; Operation: TOperation
  );
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FFresnelForm=AComponent then
      FFresnelForm:=nil;
    if FLCLForm=AComponent then
      FLCLForm:=nil;
  end;
end;

procedure TLCLWSForm.SetFormBounds(const AValue: TFresnelRect);
begin
  LCLForm.BoundsRect:=AValue.GetRect;
end;

procedure TLCLWSForm.SetCaption(AValue: TFresnelCaption);
begin
  LCLForm.Caption:=AValue;
end;

procedure TLCLWSForm.SetVisible(const AValue: boolean);
begin
  LCLForm.Visible:=AValue;
end;

constructor TLCLWSForm.Create(AOwner: TComponent);
var
  aRenderer: TFresnelRenderer;
begin
  inherited Create(AOwner);
  {$IFDEF FresnelSkia}
  aRenderer:=TFresnelSkiaRenderer.Create(Self);
  {$ELSE}
  aRenderer:=TFresnelLCLRenderer.Create(Self);
  {$ENDIF}
  SetRenderer(aRenderer);
end;

destructor TLCLWSForm.Destroy;
begin
  {$IFDEF FrsenelSkia}
  FreeAndNil(FIntfImg);
  {$ENDIF}
  inherited Destroy;
end;

function TLCLWSForm.GetClientSize: TFresnelPoint;
begin
  Result.X:=LCLForm.ClientWidth;
  Result.Y:=LCLForm.ClientHeight;
end;

procedure TLCLWSForm.Invalidate;
begin
  LCLForm.Invalidate;
end;

procedure TLCLWSForm.InvalidateRect(const aRect: TFresnelRect);
var
  PixRect: TRect;
begin
  PixRect:=aRect.GetRect;
  LCLIntf.InvalidateRect(LCLForm.Handle,@PixRect,false);
end;

function TLCLWSForm.CreateLCLForm: TForm;
var
  aFontEngine: TFresnelFontEngine;
begin
  debugln(['TLCLWSForm.CreateLCLForm Bounds=',dbgs(FresnelForm.FormBounds)]);
  FLCLForm := TFresnelLCLForm.CreateNew(Self);
  FLCLForm.FFresnelForm:=FresnelForm;
  Result:=LCLForm;
  {$IFDEF FresnelSkia}
  // create one fontengine per form
  aFontEngine:=TFresnelSkiaFontEngine.Create(Self);
  FresnelForm.FontEngine:=aFontEngine;
  {$ELSE}
  TFresnelLCLRenderer(Renderer).Canvas:=LCLForm.Canvas;
  // create one fontengine per form
  aFontEngine:=TFresnelLCLFontEngine.Create(FLCLForm);
  FresnelForm.FontEngine:=aFontEngine;
  TFresnelLCLFontEngine(aFontEngine).Canvas:=LCLForm.Canvas;
  {$ENDIF}

  // resize lcl form
  FLCLForm.BoundsRect:=FresnelForm.FormBounds.GetRect;

  // events
  FLCLForm.OnMouseDown:=@LCLMouseDown;
  FLCLForm.OnMouseMove:=@LCLMouseMove;
  FLCLForm.OnMouseUp:=@LCLMouseUp;
  FLCLForm.OnPaint:=@LCLPaint;
  FLCLForm.OnChangeBounds:=@LCLChangeBounds;
  FLCLForm.OnKeyDown:=@LCLKeyDown;
  FLCLForm.OnKeyUp:=@LCLKeyUp;
  FLCLForm.OnUTF8KeyPress:=@LCLUTF8Key;
end;

{ TFresnelLCLWidgetSet }

constructor TFresnelLCLWidgetSet.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  if FresnelLCLWidgetSet<>nil then
    raise Exception.Create('TFresnelLCLWidgetSet.Create');
  FresnelLCLWidgetSet:=Self;
  {$IFDEF FresnelSkia}
  {$ELSE}
  DefaultImageDataClass:=TFresnelLCLImageData;
  {$ENDIF}
end;

destructor TFresnelLCLWidgetSet.Destroy;
begin
  inherited Destroy;
  FresnelLCLWidgetSet:=nil;
end;

procedure TFresnelLCLWidgetSet.AppProcessMessages;
begin
  Forms.Application.ProcessMessages;
end;

procedure TFresnelLCLWidgetSet.AppTerminate;
begin
  Forms.Application.Terminate;
end;

procedure TFresnelLCLWidgetSet.AppWaitMessage;
begin
  Forms.Application.Idle(true);
end;

procedure TFresnelLCLWidgetSet.CreateWSForm(aFresnelForm: TFresnelComponent);
var
  aForm: TFresnelCustomForm;
  aWSForm: TLCLWSForm;
begin
  if not (aFresnelForm is TFresnelCustomForm) then
    raise Exception.Create('TFresnelLCLWidgetSet.CreateWSForm '+DbgSName(aFresnelForm));
  if csDesigning in aFresnelForm.ComponentState then
    raise Exception.Create('TFresnelLCLWidgetSet.CreateWSForm '+DbgSName(aFresnelForm)+' csDesigning');
  if csDestroying in aFresnelForm.ComponentState then
    raise Exception.Create('TFresnelLCLWidgetSet.CreateWSForm '+DbgSName(aFresnelForm)+' csDestroying');

  aForm:=TFresnelCustomForm(aFresnelForm);

  aWSForm:=TLCLWSForm.Create(aForm);
  aWSForm.FresnelForm:=aForm;
  aForm.WSForm:=aWSForm;
  aWSForm.CreateLCLForm;
end;

{ TFresnelLCLFontEngine }

constructor TFresnelLCLFontEngine.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFonts:=TAvgLvlTree.Create(@CompareFresnelLCLFont);
end;

destructor TFresnelLCLFontEngine.Destroy;
var
  Node: TAvgLvlTreeNode;
  aFont: TFresnelLCLFont;
begin
  Node:=FFonts.Root;
  while Node<>nil do
  begin
    aFont:=TFresnelLCLFont(Node.Data);
    Node.Data:=nil;
    aFont._Release;
    Node:=Node.Successor;
  end;
  FreeAndNil(FFonts);
  inherited Destroy;
end;

function TFresnelLCLFontEngine.FindFont(const Desc: TFresnelFontDesc
  ): TFresnelLCLFont;
var
  Node: TAvgLvlTreeNode;
begin
  Node:=FFonts.FindKey(@Desc,@CompareFresnelFontDescWithLCLFont);
  if Node=nil then
    Result:=nil
  else
    Result:=TFresnelLCLFont(Node.Data);
end;

function TFresnelLCLFontEngine.Allocate(const Desc: TFresnelFontDesc
  ): IFresnelFont;
var
  aFont: TFresnelLCLFont;
begin
  aFont:=FindFont(Desc);
  if aFont<>nil then
    exit(aFont);
  aFont:=TFresnelLCLFont.Create;
  aFont.Engine:=Self;
  aFont._AddRef;
  aFont.Family:=Desc.Family;
  aFont.Kerning:=Desc.Kerning;
  aFont.Size:=Desc.Size;
  aFont.Style:=Desc.Style;
  aFont.Weight:=Desc.Weight;
  aFont.Width:=Desc.Width;
  FFonts.Add(aFont);
  Result:=aFont;
end;

function TFresnelLCLFontEngine.TextSize(aFont: TFresnelLCLFont;
  const aText: string): TPoint;
var
  aSize: TSize;
begin
  Canvas.Font:=NeedLCLFont(aFont);
  aSize:=Canvas.TextExtent(aText);
  Result.X:=aSize.cx;
  Result.Y:=aSize.cy;
end;

function TFresnelLCLFontEngine.TextSizeMaxWidth(aFont: TFresnelLCLFont;
  const aText: string; MaxWidth: integer): TPoint;
var
  aSize: TSize;
begin
  Canvas.Font:=NeedLCLFont(aFont);

  if LCLIntf.GetTextExtentExPoint(Canvas.Handle, PChar(aText), Length(aText),
    MaxWidth, nil, nil, aSize{%H-}) then
  begin
    Result.X:=aSize.cx;
    Result.Y:=aSize.cy;
  end else begin
    Result.X:=0;
    Result.Y:=0;
  end;
end;

function TFresnelLCLFontEngine.NeedLCLFont(aFont: TFresnelLCLFont): TFont;
var
  aLCLFont: TFont;
  aStyle: TFontStyles;
begin
  if aFont.LCLFont=nil then
  begin
    aLCLFont:=TFont.Create;
    aFont.LCLFont:=aLCLFont;
    aLCLFont.Size:=round(aFont.Size);
    aStyle:=[];
    if aFont.Weight>=500 then
      Include(aStyle,fsBold);
    case aFont.Style of
    'italic': Include(aStyle,fsItalic);
    end;
    aLCLFont.Style:=aStyle;
  end;
  Result:=aFont.LCLFont;
end;

{$IFDEF FresnelSkia}
{$ELSE}
{ TFresnelLCLImageData }

class function TFresnelLCLImageData.CreateData(aWidth, aHeight: Word
  ): TFPCustomImage;
var
  IntfImg: TLazIntfImage;
  aDesc: TRawImageDescription;
begin
  IntfImg := TLazIntfImage.Create(0,0,[]);
  Result:=IntfImg;
  aDesc:=GetDescriptionFromDevice(0, 0, 0); // fallback to default
  AddAlphaToDescription(aDesc,aDesc.RedPrec);
  IntfImg.DataDescription := aDesc;
  Result.SetSize(aWidth,aHeight);
end;
{$ENDIF}

{ TFresnelLCLFont }

destructor TFresnelLCLFont.Destroy;
begin
  FreeAndNil(LCLFont);
  inherited Destroy;
end;

function TFresnelLCLFont.GetAlternates: string;
begin
  Result:='normal';
end;

function TFresnelLCLFont.GetCaps: TFresnelCSSFontVarCaps;
begin
  Result:=ffvcNormal;
end;

function TFresnelLCLFont.GetEastAsians: TFresnelCSSFontVarEastAsians;
begin
  Result:=[ffveaNormal];
end;

function TFresnelLCLFont.GetEmoji: TFresnelCSSFontVarEmoji;
begin
  Result:=ffveNormal;
end;

function TFresnelLCLFont.GetFamily: string;
begin
  Result:=Family;
end;

function TFresnelLCLFont.GetKerning: TFresnelCSSKerning;
begin
  Result:=Kerning;
end;

function TFresnelLCLFont.GetLigatures: TFresnelCSSFontVarLigaturesSet;
begin
  Result:=[ffvlNormal];
end;

function TFresnelLCLFont.GetNumerics: TFresnelCSSFontVarNumerics;
begin
  Result:=[ffvnNormal];
end;

function TFresnelLCLFont.GetPosition: TFresnelCSSFontVarPosition;
begin
  Result:=ffvpNormal;
end;

function TFresnelLCLFont.GetSize: TFresnelLength;
begin
  Result:=Size;
end;

function TFresnelLCLFont.GetStyle: string;
begin
  Result:=Style;
end;

function TFresnelLCLFont.GetWeight: TFresnelLength;
begin
  Result:=Weight;
end;

function TFresnelLCLFont.GetWidth: TFresnelLength;
begin
  Result:=Width;
end;

function TFresnelLCLFont.TextSize(const aText: string): TFresnelPoint;
var
  p: TPoint;
begin
  p:=Engine.TextSize(Self,aText);
  Result.X:=p.X;
  Result.Y:=p.Y;
end;

function TFresnelLCLFont.TextSizeMaxWidth(const aText: string;
  MaxWidth: TFresnelLength): TFresnelPoint;
var
  p: TPoint;
begin
  p:=Engine.TextSizeMaxWidth(Self,aText,Floor(Max(1,MaxWidth)));
  Result.X:=p.X;
  Result.Y:=p.Y;
end;

function TFresnelLCLFont.GetTool: TObject;
begin
  Result:=Self;
end;

function TFresnelLCLFont.GetDescription: String;
begin
  Result:='';
end;

initialization
  TFresnelLCLWidgetSet.Create(nil);
finalization
  FresnelLCLWidgetSet.Free; // it will nil itself

end.

