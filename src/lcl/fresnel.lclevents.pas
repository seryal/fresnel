unit Fresnel.LCLEvents;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, Fresnel.DOM, Fresnel.Events, Fresnel.Classes, Fresnel.LCL;

Type

  { TFresnelLCLEventControl }

  TFresnelLCLEventControl = class (TFresnelComponent)
  private
    FControl: TWinControl;
    FViewPort: TFresnelViewPort;
    procedure InitMouseEvent(out aInit: TFresnelMouseEventInit);
  protected
  class var
    _LastMouseElement : TFresnelElement;
  Protected
    procedure DoFocus(aOld, aNew: TFresnelElement);
    procedure DoBlur(aNew, aOld: TFresnelElement);
    procedure DoEnter(aInit : TFresnelMouseEventInit; aOld, aNew: TFresnelElement);
    procedure DoLeave(aInit : TFresnelMouseEventInit; aNew, aOld: TFresnelElement);
    procedure HandleClick(Sender: TObject);
    procedure HandleMouseDown(Sender: TObject; Button: Controls.TMouseButton; Shift: TShiftState; X, Y: Integer);
    procedure HandleMouseMove(Sender: TObject; Shift: TShiftState; X, Y: Integer);
    procedure HandleMouseUp(Sender: TObject; Button: Controls.TMouseButton; Shift: TShiftState; X, Y: Integer);
  Public
    Constructor Create(aOwner : TComponent; AControl : TWinControl; AViewPort: TFresnelViewPort);  reintroduce;
    Procedure HookEvents;
    Property Control : TWinControl Read FControl;
    Property ViewPort : TFresnelViewPort Read FViewPort;
  end;

implementation

Type
  THC = Class(TWinControl)
  end;

{ TFresnelLCLEventControl }


constructor TFresnelLCLEventControl.Create(aOwner: TComponent; AControl: TWinControl; AViewPort: TFresnelViewPort);
begin
  Inherited Create(aOwner);
  FControl:=aControl;
  FViewPort:=AViewPort;
end;

procedure TFresnelLCLEventControl.HookEvents;
var
  C : THC;
begin
  if not Assigned(Control) then
    exit;
  C:=THC(Control);
  C.OnClick:=@HandleClick;
  C.OnMouseDown:=@HandleMouseDown;
  C.OnMouseUp:=@HandleMouseUp;
  C.OnMouseMove:=@HandleMouseMove;
end;

procedure TFresnelLCLEventControl.InitMouseEvent(out aInit: TFresnelMouseEventInit);

var
  P,PM : TPoint;

begin
  aInit:=Default(TFresnelMouseEventInit);
  PM:=Controls.Mouse.CursorPos;
  P:=Control.ClientToScreen(Point(0,0));
  aInit.ScreenPos.SetLocation(PM);
  aInit.PagePos.X:=PM.X-P.X;
  aInit.PagePos.Y:=PM.Y-P.Y;
end;

procedure TFresnelLCLEventControl.HandleClick(Sender: TObject);
Var
  aInit : TFresnelMouseEventInit;
  aEl : TFresnelElement;
  evt : TFresnelMouseEvent;
  R : TFresnelRect;
begin
  InitMouseEvent(aInit);
  aEl:=Viewport.GetElementAt(aInit.PagePos.X,aInit.PagePos.Y);
  if aEl=Nil then
    aEl:=FViewport;
  R:=aEL.GetBorderBoxOnViewport;
  aInit.ControlPos.X:=aInit.PagePos.X-R.Left;
  aInit.ControlPos.Y:=aInit.PagePos.Y-R.Top;
  evt:=aEl.EventDispatcher.CreateEvent(aEl,evtClick) as TFresnelMouseEvent;
  try
    evt.initEvent(aInit);
    aEl.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;
end;

procedure TFresnelLCLEventControl.HandleMouseDown(Sender: TObject; Button: Controls.TMouseButton; Shift: TShiftState; X, Y: Integer);
Var
  aInit : TFresnelMouseEventInit;
  aEl : TFresnelElement;
  Evt : TFresnelMouseEvent;

begin
  FLLog(etDebug,CLassName+': HandleMouseDown (x: %x, y: %y)',[X,y]);
  TLCLWSForm.InitMouseXYEvent(aInit,Shift,X,Y,Button);
  aEl:=FViewport.GetElementAt(aInit.PagePos.X,aInit.PagePos.Y);
  if aEl=Nil then
    aEl:=Self.Viewport;
  Evt:=aEl.EventDispatcher.CreateEvent(aEl,evtMouseDown) as TFresnelMouseEvent;
  try
    Evt.initEvent(aInit);
    aEl.EventDispatcher.DispatchEvent(Evt);
  finally
    Evt.Free;
  end;
end;

procedure TFresnelLCLEventControl.DoFocus(aOld,aNew : TFresnelElement);

Var
  evtF : TFresnelFocusEvent;
  evt : TFresnelFocusInEvent;

begin
  evtF:=aNew.EventDispatcher.CreateEvent(aNew,evtFocus) as TFresnelFocusEvent;
  try
    evtF.Related:=aOld;
    aNew.EventDispatcher.DispatchEvent(evtF);
  finally
    evtF.Free;
  end;
  // Todo: Bubble
  evt:=aNew.EventDispatcher.CreateEvent(aNew,evtFocusIn) as TFresnelFocusInEvent;
  try
    evt.Related:=aOld;
    aNew.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;
end;

procedure TFresnelLCLEventControl.DoBlur(aNew,aOld : TFresnelElement);

Var
  evt : TFresnelFocusOutEvent;

begin
  evt:=aOld.EventDispatcher.CreateEvent(aOld,evtFocusOut) as TFresnelFocusOutEvent;
  try
    evt.Related:=aNew;
    aOld.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;
end;

procedure TFresnelLCLEventControl.DoEnter(aInit: TFresnelMouseEventInit; aOld, aNew: TFresnelElement);

Var
  evt : TFresnelMouseEnterEvent;

begin
  _LastMouseElement:=aNew;
  FLLog(etDebug,ClassName+'.DoEnter(%s,%s) ',[aNew.Name,aInit.Description]) ;
  evt:=aNew.EventDispatcher.CreateEvent(aNew,evtMouseEnter) as TFresnelMouseEnterEvent;
  try
    evt.InitEvent(aInit);
    evt.Related:=aOld;
    aNew.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;

end;

procedure TFresnelLCLEventControl.DoLeave(aInit: TFresnelMouseEventInit; aNew, aOld: TFresnelElement);
Var
  evt : TFresnelMouseLeaveEvent;

begin
  FLLog(etDebug,ClassName+'.DoLeave(%s,%s) ',[aOld.Name,aInit.Description]) ;
  evt:=aOld.EventDispatcher.CreateEvent(aOld,evtMouseLeave) as TFresnelMouseLeaveEvent;
  try
    evt.InitEvent(aInit);
    evt.Related:=aNew;
    aOld.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;
end;

procedure TFresnelLCLEventControl.HandleMouseMove(Sender: TObject;   Shift: TShiftState; X, Y: Integer);

Var
  aInit : TFresnelMouseEventInit;
  aEl : TFresnelElement;
  evt : TFresnelMouseEvent;

begin
  TLCLWSForm.InitMouseXYEvent(aInit,Shift,X,Y,Controls.mbLeft);
  aEl:=FViewport.GetElementAt(aInit.PagePos.X,aInit.PagePos.Y);
  if aEl=Nil then
    aEl:=Self.Viewport;
  if aEl<>_LastMouseElement then
     begin
     if Assigned(_LastMouseElement) then
       DoLeave(aInit,aEl,_LastMouseElement);
     DoEnter(aInit,_LastMouseElement,aEl);
     end;
  evt:=aEl.EventDispatcher.CreateEvent(aEl,evtMouseMove) as TFresnelMouseEvent;
  try
    evt.initEvent(aInit);
    aEl.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;
end;

procedure TFresnelLCLEventControl.HandleMouseUp(Sender: TObject;  Button: Controls.TMouseButton; Shift: TShiftState; X, Y: Integer);

Var
  aInit : TFresnelMouseEventInit;
  aEl : TFresnelElement;
  evt : TFresnelMouseEvent;

begin
  TLCLWSForm.InitMouseXYEvent(aInit,Shift,X,Y,Button);
  aEl:=FViewport.GetElementAt(aInit.PagePos.X,aInit.PagePos.Y);
  if aEl=Nil then
    aEl:=Self.Viewport;
  evt:=aEl.EventDispatcher.CreateEvent(aEl,evtMouseUp) as TFresnelMouseEvent;
  try
    evt.initEvent(aInit);
    aEl.EventDispatcher.DispatchEvent(evt);
  finally
    evt.Free;
  end;
end;

end.

