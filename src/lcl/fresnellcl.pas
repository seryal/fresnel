{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FresnelLCL;

{$warn 5023 off : no warning about unused units}
interface

uses
  Fresnel.LCL, Fresnel.LCLControls, Fresnel, Fresnel.LCLApp, fresnel.lclevents;

implementation

end.
