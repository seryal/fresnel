unit Fresnel.LCLApp;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms,
  Fresnel.Forms;

type

  { TFresnelLCLApplication }

  TFresnelLCLApplication = class(TFresnelBaseApplication)
  private
    FCritSecQueue: TRTLCriticalSection;
    FLCLQueued: boolean; // true if waiting for LCL message queue to process our messages
    procedure OnLCLQueue(Data: PtrInt);
    procedure OnQueueStarted(Sender: TObject); // can be called by other threads
    procedure SetLCLQueued(const AValue: boolean);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property LCLQueued: boolean read FLCLQueued write SetLCLQueued;
  end;

var
  FresnelLCLApp: TFresnelLCLApplication;

implementation

{ TFresnelLCLApplication }

procedure TFresnelLCLApplication.OnQueueStarted(Sender: TObject);
begin
  LCLQueued:=true;
end;

procedure TFresnelLCLApplication.OnLCLQueue(Data: PtrInt);
begin
  EnterCriticalSection(FCritSecQueue);
  FLCLQueued:=false;
  LeaveCriticalSection(FCritSecQueue);
  if Data=0 then ;
  ProcessMessages;
end;

procedure TFresnelLCLApplication.SetLCLQueued(const AValue: boolean);
begin
  EnterCriticalSection(FCritSecQueue);
  try
    if FLCLQueued=AValue then Exit;
    FLCLQueued:=AValue;
  finally
    LeaveCriticalSection(FCritSecQueue);
  end;
  if AValue then
    Forms.Application.QueueAsyncCall(@OnLCLQueue,0)
  else
    Forms.Application.RemoveAsyncCalls(Self);
end;

constructor TFresnelLCLApplication.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  InitCriticalSection(FCritSecQueue);
  FresnelLCLApp:=Self;
  AsyncCalls.OnQueueStarted:=@OnQueueStarted;
end;

destructor TFresnelLCLApplication.Destroy;
begin
  AsyncCalls.OnQueueStarted:=nil;
  DoneCriticalSection(FCritSecQueue);
  inherited Destroy;
  FresnelLCLApp:=nil;
end;

initialization
  TFresnelLCLApplication.Create(nil);
finalization
  FresnelLCLApp.Free; // will nil itself

end.

