unit Fresnel.LCLControls;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Controls, Forms, Fresnel.Events, Fresnel.Layouter, Fresnel.DOM, Fresnel.LCL,
  fresnel.lclevents;

type

  { TFresnelLCLCtlViewport }

  TFresnelLCLCtlViewport = class(TFresnelViewport)
  protected
    FIsDrawing: boolean;
  public
    function IsDrawing: boolean; override;
  end;

  { TFresnelLCLControl }

  TFresnelLCLControl = class(TCustomControl)
  private
    FClearing: boolean;
    FFontEngine: TFresnelLCLFontEngine;
    FLayouter: TViewportLayouter;
    FLayoutQueued: boolean;
    FRenderer: TFresnelLCLRenderer;
    FViewport: TFresnelLCLCtlViewport;
    FLCLEvents: TFresnelLCLEventControl;
    procedure OnDomChanged(Sender: TObject);
    procedure OnQueuedLayout({%H-}Data: PtrInt);
    procedure SetLayoutQueued(const AValue: boolean);
  Protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure Paint; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property Viewport: TFresnelLCLCtlViewport read FViewport;
    property Layouter: TViewportLayouter read FLayouter;
    property FontEngine: TFresnelLCLFontEngine read FFontEngine;
    property Renderer: TFresnelLCLRenderer read FRenderer;
    property LayoutQueued: boolean read FLayoutQueued write SetLayoutQueued;
  end;

implementation

uses Dialogs;

{ TFresnelLCLCtlViewport }

function TFresnelLCLCtlViewport.IsDrawing: boolean;
begin
  Result:=FIsDrawing;
end;

{ TFresnelLCLControl }

procedure TFresnelLCLControl.SetLayoutQueued(const AValue: boolean);
begin
  if FLayoutQueued=AValue then Exit;
  if FClearing then exit;
  if csDestroyingHandle in ControlState then exit;
  if csDestroying in ComponentState then exit;
  FLayoutQueued:=AValue;
  if FLayoutQueued then
    Forms.Application.QueueAsyncCall(@OnQueuedLayout,0);
end;

procedure TFresnelLCLControl.OnDomChanged(Sender: TObject);
begin
  LayoutQueued:=true;
end;

procedure TFresnelLCLControl.OnQueuedLayout(Data: PtrInt);
begin
  Invalidate;
end;

procedure TFresnelLCLControl.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if AComponent=FViewport then
      FViewport:=nil;
    if AComponent=FLayouter then
    begin
      FLayouter:=nil;
      if FViewport<>nil then
        FViewport.Layouter:=nil;
    end;
  end;
end;

procedure TFresnelLCLControl.Paint;
begin
  if Viewport.FIsDrawing then
    raise Exception.Create('20250303212620 already drawing');
  Viewport.FIsDrawing:=true;
  try
    inherited Paint;

    if Viewport.DomModified then
    begin
      Viewport.ApplyCSS;
    end;
    Renderer.Draw(Viewport);
  finally
    Viewport.FIsDrawing:=false;
  end;
end;

constructor TFresnelLCLControl.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FViewport:=TFresnelLCLCtlViewport.Create(nil);
  FViewport.OnDomChanged:=@OnDomChanged;
  FFontEngine:=TFresnelLCLFontEngine.Create(nil);
  FViewport.FontEngine:=FontEngine;
  FontEngine.Canvas:=Canvas;
  FViewport.Name:='Viewport';
  FLayouter:=TViewPortLayouter.Create(nil);
  Layouter.Viewport:=ViewPort;
  FRenderer:=TFresnelLCLRenderer.Create(nil);
  FRenderer.Canvas:=Canvas;
  FLCLEvents:=TFresnelLCLEventControl.Create(Nil,Self,FViewPort);
  FLCLEvents.HookEvents;
end;

destructor TFresnelLCLControl.Destroy;
begin
  FClearing:=true;
  FreeAndNil(FLCLEvents);
  FreeAndNil(FRenderer);
  FreeAndNil(FLayouter);
  FreeAndNil(FViewport);
  FreeAndNil(FFontEngine);
  inherited Destroy;
end;

end.

