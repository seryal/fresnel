{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Asynchronous call handling for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}
unit Fresnel.AsyncCalls;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Contnrs;

Type
  EAsyncCall = Class(Exception);

  { TAsyncCallQueues }

  TAsyncDataEvent = procedure (Data: Pointer) of object;

  TAsyncCallQueues = Class(TObject)
  private
    Type
      PAsyncCallQueueItem = ^TAsyncCallQueueItem;
      TAsyncCallQueueItem = record
        Method: TAsyncDataEvent;
        Data: Pointer;
        Free : Boolean;
        NextItem, PrevItem: PAsyncCallQueueItem;
      end;
      TAsyncCallQueue = record
        Top, Last: PAsyncCallQueueItem;
      end;
  Private
    FLock: TRTLCriticalSection;
    FCur: TAsyncCallQueue; // currently processing
    FNext: TAsyncCallQueue; // new calls added to this queue
    FOnQueueStarted: TNotifyEvent;
    FWakeMainThreadOnCalls : Boolean;
  Protected
    Procedure Lock; inline;
    Procedure Unlock; inline;
  Public
    constructor Create;
    destructor Destroy; override;
    procedure ProcessQueue; // called by
    procedure QueueAsyncCall(const aMethod: TAsyncDataEvent; aData: Pointer; aFreeObject : Boolean = false);
    procedure RemoveAsyncCalls(const aObject: TObject);
    property WakeMainThreadOnCalls : Boolean Read FWakeMainThreadOnCalls Write FWakeMainThreadOnCalls;
    property OnQueueStarted: TNotifyEvent read FOnQueueStarted write FOnQueueStarted; // called everytime the Queue got its first message
  end;

implementation

{ TAsyncCallQueues }

procedure TAsyncCallQueues.Lock;
begin
  System.EnterCriticalsection(FLock);
end;

procedure TAsyncCallQueues.Unlock;
begin
  System.LeaveCriticalsection(FLock);
end;

constructor TAsyncCallQueues.Create;
begin
  System.InitCriticalSection(FLock);
end;

destructor TAsyncCallQueues.Destroy;
begin
  System.DoneCriticalSection(FLock);
  inherited Destroy;
end;

procedure TAsyncCallQueues.ProcessQueue;

var
  lItem: PAsyncCallQueueItem;
  Event: TAsyncDataEvent;
  Data: Pointer;
  Obj : TObject;
  FreeList : TFPObjectList;

begin
  // move the items of NextQueue to CurQueue, keep the order
  Lock;
  try
    if FNext.Top<>nil then
      begin
      if FCur.Last<>nil then
        begin
        FCur.Last^.NextItem:=FNext.Top;
        FNext.Top^.PrevItem:=FCur.Last;
        end
      else
        begin
        FCur.Top:=FNext.Top;
        end;
      FCur.Last:=FNext.Last;
      FNext.Top:=nil;
      FNext.Last:=nil;
      end;
  finally
    UnLock;
  end;

  // process items from top to last in 'Cur' queue
  // this can create new items, which are added to the 'Next' queue
  // or it can call ProcessAsyncCallQueue, for example via calling
  // Application.ProcesssMessages
  // Using a second queue avoids an endless loop, when an event adds a new event.
  FreeList:=Nil;
  try
    repeat
      // remove top item from queue
      Lock;
      try
        if FCur.Top=nil then exit;
        lItem:=FCur.Top;
        FCur.Top := lItem^.NextItem;
        if FCur.Top = nil then
          FCur.Last := nil
        else
          FCur.Top^.PrevItem := nil;
        // free item
        Event:=lItem^.Method;
        Data:=lItem^.Data;
        if lItem^.Free then
          begin
          Obj:=TObject(TMethod(Event).Data);
          if (FreeList=Nil) then
            FreeList:=TFPObjectList.Create(True);
          if FreeList.IndexOf(Obj)=-1 then
            FreeList.Add(Obj)
          end;
        Dispose(lItem);
      finally
        UnLock;
      end;
      // call event
      if (TMethod(Event).Code<>Nil) then
        Event(Data);
    until false;

  finally
    // Will free all objects in it.
    FreeList.Free;
  end;
end;

procedure TAsyncCallQueues.QueueAsyncCall(const aMethod: TAsyncDataEvent; aData: Pointer; aFreeObject : Boolean = false);

var
  lItem: PAsyncCallQueueItem;
  WasFirst: Boolean;
begin
  New(lItem);
  lItem^.Method := aMethod;
  lItem^.Data := aData;
  lItem^.NextItem := nil;
  lItem^.Free := aFreeObject;
  Lock;
  try
    WasFirst:=FNext.Top=nil;
    with FNext do
      begin
      lItem^.PrevItem := Last;
      if Last<>nil then
        begin
        Last^.NextItem := lItem
        end
      else
        begin
        Top := lItem;
        end;
      Last := lItem;
    end;
  finally
    Unlock;
  end;
  if WasFirst then
    begin
    if Assigned(OnQueueStarted) then
      OnQueueStarted(Self);
    if WakeMainThreadOnCalls and Assigned(WakeMainThread) then
      WakeMainThread(nil);
    end;
end;

procedure TAsyncCallQueues.RemoveAsyncCalls(const aObject: TObject);

  procedure DoRemoveAsyncCalls(var AQueue: TAsyncCallQueue);
  var
    lItem, lItem2: PAsyncCallQueueItem;
  begin
    lItem := AQueue.Last;
    while lItem <> nil do
      begin
      if TMethod(lItem^.Method).Data <> Pointer(aObject) then
        lItem := lItem^.PrevItem
      else
        begin
        if lItem^.NextItem <> nil then
          lItem^.NextItem^.PrevItem := lItem^.PrevItem;
        if lItem^.PrevItem <> nil then
          lItem^.PrevItem^.NextItem := lItem^.NextItem;

        if lItem = AQueue.Last then
          AQueue.Last := lItem^.PrevItem;
        if lItem = AQueue.Top then
          AQueue.Top := lItem^.NextItem;

        lItem2 := lItem;
        lItem := lItem^.PrevItem;
        Dispose(lItem2);
        end
    end;
  end;

begin
  Lock;
  try
    DoRemoveAsyncCalls(FCur);
    DoRemoveAsyncCalls(FNext);
  finally
    UnLock;
  end;
end;

end.

