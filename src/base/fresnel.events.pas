{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Event handling for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel.Events;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

uses
  Classes, SysUtils,
  {$IF FPC_FULLVERSION>30300}
  system.UITypes,
  {$ENDIF}
  FCL.Events, Fresnel.Classes;

Const
  evtUnknown = 0;
  evtKeyDown = 1;
  evtKeyUp = 2;
  evtInput = 3;
  evtEnter = 4;
  evtLeave = 5;
  evtClick = 6;
  evtDblClick = 7;
  evtChange = 8;
  evtDrag = 9;
  evtDragEnd = 10;
  evtDragEnter = 11;
  evtDragOver = 12;
  evtDragLeave = 13;
  evtDragStart = 14;
  evtDrop = 15;
  evtMouseMove = 16;
  evtMouseDown = 17;
  evtMouseUp = 18;
  evtMouseOver = 19;
  evtMouseEnter = 20;
  evtMouseLeave = 21;
  evtMouseWheel = 22;
  evtFocusIn = 23;
  evtFocusOut = 24;
  evtFocus = 25;
  evtBlur = 26;


  evtLastControlEvent = evtBlur;

  evtAllMouse = [evtMouseMove,evtMouseDown,evtMouseUp];
    evtAllFocus = [evtFocusIn,evtFocusOut,evtFocus];

  // Form only
  evtFormEvents = evtLastControlEvent;

  evtOnFormCreate         = evtFormEvents + 1;
  evtOnFormDestroy        = evtFormEvents + 2;

  evtLastFormEvent = evtOnFormDestroy;

  // Application only
  evtAppMessages =  evtLastFormEvent + 1;
  evtAfterProcessMessages = evtAppMessages;

  evtIdle       = evtAppMessages + 1;
  evtIdleEnd    = evtAppMessages + 2;
  evtActivate   = evtAppMessages + 3;
  evtDeactivate = evtAppMessages + 4;
  evtMinimize   = evtAppMessages + 5;
  evtMaximize   = evtAppMessages + 6;
  evtRestore    = evtAppMessages + 7;

  evtLastApplicationEvent = evtRestore;

  // Last
  evtLastEvent = evtLastApplicationEvent;

Type

  { TFresnelEvent }

  TFresnelEvent = Class(TAbstractEvent)
  private
    FDefaultPrevented: Boolean;
    FPropagationStopped: Boolean;
  public
    Class Function FresnelEventID : TEventID; virtual; abstract;
    class function StandardEventName(aEventID: TEventID): TEventName;
    class function EventName: TEventName; override;
    Procedure PreventDefault; virtual;
    Property DefaultPrevented : Boolean Read FDefaultPrevented;
    Procedure StopPropagation; virtual;
    Property PropagationStopped : Boolean Read FPropagationStopped;
  end;
  TFresnelEventClass = Class of TFresnelEvent;

  TFresnelEventHandler = TEventHandler;
  TFresnelEventCallBack = TEventCallBack;
  {$IFDEF HasFunctionReferences}
  TFresnelEventHandlerRef = TEventHandlerRef;
  {$ENDIF}

  TFresnelUIEvent = class(TFresnelEvent)

  end;

  {$IF FPC_FULLVERSION>30300}
  TMouseButton = System.UITypes.TMouseButton;
  {$ELSE}
  TMouseButton = (mbLeft, mbRight, mbMiddle, mbExtra1, mbExtra2);
  {$ENDIF}
  TMouseButtons = set of TMouseButton;

Const
  // Browser aliases
  mbMain = mbLeft; // usually left
  mbAux = mbMiddle;  // usually middle wheel
  mbSecond = mbRight; // usually right
  mbFourth = mbExtra1;
  mbFifth = mbExtra2;

Type

  { TFresnelMouseEventInit }

  TFresnelMouseEventInit = Record
    Button: TMouseButton;
    Buttons: TMouseButtons;
    PagePos: TFresnelPoint;
    ScreenPos: TFresnelPoint;
    Shiftstate: TShiftState;
    ControlPos: TFresnelPoint;
    Function Description : String;
  end;

  { TFresnelMouseEvent }

  TFresnelMouseEvent = Class(TFresnelUIEvent)
  private
    function GetShiftKey(AIndex: Integer): Boolean;
  protected
    FInit : TFresnelMouseEventInit;
  Public
    Constructor Create(const aInit : TFresnelMouseEventInit); overload;
    Procedure InitEvent(const aInit : TFresnelMouseEventInit);
    Property ControlX : TFresnelLength Read FInit.ControlPos.X;
    Property ControlY : TFresnelLength Read FInit.ControlPos.Y;
    Property PageX : TFresnelLength Read FInit.PagePos.X; // document relative
    Property PageY : TFresnelLength Read FInit.PagePos.Y;
    Property ScreenX : TFresnelLength Read FInit.ScreenPos.X; // screen relative
    Property ScreenY : TFresnelLength Read FInit.ScreenPos.Y;
    Property X : TFresnelLength Read FInit.ControlPos.X; // relative to element receiving the event
    Property Y : TFresnelLength Read FInit.ControlPos.Y;
    Property Buttons: TMouseButtons Read FInit.Buttons;
    Property Button : TMouseButton Read FInit.Button;
    Property ShiftState : TShiftState Read FInit.Shiftstate;
    Property Altkey : Boolean Index Ord(ssAlt) read GetShiftKey;
    Property MetaKey : Boolean Index Ord(ssMeta) read GetShiftKey;
    Property CtrlKey : Boolean Index Ord(ssCtrl) read GetShiftKey;
    Property ShiftKey : Boolean Index Ord(ssShift) read GetShiftKey;
  end;
  TFLMouseEvent = TFresnelMouseEvent;
  TFresnelMouseEventHandler = Procedure(Event: TFresnelMouseEvent) of object;

  { TFresnelMouseClickEvent }

  TFresnelMouseClickEvent = class(TFresnelMouseEvent)
    class function FresnelEventID : TEventID; override;
  end;
  TFLMouseClickEvent = TFresnelMouseClickEvent;

  TFresnelMouseDoubleClickEvent = class(TFresnelMouseEvent)
    class function FresnelEventID : TEventID; override;
  end;
  TFLMouseDoubleClickEvent = TFresnelMouseDoubleClickEvent;

  { TFresnelMouseDownEvent }

  TFresnelMouseDownEvent = class(TFresnelMouseEvent)
    class function FresnelEventID : TEventID; override;
  end;
  TFLMouseDownEvent = TFresnelMouseDownEvent;

  { TFresnelMouseMoveEvent }

  TFresnelMouseMoveEvent = class(TFresnelMouseEvent)
    class function FresnelEventID : TEventID; override;
  end;
  TFLMouseMoveEvent = TFresnelMouseMoveEvent;

  { TFresnelMouseUpEvent }

  TFresnelMouseUpEvent = class(TFresnelMouseEvent)
    class function FresnelEventID : TEventID; override;
  end;
  TFLMouseUpEvent = TFresnelMouseUpEvent;

  { TFresnelMouseEnterEvent }

  TFresnelMouseEnterEvent = class(TFresnelMouseEvent)
  private
    FRelated: TObject;
  Public
    class function FresnelEventID : TEventID; override;
    Property Related : TObject Read FRelated Write FRelated;
  end;
  TFLMouseEnterEvent = TFresnelMouseEnterEvent;

  { TFresnelMouseLeaveEvent }

  TFresnelMouseLeaveEvent = class(TFresnelMouseEvent)
  private
    FRelated: TObject;
  Public
    class function FresnelEventID : TEventID; override;
    Property Related : TObject Read FRelated Write FRelated;
  end;
  TFLMouseLeaveEvent = TFresnelMouseLeaveEvent;

  TFresnelKeyEventInit = Record
    // State of shift keys
    ShiftState : TShiftState;
    // Composing ?
    IsComposing : Boolean;
    // Numerical key value:
    // if positive, unicode char
    // If negative: special key (see TKeyCodes).
    NumKey : Integer;
    // Key name: resolved unicode ("A" on Azerty) char or special key name (see TKeyNames)
    Key : String;
    // Key code: key code ("Q" on Azerty) or special key name (see TKeyNames)
    Code : ShortString;
  end;

  { TFresnelKeyEvent }

  TFresnelKeyEvent = class(TFresnelEvent)
  Private
    FInit : TFresnelKeyEventInit;
    function GetShiftKey(AIndex: Integer): Boolean;
  Public
    Constructor Create(const aInit : TFresnelKeyEventInit);
    function ShiftState : TShiftState;
    Property Code: ShortString Read FInit.Code;
    Property Key : String Read Finit.Key;
    Property NumKey : Integer Read Finit.NumKey;
    Property Altkey : Boolean Index Ord(ssAlt) read GetShiftKey;
    Property MetaKey : Boolean Index Ord(ssMeta) read GetShiftKey;
    Property CtrlKey : Boolean Index Ord(ssCtrl) read GetShiftKey;
    Property ShiftKey : Boolean Index Ord(ssShift) read GetShiftKey;
  end;
  TFLKeyEvent = TFresnelKeyEvent;
  TFresnelKeyEventClass = class of TFresnelKeyEvent;

  { TFresnelKeyUpEvent }

  TFresnelKeyUpEvent = class(TFresnelKeyEvent)
    Class function FresnelEventID: TEventID; override;
  end;
  TFLKeyUpEvent = TFresnelKeyUpEvent;

  { TFresnelKeyDownEvent }

  TFresnelKeyDownEvent = class(TFresnelKeyEvent)
    Class function FresnelEventID: TEventID; override;
  end;
  TFLDownEvent = TFresnelKeyDownEvent;

  { TFresnelChangeEvent }

  TFresnelChangeEvent = class(TFresnelEvent)
    Class Function FresnelEventID: TEventID; override;
  end;
  TFLChangeEvent = TFresnelChangeEvent;

  { TFresnelFocusEvent }

  TFresnelFocusEvent = Class(TFresnelUIEvent)
    Class function FresnelEventID: TEventID; override;
  private
    FRelated: TObject;
  Public
    Property Related : TObject Read FRelated Write FRelated;
  end;
  TFLFocusEvent = TFresnelFocusEvent;
  TFresnelFocusEventHandler = Procedure(Event: TFresnelFocusEvent) of object;

  { TFresnelFocusInEvent }

  TFresnelFocusInEvent = Class(TFresnelFocusEvent)
    Class function FresnelEventID: TEventID; override;
  end;
  TFLFocusInEvent = TFresnelFocusInEvent;

  { TFresnelFocusOutEvent }

  TFresnelFocusOutEvent = Class(TFresnelFocusEvent)
    Class function FresnelEventID: TEventID; override;
  end;
  TFLFocusOutEvent = TFresnelFocusOutEvent;

  { TFresnelBlurEvent }

  TFresnelBlurEvent = Class(TFresnelUIEvent)
    Class function FresnelEventID: TEventID; override;
  end;
  TFLBlurEvent = TFresnelBlurEvent;

  // List taken from
  // https://rawgit.com/w3c/input-events/v1/index.html#interface-InputEvent-Attributes

  TFresnelInputType = (
    fitInsertText, fitInsertReplacementText, fitInsertLineBreak,
    fitInsertParagraph, fitInsertOrderedList, fitInsertUnorderedList,
    fitInsertHorizontalRule, fitInsertFromYank, fitInsertFromDrop,
    fitInsertFromPaste, fitInsertFromPasteAsQuotation, fitInsertTranspose,
    fitInsertCompositionText, fitInsertLink,
    fitDeleteWordBackward, fitDeleteWordForward, fitDeleteSoftLineBackward,
    fitDeleteSoftLineForward, fitDeleteEntireSoftLine, fitDeleteHardLineBackward,
    fitDeleteHardLineForward, fitDeleteByDrag, fitDeleteByCut,
    fitDeleteContent, fitDeleteContentBackward, fitDeleteContentForward,
    fitHistoryUndo, fitHistoryRedo,
    fitFormatBold, fitFormatItalic, fitFormatUnderline, fitFormatStrikeThrough,
    fitFormatSuperscript, fitformatSubscript, fitFormatJustifyFull, fitFormatJustifyCenter,
    fitFormatJustifyRight, fitFormatJustifyLeft, fitFormatIndent, fitFormatOutdent,
    fitFormatRemove, fitFormatSetBlockTextDirection, fitFormatSetInlineTextDirection,
    fitFormatBackColor, fitFormatFontColor, fitFormatFontName
  );
  TFLInputType = TFresnelInputType;
  TFresnelInputEventInit = record
    Data : string;
    InputType : TFresnelInputType;
  end;

  { TFresnelInputEvent }

  TFresnelInputEvent = Class(TFresnelUIEvent)
  Private
    FInit : TFresnelInputEventInit;
    function GetInputType: string;
  Public
    Constructor Create(aInit : TFresnelInputEventInit);
    class function FresnelEventID: TEventID; override;
    Property Data : String Read FInit.Data;
    Property FresnelInputType : TFresnelInputType Read FInit.inputtype;
    Property InputType : string Read GetInputType;
  end;
  TFLInputEvent = TFresnelInputEvent;

  TDeltaMode = (pixel,line,page);
  TFresnelWheelInit = record
    DeltaMode : TDeltaMode;
    DeltaX : Integer;
    DeltaY : Integer;
    DeltaZ : Integer;
  end;

  TFresnelWheelEventInit = Record
    MouseInit : TFresnelMouseEventInit;
    WheelInit : TFresnelWheelInit;
  end;

  { TFresnelWheelEvent }

  TFresnelWheelEvent = Class(TFresnelMouseEvent)
  Private
    FWheelInit : TFresnelWheelInit;
  Public
    Constructor Create(aInit : TFresnelWheelEventInit); reintroduce;
    Property DeltaMode : TDeltaMode Read FWheelInit.DeltaMode;
    Property DeltaX : Integer Read FWheelInit.DeltaX;
    Property DeltaY : Integer Read FWheelInit.DeltaY;
    Property DeltaZ : Integer Read FWheelInit.DeltaZ;
  end;
  TFLWheelEvent = TFresnelWheelEvent;

   { TFresnelEventDispatcher }

   TFresnelEventDispatcher = Class(TEventDispatcher)
   Private
     Class Var _Registry : TEventRegistry;
   Protected
     class Function CreateFresnelRegistry : TEventRegistry; virtual;
     Function GetRegistry: TEventRegistry; override;
   Public
     Class Function FresnelRegistry : TEventRegistry;
     Class Procedure RegisterFresnelEvents;
     Class Destructor Done;
   end;
   TFLEventDispatcher = TFresnelEventDispatcher;

implementation

uses TypInfo;

Const
  FresnelEventNames : Array[0..evtLastEvent] of TEventName = (
    '?',
    'KeyDown',
    'KeyUp',
    'KeyPress',
    'Enter',
    'Leave',
    'Click',
    'DblClick',
    'Change',
    'Drag',
    'DragEnd',
    'DragEnter',
    'DragOver',
    'DragLeave',
    'DragStart',
    'Drop',
    'MouseMove',
    'MouseDown',
    'MouseUp',
    'MouseOver',
    'MouseEnter',
    'MouseLeave',
    'MouseWheel',
    'FocusIn',
    'FocusOut',
    'Focus',
    'Blur',
    'Create',
    'Destroy',
    'AfterProcessMessages',
    'Idle',
    'IdleEnd',
    'Activate',
    'Deactivate',
    'Minimize',
    'Maximize',
    'Restore'
  );

{ TFresnelWheelEvent }

constructor TFresnelWheelEvent.Create(aInit: TFresnelWheelEventInit);
begin
  Inherited Create(aInit.MouseInit);
  FWheelInit:=aInit.WheelInit;
end;

{ TFresnelInputEvent }

function TFresnelInputEvent.GetInputType: string;
begin
  Result:=GetEnumName(TypeInfo(TFresnelInputType),Ord(FInit.inputtype));
end;

constructor TFresnelInputEvent.Create(aInit: TFresnelInputEventInit);
begin
  Inherited Create(Nil,FresnelEventID);
  FInit:=AInit;
end;

class function TFresnelInputEvent.FresnelEventID: TEventID;
begin
  Result:=evtInput;
end;

{ TFresnelBlurEvent }

class function TFresnelBlurEvent.FresnelEventID: TEventID;
begin
  Result:=evtBlur;
end;

{ TFresnelFocusOutEvent }

class function TFresnelFocusOutEvent.FresnelEventID: TEventID;
begin
  Result:=evtFocusOut;
end;

{ TFresnelFocusInEvent }

class function TFresnelFocusInEvent.FresnelEventID: TEventID;
begin
  Result:=evtFocusin;
end;

{ TFresnelFocusEvent }

class function TFresnelFocusEvent.FresnelEventID: TEventID;
begin
  Result:=evtFocus;
end;

{ TFresnelKeyDownEvent }

class function TFresnelKeyDownEvent.FresnelEventID: TEventID;
begin
  Result:=evtKeyDown;
end;

{ TFresnelKeyUpEvent }

class function TFresnelKeyUpEvent.FresnelEventID: TEventID;
begin
  Result:=evtKeyUp;
end;


{ TFresnelKeyEvent }

function TFresnelKeyEvent.GetShiftKey(AIndex: Integer): Boolean;
begin
  Result:=TShiftStateEnum(aIndex) in Finit.ShiftState;
end;

constructor TFresnelKeyEvent.Create(const aInit: TFresnelKeyEventInit);
begin
  Inherited Create(Nil,FresnelEventID);
  FInit:=aInit;
end;

function TFresnelKeyEvent.ShiftState: TShiftState;
begin
  Result:=Finit.ShiftState;
end;

{ TFresnelMouseEvent }

function TFresnelMouseEvent.GetShiftKey(AIndex: Integer): Boolean;
begin
  Result:=TShiftStateEnum(aIndex) in ShiftState;
end;

constructor TFresnelMouseEvent.Create(const aInit: TFresnelMouseEventInit);
begin
  InitEvent(aInit);
end;

procedure TFresnelMouseEvent.InitEvent(const aInit: TFresnelMouseEventInit);
begin
  FInit:=aInit;
end;

{ TFresnelChangeEvent }

class function TFresnelChangeEvent.FresnelEventID: TEventID;
begin
  Result:=evtChange;
end;

{ TFresnelMouseClickEvent }

class function TFresnelMouseClickEvent.FresnelEventID: TEventID;
begin
  Result:=evtClick;
end;

{ TFresnelMouseDoubleClickEvent }

class function TFresnelMouseDoubleClickEvent.FresnelEventID: TEventID;
begin
  Result:=evtDblClick;
end;

{ TFresnelMouseDownEvent }

class function TFresnelMouseDownEvent.FresnelEventID: TEventID;
begin
  Result:=evtMouseDown;
end;

{ TFresnelMouseMoveEvent }

class function TFresnelMouseMoveEvent.FresnelEventID: TEventID;
begin
  Result:=evtMouseMove;
end;

{ TFresnelMouseUpEvent }

class function TFresnelMouseUpEvent.FresnelEventID: TEventID;
begin
  Result:=evtMouseUp;
end;

{ TFresnelMouseEnterEvent }

class function TFresnelMouseEnterEvent.FresnelEventID: TEventID;
begin
  Result:=evtMouseEnter;
end;

{ TFresnelMouseLeaveEvent }

class function TFresnelMouseLeaveEvent.FresnelEventID: TEventID;
begin
  Result:=evtMouseLeave;
end;

{ TFresnelEvent }

class function TFresnelEvent.StandardEventName(aEventID : TEventID): TEventName;
begin
  If aEventID<Length(FresnelEventNames) then
    Result:=FresnelEventNames[aEventID]
  else
    Result:=IntToStr(aEventID);
end;

class function TFresnelEvent.EventName: TEventName;
begin
  Result:=StandardEventName(FresnelEventID);
end;

procedure TFresnelEvent.PreventDefault;
begin
  FDefaultPrevented:=True;
end;

procedure TFresnelEvent.StopPropagation;
begin
  FPropagationStopped:=true;
end;

{ TFresnelMouseEventInit }

function TFresnelMouseEventInit.Description: String;
var
  Btn,Btns,SS : String;
begin
  Btn:=GetEnumName(TypeInfo(TMouseButton),Ord(Button)) ;
  Btns:=SetToString(PTypeInfo(TypeInfo(TMouseButtons)),Longint(Buttons),True);
  SS:=SetToString(PTypeInfo(TypeInfo(TShiftState)),Longint(ShiftSTate),True);
  Result:=Format(' (X: %f, Y: %f, Button: %s, Buttons: %s; Shift: %s) ', [PagePos.X,PagePos.Y,Btn,Btns,SS]);
end;

Type

  { TFresnelRegistry }

  TFresnelRegistry = Class(TEventRegistry)
    Class function DefaultIDOffset : TEventID; override;
  end;

{ TFresnelRegistry }

class function TFresnelRegistry.DefaultIDOffset: TEventID;
begin
  Result:=evtLastEvent+1;
end;

{ TFresnelEventDispatcher }

class function TFresnelEventDispatcher.CreateFresnelRegistry: TEventRegistry;
begin
  Result:=TFresnelRegistry.Create;
end;

function TFresnelEventDispatcher.GetRegistry: TEventRegistry;
begin
  Result:=FresnelRegistry;
end;

class function TFresnelEventDispatcher.FresnelRegistry: TEventRegistry;
begin
  if _Registry=Nil then
    _Registry:=CreateFresnelRegistry;
  Result:=_Registry;
end;

class procedure TFresnelEventDispatcher.RegisterFresnelEvents;

  Procedure R(aClass : TFresnelEventClass);

  begin
    FresnelRegistry.RegisterEventWithID(aClass.FresnelEventID,aClass);
  end;

begin
  R(TFresnelChangeEvent);
  R(TFresnelFocusEvent);
  R(TFresnelFocusInEvent);
  R(TFresnelFocusOutEvent);
  R(TFresnelKeyDownEvent);
  R(TFresnelKeyUpEvent);
  R(TFresnelInputEvent);
end;

class destructor TFresnelEventDispatcher.Done;
begin
  FreeAndNil(_Registry);
end;

end.

