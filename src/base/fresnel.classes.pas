{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Basic classes and types for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel.Classes;

{$mode objfpc}{$H+}
{$IF FPC_FULLVERSION>=30301}
  {$WARN 6060 off} // Case statement does not handle all possible cases
{$ENDIF}
{$ModeSwitch AdvancedRecords}

interface

uses
  Classes, SysUtils, Math, Types, FpImage, fpCSSScanner, fpCSSResParser;

type
  {$IF FPC_FULLVERSION<30301}
  RTLString = string;
  {$ENDIF}

  { EFresnel }

  EFresnel = class(Exception)
  end;

  TFresnelLength = double;
  TArray4FresnelLength =  array[0..3] of TFresnelLength;

  TCalcBoolean = (cbCalc,cbFalse,cbTrue);

const
  MaxFresnelLength = TFresnelLength(high(longint));

const
  FresnelCSSFormatSettings: TFormatSettings = ( // todo: use fpCSSTree.CSSFormatSettings
    CurrencyFormat: 1;
    NegCurrFormat: 5;
    ThousandSeparator: ',';
    DecimalSeparator: '.';
    CurrencyDecimals: 2;
    DateSeparator: '-';
    TimeSeparator: ':';
    ListSeparator: ',';
    CurrencyString: '$';
    ShortDateFormat: 'd/m/y';
    LongDateFormat: 'dd" "mmmm" "yyyy';
    TimeAMString: 'AM';
    TimePMString: 'PM';
    ShortTimeFormat: 'hh:nn';
    LongTimeFormat: 'hh:nn:ss';
    ShortMonthNames: ('Jan','Feb','Mar','Apr','May','Jun',
                      'Jul','Aug','Sep','Oct','Nov','Dec');
    LongMonthNames: ('January','February','March','April','May','June',
                     'July','August','September','October','November','December');
    ShortDayNames: ('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
    LongDayNames:  ('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
    TwoDigitYearCenturyWindow: 50;
  );

type
  TFresnelCaption = type string;

  { TFresnelPoint }

  TFresnelPoint = packed record
    X, Y: TFresnelLength;
    constructor Create(const aX, aY: TFresnelLength); overload;
    constructor Create(const aPoint: TPoint); overload;
    class function PointInCircle(const aPoint, aCenter: TFresnelPoint; const aRadius: TFresnelLength): Boolean; static; inline;
    class function Zero: TFresnelPoint; static; inline;
    class operator + (const p1, p2: TFresnelPoint): TFresnelPoint;
    class operator - (const p1, p2: TFresnelPoint): TFresnelPoint;
    class operator <> (const p1, p2: TFresnelPoint): Boolean;
    class operator = (const p1, p2: TFresnelPoint): Boolean;
    function Add(const p: TFresnelPoint): TFresnelPoint;
    function Angle(const p: TFresnelPoint): TFresnelLength;
    function Distance(const p: TFresnelPoint): TFresnelLength;
    function IsZero: Boolean;
    function Subtract(const p: TFresnelPoint): TFresnelPoint;
    procedure Offset(const dx,dy: TFresnelLength);
    procedure Offset(const p: TFresnelPoint);
    procedure SetLocation(const ax, ay: TFresnelLength); overload;
    procedure SetLocation(const p: TFresnelPoint); overload;
    procedure SetLocation(const p: TPoint); overload;
    function GetPoint: TPoint;
    function GetPointF: TPointF;
    function ToString : String;
  end;
  TFLPoint = TFresnelPoint;
  TFresnelPointArray = array of TFresnelPoint;

  { TFresnelRect }

  TFresnelRect = packed record
  private
    function GetHeight: TFresnelLength;
    function GetSize: TFresnelPoint;
    function GetWidth: TFresnelLength;
    procedure SetHeight(const AValue: TFresnelLength);
    procedure SetSize(const AValue: TFresnelPoint);
    procedure SetWidth(const AValue: TFresnelLength);
  public
    procedure Clear;
    constructor Create(const ALeft, ATop, ARight, ABottom: TFresnelLength); overload;
    constructor Create(const aRect: TRect); overload;
    class operator = (const L, R: TFresnelRect): Boolean;
    class operator <> (const L, R: TFresnelRect): Boolean;
    class operator + (const L, R: TFresnelRect): TFresnelRect; // union
    class operator * (const L, R: TFresnelRect): TFresnelRect; // intersection
    class function Empty: TFresnelRect; static;
    procedure NormalizeRect;
    function IsEmpty: Boolean;
    function Contains(const P: TFresnelPoint): Boolean;
    function Contains(const R: TFresnelRect): Boolean;
    function Contains(const ax, ay: TFresnelLength): Boolean;
    function IntersectsWith(const R: TFresnelRect): Boolean;
    class function Intersect(const R1, R2: TFresnelRect): TFresnelRect; static;
    procedure Intersect(const R: TFresnelRect);
    class function Union(const R1, R2: TFresnelRect): TFresnelRect; static;
    procedure Union(const R: TFresnelRect);
    class function Union(const Points: TFresnelPointArray): TFresnelRect; static;
    procedure Offset(const DX, DY: TFresnelLength);
    procedure Offset(const DP: TFresnelPoint);
    procedure SetLocation(const X, Y: TFresnelLength);
    procedure SetLocation(const P: TFresnelPoint);
    procedure Inflate(const DX, DY: TFresnelLength);
    procedure Inflate(const DL, DT, DR, DB: TFresnelLength);
    function CenterPoint: TFresnelPoint;
    function GetRectF: TRectF;
    procedure SetRectF(const r: TRectF);
    function GetRect: TRect;
    procedure SetRect(const r: TRect);
    function ToString : string;
  public
    property Height: TFresnelLength read GetHeight write SetHeight;
    property Width: TFresnelLength read GetWidth write SetWidth;
    property Size: TFresnelPoint read GetSize write SetSize;
    case Longint of
      0: (Left, Top, Right, Bottom: TFresnelLength);
      1: (TopLeft, BottomRight: TFresnelPoint);
      2: (Vector: TArray4FresnelLength);
  end;
  TFLRect = TFresnelRect;
  TFresnelRectDynArray = array of TFresnelRect;

  { TFresnelComponent }

  TFresnelComponent = class(TComponent)
  Public
    Type
      TFresnelLogHandler = Procedure (aType : TEventType; Const Msg : String) of object;
    Class var
      _LogHook : TFresnelLogHandler;
  Protected
    Class Procedure DoLog(aType: TEventType; Const Msg : string);
    Class Procedure DoLog(aType: TEventType; Const Fmt : string; Const Args : Array of const);
  Public
    function ToString: RTLString; override;
  end;
  TFLComponent = TFresnelComponent;


function FloatToCSSStr(const f: TFresnelLength): string;
function FloatToCSSPx(const p: TFresnelLength): string;
function CSSStrToFloat(const s: string; out l: TFresnelLength): boolean;
function CompareFresnelPoint(const A, B: TFresnelPoint): integer;
function CompareFresnelRect(const A, B: TFresnelRect): integer;

function PointFre(const X, Y: TFresnelLength): TFresnelPoint; overload;
function RectFre(const Left, Top, Right, Bottom: TFresnelLength): TFresnelRect; overload;
function BoundsRectFre(const Left, Top, Width, Height: TFresnelLength): TFresnelRect; overload;

function FPColor(c: TCSSAlphaColor): TFPColor; overload;

Procedure FLLog(aType: TEventType; Const Msg : string); overload;
Procedure FLLog(aType: TEventType; Const Fmt : string; Const Args : Array of const); overload;
Procedure FLLog(aType: TEventType; const args : Array of string); overload;
function DbgSName(const p: TObject): string; overload;
function DbgSName(const p: TClass): string; overload;

Var
  MinStrokeWidth : TFresnelLength = 0.09;

operator := (a : boolean) b : TCalcBoolean;
operator := (a : TCalcBoolean) b : Boolean;

procedure NormStroke(var s: TFresnelLength; NoNegative: boolean);
function  NormalizeStroke(s: TFresnelLength; NoNegative: boolean) : TFresnelLength; inline;

implementation

function FloatToCSSStr(const f: TFresnelLength): string;
begin
  Result:=FloatToStr(f,FresnelCSSFormatSettings);
end;

function FloatToCSSPx(const p: TFresnelLength): string;
begin
  Result:=FloatToStr(p,FresnelCSSFormatSettings)+'px';
end;

function CSSStrToFloat(const s: string; out l: TFresnelLength): boolean;
var
  Code: Integer;
begin
  Code:=0;
  l:=0;
  val(s,l,Code);
  if Code<>0 then exit(false);
  if IsNan(l) or (l>MaxFresnelLength) or (l<-MaxFresnelLength) then
    Result:=false
  else
    Result:=true;
end;

function CompareFresnelPoint(const A, B: TFresnelPoint): integer;
begin
  if A.X>B.X then
    Result:=1
  else if A.X<B.X then
    Result:=-1
  else if A.Y>B.Y then
    Result:=1
  else if A.Y<B.Y then
    Result:=-1
  else
    Result:=0;
end;

function CompareFresnelRect(const A, B: TFresnelRect): integer;
begin
  if A.Left>B.Left then
    Result:=1
  else if A.Left<B.Left then
    Result:=-1
  else if A.Top>B.Top then
    Result:=1
  else if A.Top<B.Top then
    Result:=-1
  else if A.Right>B.Right then
    Result:=1
  else if A.Right<B.Right then
    Result:=-1
  else if A.Bottom>B.Bottom then
    Result:=1
  else if A.Bottom<B.Bottom then
    Result:=-1
  else
    Result:=0;
end;

function PointFre(const X, Y: TFresnelLength): TFresnelPoint;
begin
  Result.X:=X;
  Result.Y:=Y;
end;

function RectFre(const Left, Top, Right, Bottom: TFresnelLength): TFresnelRect;
begin
  Result.Left:=Left;
  Result.Top:=Top;
  Result.Right:=Right;
  Result.Bottom:=Bottom;
end;

function BoundsRectFre(const Left, Top, Width, Height: TFresnelLength): TFresnelRect;
begin
  Result.Left:=Left;
  Result.Top:=Top;
  Result.Right:=Left+Width;
  Result.Bottom:=Top+Height;
end;

function FPColor(c: TCSSAlphaColor): TFPColor;
begin
  Result.Blue:=c and $ff;
  Result.Blue:=Result.Blue or (Result.Blue shl 8);
  c:=c shr 8;
  Result.Green:=c and $ff;
  Result.Green:=Result.Green or (Result.Green shl 8);
  c:=c shr 8;
  Result.Red:=c and $ff;
  Result.Red:=Result.Red or (Result.Red shl 8);
  c:=c shr 8;
  Result.Alpha:=c and $ff;
  Result.Alpha:=Result.Alpha or (Result.Alpha shl 8);
end;

procedure FLLog(aType: TEventType; const Msg: string);
begin
  TFresnelComponent.DoLog(aType,Msg);
end;

procedure FLLog(aType: TEventType; const Fmt: string;
  const Args: array of const);
begin
  TFresnelComponent.DoLog(aType,Fmt,Args);
end;

procedure FLLog(aType: TEventType; const args: array of string);

var
  Msg,S: String;

begin
  Msg:='';
  For S in Args do
    Msg:=Msg+S;
  FLLog(aType,Msg);
end;

function DbgSName(const p: TObject): string;
begin
  if p=nil then
    Result:='nil'
  else if p is TComponent then
    Result:=TComponent(p).Name+':'+p.ClassName
  else
    Result:=p.ClassName;
end;

function DbgSName(const p: TClass): string;
begin
  if p=nil then
    Result:='nil'
  else
    Result:=p.ClassName;
end;

{ TFresnelPoint }

class function TFresnelPoint.Zero: TFresnelPoint;
begin
  Result.x := 0.0;
  Result.y := 0.0;
end;

function TFresnelPoint.Add(const p: TFresnelPoint): TFresnelPoint;
begin
  Result.x := X+p.X;
  Result.y := Y+p.Y;
end;

function TFresnelPoint.Distance(const p: TFresnelPoint): TFresnelLength;
begin
  Result := Sqrt(Sqr(p.X-X)+Sqr(p.Y-Y));
end;

function TFresnelPoint.IsZero: Boolean;
begin
  Result:=SameValue(X,0) and SameValue(y,0);
end;

function TFresnelPoint.Subtract(const p: TFresnelPoint): TFresnelPoint;
begin
  Result.x := X-p.X;
  Result.y := Y-p.Y;
end;

procedure TFresnelPoint.SetLocation(const p: TFresnelPoint);
begin
  X:=p.X;
  Y:=p.Y;
end;

procedure TFresnelPoint.SetLocation(const p: TPoint);
begin
  X:=p.X;
  Y:=p.Y;
end;

function TFresnelPoint.GetPoint: TPoint;
begin
  Result.X:=round(X);
  Result.Y:=round(Y);
end;

function TFresnelPoint.GetPointF: TPointF;
begin
  Result.x:=X;
  Result.y:=Y;
end;

function TFresnelPoint.ToString: String;
begin
  Result:=Format('(%g,%g)',[X,Y]);
end;

procedure TFresnelPoint.SetLocation(const ax, ay: TFresnelLength);
begin
  X:=ax;
  Y:=ay;
end;

procedure TFresnelPoint.Offset(const p: TFresnelPoint);
begin
  X:=X+p.X;
  Y:=Y+p.Y;
end;

procedure TFresnelPoint.Offset(const dx, dy: TFresnelLength);
begin
  X:=X+dX;
  Y:=Y+dY;
end;

function TFresnelPoint.Angle(const p: TFresnelPoint): TFresnelLength;

  function ArcTan2(const y,x: TFresnelLength): TFresnelLength;
    begin
      if x=0 then
        begin
          if y=0 then
            Result:=0.0
          else if y>0 then
            Result:=pi/2
          else
            Result:=-pi/2;
        end
      else
        begin
          Result:=ArcTan(y/x);
          if x<0 then
            if y<0 then
              Result:=Result-pi
            else
              Result:=Result+pi;
        end;
    end;

begin
  Result:=ArcTan2(Y-p.Y,X-p.X);
end;

constructor TFresnelPoint.Create(const aX, aY: TFresnelLength);
begin
  X:=aX;
  Y:=aY;
end;

constructor TFresnelPoint.Create(const aPoint: TPoint);
begin
  X:=aPoint.X;
  Y:=aPoint.Y;
end;

class function TFresnelPoint.PointInCircle(const aPoint,
  aCenter: TFresnelPoint; const aRadius: TFresnelLength): Boolean;
begin
  Result := aPoint.Distance(aCenter) <= aRadius;
end;

class operator TFresnelPoint.=(const p1, p2: TFresnelPoint): Boolean;
begin
  Result:=SameValue(p1.X,p2.X) and SameValue(p1.Y,p2.Y);
end;

class operator TFresnelPoint.<>(const p1, p2: TFresnelPoint): Boolean;
begin
  Result:=(not SameValue(p1.X,p2.X)) or (not SameValue(p1.Y,p2.Y));
end;

class operator TFresnelPoint.+(const p1, p2: TFresnelPoint): TFresnelPoint;
begin
  Result.X:=p1.X+p2.X;
  Result.Y:=p1.Y+p2.Y;
end;

class operator TFresnelPoint.-(const p1, p2: TFresnelPoint): TFresnelPoint;
begin
  Result.X:=p1.X-p2.X;
  Result.Y:=p1.Y-p2.Y;
end;

{ TFresnelRect }

procedure TFresnelRect.Clear;
begin
  Left:=0.0;
  Top:=0.0;
  Right:=0.0;
  Bottom:=0.0;
end;

constructor TFresnelRect.Create(const ALeft, ATop, ARight,
  ABottom: TFresnelLength);
begin
  Left := ALeft;
  Top := ATop;
  Right := ARight;
  Bottom := ABottom;
end;

constructor TFresnelRect.Create(const aRect: TRect);
begin
  Left:=aRect.Left;
  Top:=aRect.Top;
  Right:=aRect.Right;
  Bottom:=aRect.Bottom;
end;

class operator TFresnelRect.=(const L, R: TFresnelRect): Boolean;
begin
  Result := SameValue(L.Left,R.Left) and SameValue(L.Right,R.Right)
        and SameValue(L.Top,R.Top) and SameValue(L.Bottom,R.Bottom);
end;

class operator TFresnelRect.<>(const L, R: TFresnelRect): Boolean;
begin
  Result := (not SameValue(L.Left,R.Left)) and (not SameValue(L.Right,R.Right))
        and (not SameValue(L.Top,R.Top)) and (not SameValue(L.Bottom,R.Bottom));
end;

class operator TFresnelRect.+(const L, R: TFresnelRect): TFresnelRect;
begin
  Result := TFresnelRect.Union(L, R);
end;

class operator TFresnelRect.*(const L, R: TFresnelRect): TFresnelRect;
begin
  Result := TFresnelRect.Intersect(L, R);
end;

class function TFresnelRect.Empty: TFresnelRect;
begin
  Result := TFresnelRect.Create(0,0,0,0);
end;

procedure TFresnelRect.NormalizeRect;
var
  h: TFresnelLength;
begin
  if Top>Bottom then
  begin
    h := Top;
    Top := Bottom;
    Bottom := h;
  end;
  if Left>Right then
  begin
    h := Left;
    Left := Right;
    Right := h;
  end
end;

function TFresnelRect.IsEmpty: Boolean;
begin
  Result := (Right <= Left) or (Bottom <= Top);
end;

function TFresnelRect.Contains(const P: TFresnelPoint): Boolean;
begin
  Result := (Left <= P.X) and (P.X < Right) and (Top <= P.Y) and (P.Y < Bottom);
end;

function TFresnelRect.Contains(const R: TFresnelRect): Boolean;
begin
  Result := (Left <= R.Left) and (R.Right <= Right) and (Top <= R.Top) and (R.Bottom <= Bottom);
end;

function TFresnelRect.Contains(const ax, ay: TFresnelLength): Boolean;
begin
  Result := (Left <= ax) and (ax < Right) and (Top <= ay) and (ay < Bottom);
end;

function TFresnelRect.IntersectsWith(const R: TFresnelRect): Boolean;
begin
  Result := (Left < R.Right) and (R.Left < Right) and (Top < R.Bottom) and (R.Top < Bottom);
end;

class function TFresnelRect.Intersect(const R1, R2: TFresnelRect): TFresnelRect;
begin
  Result.Left:=Max(R1.Left,R2.Left);
  Result.Right:=Min(R1.Right,R2.Right);
  Result.Top:=Max(R1.Top,R2.Top);
  Result.Bottom:=Min(R1.Bottom,R2.Bottom);
  if Result.IsEmpty then
    FillByte(Result,SizeOf(TFresnelRect),0);
end;

procedure TFresnelRect.Intersect(const R: TFresnelRect);
begin
  if Left<R.Left then Left:=R.Left;
  if Right>R.Right then Right:=R.Right;
  if Top<R.Top then Top:=R.Top;
  if Bottom>R.Bottom then Bottom:=R.Bottom;
  if IsEmpty then
    FillByte(Self,SizeOf(TFresnelRect),0);
end;

class function TFresnelRect.Union(const R1, R2: TFresnelRect): TFresnelRect;
begin
  Result.Left:=Min(R1.Left,R2.Left);
  Result.Right:=Max(R1.Right,R2.Right);
  Result.Top:=Min(R1.Top,R2.Top);
  Result.Bottom:=Max(R1.Bottom,R2.Bottom);
  if Result.IsEmpty then
    FillByte(Result,SizeOf(TFresnelRect),0);
end;

procedure TFresnelRect.Union(const R: TFresnelRect);
begin
  if Left>R.Left then Left:=R.Left;
  if Right<R.Right then Right:=R.Right;
  if Top>R.Top then Top:=R.Top;
  if Bottom<R.Bottom then Bottom:=R.Bottom;
  if IsEmpty then
    FillByte(Self,SizeOf(TFresnelRect),0);
end;

class function TFresnelRect.Union(const Points: TFresnelPointArray
  ): TFresnelRect;
var
  i: Integer;
begin
  if Length(Points) > 0 then
  begin
    Result.TopLeft := Points[Low(Points)];
    Result.BottomRight := Points[Low(Points)];

    for i := Low(Points)+1 to High(Points) do
    begin
      if Points[i].X < Result.Left then Result.Left := Points[i].X;
      if Points[i].X > Result.Right then Result.Right := Points[i].X;
      if Points[i].Y < Result.Top then Result.Top := Points[i].Y;
      if Points[i].Y > Result.Bottom then Result.Bottom := Points[i].Y;
    end;
  end else
    Result := Empty;
end;

procedure TFresnelRect.Offset(const DX, DY: TFresnelLength);
begin
  Left:=Left+DX;
  Top:=Top+DY;
  Right:=Right+DX;
  Bottom:=Bottom+DY;
end;

procedure TFresnelRect.Offset(const DP: TFresnelPoint);
begin
  Left:=Left+DP.X;
  Top:=Top+DP.Y;
  Right:=Right+DP.X;
  Bottom:=Bottom+DP.Y;
end;

procedure TFresnelRect.SetLocation(const X, Y: TFresnelLength);
begin
  Offset(X-Left, Y-Top);
end;

procedure TFresnelRect.SetLocation(const P: TFresnelPoint);
begin
  Offset(P.X-Left, P.Y-Top);
end;

procedure TFresnelRect.Inflate(const DX, DY: TFresnelLength);
begin
  Left:=Left-DX;
  Top:=Top-DY;
  Right:=Right+DX;
  Bottom:=Bottom+DY;
end;

procedure TFresnelRect.Inflate(const DL, DT, DR, DB: TFresnelLength);
begin
  Left:=Left-DL;
  Top:=Top-DT;
  Right:=Right+DR;
  Bottom:=Bottom+DB;
end;

function TFresnelRect.CenterPoint: TFresnelPoint;
begin
  Result.X := (Left+Right)/2;
  Result.Y := (Top+Bottom)/2;
end;

function TFresnelRect.GetRectF: TRectF;
begin
  Result.Left:=Left;
  Result.Top:=Top;
  Result.Right:=Right;
  Result.Bottom:=Bottom;
end;

procedure TFresnelRect.SetRectF(const r: TRectF);
begin
  Left:=r.Left;
  Top:=r.Top;
  Right:=r.Right;
  Bottom:=r.Bottom;
end;

function TFresnelRect.GetRect: TRect;
begin
  Result.Left:=round(Left);
  Result.Top:=round(Top);
  Result.Right:=round(Right);
  Result.Bottom:=round(Bottom);
end;

procedure TFresnelRect.SetRect(const r: TRect);
begin
  Left:=r.Left;
  Top:=r.Top;
  Right:=r.Right;
  Bottom:=r.Bottom;
end;

function TFresnelRect.ToString: string;
begin
  Result:=Format('[%g,%g,r=%g,b=%g]',[Left,Top,Right,Bottom]);
end;

function TFresnelRect.GetHeight: TFresnelLength;
begin
  Result:=Bottom-Top;
end;

function TFresnelRect.GetSize: TFresnelPoint;
begin
  Result.X:=Right-Left;
  Result.Y:=Bottom-Top;
end;

function TFresnelRect.GetWidth: TFresnelLength;
begin
  Result:=Right-Left;
end;

procedure TFresnelRect.SetHeight(const AValue: TFresnelLength);
begin
  Bottom:=Top+AValue;
end;

procedure TFresnelRect.SetSize(const AValue: TFresnelPoint);
begin
  Right:=Left+AValue.X;
  Bottom:=Top+AValue.Y;
end;

procedure TFresnelRect.SetWidth(const AValue: TFresnelLength);
begin
  Right:=Left+AValue;
end;

{ TFresnelComponent }

class procedure TFresnelComponent.DoLog(aType: TEventType; const Msg: string);
begin
  if Assigned(_LogHook) then
    _LogHook(aType,Msg);
end;

class procedure TFresnelComponent.DoLog(aType: TEventType; const Fmt: string;
  const Args: array of const);
begin
  if Assigned(_logHook) then
    _LogHook(aType,SafeFormat(Fmt,Args));
end;

function TFresnelComponent.ToString: RTLString;
begin
  Result:=Name+':'+ClassName;
end;

operator := (a : boolean) b : TCalcBoolean;
const
  bools : Array[Boolean] of TCalcBoolean = (cbFalse,cbTrue);

begin
  b:=Bools[a];
end;

operator := (a : TCalcBoolean) b : Boolean;

begin
  b:=(a=cbTrue);
end;

function NormalizeStroke(s: TFresnelLength; NoNegative: boolean): TFresnelLength;

begin
  if NoNegative and (s<0) then
    Exit(0);
  if SameValue(s,0,MinStrokeWidth) then
    Exit(0);
  Result:=S;
end;

procedure NormStroke(var s: TFresnelLength; NoNegative: boolean);
begin
  S:=NormalizeStroke(S,NoNegative);
end;


end.

