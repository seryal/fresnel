{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Forms unit for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel.Forms;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Math, CustApp, fpCSSTree, Contnrs,
  Fresnel.StrConsts, Fresnel.Classes, Fresnel.Resources,
  Fresnel.DOM, Fresnel.Renderer, Fresnel.Layouter, Fresnel.WidgetSet,
  Fresnel.Events, FCL.Events, Fresnel.AsyncCalls;



type
  TDataEvent = TAsyncDataEvent;
  TIdleEvent = procedure (Sender: TObject; var Done: Boolean) of object;

  TFormState = (
    fsMinimized,
    fsMaximized,
    fsLayoutQueued,
    fsDrawing
    );
  TFormStates = set of TFormState;

  TFormStyle = (fsNormal, fsStayOnTop, fsSplash, fsSystemStayOnTop);

  IFresnelFormDesigner = interface
    ['{095CB7DD-E291-45B6-892E-F486A38E597C}']
    function GetDesignerClientHeight: integer;
    function GetDesignerClientWidth: integer;
    function GetRenderer: TFresnelRenderer;
    procedure InvalidateRect(Sender: TObject; ARect: TRect; Erase: boolean);
    procedure SetDesignerFormBounds(Sender: TObject; NewBounds: TRect);
  end;

  { TFresnelCustomForm }

  TFresnelCustomForm = class(TFresnelViewport)
  private
    FDesigner: IFresnelFormDesigner;
    FFormBounds: TFresnelRect;
    FCaption: TFresnelCaption;
    FFormStates: TFormStates;
    FFormStyle: TFormStyle;
    FOnCreate: TNotifyEvent;
    FOnDestroy: TNotifyEvent;
    FVisible: boolean;
    FWSForm: TFresnelWSForm;
    function GetCaption: TFresnelCaption;
    function GetFormHeight: TFresnelLength;
    function GetFormLeft: TFresnelLength;
    function GetFormTop: TFresnelLength;
    function GetFormWidth: TFresnelLength;
    function GetLayoutQueued: boolean;
    function GetRenderer: TFresnelRenderer;
    function GetWSForm: TFresnelWSForm;
    procedure SetFormHeight(const AValue: TFresnelLength);
    procedure SetFormLeft(const AValue: TFresnelLength);
    procedure SetFormTop(const AValue: TFresnelLength);
    procedure SetFormWidth(const AValue: TFresnelLength);
    procedure SetVisible(const AValue: boolean);
  protected
    procedure ChildDestroying(El: TFresnelElement); override;
    procedure DoCreate; virtual;
    procedure DoDestroy; virtual;
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure OnQueuedLayout({%H-}Data: Pointer); virtual;
    procedure ProcessResource; virtual;
    procedure SetCaption(AValue: TFresnelCaption); virtual;
    procedure SetDesigner(AValue: IFresnelFormDesigner); virtual;
    procedure SetFormBounds(AValue: TFresnelRect); virtual;
    procedure SetFormStyle(AValue: TFormStyle); virtual;
    procedure SetLayoutQueued(const AValue: boolean); virtual;
    procedure SetWSForm(const AValue: TFresnelWSForm); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    constructor CreateNew(AOwner: TComponent); virtual;
    destructor Destroy; override;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    procedure DomChanged; override;
    procedure Hide; virtual;
    procedure Show; virtual;
    procedure Invalidate; override;
    procedure InvalidateRect(const aRect: TFresnelRect); virtual;
    function IsDrawing: boolean; override;
    property Designer: IFresnelFormDesigner read FDesigner write SetDesigner;
  public
    // widgetset
    procedure CreateWSForm; virtual;
    function WSFormAllocated: Boolean;
    property WSForm: TFresnelWSForm read GetWSForm write SetWSForm;
    procedure WSDraw; virtual;
    procedure WSResize(const NewFormBounds: TFresnelRect; NewWidth, NewHeight: TFresnelLength); virtual;
  public
    property Caption: TFresnelCaption read GetCaption write SetCaption;
    property FormStates: TFormStates read FFormStates;
    property LayoutQueued: boolean read GetLayoutQueued write SetLayoutQueued;
    property Renderer: TFresnelRenderer read GetRenderer;
    property FormStyle: TFormStyle read FFormStyle write SetFormStyle default fsNormal;
    property FormBounds: TFresnelRect read FFormBounds write SetFormBounds;
    property FormLeft: TFresnelLength read GetFormLeft write SetFormLeft;
    property FormTop: TFresnelLength read GetFormTop write SetFormTop;
    property FormWidth: TFresnelLength read GetFormWidth write SetFormWidth;
    property FormHeight: TFresnelLength read GetFormHeight write SetFormHeight;
    property Visible: boolean read FVisible write SetVisible default False;
    property OnCreate: TNotifyEvent read FOnCreate write FOnCreate;
    property OnDestroy: TNotifyEvent read FOnDestroy write FOnDestroy;
  end;
  TFresnelCustomFormClass = Class of TFresnelCustomForm;

  { TFresnelForm }

  TFresnelForm = class(TFresnelCustomForm)
  published
    property Caption;
    property FormLeft;
    property FormTop;
    property FormWidth;
    property FormHeight;
    property Stylesheet;
    property OnCreate;
    property OnDestroy;
    property Visible;
  end;
  TFresnelFormClass = class of TFresnelForm;

  TFresnelFormEvent = class(TFresnelEvent);

  { TFresnelFormCreateEvent }

  TFresnelFormCreateEvent = class(TFresnelFormEvent)
  Public
    Class Function FresnelEventID : TEventID; override;
  end;

  { TFresnelFormDestroyEvent }

  TFresnelFormDestroyEvent = class(TFresnelFormEvent)
  Public
    Class Function FresnelEventID : TEventID; override;
  end;


  TFresnelApplicationEvent = class(TFresnelEvent);

  { TFresnelAfterProcessMessagesEvent }

  TFresnelAfterProcessMessagesEvent = class(TFresnelApplicationEvent)
    Class Function FresnelEventID : TEventID; override;
  end;

  (*
  evtIdle       = evtAppMessages + 1;
  evtIdleEnd    = evtAppMessages + 2;
  evtActivate   = evtAppMessages + 3;
  evtDeactivate = evtAppMessages + 4;
  evtMinimize   = evtAppMessages + 5;
  evtMaximize   = evtAppMessages + 6;
  evtRestore    = evtAppMessages + 7;

  *)

  { TFresnelApplicationIdleEvent }

  TFresnelApplicationIdleEvent = class(TFresnelApplicationEvent)
    Class Function FresnelEventID : TEventID; override;
  end;

  { TFresnelApplicationIdleEndEvent }

  TFresnelApplicationIdleEndEvent = class(TFresnelApplicationEvent)
    Class Function FresnelEventID : TEventID; override;
  end;

  { TFresnelApplicationActivateEvent }

  TFresnelApplicationActivateEvent = class(TFresnelApplicationEvent)
    Class Function FresnelEventID : TEventID; override;
  end;

  { TFresnelApplicationDeActivateEvent }

  TFresnelApplicationDeActivateEvent = class(TFresnelApplicationEvent)
    Class Function FresnelEventID : TEventID; override;
  end;

  { TFresnelApplicationMinimizeEvent }

  TFresnelApplicationMinimizeEvent = class(TFresnelApplicationEvent)
    Class Function FresnelEventID : TEventID; override;
  end;

  { TFresnelApplicationMaximizeEvent }

  TFresnelApplicationMaximizeEvent = class(TFresnelApplicationEvent)
    Class Function FresnelEventID : TEventID; override;
  end;

  { TFresnelApplicationRestoreEvent }

  TFresnelApplicationRestoreEvent = class(TFresnelApplicationEvent)
    Class Function FresnelEventID : TEventID; override;
  end;

  { TFresnelFormManager }

  TFresnelFormManager = Class(TComponent)
  private
    FList : TFPObjectList;
    FMainForm: TFresnelCustomForm;
  Protected
    procedure SetMainForm(AValue: TFresnelCustomForm); virtual;
    Procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  Public
    Constructor Create(aOwner : TComponent); override;
    Destructor Destroy; override;
    Procedure AddForm(aForm : TFresnelCustomForm);
    Procedure RemoveForm(aForm : TFresnelCustomForm);
    Property MainForm : TFresnelCustomForm Read FMainForm Write SetMainForm;
  end;
  TFresnelFormManagerClass = Class of TFresnelFormManager;

  { TFresnelBaseApplication }

  TFresnelBaseApplication = class(TCustomApplication,IFresnelVPApplication)
  private
    FAsyncCall: TAsyncCallQueues;
    FEventDispatcher: TFresnelEventDispatcher;
    FHoverElements: TFresnelElementArray;
    fMouseDownElement: TFresnelElement;
    fMouseDownPageXY: TFresnelPoint;
    function GetHoverElements: TFresnelElementArray;
    procedure SetHoverElements(const AValue: TFresnelElementArray);
    function GetMouseDownElement(out PageXY: TFresnelPoint): TFresnelElement;
    procedure SetMouseDownElement(El: TFresnelElement; const PageXY: TFresnelPoint);
  protected
    procedure DoFresnelLog(aType: TEventType; const Msg: String); virtual;
    function GetHookFresnelLog: Boolean; virtual;
    procedure SetHookFresnelLog(AValue: Boolean); virtual;
    function CreateEventDispatcher(aDefaultSender : TObject) : TFresnelEventDispatcher; virtual;
    procedure DoHandleAsyncCalls; virtual;
    class procedure RegisterApplicationEvents; virtual;
    procedure DoProcessMessages; virtual;
    procedure ShowMainForm; virtual;
    Property AsyncCalls: TAsyncCallQueues Read FAsyncCall;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ProcessMessages; virtual;
    procedure QueueAsyncCall(const aMethod: TDataEvent; aData: Pointer);
    procedure RemoveAsyncCalls(const aObject: TObject);
    Procedure CreateForm(aClass : TComponentClass; Out FormVariable); virtual; // use this for designed forms and components
    Procedure CreateFormNew(aClass : TComponentClass; Out FormVariable); virtual;
    procedure ElementDestroying(El: TFresnelElement); virtual;
    function AddEventListener(aID : TEventID; const aHandler : TFresnelEventHandler) : Integer;
    function AddEventListener(Const aName: TEventName; const aHandler : TFresnelEventHandler): Integer;
    function IsHoverElement(El: TFresnelElement): boolean; virtual;
    property EventDispatcher : TFresnelEventDispatcher Read FEventDispatcher;
    Property HookFresnelLog : Boolean Read GetHookFresnelLog Write SetHookFresnelLog;
    property HoverElements: TFresnelElementArray read FHoverElements write SetHoverElements;
  end;

var
  Application: TFresnelBaseApplication;
  FormManagerClass : TFresnelFormManagerClass = Nil;

Function FormManager : TFresnelFormManager;

implementation

var
  _FormManager : TFresnelFormManager;

Function FormManager : TFresnelFormManager;
var
  aClass : TFresnelFormManagerClass;
begin
  if _FormManager = Nil then
    begin
    aClass:=FormManagerClass;
    if aClass=Nil then
      aClass:=TFresnelFormManager;
    _FormManager:=aClass.Create(Nil);
    end;
  Result:=_FormManager;
end;

{ TFresnelCustomForm }

procedure TFresnelCustomForm.SetFormStyle(AValue: TFormStyle);
begin
  if FFormStyle=AValue then Exit;
  FFormStyle:=AValue;
  if WSFormAllocated then
    ;
end;

function TFresnelCustomForm.GetLayoutQueued: boolean;
begin
  Result:=fsLayoutQueued in FFormStates;
end;

function TFresnelCustomForm.GetRenderer: TFresnelRenderer;
begin
  if Designer<>nil then
    begin
    //Writeln('Designer renderer');
    Result:=Designer.GetRenderer;
    end
  else if WSFormAllocated then
    begin
    //Writeln('WSForm renderer');
    Result:=WSForm.Renderer;
    end
  else
    begin
    //Writeln('Nil renderer');
    Result:=nil;
    end
end;

function TFresnelCustomForm.GetCaption: TFresnelCaption;
begin
  if WSFormAllocated then
    FCaption:=WSForm.Caption;
  Result:=FCaption;
end;

function TFresnelCustomForm.GetFormHeight: TFresnelLength;
begin
  Result:=FFormBounds.Height;
end;

function TFresnelCustomForm.GetFormLeft: TFresnelLength;
begin
  Result:=FFormBounds.Left;
end;

function TFresnelCustomForm.GetFormTop: TFresnelLength;
begin
  Result:=FFormBounds.Top;
end;

function TFresnelCustomForm.GetFormWidth: TFresnelLength;
begin
  Result:=FFormBounds.Width;
end;

function TFresnelCustomForm.GetWSForm: TFresnelWSForm;
begin
  if FWSForm=nil then
    CreateWSForm;
  Result:=FWSForm;
end;

procedure TFresnelCustomForm.SetDesigner(AValue: IFresnelFormDesigner);
begin
  if FDesigner=AValue then Exit;
  FDesigner:=AValue;
end;

procedure TFresnelCustomForm.SetCaption(AValue: TFresnelCaption);
begin
  if FCaption=AValue then exit;
  FCaption:=AValue;
  if WSFormAllocated then
    WSForm.Caption:=AValue;
end;

procedure TFresnelCustomForm.SetFormBounds(AValue: TFresnelRect);
begin
  if Designer<>nil then
    AValue.SetRect(AValue.GetRect); // round
  if AValue.Right<AValue.Left then
    AValue.Right:=AValue.Left;
  if AValue.Bottom<AValue.Top then
    AValue.Bottom:=AValue.Top;
  if FFormBounds=AValue then exit;
  //FLLog(etDebug,['TFresnelCustomForm.SetFormBounds ',ToString,' ',AValue.ToString]);
  FFormBounds:=AValue;
  if Designer<>nil then
    Designer.SetDesignerFormBounds(Self,FFormBounds.GetRect)
  else if WSFormAllocated then
    WSForm.FormBounds:=AValue;
end;

procedure TFresnelCustomForm.SetFormHeight(const AValue: TFresnelLength);
begin
  if FormHeight=AValue then exit;
  FormBounds:=TFresnelRect.Create(FFormBounds.Left,FFormBounds.Top,FFormBounds.Right,FFormBounds.Top+AValue);
end;

procedure TFresnelCustomForm.SetFormLeft(const AValue: TFresnelLength);
begin
  if FormLeft=AValue then exit;
  FormBounds:=TFresnelRect.Create(AValue,FFormBounds.Top,AValue+FFormBounds.Width,FFormBounds.Bottom);
end;

procedure TFresnelCustomForm.SetFormTop(const AValue: TFresnelLength);
begin
  if FormTop=AValue then exit;
  FormBounds:=TFresnelRect.Create(FFormBounds.Left,AValue,FFormBounds.Right,AValue+FFormBounds.Height);
end;

procedure TFresnelCustomForm.SetFormWidth(const AValue: TFresnelLength);
begin
  if FormTop=AValue then exit;
  FormBounds:=TFresnelRect.Create(FFormBounds.Left,FFormBounds.Top,FFormBounds.Left+AValue,FFormBounds.Bottom);
end;

procedure TFresnelCustomForm.SetVisible(const AValue: boolean);
begin
  if FVisible=AValue then Exit;
  FVisible:=AValue;
  if [csDestroying,csLoading,csDesigning]*ComponentState<>[] then
    exit;
  if FVisible then
    Show
  else
    Hide;
end;

procedure TFresnelCustomForm.ChildDestroying(El: TFresnelElement);
begin
  inherited ChildDestroying(El);
  if Application<>nil then
    Application.ElementDestroying(El);
end;

procedure TFresnelCustomForm.DoCreate;
begin
  if Assigned(OnCreate) then
    OnCreate(Self);
end;

procedure TFresnelCustomForm.DoDestroy;
begin
  if Assigned(OnDestroy) then
    OnDestroy(Self);
end;

procedure TFresnelCustomForm.Loaded;
begin
  //FLLog(etDebug,['TFresnelCustomForm.Loaded ',ToString]);
  inherited Loaded;
  if Visible then
    Show
  else
    Hide;
end;

procedure TFresnelCustomForm.SetWSForm(const AValue: TFresnelWSForm);
begin
  if FWSForm=AValue then exit;
  if FWSForm<>nil then
  begin
    Disconnecting;
    FreeAndNil(FWSForm);
  end;
  FWSForm:=AValue;
  if FWSForm<>nil then
    FreeNotification(FWSForm);
end;

procedure TFresnelCustomForm.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FWSForm=AComponent then
    begin
      Disconnecting;
      FWSForm:=nil;
    end;
  end;
end;

procedure TFresnelCustomForm.OnQueuedLayout(Data: Pointer);
begin
  //FLLog(etDebug,['TFresnelCustomForm.OnQueuedLayout ',Name,':',ClassName,' LayoutQueued=',BoolToStr(LayoutQueued,true)]);
  if not LayoutQueued then exit;
  try
    ApplyCSS;
    //Layouter.WriteLayoutTree;
    //FLLog(etDebug,['TFresnelCustomForm.OnQueuedLayout ',Name,':',ClassName,' After Layouter.Apply, Invalidate...']);
    Invalidate;
  finally
    Exclude(FFormStates,fsLayoutQueued);
  end;
end;

procedure TFresnelCustomForm.ProcessResource;
begin
  if not InitResourceComponent(Self, TFresnelForm) then
    FLLog(etDebug,rsFormResourceSNotFoundForResourcelessFormsCreateNew, [ClassName]);
end;

procedure TFresnelCustomForm.SetLayoutQueued(const AValue: boolean);
begin
  if LayoutQueued=AValue then Exit;
  if csDestroying in ComponentState then exit;
  //if csDestroyingHandle in Form.ControlState then exit;
  if AValue then
  begin
    Include(FFormStates,fsLayoutQueued);
    Application.QueueAsyncCall(@OnQueuedLayout,nil);
  end else begin
    Exclude(FFormStates,fsLayoutQueued);
  end;
end;

constructor TFresnelCustomForm.Create(AOwner: TComponent);
begin
  GlobalNameSpace.BeginWrite;
  try
    CreateNew(aOwner);
    if (ClassType <> TFresnelForm) and (ClassType<>TFresnelCustomForm)
        and not (csDesigning in ComponentState) then
    begin
      ProcessResource;
    end;
  finally
    GlobalNameSpace.EndWrite;
  end;
end;

constructor TFresnelCustomForm.CreateNew(AOwner: TComponent);
begin
  if Application=nil then
    raise EFresnel.Create('Fresnel.Forms.Application=nil, check if unit Fresnel is in the program uses section');
  inherited Create(AOwner);
  FVisible:=false;
  FVPApplication:=Application;
  Layouter:=TViewportLayouter.Create(nil);
  TViewportLayouter(Layouter).Viewport:=Self;
  FormManager.AddForm(Self);
end;

destructor TFresnelCustomForm.Destroy;
begin
  FormManager.RemoveForm(Self);
  Layouter.Free;
  Layouter:=nil;
  WSForm:=nil;
  inherited Destroy;
end;

procedure TFresnelCustomForm.AfterConstruction;
begin
  DoCreate;
  inherited AfterConstruction;
end;

procedure TFresnelCustomForm.BeforeDestruction;
begin
  inherited BeforeDestruction;
  DoDestroy;
end;

procedure TFresnelCustomForm.DomChanged;
begin
  LayoutQueued:=true;
  inherited DomChanged;
end;

procedure TFresnelCustomForm.Hide;
begin
  //FLLog(etDebug,'TFresnelCustomForm.Hide '+ToString);
  if (Designer<>nil)
      or ([csLoading,csDesigning]*ComponentState<>[])
      or not WSFormAllocated then
    FVisible:=false
  else
    WSForm.Visible:=false;
end;

procedure TFresnelCustomForm.Show;
begin
  //FLLog(etDebug,'TFresnelCustomForm.Show '+ToString);
  if (Designer<>nil) or ([csLoading,csDesigning,csDestroying]*ComponentState<>[]) then
    FVisible:=true
  else
    WSForm.Visible:=true;
end;

procedure TFresnelCustomForm.Invalidate;
begin
  if ([csLoading,csDestroying]*ComponentState<>[]) then
    exit;
  if Designer<>nil then
    Designer.InvalidateRect(Self,Rect(0,0,Ceil(Width),Ceil(Height)),false)
  else if WSFormAllocated then
    WSForm.Invalidate;
end;

procedure TFresnelCustomForm.InvalidateRect(const aRect: TFresnelRect);
begin
  if ([csLoading,csDestroying]*ComponentState<>[]) then
    exit;
  if Designer<>nil then
    Designer.InvalidateRect(Self,aRect.GetRect,false)
  else if WSFormAllocated then
    WSForm.InvalidateRect(aRect);
end;

function TFresnelCustomForm.IsDrawing: boolean;
begin
  Result:=fsDrawing in FFormStates;
end;

procedure TFresnelCustomForm.CreateWSForm;
begin
  if WSFormAllocated then exit;
  if csDestroying in ComponentState then exit;

  // consistency checks
  //FLLog(etDebug,['TFresnelCustomForm.CreateWSForm ',ToString]);
  if Designer<>nil then
    raise Exception.Create('TFresnelCustomForm.CreateWSForm Designer<>nil');
  if Parent = Self then
    raise Exception.Create('TFresnelCustomForm.CreateWSForm Parent = Self')
  else if (Parent <> nil) then
    raise Exception.Create('TFresnelCustomForm.CreateWSForm ToDo 20230915221832');

  // create
  WidgetSet.CreateWSForm(Self);
  if not (csLoading in ComponentState) then
    WSForm.Visible:=Visible;
end;

function TFresnelCustomForm.WSFormAllocated: Boolean;
begin
  Result:=FWSForm<>nil;
end;

procedure TFresnelCustomForm.WSDraw;
begin
  //FLLog(etDebug,'TFresnelCustomForm.WSDraw (%s)',[ToString]);
  //FLLog(etDebug,'TFresnelCustomForm.WSDraw Have renderer: %b',[Assigned(Renderer)]);
  if fsDrawing in FFormStates then
    raise Exception.Create('20250228113612 already drawing');
  Include(FFormStates,fsDrawing);
  try
    LayoutQueued:=false;
    if DomModified then
    begin
      ApplyCSS;
    end;
    Renderer.Draw(Self);
  finally
    Exclude(FFormStates,fsDrawing);
  end;
end;

procedure TFresnelCustomForm.WSResize(const NewFormBounds: TFresnelRect;
  NewWidth, NewHeight: TFresnelLength);
begin
  if (FFormBounds=NewFormBounds) and (Width=NewWidth) and (Height=NewHeight) then exit;
  //FLLog(etDebug,['TFresnelCustomForm.WSResize ',ToString,' OldForm=',FFormBounds.ToString,' NewForm=',NewFormBounds.ToString,' OldWH=',FloatToStr(Width),',',FloatToStr(Height),' NewWH=',FloatToStr(NewWidth),',',FloatToStr(NewHeight)]);
  FFormBounds:=NewFormBounds;
  Width:=NewWidth;
  Height:=NewHeight;
  LayoutQueued:=true;
end;

{ TFresnelFormCreateEvent }

class function TFresnelFormCreateEvent.FresnelEventID: TEventID;
begin
  Result:=evtOnFormCreate;
end;

{ TFresnelFormDestroyEvent }

class function TFresnelFormDestroyEvent.FresnelEventID: TEventID;
begin
  Result:=evtOnFormDestroy;
end;

{ TFresnelAfterProcessMessagesEvent }

class function TFresnelAfterProcessMessagesEvent.FresnelEventID: TEventID;
begin
  Result:=evtAfterProcessMessages;
end;

{ TFresnelApplicationIdleEvent }

class function TFresnelApplicationIdleEvent.FresnelEventID: TEventID;
begin
  Result:=evtIdle;
end;

{ TFresnelApplicationIdleEndEvent }

class function TFresnelApplicationIdleEndEvent.FresnelEventID: TEventID;
begin
  Result:=evtIdleEnd;
end;

{ TFresnelApplicationActivateEvent }

class function TFresnelApplicationActivateEvent.FresnelEventID: TEventID;
begin
  Result:=evtActivate;
end;

{ TFresnelApplicationDeActivateEvent }

class function TFresnelApplicationDeActivateEvent.FresnelEventID: TEventID;
begin
  Result:=evtDeActivate;
end;

{ TFresnelApplicationMinimizeEvent }

class function TFresnelApplicationMinimizeEvent.FresnelEventID: TEventID;
begin
  Result:=evtMinimize;
end;

{ TFresnelApplicationMaximizeEvent }

class function TFresnelApplicationMaximizeEvent.FresnelEventID: TEventID;
begin
  Result:=evtMaximize;
end;

{ TFresnelApplicationRestoreEvent }

class function TFresnelApplicationRestoreEvent.FresnelEventID: TEventID;
begin
  Result:=evtRestore;
end;


{ TFresnelFormManager }

procedure TFresnelFormManager.SetMainForm(AValue: TFresnelCustomForm);
begin
  if FMainForm=AValue then Exit;
  FMainForm:=AValue;
end;

procedure TFresnelFormManager.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation=opRemove) and Assigned(FList) then
     FList.Remove(aComponent);
end;

constructor TFresnelFormManager.Create(aOwner: TComponent);
begin
  Inherited;
  FList:=TFPObjectList.Create(False);
end;

destructor TFresnelFormManager.Destroy;

var
  I : Integer;

begin
  For I:=0 to FList.Count-1 do
    TFresnelCustomForm(FList[I]).RemoveFreeNotification(Self);
  FreeAndNil(FList);
  inherited Destroy;
end;

procedure TFresnelFormManager.AddForm(aForm: TFresnelCustomForm);
begin
  if Assigned(FList) then
    FList.Add(aForm);
  aForm.FreeNotification(Self);
  If FMainForm=Nil then
    FMainForm:=aForm;
end;

procedure TFresnelFormManager.RemoveForm(aForm: TFresnelCustomForm);
begin
  if Assigned(FList) then
    FList.Remove(aForm);
  aForm.RemoveFreeNotification(Self);
  If (aForm=FMainForm) then
    begin
    FMainForm:=Nil;
    if (FList.Count>0) then
      FMainForm:=TFresnelCustomForm(FList[0]);
    end;
end;

{ TFresnelBaseApplication }

procedure TFresnelBaseApplication.SetHookFresnelLog(AValue: Boolean);
begin
  if HookFresnelLog=AValue then Exit;
  if aValue then
    TFresnelComponent._LogHook:=@DoFresnelLog
  else
    TFresnelComponent._LogHook:=Nil;
end;

function TFresnelBaseApplication.GetHoverElements: TFresnelElementArray;
begin
  Result:=copy(FHoverElements);
end;

procedure TFresnelBaseApplication.SetHoverElements(
  const AValue: TFresnelElementArray);
var
  i: SizeInt;
  OldEls: TFresnelElementArray;
  El: TFresnelElement;
begin
  if FHoverElements=AValue then Exit;
  if length(FHoverElements)=length(AValue) then
  begin
    i:=length(FHoverElements)-1;
    while (i>=0)and (FHoverElements[i]=AValue[i]) do dec(i);
    if i<0 then exit;
  end;
  OldEls:=FHoverElements;
  FHoverElements:=AValue;
  //writeln('TFresnelBaseApplication.SetHoverElements START ',Name,':',ClassName);
  // notify old hover elements
  for i:=0 to length(OldEls)-1 do
  begin
    El:=OldEls[i];
    //writeln('TFresnelBaseApplication.SetHoverElements Old[',i,']=',El.GetPath);
    if IsHoverElement(El) then continue;
    El.CSSPseudoClass[fcpcHover]:=false;
  end;
  // notify new hover elements
  for i:=0 to length(FHoverElements)-1 do
  begin
    El:=FHoverElements[i];
    //writeln('TFresnelBaseApplication.SetHoverElements New[',i,']=',El.GetPath);
    El.CSSPseudoClass[fcpcHover]:=true;
  end;
end;

function TFresnelBaseApplication.GetMouseDownElement(out PageXY: TFresnelPoint): TFresnelElement;
begin
  Result:=fMouseDownElement;
  PageXY:=fMouseDownPageXY;
end;

procedure TFresnelBaseApplication.SetMouseDownElement(El: TFresnelElement;
  const PageXY: TFresnelPoint);
begin
  fMouseDownElement:=El;
  fMouseDownPageXY:=PageXY;
end;

procedure TFresnelBaseApplication.DoFresnelLog(aType: TEventType; const Msg: String);
begin
  //writeln('TFresnelBaseApplication.DoFresnelLog ',aType,' ',Msg);
  Log(aType,Msg);
end;

function TFresnelBaseApplication.GetHookFresnelLog: Boolean;
begin
  Result:=(TFresnelComponent._LogHook=@DoFresnelLog);
end;

function TFresnelBaseApplication.CreateEventDispatcher(aDefaultSender: TObject): TFresnelEventDispatcher;
begin
  Result:=TFresnelEventDispatcher.Create(aDefaultSender);
end;

procedure TFresnelBaseApplication.DoHandleAsyncCalls;
begin
  if FAsyncCall=nil then exit;
  FAsyncCall.ProcessQueue;
end;

class procedure TFresnelBaseApplication.RegisterApplicationEvents;

  Procedure R(aClass : TFresnelEventClass);
  begin
     TFresnelEventDispatcher.FresnelRegistry.RegisterEventWithID(aClass.FresnelEventID,aClass);
  end;

begin
  R(TFresnelAfterProcessMessagesEvent);
end;

procedure TFresnelBaseApplication.DoProcessMessages;
begin
  WidgetSet.AppProcessMessages;
end;

procedure TFresnelBaseApplication.ShowMainForm;
begin
  FLLog(etDebug,'ShowMainForm');
  If FormManager.MainForm=nil then
    raise Exception.Create('No main form available');
  FormManager.MainForm.Show;
end;

procedure TFresnelBaseApplication.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if fMouseDownElement=AComponent then
      fMouseDownElement:=nil;
  end;
end;

constructor TFresnelBaseApplication.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  if Application<>nil then
    raise Exception.Create('TBaseFresnelApplication.Create BaseFresnelApplication<>nil');
  Application:=Self;
  FAsyncCall:=TAsyncCallQueues.Create;
  FAsyncCall.WakeMainThreadOnCalls:=True;
  FEventDispatcher:=CreateEventDispatcher(Self);
end;

destructor TFresnelBaseApplication.Destroy;
begin
  FreeAndNil(FEventDispatcher);
  FreeAndNil(FAsyncCall);
  inherited Destroy;
  Application:=nil;
end;

procedure TFresnelBaseApplication.ProcessMessages;
begin
  //FLLog(etDebug,'ProcessMessages');
  DoProcessMessages;
  DoHandleAsyncCalls;
  FEventDispatcher.DispatchEvent(evtAfterProcessMessages);
end;

procedure TFresnelBaseApplication.QueueAsyncCall(const aMethod: TDataEvent;
  aData: Pointer);
begin
  FAsyncCall.QueueAsyncCall(aMethod,aData);
end;

procedure TFresnelBaseApplication.RemoveAsyncCalls(const aObject: TObject);
begin
  FAsyncCall.RemoveAsyncCalls(aObject);
end;

procedure TFresnelBaseApplication.CreateForm(aClass: TComponentClass; out FormVariable);
begin
  TComponent(FormVariable):=aClass.Create(Self);
end;

procedure TFresnelBaseApplication.CreateFormNew(aClass: TComponentClass; out FormVariable);
begin
  if aClass.InheritsFrom(TFresnelCustomForm) then
    TComponent(FormVariable):=TFresnelCustomFormClass(aClass).CreateNew(Self)
  else
    TComponent(FormVariable):=aClass.Create(Self)
end;

procedure TFresnelBaseApplication.ElementDestroying(El: TFresnelElement);
var
  i: SizeInt;
begin
  for i:=length(FHoverElements)-1 downto 0 do
    if FHoverElements[i]=El then
      System.Delete(FHoverElements,i,1);
end;

function TFresnelBaseApplication.AddEventListener(aID: TEventID;
  const aHandler: TFresnelEventHandler): Integer;
begin
  Result:=FEventDispatcher.RegisterHandler(aHandler,aID).ID;
end;

function TFresnelBaseApplication.AddEventListener(const aName: TEventName;
  const aHandler: TFresnelEventHandler): Integer;
begin
  Result:=FEventDispatcher.RegisterHandler(aHandler,aName).ID;
end;

function TFresnelBaseApplication.IsHoverElement(El: TFresnelElement): boolean;
var
  i: Integer;
begin
  for i:=0 to length(FHoverElements)-1 do
    if fHoverElements[i]=El then
      exit(true);
  Result:=false;
end;


initialization
  TFresnelBaseApplication.RegisterApplicationEvents;
finalization
  _FormManager.Free;

end.

