{
 *****************************************************************************
  This file is part of Fresnel.

  See the file COPYING.modifiedLGPL.txt, included in this distribution,
  for details about the license.
 *****************************************************************************

}
unit Fresnel.Layouter;

{$mode ObjFPC}{$H+}
{$Interfaces CORBA}
{$IF FPC_FULLVERSION>30300}
{$WARN 6060 off} // Case statement does not handle all possible cases
{$ENDIF}

interface

uses
  Classes, SysUtils, Math, fpCSSResolver, fpCSSResParser,
  Fresnel.Classes, Fresnel.DOM, Fresnel.Controls;

type
  EFresnelLayout = class(Exception)
  end;

type
  TUsedLayoutNode = class;

  { TFLNodeLayouter }

  TFLNodeLayouter = class(TComponent)
  protected
    type
      TLayoutSize = record
        Mode: TFresnelLayoutMode;
        MaxWidth, MaxHeight: TFresnelLength;
        ContentSize: TFresnelPoint;
      end;
      TLayoutSizeArray = array of TLayoutSize;
    var
      FLastLayoutSizes: TLayoutSizeArray;
  public
    Node: TUsedLayoutNode;
    procedure Apply; virtual;
    procedure Init; virtual; // called after layout node and parent was updated, CSS string values were computed, no used values yet
    procedure DeductUsedLengths(NoChildren: boolean); virtual;
    procedure ClearCachedLayoutSizes; virtual;
    function GetCachedLayoutSize(aMode: TFresnelLayoutMode; aMaxWidth, aMaxHeight: TFresnelLength; out aSize: TFresnelPoint): boolean; virtual;
    procedure AddCachedLayoutSize(aMode: TFresnelLayoutMode; aMaxWidth, aMaxHeight: TFresnelLength; const aSize: TFresnelPoint); virtual;
    function ComputeLayoutContent(aMode: TFresnelLayoutMode; aMaxWidth, aMaxHeight: TFresnelLength; Commit: boolean): TFresnelPoint; virtual; abstract;
    function GetIntrinsicContentSize(aMode: TFresnelLayoutMode; aMaxWidth: TFresnelLength=NaN;
      aMaxHeight: TFresnelLength=NaN): TFresnelPoint; virtual;
    function GetViewport: TFresnelViewport;
  end;
  TFLNodeLayouterClass = class of TFLNodeLayouter;

  { TUsedLayoutNode }

  TUsedLayoutNode = class(TFresnelLayoutNode)
  public
    Layouter: TFLNodeLayouter;
    procedure ApplyMinMaxHeight; virtual;
    procedure ApplyMinMaxWidth; virtual;
    procedure ComputeUsedLengths(NoChildren: boolean); virtual;
    procedure DeductUsedLengths(NoChildren: boolean); virtual;
    function GetIntrinsicContentSize(aMode: TFresnelLayoutMode; aMaxWidth: TFresnelLength=NaN;
      aMaxHeight: TFresnelLength=NaN): TFresnelPoint; override;
    destructor Destroy; override;
  end;

  { TFLLineLayouter - abstract base node layouter for TFLFlowLayouter and TFLFlexLayouter }

  TFLLineLayouter = class(TFLNodeLayouter)
  protected
    type

      { TLineItem - computed data for one element }

      TLineItem = class
      public
        Node: TUsedLayoutNode;
        // computed layout (only static, relative and sticky elements)
        StaticLeft: TFresnelLength;
        StaticTop: TFresnelLength;
        ContentBoxWidth: TFresnelLength;
        ContentBoxHeight: TFresnelLength;
      end;
      TLineItemClass = class of TLineItem;

  protected
    FLineItems: TFPList; // list of TLineItem
    FAbsoluteItems: TFPList; // list of TLineItem
    procedure PlaceLineItems; virtual;
    function PlaceAbsoluteItem(ChildNode: TUsedLayoutNode; aMode: TFresnelLayoutMode;
                  aMaxWidth, aMaxHeight: TFresnelLength; const DefaultPos: TFresnelPoint;
                  Commit: boolean): TFresnelPoint; virtual;
    function AddAbsoluteItem(ChildNode: TUsedLayoutNode; NodeClass: TLineItemClass): TLineItem; virtual;
    function AddLineItem(ChildNode: TUsedLayoutNode; NodeClass: TLineItemClass): TLineItem; virtual;
    procedure ClearAbsoluteItems; virtual;
    procedure ClearLineItems; virtual;
    procedure ComputeChildAttributes(Item: TLineItem; El: TFresnelElement); virtual;
  public
    destructor Destroy; override;
  end;

  { TFLFlowLayouter - flow layouter }

  TFLFlowLayouter = class(TFLLineLayouter)
  protected
    type

      { TFlowItem }

      TFlowItem = class(TLineItem)
      public
      end;

  protected
    // current line
    FLineBorderBoxHeight: TFresnelLength;
    FLineBorderBoxLeft: TFresnelLength; // border box position of last element relative to parent ContentBox
    FLineBorderBoxRight: TFresnelLength;
    FLineBorderBoxTop: TFresnelLength;
    FLineBorderBoxBottom: TFresnelLength;
    FLineMarginLeft: TFresnelLength; // margin width
    FLineMarginRight: TFresnelLength;
    FLineMarginTop: TFresnelLength;
    FLineMarginBottom: TFresnelLength;
    FLineFirstAbsoluteIndex: integer;
    FLastLineBorderBoxBottom: TFresnelLength;
    FLastLineMarginBottom: TFresnelLength;
    procedure StartLine; virtual;
    procedure EndLine(Commit: boolean); virtual;
    procedure PlaceLineItems; override;
  public
    procedure DeductUsedLengths(NoChildren: boolean); override;
    function ComputeLayoutContent(aMode: TFresnelLayoutMode; aMaxWidth, aMaxHeight: TFresnelLength; Commit: boolean): TFresnelPoint; override;
  end;

  { TFLFlexLayouter }

  TFLFlexLayouter = class(TFLLineLayouter)
  private
    FGap: array[boolean] of TFresnelLength; // true for horizontal
  protected
    type

      { TFlexItem }

      TFlexItem = class(TLineItem)
      public
        Basis: TFresnelLength; // content-width/height
        Grow: TFresnelLength;
        Shrink: TFresnelLength;
        MainContentSize: TFresnelLength;
        MainFrameLT, MainFrameRB: TFresnelLength; // margin+border+padding in main direction LT=left/top, RB=right/bottom
        CrossFrameLT, CrossFrameRB: TFresnelLength; // margin+border+padding in cross direction LT=left/top, RB=right/bottom
        CrossContentSize: TFresnelLength;
        AlignSelf, AlignSelfSub: TCSSNumericalID;
      end;

  protected
    FlexDirection: TCSSNumericalID;
    FlexWrap: TCSSNumericalID;
    JustifyContent, JustifyContentSub: TCSSNumericalID;
    AlignItems, AlignItemsSub: TCSSNumericalID;
    MainIsRow: boolean; // flex-direction is row or row-reverse
    MainIsReverse: boolean; // flex-direction is row-reverse or column-reverse
    procedure StartLine; virtual;
    procedure EndLine(MaxMainSize, MainGap: TFresnelLength;
      Commit: boolean; var NewContentSize: TFresnelPoint); virtual;
    procedure FlexLineMainDirection(MaxMainSize, MainGap: TFresnelLength; Commit: boolean;
      var NewContentSize: TFresnelPoint); virtual;
    procedure FlexLineCrossDirection(Commit: boolean;
      var NewContentSize: TFresnelPoint); virtual;
    procedure PlaceLineItems; override;
    procedure ComputeChildAttributes(Item: TLineItem; El: TFresnelElement); override;
  public
    procedure Init; override;
    function GetComputedGap(IsHorizontal: boolean): TFresnelLength;
    function ComputeLayoutContent(aMode: TFresnelLayoutMode; aMaxWidth, aMaxHeight: TFresnelLength; Commit: boolean): TFresnelPoint; override;
  end;

  { TFLGridLayouter }

  TFLGridLayouter = class(TFLNodeLayouter)
  public
    function ComputeLayoutContent(aMode: TFresnelLayoutMode; aMaxWidth, aMaxHeight: TFresnelLength; Commit: boolean): TFresnelPoint; override;
  end;

  { TViewportLayouter }

  TViewportLayouter = class(TFresnelLayouter)
  private
    FViewport: TFresnelViewport;
    procedure SetViewport(const AValue: TFresnelViewport);
  protected
    procedure ErrorLayout(const ID: int64; const Msg: string); virtual;
    procedure Layout(El: TFresnelElement); virtual;
    function CreateLayoutNode(El: TFresnelElement): TUsedLayoutNode; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    procedure Apply; override;
    procedure UpdateLayouter(El: TFresnelElement; LNode: TUsedLayoutNode); virtual;
    function GetContainer(El: TFresnelElement): TFresnelElement; virtual;
    procedure UpdateLayoutNode(El: TFresnelElement); virtual; // called after basic CSS properties were computed, create the layout nodes
    procedure SortStackingContext(LNode: TUsedLayoutNode); virtual; // apply z-index
    procedure WriteRenderingTree; // for debugging: write tree information to log
    property Viewport: TFresnelViewport read FViewport write SetViewport;
    // needs:
    // viewport width, height, DPI, font size default, font size min
    // scrollbar width, height
    // current scroll x,y in ViewPort
    // measure text width

    // produces:
    // layout rendering tree
    // boxes
    // margin, border, padding
    // left, top, width, height
    // clipping: left, top, right, bottom
  end;

function CompareLayoutNodesZIndex(Item1, Item2{$IF FPC_FULLVERSION>=30301}, {%H-}Context{$ENDIF}: Pointer): integer;

implementation

function CompareLayoutNodesZIndex(Item1, Item2{$IF FPC_FULLVERSION>=30301}, {%H-}Context{$ENDIF}: Pointer): integer;
var
  Node1: TUsedLayoutNode absolute Item1;
  Node2: TUsedLayoutNode absolute Item2;
begin
  if Node1.ZIndex>Node2.ZIndex then
    Result:=1
  else if Node1.ZIndex<Node2.ZIndex then
    Result:=-1
  else
    Result:=0;
end;

{ TFLNodeLayouter }

procedure TFLNodeLayouter.Apply;
var
  MaxWidth, MaxHeight: TFresnelLength;
  Size: TFresnelPoint;
begin
  MaxWidth:=Node.Width;
  if IsNan(MaxWidth) then
    MaxWidth:=Node.MaxWidth;
  MaxHeight:=Node.Height;
  if IsNan(MaxHeight) then
    MaxHeight:=Node.MaxWidth;
  Size:=ComputeLayoutContent(flmMaxWidth,MaxWidth,MaxHeight,true);
  if IsNan(Node.Width) then
    Node.Width:=Size.X;
  if IsNan(Node.Height) then
    Node.Height:=Size.Y;
end;

procedure TFLNodeLayouter.Init;
begin
  ClearCachedLayoutSizes;
end;

procedure TFLNodeLayouter.DeductUsedLengths(NoChildren: boolean);
begin
  if NoChildren then ;
end;

procedure TFLNodeLayouter.ClearCachedLayoutSizes;
begin
  SetLength(FLastLayoutSizes,0);
end;

function TFLNodeLayouter.GetCachedLayoutSize(aMode: TFresnelLayoutMode; aMaxWidth,
  aMaxHeight: TFresnelLength; out aSize: TFresnelPoint): boolean;

  function CheckMax(OldMax, NewMax, OldResult: TFresnelLength): boolean;
  begin
    // true if OldMax is the same as NewMax
    // or NewMax is between OldMax and OldResult
    if IsNan(NewMax) then
    begin
      Result:=IsNan(OldMax);
    end else begin
      if IsNan(OldMax) then
      begin
        if NewMax>=OldResult then
        begin
          // last time no MaxWidth, the result was 100
          // this time MaxWidth 110 => the result still fits
          Result:=true;
        end else begin
          // last time no MaxWidth, the result was 100
          // this time MaxWidth 90 => the result can't be used
          Result:=false;
        end;
      end else begin
        if CompareValue(NewMax,OldMax)=0 then
          // same maxwidth
          Result:=true
        else if NewMax>OldMax then
        begin
          if NewMax<=OldResult then
          begin
            // for example:
            // last time with a MaxWidth of 100, the result was 120.
            // this time with a MaxWidth of 110 => the result still fits
            Result:=true;
          end else begin
            // for example:
            // last time with a MaxWidth of 100, the result was 120.
            // this time with a MaxWidth of 130 => the result can't be used
            Result:=false;
          end;
        end else if NewMax>=OldResult then begin
          // for example:
          // last time with a MaxWidth of 100, the result was 80.
          // this time with a MaxWidth of 90 => the result still fits
          Result:=true;
        end else begin
          // for example:
          // last time with a MaxWidth of 100, the result was 80.
          // this time with a MaxWidth of 70 => the result can't be used
          Result:=false;
        end;
      end;
    end;
  end;

var
  i: Integer;
begin
  for i:=0 to length(FLastLayoutSizes)-1 do
  with FLastLayoutSizes[i] do begin
    if (aMode<>Mode) then continue;
    case aMode of
    flmMinWidth:
      if not CheckMax(MaxHeight,aMaxHeight,ContentSize.Y) then continue;
    flmMinHeight:
      if not CheckMax(MaxWidth,aMaxWidth,ContentSize.X) then continue;
    flmMaxWidth,flmMaxHeight:
      begin
        if not CheckMax(MaxWidth,aMaxWidth,ContentSize.X) then continue;
        if not CheckMax(MaxHeight,aMaxHeight,ContentSize.Y) then continue;
      end;
    end;
    aSize:=ContentSize;
    exit(true);
  end;
  aSize:=Default(TFresnelPoint);
  Result:=false;
end;

procedure TFLNodeLayouter.AddCachedLayoutSize(aMode: TFresnelLayoutMode; aMaxWidth,
  aMaxHeight: TFresnelLength; const aSize: TFresnelPoint);
var
  l: integer;
begin
  l:=length(FLastLayoutSizes);
  SetLength(FLastLayoutSizes,l+1);
  with FLastLayoutSizes[l] do begin
    Mode:=aMode;
    MaxWidth:=aMaxWidth;
    MaxHeight:=aMaxHeight;
    ContentSize:=aSize;
  end;
end;

function TFLNodeLayouter.GetIntrinsicContentSize(aMode: TFresnelLayoutMode;
  aMaxWidth: TFresnelLength; aMaxHeight: TFresnelLength): TFresnelPoint;
begin
  case aMode of
    flmMinWidth:
      aMaxWidth:=NaN;
    flmMinHeight:
      aMaxHeight:=NaN;
  end;
  if GetCachedLayoutSize(aMode,aMaxWidth,aMaxHeight,Result) then
    exit;
  Result:=ComputeLayoutContent(aMode,aMaxWidth,aMaxHeight,false);
  AddCachedLayoutSize(aMode,aMaxWidth,aMaxHeight,Result);
end;

function TFLNodeLayouter.GetViewport: TFresnelViewport;
begin
  Result:=Node.Element.Viewport;
end;

{ TUsedLayoutNode }

procedure TUsedLayoutNode.ApplyMinMaxHeight;
begin
  if IsNan(Height) then exit;
  if Height<0 then Height:=0;
  if (not IsNan(MaxHeight)) and (Height>MaxHeight) then
    Height:=MaxHeight;
  if (not IsNan(MinHeight)) and (Height<MinHeight) then
    Height:=MinHeight;
end;

procedure TUsedLayoutNode.ApplyMinMaxWidth;
begin
  if IsNan(Width) then exit;
  if (not IsNan(MaxWidth)) and (Width>MaxWidth) then
    Width:=MaxWidth;
  if Width<0 then Width:=0;
  if (not IsNan(MinWidth)) and (Width<MinWidth) then
    Width:=MinWidth;
end;

procedure TUsedLayoutNode.ComputeUsedLengths(NoChildren: boolean);

  function ComputeLength(Attr: TFresnelCSSAttribute): TFresnelLength;
  begin
    Result:=Element.GetComputedLength(Attr,false,NoChildren);
  end;

  function ComputeDistance(Attr: TFresnelCSSAttribute): TFresnelLength;
  begin
    Result:=Element.GetComputedLength(Attr,true,NoChildren);
  end;

  function GetContentSize(Attr: TFresnelCSSAttribute; IsWidth: boolean): TFresnelLength;
  var
    AttrID: TCSSNumericalID;
    Complete: boolean;
    s: String;
    aComp: TCSSResCompValue;
  begin
    Result:=NaN;
    AttrID:=CSSRegistry.FresnelAttrs[Attr].Index;
    s:=Element.GetCSSString(AttrID,not NoChildren,Complete);
    aComp.EndP:=PChar(s);
    if (not Element.Resolver.ReadComp(aComp)) then
    begin
      // empty or invalid token -> use default: auto
      exit;
    end else if (aComp.Kind=rvkKeyword) and (aComp.KeywordID=CSSRegistry.kwAuto) then
    begin
      exit;
    end;
    Result:=Element.GetComputedLength(aComp,AttrID,true,NoChildren);
    if IsNan(Result) then
      exit;
    case Element.ComputedBoxSizing of
    CSSRegistry.kwPaddingBox:
      if IsWidth then
      begin
        Result:=Result-PaddingLeft-PaddingRight;
      end else begin
        Result:=Result-PaddingTop-PaddingTop;
      end;
    CSSRegistry.kwBorderBox:
      if IsWidth then
      begin
        Result:=Result-BorderLeft-PaddingLeft-PaddingRight-BorderRight;
      end else begin
        Result:=Result-BorderTop-PaddingTop-PaddingBottom-BorderBottom;
      end;
    end;
    if Result<0 then Result:=0;
  end;

begin
  BorderLeft:=ComputeLength(fcaBorderLeftWidth);
  BorderTop:=ComputeLength(fcaBorderTopWidth);
  BorderRight:=ComputeLength(fcaBorderRightWidth);
  BorderBottom:=ComputeLength(fcaBorderBottomWidth);

  PaddingLeft:=ComputeLength(fcaPaddingLeft);
  PaddingTop:=ComputeLength(fcaPaddingTop);
  PaddingRight:=ComputeLength(fcaPaddingRight);
  PaddingBottom:=ComputeLength(fcaPaddingBottom);

  MarginLeft:=ComputeLength(fcaMarginLeft);
  MarginTop:=ComputeLength(fcaMarginTop);
  MarginRight:=ComputeLength(fcaMarginRight);
  MarginBottom:=ComputeLength(fcaMarginBottom);

  Left:=ComputeDistance(fcaLeft);
  Top:=ComputeDistance(fcaTop);
  Right:=ComputeDistance(fcaRight);
  Bottom:=ComputeDistance(fcaBottom);

  // compute width and height after border and padding
  MinWidth:=GetContentSize(fcaMinWidth,true);
  if IsNan(MinWidth) then MinWidth:=0;
  MinHeight:=GetContentSize(fcaMinHeight,true);
  if IsNan(MinHeight) then MinHeight:=0;
  MaxWidth:=GetContentSize(fcaMaxWidth,true);
  MaxHeight:=GetContentSize(fcaMaxHeight,true);
  Width:=GetContentSize(fcaWidth,true);
  Height:=GetContentSize(fcaHeight,true);

  DeductUsedLengths(NoChildren);

  {$IFDEF VerboseFresnelPlacing}
  writeln('TUsedLayoutNode.ComputeUsedLengths ',Element.GetPath,' NoChildren=',NoChildren,' Width=',FloatToCSSStr(Width),' Height=',FloatToCSSStr(Height),' MinWidth=',FloatToCSSStr(MinWidth),' MaxWidth=',FloatToCSSStr(MaxWidth));
  {$ENDIF}
end;

procedure TUsedLayoutNode.DeductUsedLengths(NoChildren: boolean);
var
  aContainerWidth, aContainerHeight: TFresnelLength;
begin
  if NoChildren then ;

  // apply min-width and max-width, min has higher precedence than max

  if (not IsNan(MinWidth)) and (not IsNan(MaxWidth)) and (MinWidth>=MaxWidth) then
  begin
    MaxWidth:=MinWidth;
    Width:=MinWidth;
  end;
  ApplyMinMaxWidth;

  if (not IsNan(MinHeight)) and (not IsNan(MaxHeight)) and (MinHeight>=MaxHeight) then
  begin
    MaxHeight:=MinHeight;
    Height:=MinHeight;
  end;
  ApplyMinMaxHeight;

  // deduce left, top, width from the other two and container width
  if Element.ComputedPosition in [CSSRegistry.kwAbsolute,CSSRegistry.kwFixed] then
  begin
    // left, top, right, bottom are used

    if IsNan(Width) then
    begin
      if (not IsNan(Left)) and (not IsNan(Right)) then
      begin
        // left & right -> width
        aContainerWidth:=Element.GetContainerContentWidth(true);
        if not IsNan(aContainerWidth) then
        begin
          Width:=aContainerWidth-Left-MarginLeft-BorderLeft-PaddingLeft
                                -Right-MarginRight-BorderRight-PaddingRight;
          ApplyMinMaxWidth;
        end;
      end;
    end else begin
      // width is set
      if IsNan(Left) then
      begin
        if not IsNan(Right) then
        begin
          // width & right -> left
          aContainerWidth:=Element.GetContainerContentWidth(true);
          if not IsNan(aContainerWidth) then
            Left:=aContainerWidth-Width-MarginLeft-BorderLeft-PaddingLeft
                                  -Right-MarginRight-BorderRight-PaddingRight;
        end;
      end else if IsNan(Right) then begin
        // width & left -> right
        aContainerWidth:=Element.GetContainerContentWidth(true);
        if not IsNan(aContainerWidth) then
          Right:=aContainerWidth-Width-Left-MarginLeft-BorderLeft-PaddingLeft
                                -MarginRight-BorderRight-PaddingRight;
      end;
    end;

    if IsNan(Height) then
    begin
      if (not IsNan(Top)) and (not IsNan(Bottom)) then
      begin
        // Top & Bottom -> Height
        aContainerHeight:=Element.GetContainerContentHeight(true);
        if not IsNan(aContainerHeight) then
        begin
          Height:=aContainerHeight-Top-MarginTop-BorderTop-PaddingTop
                                  -Bottom-MarginBottom-BorderBottom-PaddingBottom;
          ApplyMinMaxHeight;
        end;
      end;
    end else begin
      // Height is set
      if IsNan(Top) then
      begin
        if not IsNan(Bottom) then
        begin
          // Height & Bottom -> Top
          aContainerHeight:=Element.GetContainerContentHeight(true);
          if not IsNan(aContainerHeight) then
            Top:=aContainerHeight-Height-MarginTop-BorderTop-PaddingTop
                                 -Bottom-MarginBottom-BorderBottom-PaddingBottom;
        end;
      end else if IsNan(Bottom) then begin
        // Height & Top -> Bottom
        aContainerHeight:=Element.GetContainerContentHeight(true);
        if not IsNan(aContainerHeight) then
          Bottom:=aContainerHeight-Height-Top-MarginTop-BorderTop-PaddingTop
                                  -MarginBottom-BorderBottom-PaddingBottom;
      end;
    end;

  end else begin
    // position is static, relative or sticky
    // -> position will be computed
  end;

  if Layouter<>nil then
    Layouter.DeductUsedLengths(NoChildren);
end;

function TUsedLayoutNode.GetIntrinsicContentSize(aMode: TFresnelLayoutMode;
  aMaxWidth: TFresnelLength; aMaxHeight: TFresnelLength): TFresnelPoint;
begin
  if Layouter<>nil then
    Result:=Layouter.GetIntrinsicContentSize(aMode,aMaxWidth,aMaxHeight)
  else begin
    Result.X:=0;
    Result.Y:=0;
  end;
end;

destructor TUsedLayoutNode.Destroy;
begin
  FreeAndNil(Layouter);
  Container:=nil;
  inherited Destroy;
end;

{ TFLFlowLayouter }

procedure TFLFlowLayouter.StartLine;
begin
  ClearLineItems;
  if FLineItems=nil then
    FLineItems:=TFPList.Create;
  FLineBorderBoxHeight:=0;
  FLineBorderBoxLeft:=0;
  FLineBorderBoxRight:=FLineBorderBoxLeft;
  //writeln('TFLFlowLayouter.StartLine ',Node.Element.Name,' FLineBorderBoxLeft=',FLineBorderBoxLeft);
  FLineMarginLeft:=0;
  FLineMarginTop:=0;
  FLineMarginRight:=0;
  FLineMarginBottom:=0;
end;

procedure TFLFlowLayouter.EndLine(Commit: boolean);
var
  AbsItem: TLineItem;
  El: TFresnelElement;
  IsInline: Boolean;
begin
  if FLineItems.Count=0 then
    exit;
  FLineBorderBoxTop:=FLastLineBorderBoxBottom
            +Max(FLastLineMarginBottom,FLineMarginTop); // margin collapsing
  FLineBorderBoxBottom:=FLineBorderBoxTop+FLineBorderBoxHeight;

  if Commit then
  begin
    PlaceLineItems;
    if FAbsoluteItems<>nil then
    begin
      // set default top position of absolute blocks below line
      while FLineFirstAbsoluteIndex<FAbsoluteItems.Count do
      begin
        AbsItem:=TLineItem(FAbsoluteItems[FLineFirstAbsoluteIndex]);
        El:=AbsItem.Node.Element;
        IsInline:=El.ComputedDisplayOutside=CSSRegistry.kwInline;
        if not IsInline then
        begin
          AbsItem.StaticTop:=FLineBorderBoxBottom+FLineMarginBottom;
          {$IFDEF VerboseFresnelPlacing}
          writeln('TFLFlowLayouter.EndLine SetAbsBlockDefaultPos ',El.GetPath,' StaticLeft=',FloatToCSSStr(AbsItem.StaticLeft),' StaticTop=',FloatToCSSStr(AbsItem.StaticTop));
          {$ENDIF}
        end;
        inc(FLineFirstAbsoluteIndex);
      end;
    end;
  end;
  ClearLineItems;

  FLastLineBorderBoxBottom:=FLineBorderBoxBottom;
  FLastLineMarginBottom:=FLineMarginBottom;
  //writeln('TFLFlowLayouter.EndLine ',Node.Element.Name,' FBorderBox=',FBorderBox.X,',',FBorderBox.Y);
end;

procedure TFLFlowLayouter.PlaceLineItems;
var
  BFCNode: TFlowItem;
  i: Integer;
  ChildNode: TUsedLayoutNode;
begin
  for i:=0 to FLineItems.Count-1 do
  begin
    BFCNode:=TFlowItem(FLineItems[i]);
    ChildNode:=BFCNode.Node;
    //ChildEl:=ChildNode.Element;

    BFCNode.StaticTop:=FLineBorderBoxTop-ChildNode.MarginTop;
    //writeln('TFLFlowLayouter.PlaceLineNodes ',i,' ',ChildEl.GetPath,' ',FloatToCSSStr(BFCNode.Left),' ',FloatToCSSStr(ChildTop),' BFCNode.Height=',FloatToCSSStr(BFCNode.ContentBoxHeight));
  end;

  inherited;
end;

procedure TFLFlowLayouter.DeductUsedLengths(NoChildren: boolean);
var
  aContainerWidth, aContainerHeight: TFresnelLength;
begin
  inherited DeductUsedLengths(NoChildren);

  with Node do
  begin
    if (Element.ComputedDisplayOutside=CSSRegistry.kwBlock)
        and (Element.ComputedPosition in [CSSRegistry.kwStatic,CSSRegistry.kwRelative,CSSRegistry.kwSticky])
        then
    begin
      // a block element with auto width fills the container
      if Element.ComputedWritingMode=CSSRegistry.kwHorizontalTB then
      begin
        if not IsNan(Width) then exit;
        // a block fills the whole container width
        aContainerWidth:=Element.GetContainerContentWidth(true);
        if not IsNan(aContainerWidth) then
        begin
          Width:=aContainerWidth-MarginLeft-BorderLeft-PaddingLeft
                                -MarginRight-BorderRight-PaddingRight;
          ApplyMinMaxWidth;
        end;
      end else begin
        if not IsNan(Height) then exit;
        // a block fills the whole container height
        aContainerHeight:=Element.GetContainerContentHeight(true);
        if not IsNan(aContainerHeight) then
        begin
          Height:=aContainerHeight-MarginTop-BorderTop-PaddingTop
                                -MarginBottom-BorderBottom-PaddingBottom;
          ApplyMinMaxHeight;
        end;
      end;
    end;
  end;
end;

function TFLFlowLayouter.ComputeLayoutContent(aMode: TFresnelLayoutMode; aMaxWidth,
  aMaxHeight: TFresnelLength; Commit: boolean): TFresnelPoint;
var
  ChildIndex: Integer;
  ChildNode: TUsedLayoutNode;
  ChildEl, El: TFresnelElement;
  IsInline: Boolean;
  NewChildRight, CurSpace, OldMarginBoxBottom,
    ChildMBoxLeft, ChildMBoxRight { MarginBox relative to parent ContentBox left },
    ChildWidth, ChildHeight { ContentBox },
    ChildMinWidth, ChildMinHeight { ContentBox, at least 0 },
    ChildMaxWidth, ChildMaxHeight { ContentBox, can be NaN },
  ChildMarginLeft, ChildMarginRight, ChildMarginTop, ChildMarginBottom,
  ChildPadBorderX, ChildPadBorderY: TFresnelLength;
  ChildPrefSize, ChildDefPos: TFresnelPoint;
  AbsItem: TLineItem;

  procedure AddLineNodeCache;
  var
    N: TFlowItem;
  begin
    FLLog(etDebug,['AddLineNodeCache ',ChildEl.GetPath,' L=',FloatToStr(ChildMBoxLeft),',R=',FloatToStr(ChildMBoxRight),',W=',FloatToStr(ChildWidth),',H=',FloatToStr(ChildHeight)]);
    N:=TFlowItem(AddLineItem(ChildNode,TFlowItem));
    N.StaticLeft:=ChildMBoxLeft;
    N.ContentBoxWidth:=ChildWidth;
    N.ContentBoxHeight:=ChildHeight;
    Result.X:=Max(Result.X,ChildMBoxRight);
  end;

begin
  Result:=default(TFresnelPoint);

  //writeln('TFLFlowLayouter.ComputeLayoutContent ',Node.Element.GetPath,' ',aMode,' MaxWidth=',FloatToCSSStr(aMaxWidth),' MaxHeight=',FloatToCSSStr(aMaxHeight),' Commit=',Commit);

  // ToDo: line-height
  // ToDo: baseline
  // ToDo: writing-mode
  // ToDo: direction
  // ToDo: fragments
  // ToDo: css float and clear

  if IsNan(aMaxHeight) then ;

  FLastLineBorderBoxBottom:=0;
  FLastLineMarginBottom:=0;
  FLineFirstAbsoluteIndex:=0;
  ClearAbsoluteItems;
  El:=Node.Element;

  // add elements to the line until full
  StartLine;
  for ChildIndex:=0 to El.NodeCount-1 do
  begin
    ChildEl:=El.Nodes[ChildIndex];
    ChildNode:=TUsedLayoutNode(ChildEl.LayoutNode);

    if ChildNode.SkipLayout then continue;

    IsInline:=ChildEl.ComputedDisplayOutside=CSSRegistry.kwInline;

    // skip position: absolute and fixed
    if ChildEl.ComputedPosition in [CSSRegistry.kwAbsolute,CSSRegistry.kwFixed] then
    begin
      if Commit then
      begin
        AbsItem:=AddAbsoluteItem(ChildNode,TLineItem);
        if (FLineItems=nil) or (FLineItems.Count=0) then
        begin
          // first item after a block
          AbsItem.StaticTop:=FLineBorderBoxBottom-FLineMarginBottom;
          AbsItem.StaticLeft:=0;
        end else if IsInline then
        begin
          // right behind an inline element
          AbsItem.StaticTop:=FLineBorderBoxTop-FLineMarginTop;
          AbsItem.StaticLeft:=FLineBorderBoxRight+FLineMarginRight;
        end else begin
          // below an inline element
          AbsItem.StaticLeft:=0;
        end;
        {$IFDEF VerboseFresnelPlacing}
        writeln('TFLFlowLayouter.ComputeLayoutContent SetAbsBlockDefaultPos ',ChildEl.GetPath,' StaticLeft=',FloatToCSSStr(AbsItem.StaticLeft),' StaticTop=',FloatToCSSStr(AbsItem.StaticTop));
        {$ENDIF}
      end;
      continue;
    end;

    // display-outside: inline or block
    if (not IsInline) or (aMode in [flmMinWidth,flmMaxHeight]) then
    begin
      // start new line
      if FLineItems.Count>0 then
      begin
        EndLine(Commit);
        StartLine;
      end;
    end;

    ChildPadBorderX:=ChildNode.BorderLeft+ChildNode.PaddingLeft+ChildNode.PaddingRight+ChildNode.BorderRight;
    ChildPadBorderY:=ChildNode.BorderTop+ChildNode.PaddingTop+ChildNode.PaddingBottom+ChildNode.BorderBottom;

    if aMode in [flmMinWidth,flmMaxHeight] then
      CurSpace:=0
    else if IsNan(aMaxWidth) then
      CurSpace:=NaN
    else
      CurSpace:=Max(0,aMaxWidth-FLineBorderBoxRight-Max(FLineMarginRight,ChildNode.MarginLeft)-ChildNode.MarginRight);

    //writeln('TFLFlowLayouter.ComputeLayoutContent ',ChildEl.GetPath,' Margin=',FloatToStr(ChildMarginLeft),',',FloatToStr(ChildMarginTop),',',FloatToStr(ChildMarginRight),',',FloatToStr(ChildMarginBottom),' Border=',FloatToStr(ChildBorderLeft),',',FloatToStr(ChildBorderTop),',',FloatToStr(ChildBorderRight),',',FloatToStr(ChildBorderBottom),' Padding=',FloatToStr(ChildPaddingLeft),',',FloatToStr(ChildPaddingTop),',',FloatToStr(ChildPaddingRight),',',FloatToStr(ChildPaddingBottom));

    ChildMarginLeft:=ChildNode.MarginLeft;
    ChildMarginRight:=ChildNode.MarginRight;
    ChildMarginTop:=ChildNode.MarginTop;
    ChildMarginBottom:=ChildNode.MarginBottom;

    // width, height are contentbox and can be NaN
    ChildWidth:=ChildNode.Width;
    ChildHeight:=ChildNode.Height;
    ChildMinWidth:=ChildNode.MinWidth; // at least 0
    ChildMaxWidth:=ChildNode.MaxWidth; // can be NaN
    ChildMinHeight:=ChildNode.MinHeight; // at least 0
    ChildMaxHeight:=ChildNode.MaxHeight; // can be NaN

    //writeln('TFLFlowLayouter.ComputeLayoutContent ',ChildEl.GetPath,' Commit=',Commit,' ChildWidth=',FloatToStr(ChildWidth),' ChildHeight=',FloatToStr(ChildHeight));

    if Commit and (not IsInline) and IsNan(ChildWidth) and (not IsNan(CurSpace)) then
    begin
      // a block element expands the full line
      //writeln('TFLFlowLayouter.ComputeLayoutContent BLOCK FULL LINE: ',ChildEl.GetPath,' CurSpace=',FloatToStr(CurSpace),' ChildPadBorderX=',FloatToStr(ChildPadBorderX));
      ChildWidth:=CurSpace-ChildPadBorderX;
    end;
    if not IsNan(ChildWidth) then
    begin
      if not IsNan(ChildMaxWidth) then
        ChildWidth:=Min(ChildWidth,ChildMaxWidth);
      ChildWidth:=Max(ChildWidth,ChildMinWidth);
    end;

    if IsNan(ChildWidth) or IsNan(ChildHeight) then
    begin
      // one or both sides are dynamic
      if IsNan(ChildWidth) then
      begin
        if not IsNan(CurSpace) then
        begin
          if not IsNan(ChildMaxWidth) then
            CurSpace:=Min(CurSpace-ChildPadBorderX,ChildMaxWidth);
          CurSpace:=Max(CurSpace-ChildPadBorderX,ChildMinWidth);
        end;
      end else begin
        CurSpace:=ChildWidth;
      end;
      ChildPrefSize:=ChildEl.GetIntrinsicContentSize(aMode,CurSpace,NaN);
      //if ChildEl.Name='CaptionLabel' then
      //  writeln('TFLFlowLayouter.ComputeLayoutContent ',ChildEl.GetPath,' MaxWidth=',CurSpace,' Preferred=',ChildPrefSize.ToString,' ChildPadBorderX=',ChildPadBorderX,' ChildPadBorderY=',ChildPadBorderY);
      // apply min, max
      if IsNan(ChildWidth) then
      begin
        ChildWidth:=ChildPrefSize.X;
        if not IsNan(ChildMaxWidth) then
          ChildWidth:=Min(ChildWidth,ChildMaxWidth);
        ChildWidth:=Max(ChildWidth,ChildMinWidth);
      end;
      if IsNan(ChildHeight) then
      begin
        ChildHeight:=ChildPrefSize.Y;
        if not IsNan(ChildMaxHeight) then
          ChildHeight:=Min(ChildHeight,ChildMaxHeight);
        ChildHeight:=Max(ChildHeight,ChildMinHeight);
      end;
    end;

    NewChildRight:=0;
    if (FLineItems.Count>0) and IsInline and (not IsNan(aMaxWidth)) then
    begin
      // check if inline element fits in line
      NewChildRight:=FLineBorderBoxRight
                     +Max(FLineMarginRight,ChildMarginLeft) // margin collapsing
                     +ChildWidth+ChildPadBorderX
                     +ChildMarginRight;
      if NewChildRight>aMaxWidth then
      begin
        // element does not fit into the line -> next line
        EndLine(Commit);
        StartLine;
      end;
    end;

    //debugln(['TFLFlowLayouter.ComputeLayoutContent ',ChildEl.Name,' ',ChildHeight,':=BT ',ChildBorderTop,'+PT ',ChildPaddingTop,'+H ',ChildHeight,'+PB ',ChildPaddingBottom,'+BB ',ChildBorderBottom]);
    if FLineItems.Count=0 then
    begin
      // first element in line
      FLineMarginLeft:=ChildMarginLeft;
      FLineMarginRight:=ChildMarginRight;
      FLineMarginTop:=ChildMarginTop;
      FLineMarginBottom:=ChildMarginBottom;

      // todo: div adds margin to left/top, span only to left
      ChildMBoxLeft:=0;
      ChildMBoxRight:=ChildMBoxLeft
                 +ChildMarginLeft
                 +ChildWidth+ChildPadBorderX
                 +ChildMarginRight;

      FLineBorderBoxLeft:=ChildMBoxLeft+ChildMarginLeft;
      FLineBorderBoxRight:=ChildMBoxRight-ChildMarginRight;
      FLineBorderBoxHeight:=ChildHeight+ChildPadBorderY;
      //writeln('TFLFlowLayouter.ComputeLayoutContent ',ChildEl.Name,' FBorderLeft=',FloatToStr(FBorderLeft),' FPaddingLeft=',FloatToStr(FPaddingLeft),' ChildMarginLeft=',FloatToStr(ChildMarginLeft),' FLineBorderBoxLeft=',FloatToStr(FLineBorderBoxLeft),' ChildLeft=',FloatToStr(ChildMBoxLeft),' ChildRight=',FloatToStr(ChildMBoxRight),
      //  ' ChildLeft=',FloatToStr(ChildMBoxLeft),
      //  ' ChildMarginLeft=',FloatToStr(ChildMarginLeft),' ChildBorderLeft=',FloatToStr(ChildBorderLeft),' ChildPaddingLeft=',FloatToStr(ChildPaddingLeft),
      //  ' ChildWidth=',FloatToStr(ChildWidth),
      //  ' ChildPaddingRight=',FloatToStr(ChildPaddingRight),' ChildBorderRight=',FloatToStr(ChildBorderRight),' ChildMarginRight=',FloatToStr(ChildMarginRight));

      AddLineNodeCache;

      if ((not IsNan(aMaxWidth)) and (ChildMBoxRight>=aMaxWidth))
          or (not IsInline) then
      begin
        // first element already fills the max width
        EndLine(Commit);
        StartLine;
      end;
    end else begin
      // append element to line
      ChildMBoxLeft:=FLineBorderBoxRight
                 +Max(FLineMarginRight,ChildMarginLeft) // margin collapsing
                 -ChildMarginLeft;
      ChildMBoxRight:=ChildMBoxLeft
                 +ChildMarginLeft
                 +ChildWidth+ChildPadBorderX
                 +ChildMarginRight;

      FLineBorderBoxRight:=ChildMBoxRight-ChildMarginRight;
      FLineMarginRight:=ChildMarginRight;

      // align element at the top of the line
      FLineMarginTop:=Max(FLineMarginTop,ChildMarginTop);
      OldMarginBoxBottom:=FLineBorderBoxHeight+FLineMarginBottom;
      FLineBorderBoxHeight:=Max(FLineBorderBoxHeight,ChildHeight+ChildPadBorderY);
      if OldMarginBoxBottom<ChildHeight+ChildPadBorderY+ChildMarginBottom then
      begin
        FLineMarginBottom:=ChildHeight+ChildPadBorderY-FLineBorderBoxHeight+ChildMarginBottom;
      end;

      AddLineNodeCache;
    end;

    //writeln('TFLFlowLayouter.ComputeLayoutContent ',ChildEl.Name,' Commit=',Commit,' ChildLeft=',ChildMBoxLeft,' ChildRight=',ChildMBoxRight,' ChildWidth=',ChildWidth,' ChildHeight=',ChildHeight);
  end;
  EndLine(Commit);

  if Commit and (FAbsoluteItems<>nil) then
  begin
    // place absolute items
    for ChildIndex:=0 to FAbsoluteItems.Count-1 do
    begin
      AbsItem:=TLineItem(FAbsoluteItems[ChildIndex]);
      ChildNode:=AbsItem.Node;

      ChildDefPos.X:=AbsItem.StaticLeft;
      ChildDefPos.Y:=AbsItem.StaticTop;

      PlaceAbsoluteItem(ChildNode,aMode,aMaxWidth,aMaxHeight,ChildDefPos,Commit);
    end;
    ClearAbsoluteItems;
  end;

  Result.Y:=Max(Result.Y,FLastLineBorderBoxBottom+FLastLineMarginBottom);
end;

{ TFLGridLayouter }

function TFLGridLayouter.ComputeLayoutContent(aMode: TFresnelLayoutMode; aMaxWidth,
  aMaxHeight: TFresnelLength; Commit: boolean): TFresnelPoint;
begin
  Result:=Default(TFresnelPoint);

  if Commit then ;
  if aMode=flmMaxWidth then ;
  if IsNan(aMaxWidth) then ;
  if IsNan(aMaxHeight) then ;
end;

{ TFLFlexLayouter }

procedure TFLFlexLayouter.StartLine;
begin
  if FLineItems=nil then FLineItems:=TFPList.Create;
end;

procedure TFLFlexLayouter.EndLine(MaxMainSize, MainGap: TFresnelLength; Commit: boolean;
  var NewContentSize: TFresnelPoint);
begin
  if FLineItems.Count=0 then exit;
  FlexLineMainDirection(MaxMainSize,MainGap,Commit,NewContentSize);
  FlexLineCrossDirection(Commit,NewContentSize);
  if Commit then
    PlaceLineItems;
  ClearLineItems;
end;

procedure TFLFlexLayouter.FlexLineMainDirection(MaxMainSize, MainGap: TFresnelLength;
  Commit: boolean; var NewContentSize: TFresnelPoint);
const
  MinAdjust = 0.0001;
var
  CurMainSize: TFresnelLength;
  aSpace: TFresnelLength; // can be NaN

  procedure CalcMainSize;
  var
    i: Integer;
    Item: TFlexItem;
  begin
    CurMainSize:=0;
    for i:=0 to FLineItems.Count-1 do
    begin
      if i>0 then
        CurMainSize:=CurMainSize+MainGap;
      Item:=TFlexItem(FLineItems[i]);
      CurMainSize:=CurMainSize+Item.MainFrameLT+Item.MainContentSize+Item.MainFrameRB;
    end;
    if IsNan(MaxMainSize) then
      aSpace:=0
    else
      aSpace:=MaxMainSize-CurMainSize;
  end;

  function CalcSpaceItem(Count: integer; StartSpaceMul: TFresnelLength;
    out aPos: TFresnelLength): TFresnelLength;
  begin
    Result:=Max(0,aSpace)/Count;
    aPos:=Result*StartSpaceMul;
    if FlexDirection in [CSSRegistry.kwRowReverse,CSSRegistry.kwColumnReverse] then
    begin
      aPos:=MaxMainSize-aPos;
      Result:=-Result;
    end;
  end;

  procedure AdjustMainContentSize(p: TFresnelLength);
  begin
    if MainIsRow then
      NewContentSize.X:=Max(NewContentSize.X,p)
    else
      NewContentSize.Y:=Max(NewContentSize.Y,p);
  end;

var
  SumGrow, SumShrink, p, aSpaceItem: TFresnelLength;
  i: Integer;
  Item: TFlexItem;
  ItemNode: TUsedLayoutNode;
begin
  for i:=0 to FLineItems.Count-1 do
  begin
    Item:=TFlexItem(FLineItems[i]);
    Item.MainContentSize:=Item.Basis;
  end;

  CalcMainSize;
  // shrink or grow
  repeat
    // compute difference between current size and max size
    if aSpace>MinAdjust then
    begin
      // try to grow
      // collect grow factors of all items able to grow
      SumGrow:=0;
      for i:=0 to FLineItems.Count-1 do
      begin
        Item:=TFlexItem(FLineItems[i]);
        if Item.Grow<MinAdjust then continue;
        ItemNode:=Item.Node;
        if (not IsNan(ItemNode.MaxWidth))
            and (Item.MainContentSize>ItemNode.MaxWidth-MinAdjust) then continue;
        SumGrow:=SumGrow+Item.Grow;
      end;
      if SumGrow=0 then
        break; // can not grow
      // grow
      for i:=0 to FLineItems.Count-1 do
      begin
        Item:=TFlexItem(FLineItems[i]);
        if Item.Grow<MinAdjust then continue;
        ItemNode:=Item.Node;
        if (not IsNan(ItemNode.MaxWidth))
            and (Item.MainContentSize>ItemNode.MaxWidth-MinAdjust) then continue;
        Item.MainContentSize:=Item.MainContentSize+(Item.Grow/SumGrow)*aSpace;
        if (not IsNan(ItemNode.MaxWidth))
            and (Item.MainContentSize>ItemNode.MaxWidth-MinAdjust) then
          Item.MainContentSize:=ItemNode.MaxWidth;
      end;
      CalcMainSize;
    end else if aSpace<-MinAdjust then begin
      // try to shrink
      // collect shrink factors of all items able to shrink
      SumShrink:=0;
      for i:=0 to FLineItems.Count-1 do
      begin
        Item:=TFlexItem(FLineItems[i]);
        if Item.Shrink<MinAdjust then continue;
        if Item.MainContentSize<MinAdjust then continue;
        ItemNode:=Item.Node;
        if (not IsNan(ItemNode.MinWidth))
            and (Item.MainContentSize<ItemNode.MinWidth+MinAdjust) then continue;
        SumShrink:=SumShrink+Item.Shrink;
      end;
      if SumShrink=0 then
        break; // can not shrink
      // shrink
      for i:=0 to FLineItems.Count-1 do
      begin
        Item:=TFlexItem(FLineItems[i]);
        if Item.Shrink<MinAdjust then continue;
        if Item.MainContentSize<MinAdjust then continue;
        ItemNode:=Item.Node;
        if (not IsNan(ItemNode.MinWidth))
            and (Item.MainContentSize<ItemNode.MinWidth+MinAdjust) then continue;
        Item.MainContentSize:=Max(0,Item.MainContentSize+(Item.Shrink/SumShrink)*aSpace);
        if (not IsNan(ItemNode.MinWidth))
            and (Item.MainContentSize<ItemNode.MinWidth+MinAdjust) then
          Item.MainContentSize:=ItemNode.MinWidth;
      end;
      CalcMainSize;
    end else
      break;
  until false;

  // justify-content:
  // todo  safe, unsafe: ?

  if Commit then
  begin
    if IsNan(MaxMainSize) then
      MaxMainSize:=CurMainSize;
    if (JustifyContent in [CSSRegistry.kwLeft,CSSRegistry.kwStart,CSSRegistry.kwFlexStart]) then
      AdjustMainContentSize(CurMainSize)
    else
      AdjustMainContentSize(MaxMainSize);
  end else
    AdjustMainContentSize(CurMainSize);

  if Commit then
  begin
    p:=0;
    aSpaceItem:=0;
    case JustifyContent of
    CSSRegistry.kwLeft: ;
      //   left: in row/row-reverse move line left, in column: top
    CSSRegistry.kwRight:
      //   right: in row/row-reverse move line right, in column: top
      if MainIsRow then
        p:=MaxMainSize;
    CSSRegistry.kwStart: ;
      //   start: move line left or top
    CSSRegistry.kwEnd:
      //   end: move line right or bottom
      p:=MaxMainSize;
    CSSRegistry.kwFlexStart,
    CSSRegistry.kwStretch: ;
      //   flex-start: main start
    CSSRegistry.kwFlexEnd:
      //   flex-end: main end
      p:=MaxMainSize;
    CSSRegistry.kwCenter:
      //   center: main center
      begin
        p:=(MaxMainSize-CurMainSize)/2;
        if MainIsReverse then
          p:=MaxMainSize-p;
      end;
    CSSRegistry.kwSpaceAround:
      //   space-around: distribute space between each pair and half at start and end
      CalcSpaceItem(FLineItems.Count,0.5,p);
    CSSRegistry.kwSpaceBetween:
      //   space-between: distribute space between each pair
      CalcSpaceItem(Max(1,FLineItems.Count-1),0,p);
    CSSRegistry.kwSpaceEvenly:
      //   space-evenly: distribute space between each pair and full at start and end
      CalcSpaceItem(FLineItems.Count+1,1,p);
    end;

    for i:=0 to FLineItems.Count-1 do
    begin
      Item:=TFlexItem(FLineItems[i]);
      if FlexDirection in [CSSRegistry.kwRow,CSSRegistry.kwColumn] then
      begin
        if i>0 then p:=p+MainGap;
        if MainIsRow then
          Item.StaticLeft:=p
        else
          Item.StaticTop:=p;
        p:=p+Item.MainFrameLT+Item.MainContentSize+Item.MainFrameRB;
      end else begin
        if i>0 then p:=p-MainGap;
        p:=p-(Item.MainFrameLT+Item.MainContentSize+Item.MainFrameRB);
        if MainIsRow then
          Item.StaticLeft:=p
        else
          Item.StaticTop:=p;
      end;
      if MainIsRow then
        Item.ContentBoxWidth:=Item.MainContentSize
      else
        Item.ContentBoxHeight:=Item.MainContentSize;

      case JustifyContent of
      CSSRegistry.kwSpaceAround:
        //   space-around: distribute space between each pair and half at start and end
        if i<FLineItems.Count-1 then
          p:=p+aSpaceItem
        else
          p:=p+aSpaceItem/2;
      CSSRegistry.kwSpaceBetween:
        //   space-between: distribute space between each pair
        if i<FLineItems.Count-1 then
          p:=p+aSpaceItem;
      CSSRegistry.kwSpaceEvenly:
        //   space-evenly: distribute space between each pair and full at start and end
        p:=p+aSpaceItem;
      end;
    end;
  end;
end;

procedure TFLFlexLayouter.FlexLineCrossDirection(Commit: boolean; var NewContentSize: TFresnelPoint
  );
var
  i: Integer;
  Item: TFlexItem;
  Size, MaxSize, StartPos: TFresnelLength;
  ItemNode: TUsedLayoutNode;
begin
  // all items on line now have their size and position in main direction
  // -> compute their cross direction size and position

  // find biggest
  MaxSize:=0;
  for i:=0 to FLineItems.Count-1 do
  begin
    Item:=TFlexItem(FLineItems[i]);
    Size:=Item.CrossFrameLT+Item.CrossContentSize+Item.CrossFrameRB;
    MaxSize:=Max(MaxSize,Size);
  end;

  if Commit then
  begin
    // align items
    for i:=0 to FLineItems.Count-1 do
    begin
      Item:=TFlexItem(FLineItems[i]);
      ItemNode:=Item.Node;
      Size:=Item.CrossFrameLT+Item.CrossContentSize+Item.CrossFrameRB;
      StartPos:=0;
      case Item.AlignSelf of
      CSSRegistry.kwLeft,
      CSSRegistry.kwStart,
      CSSRegistry.kwFlexStart:
        ;
      CSSRegistry.kwRight,
      CSSRegistry.kwEnd,
      CSSRegistry.kwFlexEnd:
        StartPos:=MaxSize-Size;
      CSSRegistry.kwCenter:
        StartPos:=(MaxSize-Size)/2;
      CSSRegistry.kwBaseline:
        begin
          // todo baseline, first baseline, last baseline
        end
      else
        // CSSRegistry.kwNormal,
        // CSSRegistry.kwStretch:
        begin
          Size:=MaxSize;
          if MainIsRow then
          begin
            if not IsNan(ItemNode.MaxHeight) then
              Size:=Min(Size-Item.CrossFrameLT-Item.CrossFrameRB,ItemNode.MaxHeight)
                   +Item.CrossFrameLT+Item.CrossFrameRB;
          end else begin
            if not IsNan(ItemNode.MaxWidth) then
              Size:=Min(Size-Item.CrossFrameLT-Item.CrossFrameRB,ItemNode.MaxWidth)
                   +Item.CrossFrameLT+Item.CrossFrameRB;
          end;
        end;
      end;

      if MainIsRow then
      begin
        Item.StaticTop:=StartPos;
        Item.ContentBoxHeight:=Size-Item.CrossFrameLT-Item.CrossFrameRB;
      end else begin
        Item.StaticLeft:=StartPos;
        Item.ContentBoxWidth:=Size-Item.CrossFrameLT-Item.CrossFrameRB;
      end;
    end;
  end;

  if MainIsRow then
    NewContentSize.Y:=Max(NewContentSize.Y,MaxSize)
  else
    NewContentSize.X:=Max(NewContentSize.X,MaxSize);
end;

procedure TFLFlexLayouter.PlaceLineItems;
begin
  inherited;
end;

procedure TFLFlexLayouter.ComputeChildAttributes(Item: TLineItem;
  El: TFresnelElement);
var
  FlexItem: TFlexItem;
  ItemNode: TUsedLayoutNode;
  p: TFresnelPoint;
begin
  inherited ComputeChildAttributes(Item, El);

  ItemNode:=Item.Node;
  FlexItem:=Item as TFlexItem;
  FlexItem.Basis:=El.GetComputedLength(fcaFlexBasis,true);
  FlexItem.Grow:=El.GetComputedLength(fcaFlexGrow,true);
  FlexItem.Shrink:=El.GetComputedLength(fcaFlexShrink,true);
  FlexItem.AlignSelf:=El.GetComputedAlignSelf(FlexItem.AlignSelfSub);
  if FlexItem.AlignSelf=CSSRegistry.kwAuto then
    FlexItem.AlignSelf:=AlignItems;

  if IsNan(FlexItem.Basis) then
  begin
    if MainIsRow then
    begin
      FlexItem.MainFrameLT:=ItemNode.MarginLeft+ItemNode.BorderLeft+ItemNode.PaddingLeft;
      FlexItem.MainFrameRB:=ItemNode.MarginRight+ItemNode.BorderRight+ItemNode.PaddingRight;
      FlexItem.CrossFrameLT:=ItemNode.MarginTop+ItemNode.BorderTop+ItemNode.PaddingTop;
      FlexItem.CrossFrameRB:=ItemNode.MarginBottom+ItemNode.BorderBottom+ItemNode.PaddingBottom;
      FlexItem.CrossContentSize:=ItemNode.Height;

      FlexItem.Basis:=ItemNode.Width;
      if IsNan(FlexItem.Basis) then
      begin
        p:=El.GetIntrinsicContentSize(flmMaxWidth,ItemNode.MaxWidth,ItemNode.MaxHeight);
        FlexItem.Basis:=ItemNode.FitWidth(p.X);
        if IsNan(FlexItem.CrossContentSize) then
          FlexItem.CrossContentSize:=ItemNode.FitHeight(p.Y);
      end else if IsNan(FlexItem.CrossContentSize) then begin
        p:=El.GetIntrinsicContentSize(flmMaxWidth,FlexItem.Basis,ItemNode.MaxHeight);
        FlexItem.CrossContentSize:=ItemNode.FitHeight(p.Y);
      end;

    end else begin
      FlexItem.MainFrameLT:=ItemNode.MarginTop+ItemNode.BorderTop+ItemNode.PaddingTop;
      FlexItem.MainFrameRB:=ItemNode.MarginBottom+ItemNode.BorderBottom+ItemNode.PaddingBottom;
      FlexItem.CrossFrameLT:=ItemNode.MarginLeft+ItemNode.BorderLeft+ItemNode.PaddingLeft;
      FlexItem.CrossFrameRB:=ItemNode.MarginRight+ItemNode.BorderRight+ItemNode.PaddingRight;
      FlexItem.CrossContentSize:=ItemNode.Width;

      FlexItem.Basis:=ItemNode.Height;
      if IsNan(FlexItem.Basis) then
      begin
        p:=El.GetIntrinsicContentSize(flmMaxHeight,ItemNode.MaxWidth,ItemNode.MaxHeight);
        FlexItem.Basis:=ItemNode.FitWidth(p.Y);
        if IsNan(FlexItem.CrossContentSize) then
          FlexItem.CrossContentSize:=ItemNode.FitWidth(p.X);
      end else if IsNan(FlexItem.CrossContentSize) then begin
        p:=El.GetIntrinsicContentSize(flmMaxHeight,ItemNode.MaxWidth,FlexItem.Basis);
        FlexItem.CrossContentSize:=ItemNode.FitWidth(p.X);
      end;
    end;
  end;
  if IsNan(FlexItem.Grow) then FlexItem.Grow:=0;
  if IsNan(FlexItem.Shrink) then FlexItem.Shrink:=1;

  {$IFDEF VerboseFlexLayout}
  writeln('TFLFlexLayouter.ComputeChildAttributes ',ItemNode.Element.GetPath,' Basis=',FloatToCSSStr(FlexItem.Basis),' Grow=',FloatToCSSStr(FlexItem.Grow),' Shrink=',FloatToCSSStr(FlexItem.Shrink),' AlignSelf=',CSSRegistry.Keywords[FlexItem.AlignSelf],' CrossSize=',FloatToCSSStr(FlexItem.CrossContentSize));
  {$ENDIF}
end;

procedure TFLFlexLayouter.Init;
var
  El: TFresnelElement;
begin
  inherited Init;
  El:=Node.Element;
  FlexDirection:=El.GetComputedKeyword(fcaFlexDirection,CSSRegistry.Chk_FlexDirection_KeywordIDs);
  MainIsRow:=FlexDirection in [CSSRegistry.kwRow,CSSRegistry.kwRowReverse];
  MainIsReverse:=FlexDirection in [CSSRegistry.kwRowReverse,CSSRegistry.kwColumnReverse];
  FlexWrap:=El.GetComputedKeyword(fcaFlexWrap,CSSRegistry.Chk_FlexWrap_KeywordIDs);
  JustifyContent:=El.GetComputedJustifyContent(JustifyContentSub);
  AlignItems:=El.GetComputedAlignItems(AlignItemsSub);
  //if El.ComputedDirection; // todo direction

  FGap[false]:=NaN;
  FGap[true]:=NaN;
end;

function TFLFlexLayouter.GetComputedGap(IsHorizontal: boolean): TFresnelLength;
var
  El: TFresnelElement;
begin
  if not IsNan(FGap[IsHorizontal]) then
    exit(FGap[IsHorizontal]);
  Result:=0;
  El:=Node.Element;
  if IsHorizontal then
    Result:=El.GetComputedLength(fcaColumnGap,false,true)
  else
    Result:=El.GetComputedLength(fcaRowGap,false,true);
  FGap[IsHorizontal]:=Result;
end;

function TFLFlexLayouter.ComputeLayoutContent(aMode: TFresnelLayoutMode; aMaxWidth,
  aMaxHeight: TFresnelLength; Commit: boolean): TFresnelPoint;
var
  ChildIndex: Integer;
  ChildNode: TUsedLayoutNode;
  ChildEl, El: TFresnelElement;
  Item: TFlexItem;
  MaxMainSize: TFresnelLength; // max content size in main direction, can be NaN
  CurMainSize: TFresnelLength; // current line size in main direction
  NewMainSize, MainGap: TFresnelLength; // gap in main direction
  ChildDefPos: TFresnelPoint;
  ItemAdded: Boolean;
begin
  Result:=default(TFresnelPoint);

  if MainIsRow then
  begin
    MaxMainSize:=aMaxWidth;
    MainGap:=GetComputedGap(true);
  end else begin
    MaxMainSize:=aMaxHeight;
    MainGap:=GetComputedGap(false);
  end;

  CurMainSize:=0;

  StartLine;
  Item:=nil;
  ItemAdded:=false;
  El:=Node.Element;
  try
    for ChildIndex:=0 to El.NodeCount-1 do
    begin
      ChildEl:=El.Nodes[ChildIndex];
      ChildNode:=TUsedLayoutNode(ChildEl.LayoutNode);

      if ChildNode.SkipLayout then continue;

      // absolute and fixed: todo: place after items have their final static box
      if ChildEl.ComputedPosition in [CSSRegistry.kwAbsolute,CSSRegistry.kwFixed] then
      begin
        ChildDefPos.X:=0;
        ChildDefPos.Y:=0;
        PlaceAbsoluteItem(ChildNode,aMode,aMaxWidth,aMaxHeight,ChildDefPos,Commit);
        continue;
      end;

      Item:=TFlexItem.Create;
      Item.Node:=ChildNode;
      ItemAdded:=false;
      ComputeChildAttributes(Item,ChildEl);

      if FLineItems.Count>0 then
      begin
        // check if item fits into line
        NewMainSize:=CurMainSize+MainGap+Item.MainFrameLT+Item.Basis+Item.MainFrameRB;

        if (FlexWrap=CSSRegistry.kwNoWrap)
            or IsNan(MaxMainSize) or (NewMainSize<=MaxMainSize) then
        begin
          // append to line
          CurMainSize:=NewMainSize;
          FLineItems.Add(Item);
          ItemAdded:=true;
        end else begin
          // end line
          EndLine(MaxMainSize,MainGap,Commit,Result);
        end;
      end;

      if FLineItems.Count=0 then
      begin
        // first element in line
        FLineItems.Add(Item);
        ItemAdded:=true;
        CurMainSize:=Item.MainFrameLT+Item.Basis+Item.MainFrameRB;
      end;
    end;
    EndLine(MaxMainSize,MainGap,Commit,Result);

  finally
    if not ItemAdded then
      Item.Free;
  end;
end;

{ TFLLineLayouter }

procedure TFLLineLayouter.PlaceLineItems;
var
  i: Integer;
  Item: TLineItem;
  ChildNode: TUsedLayoutNode;
  ChildEl: TFresnelElement;
  NewLeft, NewTop: TFresnelLength;
  r: TFresnelRect;
begin
  for i:=0 to FLineItems.Count-1 do
  begin
    Item:=TLineItem(FLineItems[i]);
    ChildNode:=Item.Node;
    ChildEl:=ChildNode.Element;

    NewLeft:=Item.StaticLeft;
    NewTop:=Item.StaticTop;
    if IsNan(NewLeft) then
      raise EFresnelLayout.Create('20241001111913');
    if IsNan(NewTop) then
      raise EFresnelLayout.Create('20241001111913');
    if IsNan(Item.ContentBoxWidth) then
      raise EFresnelLayout.Create('20241001111941');
    if IsNan(Item.ContentBoxHeight) then
      raise EFresnelLayout.Create('20241001111949');

    if ChildEl.ComputedPosition in [CSSRegistry.kwRelative,CSSRegistry.kwSticky] then
    begin
      // relative left
      if not IsNan(ChildNode.Left) then
        NewLeft:=NewLeft+ChildNode.Left
      else if not IsNan(ChildNode.Right) then
        NewLeft:=NewLeft-ChildNode.Right;

      // relative top
      if not IsNan(ChildNode.Top) then
        NewTop:=NewTop+ChildNode.Top
      else if not IsNan(ChildNode.Bottom) then
        NewTop:=NewTop-ChildNode.Bottom;
    end;

    // used borderbox
    r.Left:=NewLeft+ChildNode.MarginLeft;
    r.Top:=NewTop+ChildNode.MarginTop;
    r.Right:=r.Left+ChildNode.BorderLeft+ChildNode.PaddingLeft
             +Item.ContentBoxWidth
             +ChildNode.PaddingRight+ChildNode.BorderRight;
    r.Bottom:=r.Top+ChildNode.BorderTop+ChildNode.PaddingTop
             +Item.ContentBoxHeight
             +ChildNode.PaddingBottom+ChildNode.BorderBottom;
    ChildEl.UsedBorderBox:=r;
    ChildNode.Left:=NewLeft;
    ChildNode.Top:=NewTop;

    // used contentbox
    r.Left:=r.Left+ChildNode.BorderLeft+ChildNode.PaddingLeft;
    r.Top:=r.Top+ChildNode.BorderTop+ChildNode.PaddingTop;
    r.Right:=r.Left+Item.ContentBoxWidth;
    r.Bottom:=r.Top+Item.ContentBoxHeight;
    ChildEl.UsedContentBox:=r;
    ChildNode.Width:=r.Width;
    ChildNode.Height:=r.Height;

    {$IFDEF VerboseFresnelPlacing}
    writeln('TFLFlowLayouter.PlaceLineItems '+ChildEl.GetPath+' BorderBox='+ChildEl.UsedBorderBox.ToString);
    {$ENDIF}
  end;
end;

function TFLLineLayouter.PlaceAbsoluteItem(ChildNode: TUsedLayoutNode; aMode: TFresnelLayoutMode;
  aMaxWidth, aMaxHeight: TFresnelLength; const DefaultPos: TFresnelPoint; Commit: boolean
  ): TFresnelPoint;
// returns right, bottom marginbox, so the parent can compute its needed content size
var
  ChildEl: TFresnelElement;
  NewLeft, NewTop, NewRight, NewBottom, NewWidth, NewHeight, FrameLeft, FrameRight,
    FrameTop, FrameBottom: TFresnelLength;
  p: TFresnelPoint;
  r: TFresnelRect;
begin
  Result.X:=0;
  Result.Y:=0;

  ChildEl:=ChildNode.Element;

  {$IFDEF VerboseFresnelPlacing}
  //if ChildEl.Name='Div1' then
    writeln('TFLLineLayouter.PlaceAbsoluteItem ',ChildEl.GetPath,' ',aMode,' Commit=',Commit,' Left=',FloatToCSSStr(ChildNode.Left),',Top=',FloatToCSSStr(ChildNode.Top),',Right=',FloatToCSSStr(ChildNode.Right),',Bottom=',FloatToCSSStr(ChildNode.Bottom),' Width=',FloatToCSSStr(ChildNode.Width),' Height=',FloatToCSSStr(ChildNode.Height),' Default=',FloatToCSSStr(DefaultPos.X),',',FloatToCSSStr(DefaultPos.Y));
    writeln('TFLLineLayouter.PlaceAbsoluteItem ',CSSRegistry.Keywords[ChildEl.ComputedDisplayOutside],' ',CSSRegistry.Keywords[ChildEl.ComputedDisplayInside],' Style=',ChildEl.Style);
  {$ENDIF}
  NewLeft:=ChildNode.Left;
  NewTop:=ChildNode.Top;
  NewRight:=ChildNode.Right;
  NewBottom:=ChildNode.Bottom;
  p.X:=NaN;
  if IsNan(NewLeft) and IsNan(NewRight) then
  begin
    p:=ChildEl.GetContainerOffset;
    NewLeft:=p.X+DefaultPos.X;
  end;
  if IsNan(NewTop) and IsNan(NewBottom) then
  begin
    if IsNan(p.X) then
      p:=ChildEl.GetContainerOffset;
    NewTop:=p.y+DefaultPos.Y;
  end;

  if Commit then
  begin
    NewWidth:=ChildNode.Width;
    NewHeight:=ChildNode.Height;
  end else begin
    NewWidth:=NaN;
    NewHeight:=NaN;
  end;

  // Note: The DeductUsedLengths has already computed simple cases like
  //   given container's width and child's left and right => child's width

  if IsNan(NewWidth) or IsNan(NewHeight) then
  begin
    if IsNan(NewWidth) then
    begin
      if IsNan(NewHeight) then
      begin
        // auto width and height
        p:=ChildEl.GetIntrinsicContentSize(aMode,ChildNode.MaxWidth,ChildNode.MaxHeight);
      end else begin
        // height set, width auto
        p:=ChildEl.GetIntrinsicContentSize(aMode,ChildNode.MaxWidth,NewHeight);
      end;
    end else begin
      // width set, height auto
      p:=ChildEl.GetIntrinsicContentSize(aMode,NewWidth,ChildNode.MaxHeight);
    end;
    if IsNan(NewWidth) then
      NewWidth:=ChildNode.FitWidth(p.X);
    if IsNan(NewHeight) then
      NewHeight:=ChildNode.FitHeight(p.Y);
  end;

  FrameLeft:=ChildNode.MarginLeft+ChildNode.BorderLeft+ChildNode.PaddingLeft;
  FrameRight:=ChildNode.PaddingRight+ChildNode.BorderRight+ChildNode.MarginRight;
  FrameTop:=ChildNode.MarginTop+ChildNode.BorderTop+ChildNode.PaddingTop;
  FrameBottom:=ChildNode.PaddingBottom+ChildNode.BorderBottom+ChildNode.MarginBottom;
  if Commit then
  begin
    if IsNan(NewLeft) then
    begin
      if IsNan(aMaxWidth) or IsNan(NewRight) then
        NewLeft:=0
      else
        NewLeft:=aMaxWidth-NewRight-NewWidth-FrameLeft-FrameRight;
    end;
    if IsNan(NewTop) then
    begin
      if IsNan(aMaxHeight) or IsNan(NewBottom) then
        NewTop:=0
      else
        NewTop:=aMaxHeight-NewBottom-NewHeight-FrameTop-FrameBottom;
    end;
  end;

  // compute right, bottom of marginbox
  Result.X:=FrameLeft+NewWidth+FrameRight;
  if not IsNan(NewLeft) then
    Result.X:=Result.X+NewLeft;
  if not IsNan(NewRight) then
    Result.X:=Result.X+NewRight;

  Result.Y:=FrameTop+NewWidth+FrameBottom;
  if not IsNan(NewTop) then
    Result.Y:=Result.Y+NewTop;
  if not IsNan(NewBottom) then
    Result.Y:=Result.Y+NewBottom;

  if Commit then
  begin
    ChildNode.Left:=NewLeft;
    ChildNode.Top:=NewTop;
    ChildNode.Right:=NewRight;
    ChildNode.Bottom:=NewBottom;
    ChildNode.Width:=NewWidth;
    ChildNode.Height:=NewHeight;

    // used borderbox
    r.Left:=NewLeft+ChildNode.MarginLeft;
    r.Top:=NewTop+ChildNode.MarginTop;
    r.Right:=NewLeft+FrameLeft+NewWidth+FrameRight-ChildNode.MarginRight;
    r.Bottom:=NewTop+FrameTop+NewHeight+FrameBottom-ChildNode.MarginBottom;
    ChildEl.UsedBorderBox:=r;

    // used contentbox
    r.Left:=NewLeft+FrameLeft;
    r.Top:=NewTop+FrameTop;
    r.Right:=r.Left+NewWidth;
    r.Bottom:=R.Top+NewHeight;
    ChildEl.UsedContentBox:=r;

    {$IFDEF VerboseFresnelPlacing}
    writeln('TFLLineLayouter.PlaceAbsoluteItem '+ChildEl.GetPath+' BorderBox='+ChildEl.UsedBorderBox.ToString);
    //if ChildEl.Name='Div1' then
    //writeln('TFLLineLayouter.PlaceAbsoluteItem ',ChildEl.GetPath,' ',aMode,' Commit Left=',FloatToCSSStr(ChildNode.Left),',Top=',FloatToCSSStr(ChildNode.Top),',Right=',FloatToCSSStr(ChildNode.Right),',Bottom=',FloatToCSSStr(ChildNode.Bottom),' Width=',FloatToCSSStr(ChildNode.Width),' Height=',FloatToCSSStr(ChildNode.Height));
    {$ENDIF}
  end;
end;

function TFLLineLayouter.AddLineItem(ChildNode: TUsedLayoutNode; NodeClass: TLineItemClass
  ): TLineItem;
begin
  Result:=NodeClass.Create;
  if FLineItems=nil then
    FLineItems:=TFPList.Create;
  FLineItems.Add(Result);
  Result.Node:=ChildNode;
end;

procedure TFLLineLayouter.ClearAbsoluteItems;
var
  i: Integer;
begin
  if (FAbsoluteItems=nil) or (FAbsoluteItems.Count=0) then
    exit;
  for i:=0 to FAbsoluteItems.Count-1 do
     TObject(FAbsoluteItems[i]).Free;
  FAbsoluteItems.Clear;
end;

procedure TFLLineLayouter.ClearLineItems;
var
  i: Integer;
begin
  if (FLineItems=nil) or (FLineItems.Count=0) then
    exit;
  for i:=0 to FLineItems.Count-1 do
     TObject(FLineItems[i]).Free;
  FLineItems.Clear;
end;

function TFLLineLayouter.AddAbsoluteItem(ChildNode: TUsedLayoutNode; NodeClass: TLineItemClass
  ): TLineItem;
begin
  Result:=NodeClass.Create;
  if FAbsoluteItems=nil then
    FAbsoluteItems:=TFPList.Create;
  FAbsoluteItems.Add(Result);
  Result.Node:=ChildNode;
  Result.StaticLeft:=NaN;
  Result.StaticTop:=NaN;
end;

procedure TFLLineLayouter.ComputeChildAttributes(Item: TLineItem;
  El: TFresnelElement);
begin
  if Item=nil then ;
  if El=nil then ;
end;

destructor TFLLineLayouter.Destroy;
begin
  ClearAbsoluteItems;
  FreeAndNil(FAbsoluteItems);
  ClearLineItems;
  FreeAndNil(FLineItems);
  inherited Destroy;
end;

{ TViewportLayouter }

procedure TViewportLayouter.SetViewport(const AValue: TFresnelViewport);
var
  OldLNode: TFresnelLayoutNode;
begin
  if FViewport=AValue then Exit;
  if FViewport<>nil then
    FViewport.Layouter:=nil;
  FViewport:=AValue;
  if FViewport<>nil then
  begin
    FViewport.Layouter:=Self;
    if FViewport.LayoutNode<>nil then
    begin
      OldLNode:=FViewport.LayoutNode;
      FViewport.LayoutNode:=nil;
      OldLNode.Free;
    end;
    CreateLayoutNode(FViewport);
  end;
end;

procedure TViewportLayouter.ErrorLayout(const ID: int64; const Msg: string);
var
  s: String;
begin
  s:='['+IntToStr(ID)+'] '+Msg;
  FLLog(etError,['Error: TFresnelLayouter.ErrorLayout ',s]);
  raise EFresnelLayout.Create(s);
end;

procedure TViewportLayouter.Layout(El: TFresnelElement);
var
  i: Integer;
  Node: TUsedLayoutNode;
  ChildEl: TFresnelElement;
begin
  Node:=TUsedLayoutNode(El.LayoutNode);
  if Node.SkipLayout then exit;

  //writeln('TViewportLayouter.Layout ',Node.Element.GetPath,' Width=',FloatToCSSStr(Node.Width),' Height=',FloatToCSSStr(Node.Height),' Layouter=',DbgSName(Node.Layouter));

  // sort for z-index
  SortStackingContext(Node);

  // compute used layout lengths of children, with access to used grand child lengths
  for i:=0 to El.NodeCount-1 do
  begin
    ChildEl:=El.Nodes[i];
    TUsedLayoutNode(ChildEl.LayoutNode).ComputeUsedLengths(false);
  end;

  if Node.Layouter<>nil then
    Node.Layouter.Apply;

  for i:=0 to El.NodeCount-1 do
    Layout(El.Nodes[i]);
end;

function TViewportLayouter.CreateLayoutNode(El: TFresnelElement
  ): TUsedLayoutNode;
begin
  if El.LayoutNode<>nil then
    Result:=TUsedLayoutNode(El.LayoutNode)
  else begin
    Result:=TUsedLayoutNode.Create(nil);
    El.LayoutNode:=Result;
    Result.Element:=El;
  end;
end;

constructor TViewportLayouter.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

procedure TViewportLayouter.Apply;

  procedure UpdateLayoutNodes(El: TFresnelElement);
  var
    i: Integer;
    Node: TUsedLayoutNode;
  begin
    // create or free layout nodes
    UpdateLayoutNode(El);
    El.ComputeCSSAfterLayoutNode(Self);

    // compute used layout lengths like padding and width *without*
    // accessing children - strict top down computation
    Node:=El.LayoutNode as TUsedLayoutNode;
    Node.ResetUsedLengths;
    if not Node.SkipLayout then
      Node.ComputeUsedLengths(true);

    // create child layout nodes
    for i:=0 to El.NodeCount-1 do
      UpdateLayoutNodes(El.Nodes[i]);
  end;

var
  VPNode: TUsedLayoutNode;
  r: TFresnelRect;
begin
  if Viewport=nil then
    exit;

  // the viewport must be absolute positioned and sized
  if Viewport.ComputedDisplayOutside<>CSSRegistry.kwBlock then
    ErrorLayout(20221031182815,'TFresnelLayouter.Apply expected viewport.displayoutside=block, but found "'+CSSRegistry.Keywords[Viewport.ComputedDisplayOutside]+'"');
  if Viewport.ComputedDisplayInside<>CSSRegistry.kwFlowRoot then
    ErrorLayout(20221018142350,'TFresnelLayouter.Apply expected viewport.displayinside=flow-root, but found "'+CSSRegistry.Keywords[Viewport.ComputedDisplayInside]+'"');
  if Viewport.ComputedPosition<>CSSRegistry.kwAbsolute then
    ErrorLayout(20221031153911,'TFresnelLayouter.Apply expected viewport.position=absolute, but found "'+CSSRegistry.Keywords[Viewport.ComputedPosition]+'"');
  if Viewport.ComputedVisibility<>CSSRegistry.kwVisible then
    ErrorLayout(20240918103950,'TFresnelLayouter.Apply expected viewport.visibility=visible, but found "'+CSSRegistry.Keywords[Viewport.ComputedVisibility]+'"');

  r:=TFresnelRect.Create(0,0,Viewport.Width,Viewport.Height);
  Viewport.UsedBorderBox:=r;
  Viewport.UsedContentBox:=r;

  // update layout nodes and compute used lengths (top-down)
  UpdateLayoutNodes(Viewport);

  // sanity checks
  if not (Viewport.LayoutNode is TUsedLayoutNode) then
    ErrorLayout(20221031154042,'TFresnelLayouter.Apply expected viewport.LayoutNode is TSimpleFresnelLayoutNode');
  VPNode:=TUsedLayoutNode(Viewport.LayoutNode);
  if Viewport.NodeCount=0 then
    exit; // nothing to do

  // layout
  VPNode.ComputeUsedLengths(false);
  Layout(Viewport);
end;

procedure TViewportLayouter.UpdateLayouter(El: TFresnelElement;
  LNode: TUsedLayoutNode);
var
  LayouterClass: TFLNodeLayouterClass;
begin
  LayouterClass:=nil;
  if (not LNode.SkipLayout) and (El.NodeCount>0) then
  begin
    case El.ComputedDisplayInside of
    CSSRegistry.kwGrid:
      LayouterClass:=TFLGridLayouter;
    CSSRegistry.kwFlex:
      LayouterClass:=TFLFlexLayouter;
    else
      if El.IsBlockFormattingContext then
        LayouterClass:=TFLFlowLayouter;
    end;
  end;
  if (LayouterClass<>nil) then
  begin
    if (LNode.Layouter<>nil) and (LNode.Layouter.ClassType<>LayouterClass) then
      FreeAndNil(LNode.Layouter);
    if LNode.Layouter=nil then
    begin
      LNode.Layouter:=LayouterClass.Create(nil);
      LNode.Layouter.Node:=LNode;
    end;
  end else begin
    if (LNode.Layouter<>nil) then
      FreeAndNil(LNode.Layouter);
  end;
end;

function TViewportLayouter.GetContainer(El: TFresnelElement): TFresnelElement;
begin
  case El.ComputedPosition of
  CSSRegistry.kwAbsolute:
    begin
      // the containing block is nearest ancestor element that has a position value other than static
      Result:=El.Parent;
      while (Result<>nil) do
      begin
        if not (Result.ComputedPosition in [CSSRegistry.kwStatic,CSSIDNone]) then
          exit;
        // Note:check for transform<>'none'
        Result:=Result.Parent;
      end;
    end;
  CSSRegistry.kwFixed:
    // viewport
  else
    // static, relative, sticky
    // the containing block is the nearest ancestor element that is either a
    // block container (such as an inline-block, block, or list-item element)
    // or establishes a formatting context (such as a table container,
    // flex container, grid container, or the block container itself)
    Result:=El.Parent;
    while (Result<>nil) do
    begin
      if Result.IsBlockFormattingContext then
        exit;
      Result:=Result.Parent;
    end;
  end;

  // default viewport
  if El.Parent=nil then
    Result:=nil
  else
    Result:=Viewport;
end;

procedure TViewportLayouter.UpdateLayoutNode(El: TFresnelElement);
var
  LNode, ParentLNode: TUsedLayoutNode;
begin
  // every node gets a layout node
  LNode:=CreateLayoutNode(El);
  LNode.SkipLayout:=false;
  LNode.SkipRendering:=false;

  // inherit flags
  if El.Parent<>nil then
  begin
    ParentLNode:=TUsedLayoutNode(El.Parent.LayoutNode);
    if ParentLNode.SkipLayout then
      LNode.SkipLayout:=true;
    if ParentLNode.SkipRendering then
      LNode.SkipRendering:=true;
  end
  else
    ParentLNode:=nil;

  // display-box
  if El.ComputedDisplayOutside in [CSSIDNone,CSSRegistry.kwNone] then
  begin
    LNode.SkipLayout:=true;
    LNode.SkipRendering:=true;
  end;

  // visibility
  case El.ComputedVisibility of
  CSSRegistry.kwHidden:
    LNode.SkipRendering:=true;
  CSSRegistry.kwCollapse:
    begin
      LNode.SkipLayout:=true;
      LNode.SkipRendering:=true;
    end;
  end;

  // rendering hierarchy
  if (El.Parent=nil) or LNode.SkipRendering then
    LNode.Parent:=nil
  else
    LNode.Parent:=El.Parent.LayoutNode;
  if El.ComputedPosition<>CSSRegistry.kwStatic then
    LNode.ZIndex:=TFresnelLength(El.GetComputedZIndex)+0.5
  else
    LNode.ZIndex:=0;

  // layouter
  UpdateLayouter(El,LNode);

  // block container
  LNode.Container:=GetContainer(El);

  // fetch basic CSS layout values
  if LNode.Layouter<>nil then
    LNode.Layouter.Init;
end;

procedure TViewportLayouter.SortStackingContext(LNode: TUsedLayoutNode
  );

  function IsSorted: boolean;
  var
    NextNode, Node: TUsedLayoutNode;
    i: Integer;
  begin
    if LNode.NodeCount<=1 then
      exit(true);
    i:=LNode.NodeCount-1;
    NextNode:=TUsedLayoutNode(LNode.Nodes[i]);
    while i>0 do
    begin
      dec(i);
      Node:=TUsedLayoutNode(LNode.Nodes[i]);
      if (Node.ZIndex>NextNode.ZIndex) then
        exit(false); // need sorting
      NextNode:=Node;
    end;
    Result:=true;
  end;

begin
  if IsSorted then exit;
  LNode.SortNodes(@CompareLayoutNodesZIndex);
  {$IFDEF VerboseFresnelLayouter}
  if not IsSorted then
    raise Exception.Create('20221031180116 TSimpleFresnelLayouter.SortLayoutNodes');
  {$ENDIF}
end;

procedure TViewportLayouter.WriteRenderingTree;

  procedure WriteNode(const Prefix: string; Node: TUsedLayoutNode);
  var
    i: Integer;
    El: TFresnelElement;
  begin
    El:=Node.Element;
    FLLog(etError,[El.Name,' NodeCount=',IntToStr(Node.NodeCount),' display-outside=',CSSRegistry.Keywords[El.ComputedDisplayOutside],' display-inside=',CSSRegistry.Keywords[El.ComputedDisplayInside],' position=',CSSRegistry.Keywords[El.ComputedPosition],' z-index=',El.ComputedAttribute[fcaZIndex]]);
    if Node.Layouter<>nil then
      FLLog(etDebug,[' layouter=',Node.Layouter.ClassName]);
    FLLog(etDebug,'');
    for i:=0 to Node.NodeCount-1 do
      WriteNode(Prefix+'  ',TUsedLayoutNode(Node.Nodes[i]));
  end;

begin
  if Viewport=nil then
  begin
    FLLog(etError,'TSimpleFresnelLayouter.WriteLayoutTree Viewport=nil');
    exit;
  end;
  if Viewport.LayoutNode=nil then
  begin
    FLLog(etError,'TSimpleFresnelLayouter.WriteLayoutTree Viewport.LayoutNode=nil');
    exit;
  end;
  FLLog(etDebug,'TSimpleFresnelLayouter.WriteLayoutTree BEGIN======================');
  WriteNode('',TUsedLayoutNode(Viewport.LayoutNode));
  FLLog(etDebug,'TSimpleFresnelLayouter.WriteLayoutTree END========================');
end;

end.

