{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Basic Renderer class for Fresnel elements

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel.Renderer;

{$mode objfpc}{$H+}
{$modeswitch advancedrecords}

interface

uses
  Classes, SysUtils, Math, FPImage,
//  LazLoggerBase,
  Fresnel.Classes, Fresnel.DOM, Fresnel.Controls, Fresnel.Layouter;

const
  DoublePi = 2*Pi;

type

  TFresnelRenderer = class(TComponent,IFresnelRenderer)
  private
    FSubPixel: boolean;
    FOrigin: TFresnelPoint;
    FTextShadows : TFresnelTextShadowArray;
  protected
    type
      { TBorderAndBackground }

      TBorderAndBackground = class
      Private
        FHasBorder : TCalcBoolean;
        FHasRadius : TCalcBoolean;
        FSameBorderWidth : TCalcBoolean;
        FRenderer : TFresnelRenderer;
      public
        BoundingBox: TFresnelRoundRect;
        Width: array[TFresnelCSSSide] of TFresnelLength;
        Color: array[TFresnelCSSSide] of TFPColor;
        BackgroundColorFP: TFPColor;
        BackgroundImage: TFresnelCSSBackgroundInfo;
        Constructor Create(aRenderer : TFresnelRenderer);
        destructor Destroy; override;
        procedure NormStroke(var aNorm: TFresnelLength; NoNegative: Boolean); inline;
        procedure Normalize;
        function HasBorder : Boolean;
        function SameBorderWidth : Boolean;
        function HasRadius : Boolean;
        property Renderer : TFresnelRenderer read FRenderer;
      end;
  protected
    // Not in IFresnelRenderer
    // Create backend-specific TBorderAndBackground if needed
    function CreateBorderAndBackground : TBorderAndBackground; virtual;
    // Prepare background and border drawing. Return false if no background/border drawing needed.
    function PrepareBackgroundBorder(El: TFresnelElement; Params: TBorderAndBackground) : Boolean; virtual;
    // Draw the background of the element. This is called before drawing the border. Not called if PrepareBackgroundBorder returned False.
    procedure DrawElBackground(El: TFresnelElement; Params: TBorderAndBackground); virtual;
    // Draw the border of the element. This is called after drawing the background. Not called if PrepareBackgroundBorder returned False.
    procedure DrawElBorder(El: TFresnelElement; Params: TBorderAndBackground); virtual;
    // Draw an element
    procedure DrawElement(El: TFresnelElement); virtual;
    // Draw the children of the element
    procedure DrawChildren(El: TFresnelElement); virtual;
    // Set the origin of the currently drawn element.
    procedure SetOrigin(const AValue: TFresnelPoint); virtual;
    // Get the origin of the currently drawn element.
    function GetOrigin : TFresnelPoint; virtual;
    // Minimum stroke width. If a length is less than this, it will be considered zero.
    class function GetMinStrokeWidth : TFresnelLength;
    // Normalize a length. If it is less than minimum stroke width, 0 is returned.
    // if NoNegative is True, then negative values will be changed to 0.
    class function NormalizeLength(s: TFresnelLength; NoNegative: boolean) : TFresnelLength;
  public
    //
    // Put methods not part of IFresnelRenderer here
    //

    // draw the given viewport.
    procedure Draw(Viewport: TFresnelViewport); virtual;
    // Round coordinates of given rectangle.
    procedure MathRoundRect(var r: TFresnelRect);
    // Is the rendered capable of SubPixel rendering ?
    property SubPixel: boolean read FSubPixel write FSubPixel;
  Public
    {  IFresnelRenderer }
    // Add 1 Shadow text to
    procedure AddTextShadow(const aX, aY: TFresnelLength; const aColor: TFPColor; const aRadius: TFresnelLength);
    // Clear all text shadows
    procedure ClearTextShadows;
    // Get reference to TFresnelTextShadow. Index between 0 and GetTextShadowCount-1
    function GetTextShadow(aIndex : Integer): PFresnelTextShadow;
    // Number of TextShadows that will be applied
    function GetTextShadowCount: Integer;
    // Draw and fill a rectangle with given boundaries and color.
    procedure FillRect(const aColor: TFPColor; const aRect: TFresnelRect); virtual; abstract;
    // Draw an elliptic arc with with given center and radii, from start to stop angle in rad (0=right), using specified color.
    procedure Arc(const aColor : TFPColor; const aCenter, aRadii : TFresnelPoint; aStartAngle : TFresnelLength = 0; aStopAngle : TFresnelLength = DoublePi); virtual; abstract;
    // Draw (and optionally fill) a rounded rectangle with given boundaries and color.
    procedure RoundRect(const aColor: TFPColor; const aRect: TFresnelRoundRect; Fill : Boolean); virtual; abstract;
    // Draw a line from point A (x1,y1) to B (x2,y2) using given color.
    procedure Line(const aColor: TFPColor; const x1, y1, x2, y2: TFresnelLength); virtual; abstract;
    // Draw a text (aText) at aTop,aLeft using given color and font.
    procedure TextOut(const aLeft, aTop: TFresnelLength; const aFont: IFresnelFont; const aColor: TFPColor; const aText: string); virtual; abstract;
    // Draw an image
    procedure DrawImage(const aLeft, aTop, aWidth, aHeight: TFresnelLength; const aImage: TFPCustomImage); virtual; abstract;
    // Origin of the currently drawn
    property Origin: TFresnelPoint read GetOrigin write SetOrigin;
    // Number of TextShadows that will be applied
    property TextShadowCount : Integer Read GetTextShadowCount;
    // Indexed access to TFresnelTextShadow references . Index between 0 and TextShadowCount-1
    property TextShadow[aIndex : Integer] : PFresnelTextShadow read GetTextShadow ;
  end;
  TFresnelRendererClass = class of TFresnelRenderer;

function DiameterToArc90DegCount(Diameter: single): integer; overload; // number of points to draw a 90deg arc
function RoundCorner2Polygon(const aRect: TFresnelRect; Corner: TFresnelCSSCorner): TFresnelPointArray; overload;
function RoundRect2Polygon(aRect: TFresnelRoundRect): TFresnelPointArray; overload;
procedure NormalizeRoundRect(var aRect: TFresnelRoundRect); overload;
procedure SwapFreLen(var a, b: TFresnelLength);

implementation

function DiameterToArc90DegCount(Diameter: single): integer;
// returns the minimum number of sub arcs a 90 degree arc has to be divided
// so that the points of the polygon has a deviation of less than 0.4
const
  NToDiameter: array[1..20] of integer = (
   1, // diameter 1..2 need 1 arc per 90degree
   3, // diameter 3..10 need 2 arcs per 90degree
   11,
   24,
   42,
   65,
   94,
   128,
   167,
   211,
   260,
   314,
   374,
   439,
   509,
   584,
   665,
   750,
   841,
   937
    );

// The above values were computed by choosing the minimum number of points,
// so that the error - the distance between the edges and the mathematical circle
// is less than the rounding error (0.5), for a smoother step 0.4.
//n_points:=0;
//repeat
//  inc(n_points,4);
//  t := single(1) / single(n_points) * 2 * Pi;
//  SinCos(t,SinT,CosT);
//  AX := MaxR * CosT;
//  AY := MaxR * SinT;
//  SinCos(t/2,SinT,CosT);
//  BX := MaxR * CosT;
//  BY := MaxR * SinT;
//  CX := (AX + MaxR) /2;
//  CY := (AY + 0) /2;
//  Deviation := sqrt(sqr(BX-CX)+sqr(BY-CY));
//until Deviation<0.4;
begin
  for Result:=0 to high(NToDiameter)-1 do
    if Diameter<NToDiameter[Result+1] then
      exit;

  // use a heuristic
  Result:=round(Sqrt(Diameter/2.5));
end;

function RoundCorner2Polygon(const aRect: TFresnelRect;
  Corner: TFresnelCSSCorner): TFresnelPointArray;
// returns the points of a 90degree corner arc, going clockwise.
var
  n_points, i: Integer;
  Xc, Yc, a, b, MaxR, AddRad, Rad, CosT, SinT: TFresnelLength;
begin
  Xc:=(aRect.Left+aRect.Right)/2;
  Yc:=(aRect.Top+aRect.Bottom)/2;
  a:=(aRect.Width)/2;
  b:=(aRect.Height)/2;
  MaxR:=Max(abs(a),abs(b));

  n_points:=DiameterToArc90DegCount(MaxR*2)+1;
  SetLength(Result{%H-}, n_points);

  case Corner of
  fcsTopLeft:
    begin
      AddRad:=Pi;
      Result[0].X:=aRect.Left;
      Result[0].Y:=aRect.Top+b;
      if n_points>0 then
      begin
        Result[n_points-1].X:=aRect.Left+a;
        Result[n_points-1].Y:=aRect.Top;
      end;
    end;
  fcsTopRight:
    begin
      Addrad:=Pi*3/2;
      Result[0].X:=aRect.Right-a;
      Result[0].Y:=aRect.Top;
      if n_points>0 then
      begin
        Result[n_points-1].X:=aRect.Right;
        Result[n_points-1].Y:=aRect.Top+b;
      end;
    end;
  fcsBottomLeft:
    begin
      AddRad:=Pi/2;
      Result[0].X:=aRect.Left+a;
      Result[0].Y:=aRect.Bottom;
      if n_points>0 then
      begin
        Result[n_points-1].X:=aRect.Left;
        Result[n_points-1].Y:=aRect.Bottom-b;
      end;
    end;
  fcsBottomRight:
    begin
      AddRad:=0;
      Result[0].X:=aRect.Right;
      Result[0].Y:=aRect.Bottom-b;
      if n_points>0 then
      begin
        Result[n_points-1].X:=aRect.Right-a;
        Result[n_points-1].Y:=aRect.Bottom;
      end;
    end;
  end;

  for i := 1 to n_points-2 do
  begin
    Rad := single(i) / single(n_points) / 4 * 2 * Pi + AddRad;
    SinCos(Rad,SinT,CosT);
    Result[i].X := Xc + a * CosT;
    Result[i].Y := Yc + b * SinT;
  end;
end;

function RoundRect2Polygon(aRect: TFresnelRoundRect): TFresnelPointArray;
var
  W, H: TFresnelLength;
  BottomRight, BottomLeft, TopLeft, TopRight: TFresnelPointArray;
  r: TFresnelRect;
begin
  Result:=[];
  NormalizeRoundRect(aRect);
  W:=aRect.Box.Width;
  H:=aRect.Box.Height;
  if (W<0) or (H<0) then exit;

  // bottom-right
  if aRect.Radii[fcsBottomRight].X>0 then
  begin
    r.Left:=aRect.Box.Right-2*aRect.Radii[fcsBottomRight].X;
    r.Right:=aRect.Box.Right;
    r.Top:=aRect.Box.Bottom-2*aRect.Radii[fcsBottomRight].Y;
    r.Bottom:=aRect.Box.Bottom;
    BottomRight:=RoundCorner2Polygon(r,fcsBottomRight);
  end else begin
    BottomRight:=[PointFre(aRect.Box.Right,aRect.Box.Bottom)];
  end;
  // bottom-left
  if aRect.Radii[fcsBottomLeft].X>0 then
  begin
    r.Left:=aRect.Box.Left;
    r.Right:=aRect.Box.Left+2*aRect.Radii[fcsBottomLeft].X;
    r.Top:=aRect.Box.Bottom-2*aRect.Radii[fcsBottomLeft].Y;
    r.Bottom:=aRect.Box.Bottom;
    BottomLeft:=RoundCorner2Polygon(r,fcsBottomLeft);
  end else begin
    BottomLeft:=[PointFre(aRect.Box.Left,aRect.Box.Bottom)];
  end;
  // top-left
  if aRect.Radii[fcsTopLeft].X>0 then
  begin
    r.Left:=aRect.Box.Left;
    r.Right:=aRect.Box.Left+2*aRect.Radii[fcsTopLeft].X;
    r.Top:=aRect.Box.Top;
    r.Bottom:=aRect.Box.Top+2*aRect.Radii[fcsTopLeft].Y;
    TopLeft:=RoundCorner2Polygon(r,fcsTopLeft);
  end else begin
    TopLeft:=[PointFre(aRect.Box.Left,aRect.Box.Top)];
  end;
  // top-right
  if aRect.Radii[fcsTopRight].X>0 then
  begin
    r.Left:=aRect.Box.Right-2*aRect.Radii[fcsTopRight].X;
    r.Right:=aRect.Box.Right;
    r.Top:=aRect.Box.Top;
    r.Bottom:=aRect.Box.Top+2*aRect.Radii[fcsTopRight].Y;
    TopRight:=RoundCorner2Polygon(r,fcsTopRight);
  end else begin
    TopRight:=[PointFre(aRect.Box.Right,aRect.Box.Top)];
  end;

  Result:=Concat(BottomRight,BottomLeft,TopLeft,TopRight);
end;

procedure NormalizeRoundRect(var aRect: TFresnelRoundRect);
var
  W, H, MaxRadiusX, MaxRadiusY: TFresnelLength;
  c: TFresnelCSSCorner;
begin
  W:=aRect.Box.Width;
  H:=aRect.Box.Width;
  if W<0 then
  begin
    SwapFreLen(aRect.Box.Left,aRect.Box.Right);
    W:=-W;
  end;
  if H<0 then
  begin
    SwapFreLen(aRect.Box.Top,aRect.Box.Bottom);
    H:=-H;
  end;
  if (W=0) or (H=0) then
    exit;

  MaxRadiusX:=W/2;
  MaxRadiusY:=H/2;

  for c in TFresnelCSSCorner do
  begin
    aRect.Radii[c].X:=Min(Max(0,aRect.Radii[c].X),MaxRadiusX);
    aRect.Radii[c].Y:=Min(Max(0,aRect.Radii[c].Y),MaxRadiusY);
    if aRect.Radii[c].X=0 then
      aRect.Radii[c].Y:=0
    else if aRect.Radii[c].Y=0 then
      aRect.Radii[c].X:=0;
  end;
end;

procedure SwapFreLen(var a, b: TFresnelLength);
var
  h: TFresnelLength;
begin
  h:=a;
  a:=b;
  b:=h;
end;

{ TFresnelRenderer }

procedure TFresnelRenderer.SetOrigin(const AValue: TFresnelPoint);
begin
  if CompareFresnelPoint(FOrigin,AValue)=0 then Exit;
  FOrigin:=AValue;
end;

function TFresnelRenderer.GetOrigin: TFresnelPoint;
begin
  Result:=Forigin;
end;

class function TFresnelRenderer.GetMinStrokeWidth: TFresnelLength;
begin
  Result:=0.09;
end;

class function TFresnelRenderer.NormalizeLength(s: TFresnelLength; NoNegative: boolean): TFresnelLength;
begin
  if NoNegative and (s<0) then
    Exit(0);
  if SameValue(s,0,MinStrokeWidth) then
    Exit(0);
  Result:=S;
end;

function TFresnelRenderer.GetTextShadow(aIndex : Integer): PFresnelTextShadow;
begin
  Result:=Nil;
  If (aIndex>=0) and (aIndex<Length(FTextShadows)) then
    Result:=@FTextShadows[aIndex];
end;

function TFresnelRenderer.GetTextShadowCount: Integer;
begin
  Result:=Length(FTextShadows);
end;


procedure TFresnelRenderer.AddTextShadow(const aX, aY: TFresnelLength; const aColor: TFPColor; const aRadius: TFresnelLength);
var
  Len : Integer;

begin
  Len:=Length(FTextShadows);
  SetLength(FTextShadows,Len+1);
  FTextShadows[Len].Color:=aColor;
  FTextShadows[Len].Radius:=aRadius;
  FTextShadows[Len].Offset.X:=aX;
  FTextShadows[Len].Offset.Y:=aY;
end;

function TFresnelRenderer.CreateBorderAndBackground: TBorderAndBackground;
begin
  Result:=TBorderAndBackground.Create(Self);
end;

function TFresnelRenderer.PrepareBackgroundBorder(El: TFresnelElement; Params: TBorderAndBackground): Boolean;

var
  Grad : TFresnelCSSLinearGradient;

begin
  if El=nil then ;
  Params.Normalize;
  if (Params.BackgroundImage is TFresnelCSSLinearGradient) then
    begin
    Grad:=Params.BackgroundImage as TFresnelCSSLinearGradient;
    // If none specified, use vertical gradient
    if Grad.StartPoint=Grad.EndPoint then
      begin
      // Vertical gradient
      Grad.StartPoint:=Params.BoundingBox.Box.TopLeft;
      Grad.StartPoint.Offset(Origin);
      Grad.EndPoint:=Grad.StartPoint;
      Grad.EndPoint.OffSet(0,Params.BoundingBox.Box.Height);
      end;
    end;
  Result:=True;
end;

procedure TFresnelRenderer.DrawElBackground(El: TFresnelElement; Params: TBorderAndBackground);
begin
  if el=nil then;
  if Params.BackgroundColorFP.Alpha>alphaTransparent then
  begin
    //FLLog(etDebug,'TFresnelRenderer.DrawElBorder drawing background %s',[El.GetPath]);
    FillRect(Params.BackgroundColorFP,Params.BoundingBox.Box);
  end;
end;

procedure TFresnelRenderer.MathRoundRect(var r: TFresnelRect);
begin
  r.Left:=round(r.Left);
  r.Right:=round(r.Right);
  r.Top:=round(r.Top);
  r.Bottom:=round(r.Bottom);
end;

procedure TFresnelRenderer.DrawElBorder(El: TFresnelElement;
  Params: TBorderAndBackground);
var
  i: Integer;
  s: TFresnelCSSSide;
  c: TFPColor;
begin
  if El=nil then ;
  //FLLog(etDebug,'TFresnelRenderer.DrawElBorder drawing border %s',[El.GetPath]);
  for s in TFresnelCSSSide do
  begin
    c:=Params.Color[s];
    if c.Alpha=alphaTransparent then continue;
    for i:=0 to ceil(Params.Width[s])-1 do
      With Params.BoundingBox.Box do
        case s of
        ffsLeft: Line(c,Left+i,Top,Left+i,Bottom);
        ffsTop: Line(c,Left,Top+i,Right,Top+i);
        ffsRight: Line(c,Right-i,Top,Right-i,Bottom);
        ffsBottom: Line(c,Left,Bottom-i,Right,Bottom-i);
        end;
  end;
end;

procedure TFresnelRenderer.DrawElement(El: TFresnelElement);
var
  LNode: TUsedLayoutNode;
  aBorderBox, aContentBox: TFresnelRect;
  BorderParams: TBorderAndBackground;
  aRenderable : IFresnelRenderable;
  s: TFresnelCSSSide;
  Corner: TFresnelCSSCorner;
begin
  FLLog(etDebug,'TFresnelRenderer.DrawElement %s Origin=%s',[El.GetPath,Origin.ToString]);
  LNode:=TUsedLayoutNode(El.LayoutNode);
  if LNode.SkipRendering then exit;
  aRenderable:=El as IFresnelRenderable;
  aRenderable.BeforeRender;
  El.Rendered:=true;

  aBorderBox:=El.UsedBorderBox;
  El.RenderedBorderBox:=aBorderBox;

  aContentBox:=El.UsedContentBox;
  El.RenderedContentBox:=aContentBox;

  FLLog(etDebug,'TFresnelRenderer.DrawElement %s %s',[El.GetPath,aBorderBox.ToString]);

  //writeln('TFresnelRenderer.DrawElement ',El.Name,' BorderBox=',El.RenderedBorderBox.ToString,' ContentBox=',El.RenderedContentBox.ToString);

  BorderParams:=CreateBorderAndBackground;
  try
    BorderParams.BoundingBox.Box:=aBorderBox;
    if not SubPixel then
      MathRoundRect(BorderParams.BoundingBox.Box);

    // border-width
    BorderParams.Width[ffsLeft]:=LNode.BorderLeft;
    BorderParams.Width[ffsTop]:=LNode.BorderTop;
    BorderParams.Width[ffsRight]:=LNode.BorderRight;
    BorderParams.Width[ffsBottom]:=LNode.BorderBottom;

    // background-color
    BorderParams.BackgroundColorFP:=El.GetComputedColor(fcaBackgroundColor,colTransparent);

    // border-color
    for s in TFresnelCSSSide do
      BorderParams.Color[s]:=El.GetComputedColor(TFresnelCSSAttribute(ord(fcaBorderTopColor)+ord(s)),colTransparent);

    // border-image
    BorderParams.BackgroundImage:=El.GetComputedImage(fcaBackgroundImage);

    // border-radius
    for Corner in TFresnelCSSCorner do
      BorderParams.BoundingBox.Radii[Corner]:=El.GetComputedBorderRadius(Corner);
    // Normalize
    if PrepareBackgroundBorder(El,BorderParams) then
      begin
      // Background
      DrawElBackground(El,BorderParams);
      // Border
      DrawElBorder(El,BorderParams);
      end;
  finally
    BorderParams.Free;
  end;

  // Give element a chance to draw itself
  aRenderable.Render(Self as IFresnelRenderer);

  DrawChildren(El);

  aRenderable.AfterRender;
end;

procedure TFresnelRenderer.DrawChildren(El: TFresnelElement);
var
  OldOrigin: TFresnelPoint;
  LNode: TUsedLayoutNode;
  i: Integer;
  ChildEl: TFresnelElement;
begin
  LNode:=TUsedLayoutNode(El.LayoutNode);

  OldOrigin:=Origin;
  Origin:=OldOrigin+El.RenderedContentBox.TopLeft;
  //writeln('TFresnelRenderer.DrawChildren ',El.GetPath,' Old=',OldOrigin.ToString,' Origin=',Origin.ToString);
  for i:=0 to LNode.NodeCount-1 do
  begin
    ChildEl:=TUsedLayoutNode(LNode.Nodes[i]).Element;
    //writeln('TFresnelRenderer.DrawChildren ',El.GetPath,' Child=',ChildEl.GetPath);
    DrawElement(ChildEl);
  end;
  Origin:=OldOrigin;
end;

procedure TFresnelRenderer.Draw(Viewport: TFresnelViewport);
var
  aContentBox: TFresnelRect;
  BackgroundColorFP: TFPColor;
  aRenderable: IFresnelRenderable;
begin
  //debugln(['TFresnelRenderer.Draw Origin=',dbgs(Origin)]);
  aContentBox.Left:=0;
  aContentBox.Top:=0;
  aContentBox.Right:=Viewport.Width;
  aContentBox.Bottom:=Viewport.Height;
  Viewport.UsedBorderBox:=aContentBox;
  Viewport.UsedContentBox:=aContentBox;
  aRenderable:=Viewport as IFresnelRenderable;
  aRenderable.BeforeRender;
  Viewport.Rendered:=true;

  BackgroundColorFP:=Viewport.GetComputedColor(fcaBackgroundColor,colWhite);
  FillRect(BackgroundColorFP,aContentBox);

  aRenderable.Render(Self as IFresnelRenderer);
  DrawChildren(Viewport);

  aRenderable.AfterRender;
end;

procedure TFresnelRenderer.ClearTextShadows;
begin
  SetLength(FTextShadows,0);
end;

{ TFresnelRenderer.TBorderAndBackground }

constructor TFresnelRenderer.TBorderAndBackground.Create(aRenderer: TFresnelRenderer);
begin
  FRenderer:=aRenderer;
end;

destructor TFresnelRenderer.TBorderAndBackground.Destroy;
begin
  FreeAndNil(BackgroundImage);
  inherited Destroy;
end;

procedure TFresnelRenderer.TBorderAndBackground.NormStroke(var aNorm: TFresnelLength; NoNegative : Boolean);

begin
  aNorm:=Renderer.NormalizeLength(aNorm,NoNegative);
end;

procedure TFresnelRenderer.TBorderAndBackground.Normalize;

var
  Side : TFresnelCSSSide;
  Corner : TFresnelCSSCorner;
begin
  for Side in TFresnelCSSSide do
    NormStroke(Width[Side],True);
  With BoundingBox do
    for Corner in TFresnelCSSCorner do
      begin
      NormStroke(Radii[Corner].X,True);
      NormStroke(Radii[Corner].Y,True);
      end;
end;

function TFresnelRenderer.TBorderAndBackground.HasBorder: Boolean;
begin
  if FHasBorder=cbCalc then
    FHasBorder:=(Width[ffsLeft]>0)
                or (Width[ffsRight]>0)
                or (Width[ffsTop]>0)
                or (Width[ffsBottom]>0);
  Result:=FHasBorder;
end;

function TFresnelRenderer.TBorderAndBackground.SameBorderWidth: Boolean;

var
  Side : TFresnelCSSSide;
  Ref : TFresnelLength;

begin
  if FSameBorderWidth=cbcalc then
    begin
    FSameBorderWidth:=cbtrue;
    Ref:=NormalizeStroke(Width[Low(Side)],True);
    for Side:=Succ(Low(TFresnelCSSSide)) to High(TFresnelCSSSide) do
      begin
      if (not SameValue(NormalizeStroke(Width[Side],True),Ref)) then
        FSameBorderWidth:=cbfalse;
      end;
    end;
  Result:=FSameBorderWidth;
end;

function TFresnelRenderer.TBorderAndBackground.HasRadius: Boolean;

var
  Corner : TFresnelCSSCorner;
begin
  if (FHasRadius=cbCalc) then
    begin
    FHasRadius:=cbFalse;
    With BoundingBox Do
      for Corner in TFresnelCSSCorner do
      begin
        if (NormalizeStroke(Radii[Corner].X,True)>0)
            and (NormalizeStroke(Radii[Corner].Y,True)>0) then
        begin
          FHasRadius:=cbtrue;
          break;
        end;
      end;
    end;
  Result:=FHasRadius;
end;

end.

