{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    String constants for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel.StrConsts;

{$mode objfpc}{$H+}

interface

resourcestring
  rsFormResourceSNotFoundForResourcelessFormsCreateNew = 'Form resource %s '
    +'not found. For resourceless forms CreateNew constructor must be used.';
  rsInvalidPropertyValue = 'Invalid property value';

implementation

end.

