{
 *****************************************************************************
  This file is part of Fresnel.

  See the file COPYING.modifiedLGPL.txt, included in this distribution,
  for details about the license.
 *****************************************************************************

ToDo:
- speed up GetCSSIndex
- speed up GetCSSDepth
- speed up GetCSSNextOfType
- speed up GetCSSPreviousOfType
}
unit Fresnel.DOM;

{$mode ObjFPC}{$H+}
{$IF FPC_FULLVERSION>30300}
  {$WARN 6060 off} // Case statement does not handle all possible cases
{$ENDIF}
{$Interfaces CORBA} // no reference counting
{$ModeSwitch AdvancedRecords}

interface

uses
  Classes, SysUtils, Math, FPImage,
  {$IF FPC_FULLVERSION>30300}
  sortbase,
  {$ENDIF}
  fpCSSTree, fpCSSResParser, fpCSSResolver, fpCSSScanner, fpCSSParser, FCL.Events,
  Fresnel.Classes, Fresnel.Events;

type

  { EFresnelFont }

  EFresnelFont = class(EFresnel)
  end;

const
  FresnelDefaultDPI = 96;
  FresnelDefaultFontSize = 10;
  FresnelFontWeightNormal = 400;

type
  // CSS attributes of the TFresnelElement class
  // Important:
  //  shorthands after their longhands
  //  for sides Left,Top,Right,Border use the order defined in TFresnelCSSSide
  //  for corners TopLeft,TopRight,BottomLeft,BottomRight use the order defined in TFresnelCSSCorner
  TFresnelCSSAttribute = (
    fcaFloat, // can modify display
    fcaDisplay,
    fcaPosition,
    fcaBoxSizing, // border-box, content-box
    fcaDirection,
    fcaWritingMode,
    fcaZIndex,
    fcaOverflowX, // visible|hidden|clip|scroll|auto
    fcaOverflowY,
    fcaOverflow, // x y
    fcaClear,
    fcaTop, // including margin
    fcaRight, // including margin
    fcaBottom, // including margin
    fcaLeft, // including margin
    fcaMinWidth, // max before min
    fcaMaxWidth, // max before min
    fcaMinHeight, // max before min
    fcaMaxHeight, // max before min
    fcaWidth, // only content, depending on box-sizing excluding padding and border
    fcaHeight, // only content, depending on box-sizing excluding padding and border
    fcaBorderTopWidth,
    fcaBorderRightWidth,
    fcaBorderBottomWidth,
    fcaBorderLeftWidth,
    fcaBorderWidth, // shorthand for border-[left,right,top,bottom]-width
    fcaBorderTopColor,
    fcaBorderRightColor,
    fcaBorderBottomColor,
    fcaBorderLeftColor,
    fcaBorderColor, // shorthand for border-[left,right,top,bottom]-color
    fcaBorderTopStyle,
    fcaBorderRightStyle,
    fcaBorderBottomStyle,
    fcaBorderLeftStyle,
    fcaBorderStyle, // shorthand for border-[left,right,top,bottom]-style
    fcaBorderTop, // shorthand for border-top-[width,style,color]
    fcaBorderRight, // shorthand for border-right-[width,style,color]
    fcaBorderBottom, // shorthand for border-bottom-[width,style,color]
    fcaBorderLeft, // shorthand for border-left-[width,style,color]
    fcaBorder, // shorthand for border-width border-style border-color
    fcaBorderTopLeftRadius,
    fcaBorderTopRightRadius,
    fcaBorderBottomRightRadius,
    fcaBorderBottomLeftRadius,
    fcaBorderRadius, // shorthand for border-[top-left,top-right,bottom-right,bottom-left]-radius
    fcaFontFeatureSettings,
    fcaFontFamily,
    fcaFontKerning, // auto|normal|none
    fcaFontSize,  // units like em depend on this, medium|xx-small|x-small|small|large|x-large|xx-large|smaller|larger|LengthUnit|%
    fcaFontStyle, // normal|italic|oblique
    fcaFontWeight, // normal|bold|bolder|lighter|number
    fcaFontWidth, // normal, |semi|extra|ultra-condensed, |semi|extra|ultra-expanded,
    fcaFontStretch, // shorthand for font-width
    fcaFontVariantAlternates, // normal|historical-forms|...
    fcaFontVariantCaps, // normal|small-caps|all-small-caps|petite-caps|all-petite-caps|unicasetitling-caps
    fcaFontVariantEastAsian, // normal|ruby|jis78|jis83|jis90|jis04|simplified|traditional|full-width|proportional-width or any combination
    fcaFontVariantEmoji, // normal|text|emoji|unicode
    fcaFontVariantLigatures, // normal|none|common-ligatures|no-common-ligatures|discretionary-ligatures|no-discretionary-ligatures|historical-ligatures|no-historical-ligatures|contextual|no-contextual|
    fcaFontVariantNumeric, // normal|ordinal|slashed-zero|lining-nums|oldstyle-nums|proportional-nums|tabular-nums|diagonal-fractions|stacked-fractions|
    fcaFontVariantPosition, // normal|sub|super
    fcaFontVariant, // shorthand for font-variant-[alternates|caps|east-asian|emoji|ligatures|numeric|position]
    fcaLineHeight,
    fcaFont, // shorthand for font-family, -size, -stretch, - style, -variant, -weight, line-height
    fcaTextShadow,
    fcaMarginTop,
    fcaMarginRight,
    fcaMarginBottom,
    fcaMarginLeft,
    fcaMargin, // shorthand for margin-left, -right, -top, -bottom
    fcaMarginBlockEnd,
    fcaMarginBlockStart,
    fcaMarginBlock, // shorthand for margin-block-start, -end
    fcaMarginInlineEnd,
    fcaMarginInlineStart,
    fcaMarginInline, // shorthand for margin-inline-start, -end
    fcaOpacity,
    fcaPaddingTop,
    fcaPaddingRight,
    fcaPaddingBottom,
    fcaPaddingLeft,
    fcaPadding, // shorthand for padding-left, -right, -top, -bottom
    fcaVisibility,
    fcaBackgroundAttachment,
    fcaBackgroundClip,
    fcaBackgroundColor,
    fcaBackgroundImage,
    fcaBackgroundOrigin,
    fcaBackgroundPositionX,
    fcaBackgroundPositionY,
    fcaBackgroundPosition,
    fcaBackgroundRepeat,
    fcaBackgroundSize,
    fcaBackground, // shorthand for background-[attachment,clip,color,image,origin,position,repeat,size]
    fcaColor,     // text color
    fcaCursor,
    fcaScrollbarColor,
    fcaScrollbarGutter,
    fcaScrollbarWidth,
    fcaFlexBasis,
    fcaFlexDirection,
    fcaFlexGrow,
    fcaFlexShrink,
    fcaFlexWrap,
    fcaFlexFlow, // shorthand for flex-[direction,flex-wrap]
    fcaFlex, // shorthand for flex-[grow,shrink,basis]
    fcaAlignContent,
    fcaAlignItems,
    fcaAlignSelf,
    fcaJustifyContent,
    fcaJustifyItems,
    fcaJustifySelf,
    fcaPlaceContent, // shorthand for [align,justify]-content
    fcaPlaceItems, // shorthand for [align,justify]-items
    fcaPlaceSelf, // shorthand for [align,justify]-self
    fcaColumnGap,
    fcaRowGap,
    fcaGap // shorthand for [column,row]-gap
    );
  TFresnelCSSAttributes = set of TFresnelCSSAttribute;
  TFresnelCSSAttributeArray = array of TFresnelCSSAttribute;

const
  FresnelCSSAttributeNames: array[TFresnelCSSAttribute] of string = (
    // case sensitive!
    'float',
    'display',
    'position',
    'box-sizing',
    'direction',
    'writing-mode',
    'z-index',
    'overflow-x',
    'overflow-y',
    'overflow',
    'clear',
    'top',
    'right',
    'bottom',
    'left',
    'min-width',
    'max-width',
    'min-height',
    'max-height',
    'width',
    'height',
    'border-top-width',
    'border-right-width',
    'border-bottom-width',
    'border-left-width',
    'border-width',
    'border-top-color',
    'border-right-color',
    'border-bottom-color',
    'border-left-color',
    'border-color',
    'border-top-style',
    'border-right-style',
    'border-bottom-style',
    'border-left-style',
    'border-style',
    'border-top',
    'border-right',
    'border-bottom',
    'border-left',
    'border',
    'border-top-left-radius',
    'border-top-right-radius',
    'border-bottom-left-radius',
    'border-bottom-right-radius',
    'border-radius',
    'font-feature-settings',
    'font-family',
    'font-kerning',
    'font-size',
    'font-style',
    'font-weight',
    'font-width',
    'font-stretch',
    'font-variant-alternates',
    'font-variant-caps',
    'font-variant-east-asian',
    'font-variant-emoji',
    'font-variant-ligatures',
    'font-variant-numeric',
    'font-variant-position',
    'font-variant',
    'line-height',
    'font',
    'text-shadow',
    'margin-top',
    'margin-right',
    'margin-bottom',
    'margin-left',
    'margin',
    'margin-block-end',
    'margin-block-start',
    'margin-block',
    'margin-inline-end',
    'margin-inline-start',
    'margin-inline',
    'opacity',
    'padding-top',
    'padding-right',
    'padding-bottom',
    'padding-left',
    'padding',
    'visibility',
    'background-attachment',
    'background-clip',
    'background-color',
    'background-image',
    'background-origin',
    'background-position-x',
    'background-position-y',
    'background-position',
    'background-repeat',
    'background-size',
    'background',
    'color',
    'cursor',
    'scrollbar-color',
    'scrollbar-gutter',
    'scrollbar-width',
    'flex-basis',
    'flex-direction',
    'flex-grow',
    'flex-shrink',
    'flex-wrap',
    'flex-flow',
    'flex',
    'align-content',
    'align-items',
    'align-self',
    'justify-content',
    'justify-items',
    'justify-self',
    'place-content',
    'place-items',
    'place-self',
    'column-gap',
    'row-gap',
    'gap'
    );

type
  TFresnelCSSSide = (
    // one of four side(s)
    ffsTop,
    ffsRight,
    ffsBottom,
    ffsLeft
    );
  TFresnelCSSSides = set of TFresnelCSSSide;

  TFresnelCSSCorner = (
    // one of four corner(s)
    fcsTopLeft,
    fcsTopRight,
    fcsBottomRight,
    fcsBottomLeft
    );
  TFresnelCSSCorners = set of TFresnelCSSCorner;

  TFresnelCSSKerning = (
    fckAuto,
    fckNormal,
    fckNone
    );
  TFresnelCSSKernings = set of TFresnelCSSKerning;
const
  FresnelCSSKerningNames: array[TFresnelCSSKerning] of string = (
    'auto',
    'normal',
    'none'
    );

type
  TFresnelCSSFontVarCaps = (
    ffvcNormal,
    ffvcSmallCaps,
    ffvcAllSmallCaps,
    ffvcPetiteCaps,
    ffvcAllPetiteCaps,
    ffvcUnicase,
    ffvcTitlingCaps
    );
  TFresnelCSSFontVarCapsSet = set of TFresnelCSSFontVarCaps;
  TFresnelCSSFontVarEastAsian = (
    ffveaNormal,
    ffveaRuby,
    ffveaJis78,
    ffveaJis83,
    ffveaJis90,
    ffveaJis04,
    ffveaSimplified,
    ffveaTraditional,
    ffveaFullWidth,
    ffveaProportionalWidth);
  TFresnelCSSFontVarEastAsians = set of TFresnelCSSFontVarEastAsian;
  TFresnelCSSFontVarEmoji = (
    ffveNormal,
    ffveText,
    ffveEmoji,
    ffveUnicode
    );
  TFresnelCSSFontVarEmojis = set of TFresnelCSSFontVarEmoji;
  TFresnelCSSFontVarLigatures = (
    ffvlNormal,
    ffvlNone,
    ffvlCommonLigatures,
    ffvlNoCommonLigatures,
    ffvlDiscretionaryLigatures,
    ffvlNoDiscretionaryLigatures,
    ffvlHistoricalLigatures,
    ffvlNoHistoricalLigatures,
    ffvlContextual,
    ffvlNoContextual
    );
  TFresnelCSSFontVarLigaturesSet = set of TFresnelCSSFontVarLigatures;
  TFresnelCSSFontVarNumeric = (
    ffvnNormal,
    ffvnOrdinal,
    ffvnSlashedZero,
    ffvnLiningNums,
    ffvnOldstyleNums,
    ffvnProportionalNums,
    ffvnTabularNums,
    ffvnDiagonalFractions,
    ffvnStackedFractions
    );
  TFresnelCSSFontVarNumerics = set of TFresnelCSSFontVarNumeric;
  TFresnelCSSFontVarPosition = (
    ffvpNormal,
    ffvpSub,
    ffvpSuper
    );
  TFresnelCSSFontVarPositions = set of TFresnelCSSFontVarPosition;
const
  FresnelCSSFontVarCapsNames: array[TFresnelCSSFontVarCaps] of string = (
    'normal',
    'small-caps',
    'all-small-caps',
    'petite-caps',
    'all-petite-caps',
    'unicase',
    'titling-caps'
    );
  FresnelCSSFontVarEastAsianNames: array[TFresnelCSSFontVarEastAsian] of string = (
    'normal',
    'ruby',
    'jis78',
    'jis83',
    'jis90',
    'jis04',
    'simplified',
    'traditional',
    'full-width',
    'proportional-width'
    );
  FresnelCSSFontVarEmojiNames: array[TFresnelCSSFontVarEmoji] of string = (
    'normal',
    'text',
    'emoji',
    'unicode'
    );
  FresnelCSSFontVarLigaturesNames: array[TFresnelCSSFontVarLigatures] of string = (
    'normal',
    'none',
    'common-ligatures',
    'no-common-ligatures',
    'discretionary-ligatures',
    'no-discretionary-ligatures',
    'historical-ligatures',
    'no-historical-ligatures',
    'contextual',
    'no-contextual'
    );
  FresnelCSSFontVarNumericNames: array[TFresnelCSSFontVarNumeric] of string = (
    'normal',
    'ordinal',
    'slashed-zero',
    'lining-nums',
    'oldstyle-nums',
    'proportional-nums',
    'tabular-nums',
    'diagonal-fractions',
    'stacked-fractions'
    );
  FresnelCSSFontVarPositionNames: array[TFresnelCSSFontVarPosition] of string = (
    'normal',
    'sub',
    'super'
    );

type
  TFresnelCSSPseudoClass = (
    fcpcHover,
    fcpcFocus
    );
  TFresnelCSSPseudoClasses = set of TFresnelCSSPseudoClass;

const
  FresnelCSSPseudoClassNames: array[TFresnelCSSPseudoClass] of string = (
    'hover',
    'focus'
    );

Type

  { TFresnelTextShadow }

  TFresnelTextShadow = record
    Offset : TFresnelPoint;
    Radius : TFresnelLength;
    Color : TFPColor;
  end;
  PFresnelTextShadow = ^TFresnelTextShadow;
  TFresnelTextShadowArray = Array of TFresnelTextShadow;

  { TFresnelRoundRect }

  TFresnelRoundRect = record
    Box : TFresnelRect;
    Radii: array[TFresnelCSSCorner] of TFresnelPoint;
    function ToString : String;
  end;

type
  TFresnelViewport = class;
  TFresnelElement = class;

  { TFresnelElementAttrDesc }

  TFresnelElementAttrDesc = class(TCSSAttributeDesc)
  public
    type
      TComputeEvent = procedure(El: TFresnelElement;
                                var Value: string; // returns empty for invalid
                                out Complete: boolean) of object;
      TGetAsStringEvent = function(El: TFresnelElement): string of object;
      TGetLength = function(const aComp: TCSSResCompValue; El: TFresnelElement;
        NoChildren: boolean{true=dont use child values}
        ): TFresnelLength of object;
  public
    LengthHorizontal: boolean; // // e.g. what dpi to use
    OnAsString: TGetAsStringEvent; // returns value of shorthand (e.g. font or margin-block)
    OnCompute: TComputeEvent; // removes unknown/invalid values, substitutes calc(), converts to base unit (px)
    OnGetLength: TGetLength; // resolve special length: cuPercent to 100% pixels and keyword to pixel
  end;

  { TFresnelCSSAttrDesc }

  TFresnelCSSAttrDesc = class(TFresnelElementAttrDesc)
  public
    Attr: TFresnelCSSAttribute;
  end;

  { TFresnelCSSPseudoClassDesc }

  TFresnelCSSPseudoClassDesc = class(TCSSPseudoClassDesc)
  public
    Pseudo: TFresnelCSSPseudoClass;
  end;

  { TFresnelCSSTypeDesc }

  TFresnelCSSTypeDesc = class(TCSSTypeDesc)
  public
  end;

  { TFresnelCSSRegistry }

  TFresnelCSSRegistry = class(TCSSRegistry)
  protected
    // check attribute (longhands and shorthands)
    function CheckDisplay(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckPosition(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBoxSizing(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckDirection(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckWritingMode(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckZIndex(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckOverflowXY(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckClear(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckLeftTopRightBottom(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckWidthHeight(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckColor(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBorderWidth(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBorderStyle(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBorder(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBorderRadius(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFloat(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontFeatureSettings(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontFamily(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontKerning(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontSize(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontStyle(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontVariantAlternates(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontVariantCaps(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontVariantEastAsian(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontVariantEmoji(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontVariantLigatures(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontVariantNumeric(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontVariantPosition(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontVariant(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontWeight(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFontWidth(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckLineHeight(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFont(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckTextShadow(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckMargin(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckOpacity(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckPadding(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckVisible(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackgroundAttachment(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackgroundClip(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackgroundImage(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackgroundOrigin(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackgroundPositionXY(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackgroundPosition(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackgroundRepeat(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackgroundSize(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckBackground(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckCursor(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckScrollbarColor(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckScrollbarGutter(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckScrollbarWidth(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFlexBasis(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFlexDirection(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFlexGrow(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFlexShrink(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFlexWrap(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFlexFlow(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckFlex(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckAlignContent(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckAlignItems(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckAlignSelf(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckJustifyContent(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckJustifyItems(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckJustifySelf(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckPlaceContent(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckPlaceItems(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckPlaceSelf(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckColumnRowGap(Resolver: TCSSBaseResolver): boolean; virtual;
    function CheckGap(Resolver: TCSSBaseResolver): boolean; virtual;

    // utility check/parse functions
    function CheckLinearGradient(Resolver: TCSSBaseResolver; Check: boolean): boolean; virtual;
    function CheckImage(Resolver: TCSSBaseResolver; Check: boolean): boolean; virtual;
    function ReadBackgroundPositionList(Resolver: TCSSBaseResolver; Check: boolean; out X, Y: TCSSString): boolean; virtual;
    function ReadOneBackgroundPosition(Resolver: TCSSBaseResolver; Check: boolean; out X, Y: TCSSString): boolean; virtual;
    function ReadOneBackgroundSize(Resolver: TCSSBaseResolver; Check: boolean; out W, H: TCSSString): boolean; virtual;
    function ReadBackgroundRepeat(Resolver: TCSSBaseResolver; Check: boolean; out X, Y: TCSSNumericalID): boolean; virtual;
    function ReadBackground(Resolver: TCSSBaseResolver; Check: boolean;
      out aAttachment, aClip, aColor, aImage, aOrigin, aPositionX, aPositionY, aRepeat, aSize: TCSSString): boolean; virtual;
    function ReadFont(Resolver: TCSSBaseResolver; Check: boolean;
      out aSystemFont: TCSSNumericalID; out aFamily, aStyle, aWeight, aWidth, aSize, aLineHeight, aVariantCaps: TCSSString): boolean; virtual;
    function ReadFontVariant(Resolver: TCSSBaseResolver; Check: boolean;
      out aAlternates: TCSSString; out aCaps: TCSSNumericalID;
      out aEastAsian: TCSSNumericalIDArray;
      out aEmoji: TCSSNumericalID;
      out aLigatures, aNumeric: TCSSNumericalIDArray;
      out aPosition: TCSSNumericalID): boolean; virtual;
    function ReadFlex(Resolver: TCSSBaseResolver; Check: boolean; out Grow, Shrink, Basis: TCSSString): boolean; virtual;
    function ReadFlexFlow(Resolver: TCSSBaseResolver; Check: boolean; out Direction, Wrap: TCSSString): boolean; virtual;
    function ReadAlignContent(Resolver: TCSSBaseResolver; Check: boolean; out MainKW, SubKW: TCSSNumericalID): boolean; virtual;
    function ReadAlignItems(Resolver: TCSSBaseResolver; Check: boolean; out MainKW, SubKW: TCSSNumericalID): boolean; virtual;
    function ReadAlignSelf(Resolver: TCSSBaseResolver; Check: boolean; out MainKW, SubKW: TCSSNumericalID): boolean; virtual;
    function ReadJustifyContent(Resolver: TCSSBaseResolver; Check: boolean; out MainKW, SubKW: TCSSNumericalID): boolean; virtual;
    function ReadJustifyItems(Resolver: TCSSBaseResolver; Check: boolean; out MainKW, SubKW: TCSSNumericalID): boolean; virtual;
    function ReadJustifySelf(Resolver: TCSSBaseResolver; Check: boolean; out MainKW, SubKW: TCSSNumericalID): boolean; virtual;
    function ReadPlaceContent(Resolver: TCSSBaseResolver; Check: boolean;
      out AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID): boolean; virtual;
    function ReadPlaceItems(Resolver: TCSSBaseResolver; Check: boolean;
      out AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID): boolean; virtual;
    function ReadPlaceSelf(Resolver: TCSSBaseResolver; Check: boolean;
      out AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID): boolean; virtual;
    function ReadGap(Resolver: TCSSBaseResolver; Check: boolean; out Column, Row: TCSSString): boolean; virtual;
    function IsColor(const ResValue: TCSSResCompValue): boolean; virtual;
    procedure SplitLonghandSides(var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray;
      Top, Right, Bottom, Left: TFresnelCSSAttribute; const Found: TCSSStringArray);
    procedure SplitLonghandCorners(var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray;
      TopLeft, TopRight, BottomRight, BottomLeft: TFresnelCSSAttribute; const Found: TCSSStringArray);
    function CombinePlace(SubKW, MainKW: TCSSNumericalID): TCSSString;
    function CombineKeywords(const aKeywords: TCSSNumericalIDArray): TCSSString;

    // split shorthands
    procedure SplitBackground(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitBackgroundPosition(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitBorderColor(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitBorderLeftRightTopBottom(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual; // also fcaBorder
    procedure SplitBorderRadius(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitBorderStyle(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitBorderWidth(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitFlex(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitFlexFlow(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitFont(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual; // todo systemfont
    procedure SplitFontStretch(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual; // todo systemfont
    procedure SplitFontVariant(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual; // todo systemfont
    procedure SplitGap(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual; // todo systemfont
    procedure SplitMargin(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitMarginBlock(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitMarginInline(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitOverflow(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitPadding(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitPlaceContent(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitPlaceItems(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;
    procedure SplitPlaceSelf(Resolver: TCSSBaseResolver; var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray); virtual;

    // get computed attribute as string
    function GetBackground(El: TFresnelElement): string; virtual;
    function GetBackgroundPosition(El: TFresnelElement): string; virtual;
    function GetBorder(El: TFresnelElement): string; virtual;
    function GetBorderColor(El: TFresnelElement): string; virtual;
    function GetBorderSide(El: TFresnelElement; Side: TFresnelCSSSide): string; virtual;
    function GetBorderLeft(El: TFresnelElement): string; virtual;
    function GetBorderRight(El: TFresnelElement): string; virtual;
    function GetBorderTop(El: TFresnelElement): string; virtual;
    function GetBorderBottom(El: TFresnelElement): string; virtual;
    function GetBorderStyle(El: TFresnelElement): string; virtual;
    function GetBorderRadius(El: TFresnelElement): string; virtual;
    function GetBorderWidth(El: TFresnelElement): string; virtual;
    function GetSideLengths(El: TFresnelElement; LeftAttr: TFresnelCSSAttribute): string; virtual;
    function GetFlex(El: TFresnelElement): string; virtual;
    function GetFlexFlow(El: TFresnelElement): string; virtual;
    function GetFont(El: TFresnelElement): string; virtual;
    function GetFontFamily(El: TFresnelElement): string; virtual;
    function GetFontKerning(El: TFresnelElement): string; virtual;
    function GetFontSize(El: TFresnelElement): string; virtual;
    function GetFontStyle(El: TFresnelElement): string; virtual;
    function GetFontVariant(El: TFresnelElement): string; virtual;
    function GetFontVariantAlternates(El: TFresnelElement): string; virtual;
    function GetFontVariantCaps(El: TFresnelElement): string; virtual;
    function GetFontVariantEastAsian(El: TFresnelElement): string; virtual;
    function GetFontVariantEmoji(El: TFresnelElement): string; virtual;
    function GetFontVariantLigatures(El: TFresnelElement): string; virtual;
    function GetFontVariantNumeric(El: TFresnelElement): string; virtual;
    function GetFontVariantPosition(El: TFresnelElement): string; virtual;
    function GetFontWidth(El: TFresnelElement): string; virtual;
    function GetFontWeight(El: TFresnelElement): string; virtual;
    function GetGap(El: TFresnelElement): string; virtual;
    function GetMargin(El: TFresnelElement): string; virtual;
    function GetMarginBlock(El: TFresnelElement): string; virtual;
    function GetMarginInline(El: TFresnelElement): string; virtual;
    function GetOverflow(El: TFresnelElement): string; virtual;
    function GetPadding(El: TFresnelElement): string; virtual;
    function GetPlaceContent(El: TFresnelElement): string; virtual;
    function GetPlaceItems(El: TFresnelElement): string; virtual;
    function GetPlaceSelf(El: TFresnelElement): string; virtual;

    // get length
    function GetLength_ContainerContentWidth(const aComp: TCSSResCompValue; El: TFresnelElement; NoChildren: boolean): TFresnelLength; virtual;
    function GetLength_ContainerContentHeight(const aComp: TCSSResCompValue; El: TFresnelElement; NoChildren: boolean): TFresnelLength; virtual;
    function GetLength_BorderWidth(const aComp: TCSSResCompValue; El: TFresnelElement; NoChildren: boolean): TFresnelLength; virtual;
  public
    // keywords
    const
      // predefined
      kwNone = CSSKeywordNone;
      kwInitial = CSSKeywordInitial;
      kwInherit = CSSKeywordInherit;
      kwUnset = CSSKeywordUnset;
      kwRevert = CSSKeywordRevert;
      kwRevertLayer = CSSKeywordRevertLayer;
      kwAuto = CSSKeywordAuto;

      // Fresnel additions:
      kwAbsolute = kwAuto+1;
      kwAlias = kwAbsolute+1;
      kwAllPetiteCaps = kwAlias+1;
      kwAllScroll = kwAllPetiteCaps+1; // all-scroll
      kwAllSmallCaps = kwAllScroll+1;
      kwAnchorCenter = kwAllSmallCaps+1; // anchor-center
      kwBaseline = kwAnchorCenter+1;
      kwBlock = kwBaseline+1;
      kwBlockEnd = kwBlock+1; // block-end
      kwBlockStart = kwBlockEnd+1; // block-start
      kwBold = kwBlockStart+1;
      kwBolder = kwBold+1;
      kwBorderBox = kwBolder+1; // border-box
      kwBoth = kwBorderBox+1;
      kwBothBlock = kwBoth+1; // both-block
      kwBothEdges = kwBothBlock+1; // both-edges
      kwBothInline = kwBothEdges+1; // both-inline
      kwBottom = kwBothInline+1;
      kwCaption = kwBottom+1;
      kwCell = kwCaption+1;
      kwCenter = kwCell+1;
      kwClip = kwCenter+1;
      kwCollapse = kwClip+1;
      kwColResize = kwCollapse+1; // col-resize
      kwColumn = kwColResize+1;
      kwColumnReverse = kwColumn+1; // column-reversed
      kwCommonLigatures = kwColumnReverse+1; // common-ligatures
      kwCondensed = kwCommonLigatures+1;
      kwContain = kwCondensed+1;
      kwContent = kwContain+1;
      kwContentBox = kwContent+1; // content-box
      kwContents = kwContentBox+1;
      kwContextMenu = kwContents+1;
      kwContextual = kwContextMenu+1;
      kwCopy = kwContextual+1;
      kwCover = kwCopy+1;
      kwCrosshair = kwCover+1;
      kwCurrentcolor = kwCrosshair+1; // currentcolor
      kwDashed = kwCurrentcolor+1;
      kwDefault = kwDashed+1;
      kwDiagonalFractions = kwDefault+1;
      kwDiscretionaryLigatures = kwDiagonalFractions+1;
      kwDotted = kwDiscretionaryLigatures+1;
      kwDouble = kwDotted+1;
      kwEmoji = kwDouble+1;
      kwEnd = kwEmoji+1;
      kwEResize = kwEnd+1; // e-resize
      kwEWResize = kwEResize+1; // ew-resize
      kwExpanded = kwEWResize+1;
      kwExtraCondensed = kwExpanded+1; // extra-condensed
      kwExtraExpanded = kwExtraCondensed+1; // extra-expanded
      kwFirst = kwExtraExpanded+1;
      kwFitContent = kwFirst+1; // fit-content
      kwFixed = kwFitContent+1;
      kwFlex = kwFixed+1;
      kwFlexEnd = kwFlex+1;
      kwFlexStart = kwFlexEnd+1;
      kwFlow = kwFlexStart+1;
      kwFlowRoot = kwFlow+1; // flow-root
      kwFullWidth = kwFlowRoot+1;
      kwGrab = kwFullWidth+1;
      kwGrabbing = kwGrab+1;
      kwGrid = kwGrabbing+1;
      kwGroove = kwGrid+1;
      kwHelp = kwGroove+1;
      kwHidden = kwHelp+1;
      kwHistoricalForms = kwHidden+1;
      kwHistoricalLigatures = kwHistoricalForms+1;
      kwHorizontalTB = kwHistoricalLigatures+1;
      kwIcon = kwHorizontalTB+1;
      kwIn = kwIcon+1;
      kwInline = kwIn+1;
      kwInlineBlock = kwInline+1; // inline-block
      kwInlineEnd = kwInlineBlock+1; // inline-end
      kwInlineFlow = kwInlineEnd+1; // inline-flow
      kwInlineStart = kwInlineFlow+1; // inline-start
      kwInset = kwInlineStart+1;
      kwItalic = kwInset+1;
      kwJis04 = kwItalic+1;
      kwJis78 = kwJis04+1;
      kwJis83 = kwJis78+1;
      kwJis90 = kwJis83+1;
      kwLarge = kwJis90+1;
      kwLarger = kwLarge+1;
      kwLast = kwLarger+1;
      kwLeft = kwLast+1;
      kwLegacy = kwLeft+1;
      kwLighter = kwLegacy+1;
      kwLiningNums = kwLighter+1;
      kwLocal = kwLiningNums+1;
      kwLTR = kwLocal+1;
      kwMaxContent = kwLTR+1; // max-content
      kwMedium = kwMaxContent+1;
      kwMenu = kwMedium+1;
      kwMessageBox = kwMenu+1; // message-box
      kwMinContent = kwMessageBox+1; // min-content
      kwMove = kwMinContent+1;
      kwNEResize = kwMove+1; // ne-resize
      kwNESWResize = kwNEResize+1; // nesw-resize
      kwNoCommonLigatures = kwNESWResize+1;
      kwNoContextual = kwNoCommonLigatures+1;
      kwNoDiscretionaryLigatures = kwNoContextual+1;
      kwNoDrop = kwNoDiscretionaryLigatures+1; // no-drop
      kwNoHistoricalLigatures = kwNoDrop+1;
      kwNoRepeat = kwNoHistoricalLigatures+1; // no-repeat
      kwNormal = kwNoRepeat+1;
      kwNotAllowed = kwNormal+1; // not-allowed
      kwNoWrap = kwNotAllowed+1; // nowrap
      kwNResize = kwNoWrap+1; // n-resize
      kwNSResize = kwNResize+1; // ns-resize
      kwNWResize = kwNSResize+1; // nw-resize
      kwNWSEResize = kwNWResize+1; // nwse-resize
      kwOblique = kwNWSEResize+1;
      kwOldstyleNums = kwOblique+1;
      kwOrdinal = kwOldstyleNums+1;
      kwOutset = kwOrdinal+1;
      kwPaddingBox = kwOutset+1; // padding-box
      kwPetiteCaps = kwPaddingBox+1;
      kwPointer = kwPetiteCaps+1;
      kwProgress = kwPointer+1;
      kwProportionalNums = kwProgress+1;
      kwProportionalWidth = kwProportionalNums+1;
      kwRelative = kwProportionalWidth+1;
      kwRepeat = kwRelative+1;
      kwRepeatX = kwRepeat+1; // repeat-x
      kwRepeatY = kwRepeatX+1; // repeat-y
      kwRidge = kwRepeatY+1;
      kwRight = kwRidge+1;
      kwRound = kwRight+1;
      kwRow = kwRound+1;
      kwRowResize = kwRow+1; // row-resize
      kwRowReverse = kwRowResize+1; // row-reversed
      kwRTL = kwRowReverse+1;
      kwRuby = kwRTL+1;
      kwSafe = kwRuby+1;
      kwScroll = kwSafe+1;
      kwSelfEnd = kwScroll+1; // self-end
      kwSelfStart = kwSelfEnd+1; // self-start
      kwSemiCondensed = kwSelfStart+1; // semi-condensed
      kwSemiExpanded = kwSemiCondensed+1; // semi-expanded
      kwSEResize = kwSemiExpanded+1; // se-resize
      kwSidewaysLR = kwSEResize+1; // sideways-lr
      kwSidewaysRL = kwSidewaysLR+1; // sideways-rl
      kwSimplified = kwSidewaysRL+1;
      kwSlashedZero = kwSimplified+1;
      kwSmall = kwSlashedZero+1;
      kwSmallCaps = kwSmall+1; // small-caps
      kwSmallCaption = kwSmallCaps+1; // small-caption
      kwSmaller = kwSmallCaption+1;
      kwSolid = kwSmaller+1;
      kwSpace = kwSolid+1;
      kwSpaceAround = kwSpace+1; // space-around
      kwSpaceBetween = kwSpaceAround+1; // space-between
      kwSpaceEvenly = kwSpaceBetween+1; // space-evenly
      kwSResize = kwSpaceEvenly+1; // s-resize
      kwStable = kwSResize+1;
      kwStackedFractions = kwStable+1;
      kwStart = kwStackedFractions+1;
      kwStatic = kwStart+1;
      kwStatusBar = kwStatic+1; // status-bar
      kwSticky = kwStatusBar+1;
      kwStretch = kwSticky+1;
      kwSub = kwStretch+1;
      kwSuper = kwSub+1;
      kwSWResize = kwSuper+1; // sw-resize
      kwTabularNums = kwSWResize+1;
      kwText = kwTabularNums+1;
      kwThick = kwText+1;
      kwThin = kwThick+1;
      kwTitlingCaps = kwThin+1;
      kwTo = kwTitlingCaps+1;
      kwTop = kwTo+1;
      kwTraditional = kwTop+1;
      kwUltraCondensed = kwTraditional+1; // ultra-condensed
      kwUltraExpanded = kwUltraCondensed+1; // ultra-expanded
      kwUnicase = kwUltraExpanded+1;
      kwUnicode = kwUnicase+1;
      kwUnsafe = kwUnicode+1;
      kwVerticalLR = kwUnsafe+1; // vertical-lr
      kwVerticalRL = kwVerticalLR+1; // vertical-rl
      kwVerticalText = kwVerticalRL+1; // vertical-text
      kwVisible = kwVerticalText+1;
      kwWait = kwVisible+1;
      kwWrap = kwWait+1; // w-resize
      kwWrapReverse = kwWrap+1; // wrap-reverse
      kwWResize = kwWrapReverse+1; // w-resize
      kwXLarge = kwWResize+1; // x-large
      kwXSmall = kwXLarge+1; // x-small
      kwXXLarge = kwXSmall+1; // xx-large
      kwXXSmall = kwXXLarge+1; // xx-small
      kwXXXLarge = kwXXSmall+1; // xxx-large
      kwZoomIn = kwXXXLarge+1; // zoom-in
      kwZoomOut = kwZoomIn+1; // zoom-out

      // attribute functions
      afUrl = afVar+1;
      afLinearGradient = afUrl+1; // linear-gradient
      afRadialGradient = afLinearGradient+1; // radial-gradient
      afConicGradient = afRadialGradient+1; // conic-gradient
      afRepeatingLinearGradient = afConicGradient+1; // repeating-linear-gradient
      afRepeatingRadialGradient = afRepeatingLinearGradient+1; // repeating-radial-gradient
      afRepeatingConicGradient = afRepeatingRadialGradient+1; // repeating-conic-gradient
  public
    // font-width
    type
      TFontWidthName = (
        fntwnUltraCondensed,
        fntwnExtraCondensed,
        fntwnCondensed,
        fntwnSemiCondensed,
        fntwnNormal,
        fntwnSemiExpanded,
        fntwnExpanded,
        fntwnExtraExpanded,
        fntwnUltraExpanded
        );
      TFontWidthNames = set of TFontWidthName;
    const
      FontWidths: array[TFontWidthName] of TFresnelLength = (
        0.5, // UltraCondensed
        0.625, // ExtraCondensed
        0.75, // Condensed
        0.875, // SemiCondensed
        1, // Normal
        1.125, // SemiExpanded
        1.25, // Expanded
        1.5, // ExtraExpanded
        2 // UltraExpanded
        );
      FontWidthKeywords: array[TFontWidthName] of TCSSNumericalID = (
        kwUltraCondensed,
        kwExtraCondensed,
        kwCondensed,
        kwSemiCondensed,
        kwNormal,
        kwSemiExpanded,
        kwExpanded,
        kwExtraExpanded,
        kwUltraExpanded
        );

      function RoundFontWidth(aWidth: TFresnelLength): TFontWidthName; virtual;
      function RoundFontWidthToKeyword(aWidth: TFresnelLength): TCSSNumericalID; virtual;
  public
    FresnelAttrIDBase: TCSSNumericalID;
    FresnelAttrs: array[TFresnelCSSAttribute] of TFresnelCSSAttrDesc;

    FresnelPseudoClassIDBase: TCSSNumericalID;
    FresnelPseudoClasses: array[TFresnelCSSPseudoClass] of TFresnelCSSPseudoClassDesc;

    // check parameters
    // The keyword lists are used by shorthands too, so they must not include
    // base keywords like initial, inherit, unset, revert, revert-layer
    Chk_Color_KeywordIDs: TCSSNumericalIDArray;
    Chk_DisplayBox_KeywordIDs: TCSSNumericalIDArray;
    Chk_DisplayInside_KeywordIDs: TCSSNumericalIDArray;
    Chk_DisplayOutside_KeywordIDs: TCSSNumericalIDArray;
    Chk_Display_KeywordIDs: TCSSNumericalIDArray;
    Chk_Position_KeywordIDs: TCSSNumericalIDArray;
    Chk_BoxSizing_KeywordIDs: TCSSNumericalIDArray;
    Chk_Direction_KeywordIDs: TCSSNumericalIDArray;
    Chk_WritingMode_KeywordIDs: TCSSNumericalIDArray;
    Chk_ZIndex_Dim: TCSSCheckAttrParams_Dimension;
    Chk_OverflowXY_KeywordIDs: TCSSNumericalIDArray;
    Chk_Clear_KeywordIDs: TCSSNumericalIDArray;
    Chk_LeftTopRightBottom_Dim: TCSSCheckAttrParams_Dimension;
    Chk_WidthHeight_Dim: TCSSCheckAttrParams_Dimension;
    Chk_BorderWidth_Dim: TCSSCheckAttrParams_Dimension;
    Chk_BorderStyle_KeywordIDs: TCSSNumericalIDArray;
    Chk_BorderRadius_Dim: TCSSCheckAttrParams_Dimension;
    Chk_Float_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontFeatureSettings_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontKerning_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontSize_Dim: TCSSCheckAttrParams_Dimension;
    Chk_FontWidth_Dim: TCSSCheckAttrParams_Dimension;
    Chk_FontStyle_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontWeight_Dim: TCSSCheckAttrParams_Dimension;
    Chk_FontVariantAlternates_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontVariantCaps_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontVariantEastAsian_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontVariantEmoji_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontVariantLigatures_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontVariantNumeric_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontVariantPosition_KeywordIDs: TCSSNumericalIDArray;
    Chk_FontVariant_KeywordIDs: TCSSNumericalIDArray;
    Chk_LineHeight_Dim: TCSSCheckAttrParams_Dimension;
    Chk_TextShadow_Offset_Dim: TCSSCheckAttrParams_Dimension;
    Chk_TextShadow_Radius_Dim: TCSSCheckAttrParams_Dimension;
    Chk_Margin_Dim: TCSSCheckAttrParams_Dimension;
    Chk_Opacity_Dim: TCSSCheckAttrParams_Dimension;
    Chk_Padding_Dim: TCSSCheckAttrParams_Dimension;
    Chk_Visible_KeywordIDs: TCSSNumericalIDArray;
    Chk_BackgroundAttachment_KeywordIDs: TCSSNumericalIDArray;
    Chk_BackgroundClip_KeywordIDs: TCSSNumericalIDArray;
    Chk_BackgroundOrigin_KeywordIDs: TCSSNumericalIDArray;
    Chk_BackgroundPositionX_Dim: TCSSCheckAttrParams_Dimension;
    Chk_BackgroundPositionY_Dim: TCSSCheckAttrParams_Dimension;
    Chk_BackgroundPosition_Dim: TCSSCheckAttrParams_Dimension;
    Chk_BackgroundRepeat_KeywordIDs: TCSSNumericalIDArray;
    Chk_BackgroundSize_Dim: TCSSCheckAttrParams_Dimension;
    Chk_Cursor_KeywordIDs: TCSSNumericalIDArray;
    Chk_ScrollbarWidth_KeywordIDs: TCSSNumericalIDArray;
    Chk_FlexBasis_Dim: TCSSCheckAttrParams_Dimension;
    Chk_FlexDirection_KeywordIDs: TCSSNumericalIDArray;
    Chk_FlexGrow_Dim: TCSSCheckAttrParams_Dimension;
    Chk_FlexShrink_Dim: TCSSCheckAttrParams_Dimension;
    Chk_FlexWrap_KeywordIDs: TCSSNumericalIDArray;
    Chk_AlignContent_KeywordIDs: TCSSNumericalIDArray;
    Chk_AlignItems_KeywordIDs: TCSSNumericalIDArray;
    Chk_AlignSelf_KeywordIDs: TCSSNumericalIDArray;
    Chk_JustifyContent_KeywordIDs: TCSSNumericalIDArray;
    Chk_JustifyItems_KeywordIDs: TCSSNumericalIDArray;
    Chk_JustifySelf_KeywordIDs: TCSSNumericalIDArray;
    Chk_ColumnRowGap_Dim: TCSSCheckAttrParams_Dimension;

    constructor Create;
    procedure Init; override;
    // register attributes
    function AddFresnelAttr(Attr: TFresnelCSSAttribute; aInherits: boolean;
      const OnCheck: TCSSAttributeDesc.TCheckEvent): TFresnelCSSAttrDesc; virtual; overload;
    function AddFresnelShorthand(Attr: TFresnelCSSAttribute; const OnCheck: TCSSAttributeDesc.TCheckEvent;
      const OnSplit: TCSSAttributeDesc.TSplitShorthandEvent;
      const OnAsString: TFresnelElementAttrDesc.TGetAsStringEvent;
      const CompProps: TFresnelCSSAttributeArray): TFresnelCSSAttrDesc; virtual; overload;
    function AddFresnelLonghand(Attr: TFresnelCSSAttribute; Inherits: boolean;
      const OnCheck: TCSSAttributeDesc.TCheckEvent;
      const InitialValue: TCSSString = ''): TFresnelCSSAttrDesc; virtual; overload;
    function AddFresnelLonghandLength(Attr: TFresnelCSSAttribute; Inherits: boolean;
      const OnCheck: TCSSAttributeDesc.TCheckEvent;
      Horizontal: boolean;
      const InitialValue: TCSSString = '';
      const OnGetLength: TFresnelElementAttrDesc.TGetLength = nil
      ): TFresnelCSSAttrDesc; virtual; overload;
    // register pseudo classes
    function AddFresnelPseudoClass(Pseudo: TFresnelCSSPseudoClass): TFresnelCSSPseudoClassDesc; virtual; overload;
    // register pseudo element
    function AddFresnelPseudoElement(const aName: TCSSString;
      const AllowedAttribs: TFresnelCSSAttributeArray): TCSSPseudoElementDesc; virtual; overload;
  end;

  TFresnelLayoutMode = (
    flmMinWidth,
    flmMinHeight,
    flmMaxWidth,
    flmMaxHeight
    );
  TFresnelLayoutModes = set of TFresnelLayoutMode;

  { TFresnelLayoutNode }

  TFresnelLayoutNode = class(TComponent)
  private
    FElement: TFresnelElement;
    FParent: TFresnelLayoutNode;
    FNodes: TFPList; // list of TFresnelLayoutNode
    function GetNodeCount: integer;
    function GetNodes(Index: integer): TFresnelLayoutNode;
    procedure SetElement(const AValue: TFresnelElement);
    procedure SetParent(const AValue: TFresnelLayoutNode);
  protected
    FContainerContentHeight: TFresnelLength;
    FContainerContentHeightValid: boolean;
    FContainerContentWidth: TFresnelLength;
    FContainerContentWidthValid: boolean;
  public
    Container: TFresnelElement;
    SkipLayout: boolean; // e.g. element or ancestor display:none or visibility=collapse
    SkipRendering: boolean; // e.g. element or ancestor display:none or visibility<>visible

    // used values
    ZIndex: TFresnelLength; // position=static has 0, non static have z-index+0.5
    BorderLeft: TFresnelLength;
    BorderRight: TFresnelLength;
    BorderTop: TFresnelLength;
    BorderBottom: TFresnelLength;
    PaddingLeft: TFresnelLength;
    PaddingRight: TFresnelLength;
    PaddingTop: TFresnelLength;
    PaddingBottom: TFresnelLength;
    MarginLeft: TFresnelLength;
    MarginRight: TFresnelLength;
    MarginTop: TFresnelLength;
    MarginBottom: TFresnelLength;
    LineHeight: TFresnelLength;

    // attributes depending on position:
    //  static: left, top, right, bottom are ignored
    //  relative: relative to layout position. left 10px moves 10px to the right, right 10px moves 10px to the left
    //  absolute: relative to container's contentbox. left 10px moves 10px to the right, right 10px moves 10px to the left
    //  fixed: as absolute, except relative to viewport
    //  sticky: as relative, except to container's non-scrolled contentbox
    Left: TFresnelLength;
    Top: TFresnelLength;
    Right: TFresnelLength;
    Bottom: TFresnelLength;

    // content-boxed:
    Width: TFresnelLength;
    Height: TFresnelLength;
    MinWidth: TFresnelLength;
    MinHeight: TFresnelLength;
    MaxWidth: TFresnelLength;
    MaxHeight: TFresnelLength;

    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function GetIntrinsicContentSize(aMode: TFresnelLayoutMode; aMaxWidth: TFresnelLength = NaN; aMaxHeight: TFresnelLength = NaN): TFresnelPoint; virtual; // ignoring min|max-height
    function GetRoot: TFresnelLayoutNode;
    function FitWidth(aWidth: TFresnelLength): TFresnelLength;
    function FitHeight(aHeight: TFresnelLength): TFresnelLength;
    procedure ResetUsedLengths; virtual;
    property Element: TFresnelElement read FElement write SetElement;
    // rendering order (z-index layout)
    {$IF FPC_FULLVERSION>=30301}
    procedure SortNodes(const Compare: TListSortComparer_Context; Context: Pointer = nil); virtual;
    {$ELSE}
    procedure SortNodes(const Compare: TListSortCompare); virtual;
    {$ENDIF}
    property Parent: TFresnelLayoutNode read FParent write SetParent;
    property NodeCount: integer read GetNodeCount;
    property Nodes[Index: integer]: TFresnelLayoutNode read GetNodes;
  end;
  TFresnelLayoutNodeClass = class of TFresnelLayoutNode;

  { TFresnelLayouter }

  TFresnelLayouter = class(TComponent)
  public
    procedure Apply; virtual; abstract;
  end;
  TFLLayouter = TFresnelLayouter;
  TFresnelLayouterClass = class of TFresnelLayouter;

  { IFresnelFont }

  IFresnelFont = interface ['{6B53C662-5598-419B-996B-7E839271B64E}']
    function GetFamily: string;
    function GetKerning: TFresnelCSSKerning;
    function GetSize: TFresnelLength; // in pixel
    function GetStyle: string;
    function GetWeight: TFresnelLength; // 0..750
    function GetWidth: TFresnelLength; // 1 = default

    // font-variant
    function GetAlternates: string;
    function GetCaps: TFresnelCSSFontVarCaps;
    function GetEastAsians: TFresnelCSSFontVarEastAsians;
    function GetEmoji: TFresnelCSSFontVarEmoji;
    function GetLigatures: TFresnelCSSFontVarLigaturesSet;
    function GetNumerics: TFresnelCSSFontVarNumerics;
    function GetPosition: TFresnelCSSFontVarPosition;

    function TextSize(const aText: string): TFresnelPoint;
    function TextSizeMaxWidth(const aText: string; MaxWidth: TFresnelLength): TFresnelPoint;
    function GetTool: TObject;
    Function GetDescription : String;
  end;
  IFLFont = IFresnelFont;

  IFresnelRenderer = Interface ['{06738575-BE7F-4EA5-A4D0-3E26A5441BFD}']
    procedure FillRect(const aColor: TFPColor; const aRect: TFresnelRect);
    procedure Line(const aColor: TFPColor; const x1, y1, x2, y2: TFresnelLength);
    procedure TextOut(const aLeft, aTop: TFresnelLength; const aFont: IFresnelFont; const aColor: TFPColor; const aText: string);
    procedure AddTextShadow(const aOffsetX, aOffsetY: TFresnelLength; const aColor: TFPColor; const aRadius: TFresnelLength);
    procedure ClearTextShadows;
    // Get reference to TFresnelTextShadow. Index between 0 and GetTextShadowCount-1
    function GetTextShadow(aIndex : Integer): PFresnelTextShadow;
    // Number of TextShadows that will be applied
    function GetTextShadowCount: Integer;
    procedure DrawImage(const aLeft, aTop, aWidth, aHeight: TFresnelLength; const aImage: TFPCustomImage);
    function GetOrigin : TFresnelPoint;
    procedure SetOrigin (const aValue : TFresnelPoint);
    property Origin : TFresnelPoint Read GetOrigin Write SetOrigin;
    property TextShadowCount : Integer Read GetTextShadowCount;
    property TextShadow[aIndex : Integer] : PFresnelTextShadow read GetTextShadow ;
  end;

  IFresnelRenderable = Interface ['{1364DA87-CA22-48A4-B1B2-A8A2C3047FD8}']
    Procedure BeforeRender;
    Procedure AfterRender;
    Procedure Render(aRenderer : IFresnelRenderer);
  end;

  { TFresnelFontDesc - font descriptor }

  TFresnelFontDesc = record
    Family: string;
    Kerning: TFresnelCSSKerning;
    Size: TFresnelLength; // in pixel
    Style: string;
    Weight: TFresnelLength; // 100..750
    Width: TFresnelLength; // 1 = default
    // font-variant:
    Alternates: string;
    Caps: TFresnelCSSFontVarCaps;
    EastAsians: TFresnelCSSFontVarEastAsians;
    Emoji: TFresnelCSSFontVarEmoji;
    Ligatures: TFresnelCSSFontVarLigaturesSet;
    Numerics: TFresnelCSSFontVarNumerics;
    Position: TFresnelCSSFontVarPosition;
    function Compare(const Desc: TFresnelFontDesc): integer;
    class function CompareDescriptors(const A, B: TFresnelFontDesc): integer; static;
  end;
  PFresnelFontDesc = ^TFresnelFontDesc;

  TFresnelCSSBackgroundInfo = class
  end;

  TFresnelCSSLinearGradient = class(TFresnelCSSBackgroundInfo)
  public type
    TColorPercentage = record
      Color: TFPColor;
      Percentage: TFresnelLength;
    end;
    TColorPercentageArray = array of TColorPercentage;
  public
    StartPoint : TFresnelPoint;
    EndPoint : TFresnelPoint;
    // side corner
    Colors: TColorPercentageArray;
  end;

  TPseudoElement = class;

  { TFresnelElement }

  TFresnelElement = class(TFresnelComponent, ICSSNode, IFPObserver, IFresnelRenderable)
  private
    type
      TState = (
        fesFontDescValid,
        fesHover, // mouse is over this element
        fesStyleChanged, // StyleElement needs update
        fesViewportConnected, // true if this element or any child is using a resource (e.g. Font) of Viewport
        fesPseudoElement
      );
      TStates = set of TState;
  private
    FAfterRender: TNotifyEvent;
    FBeforeRender: TNotifyEvent;
    FComputedBoxSizing: TCSSNumericalID;
    FComputedDirection: TCSSNumericalID;
    FComputedFloat: TCSSNumericalID;
    FComputedOverflowX: TCSSNumericalID;
    FComputedOverflowY: TCSSNumericalID;
    FComputedVisibility: TCSSNumericalID;
    FComputedWritingMode: TCSSNumericalID;
    FFont: IFresnelFont;
    FLayoutNode: TFresnelLayoutNode;
    FFontDesc: TFresnelFontDesc;
    FRendered: boolean;
    FRenderedBorderBox: TFresnelRect;
    FRenderedContentBox: TFresnelRect;
    FStates: TStates;
    // Todo: change to dictionary to reduce mem footprint
    FStandardEvents : Array[0..evtLastEvent] of TEventHandlerItem;
    FEventDispatcher : TFresnelEventDispatcher;
    FUsedBorderBox: TFresnelRect;
    FUsedContentBox: TFresnelRect;
    function GetNodeCount: integer;
    function GetNodes(Index: integer): TFresnelElement;
    function GetPeudoNodeCount: integer;
    function GetPseudoNodes(Index: integer): TPseudoElement;
    function GetViewportConnected: boolean;
    function GetEventHandler(AIndex: Integer): TFresnelEventHandler;
    function GetFocusEventHandler(AIndex: Integer): TFresnelFocusEventHandler;
    function GetMouseEventHandler(AIndex: Integer): TFresnelMouseEventHandler;
    procedure SetEventHandler(AIndex: Integer; const AValue: TFresnelEventHandler);
    procedure SetFocusEventHandler(AIndex: Integer; AValue: TFresnelFocusEventHandler);
    procedure SetMouseEventHandler(AIndex: Integer; const AValue: TFresnelMouseEventHandler);
  protected
    FChildren: TFPList; // list of TFresnelElement
    FPseudoChildren: TFPList; // list of TPseudoElement
    FCSSClasses: TStrings;
    FCSSValues: TCSSAttributeValues;
    FComputedDisplayInside: TCSSNumericalID;
    FComputedDisplayOutside: TCSSNumericalID;
    FParent: TFresnelElement;
    FComputedPosition: TCSSNumericalID;
    FResolver: TCSSResolver;
    FStyle: string;
    FStyleElement: TCSSRuleElement;
    FViewPort: TFresnelViewport;
    procedure SetCSSClasses(const AValue: TStrings); virtual;
    procedure SetParent(const AValue: TFresnelElement); virtual;
    procedure SetStyle(AValue: string); virtual;
    function GetCSSPseudoClass(Pseudo: TFresnelCSSPseudoClass): boolean;
    procedure SetCSSPseudoClass(Pseudo: TFresnelCSSPseudoClass; const AValue: boolean);
    class constructor InitFresnelElementClass;
    class destructor FinalFresnelElementClass;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    function CSSReadNextValue(const aValue: string; var p: integer): string; // read e.g. url("bla")
    function CSSReadNextToken(const aValue: string; var p: integer): string; // read e.g. linear-gradient without the brackets
    function GetDPI(IsHorizontal: boolean): TFresnelLength; virtual;
    procedure SetViewportConnected(AValue: boolean); virtual;
    function GetFont: IFresnelFont; virtual;
    procedure FPOObservedChanged(ASender: TObject; Operation: TFPObservedOperation; Data: Pointer); virtual;
    // compute
    procedure UnsetValue(AttrID: TCSSNumericalID); virtual;
    function ConvertCSSValueToPixel(IsHorizontal: boolean; const Value: string; UseNaNOnFail: boolean): TFresnelLength; virtual;
    procedure ComputeBaseAttributes; virtual;
    function ComputeAttribute(aDesc: TCSSAttributeDesc; var aValue: String): TCSSAttributeValue.TState; virtual;
    function GetShorthandSpaceSeparated(AttrDesc: TCSSAttributeDesc): string;
  protected
    procedure GetChildren(Proc: TGetChildProc; Root: TComponent); override;
    procedure SetParentComponent(Value: TComponent); override;
    procedure ChildDestroying(El: TFresnelElement); virtual;
    procedure DoRender({%H-}aRenderer: IFresnelRenderer); virtual;
    Function DoDispatchEvent(aEvent : TAbstractEvent) : Integer; virtual;
    { IFresnelRenderable }
    Procedure BeforeRender;
    Procedure AfterRender;
    Procedure Render(aRenderer : IFresnelRenderer);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear;
    function GetParentComponent: TComponent; override;
    function HasParent: Boolean; override;
    function GetRoot: TFresnelElement;
    function GetPath: string; virtual;
    function AcceptChildrenAtDesignTime: boolean; virtual;

    // Can this widget handle focus ?
    class function HandleFocus : Boolean; virtual;
    // Can this widget focus now ?
    function CanFocus : Boolean; virtual;
    // Is the widget focused ?
    function IsFocused : Boolean;
    // Attempt to set focus to this element. Return true if we got focus.
    function Focus : Boolean;

    procedure DomChanged; virtual;
    procedure Invalidate; virtual; // queue a redraw
    procedure InvalidateIfNotDrawing;
    property Parent: TFresnelElement read FParent write SetParent;
    property NodeCount: integer read GetNodeCount;
    property Nodes[Index: integer]: TFresnelElement read GetNodes; default;
    property PseudoNodeCount: integer read GetPeudoNodeCount;
    property PseudoNodes[Index: integer]: TPseudoElement read GetPseudoNodes;
    // CSS
    class function CSSTypeID: TCSSNumericalID; virtual; abstract;
    class function CSSTypeName: TCSSString; virtual; abstract;
    class function GetCSSTypeStyle: TCSSString; virtual;
    procedure ClearCSSValues; virtual;
    { ICSSNode }
    // CSS identifier implementation of ICSSNode
    function GetCSSID: TCSSString; virtual;
    function GetCSSTypeName: TCSSString; virtual;
    function GetCSSTypeID: TCSSNumericalID; virtual;
    function GetCSSPseudoElementName: TCSSString; virtual;
    function GetCSSPseudoElementID: TCSSNumericalID; virtual;
    // CSS parent implementation of ICSSNode
    function GetCSSParent: ICSSNode; virtual;
    function GetCSSDepth: integer; virtual;
    function GetCSSIndex: integer; virtual;
    // CSS siblings implementation of ICSSNode
    function GetCSSNextSibling: ICSSNode; virtual;
    function GetCSSPreviousSibling: ICSSNode; virtual;
    function GetCSSNextOfType: ICSSNode; virtual;
    function GetCSSPreviousOfType: ICSSNode; virtual;
    // CSS children implementation of ICSSNode
    function GetCSSEmpty: boolean; virtual;
    function GetCSSChildCount: integer; virtual;
    function GetCSSChild(const anIndex: integer): ICSSNode; virtual;
    // CSS special attributes implementation of ICSSNode
    function HasCSSClass(const aClassName: TCSSString): boolean; virtual;
    function GetCSSAttributeClass: TCSSString; virtual;
    function GetCSSCustomAttribute(const AttrID: TCSSNumericalID): TCSSString; virtual;
    function HasCSSExplicitAttribute(const AttrID: TCSSNumericalID): boolean; virtual;
    function GetCSSExplicitAttribute(const AttrID: TCSSNumericalID): TCSSString; virtual;
    function HasCSSPseudoClass(const AttrID: TCSSNumericalID): boolean; virtual;
    // CSS warnings
    procedure CSSWarning(const ID: int64; Msg: string); virtual;
    procedure CSSInvalidValueWarning(const ID: int64; Attr: TFresnelCSSAttribute; const aValue: string); virtual;
    // CSS inline style
    function GetStyleAttr(const AttrName: string): string; virtual; overload;
    function SetStyleAttr(const AttrName, aValue: string): boolean; virtual; overload;
    property StyleElement: TCSSRuleElement read FStyleElement; // computed from Style
    // CSS classes
    procedure AddCSSClass(const aName: string); virtual;
    procedure RemoveCSSClass(const aName: string); virtual;
    // CSS pseudo classes
    property CSSPseudoClass[Pseudo: TFresnelCSSPseudoClass]: boolean read GetCSSPseudoClass write SetCSSPseudoClass;
    // CSS computed attributes
    procedure ComputeInlineStyle; virtual; // parse inline style
    procedure ComputeCSSValues; virtual; // call resolver to collect CSS values and resolve shorthands
    procedure ComputeCSSAfterLayoutNode(Layouter: TFresnelLayouter); virtual; // called after layouter node, before layouter traverse children
    procedure ComputeCSSLayoutFinished; virtual; // called after layout was finished
    function GetCSSString(AttrID: TCSSNumericalID; Compute: boolean; out Complete: boolean): string; virtual;
    function GetComputedLength(Attr: TFresnelCSSAttribute; UseNaNOnFail: boolean = false; NoChildren: boolean = false): TFresnelLength; virtual; overload;
    function GetComputedLength(AttrID: TCSSNumericalID; UseNaNOnFail: boolean = false; NoChildren: boolean = false): TFresnelLength; virtual; overload;
    function GetComputedLength(const aComp: TCSSResCompValue; AttrID: TCSSNumericalID;
      UseNaNOnFail: boolean = false; NoChildren: boolean = false): TFresnelLength; virtual; overload;
    function GetComputedString(Attr: TFresnelCSSAttribute): string; virtual;
    function GetComputedCSSString(AttrID: TCSSNumericalID): string; virtual; overload;
    function GetComputedCSSString(const AttrName: string): string; overload;
    function GetComputedCSSKeyword(AttrID: TCSSNumericalID; const AllowedKeywords: TCSSNumericalIDArray): TCSSNumericalID; virtual;
    function GetComputedBorderWidth(Attr: TFresnelCSSAttribute): TFresnelLength; virtual;
    function GetComputedBorderRadius(Corner: TFresnelCSSCorner): TFresnelPoint; virtual; // on fail returns 0
    function GetComputedColor(Attr: TFresnelCSSAttribute; const CurrentColor: TFPColor): TFPColor; virtual; // on fail returns transparent
    function GetComputedKeyword(Attr: TFresnelCSSAttribute; const AllowedKeywords: TCSSNumericalIDArray): TCSSNumericalID; virtual;
    function GetComputedJustifyContent(out SubKW: TCSSNumericalID): TCSSNumericalID; virtual;
    function GetComputedJustifyItems(out SubKW: TCSSNumericalID): TCSSNumericalID; virtual;
    function GetComputedJustifySelf(out SubKW: TCSSNumericalID): TCSSNumericalID; virtual;
    function GetComputedAlignContent(out SubKW: TCSSNumericalID): TCSSNumericalID; virtual;
    function GetComputedAlignItems(out SubKW: TCSSNumericalID): TCSSNumericalID; virtual;
    function GetComputedAlignSelf(out SubKW: TCSSNumericalID): TCSSNumericalID; virtual;
    function GetComputedBackgroundOrigin: TCSSNumericalID; virtual;
    procedure GetComputedMarginBlockStartEndAttr(out aStartAttr, aEndAttr: TFresnelCSSAttribute); virtual;
    procedure GetComputedMarginInlineStartEndAttr(out aStartAttr, aEndAttr: TFresnelCSSAttribute); virtual;
    function GetComputedTextShadow(out aOffsetX, aOffsetY, aRadius: TFresnelLength; out aColor: TFPColor): boolean; virtual; // on fail returns 0
    function GetComputedImage(Attr: TFresnelCSSAttribute): TFresnelCSSBackgroundInfo; virtual; // on fail returns nil
    function GetComputedLinearGradient(const LGParams: string): TFresnelCSSLinearGradient; virtual; // on fail returns nil
    function GetComputedZIndex: integer; virtual;
    function GetUnsetCSSString(Attr: TFresnelCSSAttribute): string; virtual;
    function GetPixPerUnit(anUnit: TCSSUnit; IsHorizontal: boolean): TFresnelLength; virtual;
    procedure SetComputedCSSString(Attr: TFresnelCSSAttribute; const Value: string); virtual;
    procedure WriteComputedAttributes(Title: string);
    property ComputedAttribute[Attr: TFresnelCSSAttribute]: string read GetComputedString write SetComputedCSSString;
    property ComputedBoxSizing: TCSSNumericalID read FComputedBoxSizing;
    property ComputedDirection: TCSSNumericalID read FComputedDirection;
    property ComputedDisplayInside: TCSSNumericalID read FComputedDisplayInside; // none, flow, flow-root, flex, etc
    property ComputedDisplayOutside: TCSSNumericalID read FComputedDisplayOutside; // none, block, inline
    property ComputedFloat: TCSSNumericalID read FComputedFloat;
    property ComputedOverflowX: TCSSNumericalID read FComputedOverflowX;
    property ComputedOverflowY: TCSSNumericalID read FComputedOverflowY;
    property ComputedPosition: TCSSNumericalID read FComputedPosition;
    property ComputedVisibility: TCSSNumericalID read FComputedVisibility;
    property ComputedWritingMode: TCSSNumericalID read FComputedWritingMode;
    // Layouter
    function GetIntrinsicContentSize(aMode: TFresnelLayoutMode; aMaxWidth: TFresnelLength = NaN; aMaxHeight: TFresnelLength = NaN): TFresnelPoint; virtual; // ignoring min|max-height
    function GetContainerContentWidth(UseNaNOnFail: boolean): TFresnelLength; virtual; // used value
    function GetContainerContentHeight(UseNaNOnFail: boolean): TFresnelLength; virtual; // used value
    function GetContainerOffset: TFresnelPoint; virtual;
    function IsBlockFormattingContext: boolean; virtual;
    property LayoutNode: TFresnelLayoutNode read FLayoutNode write FLayoutNode;
    property UsedBorderBox: TFresnelRect read FUsedBorderBox write FUsedBorderBox; // relative to layout parent's used contentbox
    property UsedContentBox: TFresnelRect read FUsedContentBox write FUsedContentBox; // relative to layout parent's used contentbox
    // Renderer
    function GetBorderBoxOnViewport: TFresnelRect; virtual;
    property Rendered: boolean read FRendered write FRendered;
    property RenderedBorderBox: TFresnelRect read FRenderedBorderBox write FRenderedBorderBox; // relative to layout parent's rendered contentbox
    property RenderedContentBox: TFresnelRect read FRenderedContentBox write FRenderedContentBox; // relative to layout parent's rendered contentbox
    // Events
    function AddEventListener(aID : TEventID; aHandler : TFresnelEventHandler) : Integer;
    function AddEventListener(Const aName: TEventName; aHandler : TFresnelEventHandler) : Integer;
    Function DispatchEvent(aEvent : TAbstractEvent) : Integer;
    property EventDispatcher: TFresnelEventDispatcher Read FEventDispatcher;
    // Font
    property Font: IFresnelFont read GetFont write FFont;
    // Viewport
    property Resolver: TCSSResolver read FResolver;
    property ViewportConnected: boolean read GetViewportConnected write SetViewportConnected; // true for example if using a Font of Viewport
    property Viewport: TFresnelViewport read FViewPort;
  published
    property CSSClasses: TStrings read FCSSClasses write SetCSSClasses;
    property Style: string read FStyle write SetStyle;
    property OnClick: TFresnelEventHandler Index evtClick Read GetEventHandler Write SetEventHandler;
    property OnMouseDown: TFresnelMouseEventHandler Index evtMouseDown Read GetMouseEventHandler Write SetMouseEventHandler;
    property OnMouseMove: TFresnelMouseEventHandler Index evtMouseMove Read GetMouseEventHandler Write SetMouseEventHandler;
    property OnMouseEnter: TFresnelMouseEventHandler Index evtMouseEnter Read GetMouseEventHandler Write SetMouseEventHandler;
    property OnMouseLeave: TFresnelMouseEventHandler Index evtMouseLeave Read GetMouseEventHandler Write SetMouseEventHandler;
    property OnMouseUp: TFresnelMouseEventHandler Index evtMouseUp Read GetMouseEventHandler Write SetMouseEventHandler;
    property OnFocus: TFresnelFocusEventHandler Index evtFocus Read GetFocusEventHandler Write SetFocusEventHandler;
    property OnEnter: TFresnelFocusEventHandler Index evtFocusIn Read GetFocusEventHandler Write SetFocusEventHandler;
    property OnLeave: TFresnelFocusEventHandler Index evtFocusOut Read GetFocusEventHandler Write SetFocusEventHandler;
    property BeforeRendering: TNotifyEvent Read FBeforeRender Write FBeforeRender;
    property AfterRendered: TNotifyEvent Read FAfterRender Write FAfterRender;
  end;
  TFresnelElementClass = class of TFresnelElement;
  TFresnelElementArray = array of TFresnelElement;

  { TPseudoElement }

  TPseudoElement = class(TFresnelElement)
  public
    constructor Create(AOwner: TComponent); override;
    class function CSSTypeID: TCSSNumericalID; override;
    class function CSSTypeName: TCSSString; override;
    { ICSSNode }
    // CSS identifier implementation of ICSSNode
    function GetCSSID: TCSSString; override;
    function GetCSSTypeName: TCSSString; override;
    function GetCSSTypeID: TCSSNumericalID; override;
    // CSS parent implementation of ICSSNode
    function GetCSSIndex: integer; override;
    // CSS siblings implementation of ICSSNode
    function GetCSSNextSibling: ICSSNode; override;
    function GetCSSPreviousSibling: ICSSNode; override;
    function GetCSSNextOfType: ICSSNode; override;
    function GetCSSPreviousOfType: ICSSNode; override;
  end;

  { TPseudoElSelection }

  TPseudoElSelection = class(TPseudoElement)
  private
    class var FFresnelSelectionTypeID: TCSSNumericalID;
  public
    function GetCSSPseudoElementID: TCSSNumericalID; override;
    function GetCSSPseudoElementName: TCSSString; override;
  end;

  { TReplacedElement - base class for elements with special content and no child elements, e.g. label, video }

  TReplacedElement = class(TFresnelElement)
  public
    function AcceptChildrenAtDesignTime: boolean; override;
  end;

  TFresnelViewportLength = (
    vlDPIHorizontal,
    vlDPIVertical,
    vlHorizontalScrollbarWidth,
    vlVerticalScrollbarWidth
    );
  TFresnelViewportLengths = set of TFresnelViewportLength;

  { TFresnelFontEngine }

  TFresnelFontEngine = class(TComponent)
  public
    class var WSEngine: TFresnelFontEngine;
    function Allocate(const Desc: TFresnelFontDesc): IFresnelFont; virtual; abstract;
    // DeAllocate is done automatically when the last reference is released
  end;
  TFLFontEngine = TFresnelFontEngine;

  IFresnelVPApplication = interface ['{2075A1FE-F729-4083-8CE5-01932ADB4D69}']
    function GetHoverElements: TFresnelElementArray;
    function GetMouseDownElement(out PageXY: TFresnelPoint): TFresnelElement;
    procedure SetHoverElements(const ElArr: TFresnelElementArray);
    procedure SetMouseDownElement(El: TFresnelElement; const PageXY: TFresnelPoint);
  end;

  { TFresnelViewport }

  TFresnelViewport = class(TFresnelElement)
  private
    FDomModified: boolean;
    FFontEngine: TFresnelFontEngine;
    FLayouter: TFresnelLayouter;
    FOnDomChanged: TNotifyEvent;
    FStylesheetStamp: TCSSNumericalID;
    FStylesheetResolverStamp: TCSSNumericalID;
    FStylesheet: TStrings;
    FDPI: array[boolean] of TFresnelLength;
    FScrollbarWidth: array[boolean] of TFresnelLength;
    FHeight: TFresnelLength;
    FWidth: TFresnelLength;
    FFocusedElement : TFresnelElement;
    procedure CSSResolverLog(Sender: TObject; Entry: TCSSResolverLogEntry);
    function GetFocusedElement: TFresnelElement;
    procedure SetFocusedElement(const aValue: TFresnelElement);
  protected
    class var FFresnelEventsRegistered: boolean;
  protected
    FVPApplication: IFresnelVPApplication;
    procedure Bubble(lElement: TFresnelElement; lEvt: TFresnelEvent);
    function GetDPI(IsHorizontal: boolean): TFresnelLength; override;
    function GetHeight: TFresnelLength; virtual;
    function GetScrollbarWidth(IsHorizontal: boolean): TFresnelLength; virtual;
    function GetVPLength(l: TFresnelViewportLength): TFresnelLength; virtual;
    function GetWidth: TFresnelLength; virtual;
    procedure FPOObservedChanged(ASender: TObject; {%H-}Operation: TFPObservedOperation; {%H-}Data: Pointer); override;
    procedure InitResolver; virtual;
    procedure UpdateResolverStylesheet; virtual;
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    procedure SetDPI(IsHorizontal: boolean; const AValue: TFresnelLength);
    procedure SetHeight(AValue: TFresnelLength); virtual;
    procedure SetScrollbarWidth(IsHorizontal: boolean;
      const AValue: TFresnelLength); virtual;
    procedure SetStylesheet(AValue: TStrings); virtual;
    procedure SetVPLength(l: TFresnelViewportLength;
      const AValue: TFresnelLength); virtual;
    procedure SetWidth(AValue: TFresnelLength); virtual;
    procedure StylesheetChanged; virtual;
    function TrySetFocusControl(aControl : TFresnelElement) : Boolean;
  public type

      { TBubbleMouseClickEvent }

      TBubbleMouseClickEvent = class(TFresnelMouseClickEvent)
      private
        FElement: TFresnelElement;
        procedure SetXY(const XY: TFresnelPoint);
      public
        property Element: TFresnelElement read FElement;
      end;

      { TBubbleMouseDoubleClickEvent }

      TBubbleMouseDoubleClickEvent = class(TFresnelMouseDoubleClickEvent)
      private
        FElement: TFresnelElement;
        procedure SetXY(const XY: TFresnelPoint);
      public
        property Element: TFresnelElement read FElement;
      end;

      { TBubbleMouseDownEvent }

      TBubbleMouseDownEvent = class(TFresnelMouseDownEvent)
      private
        FElement: TFresnelElement;
        procedure SetXY(const XY: TFresnelPoint);
      public
        property Element: TFresnelElement read FElement;
      end;

      { TBubbleMouseMoveEvent }

      TBubbleMouseMoveEvent = class(TFresnelMouseMoveEvent)
      private
        FElement: TFresnelElement;
        procedure SetXY(const XY: TFresnelPoint);
      public
        property Element: TFresnelElement read FElement;
      end;

      { TBubbleMouseUpEvent }

      TBubbleMouseUpEvent = class(TFresnelMouseUpEvent)
      private
        FElement: TFresnelElement;
        procedure SetXY(const XY: TFresnelPoint);
      public
        property Element: TFresnelElement read FElement;
      end;

  public
    const StylesheetName = 'author';
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure ApplyCSS; virtual;
    procedure DomChanged; override;
    procedure Disconnecting; virtual;
    function AllocateFont(const Desc: TFresnelFontDesc): IFresnelFont; virtual;
    function IsDrawing: boolean; virtual; abstract;
    function GetCSSString(AttrID: TCSSNumericalID; Compute: boolean; out Complete: boolean): string; override;
    class function CSSTypeID: TCSSNumericalID; override;
    class function CSSTypeName: TCSSString; override;
    class function GetCSSTypeStyle: TCSSString; override;
    function GetElementAt(const x, y: TFresnelLength): TFresnelElement; virtual;
    function GetElementsAt(const x, y: TFresnelLength): TFresnelElementArray; virtual;
    function ContentToPagePos(El: TFresnelElement; const x, y: TFresnelLength): TFresnelPoint; virtual; overload; // content box of El to content of viewport
    function ContentToPagePos(El: TFresnelElement; const p: TFresnelPoint): TFresnelPoint; overload; // content box of El to content of viewport
    function PageToContentPos(El: TFresnelElement; const x, y: TFresnelLength): TFresnelPoint; virtual; overload; // content of viewport to content box of El
    function PageToContentPos(El: TFresnelElement; const p: TFresnelPoint): TFresnelPoint; virtual; overload; // content of viewport to content box of El
    procedure WSMouseXY(WSData: TFresnelMouseEventInit; MouseEventId: TEventID); virtual;
    // Return true if default was prevented
    function WSKey(WSData: TFresnelKeyEventInit; KeyEventId: TEventID) : boolean; virtual;
    function WSInput(WSData: TFresnelInputEventInit) : boolean; virtual;
    property VPApplication: IFresnelVPApplication read FVPApplication;
    property DPI[IsHorizontal: boolean]: TFresnelLength read GetDPI write SetDPI;
    property ScrollbarWidth[IsHorizontal: boolean]: TFresnelLength read GetScrollbarWidth write SetScrollbarWidth;
    property VPLength[l: TFresnelViewportLength]: TFresnelLength read GetVPLength write SetVPLength;
    property Resolver: TCSSResolver read FResolver;
    property Stylesheet: TStrings read FStylesheet write SetStylesheet; // CSS origin author
    property StylesheetStamp: TCSSNumericalID read FStylesheetStamp;
    property Width: TFresnelLength read GetWidth write SetWidth;
    property Height: TFresnelLength read GetHeight write SetHeight;
    property Layouter: TFresnelLayouter read FLayouter write FLayouter;
    property FontEngine: TFresnelFontEngine read FFontEngine write FFontEngine;
    property OnDomChanged: TNotifyEvent read FOnDomChanged write FOnDomChanged;
    property DomModified: boolean read FDomModified write FDomModified;
    Property FocusedElement : TFresnelElement read FFocusedElement Write SetFocusedElement;
  end;
  TFLViewPort = TFresnelViewport;

  IFresnelStreamRoot = interface
    ['{A53F44CF-3BFB-42F6-A711-8C555E835E7F}']
    function GetViewport: TFresnelViewport;
  end;

var
  CSSRegistry: TFresnelCSSRegistry;

function FPColorToCSS(const c: TFPColor): string;
function CSSToFPColor(const s: string; out c: TFPColor): boolean;

function FontVariantEastAsiansToStr(const FVEastAsians: TFresnelCSSFontVarEastAsians): string;
function FontVariantLigaturesToStr(const FVLigatures: TFresnelCSSFontVarLigaturesSet): string;
function FontVariantNumericsToStr(const FVNumerics: TFresnelCSSFontVarNumerics): string;

function dbgs(const p: TFresnelPoint): string; overload;
function dbgs(const r: TFresnelRect): string; overload;
function dbgs(const c: TFPColor): string; overload;

implementation

function FPColorToCSS(const c: TFPColor): string;
const
  hex: array[0..15] of char = '0123456789abcdef';
var
  br, bg, bb, ba: Byte;
begin
  if (lo(c.Red)=hi(c.Red))
      and (lo(c.Green)=hi(c.Green))
      and (lo(c.Blue)=hi(c.Blue))
      and (lo(c.Alpha)=hi(c.Alpha)) then
  begin
    br:=lo(c.Red);
    bg:=lo(c.Green);
    bb:=lo(c.Blue);
    ba:=lo(c.Alpha);
    if (br and 15 = br shr 4)
        and (bg and 15 = bg shr 4)
        and (bb and 15 = bb shr 4)
        and (ba and 15 = ba shr 4) then
    begin
      Result:='#'+hex[br and 15]+hex[bg and 15]+hex[bb and 15];
      if c.Alpha<>alphaOpaque then
        Result:=Result+hex[ba and 15];
    end else begin
      Result:='#'+hex[br shr 4]+hex[br and 15]
                 +hex[bg shr 4]+hex[bg and 15]
                 +hex[bb shr 4]+hex[bb and 15];
      if c.Alpha<>alphaOpaque then
        Result:=Result+hex[ba shr 4]+hex[ba and 15];
    end;
  end else begin
    Result:='#'+HexStr(c.Red,4)+HexStr(c.Green,4)+HexStr(c.Blue,4);
    if c.Alpha<>alphaOpaque then
      Result:=Result+HexStr(c.Alpha,4);
  end;
end;

function CSSToFPColor(const s: string; out c: TFPColor): boolean;

  function ReadColor(var p: PChar; Count: byte): word;
  var
    i: Integer;
    ch: Char;
    v: word;
  begin
    Result:=0;
    for i:=1 to Count do
    begin
      ch:=p^;
      case ch of
      '0'..'9': v:=ord(ch)-ord('0');
      'a'..'f': v:=ord(ch)-ord('a')+10;
      'A'..'F': v:=ord(ch)-ord('A')+10;
      end;
      Result:=Result shl 4+v;
      inc(p);
    end;
    case Count of
    1: begin Result:=Result shl 4+Result; Result:=Result shl 8+Result; end;
    2: Result:=Result shl 8+Result;
    3: Result:=Result shl 4+Result and $f;
    end;
  end;

var
  HexPerColor: byte;
  p, StartP: PChar;
  HasAlpha: Boolean;
  KW: TCSSNumericalID;
begin
  Result := False;
  c.Red := 0;
  c.Green := 0;
  c.Blue := 0;
  c.Alpha := alphaOpaque;
  if s='' then
    exit;
  p:=@s[1];
  if p^='#' then
  begin
    inc(p);
    StartP:=p;
    repeat
      case p^ of
      #0:
        if p-StartP+1=length(s) then
          break
        else
          exit;
      '0'..'9','a'..'f','A'..'F': inc(p);
      else
        exit;
      end;
    until false;
    p:=StartP;
    case length(s) of
    4:
      begin
        HexPerColor:=1;
        HasAlpha:=false;
      end;
    5:
      begin
        HexPerColor:=1;
        HasAlpha:=true;
      end;
    7:
      begin
        HexPerColor:=2;
        HasAlpha:=false;
      end;
    9:
      begin
        HexPerColor:=2;
        HasAlpha:=true;
      end;
    13:
      begin
        HexPerColor:=4;
        HasAlpha:=false;
      end;
    17:
      begin
        HexPerColor:=4;
        HasAlpha:=true;
      end;
    else
      exit;
    end;
    c.Red:=ReadColor(p,HexPerColor);
    c.Green:=ReadColor(p,HexPerColor);
    c.Blue:=ReadColor(p,HexPerColor);
    if HasAlpha then
      c.Alpha:=ReadColor(p,HexPerColor);
  end else begin
    KW:=CSSRegistry.IndexOfKeyword(s);
    if (KW<CSSRegistry.kwFirstColor) or (KW>CSSRegistry.kwLastColor) then exit;
    c:=FPColor(CSSRegistry.GetKeywordColor(KW));
  end;
  Result:=true;
end;

function FontVariantEastAsiansToStr(const FVEastAsians: TFresnelCSSFontVarEastAsians): string;
var
  ea: TFresnelCSSFontVarEastAsian;
begin
  Result:='';
  for ea in FVEastAsians do
  begin
    if Result>'' then Result+=' ';
    Result+=FresnelCSSFontVarEastAsianNames[ea];
  end;
end;

function FontVariantLigaturesToStr(const FVLigatures: TFresnelCSSFontVarLigaturesSet): string;
var
  l: TFresnelCSSFontVarLigatures;
begin
  Result:='';
  for l in FVLigatures do
  begin
    if Result>'' then Result+=' ';
    Result+=FresnelCSSFontVarLigaturesNames[l];
  end;
end;

function FontVariantNumericsToStr(const FVNumerics: TFresnelCSSFontVarNumerics): string;
var
  n: TFresnelCSSFontVarNumeric;
begin
  Result:='';
  for n in FVNumerics do
  begin
    if Result>'' then Result+=' ';
    Result+=FresnelCSSFontVarNumericNames[n];
  end;
end;

function dbgs(const p: TFresnelPoint): string;
begin
  Result:=FloatToStr(p.X)+','+FloatToStr(p.Y);
end;

function dbgs(const r: TFresnelRect): string;
begin
  Result:=FloatToStr(r.Left)+','+FloatToStr(r.Top)+','+FloatToStr(r.Right)+','+FloatToStr(r.Bottom);
end;

function dbgs(const c: TFPColor): string;

  function IsByte(w: word): boolean;
  begin
    Result:=lo(w)=hi(w);
  end;

begin
  if IsByte(c.Alpha) and IsByte(c.Red) and IsByte(c.Green) and IsByte(c.Blue) then
    Result:='$'+HexStr(c.Alpha shr 8,2)+HexStr(c.Red shr 8,2)+HexStr(c.Green shr 8,2)+HexStr(c.Blue shr 8,2)
  else
    Result:='$A'+HexStr(c.Alpha,4)+'R'+HexStr(c.Red,4)+'G'+HexStr(c.Green,4)+'B'+HexStr(c.Blue,4);
end;

{ TFresnelCSSRegistry }

function TFresnelCSSRegistry.CheckDisplay(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_Display_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckPosition(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_Position_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckBoxSizing(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_BoxSizing_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckDirection(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_Direction_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckWritingMode(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_WritingMode_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckZIndex(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_ZIndex_Dim);
end;

function TFresnelCSSRegistry.CheckOverflowXY(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_OverflowXY_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckClear(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_Clear_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckLeftTopRightBottom(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_LeftTopRightBottom_Dim);
end;

function TFresnelCSSRegistry.CheckWidthHeight(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_WidthHeight_Dim);
end;

function TFresnelCSSRegistry.CheckBorderWidth(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_BorderWidth_Dim);
end;

function TFresnelCSSRegistry.CheckColor(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Color(Chk_Color_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckBorderStyle(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_BorderStyle_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckBorder(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=true;
  repeat
    if Chk_BorderWidth_Dim.Fits(Resolver.CurComp) then
      exit;
    if IsColor(Resolver.CurComp) then
      exit;
    if Resolver.IsKeywordIn(Chk_BorderStyle_KeywordIDs) then
      exit;
    // todo warn invalid
  until Resolver.ReadNext;
  Result:=false;
end;

function TFresnelCSSRegistry.CheckBorderRadius(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_BorderRadius_Dim);
end;

function TFresnelCSSRegistry.CheckFloat(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_Float_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFontFeatureSettings(Resolver: TCSSBaseResolver): boolean;
begin
  // not yet supported
  Result:=Resolver.CheckAttribute_Keyword(Chk_FontFeatureSettings_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFontFamily(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=true;
  repeat
    case Resolver.CurComp.Kind of
    rvkString, rvkKeyword, rvkKeywordUnknown:
      exit;
    rvkFunction:
      // todo
    else
      break;
    end;
    // todo warn invalid
  until not Resolver.ReadNext;
  Result:=false;
end;

function TFresnelCSSRegistry.CheckFontKerning(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_FontKerning_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFontSize(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_FontSize_Dim);
end;

function TFresnelCSSRegistry.CheckFontStyle(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_FontStyle_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFontVariantAlternates(Resolver: TCSSBaseResolver): boolean;
begin
  // todo: functions like stylistic()
  Result:=Resolver.CheckAttribute_Keyword(Chk_FontVariantAlternates_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFontVariantCaps(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_FontVariantCaps_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFontVariantEastAsian(Resolver: TCSSBaseResolver): boolean;
var
  HasRuby, HasEastAsianVariant, HasEastAsianWidth: Boolean;
  KW: TCSSNumericalID;
begin
  Result:=false;
  HasRuby:=false;
  HasEastAsianVariant:=false;
  HasEastAsianWidth:=false;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwRuby:
          begin
            if HasRuby then
              exit; // todo warn
            HasRuby:=true;
          end;
        kwJis78,kwJis83,kwJis90,kwJis04,kwSimplified,kwTraditional:
          begin
            if HasEastAsianVariant then
              exit; // todo warn
            HasEastAsianVariant:=true;
          end;
        kwProportionalWidth,kwFullWidth:
          begin
            if HasEastAsianWidth then
              exit; // todo warn
            HasEastAsianWidth:=true;
          end;
        else
          // todo warn
          exit;
        end;
      end;
    rvkFunction:
      // todo
      ;
    rvkInvalid:
      // todo warn
      exit;
    else
      break;
    end;
  until not Resolver.ReadNext;
  Result:=true;
end;

function TFresnelCSSRegistry.CheckFontVariantEmoji(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_FontVariantEmoji_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFontVariantLigatures(Resolver: TCSSBaseResolver): boolean;
var
  KW: TCSSNumericalID;
  HasCommonLigatures, HasDiscretionaryLigatures, HasHistoricalLigatures,
    HasContextualLigatures: Boolean;
begin
  Result:=false;
  HasCommonLigatures:=false;
  HasDiscretionaryLigatures:=false;
  HasHistoricalLigatures:=false;
  HasContextualLigatures:=false;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwCommonLigatures,kwNoCommonLigatures:
          begin
            if HasCommonLigatures then
              exit; // todo warn
            HasCommonLigatures:=true;
          end;
        kwDiscretionaryLigatures,kwNoDiscretionaryLigatures:
          begin
            if HasDiscretionaryLigatures then
              exit; // todo warn
            HasDiscretionaryLigatures:=true;
          end;
        kwHistoricalLigatures,kwNoHistoricalLigatures:
          begin
            if HasHistoricalLigatures then
              exit; // todo warn
            HasHistoricalLigatures:=true;
          end;
        kwContextual,kwNoContextual:
          begin
            if HasContextualLigatures then
              exit; // todo warn
            HasContextualLigatures:=true;
          end;
        else
          // todo warn
          exit;
        end;
      end;
    rvkFunction:
      // todo
      ;
    rvkInvalid:
      // todo warn
      exit;
    else
      exit(false);
    end;
  until not Resolver.ReadNext;

  Result:=true;
end;

function TFresnelCSSRegistry.CheckFontVariantNumeric(Resolver: TCSSBaseResolver): boolean;
var
  HasOrdinal, HasSlashedZero, HasNums, HasFractions: Boolean;
  KW: TCSSNumericalID;
begin
  Result:=false;
  HasOrdinal:=false;
  HasSlashedZero:=false;
  HasNums:=false;
  HasFractions:=false;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwOrdinal:
          begin
            if HasOrdinal then
              exit; // todo warn
            HasOrdinal:=true;
          end;
        kwSlashedZero:
          begin
            if HasSlashedZero then
              exit; // todo warn
            HasSlashedZero:=true;
          end;
        kwProportionalNums,kwTabularNums:
          begin
            if HasNums then
              exit; // todo warn
            HasNums:=true;
          end;
        kwDiagonalFractions,kwStackedFractions:
          begin
            if HasFractions then
              exit; // todo warn
            HasFractions:=true;
          end;
        else
          // todo warn
          exit;
        end;
      end;
    rvkFunction:
      // todo
      ;
    rvkInvalid:
      // todo
      exit;
    else
      break;
    end;
  until not Resolver.ReadNext;
  Result:=true;
end;

function TFresnelCSSRegistry.CheckFontVariantPosition(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_FontVariantPosition_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFontVariant(Resolver: TCSSBaseResolver): boolean;
var
  aAlternates: TCSSString;
  aCaps, aEmoji, aPosition: TCSSNumericalID;
  aEastAsian, aLigatures, aNumeric: TCSSNumericalIDArray;
begin
  Result:=ReadFontVariant(Resolver,true,aAlternates, aCaps, aEastAsian, aEmoji,
    aLigatures, aNumeric, aPosition);
end;

function TFresnelCSSRegistry.CheckFontWeight(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_FontWeight_Dim);
end;

function TFresnelCSSRegistry.CheckFontWidth(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_FontWidth_Dim);
end;

function TFresnelCSSRegistry.CheckLineHeight(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_LineHeight_Dim);
end;

function TFresnelCSSRegistry.CheckFont(Resolver: TCSSBaseResolver): boolean;
var
  aSystemFont: TCSSNumericalID;
  aFamily, aStyle, aWeight, aWidth, aSize, aLineHeight, aVariantCaps: TCSSString;
begin
  Result:=ReadFont(Resolver,true, aSystemFont, aFamily, aStyle, aWeight, aWidth,
    aSize, aLineHeight, aVariantCaps);
end;

function TFresnelCSSRegistry.CheckTextShadow(Resolver: TCSSBaseResolver): boolean;
// comma separated list of an optional color, offset-x, offset-y (lengths), optional blur-radius
var
  HasColor: Boolean;
  FloatCnt: Integer;
begin
  Result:=false;
  HasColor:=false;
  FloatCnt:=0;
  repeat
    if IsColor(Resolver.CurComp) then
    begin
      if HasColor then
        exit; // todo warn
      HasColor:=true;
    end else if (FloatCnt<2) and Chk_TextShadow_Offset_Dim.Fits(Resolver.CurComp) then
    begin
      // offset-x/y
      inc(FloatCnt);
    end else if (FloatCnt=2) and Chk_TextShadow_Radius_Dim.Fits(Resolver.CurComp) then
    begin
      // blur-radius
      inc(FloatCnt);
    end else if Resolver.IsSymbol(ctkCOMMA) then
    begin
      if FloatCnt<2 then
        exit; // missing offset
      HasColor:=false;
      FloatCnt:=0;
    end else
      ; // todo: warn garbage
  until not Resolver.ReadNext;
  Result:=true;
end;

function TFresnelCSSRegistry.CheckMargin(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_Margin_Dim);
end;

function TFresnelCSSRegistry.CheckOpacity(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_Opacity_Dim);
end;

function TFresnelCSSRegistry.CheckPadding(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_Padding_Dim);
end;

function TFresnelCSSRegistry.CheckVisible(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_Visible_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckBackgroundImage(Resolver: TCSSBaseResolver): boolean;
// read comma list of images
begin
  Result:=true;
  repeat
    if not CheckImage(Resolver,true) then
      exit(false);

    if not Resolver.ReadNext then
    begin
      // ok
      exit;
    end;
    if not Resolver.IsSymbol(ctkCOMMA) then
      exit(false);
  until not Resolver.ReadNext;
  Result:=false;
end;

function TFresnelCSSRegistry.CheckLinearGradient(Resolver: TCSSBaseResolver; Check: boolean
  ): boolean;
// --linear-gradient(comma separated list)
// First param is direction:
//   <angle>
//   to <side>
//   to <side> <side>
// Next params are each a color followed by one or two stops (percentage lenght)
var
  OldComp: TCSSResCompValue;
  Side: TCSSNumericalID;
begin
  Result:=false;

  OldComp:=Resolver.CurComp;
  try
    Resolver.CurComp.EndP:=Resolver.CurComp.BracketOpen+1;

    // read optional parameter: direction
    if not Resolver.ReadNext then exit;
    case Resolver.CurComp.Kind of
    rvkKeyword:
      if Resolver.CurComp.KeywordID=kwTo then
      begin
        // "to <side or corner>", a corner is two sides
        if not Resolver.ReadNext then
        begin
          if Check then
            ; // todo warn
          exit;
        end;

        // first side
        if Resolver.CurComp.Kind<>rvkKeyword then exit;
        Side:=Resolver.CurComp.KeywordID;
        if not (Side in [kwLeft,kwRight,kwTop,kwBottom]) then exit;

        if not Resolver.ReadNext then exit;
        if Resolver.CurComp.Kind=rvkKeyword then
        begin
          // second side
          if Side in [kwLeft,kwRight] then
          begin
            if not (Resolver.CurComp.KeywordID in [kwTop,kwBottom]) then exit;
          end else begin
            if not (Resolver.CurComp.KeywordID in [kwLeft,kwRight]) then exit;
          end;
          if not Resolver.ReadNext then exit;
        end;

        if not Resolver.IsSymbol(ctkCOMMA) then exit;
        if not Resolver.ReadNext then exit;
      end;
    rvkFloat:
      if Resolver.CurComp.FloatUnit in cuAllAngles then
      begin
        // angle
        if not Resolver.ReadNext then exit;
        if not Resolver.IsSymbol(ctkCOMMA) then exit;
        if not Resolver.ReadNext then exit;
      end;
    end;

    // read comma separated list of colors and percentages
    repeat
      // read color and optional one or two stops
      if not IsColor(Resolver.CurComp) then
        exit;
      if not Resolver.ReadNext then exit;
      if Resolver.IsLengthOrPercentage(false) then
      begin
        if not Resolver.ReadNext then exit;
        if Resolver.IsLengthOrPercentage(false) then
        begin
          if not Resolver.ReadNext then exit;
        end;
      end;
      if (Resolver.CurComp.Kind=rvkSymbol) then
      begin
        case Resolver.CurComp.Symbol of
        ctkRPARENTHESIS:
          exit(true); // close bracket )
        ctkCOMMA:
          ; // read next stop
        else
          exit;
        end;
      end else
        exit;
    until not Resolver.ReadNext;
  finally
    Resolver.CurComp:=OldComp;
  end;
end;

function TFresnelCSSRegistry.CheckImage(Resolver: TCSSBaseResolver; Check: boolean): boolean;
begin
  Result:=false;
  case Resolver.CurComp.Kind of
  rvkFunction:
    if Resolver.CurComp.FunctionID=afLinearGradient then
      Result:=CheckLinearGradient(Resolver,Check);
  // todo: radial-gradient,
  // todo: conic-gradient,
  // todo: repeating-linear-gradient,
  // todo: repeating-radial-gradient,
  // todo: repeating-conic-gradient,
  // todo: image()
  // todo: cross-fade()
  // todo: image-set()
  end;
end;

function TFresnelCSSRegistry.ReadBackgroundPositionList(Resolver: TCSSBaseResolver; Check: boolean;
  out X, Y: TCSSString): boolean;
// read comma separated list of border-positions and split them into border-position-x and -y
var
  PosX, PosY: TCSSString;
begin
  Result:=false;
  X:='';
  Y:='';

  // read comma separated list
  repeat
    if ReadOneBackgroundPosition(Resolver,Check,PosX,PosY) then
    begin
      if X>'' then X+=', ';
      X+=PosX;
      if Y>'' then Y+=', ';
      Y+=PosY;
      Result:=true;
    end else
      exit;
    // read comma
    if not Resolver.ReadNext then exit;
    if not Resolver.IsSymbol(ctkCOMMA) then
      exit;
  until not Resolver.ReadNext;
end;

function TFresnelCSSRegistry.ReadOneBackgroundPosition(Resolver: TCSSBaseResolver; Check: boolean;
  out X, Y: TCSSString): boolean;
// left
// left 10px
// top
// center
// top center
// left 10px bottom 15%
// 10% center 15%
// 5% 50%
// right -10% 30%
// left bottom -10%

  function Combine(Kw: TCSSNumericalID; const anOffset: TCSSString): TCSSString;
  begin
    if Kw>0 then
    begin
      Result:=CSSRegistry.Keywords[Kw];
      if anOffset<>'' then
        Result+=' '+anOffset;
    end else
      Result:=anOffset;
  end;

var
  FirstIsX: Boolean;
  Keyword1, Keyword2, KW: TCSSNumericalID;
  Offset1, Offset2: TCSSString;
begin
  Result:=false;
  X:='';
  Y:='';
  Keyword1:=0;
  Offset1:='';
  Keyword2:=0;
  Offset2:='';
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        if (KW=kwLeft) or (KW=kwRight)
            or (KW=kwCenter)
            or (KW=kwTop) or (KW=kwBottom) then
        begin
          if (Keyword1=0) and (Offset1='') then
            Keyword1:=KW
          else if (Keyword2=0) and (Offset2='') then
            Keyword2:=KW
          else begin
            if Check then
              ; // todo warn
            break;
          end;
        end else
          break;
      end;
    rvkFunction:
      // todo: calc()
      break;
    rvkFloat:
      if Resolver.CurComp.FloatUnit in cuAllLengthsAndPercent then
      begin
        if (Offset1='') and (Keyword2=0) then
          Offset1:=Resolver.GetCompString
        else if Offset2='' then
        begin
          Offset2:=Resolver.GetCompString;
          break;
        end else
          break;
      end else
        break;
    else
      break;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  FirstIsX:=true;
  if (Keyword1=kwLeft) or (Keyword1=kwRight) then
  begin
    FirstIsX:=true;
    if (Keyword2=kwLeft) or (Keyword2=kwRight) then
      exit;
  end else if (Keyword1=kwTop) or (Keyword1=kwBottom) then
  begin
    FirstIsX:=false;
    if (Keyword2=kwTop) or (Keyword2=kwBottom) then
      exit;
  end else if (Keyword2=kwLeft) or (Keyword2=kwRight) then
  begin
    FirstIsX:=false;
    if (Keyword1=kwLeft) or (Keyword1=kwRight) then
      exit;
  end else if (Keyword2=kwTop) or (Keyword2=kwBottom) then
  begin
    FirstIsX:=true;
    if (Keyword1=kwTop) or (Keyword1=kwBottom) then
      exit;
  end;
  if FirstIsX then
  begin
    X:=Combine(Keyword1,Offset1);
    Y:=Combine(Keyword2,Offset2);
  end else begin
    X:=Combine(Keyword2,Offset2);
    Y:=Combine(Keyword1,Offset1);
  end;
  if X='' then
  begin
    if Y='' then exit;
    X:='0%';
  end;
  if Y='' then
    Y:='0%';
  Result:=true;
end;

function TFresnelCSSRegistry.ReadOneBackgroundSize(Resolver: TCSSBaseResolver; Check: boolean; out
  W, H: TCSSString): boolean;
begin
  Result:=false;
  W:='';
  H:='';
  if not Chk_BackgroundSize_Dim.Fits(Resolver.CurComp) then
  begin
    if Check then
      ; // todo warn
    exit(false);
  end;
  W:=Resolver.GetCompString;
  H:='auto';
  if not Resolver.ReadNext then
    exit(Resolver.CurComp.Kind<>rvkInvalid);
  if Chk_BackgroundSize_Dim.Fits(Resolver.CurComp) then
  begin
    H:=Resolver.GetCompString;
    Resolver.ReadNext;
  end;
  Result:=Resolver.CurComp.Kind=rvkNone;
end;

function TFresnelCSSRegistry.CheckBackgroundAttachment(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_CommaList_Keyword(Chk_BackgroundAttachment_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckBackgroundOrigin(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_CommaList_Keyword(Chk_BackgroundOrigin_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckBackgroundPositionXY(Resolver: TCSSBaseResolver): boolean;
var
  KW: TCSSNumericalID;
  IsX: Boolean;
begin
  Result:=false;
  IsX:=TFresnelCSSAttrDesc(Resolver.CurDesc).Attr=fcaBackgroundPositionX;
  repeat
    // read optional keyword
    KW:=0;
    if (Resolver.CurComp.Kind=rvkKeyword) then
    begin
      KW:=Resolver.CurComp.KeywordID;
      if IsX then
      begin
        if (KW=kwLeft) or (KW=kwRight) or (KW=kwCenter) then
          Result:=true
        else
          exit(false); // todo warn
      end else begin
        if (KW=kwTop) or (KW=kwBottom) or (KW=kwCenter) then
          Result:=true
        else
          exit(false); // todo warn
      end;
    end;
    // read offset
    if Resolver.IsLengthOrPercentage(false) then
    begin
      Result:=true;
      if not Resolver.ReadNext then exit;
    end else if KW=0 then
      exit(false);
    if Resolver.IsSymbol(ctkCOMMA) then
      // next
    else if Resolver.CurComp.Kind>rvkNone then
      exit(false); // garbage
  until not Resolver.ReadNext;
end;

function TFresnelCSSRegistry.CheckBackgroundPosition(Resolver: TCSSBaseResolver): boolean;
var
  X, Y: TCSSString;
begin
  Result:=ReadBackgroundPositionList(Resolver,true,X,Y);
end;

function TFresnelCSSRegistry.CheckBackgroundRepeat(Resolver: TCSSBaseResolver): boolean;
var
  X, Y: TCSSNumericalID;
begin
  Result:=ReadBackgroundRepeat(Resolver,true,X,Y);
end;

function TFresnelCSSRegistry.CheckBackgroundSize(Resolver: TCSSBaseResolver): boolean;
var
  W, H: TCSSString;
begin
  Result:=false;
  repeat
    if not ReadOneBackgroundSize(Resolver,true,W,H) then
      exit(false);
    if Resolver.IsSymbol(ctkCOMMA) then
      // read next
    else if Resolver.CurComp.Kind=rvkNone then
      exit(true)
    else
      exit(false); // garbage
  until not Resolver.ReadNext;
end;

function TFresnelCSSRegistry.CheckBackgroundClip(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_BackgroundClip_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckBackground(Resolver: TCSSBaseResolver): boolean;
var
  aAttachment, aClip, aColor, aImage, aOrigin, aPositionX, aPositionY, aRepeat, aSize: TCSSString;
begin
  Result:=ReadBackground(Resolver,true, aAttachment, aClip, aColor, aImage,
    aOrigin, aPositionX, aPositionY, aRepeat, aSize);
end;

function TFresnelCSSRegistry.CheckCursor(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_Cursor_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckScrollbarColor(Resolver: TCSSBaseResolver): boolean;
var
  i: Integer;
begin
  Result:=false;
  // check for 'auto'
  if (Resolver.CurComp.Kind=rvkKeyword)
      and (Resolver.CurComp.KeywordID=kwAuto) then
  begin
    Resolver.ReadNext;
    Result:=Resolver.CurComp.Kind=rvkNone;
    exit;
  end;
  // check for two colors
  for i:=1 to 2 do
  begin
    if IsColor(Resolver.CurComp) then
      // ok
    else if (Resolver.CurComp.Kind=rvkFunction) and (Resolver.CurComp.FunctionID=afVar) then
      // ignore here
      exit(true)
    else
      exit; // todo warn
  end;
  Result:=Resolver.CurComp.Kind=rvkNone;
end;

function TFresnelCSSRegistry.CheckScrollbarGutter(Resolver: TCSSBaseResolver): boolean;
var
  KW: TCSSNumericalID;
  HasStable, HasBothEdges: Boolean;
begin
  Result:=false;
  HasStable:=false;
  HasBothEdges:=false;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwAuto:
          begin
            if HasStable or HasBothEdges then exit(false);
            Resolver.ReadNext;
            Result:=Resolver.CurComp.Kind=rvkNone;
            exit;
          end;
        kwStable:
          if HasStable then
            exit // todo warn
          else
            HasStable:=true;
        kwBothEdges:
          if HasBothEdges then
            exit // todo warn
          else
            HasBothEdges:=true;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(true)
      else
        // todo warn
        exit(false);
    else
      // todo warn
      exit(false);
    end;
  until not Resolver.ReadNext;
  Result:=Resolver.CurComp.Kind=rvkNone;
end;

function TFresnelCSSRegistry.CheckScrollbarWidth(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_ScrollbarWidth_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFlexBasis(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_FlexBasis_Dim);
end;

function TFresnelCSSRegistry.CheckFlexDirection(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_FlexDirection_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFlexGrow(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_FlexGrow_Dim);
end;

function TFresnelCSSRegistry.CheckFlexShrink(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_FlexShrink_Dim);
end;

function TFresnelCSSRegistry.CheckFlexWrap(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Keyword(Chk_FlexWrap_KeywordIDs);
end;

function TFresnelCSSRegistry.CheckFlexFlow(Resolver: TCSSBaseResolver): boolean;
var
  Direction, Wrap: TCSSString;
begin
  Result:=ReadFlexFlow(Resolver,true,Direction,Wrap);
end;

function TFresnelCSSRegistry.CheckFlex(Resolver: TCSSBaseResolver): boolean;
var
  Grow, Shrink, Basis: TCSSString;
begin
  Result:=ReadFlex(Resolver,true,Grow,Shrink,Basis);
end;

function TFresnelCSSRegistry.CheckAlignContent(Resolver: TCSSBaseResolver): boolean;
var
  MainKW, SubKW: TCSSNumericalID;
begin
  Result:=ReadAlignContent(Resolver,true,MainKW,SubKW);
end;

function TFresnelCSSRegistry.CheckAlignItems(Resolver: TCSSBaseResolver): boolean;
var
  MainKW, SubKW: TCSSNumericalID;
begin
  Result:=ReadAlignItems(Resolver,true,MainKW,SubKW);
end;

function TFresnelCSSRegistry.CheckAlignSelf(Resolver: TCSSBaseResolver): boolean;
var
  MainKW, SubKW: TCSSNumericalID;
begin
  Result:=ReadAlignSelf(Resolver,true,MainKW,SubKW);
end;

function TFresnelCSSRegistry.CheckJustifyContent(Resolver: TCSSBaseResolver): boolean;
var
  MainKW, SubKW: TCSSNumericalID;
begin
  Result:=ReadJustifyContent(Resolver,true,MainKW,SubKW);
end;

function TFresnelCSSRegistry.CheckJustifyItems(Resolver: TCSSBaseResolver): boolean;
var
  MainKW, SubKW: TCSSNumericalID;
begin
  Result:=ReadJustifyItems(Resolver,true,MainKW,SubKW);
end;

function TFresnelCSSRegistry.CheckJustifySelf(Resolver: TCSSBaseResolver): boolean;
var
  MainKW, SubKW: TCSSNumericalID;
begin
  Result:=ReadJustifySelf(Resolver,true,MainKW,SubKW);
end;

function TFresnelCSSRegistry.CheckPlaceContent(Resolver: TCSSBaseResolver): boolean;
var
  AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID;
begin
  Result:=ReadPlaceContent(Resolver,true,AlignKW, AlignSubKW, JustifyKW, JustifySubKW);
end;

function TFresnelCSSRegistry.CheckPlaceItems(Resolver: TCSSBaseResolver): boolean;
var
  AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID;
begin
  Result:=ReadPlaceItems(Resolver,true,AlignKW, AlignSubKW, JustifyKW, JustifySubKW);
end;

function TFresnelCSSRegistry.CheckPlaceSelf(Resolver: TCSSBaseResolver): boolean;
var
  AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID;
begin
  Result:=ReadPlaceSelf(Resolver,true,AlignKW, AlignSubKW, JustifyKW, JustifySubKW);
end;

function TFresnelCSSRegistry.CheckColumnRowGap(Resolver: TCSSBaseResolver): boolean;
begin
  Result:=Resolver.CheckAttribute_Dimension(Chk_ColumnRowGap_Dim);
end;

function TFresnelCSSRegistry.CheckGap(Resolver: TCSSBaseResolver): boolean;
var
  Column, Row: TCSSString;
begin
  Result:=ReadGap(Resolver,true,Column,Row);
end;

function TFresnelCSSRegistry.ReadBackgroundRepeat(Resolver: TCSSBaseResolver; Check: boolean; out
  X, Y: TCSSNumericalID): boolean;
// For example:
// repeat
// repeat-x
// space
// round
// no-repeat
// repeat space
// space round
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  X:=0;
  Y:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        if KW=kwRepeatX then begin
          if X>0 then
            exit;
          X:=kwRepeatX;
        end else if KW=kwRepeatY then begin
          if Y>0 then
            exit;
          Y:=kwRepeatY;
        end else if (KW=kwRepeat)
            or (KW=kwNoRepeat)
            or (KW=kwSpace)
            or (KW=kwRound) then
        begin
          if X=0 then
          begin
            X:=KW;
          end else if Y=0 then
            Y:=KW
          else
            exit;
        end else begin
          if Check then
            ; // todo warn
          exit;
        end;
      end;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  if X=0 then
  begin
    if Y=0 then
      exit;
    X:=kwRepeat;
  end;
  if Y=0 then
  begin
    if X<>kwRepeatX then
      Y:=X
    else
      Y:=kwRepeat;
  end;
  Result:=true;
end;

function TFresnelCSSRegistry.ReadBackground(Resolver: TCSSBaseResolver; Check: boolean; out
  aAttachment, aClip, aColor, aImage, aOrigin, aPositionX, aPositionY, aRepeat, aSize: TCSSString
  ): boolean;
// comma separated list of layers. Each layer:
//   attachment image position/size repeat
// the border/padding/content-box value can appear 0..2 times: origin clip
// color may only occur in last layer

  procedure Add(var CommaList: TCSSString; const Addition: TCSSString);
  begin
    if CommaList='' then
      CommaList:=Addition
    else
      CommaList+=', '+Addition;
  end;

var
  SizeW, SizeH: TCSSString;
  CurAttachment, CurClip, CurImage, CurOrigin, CurPositionX, CurPositionY, CurRepeat, CurSize: String;
  KW: TCSSNumericalID;

  function HasLayer: boolean;
  begin
    Result:=(CurAttachment<>'') or (CurClip<>'') or (CurOrigin<>'') or (CurImage<>'')
         or (CurPositionX<>'') or (CurPositionY<>'') or (CurRepeat<>'') or (CurSize<>'');
  end;

  procedure AddLayer;
  begin
    if CurAttachment='' then
      CurAttachment:='scroll';
    if CurClip='' then
      CurClip:='border-box';
    if CurOrigin='' then
      CurOrigin:='padding-box';
    if CurImage='' then
      CurImage:='none';
    if CurPositionX='' then
      CurPositionX:='0%';
    if CurPositionY='' then
      CurPositionY:='0%';
    if CurRepeat='' then
      CurRepeat:='repeat';
    if CurSize='' then
      CurSize:='auto';
    Add(aAttachment,CurAttachment); CurAttachment:='';
    Add(aClip,CurClip); CurClip:='';
    Add(aOrigin,CurOrigin); CurOrigin:='';
    Add(aImage,CurImage); CurImage:='';
    Add(aPositionX,CurPositionX); CurPositionX:='';
    Add(aPositionY,CurPositionY); CurPositionY:='';
    Add(aRepeat,CurRepeat); CurRepeat:='';
    Add(aSize,CurSize); CurSize:='';
  end;

begin
  Result:=false;
  aAttachment:='';
  CurAttachment:='';
  aClip:='';
  CurClip:='';
  aColor:='';
  aImage:='';
  CurImage:='';
  aOrigin:='';
  CurOrigin:='';
  aPositionX:='';
  CurPositionX:='';
  aPositionY:='';
  CurPositionY:='';
  aRepeat:='';
  CurRepeat:='';
  aSize:='';
  CurSize:='';
  repeat
    if Chk_BackgroundPosition_Dim.Fits(Resolver.CurComp) then
    begin
      if CurPositionX>'' then
        exit; // duplicate position
      if not ReadOneBackgroundPosition(Resolver,Check,CurPositionX,CurPositionY) then
        exit;

      if not Resolver.ReadNext then
        break;
      if Resolver.IsSymbol(ctkDIV) then
      begin
        // read size
        if not Resolver.ReadNext then
        begin
          // todo: warn missing size
          break;
        end;
        if not ReadOneBackgroundSize(Resolver,Check,SizeW,SizeH) then
          exit;
        aSize:=SizeW+' '+SizeH;
      end;
    end;

    if IsColor(Resolver.CurComp) then
    begin
      if aColor<>'' then
        exit; // duplicate color
      aColor:=Resolver.GetCompString;
    end else begin
      case Resolver.CurComp.Kind of
      rvkKeyword:
        begin
          KW:=Resolver.CurComp.KeywordID;
          if Resolver.IsKeywordIn(KW,Chk_BackgroundAttachment_KeywordIDs) then begin
            CurAttachment:=Keywords[KW];
          end else if Resolver.IsKeywordIn(KW,Chk_BackgroundOrigin_KeywordIDs) then begin
            if CurOrigin='' then
              CurOrigin:=Keywords[KW]
            else if CurClip='' then
              CurClip:=Keywords[KW]
            else
              exit; // duplicate origin
          end else if KW=kwText then begin
            if CurClip<>'' then
              exit; // duplicate clip
            CurClip:=Keywords[KW];
          end else
            exit; // unknown keyword
        end;
      rvkFunction:
        if CheckImage(Resolver,Check) then
          CurImage:=Resolver.GetCompString
        else
          exit; // not an image
      end;
    end;

    if Resolver.IsSymbol(ctkCOMMA) then
    begin
      if not HasLayer then
        exit;
      AddLayer;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  if HasLayer then
  begin
    AddLayer;
    Result:=true;
  end else if aColor>'' then
    Result:=true;
end;

function TFresnelCSSRegistry.ReadFont(Resolver: TCSSBaseResolver; Check: boolean; out
  aSystemFont: TCSSNumericalID; out aFamily, aStyle, aWeight, aWidth, aSize, aLineHeight,
  aVariantCaps: TCSSString): boolean;
var
  KW: TCSSNumericalID;
  p: PCSSChar;
  Used: Boolean;
begin
  Result:=false;
  aSystemFont:=0;
  aFamily:='';
  aSize:='';
  aStyle:='';
  aWeight:='';
  aWidth:='';
  aLineHeight:='';
  aVariantCaps:='';

  if Resolver.CurComp.Kind=rvkKeyword then
  begin
    KW:=Resolver.CurComp.KeywordID;
    if (KW=kwCaption) or (KW=kwIcon) or (KW=kwMenu) or (KW=kwMessageBox)
        or (KW=kwSmallCaption) or (KW=kwStatusBar) then
    begin
      // system font
      aSystemFont:=KW;
      Result:=true;
      if Resolver.ReadNext then
        if Check then
          ; // todo warn garbage
      exit;
    end;
  end;

  // required: family and size
  // optional: style, variant, weight, stretch, height
  // style, variant, weight must precede size
  // stretch as single keyword
  // line-height behind size separated by /
  // family must be last
  repeat
    Used:=false;
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        if Resolver.IsKeywordIn(KW,Chk_FontStyle_KeywordIDs) then
        begin
          if aStyle>'' then
            exit; // todo warn
          aStyle:=Resolver.GetCompString;
          Used:=true;
        end
        else if (KW=kwNormal) or (KW=kwSmallCaps) then
        begin
          // variant: in font shorthand only normal and small-caps are allowed
          if aVariantCaps>'' then
            exit; // todo warn
          aVariantCaps:=Resolver.GetCompString;
          Used:=true;
        end
        else if Resolver.IsKeywordIn(KW,Chk_FontWeight_Dim.AllowedKeywordIDs) then
        begin
          if aWeight>'' then
            exit; // todo warn
          aWeight:=Resolver.GetCompString;
          Used:=true;
        end
        else if Resolver.IsKeywordIn(KW,Chk_FontWidth_Dim.AllowedKeywordIDs) then
        begin
          if aWidth>'' then
            exit; // todo warn
          aWidth:=Resolver.GetCompString;
          Used:=true;
        end
      end;
    rvkFloat:
      if Resolver.CurComp.FloatUnit=cuNone then
      begin
        if aWeight>'' then
          exit; // todo warn
        aWeight:=Resolver.GetCompString;
        Used:=true;
      end;
    end;

    if (not Used ) and Chk_FontSize_Dim.Fits(Resolver.CurComp) then
    begin
      if aSize>'' then
        exit; // todo warn
      aSize:=Resolver.GetCompString;
      p:=Resolver.CurComp.EndP;
      while p^=' ' do inc(p);
      if p^='/' then
      begin
        // read line-height
        if not Resolver.ReadNext then
          exit; // skipping / failed
        if not Resolver.ReadNext then
          exit; // todo warn: missing line-height
        if not Chk_LineHeight_Dim.Fits(Resolver.CurComp) then
          exit; // todo warn
        aLineHeight:=Resolver.GetCompString;
      end;
      Used:=true;
    end;

    if Used then
      aFamily:=''
    else
      aFamily:=Resolver.GetCompString;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  Result:=(aSize>'') and (aFamily>'');
end;

function TFresnelCSSRegistry.ReadFontVariant(Resolver: TCSSBaseResolver; Check: boolean; out
  aAlternates: TCSSString; out aCaps: TCSSNumericalID; out aEastAsian: TCSSNumericalIDArray; out
  aEmoji: TCSSNumericalID; out aLigatures, aNumeric: TCSSNumericalIDArray; out
  aPosition: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
  i: Integer;
  HasValue, HasNormalOrNone, HasRuby, HasEastAsianVariant, HasEastAsianWidth,
    HasCommonLigatures, HasOrdinal, HasDiscretionaryLigatures, HasHistoricalLigatures,
    HasContextualLigatures, HasSlashedZero, HasNums, HasFractions: Boolean;
begin
  Result:=false;
  aAlternates:='';
  aCaps:=0;
  aEastAsian:=[];
  aEmoji:=0;
  aLigatures:=[];
  aNumeric:=[];
  aPosition:=0;

  HasValue:=false;
  HasNormalOrNone:=false;
  HasRuby:=false;
  HasEastAsianVariant:=false;
  HasEastAsianWidth:=false;
  HasCommonLigatures:=false;
  HasDiscretionaryLigatures:=false;
  HasHistoricalLigatures:=false;
  HasContextualLigatures:=false;
  HasOrdinal:=false;
  HasSlashedZero:=false;
  HasNums:=false;
  HasFractions:=false;

  if Check then ; // log warnings

  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        if KW=kwNormal then
        begin
          // sets all to normal, must be the only value
          if HasValue then
            exit; // todo: warn
          HasNormalOrNone:=true;
        end else if KW=kwNone then
        begin
          // sets ligatures to none, all other to normal, must be the only value
          if HasValue then
            exit; // todo: warn
          HasNormalOrNone:=true;
          aLigatures:=[kwNone];
        end else if HasNormalOrNone then
        begin
          // todo: warn
          exit;
        end;
        HasValue:=true;

        case KW of
        kwRuby:
          begin
            if HasRuby then
              exit; // todo warn
            HasRuby:=true;
            Insert(KW,aEastAsian,length(aEastAsian));
          end;
        kwJis78,kwJis83,kwJis90,kwJis04,kwSimplified,kwTraditional:
          begin
            if HasEastAsianVariant then
              exit; // todo warn
            HasEastAsianVariant:=true;
            Insert(KW,aEastAsian,length(aEastAsian));
          end;
        kwProportionalWidth,kwFullWidth:
          begin
            if HasEastAsianWidth then
              exit; // todo warn
            HasEastAsianWidth:=true;
            Insert(KW,aEastAsian,length(aEastAsian));
          end;
        kwCommonLigatures,kwNoCommonLigatures:
          begin
            if HasCommonLigatures then
              exit; // todo warn
            HasCommonLigatures:=true;
            Insert(KW,aLigatures,length(aLigatures));
          end;
        kwDiscretionaryLigatures,kwNoDiscretionaryLigatures:
          begin
            if HasDiscretionaryLigatures then
              exit; // todo warn
            HasDiscretionaryLigatures:=true;
            Insert(KW,aLigatures,length(aLigatures));
          end;
        kwHistoricalLigatures,kwNoHistoricalLigatures:
          begin
            if HasHistoricalLigatures then
              exit; // todo warn
            HasHistoricalLigatures:=true;
            Insert(KW,aLigatures,length(aLigatures));
          end;
        kwContextual,kwNoContextual:
          begin
            if HasContextualLigatures then
              exit; // todo warn
            HasContextualLigatures:=true;
            Insert(KW,aLigatures,length(aLigatures));
          end;
        kwOrdinal:
          begin
            if HasOrdinal then
              exit; // todo warn
            HasOrdinal:=true;
            Insert(KW,aNumeric,length(aNumeric));
          end;
        kwSlashedZero:
          begin
            if HasSlashedZero then
              exit; // todo warn
            HasSlashedZero:=true;
            Insert(KW,aNumeric,length(aNumeric));
          end;
        kwProportionalNums,kwTabularNums:
          begin
            if HasNums then
              exit; // todo warn
            HasNums:=true;
            Insert(KW,aNumeric,length(aNumeric));
          end;
        kwDiagonalFractions,kwStackedFractions:
          begin
            if HasFractions then
              exit; // todo warn
            HasFractions:=true;
            Insert(KW,aNumeric,length(aNumeric));
          end;
        end;

        if Resolver.IsKeywordIn(KW,Chk_FontVariantAlternates_KeywordIDs) then
        begin
          if aAlternates>'' then
            exit; // todo: warn
          aAlternates:=Resolver.GetCompString;
        end else if Resolver.IsKeywordIn(KW,Chk_FontVariantCaps_KeywordIDs) then
        begin
          if aCaps>0 then
            exit; // todo: warn
          aCaps:=KW;
        end else if Resolver.IsKeywordIn(KW,Chk_FontVariantEmoji_KeywordIDs) then
        begin
          if aEmoji>0 then
            exit; // todo: warn
          aEmoji:=KW;
        end else if Resolver.IsKeywordIn(KW,Chk_FontVariantPosition_KeywordIDs) then
        begin
          if aPosition>0 then
            exit; // todo: warn
          aPosition:=KW;
        end else
          exit; // todo: warn
      end;
    rvkFunction:
      begin
        // todo
        HasValue:=true;
      end;
    else
      exit; // todo warn
    end;

  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;
  if not HasValue then
    exit;

  if aAlternates='' then aAlternates:='normal';
  if aCaps=0 then aCaps:=kwNormal;
  if aEastAsian=nil then aEastAsian:=[kwNormal];
  if aEmoji=0 then aEmoji:=kwNormal;
  if aLigatures=nil then aLigatures:=[kwNormal];
  if aNumeric=nil then aNumeric:=[kwNormal];
  if aPosition=0 then aPosition:=kwNormal;

  Result:=true;
end;

function TFresnelCSSRegistry.ReadFlex(Resolver: TCSSBaseResolver; Check: boolean; out Grow, Shrink,
  Basis: TCSSString): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  Grow:='';
  Shrink:='';
  Basis:='';
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        if Resolver.IsKeywordIn(Chk_FlexBasis_Dim.AllowedKeywordIDs) then
        begin
          if Basis>'' then
            exit; // invalid
        end else if KW=kwNone then
        begin
          if (Grow>'') or (Shrink>'') or (Basis>'') then
            exit; // invalid
          Grow:='0';
          Shrink:='0';
          Basis:='auto';
        end;
      end;
    rvkFloat:
      if Resolver.CurComp.FloatUnit=cuNone then
      begin
        if Grow='' then
          Grow:=Resolver.GetCompString
        else if Shrink='' then
          Shrink:=Resolver.GetCompString
        else if Basis='' then
          Basis:=Resolver.GetCompString
        else
          exit; // invalid
      end else if Resolver.CurComp.FloatUnit in cuAllLengthsAndPercent then begin
        if Basis>'' then
          exit; // invalid
        Basis:=Resolver.GetCompString;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else begin
        // todo
        exit;
      end;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  if (Grow='') and (Basis='') then exit;

  if Grow='' then Grow:='1';
  if Shrink='' then Shrink:='0';
  if Basis='' then Basis:='auto';
  Result:=true;
end;

function TFresnelCSSRegistry.ReadFlexFlow(Resolver: TCSSBaseResolver; Check: boolean; out
  Direction, Wrap: TCSSString): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  Direction:='';
  Wrap:='';
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        if Resolver.IsKeywordIn(Chk_FlexDirection_KeywordIDs) then
        begin
          if Direction>'' then
            exit;
          Direction:=Keywords[Kw];
        end else if Resolver.IsKeywordIn(Chk_FlexWrap_KeywordIDs) then
        begin
          if Wrap>'' then
            exit;
          Wrap:=Keywords[Kw];
        end else
          exit; // invalid
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit; // invalid
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  if (Direction='') and (Wrap='') then exit;

  if Direction='' then Direction:='row';
  if Wrap='' then Wrap:='nowrap';
  Result:=true;
end;

function TFresnelCSSRegistry.ReadAlignContent(Resolver: TCSSBaseResolver; Check: boolean; out
  MainKW, SubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  MainKW:=0;
  SubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwNormal, kwStretch:
          begin
            if MainKW>0 then
              exit;
            if SubKW>0 then
              exit;
            MainKW:=KW;
          end;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwCenter,
        kwSpaceAround,kwSpaceBetween,kwSpaceEvenly:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwSafe,kwUnsafe]) then
              exit;
            MainKW:=KW;
          end;
        kwSafe,kwUnsafe:
          begin
            if (MainKW>0) and not (MainKW in [kwStart, kwEnd, kwFlexStart, kwFlexEnd,
                kwCenter,
                kwSpaceAround,kwSpaceBetween,kwSpaceEvenly]) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwFirst,kwLast:
          begin
            if (MainKW>0) and (MainKW<>kwBaseline) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwBaseline:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwFirst,kwLast]) then
              exit;
            MainKW:=KW;
          end;
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  Result:=MainKW>0;
end;

function TFresnelCSSRegistry.ReadAlignItems(Resolver: TCSSBaseResolver; Check: boolean; out MainKW,
  SubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  MainKW:=0;
  SubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwNormal,kwStretch,kwAnchorCenter:
          begin
            if MainKW>0 then
              exit;
            if SubKW>0 then
              exit;
            MainKW:=KW;
          end;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwSelfStart,kwSelfEnd,
        kwCenter:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwSafe,kwUnsafe]) then
              exit;
            MainKW:=KW;
          end;
        kwSafe,kwUnsafe:
          begin
            if (MainKW>0) and not (MainKW in [kwStart, kwEnd, kwFlexStart, kwFlexEnd,
                kwSelfStart, kwSelfEnd, kwCenter]) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwFirst,kwLast:
          begin
            // baseline with optional first or last
            if (MainKW>0) and (MainKW<>kwBaseline) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwBaseline:
          begin
            if MainKW>0 then
              exit;
            // baseline with optional first or last
            if (SubKW>0) and not (SubKW in [kwFirst,kwLast]) then
              exit;
            MainKW:=KW;
          end;
        kwLegacy:
          begin
            // legacy followed by left or right
            MainKW:=KW;
            if not Resolver.ReadNext then exit;
            case Resolver.CurComp.Kind of
            rvkKeyword:
              begin
                KW:=Resolver.CurComp.KeywordID;
                if not (KW in [kwCenter,kwLeft,kwRight]) then
                  exit;
                SubKW:=KW;
              end;
            rvkFunction:
              if Resolver.CurComp.FunctionID=afVar then
                exit(Check)
              else
                exit;
            else
              exit;
            end;
          end
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  Result:=MainKW>0;
end;

function TFresnelCSSRegistry.ReadAlignSelf(Resolver: TCSSBaseResolver; Check: boolean; out MainKW,
  SubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  MainKW:=0;
  SubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwAuto, kwNormal, kwStretch, kwAnchorCenter:
          begin
            if MainKW>0 then
              exit;
            if SubKW>0 then
              exit;
            MainKW:=KW;
          end;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwSelfStart,kwSelfEnd,
        kwCenter:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwSafe,kwUnsafe]) then
              exit;
            MainKW:=KW;
          end;
        kwSafe,kwUnsafe:
          begin
            if (MainKW>0) and not (MainKW in [kwStart, kwEnd, kwFlexStart, kwFlexEnd,
                kwSelfStart, kwSelfEnd, kwCenter]) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwFirst,kwLast:
          begin
            if (MainKW>0) and (MainKW<>kwBaseline) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwBaseline:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwFirst,kwLast]) then
              exit;
            MainKW:=KW;
          end;
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  Result:=MainKW>0;
end;

function TFresnelCSSRegistry.ReadJustifyContent(Resolver: TCSSBaseResolver; Check: boolean; out
  MainKW, SubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  MainKW:=0;
  SubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwNormal, kwStretch:
          begin
            if MainKW>0 then
              exit;
            if SubKW>0 then
              exit;
            MainKW:=KW;
          end;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwCenter,kwLeft,kwRight,
        kwSpaceAround,kwSpaceBetween,kwSpaceEvenly:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwSafe,kwUnsafe]) then
              exit;
            MainKW:=KW;
          end;
        kwSafe,kwUnsafe:
          begin
            if (MainKW>0) and not (MainKW in [kwStart, kwEnd, kwFlexStart, kwFlexEnd,
                kwCenter,kwLeft,kwRight,
                kwSpaceAround,kwSpaceBetween,kwSpaceEvenly]) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  Result:=MainKW>0;
end;

function TFresnelCSSRegistry.ReadJustifySelf(Resolver: TCSSBaseResolver; Check: boolean; out
  MainKW, SubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  MainKW:=0;
  SubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwAuto, kwNormal:
          begin
            if MainKW>0 then
              exit;
            if SubKW>0 then
              exit;
            MainKW:=KW;
          end;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwSelfStart,kwSelfEnd,
        kwCenter,kwLeft,kwRight,
        kwStretch,
        kwAnchorCenter:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwSafe,kwUnsafe]) then
              exit;
            MainKW:=KW;
          end;
        kwSafe,kwUnsafe:
          begin
            if (MainKW>0) and not (MainKW in [kwStart, kwEnd, kwFlexStart, kwFlexEnd,
                kwSelfStart, kwSelfEnd, kwCenter, kwLeft, kwRight]) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwFirst,kwLast:
          begin
            if (MainKW>0) and (MainKW<>kwBaseline) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwBaseline:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwFirst,kwLast]) then
              exit;
            MainKW:=KW;
          end;
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  Result:=MainKW>0;
end;

function TFresnelCSSRegistry.ReadJustifyItems(Resolver: TCSSBaseResolver; Check: boolean; out
  MainKW, SubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  MainKW:=0;
  SubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwNormal,kwStretch,kwAnchorCenter:
          begin
            if MainKW>0 then
              exit;
            if SubKW>0 then
              exit;
            MainKW:=KW;
          end;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwSelfStart,kwSelfEnd,
        kwCenter,kwLeft,kwRight:
          begin
            if MainKW>0 then
              exit;
            if (SubKW>0) and not (SubKW in [kwSafe,kwUnsafe]) then
              exit;
            MainKW:=KW;
          end;
        kwSafe,kwUnsafe:
          begin
            if (MainKW>0) and not (MainKW in [kwStart, kwEnd, kwFlexStart, kwFlexEnd,
                kwSelfStart, kwSelfEnd, kwCenter, kwLeft, kwRight]) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwFirst,kwLast:
          begin
            // baseline with optional first or last
            if (MainKW>0) and (MainKW<>kwBaseline) then
              exit;
            if SubKW>0 then
              exit;
            SubKW:=KW;
          end;
        kwBaseline:
          begin
            if MainKW>0 then
              exit;
            // baseline with optional first or last
            if (SubKW>0) and not (SubKW in [kwFirst,kwLast]) then
              exit;
            MainKW:=KW;
          end;
        kwLegacy:
          begin
            // legacy can be followed by center, left or right
            MainKW:=KW;
            if not Resolver.ReadNext then
              exit(Resolver.CurComp.Kind<>rvkInvalid);
            case Resolver.CurComp.Kind of
            rvkKeyword:
              begin
                KW:=Resolver.CurComp.KeywordID;
                if not (KW in [kwCenter,kwLeft,kwRight]) then
                  exit;
                SubKW:=KW;
              end;
            rvkFunction:
              if Resolver.CurComp.FunctionID=afVar then
                exit(Check)
              else
                exit;
            else
              exit;
            end;
          end
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  Result:=MainKW>0;
end;

function TFresnelCSSRegistry.ReadGap(Resolver: TCSSBaseResolver; Check: boolean; out Column,
  Row: TCSSString): boolean;
begin
  Result:=false;
  Column:='';
  Row:='';
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      if Resolver.CurComp.KeywordID=kwNormal then
      begin
        if Column='' then
          Column:=Keywords[kwNormal]
        else if Row='' then
          Row:=Keywords[kwNormal]
        else
          exit;
      end else
        exit;
    rvkFloat:
      begin
        if not Resolver.IsLengthOrPercentage(false) then
          exit;
        if Column='' then
          Column:=Resolver.GetCompString
        else if Row='' then
          Row:=Resolver.GetCompString
        else
          exit;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit; // todo calc()
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  if Column='' then exit;
  if Row='' then Row:=Column;

  Result:=true;
end;

function TFresnelCSSRegistry.ReadPlaceContent(Resolver: TCSSBaseResolver; Check: boolean; out
  AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  AlignKW:=0;
  AlignSubKW:=0;
  JustifyKW:=0;
  JustifySubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwNormal, kwStretch:
          if AlignKW=0 then
          begin
            if AlignSubKW>0 then
              exit;
            AlignKW:=KW;
          end else if JustifyKW=0 then
          begin
            if JustifySubKW>0 then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwCenter,
        kwSpaceAround,kwSpaceBetween,kwSpaceEvenly:
          if AlignKW=0 then
          begin
            if not (AlignSubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            AlignKW:=KW;
          end else if JustifyKW=0 then
          begin
            if not (JustifySubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwLeft,kwRight:
          if JustifyKW=0 then
          begin
            if not (JustifySubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwSafe,kwUnsafe:
          if AlignKW=0 then
          begin
            if AlignSubKW>0 then
              exit;
            AlignSubKW:=KW;
          end else if JustifyKW=0 then
          begin
            if JustifySubKW>0 then
              exit;
            JustifySubKW:=KW;
          end else
            exit;
        kwFirst,kwLast:
          if JustifyKW>0 then
            exit
          else if JustifySubKW>0 then
            exit
          else
            JustifySubKW:=KW;
        kwBaseline:
          if JustifyKW>0 then
            exit
          else if not (JustifySubKW in [0,kwFirst,kwLast]) then
            exit
          else
            JustifyKW:=KW;
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  if AlignKW=0 then
  begin
    if AlignSubKW>0 then
      exit;
    case JustifyKW of
    kwLeft: AlignKW:=kwStart;
    kwRight: AlignKW:=kwEnd;
    kwBaseline:
      if JustifySubKW=kwLast then
        AlignKW:=kwEnd
      else
        AlignKW:=kwStart;
    else
      exit;
    end;
  end;
  if JustifyKW=0 then
  begin
    if JustifySubKW>0 then
      exit;
    JustifyKW:=AlignKW;
    JustifySubKW:=AlignSubKW;
  end;

  Result:=true;
end;

function TFresnelCSSRegistry.ReadPlaceItems(Resolver: TCSSBaseResolver; Check: boolean; out
  AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  AlignKW:=0;
  AlignSubKW:=0;
  JustifyKW:=0;
  JustifySubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwNormal, kwStretch, kwAnchorCenter:
          if AlignKW=0 then
          begin
            if AlignSubKW>0 then
              exit;
            AlignKW:=KW;
          end else if JustifyKW=0 then
          begin
            if JustifySubKW>0 then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwCenter,
        kwSpaceAround,kwSpaceBetween,kwSpaceEvenly:
          if AlignKW=0 then
          begin
            if not (AlignSubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            AlignKW:=KW;
          end else if JustifyKW=0 then
          begin
            if not (JustifySubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwLeft,kwRight,kwLegacy:
          if JustifyKW=0 then
          begin
            if not (JustifySubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwSafe,kwUnsafe:
          if AlignKW=0 then
          begin
            if AlignSubKW>0 then
              exit;
            AlignSubKW:=KW;
          end else if JustifyKW=0 then
          begin
            if JustifySubKW>0 then
              exit;
            JustifySubKW:=KW;
          end else
            exit;
        kwFirst,kwLast:
          if JustifyKW>0 then
            exit
          else if JustifySubKW>0 then
            exit
          else
            JustifySubKW:=KW;
        kwBaseline:
          if JustifyKW>0 then
            exit
          else if not (JustifySubKW in [0,kwFirst,kwLast]) then
            exit
          else
            JustifyKW:=KW;
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  if AlignKW=0 then
  begin
    if AlignSubKW>0 then
      exit;
    case JustifyKW of
    kwLeft: AlignKW:=kwStart;
    kwRight: AlignKW:=kwEnd;
    kwBaseline:
      if JustifySubKW=kwLast then
        AlignKW:=kwEnd
      else
        AlignKW:=kwStart;
    else
      exit;
    end;
  end;
  if JustifyKW=0 then
  begin
    if JustifySubKW>0 then
      exit;
    JustifyKW:=AlignKW;
    JustifySubKW:=AlignSubKW;
  end;

  Result:=true;
end;

function TFresnelCSSRegistry.ReadPlaceSelf(Resolver: TCSSBaseResolver; Check: boolean; out AlignKW,
  AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  AlignKW:=0;
  AlignSubKW:=0;
  JustifyKW:=0;
  JustifySubKW:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        case KW of
        kwAuto, kwNormal, kwStretch, kwAnchorCenter:
          if AlignKW=0 then
          begin
            if AlignSubKW>0 then
              exit;
            AlignKW:=KW;
          end else if JustifyKW=0 then
          begin
            if JustifySubKW>0 then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwStart,kwEnd,
        kwFlexStart,kwFlexEnd,
        kwSelfStart,kwSelfEnd,
        kwCenter,
        kwSpaceAround,kwSpaceBetween,kwSpaceEvenly:
          if AlignKW=0 then
          begin
            if not (AlignSubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            AlignKW:=KW;
          end else if JustifyKW=0 then
          begin
            if not (JustifySubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwLeft,kwRight:
          if JustifyKW=0 then
          begin
            if not (JustifySubKW in [0,kwSafe,kwUnsafe]) then
              exit;
            JustifyKW:=KW;
          end else
            exit;
        kwSafe,kwUnsafe:
          if AlignKW=0 then
          begin
            if AlignSubKW>0 then
              exit;
            AlignSubKW:=KW;
          end else if JustifyKW=0 then
          begin
            if JustifySubKW>0 then
              exit;
            JustifySubKW:=KW;
          end else
            exit;
        kwFirst,kwLast:
          if JustifyKW>0 then
            exit
          else if JustifySubKW>0 then
            exit
          else
            JustifySubKW:=KW;
        kwBaseline:
          if JustifyKW>0 then
            exit
          else if not (JustifySubKW in [0,kwFirst,kwLast]) then
            exit
          else
            JustifyKW:=KW;
        else
          exit;
        end;
      end;
    rvkFunction:
      if Resolver.CurComp.FunctionID=afVar then
        exit(Check)
      else
        exit;
    else
      exit;
    end;
  until not Resolver.ReadNext;
  if Resolver.CurComp.Kind=rvkInvalid then exit;

  if AlignKW=0 then
  begin
    if AlignSubKW>0 then
      exit;
    case JustifyKW of
    kwLeft: AlignKW:=kwStart;
    kwRight: AlignKW:=kwEnd;
    kwBaseline:
      if JustifySubKW=kwLast then
        AlignKW:=kwEnd
      else
        AlignKW:=kwStart;
    else
      exit;
    end;
  end;
  if JustifyKW=0 then
  begin
    if JustifySubKW>0 then
      exit;
    JustifyKW:=AlignKW;
    JustifySubKW:=AlignSubKW;
  end;

  Result:=true;
end;

procedure TFresnelCSSRegistry.SplitOverflow(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  X, Y: TCSSString;
  KW: TCSSNumericalID;
begin
  X:='';
  Y:='';
  repeat
    if Resolver.CurComp.Kind<>rvkKeyword then
      exit; // garbage
    KW:=Resolver.CurComp.KeywordID;
    if Resolver.IsKeywordIn(KW,Chk_OverflowXY_KeywordIDs) then
    begin
      if X='' then
        X:=Keywords[KW]
      else if Y='' then
        Y:=Keywords[KW]
      else
        exit; // garbage
    end;
  until not Resolver.ReadNext;
  if X='' then
    exit;
  if Y='' then
    Y:=X;
  SetLength(AttrIDs,2);
  SetLength(Values,2);
  AttrIDs[0]:=FresnelAttrs[fcaOverflowX].Index;
  AttrIDs[1]:=FresnelAttrs[fcaOverflowY].Index;
  Values[0]:=X;
  Values[1]:=Y;
end;

procedure TFresnelCSSRegistry.SplitBorderWidth(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Found: TCSSStringArray;
begin
  Found:=[];
  repeat
    case Resolver.CurComp.Kind of
    rvkFloat:
      if Chk_BorderWidth_Dim.Fits(Resolver.CurComp) then
        Insert(Resolver.GetCompString,Found,length(Found));
    rvkKeyword:
      if Resolver.IsKeywordIn(Resolver.CurComp.KeywordID,Chk_BorderWidth_Dim.AllowedKeywordIDs) then
        Insert(Keywords[Resolver.CurComp.KeywordID],Found,length(Found));
    rvkFunction:
      Insert(Resolver.GetCompString,Found,length(Found));
    end;
    if length(Found)=4 then break;
  until not Resolver.ReadNext;

  SplitLonghandSides(AttrIDs,Values,
    fcaBorderTopWidth,fcaBorderRightWidth,fcaBorderBottomWidth,fcaBorderLeftWidth,Found);
end;

procedure TFresnelCSSRegistry.SplitFlex(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Grow, Shrink, Basis: String;
begin
  if not ReadFlex(Resolver,false,Grow,Shrink,Basis) then exit;

  SetLength(AttrIDs,3);
  AttrIDs[0]:=FresnelAttrs[fcaFlexGrow].Index;
  AttrIDs[1]:=FresnelAttrs[fcaFlexShrink].Index;
  AttrIDs[2]:=FresnelAttrs[fcaFlexBasis].Index;
  SetLength(Values,3);
  Values[0]:=Grow;
  Values[1]:=Shrink;
  Values[2]:=Basis;
end;

procedure TFresnelCSSRegistry.SplitFlexFlow(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Direction, Wrap: String;
begin
  if not ReadFlexFlow(Resolver,false,Direction,Wrap) then exit;
  SetLength(AttrIDs,2);
  AttrIDs[0]:=FresnelAttrs[fcaFlexDirection].Index;
  AttrIDs[1]:=FresnelAttrs[fcaFlexWrap].Index;
  SetLength(Values,2);
  Values[0]:=Direction;
  Values[1]:=Wrap;
end;

procedure TFresnelCSSRegistry.SplitBorderColor(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  i: Integer;
  Found: TCSSStringArray;
  KW: TCSSNumericalID;
begin
  Found:=[];
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      begin
        KW:=Resolver.CurComp.KeywordID;
        if (KW>=kwFirstColor) and (KW<=kwLastColor) then
          Insert(Keywords[KW],Found,length(Found))
        else
          for i:=0 to length(Chk_Color_KeywordIDs)-1 do
            if KW=Chk_Color_KeywordIDs[i] then
              Insert(Keywords[KW],Found,length(Found));
      end;
    rvkFunction:
      Insert(Resolver.GetCompString,Found,length(Found));
    rvkHexColor:
      Insert(Resolver.GetCompString,Found,length(Found));
    end;
    if length(Found)=4 then break;
  until not Resolver.ReadNext;

  SplitLonghandSides(AttrIDs,Values,
    fcaBorderTopColor,fcaBorderRightColor,fcaBorderBottomColor,fcaBorderLeftColor,Found);
end;

procedure TFresnelCSSRegistry.SplitBorderStyle(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Found: TCSSStringArray;
begin
  Found:=[];
  repeat
    case Resolver.CurComp.Kind of
    rvkKeyword:
      if Resolver.IsKeywordIn(Resolver.CurComp.KeywordID,Chk_BorderStyle_KeywordIDs) then
        Insert(Keywords[Resolver.CurComp.KeywordID],Found,length(Found));
    rvkFunction:
      Insert(Resolver.GetCompString,Found,length(Found));
    end;
    if length(Found)=4 then break;
  until not Resolver.ReadNext;

  SplitLonghandSides(AttrIDs,Values,
    fcaBorderTopStyle,fcaBorderRightStyle,fcaBorderBottomStyle,fcaBorderLeftStyle,Found);
end;

procedure TFresnelCSSRegistry.SplitBorderLeftRightTopBottom(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  i, j: Integer;
  aColor, aStyle, aWidth: TCSSString;
  WidthAttr, StyleAttr, ColorAttr: TFresnelCSSAttribute;
  KW: TCSSNumericalID;
begin
  aColor:='';
  aStyle:='';
  aWidth:='';
  repeat
    if IsColor(Resolver.CurComp) then
    begin
      if aColor='' then
        aColor:=Resolver.GetCompString;
    end else begin
      case Resolver.CurComp.Kind of
      rvkFloat:
        if (aWidth='') and Chk_BorderWidth_Dim.Fits(Resolver.CurComp) then
          aWidth:=Resolver.GetCompString;
      rvkKeyword:
        begin
          KW:=Resolver.CurComp.KeywordID;
          if aStyle='' then
            if Resolver.IsKeywordIn(KW,Chk_BorderStyle_KeywordIDs) then
              aStyle:=Keywords[KW];
          if aWidth='' then
            if Resolver.IsKeywordIn(KW,Chk_BorderWidth_Dim.AllowedKeywordIDs) then
              aWidth:=Keywords[KW];
        end;
      rvkFunction:
        if aWidth='' then
          aWidth:=Resolver.GetCompString;
      end;
    end;
  until not Resolver.ReadNext;

  case TFresnelCSSAttrDesc(Resolver.CurDesc).Attr of
  fcaBorderTop: i:=0;
  fcaBorderRight: i:=1;
  fcaBorderBottom: i:=2;
  fcaBorderLeft: i:=3;
  fcaBorder:
    begin
      SetLength(AttrIDs,12);
      SetLength(Values,12);
      j:=0;
      for i:=0 to 3 do
      begin
        WidthAttr:=TFresnelCSSAttribute(ord(fcaBorderTopWidth)+i);
        StyleAttr:=TFresnelCSSAttribute(ord(fcaBorderTopStyle)+i);
        ColorAttr:=TFresnelCSSAttribute(ord(fcaBorderTopColor)+i);

        j:=i*3;
        AttrIDs[j]:=FresnelAttrs[WidthAttr].Index;
        AttrIDs[j+1]:=FresnelAttrs[StyleAttr].Index;
        AttrIDs[j+2]:=FresnelAttrs[ColorAttr].Index;

        Values[j]:=aWidth;
        Values[j+1]:=aStyle;
        Values[j+2]:=aColor;
      end;
      exit;
    end
  else raise EFresnel.Create('20240713111308');
  end;
  SetLength(AttrIDs,3);
  SetLength(Values,3);

  WidthAttr:=TFresnelCSSAttribute(ord(fcaBorderTopWidth)+i);
  StyleAttr:=TFresnelCSSAttribute(ord(fcaBorderTopStyle)+i);
  ColorAttr:=TFresnelCSSAttribute(ord(fcaBorderTopColor)+i);

  AttrIDs[0]:=FresnelAttrs[WidthAttr].Index;
  AttrIDs[1]:=FresnelAttrs[StyleAttr].Index;
  AttrIDs[2]:=FresnelAttrs[ColorAttr].Index;
  Values[0]:=aWidth;
  Values[1]:=aStyle;
  Values[2]:=aColor;
end;

procedure TFresnelCSSRegistry.SplitBorderRadius(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
// shorthand for all four corners, order matters
// 1..4 lengths, e.g. 10px 20px 30px 40px
// Plus optional separated by / 1..4 lengths for vertical radius
// e.g. 10px / 20px 30px
// border-radius: 10px; -> set for all 4 sides
// border-radius: 10px 5%; -> top-left-and-bottom-right top-right-and-bottom-left
// border-radius: 2px 4px 2px; -> top-left top-right-and-bottom-left bottom-right
// border-radius: 1px 0 3px 4px; -> top-left top-right bottom-right bottom-left
var
  Radii: array[0..1, 0..3] of TCSSString;
  Corner: array[0..1] of integer;
  Coord, i: Integer;

  procedure Fill;
  begin
    if Radii[Coord,1]='' then
      Radii[Coord,1]:=Radii[Coord,0];
    if Radii[Coord,2]='' then
      Radii[Coord,2]:=Radii[Coord,1];
    if Radii[Coord,3]='' then
      Radii[Coord,3]:=Radii[Coord,0];
  end;

begin
  Coord:=0;
  Corner[0]:=0;
  Corner[1]:=0;
  repeat
    case Resolver.CurComp.Kind of
    rvkFloat:
      if (Corner[Coord]<4)
          and Chk_BorderRadius_Dim.Fits(Resolver.CurComp)
      then begin
        Radii[Coord,Corner[Coord]]:=Resolver.GetCompString;
        inc(Corner[Coord]);
      end;
    rvkFunction:
      if (Corner[Coord]<4) then
      begin
        Radii[Coord,Corner[Coord]]:=Resolver.GetCompString;
        inc(Corner[Coord]);
      end;
    rvkSymbol:
      if Resolver.CurComp.Symbol=ctkDIV then
      begin
        if Coord>0 then break;
        if Corner[0]=0 then
          exit;
        Fill;
        Coord:=1;
      end;
    end;
  until not Resolver.ReadNext;

  if Corner[0]=0 then
    exit;
  Fill;

  SetLength(AttrIDs,4);
  SetLength(Values,4);
  for i:=0 to 3 do
    AttrIDs[i]:=FresnelAttrs[TFresnelCSSAttribute(ord(fcaBorderTopLeftRadius)+i)].Index;

  if Corner[1]=0 then
  begin
    for i:=0 to 3 do
      Values[i]:=Radii[0,i];
  end else begin
    for i:=0 to 3 do
      Values[i]:=Radii[0,i]+' / '+Radii[1,i];
  end;
end;

procedure TFresnelCSSRegistry.SplitFont(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  aSystemFont: TCSSNumericalID;
  aFamily, aStyle, aVariantCaps, aWeight, aWidth, aSize, aLineHeight: TCSSString;
begin
  if not ReadFont(Resolver,false, aSystemFont, aFamily, aStyle, aWeight, aWidth, aSize,
    aLineHeight, aVariantCaps) then exit;

  if aSystemFont>0 then
  begin
    // todo: systemfonts
  end else begin
    SetLength(AttrIDs,13);
    SetLength(Values,13);
    AttrIDs[0]:=FresnelAttrs[fcaFontFamily].Index;
    Values[0]:=aFamily;
    AttrIDs[1]:=FresnelAttrs[fcaFontStyle].Index;
    Values[1]:=aStyle;
    AttrIDs[2]:=FresnelAttrs[fcaFontWeight].Index;
    Values[2]:=aWeight;
    AttrIDs[3]:=FresnelAttrs[fcaFontWidth].Index;
    Values[3]:=aWidth;
    AttrIDs[4]:=FresnelAttrs[fcaFontSize].Index;
    Values[4]:=aSize;
    AttrIDs[5]:=FresnelAttrs[fcaLineHeight].Index;
    Values[5]:=aLineHeight;
    // variant
    AttrIDs[6]:=FresnelAttrs[fcaFontVariantAlternates].Index;
    Values[6]:='normal';
    AttrIDs[7]:=FresnelAttrs[fcaFontVariantCaps].Index;
    Values[7]:=aVariantCaps;
    AttrIDs[8]:=FresnelAttrs[fcaFontVariantEastAsian].Index;
    Values[8]:='normal';
    AttrIDs[9]:=FresnelAttrs[fcaFontVariantEmoji].Index;
    Values[9]:='normal';
    AttrIDs[10]:=FresnelAttrs[fcaFontVariantLigatures].Index;
    Values[10]:='normal';
    AttrIDs[11]:=FresnelAttrs[fcaFontVariantNumeric].Index;
    Values[11]:='normal';
    AttrIDs[12]:=FresnelAttrs[fcaFontVariantPosition].Index;
    Values[12]:='normal';
  end;
end;

procedure TFresnelCSSRegistry.SplitFontStretch(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
begin
  SetLength(AttrIDs,1);
  AttrIDs[0]:=FresnelAttrs[fcaFontWidth].Index;
  SetLength(Values,1);
  Values[0]:=Resolver.CurValue;
end;

procedure TFresnelCSSRegistry.SplitFontVariant(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  aAlternates: TCSSString;
  aCaps, aEmoji, aPosition: TCSSNumericalID;
  aEastAsian, aLigatures, aNumeric: TCSSNumericalIDArray;
begin
  if not ReadFontVariant(Resolver,false,aAlternates,aCaps,aEastAsian,aEmoji,aLigatures,
    aNumeric,aPosition) then exit;
  SetLength(AttrIDs,7);
  AttrIDs[0]:=FresnelAttrs[fcaFontVariantAlternates].Index;
  AttrIDs[1]:=FresnelAttrs[fcaFontVariantCaps].Index;
  AttrIDs[2]:=FresnelAttrs[fcaFontVariantEastAsian].Index;
  AttrIDs[3]:=FresnelAttrs[fcaFontVariantEmoji].Index;
  AttrIDs[4]:=FresnelAttrs[fcaFontVariantLigatures].Index;
  AttrIDs[5]:=FresnelAttrs[fcaFontVariantNumeric].Index;
  AttrIDs[6]:=FresnelAttrs[fcaFontVariantPosition].Index;
  SetLength(Values,7);
  Values[0]:=aAlternates;
  Values[1]:=Keywords[aCaps];
  Values[2]:=CombineKeywords(aEastAsian);
  Values[3]:=Keywords[aEmoji];
  Values[4]:=CombineKeywords(aLigatures);
  Values[5]:=CombineKeywords(aNumeric);
  Values[6]:=Keywords[aPosition];
end;

procedure TFresnelCSSRegistry.SplitGap(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Column, Row: TCSSString;
begin
  if not ReadGap(Resolver,false,Column,Row) then exit;
  SetLength(AttrIDs,2);
  AttrIDs[0]:=FresnelAttrs[fcaColumnGap].Index;
  AttrIDs[1]:=FresnelAttrs[fcaRowGap].Index;
  SetLength(Values,2);
  Values[0]:=Column;
  Values[1]:=Row;
end;

procedure TFresnelCSSRegistry.SplitMargin(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Found: TCSSStringArray;
begin
  Found:=[];
  repeat
    if Chk_Margin_Dim.Fits(Resolver.CurComp) or (Resolver.CurComp.Kind=rvkFunction) then
      Insert(Resolver.GetCompString,Found,length(Found));
    if length(Found)=4 then break;
  until not Resolver.ReadNext;

  SplitLonghandSides(AttrIDs,Values,
    fcaMarginTop,fcaMarginRight,fcaMarginBottom,fcaMarginLeft,Found);
end;

procedure TFresnelCSSRegistry.SplitMarginBlock(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Found: TCSSStringArray;
begin
  Found:=[];
  repeat
    if Chk_Margin_Dim.Fits(Resolver.CurComp) or (Resolver.CurComp.Kind=rvkFunction) then
      Insert(Resolver.GetCompString,Found,length(Found));
    if length(Found)=2 then break;
  until not Resolver.ReadNext;

  if length(Found)=0 then exit;

  SetLength(AttrIDs,2);
  SetLength(Values,2);
  AttrIDs[0]:=FresnelAttrs[fcaMarginBlockStart].Index;
  AttrIDs[1]:=FresnelAttrs[fcaMarginBlockEnd].Index;
  Values[0]:=Found[0];
  if length(Found)=2 then
    Values[1]:=Found[1]
  else
    Values[1]:=Found[0];
end;

procedure TFresnelCSSRegistry.SplitMarginInline(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Found: TCSSStringArray;
begin
  Found:=[];
  repeat
    if Chk_Margin_Dim.Fits(Resolver.CurComp) or (Resolver.CurComp.Kind=rvkFunction) then
      Insert(Resolver.GetCompString,Found,length(Found));
    if length(Found)=2 then break;
  until not Resolver.ReadNext;

  if length(Found)=0 then exit;

  SetLength(AttrIDs,2);
  SetLength(Values,2);
  AttrIDs[0]:=FresnelAttrs[fcaMarginInlineStart].Index;
  AttrIDs[1]:=FresnelAttrs[fcaMarginInlineEnd].Index;
  Values[0]:=Found[0];
  if length(Found)=2 then
    Values[1]:=Found[1]
  else
    Values[1]:=Found[0];
end;

procedure TFresnelCSSRegistry.SplitPadding(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  Found: TCSSStringArray;
begin
  Found:=[];
  repeat
    if Chk_Padding_Dim.Fits(Resolver.CurComp) or (Resolver.CurComp.Kind=rvkFunction) then
      Insert(Resolver.GetCompString,Found,length(Found));
    if length(Found)=4 then break;
  until not Resolver.ReadNext;

  SplitLonghandSides(AttrIDs,Values,
    fcaPaddingTop,fcaPaddingRight,fcaPaddingBottom,fcaPaddingLeft,Found);
end;

procedure TFresnelCSSRegistry.SplitPlaceContent(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID;
begin
  if not ReadPlaceContent(Resolver,false,AlignKW, AlignSubKW, JustifyKW, JustifySubKW) then exit;
  SetLength(AttrIDs,2);
  AttrIDs[0]:=FresnelAttrs[fcaAlignContent].Index;
  AttrIDs[1]:=FresnelAttrs[fcaJustifyContent].Index;
  SetLength(Values,2);
  Values[0]:=CombinePlace(AlignSubKW,AlignKW);
  Values[1]:=CombinePlace(JustifySubKW,JustifyKW);
end;

procedure TFresnelCSSRegistry.SplitPlaceItems(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID;
begin
  if not ReadPlaceItems(Resolver,false,AlignKW, AlignSubKW, JustifyKW, JustifySubKW) then exit;
  SetLength(AttrIDs,2);
  AttrIDs[0]:=FresnelAttrs[fcaAlignItems].Index;
  AttrIDs[1]:=FresnelAttrs[fcaJustifyItems].Index;
  SetLength(Values,2);
  Values[0]:=CombinePlace(AlignSubKW,AlignKW);
  Values[1]:=CombinePlace(JustifySubKW,JustifyKW);
end;

procedure TFresnelCSSRegistry.SplitPlaceSelf(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  AlignKW, AlignSubKW, JustifyKW, JustifySubKW: TCSSNumericalID;
begin
  if not ReadPlaceSelf(Resolver,false,AlignKW, AlignSubKW, JustifyKW, JustifySubKW) then exit;
  SetLength(AttrIDs,2);
  AttrIDs[0]:=FresnelAttrs[fcaAlignSelf].Index;
  AttrIDs[1]:=FresnelAttrs[fcaJustifySelf].Index;
  SetLength(Values,2);
  Values[0]:=CombinePlace(AlignSubKW,AlignKW);
  Values[1]:=CombinePlace(JustifySubKW,JustifyKW);
end;

function TFresnelCSSRegistry.GetBackground(El: TFresnelElement): string;
begin
  Result:='';
  // todo
  if El=nil then ;
end;

function TFresnelCSSRegistry.GetBackgroundPosition(El: TFresnelElement): string;
var
  X, Y: String;
begin
  X:=El.GetComputedString(fcaBackgroundPositionX);
  Y:=El.GetComputedString(fcaBackgroundPositionY);
  if X>'' then
  begin
    Result:=X;
    if Y>'' then
      Result+=' '+Y;
  end else
    Result:=Y;
end;

function TFresnelCSSRegistry.GetBorder(El: TFresnelElement): string;
var
  aLeft, aRight, aTop, aBottom: String;
begin
  aLeft:=GetBorderSide(El,ffsLeft);
  aRight:=GetBorderSide(El,ffsRight);
  aTop:=GetBorderSide(El,ffsTop);
  aBottom:=GetBorderSide(El,ffsBottom);

  if (aLeft=aRight) and (aLeft=aTop) and (aLeft=aBottom) then
    Result:=aLeft
  else
    Result:='';
end;

function TFresnelCSSRegistry.GetBorderColor(El: TFresnelElement): string;
var
  aLeft, aRight, aTop, aBottom: String;
begin
  aTop:=El.GetComputedString(fcaBorderTopColor);
  aRight:=El.GetComputedString(fcaBorderRightColor);
  aBottom:=El.GetComputedString(fcaBorderBottomColor);
  aLeft:=El.GetComputedString(fcaBorderLeftColor);

  if aTop='' then aTop:='currentcolor';
  if aRight='' then aRight:='currentcolor';
  if aBottom='' then aBottom:='currentcolor';
  if aLeft='' then aLeft:='currentcolor';

  Result:=aTop+' '+aRight+' '+aBottom+' '+aLeft;
end;

function TFresnelCSSRegistry.GetBorderSide(El: TFresnelElement; Side: TFresnelCSSSide): string;
var
  aColor, aStyle, aWidth: String;
begin
  aColor:=El.GetComputedString(TFresnelCSSAttribute(ord(fcaBorderTopColor)+ord(Side)));
  if aColor='' then aColor:='currentcolor';
  aStyle:=El.GetComputedString(TFresnelCSSAttribute(ord(fcaBorderTopStyle)+ord(Side)));
  if aStyle='' then aStyle:='none';
  aWidth:=El.GetComputedString(TFresnelCSSAttribute(ord(fcaBorderTopWidth)+ord(Side)));
  if aWidth='' then aWidth:='0';

  Result:=aColor+' '+aStyle+' '+aWidth;
end;

function TFresnelCSSRegistry.GetBorderLeft(El: TFresnelElement): string;
begin
  Result:=GetBorderSide(El,ffsLeft);
end;

function TFresnelCSSRegistry.GetBorderRight(El: TFresnelElement): string;
begin
  Result:=GetBorderSide(El,ffsRight);
end;

function TFresnelCSSRegistry.GetBorderTop(El: TFresnelElement): string;
begin
  Result:=GetBorderSide(El,ffsTop);
end;

function TFresnelCSSRegistry.GetBorderBottom(El: TFresnelElement): string;
begin
  Result:=GetBorderSide(El,ffsBottom);
end;

function TFresnelCSSRegistry.GetBorderWidth(El: TFresnelElement): string;
begin
  Result:=GetSideLengths(El,fcaBorderTopWidth);
end;

function TFresnelCSSRegistry.GetSideLengths(El: TFresnelElement; LeftAttr: TFresnelCSSAttribute
  ): string;
var
  Sides: array[TFresnelCSSSide] of TFresnelLength;
  Side: TFresnelCSSSide;
begin
  for Side in TFresnelCSSSide do
    Sides[Side]:=El.GetComputedLength(TFresnelCSSAttribute(ord(LeftAttr)+ord(Side)));

  Result:=FloatToCSSPx(Sides[ffsTop])
     +' '+FloatToCSSPx(Sides[ffsRight])
     +' '+FloatToCSSPx(Sides[ffsBottom])
     +' '+FloatToCSSPx(Sides[ffsLeft]);
end;

function TFresnelCSSRegistry.GetFlex(El: TFresnelElement): string;
var
  Grow, Shrink, Basis: String;
begin
  Basis:=El.GetComputedString(fcaFlexShrink);
  Grow:=El.GetComputedString(fcaFlexGrow);
  Shrink:=El.GetComputedString(fcaFlexShrink);
  Result:=Basis+' '+Grow+' '+Shrink;
end;

function TFresnelCSSRegistry.GetFlexFlow(El: TFresnelElement): string;
var
  Direction, Wrap: String;
begin
  Direction:=El.GetComputedString(fcaFlexDirection);
  Wrap:=El.GetComputedString(fcaFlexWrap);
  Result:=Direction+' '+Wrap;
end;

function TFresnelCSSRegistry.GetFont(El: TFresnelElement): string;

  procedure Add(const aValue, aDefault: string);
  begin
    if aValue='' then exit;
    if aValue=aDefault then exit;

    if Result>'' then
      Result+=' '+aValue
    else
      Result:=aValue;
  end;

var
  aSize, aWeight, aLineHeight: TFresnelLength;
  aFamily, aStyle: String;
  aWidth: TCSSNumericalID;
  aCaps: TFresnelCSSFontVarCaps;
begin
  Result:='';

  aSize:=El.Font.GetSize;
  aFamily:=El.Font.GetFamily;
  aStyle:=El.Font.GetStyle;
  aCaps:=El.Font.GetCaps;
  aWeight:=El.Font.GetWeight;
  aLineHeight:=El.GetComputedLength(fcaLineHeight);

  // style, variant and weight must precede size
  Add(aStyle,FresnelAttrs[fcaFontStyle].InitialValue);
  // for variant only normal and small-caps are allowed
  if aCaps=ffvcSmallCaps then
    Add(FresnelCSSFontVarCapsNames[aCaps],'');
  Add(FloatToCSSStr(aWeight),'');

  // font-size/line-height
  Add(FloatToCSSPx(aSize),'');
  Result+='/'+FloatToCSSPx(aLineHeight);

  aWidth:=RoundFontWidthToKeyword(El.Font.GetWidth);
  if aWidth<>kwNormal then
    Result+=' '+Keywords[aWidth];

  Add(aFamily,'');
end;

function TFresnelCSSRegistry.GetFontFamily(El: TFresnelElement): string;
begin
  Result:=El.Font.GetFamily;
end;

function TFresnelCSSRegistry.GetFontKerning(El: TFresnelElement): string;
begin
  Result:=FresnelCSSKerningNames[El.Font.GetKerning];
end;

function TFresnelCSSRegistry.GetFontSize(El: TFresnelElement): string;
begin
  Result:=FloatToCSSPx(El.Font.GetSize);
end;

function TFresnelCSSRegistry.GetFontStyle(El: TFresnelElement): string;
begin
  Result:=El.Font.GetStyle;
end;

function TFresnelCSSRegistry.GetFontVariant(El: TFresnelElement): string;

  procedure Add(const s: string);
  begin
    if Result>'' then Result+=' ';
    Result+=s;
  end;

var
  aAlternates: String;
  aCaps: TFresnelCSSFontVarCaps;
  aEastAsian: TFresnelCSSFontVarEastAsians;
  aEmoji: TFresnelCSSFontVarEmoji;
  aLigatures: TFresnelCSSFontVarLigaturesSet;
  aNumerics: TFresnelCSSFontVarNumerics;
  aPosition: TFresnelCSSFontVarPosition;
  IsAltNormal, IsCapsNormal, IsEastAsianNormal, IsEmojiNormal, IsLigNormal, IsNumsNormal,
    IsPosNormal: Boolean;
begin
  Result:='';
  aAlternates:=El.Font.GetAlternates;
  aCaps:=El.Font.GetCaps;
  aEastAsian:=El.Font.GetEastAsians;
  aEmoji:=El.Font.GetEmoji;
  aLigatures:=El.Font.GetLigatures;
  aNumerics:=El.Font.GetNumerics;
  aPosition:=El.Font.GetPosition;

  IsAltNormal:=(aAlternates='') or (aAlternates='normal');
  IsCapsNormal:=(aCaps=ffvcNormal);
  IsEastAsianNormal:=(aEastAsian=[]) or (aEastAsian=[ffveaNormal]);
  IsEmojiNormal:=(aEmoji=ffveNormal);
  IsLigNormal:=(aLigatures=[]) or (aLigatures=[ffvlNormal]);
  IsNumsNormal:=(aNumerics=[]) or (aNumerics=[ffvnNormal]);
  IsPosNormal:=(aPosition=ffvpNormal);

  if not IsAltNormal then
    Add(aAlternates);
  if not IsCapsNormal then
    Add(FresnelCSSFontVarCapsNames[aCaps]);
  if not IsEastAsianNormal then
    Add(FontVariantEastAsiansToStr(aEastAsian));
  if not IsEmojiNormal then
    Add(FresnelCSSFontVarEmojiNames[aEmoji]);
  if not IsLigNormal then
    Add(FontVariantLigaturesToStr(aLigatures));
  if not IsNumsNormal then
    Add(FontVariantNumericsToStr(aNumerics));
  if not IsPosNormal then
    Add(FresnelCSSFontVarPositionNames[aPosition]);
  if Result='' then
    Result:='normal';
end;

function TFresnelCSSRegistry.GetFontVariantAlternates(El: TFresnelElement): string;
begin
  Result:=El.Font.GetAlternates;
end;

function TFresnelCSSRegistry.GetFontVariantCaps(El: TFresnelElement): string;
begin
  Result:=FresnelCSSFontVarCapsNames[El.Font.GetCaps];
end;

function TFresnelCSSRegistry.GetFontVariantEastAsian(El: TFresnelElement): string;
begin
  Result:=FontVariantEastAsiansToStr(El.Font.GetEastAsians);
end;

function TFresnelCSSRegistry.GetFontVariantEmoji(El: TFresnelElement): string;
begin
  Result:=FresnelCSSFontVarEmojiNames[El.Font.GetEmoji];
end;

function TFresnelCSSRegistry.GetFontVariantLigatures(El: TFresnelElement): string;
begin
  Result:=FontVariantLigaturesToStr(El.Font.GetLigatures);
end;

function TFresnelCSSRegistry.GetFontVariantNumeric(El: TFresnelElement): string;
begin
  Result:=FontVariantNumericsToStr(El.Font.GetNumerics);
end;

function TFresnelCSSRegistry.GetFontVariantPosition(El: TFresnelElement): string;
begin
  Result:=FresnelCSSFontVarPositionNames[El.Font.GetPosition];
end;

function TFresnelCSSRegistry.GetFontWidth(El: TFresnelElement): string;
begin
  Result:=FloatToCSSStr(El.Font.GetWidth*100)+'%';
end;

function TFresnelCSSRegistry.GetFontWeight(El: TFresnelElement): string;
begin
  Result:=FloatToCSSStr(El.Font.GetWeight);
end;

function TFresnelCSSRegistry.GetGap(El: TFresnelElement): string;
var
  Column, Row: String;
begin
  Column:=El.GetComputedString(fcaColumnGap);
  Row:=El.GetComputedString(fcaRowGap);
  Result:=Column+' '+Row;
end;

function TFresnelCSSRegistry.GetMargin(El: TFresnelElement): string;
var
  Margins: array[TFresnelCSSSide] of TFresnelLength;
  Side: TFresnelCSSSide;
begin
  for Side in TFresnelCSSSide do
    Margins[Side]:=El.GetComputedLength(TFresnelCSSAttribute(ord(fcaMarginTop)+ord(Side)));

  Result:=FloatToCSSPx(Margins[ffsTop])+' '+FloatToCSSPx(Margins[ffsRight])
     +' '+FloatToCSSPx(Margins[ffsBottom])+' '+FloatToCSSPx(Margins[ffsLeft]);
end;

function TFresnelCSSRegistry.GetMarginBlock(El: TFresnelElement): string;
var
  aStartAttr, aEndAttr: TFresnelCSSAttribute;
  aStart, aEnd: TFresnelLength;
begin
  El.GetComputedMarginBlockStartEndAttr(aStartAttr,aEndAttr);
  aStart:=El.GetComputedLength(aStartAttr);
  aEnd:=El.GetComputedLength(aEndAttr);
  Result:=FloatToCSSPx(aStart)+' '+FloatToCSSPx(aEnd);
end;

function TFresnelCSSRegistry.GetMarginInline(El: TFresnelElement): string;
var
  aStartAttr, aEndAttr: TFresnelCSSAttribute;
  aStart, aEnd: TFresnelLength;
begin
  El.GetComputedMarginInlineStartEndAttr(aStartAttr,aEndAttr);
  aStart:=El.GetComputedLength(aStartAttr);
  aEnd:=El.GetComputedLength(aEndAttr);
  Result:=FloatToCSSPx(aStart)+' '+FloatToCSSPx(aEnd);
end;

function TFresnelCSSRegistry.GetBorderStyle(El: TFresnelElement): string;
var
  aLeft, aRight, aTop, aBottom: String;
begin
  aTop:=El.GetComputedString(fcaBorderTopStyle);
  aRight:=El.GetComputedString(fcaBorderRightStyle);
  aBottom:=El.GetComputedString(fcaBorderBottomStyle);
  aLeft:=El.GetComputedString(fcaBorderLeftStyle);

  if aTop='' then aTop:='none';
  if aRight='' then aRight:='none';
  if aBottom='' then aBottom:='none';
  if aLeft='' then aLeft:='none';

  Result:=aTop+' '+aRight+' '+aBottom+' '+aLeft;
end;

function TFresnelCSSRegistry.GetBorderRadius(El: TFresnelElement): string;
var
  aCorner: TFresnelCSSCorner;
  Radii: array[TFresnelCSSCorner] of TFresnelPoint;
begin
  for aCorner in TFresnelCSSCorner do
    Radii[aCorner]:=El.GetComputedBorderRadius(aCorner);

  Result:=FloatToCSSPx(Radii[fcsTopLeft].X)
     +' '+FloatToCSSPx(Radii[fcsTopRight].X)
     +' '+FloatToCSSPx(Radii[fcsBottomRight].X)
     +' '+FloatToCSSPx(Radii[fcsBottomLeft].X)
     +' / '+FloatToCSSPx(Radii[fcsTopLeft].Y)
     +' '+FloatToCSSPx(Radii[fcsTopRight].Y)
     +' '+FloatToCSSPx(Radii[fcsBottomRight].Y)
     +' '+FloatToCSSPx(Radii[fcsBottomLeft].Y);
end;

procedure TFresnelCSSRegistry.SplitBackgroundPosition(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  X, Y: TCSSString;
begin
  if not ReadOneBackgroundPosition(Resolver,false,X,Y) then
    exit;

  if X='' then X:='0%';
  if Y='' then Y:='0%';

  SetLength(AttrIDs,2);
  SetLength(Values,2);
  AttrIDs[0]:=FresnelAttrs[fcaBackgroundPositionX].Index;
  AttrIDs[1]:=FresnelAttrs[fcaBackgroundPositionY].Index;
  Values[0]:=X;
  Values[1]:=Y;
end;

procedure TFresnelCSSRegistry.SplitBackground(Resolver: TCSSBaseResolver;
  var AttrIDs: TCSSNumericalIDArray; var Values: TCSSStringArray);
var
  aAttachment, aClip, aColor, aImage, aOrigin, aPositionX, aPositionY, aRepeat, aSize: TCSSString;
begin
  if not ReadBackground(Resolver,false, aAttachment, aClip, aColor, aImage, aOrigin,
                        aPositionX, aPositionY, aRepeat, aSize) then exit;
  SetLength(AttrIDs,9);
  SetLength(Values,9);
  AttrIDs[0]:=FresnelAttrs[fcaBackgroundAttachment].Index;
  Values[0]:=aAttachment;
  AttrIDs[1]:=FresnelAttrs[fcaBackgroundClip].Index;
  Values[1]:=aClip;
  AttrIDs[2]:=FresnelAttrs[fcaBackgroundColor].Index;
  Values[2]:=aColor;
  AttrIDs[3]:=FresnelAttrs[fcaBackgroundImage].Index;
  Values[3]:=aImage;
  AttrIDs[4]:=FresnelAttrs[fcaBackgroundOrigin].Index;
  Values[4]:=aOrigin;
  AttrIDs[5]:=FresnelAttrs[fcaBackgroundPositionX].Index;
  Values[5]:=aPositionX;
  AttrIDs[6]:=FresnelAttrs[fcaBackgroundPositionY].Index;
  Values[6]:=aPositionY;
  AttrIDs[7]:=FresnelAttrs[fcaBackgroundRepeat].Index;
  Values[7]:=aRepeat;
  AttrIDs[8]:=FresnelAttrs[fcaBackgroundSize].Index;
  Values[8]:=aSize;
end;

function TFresnelCSSRegistry.GetOverflow(El: TFresnelElement): string;
var
  X, Y: TCSSNumericalID;
begin
  X:=El.ComputedOverflowX;
  Y:=El.ComputedOverflowY;
  Result:=CSSRegistry.Keywords[X];
  if X<>Y then
    Result:=Result+' '+CSSRegistry.Keywords[Y];
end;

function TFresnelCSSRegistry.GetPadding(El: TFresnelElement): string;
begin
  Result:=GetSideLengths(El,fcaPaddingTop);
end;

function TFresnelCSSRegistry.GetPlaceContent(El: TFresnelElement): string;
var
  anAlign, aJustify: String;
begin
  anAlign:=El.GetComputedString(fcaAlignContent);
  aJustify:=El.GetComputedString(fcaJustifyContent);
  Result:=anAlign+' '+aJustify;
end;

function TFresnelCSSRegistry.GetPlaceItems(El: TFresnelElement): string;
var
  anAlign, aJustify: String;
begin
  anAlign:=El.GetComputedString(fcaAlignItems);
  aJustify:=El.GetComputedString(fcaJustifyItems);
  Result:=anAlign+' '+aJustify;
end;

function TFresnelCSSRegistry.GetPlaceSelf(El: TFresnelElement): string;
var
  anAlign, aJustify: String;
begin
  anAlign:=El.GetComputedString(fcaAlignSelf);
  aJustify:=El.GetComputedString(fcaJustifySelf);
  Result:=anAlign+' '+aJustify;
end;

function TFresnelCSSRegistry.GetLength_ContainerContentWidth(const aComp: TCSSResCompValue;
  El: TFresnelElement; NoChildren: boolean): TFresnelLength;
begin
  if (aComp.Kind=rvkFloat) and (aComp.FloatUnit=cuPercent) then
    Result:=El.GetContainerContentWidth(true)
  else
    Result:=NaN;
  if NoChildren then ;
end;

function TFresnelCSSRegistry.GetLength_ContainerContentHeight(const aComp: TCSSResCompValue;
  El: TFresnelElement; NoChildren: boolean): TFresnelLength;
begin
  if (aComp.Kind=rvkFloat) and (aComp.FloatUnit=cuPercent) then
    Result:=El.GetContainerContentHeight(true)
  else
    Result:=NaN;
  if NoChildren then ;
end;

function TFresnelCSSRegistry.GetLength_BorderWidth(const aComp: TCSSResCompValue;
  El: TFresnelElement; NoChildren: boolean): TFresnelLength;
begin
  case aComp.Kind of
  rvkKeyword:
    case aComp.KeywordID of
    kwThin: exit(0.5);
    kwMedium: exit(1);
    kwThick: exit(2);
    end;
  end;
  Result:=NaN;
  if NoChildren then ;
  if El=nil then ;
end;

function TFresnelCSSRegistry.RoundFontWidth(aWidth: TFresnelLength): TFontWidthName;
var
  fw: TFontWidthName;
begin
  fw:=Low(TFontWidthName);
  for fw:=Low(TFontWidthName) to Pred(high(TFontWidthName)) do
    if aWidth<(FontWidths[fw]+FontWidths[Succ(fw)])/2 then
      exit(fw);
  Result:=high(TFontWidthName);
end;

function TFresnelCSSRegistry.RoundFontWidthToKeyword(aWidth: TFresnelLength): TCSSNumericalID;
begin
  Result:=FontWidthKeywords[RoundFontWidth(aWidth)];
end;

constructor TFresnelCSSRegistry.Create;
begin
  Attribute_ClassOf:=TFresnelCSSAttrDesc;
  inherited Create;
end;

procedure TFresnelCSSRegistry.Init;

  procedure AddKW(ExpectedID: TCSSNumericalID; const aName: TCSSString);
  var
    Actual: TCSSNumericalID;
  begin
    Actual:=AddKeyword(aName);
    if Actual=ExpectedID then exit;
    raise EFresnel.Create('register keyword "'+aName+'" failed. Expected '+IntToStr(ExpectedID)+', but got '+IntToStr(Actual));
  end;

  procedure AddFunc(ExpectedID: TCSSNumericalID; const aName: TCSSString);
  var
    Actual: TCSSNumericalID;
  begin
    Actual:=AddAttrFunction(aName);
    if Actual=ExpectedID then exit;
    raise EFresnel.Create('register attribute function "'+aName+'" failed. Expected '+IntToStr(ExpectedID)+', but got '+IntToStr(Actual));
  end;

var
  Desc: TFresnelCSSAttrDesc;
  PseudoCl: TFresnelCSSPseudoClass;
  PseElDesc: TCSSPseudoElementDesc;
begin
  inherited Init;

  // keywords
  AddKW(kwAbsolute,'absolute'); // e.g. position
  AddKW(kwAlias,'alias'); // e.g. cursor
  AddKW(kwAllPetiteCaps,'all-petite-caps'); // e.g. font-variant-caps
  AddKW(kwAllScroll,'all-scroll'); // e.g. cursor
  AddKW(kwAllSmallCaps,'all-small-caps'); // e.g. font-variant-caps
  AddKW(kwAnchorCenter,'anchor-center'); // e.g. justify-self
  AddKW(kwBaseline,'baseline'); // e.g. justify-self
  AddKW(kwBlock,'block'); // e.g. display
  AddKW(kwBlockEnd,'block-end'); // e.g. clear
  AddKW(kwBlockStart,'block-start'); // e.g. clear
  AddKW(kwBold,'bold'); // e.g. font-weight
  AddKW(kwBolder,'bolder'); // e.g. font-weight
  AddKW(kwBorderBox,'border-box'); // e.g. box-sizing
  AddKW(kwBoth,'both'); // e.g. clear
  AddKW(kwBothBlock,'both-block'); // e.g. clear
  AddKW(kwBothEdges,'both-edges'); // e.g. scrollbar-gutter
  AddKW(kwBothInline,'both-inline'); // e.g. clear
  AddKW(kwBottom,'bottom'); // e.g. clear
  AddKW(kwCaption,'caption'); // e.g. font
  AddKW(kwCell,'cell'); // e.g. cursor
  AddKW(kwCenter,'center'); // e.g. background-position-x
  AddKW(kwClip,'clip'); // e.g. overflow-x
  AddKW(kwCollapse,'collapse'); // e.g. visible
  AddKW(kwColResize,'col-resize'); // e.g. cursor
  AddKW(kwColumn,'column'); // e.g. flex-direction
  AddKW(kwColumnReverse,'column-reverse'); // e.g. flex-direction
  AddKW(kwCommonLigatures,'common-ligatures'); // e.g. font-variant-ligatures
  AddKW(kwCondensed,'condensed'); // e.g. font-width
  AddKW(kwContain,'contain'); // e.g. background-size
  AddKW(kwContent,'content'); // e.g. flex-basis
  AddKW(kwContentBox,'content-box'); // e.g. box-sizing
  AddKW(kwContents,'contents'); // e.g. display
  AddKW(kwContextMenu,'context-menu'); // e.g. cursor
  AddKW(kwContextual,'contextual'); // e.g. font-variant-numeric
  AddKW(kwCopy,'copy'); // e.g. cursor
  AddKW(kwCover,'cover'); // e.g. background-size
  AddKW(kwCrosshair,'crosshair'); // e.g. cursor
  AddKW(kwCurrentcolor,'currentcolor'); // e.g. color
  AddKW(kwDashed,'dashed'); // e.g. border-style
  AddKW(kwDefault,'default'); // e.g. cursor
  AddKW(kwDiagonalFractions,'diagonal-fractions'); // e.g. font-variant-numeric
  AddKW(kwDiscretionaryLigatures,'discretionary-ligatures'); // e.g. font-variant-ligatures
  AddKW(kwDotted,'dotted'); // e.g. border-style
  AddKW(kwDouble,'double'); // e.g. border-style
  AddKW(kwEmoji,'emoji'); // e.g. font-variant-emoji
  AddKW(kwEnd,'end'); // e.g. justify-content
  AddKW(kwEResize,'e-resize'); // e.g. cursor
  AddKW(kwEWResize,'ew-resize'); // e.g. cursor
  AddKW(kwExpanded,'expanded'); // e.g. font-width
  AddKW(kwExtraCondensed,'extra-condensed'); // e.g. font-width
  AddKW(kwExtraExpanded,'extra-expanded'); // e.g. font-width
  AddKW(kwFirst,'first'); // e.g. justify-self
  AddKW(kwFitContent,'fit-content'); // e.g. width, min-width
  AddKW(kwFixed,'fixed'); // e.g. position, background-attachment
  AddKW(kwFlex,'flex'); // e.g. display
  AddKW(kwFlexEnd,'flex-end'); // e.g. justify-content
  AddKW(kwFlexStart,'flex-start'); // e.g. justify-self
  AddKW(kwFlow,'flow'); // e.g. display
  AddKW(kwFlowRoot,'flow-root'); // e.g. display
  AddKW(kwFullWidth,'full-width'); // e.g. font-variant-east-asian
  AddKW(kwGrab,'grab'); // e.g. cursor
  AddKW(kwGrabbing,'grabbing'); // e.g. cursor
  AddKW(kwGrid,'grid'); // e.g. display
  AddKW(kwGroove,'groove'); // e.g. border-style
  AddKW(kwHelp,'help'); // e.g. cursor
  AddKW(kwHidden,'hidden'); // e.g. overflow-x, border-style, visible
  AddKW(kwHistoricalForms,'historical-forms'); // e.g. font-variant-alternates
  AddKW(kwHistoricalLigatures,'historical-ligatures'); // e.g. font-variant-ligatures
  AddKW(kwHorizontalTB,'horizontal-tb'); // e.g. writing-mode
  AddKW(kwIcon,'icon'); // e.g. font
  AddKW(kwIn,'in'); // e.g. background-image
  AddKW(kwInline,'inline'); // e.g. display-outside
  AddKW(kwInlineBlock,'inline-block'); // e.g. display
  AddKW(kwInlineEnd,'inline-end'); // e.g. clear, float
  AddKW(kwInlineFlow,'inline-flow'); // e.g. display
  AddKW(kwInlineStart,'inline-start'); // e.g. clear, float
  AddKW(kwInset,'inset'); // e.g. border-style
  AddKW(kwItalic,'italic'); // e.g. font-style
  AddKW(kwJis04,'jis04'); // e.g. font-variant-east-asian
  AddKW(kwJis78,'jis78'); // e.g. font-variant-east-asian
  AddKW(kwJis83,'jis83'); // e.g. font-variant-east-asian
  AddKW(kwJis90,'jis90'); // e.g. font-variant-east-asian
  AddKW(kwLarge,'large'); // e.g. font-size
  AddKW(kwLarger,'larger'); // e.g. font-size
  AddKW(kwLast,'last'); // e.g. justify-self
  AddKW(kwLeft,'left'); // e.g. clear, float
  AddKW(kwLegacy,'legacy'); // e.g. justify-items
  AddKW(kwLighter,'lighter'); // e.g. font-weight
  AddKW(kwLiningNums,'lining-nums'); // e.g. font-variant-numeric
  AddKW(kwLocal,'local'); // e.g. background-attachment
  AddKW(kwLTR,'ltr'); // e.g. direction
  AddKW(kwMaxContent,'max-content'); // e.g. width, min-width
  AddKW(kwMedium,'medium'); // e.g. border-left-width, font-size
  AddKW(kwMenu,'menu'); // e.g. font
  AddKW(kwMessageBox,'message-box'); // e.g. font
  AddKW(kwMinContent,'min-content'); // e.g. width, min-width
  AddKW(kwMove,'move'); // e.g. cursor
  AddKW(kwNEResize,'ne-resize'); // e.g. cursor
  AddKW(kwNESWResize,'nesw-resize'); // e.g. cursor
  AddKW(kwNoCommonLigatures,'no-common-ligatures'); // e.g. font-variant-ligatures
  AddKW(kwNoContextual,'no-contextual'); // e.g. font-variant-ligatures
  AddKW(kwNoDiscretionaryLigatures,'no-discretionary-ligatures'); // e.g. font-variant-ligatures
  AddKW(kwNoDrop,'no-drop'); // e.g. cursor
  AddKW(kwNoHistoricalLigatures,'no-historical-ligatures'); // e.g. font-variant-ligatures
  AddKW(kwNoRepeat,'no-repeat'); // e.g. background-repeat
  AddKW(kwNormal,'normal'); // e.g. font-feature-settings, font-size
  AddKW(kwNotAllowed,'not-allowed'); // e.g. cursor
  AddKW(kwNoWrap,'nowrap'); // e.g. flex-wrap
  AddKW(kwNResize,'n-resize'); // e.g. cursor
  AddKW(kwNSResize,'ns-resize'); // e.g. cursor
  AddKW(kwNWResize,'nw-resize'); // e.g. cursor
  AddKW(kwNWSEResize,'nwse-resize'); // e.g. cursor
  AddKW(kwOblique,'oblique'); // e.g. font-style
  AddKW(kwOldstyleNums,'oldstyle-nums'); // e.g. font-variant-numeric
  AddKW(kwOrdinal,'ordinal'); // e.g. font-variant-caps
  AddKW(kwOutset,'outset'); // e.g. border-style
  AddKW(kwPaddingBox,'padding-box'); // e.g. background-origin
  AddKW(kwPetiteCaps,'petite-caps'); // e.g. font-variant-caps
  AddKW(kwPointer,'pointer'); // e.g. cursor
  AddKW(kwProgress,'progress'); // e.g. cursor
  AddKW(kwProportionalNums,'proportional-nums'); // e.g. font-variant-numeric
  AddKW(kwProportionalWidth,'proportional-width'); // e.g. font-variant-east-asian
  AddKW(kwRelative,'relative'); // e.g. position
  AddKW(kwRepeat,'repeat'); // e.g. background-repeat
  AddKW(kwRepeatX,'repeat-x'); // e.g. background-repeat
  AddKW(kwRepeatY,'repeat-y'); // e.g. background-repeat
  AddKW(kwRidge,'ridge'); // e.g. border-style
  AddKW(kwRight,'right'); // e.g. clear, float
  AddKW(kwRound,'round'); // e.g. background-repeat
  AddKW(kwRow,'row'); // e.g. flex-direction
  AddKW(kwRowResize,'row-resize'); // e.g. cursor
  AddKW(kwRowReverse,'row-reverse'); // e.g. flex-direction
  AddKW(kwRTL,'rtl'); // e.g. direction
  AddKW(kwRuby,'ruby'); // e.g. font-variant-east-asian
  AddKW(kwSafe,'safe'); // e.g. justify-content
  AddKW(kwScroll,'scroll'); // e.g. overflow-x, background-attachment
  AddKW(kwSelfEnd,'self-end'); // e.g. justify-self
  AddKW(kwSelfStart,'self-start'); // e.g. justify-self
  AddKW(kwSemiCondensed,'semi-condensed'); // e.g. font-width
  AddKW(kwSemiExpanded,'semi-expanded'); // e.g. font-width
  AddKW(kwSEResize,'se-resize'); // e.g. cursor
  AddKW(kwSidewaysLR,'sideways-lr'); // e.g. writing-mode
  AddKW(kwSidewaysRL,'sideways-rl'); // e.g. writing-mode
  AddKW(kwSimplified,'simplified'); // e.g. font-variant-east-asian
  AddKW(kwSlashedZero,'slashed-zero'); // e.g. font-variant-numeric
  AddKW(kwSmall,'small'); // e.g. font-size
  AddKW(kwSmallCaps,'small-caps'); // e.g. font-variant-caps
  AddKW(kwSmallCaption,'small-caption'); // e.g. font
  AddKW(kwSmaller,'smaller'); // e.g. font-size
  AddKW(kwSolid,'solid'); // e.g. border-style
  AddKW(kwSpace,'space'); // e.g. background-repeat
  AddKW(kwSpaceAround,'space-around'); // e.g. justify-content
  AddKW(kwSpaceBetween,'space-between'); // e.g. justify-content
  AddKW(kwSpaceEvenly,'space-evenly'); // e.g. justify-content
  AddKW(kwSResize,'s-resize'); // e.g. cursor
  AddKW(kwStable,'stable'); // e.g. scrollbar-gutter
  AddKW(kwStackedFractions,'stacked-fractions'); // e.g. font-variant-numeric
  AddKW(kwStart,'start'); // e.g. justify-content
  AddKW(kwStatic,'static'); // e.g. position
  AddKW(kwStatusBar,'status-bar'); // e.g. font
  AddKW(kwSticky,'sticky'); // e.g. position
  AddKW(kwStretch,'stretch'); // e.g. justify-content
  AddKW(kwSub,'sub'); // e.g. font-variant-position
  AddKW(kwSuper,'super'); // e.g. font-variant-position
  AddKW(kwSWResize,'sw-resize'); // e.g. cursor
  AddKW(kwTabularNums,'tabular-nums'); // e.g. font-variant-numeric
  AddKW(kwText,'text'); // e.g. cursor, font-variant-emoji
  AddKW(kwThick,'thick'); // e.g. border-left-width
  AddKW(kwThin,'thin'); // e.g. border-left-width
  AddKW(kwTitlingCaps,'titling-caps'); // e.g. font-variant-caps
  AddKW(kwTo,'to'); // e.g. background-image
  AddKW(kwTop,'top'); // e.g. clear
  AddKW(kwTraditional,'traditional'); // e.g. font-variant-east-asian
  AddKW(kwUltraCondensed,'ultra-condensed'); // e.g. font-width
  AddKW(kwUltraExpanded,'ultra-expanded'); // e.g. font-width
  AddKW(kwUnicase,'unicase'); // e.g. font-variant-caps
  AddKW(kwUnicode,'unicode'); // e.g. font-variant-emoji
  AddKW(kwUnsafe,'unsafe'); // e.g. justify-content
  AddKW(kwVerticalLR,'vertical-lr'); // e.g. writing-mode
  AddKW(kwVerticalRL,'vertical-rl'); // e.g. writing-mode
  AddKW(kwVerticalText,'vertical-text'); // e.g. cursor
  AddKW(kwVisible,'visible'); // e.g. overflow-x, visible
  AddKW(kwWait,'wait'); // e.g. cursor
  AddKW(kwWrap,'wrap'); // e.g. flex-wrap
  AddKW(kwWrapReverse,'wrap-reverse'); // e.g. flex-wrap
  AddKW(kwWResize,'w-resize'); // e.g. cursor
  AddKW(kwXLarge,'x-large'); // e.g. font-size
  AddKW(kwXSmall,'x-small'); // e.g. font-size
  AddKW(kwXXLarge,'xx-large'); // e.g. font-size
  AddKW(kwXXSmall,'xx-small'); // e.g. font-size
  AddKW(kwXXXLarge,'xxx-large'); // e.g. font-size
  AddKW(kwZoomIn,'zoom-in'); // e.g. cursor
  AddKW(kwZoomOut,'zoom-out'); // e.g. cursor

  // color keywords
  AddColorKeywords;

  // css functions - - - - - - - - - - - - - - - - - - - - - -
  AddFunc(afUrl,'url'); // e.g. background-image
  AddFunc(afLinearGradient,'linear-gradient'); // e.g. background-image
  AddFunc(afRadialGradient,'radial-gradient'); // e.g. background-image
  AddFunc(afConicGradient,'conic-gradient'); // e.g. background-image
  AddFunc(afRepeatingLinearGradient,'repeating-linear-gradient'); // e.g. background-image
  AddFunc(afRepeatingRadialGradient,'repeating-radial-gradient'); // e.g. background-image
  AddFunc(afRepeatingConicGradient,'repeating-conic-gradient'); // e.g. background-image

  // attributes - - - - - - - - - - - - - - - - - - - - - -
  Chk_Color_KeywordIDs:=[kwCurrentcolor];

  // display
  Chk_DisplayBox_KeywordIDs:=[kwContents,kwNone];
  Chk_DisplayInside_KeywordIDs:=[kwFlow,kwFlowRoot,kwFlex];
  Chk_DisplayOutside_KeywordIDs:=[kwBlock,kwInline];

  AddFresnelLonghand(fcaDisplay,false,@CheckDisplay,'inline');
  Chk_Display_KeywordIDs:=concat(Chk_DisplayBox_KeywordIDs,
    Chk_DisplayInside_KeywordIDs,
    Chk_DisplayOutside_KeywordIDs);

  // position
  AddFresnelLonghand(fcaPosition,false,@CheckPosition,'static');
  Chk_Position_KeywordIDs:=[kwStatic,kwRelative,kwAbsolute,kwFixed,kwStatic];

  // box-sizing
  AddFresnelLonghand(fcaBoxSizing,false,@CheckBoxSizing,'content-box');
  Chk_BoxSizing_KeywordIDs:=[kwContentBox,kwBorderBox];

  // direction
  Desc:=AddFresnelLonghand(fcaDirection,true,@CheckDirection,'ltr');
  Desc.All:=false;
  Chk_Direction_KeywordIDs:=[kwLTR,kwRTL];

  // writing-mode
  AddFresnelLonghand(fcaWritingMode,true,@CheckWritingMode,'horizontal-tb');
  Chk_WritingMode_KeywordIDs:=[kwHorizontalTB,kwVerticalRL,kwVerticalLR,kwSidewaysRL,kwSidewaysLR];

  // z-index
  AddFresnelLonghand(fcaZIndex,false,@CheckZIndex,'auto');
  Chk_ZIndex_Dim.AllowNegative:=true;
  Chk_ZIndex_Dim.AllowedKeywordIDs:=[kwAuto];
  Chk_ZIndex_Dim.AllowedUnits:=[cuNone];

  // overflow
  AddFresnelLonghand(fcaOverflowX,false,@CheckOverflowXY,'visible');
  AddFresnelLonghand(fcaOverflowY,false,@CheckOverflowXY,'visible');
  Chk_OverflowXY_KeywordIDs:=[kwVisible,kwHidden,kwClip,kwScroll,kwAuto];

  AddFresnelShorthand(fcaOverflow,@CheckOverflowXY,@SplitOverflow,@GetOverflow,[fcaOverflowX,fcaOverflowY]);

  // clear
  AddFresnelLonghand(fcaClear,false,@CheckClear);
  Chk_Clear_KeywordIDs:=[kwInlineStart,kwInlineEnd,kwBlockStart,kwBlockEnd,
    kwLeft,kwRight,kwTop,kwBottom,
    kwBothInline,kwBothBlock,kwBoth];

  // top, right, bottom, left
  AddFresnelLonghandLength(fcaTop,false,@CheckLeftTopRightBottom,false,'auto',@GetLength_ContainerContentHeight);
  AddFresnelLonghandLength(fcaRight,false,@CheckLeftTopRightBottom,true,'auto',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaBottom,false,@CheckLeftTopRightBottom,false,'auto',@GetLength_ContainerContentHeight);
  AddFresnelLonghandLength(fcaLeft,false,@CheckLeftTopRightBottom,true,'auto',@GetLength_ContainerContentWidth);
  with Chk_LeftTopRightBottom_Dim do begin
    AllowedUnits:=cuAllLengthsAndPercent;
    AllowNegative:=true;
    AllowFrac:=true;
    AllowedKeywordIDs:=[kwAuto];
  end;

  // width, height
  AddFresnelLonghandLength(fcaWidth,false,@CheckWidthHeight,true,'auto',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaHeight,false,@CheckWidthHeight,false,'auto',@GetLength_ContainerContentHeight);
  Chk_WidthHeight_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_WidthHeight_Dim.AllowFrac:=true;
  Chk_WidthHeight_Dim.AllowedKeywordIDs:=[kwAuto,kwFitContent,kwMinContent,kwMaxContent];

  // min-width, max-width, min-height, max-height
  AddFresnelLonghandLength(fcaMinWidth,false,@CheckWidthHeight,true,'auto',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaMaxWidth,false,@CheckWidthHeight,false,'auto',@GetLength_ContainerContentHeight);
  AddFresnelLonghandLength(fcaMinHeight,false,@CheckWidthHeight,true,'auto',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaMaxHeight,false,@CheckWidthHeight,false,'auto',@GetLength_ContainerContentHeight);

  // border-width
  AddFresnelLonghandLength(fcaBorderTopWidth,false,@CheckBorderWidth,false,'medium',@GetLength_BorderWidth);
  AddFresnelLonghandLength(fcaBorderRightWidth,false,@CheckBorderWidth,true,'medium',@GetLength_BorderWidth);
  AddFresnelLonghandLength(fcaBorderBottomWidth,false,@CheckBorderWidth,false,'medium',@GetLength_BorderWidth);
  AddFresnelLonghandLength(fcaBorderLeftWidth,false,@CheckBorderWidth,true,'medium',@GetLength_BorderWidth);
  Chk_BorderWidth_Dim.AllowedUnits:=cuAllLengths;
  Chk_BorderWidth_Dim.AllowFrac:=true;
  Chk_BorderWidth_Dim.AllowedKeywordIDs:=[kwThin,kwMedium,kwThick];

  AddFresnelShorthand(fcaBorderWidth,@CheckBorderWidth,@SplitBorderWidth,@GetBorderWidth,
    [fcaBorderLeftWidth,fcaBorderRightWidth,fcaBorderTopWidth,fcaBorderBottomWidth]);

  // border-color
  AddFresnelLonghand(fcaBorderTopColor,false,@CheckColor,'currentcolor');
  AddFresnelLonghand(fcaBorderRightColor,false,@CheckColor,'currentcolor');
  AddFresnelLonghand(fcaBorderBottomColor,false,@CheckColor,'currentcolor');
  AddFresnelLonghand(fcaBorderLeftColor,false,@CheckColor,'currentcolor');

  AddFresnelShorthand(fcaBorderColor,@CheckColor,@SplitBorderColor,@GetBorderColor,
    [fcaBorderLeftColor,fcaBorderRightColor,fcaBorderTopColor,fcaBorderBottomColor]);

  // border-style
  AddFresnelLonghand(fcaBorderTopStyle,false,@CheckBorderStyle);
  AddFresnelLonghand(fcaBorderRightStyle,false,@CheckBorderStyle);
  AddFresnelLonghand(fcaBorderBottomStyle,false,@CheckBorderStyle);
  AddFresnelLonghand(fcaBorderLeftStyle,false,@CheckBorderStyle);
  Chk_BorderStyle_KeywordIDs:=[kwNone,kwHidden,kwDotted,kwDashed,kwSolid,kwDouble,kwGroove,kwRidge,kwInset,kwOutset];

  AddFresnelShorthand(fcaBorderStyle,@CheckBorderStyle,@SplitBorderStyle,@GetBorderStyle,
    [fcaBorderLeftStyle,fcaBorderRightStyle,fcaBorderTopStyle,fcaBorderBottomStyle]);

  // border-left, -right, -top, -bottom
  AddFresnelShorthand(fcaBorderTop,@CheckBorder,@SplitBorderLeftRightTopBottom,@GetBorderTop,
    [fcaBorderTopWidth,fcaBorderTopStyle,fcaBorderTopColor]);
  AddFresnelShorthand(fcaBorderRight,@CheckBorder,@SplitBorderLeftRightTopBottom,@GetBorderRight,
    [fcaBorderRightWidth,fcaBorderRightStyle,fcaBorderRightColor]);
  AddFresnelShorthand(fcaBorderBottom,@CheckBorder,@SplitBorderLeftRightTopBottom,@GetBorderBottom,
    [fcaBorderBottomWidth,fcaBorderBottomStyle,fcaBorderBottomColor]);
  AddFresnelShorthand(fcaBorderLeft,@CheckBorder,@SplitBorderLeftRightTopBottom,@GetBorderLeft,
    [fcaBorderLeftWidth,fcaBorderLeftStyle,fcaBorderLeftColor]);

  // border: shorthand for all 12 border-[left,right,top,bottom]-[widht,style,color]
  AddFresnelShorthand(fcaBorder,@CheckBorder,@SplitBorderLeftRightTopBottom,@GetBorder,
    [fcaBorderWidth,fcaBorderStyle,fcaBorderColor, // overrides these shorthands too
     fcaBorderLeftWidth,fcaBorderLeftStyle,fcaBorderLeftColor,
     fcaBorderRightWidth,fcaBorderRightStyle,fcaBorderRightColor,
     fcaBorderTopWidth,fcaBorderTopStyle,fcaBorderTopColor,
     fcaBorderBottomWidth,fcaBorderBottomStyle,fcaBorderBottomColor]);

  // border-radius
  AddFresnelLonghand(fcaBorderTopLeftRadius,false,@CheckBorderRadius);
  AddFresnelLonghand(fcaBorderTopRightRadius,false,@CheckBorderRadius);
  AddFresnelLonghand(fcaBorderBottomRightRadius,false,@CheckBorderRadius);
  AddFresnelLonghand(fcaBorderBottomLeftRadius,false,@CheckBorderRadius);
  Chk_BorderRadius_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_BorderRadius_Dim.AllowFrac:=true;
  AddFresnelShorthand(fcaBorderRadius,@CheckBorderRadius,@SplitBorderRadius,@GetBorderRadius,
    [fcaBorderTopLeftRadius,fcaBorderTopRightRadius,fcaBorderBottomRightRadius,fcaBorderBottomLeftRadius]);

  // float
  AddFresnelLonghand(fcaFloat,false,@CheckFloat,'none');
  Chk_Float_KeywordIDs:=[kwNone,kwLeft,kwRight,kwInlineStart,kwInlineEnd];

  // font-feature-settings
  AddFresnelLonghand(fcaFontFeatureSettings,true,@CheckFontFeatureSettings,'normal');
  Chk_FontFeatureSettings_KeywordIDs:=[kwNormal];
  // font-family
  Desc:=AddFresnelLonghand(fcaFontFamily,true,@CheckFontFamily);
  Desc.OnAsString:=@GetFontFamily;
  // font-kerning
  Desc:=AddFresnelLonghand(fcaFontKerning,true,@CheckFontKerning,'auto');
  Desc.OnAsString:=@GetFontKerning;
  Chk_FontKerning_KeywordIDs:=[kwAuto,kwNormal,kwNone];
  // font-size
  Desc:=AddFresnelLonghand(fcaFontSize,true,@CheckFontSize,'medium');
  Desc.OnAsString:=@GetFontSize;
  Chk_FontSize_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_FontSize_Dim.AllowFrac:=true;
  Chk_FontSize_Dim.AllowedKeywordIDs:=[kwXXSmall,kwXSmall,kwSmall,kwSmaller,kwMedium,
    kwLarge,kwLarger,kwXLarge,kwXXLarge,kwXXXLarge];
  // font-style
  Desc:=AddFresnelLonghand(fcaFontStyle,true,@CheckFontStyle,'normal');
  Desc.OnAsString:=@GetFontStyle;
  Chk_FontStyle_KeywordIDs:=[kwNormal,kwItalic,kwOblique];
  // font-weight
  Desc:=AddFresnelLonghand(fcaFontWeight,true,@CheckFontWeight,'normal');
  Desc.OnAsString:=@GetFontWeight;
  Chk_FontWeight_Dim.AllowedKeywordIDs:=[kwNormal,kwBold,kwBolder,kwLighter];
  Chk_FontWeight_Dim.AllowedUnits:=[cuNone];
  // font-width
  Desc:=AddFresnelLonghand(fcaFontWidth,true,@CheckFontWidth,'normal');
  Desc.OnAsString:=@GetFontWidth;
  Chk_FontWidth_Dim.AllowedUnits:=[cuPercent];
  Chk_FontWidth_Dim.AllowFrac:=true;
  Chk_FontWidth_Dim.AllowedKeywordIDs:=[kwNormal,
    kwCondensed,kwSemiCondensed,kwExtraCondensed,kwUltraCondensed,
    kwExpanded,kwSemiExpanded,kwExtraExpanded,kwUltraExpanded];
  // font-stretch
  AddFresnelShorthand(fcaFontStretch,@CheckFontWidth,@SplitFontStretch,@GetFontWidth,[fcaFontWidth]);
  // font-variant-alternates
  Desc:=AddFresnelLonghand(fcaFontVariantAlternates,true,@CheckFontVariantAlternates,'normal');
  Desc.OnAsString:=@GetFontVariantAlternates;
  Chk_FontVariantAlternates_KeywordIDs:=[kwNormal,kwHistoricalForms];
  // font-variant-caps
  Desc:=AddFresnelLonghand(fcaFontVariantCaps,true,@CheckFontVariantCaps,'normal');
  Desc.OnAsString:=@GetFontVariantCaps;
  Chk_FontVariantCaps_KeywordIDs:=[kwNormal,kwSmallCaps,kwAllSmallCaps,kwPetiteCaps,
                                   kwAllPetiteCaps,kwUnicase,kwTitlingCaps];
  // font-variant-east-asian
  Desc:=AddFresnelLonghand(fcaFontVariantEastAsian,true,@CheckFontVariantEastAsian,'normal');
  Desc.OnAsString:=@GetFontVariantEastAsian;
  Chk_FontVariantEastAsian_KeywordIDs:=[kwNormal,kwRuby,kwJis78,kwJis83,kwJis90,kwJis04,
                            kwSimplified,kwTraditional,kwFullWidth,kwProportionalWidth];
  // font-variant-emoji
  Desc:=AddFresnelLonghand(fcaFontVariantEmoji,true,@CheckFontVariantEmoji,'normal');
  Desc.OnAsString:=@GetFontVariantEmoji;
  Chk_FontVariantEmoji_KeywordIDs:=[kwNormal,kwText,kwEmoji,kwUnicode];
  // font-variant-ligatures
  Desc:=AddFresnelLonghand(fcaFontVariantLigatures,true,@CheckFontVariantLigatures,'normal');
  Desc.OnAsString:=@GetFontVariantLigatures;
  Chk_FontVariantLigatures_KeywordIDs:=[kwNormal,kwNone,kwCommonLigatures,
      kwNoCommonLigatures,kwDiscretionaryLigatures,kwNoDiscretionaryLigatures,
      kwHistoricalLigatures,kwNoHistoricalLigatures,kwContextual,kwNoContextual];
  // font-variant-numeric
  Desc:=AddFresnelLonghand(fcaFontVariantNumeric,true,@CheckFontVariantNumeric,'normal');
  Desc.OnAsString:=@GetFontVariantNumeric;
  Chk_FontVariantNumeric_KeywordIDs:=[kwNormal,kwOrdinal,kwSlashedZero,kwLiningNums,
      kwOldstyleNums,kwProportionalNums,kwTabularNums,kwDiagonalFractions,kwStackedFractions];
  // font-variant-position
  Desc:=AddFresnelLonghand(fcaFontVariantPosition,true,@CheckFontVariantPosition,'normal');
  Desc.OnAsString:=@GetFontVariantPosition;
  Chk_FontVariantPosition_KeywordIDs:=[kwNormal,kwSub,kwSuper];
  // font-variant
  Desc:=AddFresnelShorthand(fcaFontVariant,@CheckFontVariant,@SplitFontVariant,@GetFontVariant,
    [fcaFontVariantAlternates,fcaFontVariantCaps,fcaFontVariantEastAsian,
     fcaFontVariantEmoji,fcaFontVariantLigatures,fcaFontVariantNumeric,
     fcaFontVariantPosition]);
  Chk_FontVariant_KeywordIDs:=[
      kwNormal,kwNone,
      // font-variant-caps
      kwSmallCaps,kwAllSmallCaps,kwPetiteCaps,kwAllPetiteCaps,kwUnicase,kwTitlingCaps,
      // font-variant-east-asian
      kwRuby,kwJis78,kwJis83,kwJis90,kwJis04,
      kwSimplified,kwTraditional,kwFullWidth,kwProportionalWidth,
      // font-variant-emoji
      kwText,kwEmoji,kwUnicode,
      // font-variant-ligatures
      kwCommonLigatures,kwNoCommonLigatures,kwDiscretionaryLigatures,kwNoDiscretionaryLigatures,
      kwHistoricalLigatures,kwNoHistoricalLigatures,kwContextual,kwNoContextual,
      // font-variant-numeric
      kwOrdinal,kwSlashedZero,kwLiningNums,
      kwOldstyleNums,kwProportionalNums,kwTabularNums,kwDiagonalFractions,kwStackedFractions,
      // font-variant-position
      kwSub,kwSuper];
  // line-height
  AddFresnelLonghand(fcaLineHeight,true,@CheckLineHeight,'normal');
  Chk_LineHeight_Dim.AllowedKeywordIDs:=[kwNormal];
  Chk_LineHeight_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_LineHeight_Dim.AllowFrac:=true;

  // font
  AddFresnelShorthand(fcaFont,@CheckFont,@SplitFont,@GetFont,
    [fcaFontFamily,fcaFontSize,fcaFontStyle,fcaFontVariant,fcaFontWeight,fcaLineHeight]);

  // text-shadow
  AddFresnelLonghand(fcaTextShadow,true,@CheckTextShadow,'none');
  Chk_TextShadow_Offset_Dim.AllowedUnits:=cuAllLengths;
  Chk_TextShadow_Offset_Dim.AllowFrac:=true;
  Chk_TextShadow_Offset_Dim.AllowNegative:=true;
  Chk_TextShadow_Radius_Dim.AllowedUnits:=cuAllLengths;
  Chk_TextShadow_Radius_Dim.AllowFrac:=true;

  // margin, Note: % uses container's width even for top and bottom
  AddFresnelLonghandLength(fcaMarginTop,false,@CheckMargin,false,'0',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaMarginRight,false,@CheckMargin,true,'0',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaMarginBottom,false,@CheckMargin,false,'0',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaMarginLeft,false,@CheckMargin,true,'0',@GetLength_ContainerContentWidth);
  Chk_Margin_Dim.AllowedKeywordIDs:=[kwAuto];
  Chk_Margin_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_Margin_Dim.AllowFrac:=true;
  Chk_Margin_Dim.AllowNegative:=true;
  AddFresnelShorthand(fcaMargin,@CheckMargin,@SplitMargin,@GetMargin,
    [fcaMarginLeft,fcaMarginRight,fcaMarginTop,fcaMarginBottom]);

  // margin-block
  AddFresnelLonghand(fcaMarginBlockEnd,false,@CheckMargin,'0');
  AddFresnelLonghand(fcaMarginBlockStart,false,@CheckMargin,'0');
  AddFresnelShorthand(fcaMarginBlock,@CheckMargin,@SplitMarginBlock,@GetMarginBlock,
    [fcaMarginBlockStart,fcaMarginBlockEnd]);

  // margin-inline
  AddFresnelLonghand(fcaMarginInlineEnd,false,@CheckMargin,'0');
  AddFresnelLonghand(fcaMarginInlineStart,false,@CheckMargin,'0');
  AddFresnelShorthand(fcaMarginInline,@CheckMargin,@SplitMarginInline,@GetMarginInline,
    [fcaMarginInlineStart,fcaMarginInlineEnd]);

  // opacity
  AddFresnelLonghand(fcaOpacity,false,@CheckOpacity,'1');
  Chk_Opacity_Dim.AllowedUnits:=[cuNone,cuPercent];
  Chk_Opacity_Dim.AllowFrac:=true;

  // padding, Note: % uses container's width even for top and bottom
  AddFresnelLonghandLength(fcaPaddingTop,false,@CheckPadding,false,'0',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaPaddingRight,false,@CheckPadding,true,'0',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaPaddingBottom,false,@CheckPadding,false,'0',@GetLength_ContainerContentWidth);
  AddFresnelLonghandLength(fcaPaddingLeft,false,@CheckPadding,true,'0',@GetLength_ContainerContentWidth);
  Chk_Padding_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_Padding_Dim.AllowFrac:=true;
  AddFresnelShorthand(fcaPadding,@CheckPadding,@SplitPadding,@GetPadding,
    [fcaPaddingLeft,fcaPaddingRight,fcaPaddingTop,fcaPaddingBottom]);

  // visibility
  AddFresnelLonghand(fcaVisibility,true,@CheckVisible,'visible');
  Chk_Visible_KeywordIDs:=[kwVisible,kwCollapse,kwHidden];

  // background-attachment
  AddFresnelLonghand(fcaBackgroundAttachment,false,@CheckBackgroundAttachment,'scroll');
  Chk_BackgroundAttachment_KeywordIDs:=[kwScroll,kwFixed,kwLocal];
  // background-clip
  AddFresnelLonghand(fcaBackgroundClip,false,@CheckBackgroundClip,'border-box');
  Chk_BackgroundClip_KeywordIDs:=[kwContentBox,kwPaddingBox,kwBorderBox,kwText];
  // background-color
  AddFresnelLonghand(fcaBackgroundColor,false,@CheckColor,'transparent');
  // background-image
  AddFresnelLonghand(fcaBackgroundImage,false,@CheckBackgroundImage,'');
  // background-origin
  AddFresnelLonghand(fcaBackgroundOrigin,false,@CheckBackgroundOrigin,'padding-box');
  Chk_BackgroundOrigin_KeywordIDs:=[kwContentBox,kwPaddingBox,kwBorderBox];
  // background-position-x, % depends on background-origin
  AddFresnelLonghandLength(fcaBackgroundPositionX,false,@CheckBackgroundPositionXY,true,'0%');
  Chk_BackgroundPositionX_Dim.AllowedKeywordIDs:=[kwLeft,kwCenter,kwRight];
  Chk_BackgroundPositionX_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_BackgroundPositionX_Dim.AllowFrac:=true;
  // background-position-y
  AddFresnelLonghandLength(fcaBackgroundPositionY,false,@CheckBackgroundPositionXY,false,'0%');
  Chk_BackgroundPositionY_Dim.AllowedKeywordIDs:=[kwTop,kwCenter,kwBottom];
  Chk_BackgroundPositionY_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_BackgroundPositionY_Dim.AllowFrac:=true;
  // background-position
  AddFresnelShorthand(fcaBackgroundPosition,@CheckBackgroundPosition,@SplitBackgroundPosition,
    @GetBackgroundPosition,[fcaBackgroundPositionX,fcaBackgroundPositionY]);
  Chk_BackgroundPosition_Dim.AllowedKeywordIDs:=[kwLeft,kwRight,kwTop,kwBottom,kwCenter];
  Chk_BackgroundPosition_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_BackgroundPosition_Dim.AllowFrac:=true;
  // background-repeat
  AddFresnelLonghand(fcaBackgroundRepeat,false,@CheckBackgroundRepeat,'repeat');
  Chk_BackgroundRepeat_KeywordIDs:=[kwRepeat,kwNoRepeat,kwSpace,kwRound,kwRepeatX,kwRepeatY];
  // background-size
  AddFresnelLonghand(fcaBackgroundSize,false,@CheckBackgroundSize,'auto auto');
  Chk_BackgroundSize_Dim.AllowedKeywordIDs:=[kwCover,kwContain,kwAuto];
  Chk_BackgroundSize_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_BackgroundSize_Dim.AllowFrac:=true;

  // background
  AddFresnelShorthand(fcaBackground,@CheckBackground,@SplitBackground,@GetBackground,
    [fcaBackgroundAttachment,fcaBackgroundClip,fcaBackgroundColor,fcaBackgroundImage,
     fcaBackgroundOrigin,fcaBackgroundRepeat,fcaBackgroundSize,fcaBackgroundPosition,
     fcaBackgroundPositionX,fcaBackgroundPositionY]);

  // color
  AddFresnelLonghand(fcaColor,true,@CheckColor,'canvastext');

  // cursor
  AddFresnelLonghand(fcaCursor,true,@CheckCursor,'auto');
  Chk_Cursor_KeywordIDs:=[kwAuto,kwDefault,kwNone,kwContextMenu,kwHidden,kwPointer,
    kwProgress,kwContentBox,kwCrosshair,kwText,kwVerticalText,kwAlias,kwCopy,kwMove,kwNoDrop,
    kwNotAllowed,kwGrab,kwGrabbing,kwEResize,kwNResize,kwNEResize,kwNWResize,kwSResize,
    kwSEResize,kwSWResize,kwWResize,kwEWResize,kwNSResize,kwNESWResize,kwNWSEResize,
    kwColResize,kwRowResize,kwAllScroll,kwZoomIn,kwZoomOut];

  // scrollbar-color
  AddFresnelLonghand(fcaScrollbarColor,false,@CheckScrollbarColor,'auto');
  // scrollbar-gutter
  AddFresnelLonghand(fcaScrollbarGutter,false,@CheckScrollbarGutter,'auto');
  // scrollbar-width
  AddFresnelLonghand(fcaScrollbarWidth,false,@CheckScrollbarWidth,'auto');
  Chk_ScrollbarWidth_KeywordIDs:=[kwAuto,kwThin,kwNone];

  // flex-basis
  AddFresnelLonghand(fcaFlexBasis,false,@CheckFlexBasis,'auto');
  Chk_FlexBasis_Dim.AllowedKeywordIDs:=[kwAuto,kwMaxContent,kwMinContent,kwFitContent,kwContent];
  Chk_FlexBasis_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_FlexBasis_Dim.AllowFrac:=true;
  // flex-direction
  AddFresnelLonghand(fcaFlexDirection,false,@CheckFlexDirection,'row');
  Chk_FlexDirection_KeywordIDs:=[kwColumn,kwColumnReverse,kwRow,kwRowResize];
  // flex-grow
  AddFresnelLonghand(fcaFlexGrow,false,@CheckFlexGrow,'0');
  Chk_FlexGrow_Dim.AllowedUnits:=[cuNone];
  Chk_FlexGrow_Dim.AllowFrac:=true;
  // flex-shrink
  AddFresnelLonghand(fcaFlexShrink,false,@CheckFlexShrink,'1');
  Chk_FlexShrink_Dim.AllowedUnits:=[cuNone];
  Chk_FlexShrink_Dim.AllowFrac:=true;
  // flex-wrap
  AddFresnelLonghand(fcaFlexWrap,false,@CheckFlexWrap,'nowrap');
  Chk_FlexWrap_KeywordIDs:=[kwNoWrap,kwWrap,kwWrapReverse];
  // flex-flow
  AddFresnelShorthand(fcaFlexFlow,@CheckFlexFlow,@SplitFlexFlow,@GetFlexFlow,
    [fcaFlexDirection,fcaFlexWrap]);
  // flex
  AddFresnelShorthand(fcaFlex,@CheckFlex,@SplitFlex,@GetFlex,
    [fcaFlexGrow,fcaFlexShrink,fcaFlexBasis]);

  // shared attributes of flex+grid

  // align-content
  AddFresnelLonghand(fcaAlignContent,false,@CheckAlignContent,'normal');
  Chk_AlignContent_KeywordIDs:=[kwNormal, kwCenter,kwStart,kwEnd,kwFlexEnd,kwCenter,
    kwBaseline,kwFirst,kwLast,
    kwSpaceBetween,kwSpaceAround,kwSpaceEvenly,kwStretch,
    kwSafe,kwUnsafe];
  // justify-items
  AddFresnelLonghand(fcaAlignItems,false,@CheckAlignItems,'normal');
  Chk_AlignItems_KeywordIDs:=[kwNormal,kwStretch,kwAnchorCenter,
    kwCenter,kwStart,kwEnd,kwFlexStart,kwFlexEnd,kwSelfStart,kwSelfEnd,
    kwSafe,kwUnsafe,
    kwBaseline,kwFirst,kwLast];
  // align-self
  AddFresnelLonghand(fcaAlignSelf,false,@CheckAlignSelf,'auto');
  Chk_AlignSelf_KeywordIDs:=[kwAuto,kwNormal,kwStretch,kwAnchorCenter,
    kwCenter,kwStart,kwEnd,kwFlexStart,kwFlexEnd,kwSelfStart,kwSelfEnd,
    kwSafe,kwUnsafe,
    kwBaseline,kwFirst,kwLast];

  // justify-content
  AddFresnelLonghand(fcaJustifyContent,false,@CheckJustifyContent,'normal');
  Chk_JustifyContent_KeywordIDs:=[kwNormal,kwStretch,
    kwStart,kwEnd,kwFlexStart,kwFlexEnd,kwCenter,kwLeft,kwRight,
    kwSpaceBetween,kwSpaceAround,kwSpaceEvenly,
    kwSafe,kwUnsafe];
  // justify-items
  AddFresnelLonghand(fcaJustifyItems,false,@CheckJustifyItems,'legacy');
  Chk_JustifyItems_KeywordIDs:=[kwLegacy,kwNormal,kwStretch,kwAnchorCenter,
    kwStart,kwEnd,kwFlexStart,kwFlexEnd,kwSelfStart,kwSelfEnd,
    kwCenter,kwLeft,kwRight,
    kwSafe,kwUnsafe,
    kwBaseline,kwFirst,kwLast];
  // justify-self
  AddFresnelLonghand(fcaJustifySelf,false,@CheckJustifySelf,'auto');
  Chk_JustifySelf_KeywordIDs:=[kwAuto,kwNormal,kwStretch,kwAnchorCenter,
    kwStart,kwEnd,kwFlexStart,kwFlexEnd,kwSelfStart,kwSelfEnd,
    kwCenter,kwLeft,kwRight,
    kwSafe,kwUnsafe,
    kwBaseline,kwFirst,kwLast];

  // place-content
  AddFresnelShorthand(fcaPlaceContent,@CheckPlaceContent,@SplitPlaceContent,@GetPlaceContent,
    [fcaAlignContent,fcaJustifyContent]);
  AddFresnelShorthand(fcaPlaceItems,@CheckPlaceItems,@SplitPlaceItems,@GetPlaceItems,
    [fcaAlignItems,fcaJustifyItems]);
  AddFresnelShorthand(fcaPlaceSelf,@CheckPlaceSelf,@SplitPlaceSelf,@GetPlaceSelf,
    [fcaAlignSelf,fcaJustifySelf]);

  // column-gap
  AddFresnelLonghandLength(fcaColumnGap,false,@CheckColumnRowGap,true,'normal',@GetLength_ContainerContentWidth);
  // row-gap
  AddFresnelLonghandLength(fcaRowGap,false,@CheckColumnRowGap,false,'normal',@GetLength_ContainerContentHeight);
  Chk_ColumnRowGap_Dim.AllowedKeywordIDs:=[kwNormal];
  Chk_ColumnRowGap_Dim.AllowedUnits:=cuAllLengthsAndPercent;
  Chk_ColumnRowGap_Dim.AllowFrac:=true;
  // gap
  AddFresnelShorthand(fcaGap,@CheckGap,@SplitGap,@GetGap,[fcaColumnGap,fcaRowGap]);

  // pseudo classes - - - - - - - - - - - - - - - - - - - - - -
  for PseudoCl in TFresnelCSSPseudoClass do
    AddFresnelPseudoClass(PseudoCl);

  // pseudo elements - - - - - - - - - - - - - - - - - - - - - -
  PseElDesc:=AddFresnelPseudoElement('selection',[fcaColor,fcaBackgroundColor,fcaTextShadow]);
  TPseudoElSelection.FFresnelSelectionTypeID:=PseElDesc.Index;
end;

function TFresnelCSSRegistry.AddFresnelAttr(Attr: TFresnelCSSAttribute;
  aInherits: boolean; const OnCheck: TCSSAttributeDesc.TCheckEvent
  ): TFresnelCSSAttrDesc;
begin
  Result:=TFresnelCSSAttrDesc(AddAttribute(FresnelCSSAttributeNames[Attr],'',aInherits,true,TFresnelCSSAttrDesc));
  Result.Attr:=Attr;
  Result.LengthHorizontal:=true;
  Result.OnCheck:=OnCheck;
  FresnelAttrs[Attr]:=Result;
end;

function TFresnelCSSRegistry.AddFresnelShorthand(Attr: TFresnelCSSAttribute;
  const OnCheck: TCSSAttributeDesc.TCheckEvent; const OnSplit: TCSSAttributeDesc.
  TSplitShorthandEvent; const OnAsString: TFresnelElementAttrDesc.TGetAsStringEvent;
  const CompProps: TFresnelCSSAttributeArray): TFresnelCSSAttrDesc;
var
  i: Integer;
  AttrDesc: TFresnelCSSAttrDesc;
begin
  if CompProps=nil then
    raise EFresnel.Create('20240712135716');
  Result:=AddFresnelAttr(Attr,false,OnCheck);
  Result.OnSplitShorthand:=OnSplit;
  Result.OnAsString:=OnAsString;
  SetLength(Result.CompProps,length(CompProps));
  for i:=0 to high(CompProps) do
  begin
    AttrDesc:=FresnelAttrs[CompProps[i]];
    if AttrDesc=nil then
      raise EFresnel.Create('20240712135833');
    Result.CompProps[i]:=AttrDesc;
  end;
end;

function TFresnelCSSRegistry.AddFresnelLonghand(Attr: TFresnelCSSAttribute;
  Inherits: boolean; const OnCheck: TCSSAttributeDesc.TCheckEvent;
  const InitialValue: TCSSString): TFresnelCSSAttrDesc;
begin
  Result:=AddFresnelAttr(Attr,Inherits,OnCheck);
  Result.InitialValue:=InitialValue;
end;

function TFresnelCSSRegistry.AddFresnelLonghandLength(Attr: TFresnelCSSAttribute;
  Inherits: boolean; const OnCheck: TCSSAttributeDesc.TCheckEvent; Horizontal: boolean;
  const InitialValue: TCSSString; const OnGetLength: TFresnelElementAttrDesc.TGetLength
  ): TFresnelCSSAttrDesc;
begin
  Result:=AddFresnelLonghand(Attr,Inherits,OnCheck,InitialValue);
  Result.LengthHorizontal:=Horizontal;
  Result.OnGetLength:=OnGetLength;
end;

procedure TFresnelCSSRegistry.SplitLonghandSides(var AttrIDs: TCSSNumericalIDArray;
  var Values: TCSSStringArray; Top, Right, Bottom, Left: TFresnelCSSAttribute;
  const Found: TCSSStringArray);
begin
  AddSplitLonghandSides(AttrIDs,Values,
    FresnelAttrs[Top].Index,
    FresnelAttrs[Right].Index,
    FresnelAttrs[Bottom].Index,
    FresnelAttrs[Left].Index,
    Found);
end;

procedure TFresnelCSSRegistry.SplitLonghandCorners(var AttrIDs: TCSSNumericalIDArray;
  var Values: TCSSStringArray; TopLeft, TopRight, BottomRight, BottomLeft: TFresnelCSSAttribute;
  const Found: TCSSStringArray);
begin
  AddSplitLonghandCorners(AttrIDs,Values,
    FresnelAttrs[TopLeft].Index,
    FresnelAttrs[TopRight].Index,
    FresnelAttrs[BottomRight].Index,
    FresnelAttrs[BottomLeft].Index,
    Found);
end;

function TFresnelCSSRegistry.CombinePlace(SubKW, MainKW: TCSSNumericalID): TCSSString;
begin
  if SubKW>0 then
    Result:=Keywords[SubKW]+' '+Keywords[MainKW]
  else
    Result:=Keywords[MainKW];
end;

function TFresnelCSSRegistry.CombineKeywords(const aKeywords: TCSSNumericalIDArray): TCSSString;
var
  i: Integer;
begin
  Result:='';
  for i:=0 to length(aKeywords)-1 do begin
    if i>0 then Result+=' ';
    Result+=Keywords[aKeywords[i]];
  end;
end;

function TFresnelCSSRegistry.AddFresnelPseudoClass(Pseudo: TFresnelCSSPseudoClass
  ): TFresnelCSSPseudoClassDesc;
begin
  Result:=TFresnelCSSPseudoClassDesc(AddPseudoClass(FresnelCSSPseudoClassNames[Pseudo],TFresnelCSSPseudoClassDesc));
  Result.Pseudo:=Pseudo;
  FresnelPseudoClasses[Pseudo]:=Result;
end;

function TFresnelCSSRegistry.AddFresnelPseudoElement(const aName: TCSSString;
  const AllowedAttribs: TFresnelCSSAttributeArray): TCSSPseudoElementDesc;
var
  i: Integer;
begin
  Result:=AddPseudoElement(aName);
  SetLength(Result.Attributes,length(AllowedAttribs));
  for i:=0 to length(AllowedAttribs)-1 do
    Result.Attributes[i]:=FresnelAttrs[AllowedAttribs[i]];
end;

function TFresnelCSSRegistry.IsColor(const ResValue: TCSSResCompValue): boolean;
var
  KW: TCSSNumericalID;
begin
  Result:=false;
  case ResValue.Kind of
  rvkKeyword:
    begin
      KW:=ResValue.KeywordID;
      if ((KW>=kwFirstColor) and (KW<=kwLastColor)) or (KW=kwCurrentcolor) then
        exit(true);
    end;
  rvkHexColor:
    exit(true);
  rvkFunction:
    begin
      // todo: rgb, rgba, hsl, hwb, lab, lch, oklab, oklch, light-dark, color(), color-mix()
    end;
  end;
end;

{ TFresnelRoundRect }

function TFresnelRoundRect.ToString: String;
begin
  Result:='['+Box.ToString+'] - [';
  Result:=Result+'TL='+Radii[fcsTopLeft].ToString+',';
  Result:=Result+'TR='+Radii[fcsTopRight].ToString+',';
  Result:=Result+'BR='+Radii[fcsBottomRight].ToString+',';
  Result:=Result+'BL='+Radii[fcsBottomLeft].ToString+']';
end;

{ TFresnelLayoutNode }

procedure TFresnelLayoutNode.SetElement(const AValue: TFresnelElement);
begin
  if FElement=AValue then Exit;
  if FElement<>nil then
    FElement.FLayoutNode:=nil;
  FElement:=AValue;
  if FElement<>nil then
    FElement.FLayoutNode:=Self;
end;

function TFresnelLayoutNode.GetNodeCount: integer;
begin
  if FNodes<>nil then
    Result:=FNodes.Count
  else
    Result:=0;
end;

function TFresnelLayoutNode.GetNodes(Index: integer): TFresnelLayoutNode;
begin
  Result:=TFresnelLayoutNode(FNodes[Index]);
end;

procedure TFresnelLayoutNode.SetParent(const AValue: TFresnelLayoutNode);
begin
  if FParent=AValue then Exit;
  if FParent<>nil then
  begin
    if FParent.FNodes<>nil then
      FParent.FNodes.Remove(Self);
  end;
  FParent:=AValue;
  if FParent<>nil then
  begin
    if FParent.FNodes=nil then
      FParent.FNodes:=TFPList.Create;
    FParent.FNodes.Add(Self);
  end;
end;

constructor TFresnelLayoutNode.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFresnelLayoutNode.Destroy;
var
  i: Integer;
begin
  for i:=NodeCount-1 downto 0 do
    Nodes[i].FParent:=nil;
  FreeAndNil(FNodes);
  Parent:=nil;
  inherited Destroy;
end;

function TFresnelLayoutNode.GetIntrinsicContentSize(aMode: TFresnelLayoutMode;
  aMaxWidth: TFresnelLength; aMaxHeight: TFresnelLength): TFresnelPoint;
begin
  Result.X:=0;
  Result.Y:=0;
  if aMode=flmMaxHeight then ;
  if IsNan(aMaxWidth) then ;
  if IsNan(aMaxHeight) then ;
end;

function TFresnelLayoutNode.GetRoot: TFresnelLayoutNode;
begin
  Result:=Self;
  while Result.Parent<>nil do
    Result:=Result.Parent;
end;

function TFresnelLayoutNode.FitWidth(aWidth: TFresnelLength): TFresnelLength;
begin
  Result:=aWidth;
  if not IsNan(MinWidth) then
  begin
    if not IsNan(MaxWidth) then
      Result:=MinWidth
    else if (not IsNan(Result)) and (Result<MinWidth) then
      Result:=MinWidth;
  end else if (not IsNan(MaxWidth)) and (not IsNan(Result)) and (Result>MaxWidth) then
    Result:=MaxWidth;
end;

function TFresnelLayoutNode.FitHeight(aHeight: TFresnelLength): TFresnelLength;
begin
  Result:=aHeight;
  if not IsNan(MinHeight) then
  begin
    if not IsNan(MaxHeight) then
      Result:=MinHeight
    else if (not IsNan(Result)) and (Result<MinHeight) then
      Result:=MinHeight;
  end else if (not IsNan(MaxHeight)) and (not IsNan(Result)) and (Result>MaxHeight) then
    Result:=MaxHeight;
end;

procedure TFresnelLayoutNode.ResetUsedLengths;
begin
  BorderLeft:=0;
  BorderRight:=0;
  BorderTop:=0;
  BorderBottom:=0;
  PaddingLeft:=0;
  PaddingRight:=0;
  PaddingTop:=0;
  PaddingBottom:=0;
  MarginLeft:=0;
  MarginRight:=0;
  MarginTop:=0;
  MarginBottom:=0;
  LineHeight:=0;

  Left:=NaN;
  Top:=NaN;
  Right:=NaN;
  Bottom:=NaN;

  Width:=NaN;
  Height:=NaN;
  MinWidth:=NaN;
  MinHeight:=NaN;
  MaxWidth:=NaN;
  MaxHeight:=NaN;

  FContainerContentHeightValid:=false;
  FContainerContentHeight:=NaN;
  FContainerContentWidthValid:=false;
  FContainerContentWidth:=NaN;
end;

{$IF FPC_FULLVERSION>=30301}
procedure TFresnelLayoutNode.SortNodes(
  const Compare: TListSortComparer_Context; Context: Pointer);
begin
  FNodes.Sort(Compare,Context,SortBase.DefaultSortingAlgorithm);
end;
{$ELSE}
procedure TFresnelLayoutNode.SortNodes(
  const Compare: TListSortCompare);
begin
  FNodes.Sort(Compare);
end;
{$ENDIF}

{ TFresnelFontDesc }

function TFresnelFontDesc.Compare(const Desc: TFresnelFontDesc): integer;
begin
  Result:=CompareText(Family,Desc.Family);
  if Result<>0 then exit;
  Result:=CompareValue(ord(Kerning),ord(Desc.Kerning));
  if Result<>0 then exit;
  Result:=CompareValue(Size,Desc.Size);
  if Result<>0 then exit;
  Result:=CompareText(Style,Desc.Style);
  if Result<>0 then exit;
  Result:=CompareValue(Weight,Desc.Weight);
  if Result<>0 then exit;
  Result:=CompareValue(Width,Desc.Width);
  if Result<>0 then exit;

  // font-variant:
  Result:=CompareText(Alternates,Desc.Alternates);
  if Result<>0 then exit;
  Result:=CompareValue(ord(Caps),ord(Desc.Caps));
  if Result<>0 then exit;
  Result:=CompareValue(integer(EastAsians),integer(Desc.EastAsians));
  if Result<>0 then exit;
  Result:=CompareValue(ord(Emoji),ord(Desc.Emoji));
  if Result<>0 then exit;
  Result:=CompareValue(integer(Ligatures),integer(Desc.Ligatures));
  if Result<>0 then exit;
  Result:=CompareValue(integer(Numerics),integer(Desc.Numerics));
  if Result<>0 then exit;
  Result:=CompareValue(ord(Position),ord(Desc.Position));
end;

class function TFresnelFontDesc.CompareDescriptors(const A, B: TFresnelFontDesc
  ): integer;
begin
  Result:=A.Compare(B);
end;

{ TFresnelViewport }

procedure TFresnelViewport.CSSResolverLog(Sender: TObject;
  Entry: TCSSResolverLogEntry);
begin
  DoLog(etDebug,'TFresnelViewport.CSSResolverLog ['+IntToStr(Entry.ID)+'] '+Entry.Msg+' at '+Resolver.GetElPos(Entry.PosEl));
end;

function TFresnelViewport.GetFocusedElement: TFresnelElement;
begin
  Result:=FFocusedElement;
end;

procedure TFresnelViewport.SetFocusedElement(const aValue: TFresnelElement);
begin
  TrySetFocusControl(aValue);
end;

function TFresnelViewport.GetDPI(IsHorizontal: boolean): TFresnelLength;
begin
  Result:=FDPI[IsHorizontal];
end;

function TFresnelViewport.GetScrollbarWidth(IsHorizontal: boolean
  ): TFresnelLength;
begin
  Result:=FScrollbarWidth[IsHorizontal];
end;

function TFresnelViewport.GetHeight: TFresnelLength;
begin
  Result:=FHeight;
end;

function TFresnelViewport.GetVPLength(l: TFresnelViewportLength
  ): TFresnelLength;
begin
  Result:=0;
  case l of
    vlDPIHorizontal: Result:=FDPI[true];
    vlDPIVertical: Result:=FDPI[false];
    vlHorizontalScrollbarWidth: Result:=FScrollbarWidth[true];
    vlVerticalScrollbarWidth: Result:=FScrollbarWidth[false];
  end;
end;

function TFresnelViewport.GetWidth: TFresnelLength;
begin
  Result:=FWidth;
end;

procedure TFresnelViewport.SetDPI(IsHorizontal: boolean;
  const AValue: TFresnelLength);
begin
  if FDPI[IsHorizontal]=AValue then exit;
  FDPI[IsHorizontal]:=AValue;
  DomChanged;
end;

procedure TFresnelViewport.StylesheetChanged;
begin
  if FStylesheetStamp=High(FStylesheetStamp) then
    FStylesheetStamp:=low(FStylesheetStamp)
  else
    inc(FStylesheetStamp);
  DomChanged;
end;

function TFresnelViewport.TrySetFocusControl(aControl: TFresnelElement): Boolean;

var
  lOldElement : TFresnelElement;
  lEvt : TFresnelFocusEvent;

begin
  Result:=Assigned(aControl) and (aControl.HandleFocus and aControl.CanFocus);
  if not Result then
    exit;
  lEvt:=Nil;
  try
    if assigned(FFocusedElement) then
      begin
      // Blur does not bubble;
      lEvt:=TFresnelFocusEvent.Create(FFocusedElement,evtBlur);
      lEvt.Related:=aControl;
      FFocusedElement.DispatchEvent(lEvt);
      FreeAndNil(lEvt);
      // But focusout does...
      lEvt:=TFresnelFocusEvent.Create(FFocusedElement,evtFocusOut);
      lEvt.Related:=aControl;
      Bubble(FFocusedElement,lEvt);
      end;
    lOldElement:=FFocusedElement;
    FFocusedElement:=aControl;
    if assigned(FFocusedElement) then
      begin
      // Focus does not bubble;
      lEvt:=TFresnelFocusEvent.Create(FFocusedElement,evtFocus);
      lEvt.Related:=lOldElement;
      FFocusedElement.DispatchEvent(levt);
      FreeAndNil(lEvt);
      // But focusin does...
      lEvt:=TFresnelFocusEvent.Create(FFocusedElement,evtFocusIn);
      lEvt.Related:=lOldElement;
      Bubble(FFocusedElement,lEvt);
      end;
  finally
    lEvt.Free;
  end;
end;

procedure TFresnelViewport.FPOObservedChanged(ASender: TObject;
  Operation: TFPObservedOperation; Data: Pointer);
begin
  if ASender=FStylesheet then
    StylesheetChanged;
  inherited FPOObservedChanged(ASender,Operation,Data);
end;

procedure TFresnelViewport.InitResolver;
// ToDo: dont collect all types every time
var
  FoundStyles: array of TFresnelElementClass;

  procedure AddTypeStyle(ElClass: TFresnelElementClass);
  var
    i: Integer;
    Src, ParentSrc: TCSSString;
    ParentElClass: TFresnelElementClass;
  begin
    for i:=0 to length(FoundStyles)-1 do
      if FoundStyles[i]=ElClass then exit;
    Insert(ElClass,FoundStyles,length(FoundStyles));

    Src:=ElClass.GetCSSTypeStyle;
    if Src='' then exit;
    if ElClass.ClassType<>TFresnelElement then
    begin
      ParentElClass:=TFresnelElementClass(ElClass.ClassParent);
      AddTypeStyle(ParentElClass);
      ParentSrc:=ParentElClass.GetCSSTypeStyle;
      if Src=ParentSrc then exit;
    end;
    {$IFDEF VerboseCSSAttr}
    writeln('AddTypeStyle ',ElClass.ClassName,' Src="',Src,'"');
    {$ENDIF}
    FResolver.AddStyleSheet(cssoUserAgent,ElClass.ClassName,Src);
  end;

  procedure CollectTypeStyles(El: TFresnelElement);
  var
    ElClass: TFresnelElementClass;
    i: Integer;
  begin
    ElClass:=TFresnelElementClass(El.ClassType);
    AddTypeStyle(ElClass);
    for i:=0 to El.NodeCount-1 do
      CollectTypeStyles(El[i]);
  end;

begin
  Resolver.Init;

  CollectTypeStyles(Self);

  if FStylesheetResolverStamp<>FStylesheetStamp then
  begin
    FStylesheetResolverStamp:=FStylesheetStamp;
    UpdateResolverStylesheet;
  end;
end;

procedure TFresnelViewport.UpdateResolverStylesheet;
begin
  Resolver.AddStyleSheet(cssoAuthor,StylesheetName,Stylesheet.Text);
end;

procedure TFresnelViewport.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if AComponent=FLayouter then
      FLayouter:=nil
    else if AComponent=FResolver then
      FResolver:=nil
    else if AComponent=FViewPort then
      FViewPort:=nil;
  end;
end;

procedure TFresnelViewport.SetHeight(AValue: TFresnelLength);
begin
  if FHeight=AValue then exit;
  FHeight:=AValue;
  DomChanged;
end;

procedure TFresnelViewport.SetScrollbarWidth(IsHorizontal: boolean;
  const AValue: TFresnelLength);
begin
  if FScrollbarWidth[IsHorizontal]=AValue then exit;
  FScrollbarWidth[IsHorizontal]:=AValue;
end;

procedure TFresnelViewport.SetStylesheet(AValue: TStrings);
begin
  if AValue=nil then exit;
  if FStylesheet=AValue then Exit;
  if FStylesheet.Equals(AValue) then Exit;
  FStylesheet.Assign(AValue);
  StylesheetChanged;
end;

procedure TFresnelViewport.SetVPLength(l: TFresnelViewportLength;
  const AValue: TFresnelLength);
begin
  case l of
    vlDPIHorizontal: DPI[true]:=AValue;
    vlDPIVertical: DPI[false]:=AValue;
    vlHorizontalScrollbarWidth: ScrollbarWidth[true]:=AValue;
    vlVerticalScrollbarWidth: ScrollbarWidth[false]:=AValue;
  end;
end;

procedure TFresnelViewport.SetWidth(AValue: TFresnelLength);
begin
  if FWidth=AValue then exit;
  FWidth:=AValue;
  DomChanged;
end;

destructor TFresnelViewport.Destroy;
begin
  FreeAndNil(FStylesheet);
  FreeAndNil(FResolver);
  inherited Destroy;
end;

procedure TFresnelViewport.ApplyCSS;

  procedure TraverseCSS(El: TFresnelElement);
  var
    i: Integer;
    PE: TPseudoElement;
  begin
    //writeln('Traverse ',El.ClassName,' CSSTypeName=',El.CSSTypeName);
    if El<>Self then
    begin
      El.FResolver:=Resolver;
      El.FViewPort:=Self;
    end;
    El.ComputeInlineStyle;
    El.ComputeCSSValues;

    if fesPseudoElement in FStates then
      exit;
    for i:=0 to El.NodeCount-1 do
      TraverseCSS(El.Nodes[i]);
    for i:=0 to El.PseudoNodeCount-1 do
      TraverseCSS(El.PseudoNodes[i]);
  end;

  procedure TraverseLayoutFinished(El: TFresnelElement);
  var
    i: Integer;
  begin
    El.ComputeCSSLayoutFinished;
    if fesPseudoElement in FStates then
      exit;
    for i:=0 to El.NodeCount-1 do
      TraverseLayoutFinished(El.Nodes[i]);
    for i:=0 to El.PseudoNodeCount-1 do
      TraverseLayoutFinished(El.PseudoNodes[i]);
  end;

begin
  //writeln('TFresnelViewport.ApplyCSS ',Name,' Width=',FloatToCSSStr(Width),',Height=',FloatToCSSStr(Height));
  DomModified:=false;
  if NodeCount>1 then
    CSSWarning(20221018152912,'TFresnelViewport.ApplyCSS more than 1 root element: NodeCount='+IntToStr(NodeCount));
  ClearCSSValues;
  InitResolver;
  //writeln('TFresnelViewport.ApplyCSS ',StylesheetElements.ClassName);
  // compute CSS
  TraverseCSS(Self);
  // layout
  Layouter.Apply;
  // notify layout finished
  TraverseLayoutFinished(Self);
end;

procedure TFresnelViewport.DomChanged;
begin
  DomModified:=true;
  if Assigned(OnDomChanged) then
    OnDomChanged(Self);
end;

procedure TFresnelViewport.Disconnecting;
begin
  ViewportConnected:=false;
end;

function TFresnelViewport.AllocateFont(const Desc: TFresnelFontDesc
  ): IFresnelFont;
begin
  if FFontEngine<>nil then
    Result:=FFontEngine.Allocate(Desc)
  else
    raise EFresnelFont.Create('TFresnelViewport.AllocateFont no FontEngine');
end;

function TFresnelViewport.GetCSSString(AttrID: TCSSNumericalID; Compute: boolean; out
  Complete: boolean): string;
var
  Desc: TCSSAttributeDesc;
  Attr: TFresnelCSSAttribute;
begin
  if AttrID<CSSRegistry.AttributeCount then
  begin
    Desc:=CSSRegistry.Attributes[AttrID];
    if Desc is TFresnelCSSAttrDesc then
    begin
      // some values of the viewport are fixed and cannot be overridden by CSS
      Attr:=TFresnelCSSAttrDesc(Desc).Attr;
      Complete:=true;
      case Attr of
      fcaDisplay: Result:='block flow-root';
      fcaPosition: Result:='absolute';
      fcaBoxSizing: Result:='content-box';
      fcaZIndex: Result:='0';
      fcaWidth: Result:=FloatToCSSStr(Width)+'px';
      fcaHeight: Result:=FloatToCSSStr(Height)+'px';
      fcaFontSize: Result:=FloatToCSSStr(FresnelDefaultFontSize)+'px'; // todo use system font
      fcaFontWeight: Result:=FloatToCSSStr(FresnelFontWeightNormal); // todo use system font
      else
        Result:=inherited;
      end;
      exit;
    end;
  end;
  Result:=inherited;
end;

class function TFresnelViewport.CSSTypeID: TCSSNumericalID;
begin
  Result:=0;
end;

class function TFresnelViewport.CSSTypeName: TCSSString;
begin
  Result:='';
end;

class function TFresnelViewport.GetCSSTypeStyle: TCSSString;
begin
  Result:=':root {'
    +' display: block flow-root; position: absolute;'
    +' font-family: arial; font-size: 12px;'
    +' color: #000; background-color: #fff;'
    +' }';
end;

function TFresnelViewport.GetElementAt(const x, y: TFresnelLength
  ): TFresnelElement;

  function Check(Node: TFresnelLayoutNode; const dx, dy: TFresnelLength): TFresnelElement;
  var
    El: TFresnelElement;
    i: Integer;
    BorderBox: TFresnelRect;
    aContentOffset: TFresnelPoint;
  begin
    Result:=nil;
    if Node=nil then exit;
    El:=Node.Element;
    if not El.Rendered then exit;
    if Node.NodeCount>0 then begin
      aContentOffset:=El.RenderedContentBox.TopLeft;
      for i:=Node.NodeCount-1 downto 0 do
      begin
        Result:=Check(Node.Nodes[i],dx+aContentOffset.X,dy+aContentOffset.Y);
        if Result<>nil then exit;
      end;
    end;
    BorderBox:=El.RenderedBorderBox;
    if BorderBox.Contains(x-dx,y-dy) then
      Result:=El
    else
      Result:=nil;
  end;

begin
  Result:=Check(LayoutNode,0,0);
end;

function TFresnelViewport.GetElementsAt(const x, y: TFresnelLength
  ): TFresnelElementArray;

  procedure Check(Node: TFresnelLayoutNode; const dx, dy: TFresnelLength);
  var
    El: TFresnelElement;
    i: Integer;
    BorderBox: TFresnelRect;
    aContentOffset: TFresnelPoint;
  begin
    if Node=nil then exit;
    El:=Node.Element;
    if not El.Rendered then exit;
    if Node.NodeCount>0 then begin
      aContentOffset:=El.RenderedContentBox.TopLeft;
      for i:=Node.NodeCount-1 downto 0 do
        Check(Node.Nodes[i],dx+aContentOffset.X,dy+aContentOffset.Y);
    end;
    BorderBox:=El.RenderedBorderBox;
    if BorderBox.Contains(x-dx,y-dy) then
    begin
      System.Insert(El,Result,length(Result));
    end;
  end;

begin
  Result:=[];
  Check(LayoutNode,0,0);
end;

function TFresnelViewport.ContentToPagePos(El: TFresnelElement; const x,
  y: TFresnelLength): TFresnelPoint;
var
  Node: TFresnelLayoutNode;
  aContentOffset: TFresnelPoint;
begin
  Result.X:=x;
  Result.Y:=y;
  if not El.Rendered then exit;
  Node:=El.LayoutNode;

  repeat
    aContentOffset:=El.RenderedContentBox.TopLeft;
    Result.X+=aContentOffset.X;
    Result.Y+=aContentOffset.Y;
    if Node=nil then exit;
    Node:=Node.Parent;
    if Node=nil then exit;
    El:=Node.Element;
  until El=nil;
end;

function TFresnelViewport.ContentToPagePos(El: TFresnelElement; const p: TFresnelPoint
  ): TFresnelPoint;
begin
  Result:=ContentToPagePos(El,p.X,p.Y);
end;

function TFresnelViewport.PageToContentPos(El: TFresnelElement; const x,
  y: TFresnelLength): TFresnelPoint;
var
  p: TFresnelPoint;
begin
  p:=ContentToPagePos(El,0,0);
  Result.X:=x-p.X;
  Result.Y:=y-p.Y;
end;

function TFresnelViewport.PageToContentPos(El: TFresnelElement; const p: TFresnelPoint
  ): TFresnelPoint;
begin
  Result:=PageToContentPos(El,p.X,p.Y);
end;

procedure TFresnelViewport.WSMouseXY(WSData: TFresnelMouseEventInit; MouseEventId: TEventID);
var
  El, BubbleEl, OldMouseDownEl: TFresnelElement;
  MouseEvt: TFresnelMouseEvent;
  ClickEvt: TFresnelMouseClickEvent;
  NewHoverElements, OldHoverElements: TFresnelElementArray;
  NewPageXY, OldPageXY: TFresnelPoint;
  i: Integer;
  j: SizeInt;
begin
  if not (MouseEventId in [evtMouseDown,evtMouseUp,evtMouseMove,evtMouseEnter,evtMouseLeave]) then
    exit;

  NewPageXY:=WSData.PagePos;
  if evtMouseDown=MouseEventID then
    Writeln('Ah');
  NewHoverElements:=GetElementsAt(NewPageXY.X,NewPageXY.Y);
  OldHoverElements:=VPApplication.GetHoverElements;
  OldMouseDownEl:=VPApplication.GetMouseDownElement(OldPageXY);

  MouseEvt:=nil;
  try
    // send mouse leave events in stacking order top to bottom
    for i:=0 to length(OldHoverElements)-1 do
    begin
      El:=OldHoverElements[i];
      j:=length(NewHoverElements)-1;
      while (j>=0) and (El<>NewHoverElements[j]) do dec(j);
      if j>=0 then continue;
      // dispatch mouse leave event
      {$IFDEF VerboseFresnelMouse}
      writeln('TFresnelViewport.WSMouseXY LEAVE ',El.GetPath);
      {$ENDIF}
      MouseEvt:=El.EventDispatcher.CreateEvent(El,evtMouseLeave) as TFresnelMouseLeaveEvent;
      El.DispatchEvent(MouseEvt);
      FreeAndNil(MouseEvt);

      if El=OldMouseDownEl then
        VPApplication.SetMouseDownElement(nil,NewPageXY);
    end;

    if MouseEventId=evtMouseLeave then
    begin
      // dispatch mouse leave in viewport
      {$IFDEF VerboseFresnelMouse}
      writeln('TFresnelViewport.WSMouseXY LEAVE VIEWPORT');
      {$ENDIF}
      FreeAndNil(MouseEvt);
      MouseEvt:=EventDispatcher.CreateEvent(Self,evtMouseLeave) as TFresnelMouseLeaveEvent;
      DispatchEvent(MouseEvt);
    end;

    VPApplication.SetHoverElements(NewHoverElements);

    if MouseEventId=evtMouseEnter then
    begin
      // dispatch mouse enter in viewport
      {$IFDEF VerboseFresnelMouse}
      writeln('TFresnelViewport.WSMouseXY ENTER VIEWPORT');
      {$ENDIF}
      FreeAndNil(MouseEvt);
      MouseEvt:=EventDispatcher.CreateEvent(Self,evtMouseEnter) as TFresnelMouseEnterEvent;
      EventDispatcher.DispatchEvent(MouseEvt);
    end;

    // sent mouse enter events in stacking order bottom to top
    for i:=length(NewHoverElements)-1 downto 0 do
    begin
      El:=NewHoverElements[i];
      j:=length(OldHoverElements)-1;
      while (j>=0) and (El<>OldHoverElements[j]) do dec(j);
      if j>=0 then continue;
      // dispatch mouse enter event
      {$IFDEF VerboseFresnelMouse}
      writeln('TFresnelViewport.WSMouseXY ENTER ',El.GetPath);
      {$ENDIF}
      FreeAndNil(MouseEvt);
      MouseEvt:=El.EventDispatcher.CreateEvent(El,evtMouseEnter) as TFresnelMouseEnterEvent;
      El.DispatchEvent(MouseEvt);
    end;

  finally
    FreeAndNil(MouseEvt);
  end;

  if length(NewHoverElements)>0 then
  begin
    El:=NewHoverElements[0];
    WSData.ControlPos:=PageToContentPos(El,NewPageXY);
  end else begin
    El:=Self;
    WSData.ControlPos:=WSData.PagePos;
  end;

  case MouseEventId of
  evtMouseDown:
    VPApplication.SetMouseDownElement(El,NewPageXY);
  evtMouseLeave:
    VPApplication.SetMouseDownElement(nil,NewPageXY);
  end;

  if MouseEventId in [evtMouseDown,evtMouseUp,evtMouseMove] then
  begin
    MouseEvt:=nil;
    ClickEvt:=nil;
    try
      // dispatch mouse down/move/up events, in stacking order
      i:=0;
      MouseEvt:=El.EventDispatcher.CreateEvent(El,MouseEventId) as TFresnelMouseEvent;
      MouseEvt.InitEvent(WSData);

      BubbleEl:=El;
      repeat
        if MouseEvt is TBubbleMouseDownEvent then
          TBubbleMouseDownEvent(MouseEvt).FElement:=BubbleEl
        else if MouseEvt is TBubbleMouseUpEvent then
          TBubbleMouseUpEvent(MouseEvt).FElement:=BubbleEl
        else if MouseEvt is TBubbleMouseMoveEvent then
          TBubbleMouseMoveEvent(MouseEvt).FElement:=BubbleEl;

        {$IFDEF VerboseFresnelMouse}
        writeln('TFresnelViewport.WSMouseXY ',MouseEventId,' ',BubbleEl.GetPath,' ',FloatToCSSStr(MouseEvt.X),',',FloatToCSSStr(MouseEvt.Y));
        {$ENDIF}
        BubbleEl.DispatchEvent(MouseEvt);
        inc(i);
        if (MouseEvt.PropagationStopped) or (i>=length(NewHoverElements)) then
          break;
        BubbleEl:=NewHoverElements[i];
        if MouseEvt is TBubbleMouseDownEvent then
          TBubbleMouseDownEvent(MouseEvt).SetXY(PageToContentPos(BubbleEl,NewPageXY))
        else if MouseEvt is TBubbleMouseUpEvent then
          TBubbleMouseUpEvent(MouseEvt).SetXY(PageToContentPos(BubbleEl,NewPageXY))
        else if MouseEvt is TBubbleMouseMoveEvent then
          TBubbleMouseMoveEvent(MouseEvt).SetXY(PageToContentPos(BubbleEl,NewPageXY))
        else
          break;
      until false;

      FreeAndNil(MouseEvt);


      if (MouseEventId=evtMouseUp) and (VPApplication.GetMouseDownElement(OldPageXY)=El) then
      begin
        // dispatch click event up the element hierarchy
        ClickEvt:=El.EventDispatcher.CreateEvent(El,evtClick) as TFresnelMouseClickEvent;
        Bubble(El,ClickEvt);
        VPApplication.SetMouseDownElement(nil,NewPageXY);
      end;

      // Focus gets set on mouse down. TrySetFocusControl will test whether the element actually can get focus.
      // Todo: Test what if a non-focus handling element is positioned over one which does handle focus ?
      // -> go over hover list ?
      if (MouseEventId=evtMouseDown) and Assigned(El) then
        TrySetFocusControl(El);
    finally
      MouseEvt.Free;
      ClickEvt.Free;
    end;
  end;
end;

procedure TFresnelViewport.Bubble(lElement : TFresnelElement; lEvt : TFresnelEvent);

begin
  While lElement<>Nil do
    begin
    lEvt.Sender:=lElement;
    lElement.DispatchEvent(lEvt);
    if lEvt.PropagationStopped then
      lElement:=Nil
    else
      lElement:=lElement.Parent;
    end;
end;

function TFresnelViewport.WSKey(WSData: TFresnelKeyEventInit; KeyEventId: TEventID): boolean;

var
  lFocus : TFresnelElement;
  lEvtClass : TAbstractEventClass;
  lKeyEvtClass : TFresnelKeyEventClass absolute lEvtClass;
  lEvt : TFresnelKeyEvent;

begin
  lFocus:=FFocusedElement;
  if not Assigned(lFocus) then
    lFocus:=Self;
  lEvtClass:=EventDispatcher.Registry.FindEventClass(KeyEventID);
  if not lEvtClass.InheritsFrom(TFresnelKeyEvent) then
     Raise EFresnel.CreateFmt('%s is not a descendent of class TFresnelKeyEvent',[lEvtClass.ClassName]);
  lEvt:=lKeyEvtClass.Create(WSData);
  try
    Bubble(lFocus,lEvt);
    Result:=lEvt.DefaultPrevented;
  finally
    lEvt.Free;
  end;
end;

function TFresnelViewport.WSInput(WSData: TFresnelInputEventInit): boolean;
var
  lFocus : TFresnelElement;
  lEvt : TFresnelInputEvent;


begin
  lFocus:=FFocusedElement;
  if not Assigned(lFocus) then
    lFocus:=Self;
  lEvt:=TFresnelInputEvent.Create(WSData);
  try
    Bubble(lFocus,lEvt);
    Result:=lEvt.DefaultPrevented;
  finally
    lEvt.Free;
  end;
end;

constructor TFresnelViewport.Create(AOwner: TComponent);

  Procedure R(aClass: TFresnelEventClass);
  begin
    TFresnelEventDispatcher.FresnelRegistry.RegisterEventWithID(aClass.FresnelEventID,aClass);
  end;

begin
  inherited Create(AOwner);
  FViewPort:=Self;
  FWidth:=800;
  FHeight:=600;
  FDPI[false]:=FresnelDefaultDPI;
  FDPI[true]:=FresnelDefaultDPI;
  FScrollbarWidth[False]:=12;
  FScrollbarWidth[true]:=12;

  FResolver:=TCSSResolver.Create(nil);
  FResolver.OnLog:=@CSSResolverLog;
  FResolver.CSSRegistry:=CSSRegistry;

  FStylesheet:=TStringList.Create{$IF FPC_FULLVERSION>=30301}(false){$ENDIF};
  FStylesheet.FPOAttachObserver(Self);

  if not FFresnelEventsRegistered then
  begin
    FFresnelEventsRegistered:=true;
    TFresnelEventDispatcher.RegisterFresnelEvents;
    R(TBubbleMouseClickEvent);
    R(TBubbleMouseDoubleClickEvent);
    R(TBubbleMouseDownEvent);
    R(TBubbleMouseUpEvent);
    R(TBubbleMouseMoveEvent);
    R(TFresnelMouseLeaveEvent);
    R(TFresnelMouseEnterEvent);
  end;
end;

{ TFresnelViewport.TBubbleMouseClickEvent }

procedure TFresnelViewport.TBubbleMouseClickEvent.SetXY(const XY: TFresnelPoint);
begin
  FInit.ControlPos:=XY;
end;

{ TFresnelViewport.TBubbleMouseDoubleClickEvent }

procedure TFresnelViewport.TBubbleMouseDoubleClickEvent.SetXY(const XY: TFresnelPoint);
begin
  FInit.ControlPos:=XY;
end;

{ TFresnelViewport.TBubbleMouseDownEvent }

procedure TFresnelViewport.TBubbleMouseDownEvent.SetXY(const XY: TFresnelPoint);
begin
  FInit.ControlPos:=XY;
end;

{ TFresnelViewport.TBubbleMouseMoveEvent }

procedure TFresnelViewport.TBubbleMouseMoveEvent.SetXY(const XY: TFresnelPoint);
begin
  FInit.ControlPos:=XY;
end;

{ TFresnelViewport.TBubbleMouseUpEvent }

procedure TFresnelViewport.TBubbleMouseUpEvent.SetXY(const XY: TFresnelPoint);
begin
  FInit.ControlPos:=XY;
end;

{ TFresnelElement }

function TFresnelElement.GetCSSPseudoClass(Pseudo: TFresnelCSSPseudoClass
  ): boolean;
begin
  Result:=false;
  case Pseudo of
    fcpcHover: Result:=fesHover in FStates;
    fcpcFocus: Result:=IsFocused;
  end;
end;

procedure TFresnelElement.SetCSSPseudoClass(Pseudo: TFresnelCSSPseudoClass;
  const AValue: boolean);
begin
  case Pseudo of
    fcpcHover:
      begin
        if fesHover in FStates=AValue then exit;
        if AValue then
          Include(FStates,fesHover)
        else
          Exclude(FStates,fesHover);
      end;
    fcpcFocus:
      begin
        if IsFocused then exit;
        if aValue then
          Focus;
      end;
  end;
  DomChanged;
end;

class constructor TFresnelElement.InitFresnelElementClass;
begin

end;

class destructor TFresnelElement.FinalFresnelElementClass;
begin

end;

function TFresnelElement.GetNodeCount: integer;
begin
  Result:=FChildren.Count;
end;

function TFresnelElement.GetViewportConnected: boolean;
begin
  Result:=fesViewportConnected in FStates;
end;

function TFresnelElement.GetEventHandler(AIndex: Integer): TFresnelEventHandler;
begin
  Result:=nil;
  If Assigned(FStandardEvents[aIndex]) then
    Result:=TObjectEventHandlerItem(FStandardEvents[aIndex]).EventHandler;
end;

function TFresnelElement.GetFocusEventHandler(AIndex: Integer): TFresnelFocusEventHandler;
begin
  Result:=nil;
  if AIndex in evtAllMouse then
    Result:=TFresnelFocusEventHandler(GetEventHandler(AIndex));
end;

function TFresnelElement.GetMouseEventHandler(AIndex: Integer
  ): TFresnelMouseEventHandler;
begin
  Result:=nil;
  if AIndex in evtAllMouse then
    Result:=TFresnelMouseEventHandler(GetEventHandler(AIndex));
end;

procedure TFresnelElement.SetEventHandler(AIndex: Integer;
  const AValue: TFresnelEventHandler);
begin
  If Assigned(FStandardEvents[aIndex]) then
    TObjectEventHandlerItem(FStandardEvents[aIndex]).EventHandler:=aValue
  else
    FStandardEvents[aIndex]:=FEventDispatcher.RegisterHandler(aValue,aIndex);
end;

procedure TFresnelElement.SetFocusEventHandler(AIndex: Integer; AValue: TFresnelFocusEventHandler);
begin
  if AIndex in evtAllFocus then
    SetEventHandler(AIndex,TFresnelEventHandler(AValue));
end;

procedure TFresnelElement.SetMouseEventHandler(AIndex: Integer;
  const AValue: TFresnelMouseEventHandler);
begin
  if AIndex in evtAllMouse then
    SetEventHandler(AIndex,TFresnelEventHandler(AValue));
end;

procedure TFresnelElement.SetViewportConnected(AValue: boolean);
var
  i: Integer;
begin
  if AValue then
  begin
    if fesViewportConnected in FStates then exit;
    // a resource is used -> mark this and all parents
    Include(FStates,fesViewportConnected);
    if Parent<>nil then
      Parent.ViewportConnected:=true;
  end else begin
    if not (fesViewportConnected in FStates) then exit;
    // viewport disconnected -> free resources of this and all children
    Exclude(FStates,fesViewportConnected);
    FFont:=nil;
    for i:=0 to NodeCount-1 do
      Nodes[i].ViewportConnected:=false;
    for i:=0 to PseudoNodeCount-1 do
      PseudoNodes[i].ViewportConnected:=false;
  end;
end;

procedure TFresnelElement.SetCSSClasses(const AValue: TStrings);
begin
  if FCSSClasses=AValue then Exit;
  if AValue=nil then
  begin
    if FCSSClasses.Count=0 then exit;
    FCSSClasses.Clear;
  end else begin
    if FCSSClasses.Equals(AValue) then exit;
    FCSSClasses.Assign(AValue);
    FCSSClasses.Delimiter:=' ';
  end;
  DomChanged;
end;

function TFresnelElement.GetNodes(Index: integer): TFresnelElement;
begin
  Result:=TFresnelElement(FChildren[Index]);
end;

function TFresnelElement.GetPeudoNodeCount: integer;
begin
  Result:=FPseudoChildren.Count;
end;

function TFresnelElement.GetPseudoNodes(Index: integer): TPseudoElement;
begin
  Result:=TPseudoElement(FPseudoChildren[Index]);
end;

procedure TFresnelElement.SetParent(const AValue: TFresnelElement);
begin
  if FParent=AValue then Exit;
  if AValue=Self then
    raise Exception.Create('cycle');

  if FParent<>nil then
  begin
    ViewportConnected:=false;
    FResolver:=nil;
    FViewPort:=nil;
    if fesPseudoElement in FStates then
      Parent.FPseudoChildren.Remove(Self)
    else begin
      Parent.FChildren.Remove(Self);
      Parent.DomChanged;
    end;
  end;
  FParent:=AValue;
  if FParent<>nil then
  begin
    if fesPseudoElement in FStates then
      Parent.FPseudoChildren.Add(Self)
    else
      Parent.FChildren.Add(Self);
    FreeNotification(FParent);
    FResolver:=Parent.Resolver;
    FViewPort:=Parent.FViewPort;
    if fesPseudoElement in FStates then
      Parent.DomChanged;
  end;
end;

procedure TFresnelElement.SetStyle(AValue: string);
begin
  AValue:=Trim(AValue);
  if FStyle=AValue then Exit;
  FStyle:=AValue;
  Include(FStates,fesStyleChanged);
  //debugln(['TFresnelElement.SetStyle ',DbgSName(Self),' ',Style]);
  DomChanged;
end;

procedure TFresnelElement.ComputeInlineStyle;
begin
  if not (fesStyleChanged in FStates) then exit;
  Exclude(FStates,fesStyleChanged);
  FreeAndNil(FStyleElement);
  if FStyle<>'' then
    FStyleElement:=Resolver.ParseInlineStyle(FStyle);
end;

procedure TFresnelElement.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if AComponent=Self then exit;
  if Operation=opRemove then
  begin
    if FParent=AComponent then
      FParent:=nil;
    if FChildren<>nil then
      FChildren.Remove(AComponent);
    if FPseudoChildren<>nil then
      FPseudoChildren.Remove(AComponent);
  end;
end;

function TFresnelElement.CSSReadNextValue(const aValue: string; var p: integer
  ): string;
var
  l: SizeInt;

  function SkipApostroph: boolean;
  begin
    inc(p);
    while (p<=l) and (aValue[p]<>'"') do inc(p);
    if p>l then
      exit(false); // missing apostroph
    inc(p);
    Result:=true;
  end;

  function SkipRoundBrackets: boolean;
  var
    Lvl: Integer;
  begin
    Result:=false;
    inc(p);
    Lvl:=1;
    while (p<=l) do
    begin
      case aValue[p] of
      '(': inc(Lvl);
      ')':
        begin
          dec(Lvl);
          if Lvl=0 then exit(true);
        end;
      '"':
        if not SkipApostroph then exit;
      end;
      inc(p);
    end;
  end;

var
  StartP: Integer;
begin
  Result:='';
  l:=length(aValue);
  while (p<=l) and (aValue[p] in [' ',#9,#10,#13]) do inc(p);
  if p>l then exit;
  StartP:=p;
  case aValue[p] of
  ',',';',')','{','}':
    begin
      Result:=aValue[p];
      inc(p);
      exit;
    end;
  '(':
    if not SkipRoundBrackets then exit;
  '"':
    if not SkipApostroph then exit;
  else
    while (p<=l) do
    begin
      case aValue[p] of
      ' ',#9,#10,#13,',',';',')','{','}': break;
      '(':
        if not SkipRoundBrackets then exit;
      '"':
        if not SkipApostroph then exit;
      end;
      inc(p);
    end;
  end;
  Result:=copy(aValue,StartP,p-StartP);
end;

function TFresnelElement.CSSReadNextToken(const aValue: string; var p: integer
  ): string;
var
  l: SizeInt;
  StartP: Integer;
begin
  Result:='';
  l:=length(aValue);
  while (p<=l) and (aValue[p] in [' ',#9,#10,#13]) do inc(p);
  if p>l then exit;
  StartP:=p;
  case aValue[p] of
  ',',';','(',')','{','}':
    begin
      Result:=aValue[p];
      inc(p);
      exit;
    end;
  '"':
    begin
      inc(p);
      while (p<=l) and (aValue[p]<>'"') do inc(p);
      if p>l then
        exit; // missing apostroph
      inc(p);
    end;
  else
    while (p<=l) do
    begin
      case aValue[p] of
      ' ',#9,#10,#13,',',';','(',')','"','{','}': break;
      end;
      inc(p);
    end;
  end;
  Result:=copy(aValue,StartP,p-StartP);
end;

procedure TFresnelElement.CSSWarning(const ID: int64; Msg: string);
begin
  //if FCSSPosElement<>nil then
  //  Msg:=FCSSPosElement.SourceFileName+'('+IntToStr(FCSSPosElement.SourceRow)+','+IntToStr(FCSSPosElement.SourceCol)+') '+Msg;
  Msg:=Msg+'['+IntToStr(ID)+'] '+Msg;
  DoLog(etError,'TFresnelElement.CSSError '+Msg);
end;

procedure TFresnelElement.CSSInvalidValueWarning(const ID: int64;
  Attr: TFresnelCSSAttribute; const aValue: string);
begin
  CSSWarning(ID,'invalid '+FresnelCSSAttributeNames[Attr]+' value "'+AValue+'"');
end;

function TFresnelElement.GetStyleAttr(const AttrName: string): string;
var
  Found: Boolean;
  l: Integer;
  aComp: TCSSResCompValue;
  NameP, StartP, EndP: PChar;
begin
  Result:='';
  if not IsValidCSSAttributeName(AttrName) then exit;
  NameP:=PChar(AttrName);

  aComp.EndP:=PChar(FStyle);
  repeat
    // read attribute name
    if not TCSSResolver.ReadValue(aComp) then exit;
    if aComp.Kind<>rvkKeywordUnknown then exit;
    l:=aComp.EndP-aComp.StartP;
    Found:=(l=length(AttrName)) and CompareMem(aComp.StartP,NameP,l);
    // read colon
    if not TCSSResolver.ReadValue(aComp) then exit;
    if aComp.StartP^<>':' then exit;
    // read value
    StartP:=nil;
    EndP:=nil;
    repeat
      if not TCSSResolver.ReadValue(aComp)  then break;
      if aComp.StartP^=';'  then break;
      if Found and (StartP=nil) then
        StartP:=aComp.StartP;
      EndP:=aComp.EndP;
    until false;
    if Found then
    begin
      if (EndP<>nil) and (EndP>StartP) then
      begin
        l:=EndP-StartP;
        SetLength(Result,l);
        System.Move(StartP^,Result[1],l);
      end;
      exit;
    end;
  until false;
end;

function TFresnelElement.SetStyleAttr(const AttrName, aValue: string): boolean;
const
  WhiteSpace = [' ',#9,#10,#13];
var
  l: Integer;
  NameP: PChar;
  Found, ValueEmpty: Boolean;
  aComp: TCSSResCompValue;
  StartP, EndP, p: PChar;
  NameStartP: PCSSChar;
begin
  Result:=false;
  if not IsValidCSSAttributeName(AttrName) then exit;
  NameP:=PChar(AttrName);

  // check if aValue is valid
  aComp.EndP:=PChar(aValue);
  ValueEmpty:=true;
  repeat
    // read attribute name
    if not TCSSResolver.ReadValue(aComp) then
    begin
      if aComp.Kind=rvkInvalid then
        exit; // syntax error in aValue
      break;
    end else
      ValueEmpty:=false;
    if aComp.StartP^ in [':',')','}',']',';'] then
      exit; // invalid token in aValue
  until false;

  // search old value
  aComp.EndP:=PChar(FStyle);
  repeat
    // read attribute name
    if not TCSSResolver.ReadValue(aComp) then break;
    if not (aComp.Kind in [rvkKeyword,rvkKeywordUnknown]) then
      exit; // syntax error in Style
    NameStartP:=aComp.StartP;
    l:=aComp.EndP-aComp.StartP;
    Found:=(l=length(AttrName)) and CompareMem(aComp.StartP,NameP,l);
    // read colon
    if not TCSSResolver.ReadValue(aComp) then
      exit; // syntax error in Style
    if aComp.StartP^<>':' then exit;
    // read value
    StartP:=nil;
    EndP:=nil;
    repeat
      if not TCSSResolver.ReadValue(aComp) then break;
      if (aComp.StartP^=';') then
      begin
        if Found then
          if ValueEmpty then
            EndP:=aComp.EndP // delete including semicolon
          else
            EndP:=aComp.StartP; // replace excluding semicolon
        break;
      end;
      if Found and (StartP=nil) then
        StartP:=aComp.StartP;
      EndP:=aComp.EndP;
    until false;
    if Found then
    begin
      p:=PChar(FStyle);
      while EndP^ in WhiteSpace do inc(EndP);

      if ValueEmpty then
      begin
        // unset / delete
        while (NameStartP>p) and (NameStartP[-1] in WhiteSpace) do
          dec(NameStartP);

        if EndP=nil then
          FStyle:=LeftStr(FStyle,NameStartP-p)
        else
          System.Delete(FStyle,NameStartP-p+1,EndP-NameStartP);
      end else begin
        // replace
        if StartP=nil then
          StartP:=aComp.StartP;
        if EndP=nil then
          EndP:=aComp.EndP;
        while (StartP[-1] in WhiteSpace) do
          dec(StartP);

        l:=EndP-StartP;
        if (l=length(aValue)) and CompareMem(StartP,@aValue[1],l) then
          exit(true); // no change
        FStyle:=LeftStr(FStyle,StartP-p)+aValue+copy(FStyle,EndP-p+1,length(FStyle));
      end;
      Include(FStates,fesStyleChanged);
      DomChanged;
      exit(true);
    end;
  until false;

  // not found
  if ValueEmpty then
    exit(true);

  // append
  if (FStyle>'') then
  begin
    if FStyle[length(FStyle)]=';' then
      FStyle+=' '+AttrName+':'+aValue
    else
      FStyle+='; '+AttrName+':'+aValue
  end
  else
    FStyle:=AttrName+':'+aValue;
  Include(FStates,fesStyleChanged);
  DomChanged;
  Result:=true;
end;

procedure TFresnelElement.AddCSSClass(const aName: string);
begin
  if CSSClasses.IndexOf(aName)>=0 then exit;
  CSSClasses.Add(aName);
end;

procedure TFresnelElement.RemoveCSSClass(const aName: string);
var
  i: Integer;
begin
  i:=CSSClasses.IndexOf(aName);
  if i>=0 then
    CSSClasses.Delete(i);
end;

function TFresnelElement.GetDPI(IsHorizontal: boolean): TFresnelLength;
begin
  if Parent<>nil then
    Result:=Parent.GetDPI(IsHorizontal)
  else
    Result:=FresnelDefaultDPI;
end;

function TFresnelElement.GetCSSString(AttrID: TCSSNumericalID; Compute: boolean; out
  Complete: boolean): string;
var
  AttrDesc: TCSSAttributeDesc;

  procedure UseInitialValue;
  var
    aState: TCSSAttributeValue.TState;
  begin
    Complete:=false;
    Result:=AttrDesc.InitialValue;
    if Compute then
    begin
      aState:=ComputeAttribute(AttrDesc,Result);
      Complete:=aState=cavsComputed;
    end;
  end;

var
  i: Integer;
  aValue: TCSSAttributeValue;
  ElAttrDesc: TFresnelElementAttrDesc;
  aState: TCSSAttributeValue.TState;
begin
  Complete:=true;

  AttrDesc:=Resolver.GetAttributeDesc(AttrID);
  if AttrDesc=nil then
    exit('');

  if Compute and (AttrDesc is TFresnelElementAttrDesc) then
  begin
    ElAttrDesc:=TFresnelElementAttrDesc(AttrDesc);
    if Assigned(ElAttrDesc.OnAsString) then
    begin
      // shorthand
      Result:=ElAttrDesc.OnAsString(Self);
      exit;
    end;
  end;

  if FCSSValues<>nil then
  begin
    i:=FCSSValues.IndexOf(AttrID);
    if (i>=0) then
    begin
      aValue:=FCSSValues.Values[i];
      aState:=aValue.State;
      if aState<>cavsInvalid then
      begin
        // element has value
        Complete:=aState=cavsComputed;
        if Compute and not Complete then
        begin
          aState:=ComputeAttribute(AttrDesc,FCSSValues.Values[i].Value);
          aValue.State:=aState;
          Complete:=aState=cavsComputed;
        end;
        if aState<>cavsInvalid then
        begin
          Result:=aValue.Value;
          if Result>'' then exit;
        end;
      end;
    end;

    if AttrDesc.All then
    begin
      case FCSSValues.AllValue of
      CSSKeywordInherit:
        if Parent<>nil then
        begin
          Result:=Parent.GetCSSString(AttrID,true,Complete);
          exit;
        end;
      CSSKeywordInitial:
        begin
          UseInitialValue;
          exit;
        end;
      CSSKeywordUnset,
      CSSKeywordRevert,CSSKeywordRevertLayer:
        ; // for now: use default
      end;
    end;
  end;

  // use default
  if AttrDesc.Inherits and (Parent<>nil) then
    Result:=Parent.GetCSSString(AttrID,true,Complete)
  else
    UseInitialValue;
end;

function TFresnelElement.GetComputedLength(Attr: TFresnelCSSAttribute; UseNaNOnFail: boolean;
  NoChildren: boolean): TFresnelLength;
var
  AttrID: TCSSNumericalID;
begin
  AttrID:=CSSRegistry.FresnelAttrs[Attr].Index;
  Result:=GetComputedLength(AttrID,UseNaNOnFail,NoChildren);
end;

function TFresnelElement.GetComputedLength(AttrID: TCSSNumericalID; UseNaNOnFail: boolean;
  NoChildren: boolean): TFresnelLength;
var
  s: String;
  Complete: boolean;
  aComp: TCSSResCompValue;
begin
  if UseNaNOnFail then
    Result:=NaN
  else
    Result:=0;

  s:=GetCSSString(AttrID,not NoChildren,Complete);
  if s='' then exit;

  aComp.EndP:=PChar(s);
  if (not Resolver.ReadComp(aComp)) or (aComp.Kind<>rvkFloat) then
    exit;
  Result:=GetComputedLength(aComp,AttrID,UseNaNOnFail,NoChildren);
end;

function TFresnelElement.GetComputedLength(const aComp: TCSSResCompValue; AttrID: TCSSNumericalID;
  UseNaNOnFail: boolean; NoChildren: boolean): TFresnelLength;

  function ResolveSpecial(var aResult: TFresnelLength): boolean;
  var
    Desc: TCSSAttributeDesc;
    ElDesc: TFresnelElementAttrDesc;
  begin
    Result:=false;
    Desc:=CSSRegistry.Attributes[AttrID];
    if Desc is TFresnelElementAttrDesc then
    begin
      ElDesc:=TFresnelElementAttrDesc(Desc);
      if ElDesc.OnGetLength<>nil then
      begin
        aResult:=ElDesc.OnGetLength(aComp,Self,NoChildren);
        if not IsNan(aResult) then
          exit(true);
        if not UseNaNOnFail then
          aResult:=0;
      end;
    end;
  end;

var
  Desc: TCSSAttributeDesc;
  Horizontal: Boolean;
begin
  if UseNaNOnFail then
    Result:=NaN
  else
    Result:=0;
  if (aComp.Kind<>rvkFloat) then
  begin
    ResolveSpecial(Result);
    exit;
  end;
  case aComp.FloatUnit of
  cuNone,cu_px:
    exit(aComp.Float);
  cuPercent:
    begin
      if not ResolveSpecial(Result) then exit;
      Result:=aComp.Float*Result/100;
      exit;
    end;
  end;
  if not (aComp.FloatUnit in cuAllLengths) then
    exit;
  Desc:=CSSRegistry.Attributes[AttrID];
  Horizontal:=(Desc is TFresnelElementAttrDesc) and TFresnelElementAttrDesc(Desc).LengthHorizontal;
  Result:=aComp.Float*GetPixPerUnit(aComp.FloatUnit,Horizontal);
end;

function TFresnelElement.GetComputedString(Attr: TFresnelCSSAttribute): string;
var
  AttrDesc: TFresnelCSSAttrDesc;
begin
  AttrDesc:=CSSRegistry.FresnelAttrs[Attr];
  Result:=GetComputedCSSString(AttrDesc.Index);
end;

function TFresnelElement.GetComputedCSSString(AttrID: TCSSNumericalID): string;
var
  Complete: boolean;
begin
  Result:=GetCSSString(AttrID,true,Complete);
  if Complete then exit;
end;

function TFresnelElement.GetComputedCSSString(const AttrName: string): string;
var
  AttrID: TCSSNumericalID;
begin
  AttrID:=Resolver.GetAttributeID(AttrName);
  if AttrID<=0 then exit('');
  Result:=GetComputedCSSString(AttrID);
end;

function TFresnelElement.GetShorthandSpaceSeparated(AttrDesc: TCSSAttributeDesc): string;
var
  SubDesc: TCSSAttributeDesc;
  i: Integer;
  s: String;
begin
  Result:='';
  if AttrDesc=nil then exit;
  for i:=0 to length(AttrDesc.CompProps)-1 do
  begin
    SubDesc:=AttrDesc.CompProps[i];
    if SubDesc.CompProps<>nil then continue;
    s:=GetComputedCSSString(SubDesc.Index);
    if s='' then continue;
    if Result>'' then Result+=' ';
    Result+=s;
  end;
end;

function TFresnelElement.GetComputedBorderWidth(Attr: TFresnelCSSAttribute
  ): TFresnelLength;
var
  StyleAttr: TFresnelCSSAttribute;
  aStyle, s: String;
begin
  Result:=0.0;
  // check style, because without style the border-width becomes 0
  StyleAttr:=TFresnelCSSAttribute(ord(fcaBorderTopStyle)+ord(Attr)-ord(fcaBorderTopWidth));
  aStyle:=GetComputedString(StyleAttr);
  if (aStyle='') or (aStyle='none') then
    exit;
  s:=GetComputedString(Attr);
  case s of
  'thin': exit(0.5);
  'medium': exit(1);
  'thick': exit(2);
  end;
  // Note: border-top and border-bottom percentage use container's width
  Result:=ConvertCSSValueToPixel(true,s,false);
end;

procedure TFresnelElement.WriteComputedAttributes(Title: string);
var
  Attr: TFresnelCSSAttribute;
  CurValue, DefValue: String;
begin
  writeln('TFresnelElement.WriteComputedAttributes ',Title,' ',GetPath,'================');
  for Attr in TFresnelCSSAttribute do
  begin
    //writeln('TFresnelElement.WriteComputedAttributes ',Attr);
    CurValue:=GetComputedString(Attr);
    DefValue:=GetUnsetCSSString(Attr);
    if CurValue<>DefValue then
      writeln('  ',Attr,'="',CurValue,'"');
  end;
end;

function TFresnelElement.GetIntrinsicContentSize(aMode: TFresnelLayoutMode;
  aMaxWidth: TFresnelLength; aMaxHeight: TFresnelLength): TFresnelPoint;
begin
  Result:=LayoutNode.GetIntrinsicContentSize(aMode,aMaxWidth,aMaxHeight);
end;

function TFresnelElement.GetContainerContentWidth(UseNaNOnFail: boolean): TFresnelLength;
var
  El: TFresnelElement;
begin
  El:=LayoutNode.Container;
  if El<>nil then
  begin
    Result:=El.LayoutNode.Width;
    if not UseNaNOnFail and IsNan(Result) then Result:=0;
  end else if UseNaNOnFail then
    Result:=NaN
  else
    Result:=0;
end;

function TFresnelElement.GetContainerContentHeight(UseNaNOnFail: boolean): TFresnelLength;
var
  El: TFresnelElement;
begin
  El:=LayoutNode.Container;
  if El<>nil then
  begin
    Result:=El.LayoutNode.Height;
    if not UseNaNOnFail and IsNan(Result) then Result:=0;
  end else if UseNaNOnFail then
    Result:=NaN
  else
    Result:=0;
end;

function TFresnelElement.GetContainerOffset: TFresnelPoint;
var
  Container, El: TFresnelElement;
begin
  Result:=Default(TFresnelPoint);
  Container:=LayoutNode.Container;
  El:=Parent;
  while El<>Container do
  begin
    Result.X:=Result.X+El.UsedContentBox.Left;
    Result.Y:=Result.Y+El.UsedContentBox.Top;
    El:=El.Parent;
  end;
end;

function TFresnelElement.IsBlockFormattingContext: boolean;
// BFC
// contain internal and external floats
// surpress margin collapsing
begin
  if Parent=nil then
    exit(true);

  case ComputedPosition of
  CSSRegistry.kwAbsolute,
  CSSRegistry.kwFixed:
    exit(true);
  end;

  case ComputedDisplayOutside of
  CSSIDNone,
  CSSRegistry.kwNone: exit(false);
  CSSRegistry.kwBlock: exit(true);
  end;

  case ComputedDisplayInside of
  //CSSRegistry.kwTable,
  CSSRegistry.kwFlex,
  CSSRegistry.kwGrid,
  //CSSRegistry.kwRuby,
  CSSRegistry.kwFlowRoot:
    exit(true);
  end;

  if ComputedOverflowX in [CSSRegistry.kwVisible,CSSRegistry.kwClip] then
    exit(true);
  if ComputedOverflowY in [CSSRegistry.kwVisible,CSSRegistry.kwClip] then
    exit(true);

  if ComputedFloat<>CSSRegistry.kwNone then
    exit(true);

  Result:=false;
end;

function TFresnelElement.GetBorderBoxOnViewport: TFresnelRect;
var
  Node: TFresnelLayoutNode;
begin
  if (not Rendered) then
  begin
    Result.Clear;
    exit;
  end;
  Result:=RenderedBorderBox;
  Node:=LayoutNode.Parent;
  while Node<>nil do begin
    Result.Offset(Node.Element.RenderedContentBox.TopLeft);
    Node:=Node.Parent;
  end;
  //debugln(['TFresnelElement.GetBoundingClientRect ',Name,' Result=',Result.Left,',',Result.Top,',',Result.Right,',',Result.Bottom]);
end;

function TFresnelElement.GetComputedBorderRadius(Corner: TFresnelCSSCorner
  ): TFresnelPoint;
var
  aComp: TCSSResCompValue;

  function GetRadius(out Radius: TFresnelLength; IsHorizontal: boolean): boolean;
  begin
    Result:=false;
    Radius:=0;
    case aComp.Kind of
    rvkFloat:
      begin
        if aComp.FloatUnit=cu_px then
        begin
          Radius:=aComp.Float;
          exit(true);
        end else if aComp.FloatUnit=cuPercent then
        begin
          if IsHorizontal then
            Radius:=GetComputedLength(fcaWidth)
          else
            Radius:=GetComputedLength(fcaHeight);
          Radius:=aComp.Float*Radius/100;
          exit(true);
        end else if aComp.FloatUnit in cuAllAbsoluteLengths then
        begin
          Radius:=aComp.Float*GetPixPerUnit(aComp.FloatUnit,IsHorizontal);
          exit(true);
        end;
      end;
    else
      exit;
    end;
  end;

var
  Attr: TFresnelCSSAttribute;
  s: String;
  X, Y: TFresnelLength;
  Complete: boolean;
  AttrID: TCSSNumericalID;
begin
  // default is initial value 0 0
  Result.X:=0;
  Result.Y:=0;

  Attr:=TFresnelCSSAttribute(ord(fcaBorderTopLeftRadius)+ord(Corner));
  AttrID:=CSSRegistry.FresnelAttrs[Attr].Index;
  s:=GetCSSString(AttrID,false,Complete);
  aComp.EndP:=PChar(s);
  if not Resolver.ReadComp(aComp) then exit;
  if not GetRadius(X,true) then exit;
  if Resolver.ReadComp(aComp) and (aComp.Kind=rvkSymbol) and (aComp.StartP^='/') then
  begin
    if not Resolver.ReadComp(aComp) then exit;
    if not GetRadius(Y,false) then exit;
  end else
    Y:=X;
  Result.X:=X;
  Result.Y:=Y;
end;

function TFresnelElement.GetComputedColor(Attr: TFresnelCSSAttribute; const CurrentColor: TFPColor
  ): TFPColor;
var
  Complete: boolean;
  CurComp: TCSSResCompValue;
  AttrID, KW: TCSSNumericalID;
  s: String;
begin
  Result:=CurrentColor;
  AttrID:=CSSRegistry.FresnelAttrs[Attr].Index;
  s:=GetCSSString(AttrID,false,Complete);
  CurComp.EndP:=PChar(s);
  if not Resolver.ReadComp(CurComp) then exit;

  case CurComp.Kind of
  rvkKeyword:
    begin
      KW:=CurComp.KeywordID;
      if (KW>=CSSRegistry.kwFirstColor)
          and (KW<=CSSRegistry.kwLastColor)
      then begin
        //writeln('TFresnelElement.GetComputedColor ',s,' KW=',KW,' ',CSSRegistry.kwFirstColor,' ',CSSRegistry.kwLastColor, ' ',CSSRegistry.GetKeywordColor(KW));
        Result:=FPColor(CSSRegistry.GetKeywordColor(KW));
        exit;
      end;
      if KW=CSSRegistry.kwCurrentcolor then
        exit;
    end;
  rvkFunction:
    begin
      // todo: check for allowed functions
    end;
  rvkHexColor:
    begin
      if not CSSToFPColor(copy(s,CurComp.StartP-PChar(s),CurComp.EndP-CurComp.StartP),Result) then
        Result:=CurrentColor;
      exit;
    end;
  end;
end;

function TFresnelElement.GetComputedKeyword(Attr: TFresnelCSSAttribute;
  const AllowedKeywords: TCSSNumericalIDArray): TCSSNumericalID;
begin
  Result:=GetComputedCSSKeyword(CSSRegistry.FresnelAttrs[Attr].Index,AllowedKeywords);
end;

function TFresnelElement.GetComputedJustifyContent(out SubKW: TCSSNumericalID
  ): TCSSNumericalID;
var
  s: String;
begin
  s:=GetComputedString(fcaJustifyContent);
  Resolver.InitParseAttr(s);
  if not CSSRegistry.ReadJustifyContent(Resolver,false,Result,SubKW) then
  begin
    Result:=CSSRegistry.kwNormal;
    SubKW:=0;
  end;
end;

function TFresnelElement.GetComputedJustifyItems(out SubKW: TCSSNumericalID
  ): TCSSNumericalID;
var
  s: String;
begin
  s:=GetComputedString(fcaJustifyItems);
  Resolver.InitParseAttr(s);
  if not CSSRegistry.ReadJustifyItems(Resolver,false,Result,SubKW) then
  begin
    Result:=CSSRegistry.kwLegacy;
    SubKW:=0;
  end;
end;

function TFresnelElement.GetComputedJustifySelf(out SubKW: TCSSNumericalID
  ): TCSSNumericalID;
var
  s: String;
begin
  s:=GetComputedString(fcaJustifySelf);
  Resolver.InitParseAttr(s);
  if not CSSRegistry.ReadJustifySelf(Resolver,false,Result,SubKW) then
  begin
    Result:=CSSRegistry.kwAuto;
    SubKW:=0;
  end;
end;

function TFresnelElement.GetComputedAlignContent(out SubKW: TCSSNumericalID
  ): TCSSNumericalID;
var
  s: String;
begin
  s:=GetComputedString(fcaAlignContent);
  Resolver.InitParseAttr(s);
  if not CSSRegistry.ReadAlignContent(Resolver,false,Result,SubKW) then
  begin
    Result:=CSSRegistry.kwNormal;
    SubKW:=0;
  end;
end;

function TFresnelElement.GetComputedAlignItems(out SubKW: TCSSNumericalID
  ): TCSSNumericalID;
var
  s: String;
begin
  s:=GetComputedString(fcaAlignItems);
  Resolver.InitParseAttr(s);
  if not CSSRegistry.ReadAlignItems(Resolver,false,Result,SubKW) then
  begin
    Result:=CSSRegistry.kwNormal;
    SubKW:=0;
  end;
end;

function TFresnelElement.GetComputedAlignSelf(out SubKW: TCSSNumericalID
  ): TCSSNumericalID;
var
  s: String;
begin
  s:=GetComputedString(fcaAlignSelf);
  Resolver.InitParseAttr(s);
  if not CSSRegistry.ReadAlignSelf(Resolver,false,Result,SubKW) then
  begin
    Result:=CSSRegistry.kwAuto;
    SubKW:=0;
  end;
end;

function TFresnelElement.GetComputedBackgroundOrigin: TCSSNumericalID;
begin
  Result:=GetComputedCSSKeyword(CSSRegistry.FresnelAttrs[fcaBackgroundOrigin].Index,
                                CSSRegistry.Chk_BackgroundOrigin_KeywordIDs);
end;

procedure TFresnelElement.GetComputedMarginBlockStartEndAttr(out aStartAttr,
  aEndAttr: TFresnelCSSAttribute);
begin
  aStartAttr:=fcaMarginLeft;
  aEndAttr:=fcaMarginRight;
end;

procedure TFresnelElement.GetComputedMarginInlineStartEndAttr(out aStartAttr,
  aEndAttr: TFresnelCSSAttribute);
begin
  aStartAttr:=fcaMarginLeft;
  aEndAttr:=fcaMarginRight;
end;

function TFresnelElement.GetComputedCSSKeyword(AttrID: TCSSNumericalID;
  const AllowedKeywords: TCSSNumericalIDArray): TCSSNumericalID;
var
  aValue: String;
  aComp: TCSSResCompValue;
  Complete: boolean;
  AttrDesc: TCSSAttributeDesc;
begin
  Result:=CSSIDNone;
  aValue:=GetCSSString(AttrID,false,Complete);
  aComp.EndP:=PChar(aValue);
  repeat
    if not Resolver.ReadComp(aComp) then break;
    if (aComp.Kind=rvkKeyword) and Resolver.IsKeywordIn(aComp.KeywordID,AllowedKeywords) then
      exit(aComp.KeywordID);
  until false;
  // use initial value
  AttrDesc:=CSSRegistry.Attributes[AttrID];
  aValue:=AttrDesc.InitialValue;
  aComp.EndP:=PChar(aValue);
  if Resolver.ReadComp(aComp)
      and (aComp.Kind=rvkKeyword)
      and Resolver.IsKeywordIn(aComp.KeywordID,AllowedKeywords) then
    exit(aComp.KeywordID);
  // use default
  Result:=AllowedKeywords[0];
end;

function TFresnelElement.GetComputedTextShadow(out aOffsetX, aOffsetY,
  aRadius: TFresnelLength; out aColor: TFPColor): boolean;
// color offset-x offset-y blur-radius
// color can be at a later position
var
  p, i, NumberIndex: Integer;
  s, aValue: String;
  l: TFresnelLength;
  Complete: boolean;
  AttrID: TCSSNumericalID;
begin
  Result:=false;
  aColor:=colDkGray;
  aOffsetX:=0;
  aOffsetY:=0;
  aRadius:=0;
  AttrID:=CSSRegistry.FresnelAttrs[fcaTextShadow].Index;
  aValue:=GetCSSString(AttrID,false,Complete);
  if aValue='' then exit;
  p:=1;
  NumberIndex:=0;
  for i:=1 to 4 do
  begin
    s:=CSSReadNextValue(AValue,p);
    if s='' then
    begin
      if i=1 then
        exit; // empty
      break;
    end else if s[1] in ['-','+','.','0'..'9'] then
    begin
      inc(NumberIndex);
      if not CSSStrToFloat(s,l) then
        exit;
      case NumberIndex of
      1: aOffsetX:=l;
      2: aOffsetY:=l;
      3: aRadius:=l;
      else exit;
      end;
    end else begin
      if not CSSToFPColor(s,aColor) then
        exit;
    end;
  end;
  Result:=true;
end;

function TFresnelElement.GetComputedImage(Attr: TFresnelCSSAttribute
  ): TFresnelCSSBackgroundInfo;
var
  p: Integer;
  s, aValue: String;
begin
  Result:=nil;
  aValue:=GetComputedString(Attr);
  if aValue='' then exit;
  p:=1;
  s:=CSSReadNextToken(aValue,p);
  case s of
  'linear-gradient':
    Result:=GetComputedLinearGradient(copy(aValue,p,length(aValue)));
  end;
end;

function TFresnelElement.GetComputedLinearGradient(const LGParams: string
  ): TFresnelCSSLinearGradient;
// For example:
// linear-gradient(red, orange, yellow, green, blue);
// linear-gradient(red 0%, orange 25%, yellow 50%, green 75%, blue 100%);
// linear-gradient(red 10%, 30%, blue 90%);
// linear-gradient(red, orange 10% 30%, yellow 50% 70%, green 90%);
// linear-gradient(red 0%, orange 10% 30%, yellow 50% 70%, green 90% 100%);
// linear-gradient(45deg, blue, red)
// linear-gradient(to left top, blue, red)
// linear-gradient(in oklab, blue, red)
var
  p, ColorCnt, i: Integer;
  s: String;
  aColor: TFPColor;
begin
  Result:=TFresnelCSSLinearGradient.Create;
  p:=1;
  s:=CSSReadNextToken(LGParams,p);
  if s<>'(' then exit;
  ColorCnt:=0;
  repeat
    s:=CSSReadNextToken(LGParams,p);
    case s of
    ')':
      exit;
    // todo 'to'
    // todo angle
    // todo percentage, e.g. linear-gradient(red 10%, 30%, blue 90%);
    else
      if not CSSToFPColor(s,aColor) then
        break;
      inc(ColorCnt);
      SetLength(Result.Colors,ColorCnt);
      Result.Colors[ColorCnt-1].Color:=aColor;
      if ColorCnt=1 then
        Result.Colors[ColorCnt-1].Percentage:=0
      else
        Result.Colors[ColorCnt-1].Percentage:=-1;

      s:=CSSReadNextToken(LGParams,p);
      case s of
      ',':
        continue;
      // todo percentages and lengths
      ')':
        break;
      else
        break;
      end;
    end;
  until false;

  if ColorCnt=0 then
  begin
    // missing params
    FreeAndNil(Result);
    exit;
  end;

  if (ColorCnt>1) and (Result.Colors[ColorCnt-1].Percentage<0) then
    Result.Colors[ColorCnt-1].Percentage:=100;

  for i:=1 to ColorCnt-2 do
  begin
    // todo: percentage without color: mix colors from neighbours
    // todo: check for monoton percentages
    if Result.Colors[i].Percentage<0 then
    begin
      Result.Colors[i].Percentage:=TFresnelLength(i)/(ColorCnt-1);
    end;
  end;
  //for i:=0 to ColorCnt-1 do
  //  writeln('TFresnelElement.GetRenderedCSSLinearGradient ',GetPath,' ',i,' ',dbgs(Result.Colors[i].Color),' ',Result.Colors[i].Percentage);
end;

function TFresnelElement.GetComputedZIndex: integer;
var
  Complete: boolean;
  s: String;
  Code: integer;
begin
  Result:=0;
  s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaZIndex].Index,false,Complete);
  case s of
  '','auto': exit;
  else
    val(s,Result,Code);
    if Code=0 then ;
  end;
end;

function TFresnelElement.GetUnsetCSSString(Attr: TFresnelCSSAttribute): string;
var
  AttrDesc: TFresnelCSSAttrDesc;
begin
  AttrDesc:=CSSRegistry.FresnelAttrs[Attr];
  if AttrDesc.Inherits and (Parent<>nil) then
    Result:=Parent.GetComputedString(Attr)
  else
    Result:=AttrDesc.InitialValue;
end;

function TFresnelElement.GetPixPerUnit(anUnit: TCSSUnit; IsHorizontal: boolean): TFresnelLength;
begin
  Result:=1.0;
  case anUnit of
    cuNone: ;
    cu_px: ;
    cu_cm: Result:=Viewport.DPI[IsHorizontal]/2.54;
    cu_mm: Result:=Viewport.DPI[IsHorizontal]/25.4;
    cu_Q: Result:=Viewport.DPI[IsHorizontal]/101.6;
    cu_in: Result:=Viewport.DPI[IsHorizontal];
    cu_pt: Result:=Viewport.DPI[IsHorizontal]/72; // 1pt = 1/72 in
    cu_pc: Result:=Viewport.DPI[IsHorizontal]/6; // 1pc = 12 pt
    // relative to element's font
    cu_em: Result:=Font.GetSize; // todo
    cu_ex: Result:=Font.GetSize/2; // todo
    cu_cap: Result:=Font.GetSize; // todo
    cu_ch: Result:=Font.GetSize; // todo
    // relative to root's font
    cu_rem: Result:=Viewport.Font.GetSize; // todo
    cu_rex: Result:=Viewport.Font.GetSize/2; // todo
    cu_rcap: Result:=Viewport.Font.GetSize; // todo
    cu_rchh: Result:=Viewport.Font.GetSize; // todo
    // relative to default viewport size
    cu_vw,cu_svw,cu_lvw,cu_dvw: Result:=Viewport.Width/100;
    cu_vh,cu_svh,cu_lvh,cu_dvh: Result:=Viewport.Height/100;
    cu_vmax,cu_svmax,cu_lvmax,cu_dvmax:
      if Viewport.Width>Viewport.Height then
        Result:=Viewport.Width/100
      else
        Result:=Viewport.Height/100;
    cu_vmin,cu_svmin,cu_lvmin,cu_dvmin:
      if Viewport.Width<Viewport.Height then
        Result:=Viewport.Width/100
      else
        Result:=Viewport.Height/100;
  else
    raise Exception.Create('css unit not supported: '+CSSUnitNames[anUnit]);
  end;
end;

procedure TFresnelElement.SetComputedCSSString(Attr: TFresnelCSSAttribute; const Value: string);
var
  AttrDesc: TFresnelCSSAttrDesc;
begin
  {$IFDEF VerboseCSSAttr}
  writeln('TFresnelElement.SetComputedCSSString ',Name,' ',Attr,': ',Value);
  {$ENDIF}
  AttrDesc:=CSSRegistry.FresnelAttrs[Attr];
  if FCSSValues=nil then
    FCSSValues:=TCSSAttributeValues.Create;
  FCSSValues.SetComputedValue(AttrDesc.Index,Value);
end;

function TFresnelElement.AddEventListener(aID: TEventID; aHandler: TFresnelEventHandler): Integer;
begin
  Result:=EventDispatcher.RegisterHandler(aHandler,aID).ID;
end;

function TFresnelElement.AddEventListener(const aName: TEventName; aHandler: TFresnelEventHandler) : Integer;
begin
   Result:=EventDispatcher.RegisterHandler(aHandler,aName).ID;
end;

function TFresnelElement.DispatchEvent(aEvent: TAbstractEvent): Integer;
begin
  Result:=DoDispatchEvent(aEvent);
end;

function TFresnelElement.GetFont: IFresnelFont;

  function GetViewportFontSize: TFresnelLength;
  var
    aFont: IFresnelFont;
  begin
    Result:=FresnelDefaultFontSize;
    aFont:=Viewport.GetFont;
    if aFont<>nil then
      Result:=aFont.GetSize;
  end;

  function GetParentFontSize: TFresnelLength;
  var
    ParentFont: IFresnelFont;
  begin
    Result:=FresnelDefaultFontSize;
    if Parent<>nil then
    begin
      ParentFont:=Parent.GetFont;
      if ParentFont<>nil then
        Result:=ParentFont.GetSize;
    end;
  end;

  function GetSmaller: TFresnelLength;
  var
    aRatio, aParentSize, aViewportSize: TFresnelLength;
  begin
    aParentSize:=GetParentFontSize;
    aViewportSize:=GetViewportFontSize;
    aRatio:=aParentSize / aViewportSize;
    if aRatio<=0.6 then
      Result:=aParentSize * 0.89
    else if aRatio<=0.75 then
      Result:=aParentSize * 0.6
    else if aRatio<=0.89 then
      Result:=aParentSize * 0.75
    else if aRatio<=1.0 then
      Result:=aParentSize * 0.89
    else if aRatio<=1.2 then
      Result:=aParentSize * 1.0
    else if aRatio<=1.5 then
      Result:=aParentSize * 1.2
    else if aRatio<=2 then
      Result:=aParentSize * 1.5
    else if aRatio<=3 then
      Result:=aParentSize * 2.0
    else if aRatio<=4.5 then
      Result:=aParentSize * 3.0
    else
      Result:=aParentSize * 4.5;
  end;

  function GetLarger: TFresnelLength;
  var
    aRatio, aParentSize, aViewportSize: TFresnelLength;
  begin
    aParentSize:=GetParentFontSize;
    aViewportSize:=GetViewportFontSize;
    aRatio:=aParentSize / aViewportSize;
    if aRatio>3.0 then
      Result:=aParentSize * 4.5
    else if aRatio>2.0 then
      Result:=aParentSize * 3.0
    else if aRatio>1.5 then
      Result:=aParentSize * 2.0
    else if aRatio>1.2 then
      Result:=aParentSize * 1.5
    else if aRatio>1.0 then
      Result:=aParentSize * 1.2
    else if aRatio>=0.89 then
      Result:=aParentSize * 1.0
    else if aRatio>=0.75 then
      Result:=aParentSize * 0.89
    else if aRatio>=0.6 then
      Result:=aParentSize * 0.75
    else
      Result:=aParentSize * 0.6;
  end;

var
  s: String;
  aComp: TCSSResCompValue;
  Complete: boolean;
begin
  if fesFontDescValid in FStates then
    exit(FFont);
  Include(FStates,fesFontDescValid);

  FFontDesc:=Default(TFresnelFontDesc);
  s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontFamily].Index,false,Complete);
  FFontDesc.Family:=s;
  s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontStyle].Index,false,Complete);
  FFontDesc.Style:=s;

  // font-kerning
  FFontDesc.Kerning:=fckAuto;
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontKerning].Index,false,Complete);
    aComp.EndP:=PChar(s);
    if Resolver.ReadComp(aComp) then
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwAuto: FFontDesc.Kerning:=fckAuto;
        CSSRegistry.kwNormal: FFontDesc.Kerning:=fckNormal;
        CSSRegistry.kwNone: FFontDesc.Kerning:=fckNone;
        end;
      end;
    end;
  end;

  // font-size
  FFontDesc.Size:=FresnelDefaultFontSize;
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontSize].Index,false,Complete);
    aComp.EndP:=PChar(s);
    if Resolver.ReadComp(aComp) then
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwXXSmall: FFontDesc.Size:=GetViewportFontSize*0.6;
        CSSRegistry.kwXSmall: FFontDesc.Size:=GetViewportFontSize*0.75;
        CSSRegistry.kwSmaller: FFontDesc.Size:=GetSmaller;
        CSSRegistry.kwSmall: FFontDesc.Size:=GetViewportFontSize*0.89;
        CSSRegistry.kwMedium: FFontDesc.Size:=GetViewportFontSize;
        CSSRegistry.kwLarge: FFontDesc.Size:=GetViewportFontSize*1.2;
        CSSRegistry.kwLarger: FFontDesc.Size:=GetLarger;
        CSSRegistry.kwXLarge: FFontDesc.Size:=GetViewportFontSize*2;
        CSSRegistry.kwXXLarge: FFontDesc.Size:=GetViewportFontSize*3;
        CSSRegistry.kwXXXLarge: FFontDesc.Size:=GetViewportFontSize*4.5;
        end;
      rvkFloat:
        if aComp.FloatUnit=cu_px then
          FFontDesc.Size:=aComp.Float
        else if aComp.FloatUnit in cuAllLengths then
        begin
          if Parent<>nil then
            FFontDesc.Size:=aComp.Float*Parent.GetPixPerUnit(aComp.FloatUnit,false);
        end else if aComp.FloatUnit=cuPercent then
        begin
          if Parent<>nil then
            FFontDesc.Size:=aComp.Float*Parent.Font.GetSize/100;
        end;
      end;
    end;
  end;
  if FFontDesc.Size<1 then
    FFontDesc.Size:=1;

  // font-width
  FFontDesc.Width:=1;
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontWidth].Index,false,Complete);
    aComp.EndP:=PChar(s);
    if Resolver.ReadComp(aComp) then
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwUltraCondensed: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnUltraCondensed];
        CSSRegistry.kwExtraCondensed: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnExtraCondensed];
        CSSRegistry.kwCondensed: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnCondensed];
        CSSRegistry.kwSemiCondensed: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnSemiCondensed];
        CSSRegistry.kwNormal: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnNormal];
        CSSRegistry.kwSemiExpanded: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnSemiExpanded];
        CSSRegistry.kwExpanded: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnExpanded];
        CSSRegistry.kwExtraExpanded: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnExtraExpanded];
        CSSRegistry.kwUltraExpanded: FFontDesc.Width:=CSSRegistry.FontWidths[fntwnUltraExpanded];
        end;
      rvkFloat:
        if (aComp.FloatUnit=cuPercent) and (aComp.Float>=0) then
          FFontDesc.Width:=aComp.Float/100;
      end;
    end;
  end;

  // font-weight
  FFontDesc.Weight:=FresnelFontWeightNormal;
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontWeight].Index,false,Complete);
    aComp.EndP:=PChar(s);
    if Resolver.ReadComp(aComp) then
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwNormal: FFontDesc.Weight:=FresnelFontWeightNormal;
        CSSRegistry.kwBold: FFontDesc.Weight:=700;
        CSSRegistry.kwLighter: FFontDesc.Weight:=300;
        CSSRegistry.kwBolder: FFontDesc.Weight:=500;
        end;
      rvkFloat:
        if (aComp.FloatUnit=cuNone) and (aComp.Float>=100) and (aComp.Float<=1000) then
          FFontDesc.Weight:=aComp.Float;
      end;
    end;
  end;

  // font-variant-alternates
  s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontVariantAlternates].Index,false,Complete);
  FFontDesc.Alternates:=s;

  // font-variant-caps
  FFontDesc.Caps:=ffvcNormal;
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontVariantCaps].Index,false,Complete);
    aComp.EndP:=PChar(s);
    if Resolver.ReadComp(aComp) then
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwNormal: FFontDesc.Caps:=ffvcSmallCaps;
        CSSRegistry.kwAllSmallCaps: FFontDesc.Caps:=ffvcAllSmallCaps;
        CSSRegistry.kwPetiteCaps: FFontDesc.Caps:=ffvcPetiteCaps;
        CSSRegistry.kwAllPetiteCaps: FFontDesc.Caps:=ffvcAllPetiteCaps;
        CSSRegistry.kwUnicase: FFontDesc.Caps:=ffvcUnicase;
        CSSRegistry.kwTitlingCaps: FFontDesc.Caps:=ffvcTitlingCaps;
        end;
      end;
    end;
  end;

  // font-variant-east-asian
  FFontDesc.EastAsians:=[];
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontVariantEastAsian].Index,false,Complete);
    aComp.EndP:=PChar(s);
    while Resolver.ReadComp(aComp) do
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwNormal: Include(FFontDesc.EastAsians,ffveaNormal);
        CSSRegistry.kwRuby: Include(FFontDesc.EastAsians,ffveaRuby);
        CSSRegistry.kwJis78: Include(FFontDesc.EastAsians,ffveaJis78);
        CSSRegistry.kwJis83: Include(FFontDesc.EastAsians,ffveaJis83);
        CSSRegistry.kwJis90: Include(FFontDesc.EastAsians,ffveaJis90);
        CSSRegistry.kwJis04: Include(FFontDesc.EastAsians,ffveaJis04);
        CSSRegistry.kwSimplified: Include(FFontDesc.EastAsians,ffveaSimplified);
        CSSRegistry.kwTraditional: Include(FFontDesc.EastAsians,ffveaTraditional);
        CSSRegistry.kwFullWidth: Include(FFontDesc.EastAsians,ffveaFullWidth);
        CSSRegistry.kwProportionalWidth: Include(FFontDesc.EastAsians,ffveaProportionalWidth);
        end;
      end;
    end;
  end;

  // font-variant-emoji
  FFontDesc.Emoji:=ffveNormal;
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontVariantEmoji].Index,false,Complete);
    aComp.EndP:=PChar(s);
    if Resolver.ReadComp(aComp) then
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwNormal: FFontDesc.Emoji:=ffveNormal;
        CSSRegistry.kwText: FFontDesc.Emoji:=ffveText;
        CSSRegistry.kwEmoji: FFontDesc.Emoji:=ffveEmoji;
        CSSRegistry.kwUnicode: FFontDesc.Emoji:=ffveUnicode;
        end;
      end;
    end;
  end;

  // font-variant-ligatures
  FFontDesc.Ligatures:=[];
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontVariantLigatures].Index,false,Complete);
    aComp.EndP:=PChar(s);
    while Resolver.ReadComp(aComp) do
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwNormal: Include(FFontDesc.Ligatures,ffvlNormal);
        CSSRegistry.kwNone: Include(FFontDesc.Ligatures,ffvlNone);
        CSSRegistry.kwCommonLigatures: Include(FFontDesc.Ligatures,ffvlCommonLigatures);
        CSSRegistry.kwNoCommonLigatures: Include(FFontDesc.Ligatures,ffvlNoCommonLigatures);
        CSSRegistry.kwDiscretionaryLigatures: Include(FFontDesc.Ligatures,ffvlDiscretionaryLigatures);
        CSSRegistry.kwNoDiscretionaryLigatures: Include(FFontDesc.Ligatures,ffvlNoDiscretionaryLigatures);
        CSSRegistry.kwHistoricalLigatures: Include(FFontDesc.Ligatures,ffvlHistoricalLigatures);
        CSSRegistry.kwNoHistoricalLigatures: Include(FFontDesc.Ligatures,ffvlNoHistoricalLigatures);
        CSSRegistry.kwContextual: Include(FFontDesc.Ligatures,ffvlContextual);
        CSSRegistry.kwNoContextual: Include(FFontDesc.Ligatures,ffvlNoContextual);
        end;
      end;
    end;
  end;

  // font-variant-numerics
  FFontDesc.Numerics:=[];
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontVariantNumeric].Index,false,Complete);
    aComp.EndP:=PChar(s);
    while Resolver.ReadComp(aComp) do
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwNormal: Include(FFontDesc.Numerics,ffvnNormal);
        CSSRegistry.kwOrdinal: Include(FFontDesc.Numerics,ffvnOrdinal);
        CSSRegistry.kwSlashedZero: Include(FFontDesc.Numerics,ffvnSlashedZero);
        CSSRegistry.kwLiningNums: Include(FFontDesc.Numerics,ffvnLiningNums);
        CSSRegistry.kwOldstyleNums: Include(FFontDesc.Numerics,ffvnOldstyleNums);
        CSSRegistry.kwProportionalNums: Include(FFontDesc.Numerics,ffvnProportionalNums);
        CSSRegistry.kwTabularNums: Include(FFontDesc.Numerics,ffvnTabularNums);
        CSSRegistry.kwDiagonalFractions: Include(FFontDesc.Numerics,ffvnDiagonalFractions);
        CSSRegistry.kwStackedFractions: Include(FFontDesc.Numerics,ffvnStackedFractions);
        end;
      end;
    end;
  end;

  // font-variant-position
  FFontDesc.Position:=ffvpNormal;
  if Parent<>nil then
  begin
    s:=GetCSSString(CSSRegistry.FresnelAttrs[fcaFontVariantPosition].Index,false,Complete);
    aComp.EndP:=PChar(s);
    if Resolver.ReadComp(aComp) then
    begin
      case aComp.Kind of
      rvkKeyword:
        case aComp.KeywordID of
        CSSRegistry.kwNormal: FFontDesc.Position:=ffvpNormal;
        CSSRegistry.kwSub: FFontDesc.Position:=ffvpSub;
        CSSRegistry.kwSuper: FFontDesc.Position:=ffvpSuper;
        end;
      end;
    end;
  end;

  // check if changed
  if FFont<>nil then
  begin
    if (FFont.GetFamily=FFontDesc.Family)
        and (FFont.GetKerning=FFontDesc.Kerning)
        and (FFont.GetSize=FFontDesc.Size)
        and (FFont.GetStyle=FFontDesc.Style)
        and (FFont.GetWeight=FFontDesc.Weight)
        and (FFont.GetWidth=FFontDesc.Width)
        and (FFont.GetAlternates=FFontDesc.Alternates)
        and (FFont.GetCaps=FFontDesc.Caps)
        and (FFont.GetEastAsians=FFontDesc.EastAsians)
        and (FFont.GetEmoji=FFontDesc.Emoji)
        and (FFont.GetLigatures=FFontDesc.Ligatures)
        and (FFont.GetNumerics=FFontDesc.Numerics)
        and (FFont.GetPosition=FFontDesc.Position) then
      exit(FFont); // still valid
    FFont:=nil;
  end;

  // allocate
  FFont:=ViewPort.AllocateFont(FFontDesc);
  Result:=FFont;
  if FFont<>nil then
    ViewportConnected:=true;
end;

procedure TFresnelElement.DomChanged;
begin
  if FParent<>nil then
    FParent.DomChanged;
end;

procedure TFresnelElement.Invalidate;
begin
  if LayoutNode=nil then exit;
  if LayoutNode.SkipRendering then exit;
  if (Viewport<>nil) and (Viewport<>Self) then
    Viewport.Invalidate;
end;

procedure TFresnelElement.InvalidateIfNotDrawing;
begin
  if (Viewport<>nil) and not Viewport.IsDrawing then
    Invalidate;
end;

function TFresnelElement.HasParent: Boolean;
begin
  Result:=Parent<>nil;
end;

procedure TFresnelElement.SetParentComponent(Value: TComponent);
var
  aStreamRoot: IFresnelStreamRoot;
begin
  //DoLog(etDebug,'TFresnelElement.SetParentComponent Self=%s NewParent=%s',[DbgSName(Self),DbgSName(Value)]);
  if Value=nil then
  begin
    Parent:=nil
  end
  else if Value is TFresnelElement then
  begin
    Parent:=TFresnelElement(Value)
  end
  else if Supports(Value,IFresnelStreamRoot,aStreamRoot) then
  begin
    //DoLog(etDebug,'TFresnelElement.SetParentComponent Self=%s redirecting to viewport',[ToString]);
    Parent:=aStreamRoot.GetViewport;
  end
  else
    raise EFresnel.CreateFmt('TFresnelElement.SetParentComponent Self=%s NewParentComponent=%s',[Name+':'+ClassName,Value.Name+':'+Value.ClassName]);
end;

procedure TFresnelElement.ChildDestroying(El: TFresnelElement);
begin
  if FParent<>nil then
    FParent.ChildDestroying(El);
end;

procedure TFresnelElement.DoRender(aRenderer: IFresnelRenderer);
begin
  //
end;

function TFresnelElement.DoDispatchEvent(aEvent: TAbstractEvent): Integer;
begin
  Result:=EventDispatcher.DispatchEvent(aEvent);
end;

procedure TFresnelElement.BeforeRender;
begin
  FRenderedBorderBox:=FUsedBorderBox;
  FRenderedContentBox:=FUsedContentBox;

  If Assigned(FBeforeRender) then
    FBeforeRender(Self);
end;

procedure TFresnelElement.AfterRender;
begin
  If Assigned(FAfterRender) then
    FAfterRender(Self);
end;

procedure TFresnelElement.Render(aRenderer: IFresnelRenderer);
begin
  DoRender(aRenderer);
end;

procedure TFresnelElement.GetChildren(Proc: TGetChildProc; Root: TComponent);
var
  i: Integer;
begin
  for i:=0 to NodeCount-1 do
    if Nodes[i].Owner=Root then
      Proc(Nodes[i]);

  if Root = Self then
    for i:=0 to ComponentCount-1 do
      if Components[i].GetParentComponent = nil then
        Proc(Components[i]);
end;

function TFresnelElement.GetParentComponent: TComponent;
begin
  Result:=Parent;
end;

constructor TFresnelElement.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FChildren:=TFPList.Create;
  FPseudoChildren:=TFPList.Create;
  FCSSClasses:=TStringList.Create;
  FCSSClasses.Delimiter:=' ';
  FCSSClasses.FPOAttachObserver(Self);
  FEventDispatcher:=TFresnelEventDispatcher.Create(Self);
end;

destructor TFresnelElement.Destroy;
var
  i: Integer;
begin
  Clear;
  for i:=PseudoNodeCount-1 downto 0 do
    PseudoNodes[i].Parent:=nil;
  FreeAndNil(FLayoutNode);
  FreeAndNil(FPseudoChildren);
  FreeAndNil(FChildren);
  FreeAndNil(FCSSValues);
  FreeAndNil(FCSSClasses);
  FreeAndNil(FEventDispatcher);
  if FParent<>nil then
    FParent.ChildDestroying(Self);

  inherited Destroy;
end;

procedure TFresnelElement.Clear;
var
  i: Integer;
begin
  FreeAndNil(FStyleElement);
  FreeAndNil(FCSSValues);
  FCSSClasses.Clear;
  for i:=NodeCount-1 downto 0 do
    Nodes[i].Parent:=nil;
  FChildren.Clear;
  // keep pseudo elements
end;

function TFresnelElement.GetRoot: TFresnelElement;
begin
  Result:=Self;
  while Result.Parent<>nil do
    Result:=Result.Parent;
end;

function TFresnelElement.GetPath: string;
begin
  if Parent<>nil then
    Result:=Parent.GetPath+'.'
  else
    Result:='';
  if Name='' then
    Result:=Result+ClassName
  else
    Result:=Result+Name;
end;

function TFresnelElement.AcceptChildrenAtDesignTime: boolean;
begin
  Result:=true;
end;

class function TFresnelElement.HandleFocus: Boolean;
begin
  Result:=False;
end;

function TFresnelElement.CanFocus: Boolean;
begin
  Result:=False;
end;

function TFresnelElement.IsFocused: Boolean;
var
  VP : TFresnelViewport;
begin
  VP:=Viewport;
  Result:=Assigned(VP) and (VP.FocusedElement=Self);
end;

function TFresnelElement.Focus: Boolean;
var
  VP : TFresnelViewport;
begin
  VP:=Viewport;
  Result:=Assigned(VP);
  if not Result then
    exit;
  Result:=VP.TrySetFocusControl(Self);
end;

procedure TFresnelElement.FPOObservedChanged(ASender: TObject;
  Operation: TFPObservedOperation; Data: Pointer);
begin
  if Operation=ooFree then exit;
  if Data=nil then ;
  if ASender=FCSSClasses then
    DomChanged;
end;

procedure TFresnelElement.UnsetValue(AttrID: TCSSNumericalID);
var
  i: Integer;
begin
  if FCSSValues=nil then exit;
  i:=FCSSValues.IndexOf(AttrID);
  if i<0 then exit;
  FCSSValues.Values[i].State:=cavsInvalid;
end;

function TFresnelElement.ConvertCSSValueToPixel(IsHorizontal: boolean; const Value: string;
  UseNaNOnFail: boolean): TFresnelLength;
var
  aComp: TCSSResCompValue;
begin
  if UseNaNOnFail then
    Result:=NaN
  else
    Result:=0;

  if Value='' then
    exit;
  aComp.EndP:=PChar(Value);
  if (not TCSSResolver.ReadValue(aComp)) or (aComp.Kind<>rvkFloat) or IsNan(aComp.Float) then
    exit;
  case aComp.FloatUnit of
  cuNone:
    if aComp.Float=0 then
    begin
      exit(aComp.Float);
    end else begin
      // number without unit is not allowed for pixel length
      exit;
    end;
  cu_px:
    exit(aComp.Float);
  end;
  if not (aComp.FloatUnit in cuAllLengths) then
    exit;
  Result:=aComp.Float*GetPixPerUnit(aComp.FloatUnit,IsHorizontal);
end;

procedure TFresnelElement.ComputeBaseAttributes;
var
  Complete: boolean;
  aValue: String;
  aComp: TCSSResCompValue;
  KW: TCSSNumericalID;
begin
  FComputedDisplayInside:=CSSIDNone;
  FComputedDisplayOutside:=CSSIDNone;
  FComputedDirection:=GetComputedKeyword(fcaDirection,CSSRegistry.Chk_Direction_KeywordIDs);
  FComputedWritingMode:=GetComputedKeyword(fcaWritingMode,CSSRegistry.Chk_WritingMode_KeywordIDs);
  FComputedVisibility:=CSSRegistry.kwHidden;

  FComputedBoxSizing:=CSSRegistry.kwContentBox;
  FComputedPosition:=CSSRegistry.kwStatic;
  FComputedOverflowX:=CSSRegistry.kwVisible;
  FComputedOverflowY:=CSSRegistry.kwVisible;
  FComputedFloat:=CSSRegistry.kwNone;

  if fesPseudoElement in FStates then exit;

  FComputedVisibility:=GetComputedKeyword(fcaVisibility,CSSRegistry.Chk_Visible_KeywordIDs);
  if FComputedVisibility=CSSRegistry.kwCollapse then
    exit;

  aValue:=GetCSSString(CSSRegistry.FresnelAttrs[fcaDisplay].Index,false,Complete);

  aComp.EndP:=PChar(aValue);
  repeat
    if not Resolver.ReadComp(aComp) then break;
    if aComp.Kind=rvkKeyword then
    begin
      KW:=aComp.KeywordID;
      case KW of
      CSSRegistry.kwNone:
        begin
          FComputedDisplayInside:=CSSIDNone;
          FComputedDisplayOutside:=CSSIDNone;
          FComputedVisibility:=CSSRegistry.kwCollapse;
          break;
        end;
      CSSRegistry.kwContents:
        begin
          FComputedDisplayInside:=CSSRegistry.kwContents;
          FComputedDisplayOutside:=CSSRegistry.kwContents;
          break;
        end;
      CSSRegistry.kwBlock,
      CSSRegistry.kwInline:
        if FComputedDisplayOutside=CSSIDNone then
          FComputedDisplayOutside:=KW;
      CSSRegistry.kwFlow,
      CSSRegistry.kwFlowRoot,
      CSSRegistry.kwGrid,
      CSSRegistry.kwFlex:
        if FComputedDisplayInside=CSSIDNone then
          FComputedDisplayInside:=KW;
      CSSRegistry.kwInlineBlock:
        begin
          // inline-block -> inline flow-root
          FComputedDisplayOutside:=CSSRegistry.kwInline;
          FComputedDisplayInside:=CSSRegistry.kwFlowRoot;
          break;
        end;
      CSSRegistry.kwInlineFlow:
        begin
          FComputedDisplayOutside:=CSSRegistry.kwInline;
          FComputedDisplayInside:=CSSRegistry.kwFlow;
          break;
        end;
      end;
    end;
  until false;
  if (FComputedDisplayOutside=CSSIDNone) and (FComputedDisplayInside>CSSIDNone) then
  begin
    case FComputedDisplayInside of
    CSSRegistry.kwGrid,
    CSSRegistry.kwFlex:
      FComputedDisplayOutside:=CSSRegistry.kwBlock;
    else
      FComputedDisplayOutside:=CSSRegistry.kwInline;
    end;
  end;
  if (FComputedDisplayInside=CSSIDNone) and (FComputedDisplayOutside>CSSIDNone) then
    FComputedDisplayInside:=CSSRegistry.kwFlow;

  if FComputedVisibility=CSSRegistry.kwCollapse then
    exit;
  FComputedBoxSizing:=GetComputedKeyword(fcaBoxSizing,CSSRegistry.Chk_BoxSizing_KeywordIDs);
  FComputedPosition:=GetComputedKeyword(fcaPosition,CSSRegistry.Chk_Position_KeywordIDs);
  FComputedFloat:=GetComputedKeyword(fcaFloat,CSSRegistry.Chk_Float_KeywordIDs);

  // overflow
  FComputedOverflowX:=GetComputedKeyword(fcaOverflowX,CSSRegistry.Chk_OverflowXY_KeywordIDs);
  FComputedOverflowY:=GetComputedKeyword(fcaOverflowY,CSSRegistry.Chk_OverflowXY_KeywordIDs);
  if (FComputedOverflowY=CSSRegistry.kwVisible)
      and (FComputedOverflowX in [CSSRegistry.kwHidden,CSSRegistry.kwScroll,CSSRegistry.kwAuto]) then
    FComputedOverflowY:=CSSRegistry.kwAuto;
  if (FComputedOverflowX=CSSRegistry.kwVisible)
      and (FComputedOverflowY in [CSSRegistry.kwHidden,CSSRegistry.kwScroll,CSSRegistry.kwAuto]) then
    FComputedOverflowX:=CSSRegistry.kwAuto;
end;

function TFresnelElement.ComputeAttribute(aDesc: TCSSAttributeDesc; var aValue: String
  ): TCSSAttributeValue.TState;
var
  ElAttrDesc: TFresnelElementAttrDesc;
  Complete: boolean;
begin
  if aValue='' then exit(cavsInvalid);
  Result:=cavsComputed;
  if aDesc is TFresnelElementAttrDesc then
  begin
    ElAttrDesc:=TFresnelElementAttrDesc(aDesc);
    if ElAttrDesc.OnCompute<>nil then
    begin
      ElAttrDesc.OnCompute(Self,aValue,Complete);
      if aValue='' then exit(cavsInvalid);
      if Complete then
        exit(cavsComputed)
      else
        exit(cavsBaseKeywords);
    end;
  end;
end;

class function TFresnelElement.GetCSSTypeStyle: TCSSString;
begin
  Result:='';
end;

procedure TFresnelElement.ClearCSSValues;
var
  i: Integer;
begin
  FreeAndNil(FCSSValues);
  for i:=0 to NodeCount-1 do
    Nodes[i].ClearCSSValues;
  for i:=0 to PseudoNodeCount-1 do
    PseudoNodes[i].ClearCSSValues;
end;

function TFresnelElement.HasCSSExplicitAttribute(const AttrID: TCSSNumericalID
  ): boolean;
begin
  Result:=AttrID=CSSAttributeID_ID;
end;

function TFresnelElement.GetCSSExplicitAttribute(const AttrID: TCSSNumericalID
  ): TCSSString;
begin
  if AttrID=CSSAttributeID_ID then
    Result:=Name
  else
    Result:='';
end;

function TFresnelElement.GetCSSAttributeClass: TCSSString;
begin
  FCSSClasses.Delimiter:=' ';
  Result:=FCSSClasses.DelimitedText;
end;

function TFresnelElement.GetCSSChild(const anIndex: integer): ICSSNode;
begin
  Result:=Nodes[anIndex];
end;

function TFresnelElement.GetCSSChildCount: integer;
begin
  Result:=NodeCount;
end;

function TFresnelElement.GetCSSDepth: integer;
var
  aChild: TFresnelElement;
begin
  // ToDo: store
  Result:=0;
  aChild:=Parent;
  while aChild<>nil do
  begin
    inc(Result);
    aChild:=aChild.Parent;
  end;
end;

function TFresnelElement.GetCSSEmpty: boolean;
begin
  Result:=NodeCount=0;
end;

function TFresnelElement.GetCSSID: TCSSString;
begin
  Result:=Name;
end;

function TFresnelElement.GetCSSIndex: integer;
begin
  // ToDo: store
  if Parent=nil then
    Result:=-1
  else
    Result:=Parent.FChildren.IndexOf(Self);
end;

function TFresnelElement.GetCSSNextOfType: ICSSNode;
var
  i, Cnt: Integer;
  MyID: TCSSNumericalID;
  aChild: TFresnelElement;
begin
  Result:=nil;
  i:=GetCSSIndex;
  if i<0 then exit;
  inc(i);
  MyID:=CSSTypeID;
  Cnt:=Parent.NodeCount;
  while i<Cnt do
  begin
    aChild:=Parent[i];
    if aChild.CSSTypeID=MyID then
      exit(aChild);
    inc(i);
  end;
end;

function TFresnelElement.GetCSSNextSibling: ICSSNode;
var
  i: Integer;
begin
  i:=GetCSSIndex;
  if (i<0) or (i+1>=Parent.NodeCount) then
    Result:=nil
  else
    Result:=Parent[i+1];
end;

function TFresnelElement.GetCSSParent: ICSSNode;
begin
  Result:=Parent;
end;

function TFresnelElement.GetCSSPreviousOfType: ICSSNode;
var
  i: Integer;
  MyID: TCSSNumericalID;
  aChild: TFresnelElement;
begin
  Result:=nil;
  i:=GetCSSIndex;
  if i<0 then exit;
  dec(i);
  MyID:=CSSTypeID;
  while i>=0 do
  begin
    aChild:=Parent[i];
    if aChild.CSSTypeID=MyID then
      exit(aChild);
    dec(i);
  end;
end;

function TFresnelElement.GetCSSPreviousSibling: ICSSNode;
var
  i: Integer;
begin
  i:=GetCSSIndex;
  if i<1 then
    Result:=nil
  else
    Result:=Parent[i-1];
end;

function TFresnelElement.GetCSSTypeID: TCSSNumericalID;
begin
  Result:=CSSTypeID;
end;

function TFresnelElement.GetCSSPseudoElementName: TCSSString;
begin
  Result:='';
end;

function TFresnelElement.GetCSSPseudoElementID: TCSSNumericalID;
begin
  Result:=CSSIDNone;
end;

function TFresnelElement.GetCSSTypeName: TCSSString;
begin
  Result:=CSSTypeName;
end;

function TFresnelElement.GetCSSCustomAttribute(const AttrID: TCSSNumericalID): TCSSString;
var
  El: TFresnelElement;
  i: Integer;
begin
  Result:='';
  El:=Self;
  repeat
    if El.FCSSValues<>nil then
    begin
      i:=El.FCSSValues.IndexOf(AttrID);
      if i>=0 then
      begin
        Result:=El.FCSSValues.Values[i].Value;
        if Result>'' then exit;
      end;
    end;
    El:=El.Parent;
  until El=nil;
end;

function TFresnelElement.HasCSSClass(const aClassName: TCSSString): boolean;
var
  i: Integer;
begin
  for i:=0 to CSSClasses.Count-1 do
    if aClassName=CSSClasses[i] then
      exit(true);
  Result:=false;
end;

function TFresnelElement.HasCSSPseudoClass(const AttrID: TCSSNumericalID
  ): boolean;
var
  Pseudo: TFresnelCSSPseudoClass;
  Desc: TCSSPseudoClassDesc;
begin
  Desc:=CSSRegistry.PseudoClasses[AttrID];
  if Desc is TFresnelCSSPseudoClassDesc then
  begin
    Pseudo:=TFresnelCSSPseudoClassDesc(Desc).Pseudo;
    Result:=CSSPseudoClass[Pseudo];
  end else
    Result:=false;
end;

procedure TFresnelElement.ComputeCSSValues;
var
  Rules: TCSSSharedRuleList;
  i: SizeInt;
  aValue: TCSSAttributeValue;
  AttrDesc: TCSSAttributeDesc;
  aComp: TCSSResCompValue;
  s: TCSSString;
  Complete, UseInherit: Boolean;
  AttrID: TCSSNumericalID;
begin
  Exclude(FStates,fesFontDescValid);

  //writeln('TFresnelElement.ComputeCSSValues ',GetPath,' ',Resolver<>nil);
  Resolver.Compute(Self,StyleElement,Rules,FCSSValues);
  // Note: var() and shorthands are already substituted

  // apply base keywords 'initial', 'inherit', 'unset', ...
  for i:=0 to length(FCSSValues.Values)-1 do
  begin
    aValue:=FCSSValues.Values[i];
    if aValue.State<>cavsSource then continue;
    AttrID:=aValue.AttrID;
    if AttrID>=CSSRegistry.AttributeCount then
      continue; // custom attributes are not parsed, their values were already copied
    AttrDesc:=CSSRegistry.Attributes[AttrID];
    s:=aValue.Value;
    {$IFDEF VerboseCSSAttr}
    writeln('TFresnelElement.ComputeCSSValues ',Name,':',ClassName,' i=',i,'/',length(FCSSValues.Values),' ',AttrDesc.Name,': ',s,';');
    {$ENDIF}

    aComp.EndP:=PChar(s);
    if not Resolver.ReadComp(aComp) then
    begin
      aValue.State:=cavsInvalid;
      continue;
    end;
    aValue.State:=cavsBaseKeywords;

    if aComp.Kind<>rvkKeyword then continue;
    UseInherit:=false;
    case aComp.KeywordID of
    CSSKeywordInitial:
      UseInherit:=false;
    CSSKeywordInherit:
      UseInherit:=true;
    CSSKeywordUnset,
    CSSKeywordRevert, CSSKeywordRevertLayer:
      UseInherit:=AttrDesc.Inherits;
    else
      continue;
    end;
    if UseInherit=AttrDesc.Inherits then
    begin
      // unset
      aValue.State:=cavsInvalid;
    end else if UseInherit and (Parent<>nil) then begin
      aValue.Value:=Parent.GetCSSString(aValue.AttrID,false,Complete);
    end else begin
      aValue.Value:=AttrDesc.InitialValue;
    end;
  end;

  ComputeBaseAttributes;
end;

procedure TFresnelElement.ComputeCSSAfterLayoutNode(Layouter: TFresnelLayouter);
begin
  if Layouter<>nil then ;
end;

procedure TFresnelElement.ComputeCSSLayoutFinished;
begin

end;

{ TPseudoElement }

constructor TPseudoElement.Create(AOwner: TComponent);
begin
  Include(FStates,fesPseudoElement);
  inherited Create(AOwner);
end;

class function TPseudoElement.CSSTypeID: TCSSNumericalID;
begin
  Result:=CSSIDNone;
end;

class function TPseudoElement.CSSTypeName: TCSSString;
begin
  Result:='';
end;

function TPseudoElement.GetCSSID: TCSSString;
begin
  Result:='';
end;

function TPseudoElement.GetCSSTypeName: TCSSString;
begin
  Result:='';
end;

function TPseudoElement.GetCSSTypeID: TCSSNumericalID;
begin
  Result:=CSSIDNone;
end;

function TPseudoElement.GetCSSIndex: integer;
begin
  Result:=-1;
end;

function TPseudoElement.GetCSSNextSibling: ICSSNode;
begin
  Result:=nil;
end;

function TPseudoElement.GetCSSPreviousSibling: ICSSNode;
begin
  Result:=nil;
end;

function TPseudoElement.GetCSSNextOfType: ICSSNode;
begin
  Result:=nil;
end;

function TPseudoElement.GetCSSPreviousOfType: ICSSNode;
begin
  Result:=nil;
end;

{ TPseudoElSelection }

function TPseudoElSelection.GetCSSPseudoElementID: TCSSNumericalID;
begin
  Result:=FFresnelSelectionTypeID;
end;

function TPseudoElSelection.GetCSSPseudoElementName: TCSSString;
begin
  Result:='selection';
end;

{ TReplacedElement }

function TReplacedElement.AcceptChildrenAtDesignTime: boolean;
begin
  Result:=false;
end;

initialization
  CSSRegistry:=TFresnelCSSRegistry.Create;
  CSSRegistry.Init;

finalization
  FreeAndNil(CSSRegistry);

end.

