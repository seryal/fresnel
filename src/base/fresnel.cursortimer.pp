unit Fresnel.CursorTimer;

{$mode objfpc}{$H+}
{$Interfaces CORBA}

interface

uses
  Classes, SysUtils, fptimer;

type
  IBlinkControl = Interface
    procedure blink(aVisible : boolean);
  end;

  { TCursorTimer }

  TCursorTimer = class
  private
    FBlinkControl: IBlinkControl;
    FVisible: Boolean;
    FBlinkRate: Word;
    FTimer : TFPTimer;
    class var _Instance: TCursorTimer;
    function GetVisible: boolean;
    procedure HandleBlink(Sender: TObject);
    procedure SetBlinkControl(const aValue: IBlinkControl);
    procedure SetBlinkRate(const aValue: Word);
  protected
    procedure DisableTimer;
    procedure EnableTimer;
    Procedure Blink; virtual;
  Public
    class constructor Init;
    class destructor Done;
    constructor Create; virtual;
    procedure Restart; virtual; // if blinking, restart timer and show cursor
    Property BlinkControl : IBlinkControl Read FBlinkControl Write SetBlinkControl;
    Property BlinkRate : Word Read FBlinkRate Write SetBlinkRate; // interval in ms
    Property Visible : boolean read GetVisible;
    Class property Instance : TCursorTimer Read _Instance;
  end;

function CursorTimer : TCursorTimer;

implementation

function CursorTimer: TCursorTimer;
begin
  Result:=TCursorTimer.Instance;
end;

{ TCursorTimer }

procedure TCursorTimer.SetBlinkControl(const aValue: IBlinkControl);
begin
  if FBlinkControl=aValue then Exit;
  if FVisible then
    Blink; // hide old cursor
  DisableTimer;
  FBlinkControl:=aValue;
  if assigned(FBlinkControl) then
    EnableTimer;
end;

procedure TCursorTimer.HandleBlink(Sender: TObject);
begin
  Blink;
end;

function TCursorTimer.GetVisible: boolean;
begin
  Result:=(BlinkControl<>nil) and FVisible;
end;

procedure TCursorTimer.SetBlinkRate(const aValue: Word);
begin
  if FBlinkRate=aValue then Exit;
  FBlinkRate:=aValue;
  // Safety
  if FBlinkRate<30 then
    FBlinkRate:=30;
  if Assigned(FTimer) then
    FTimer.Interval:=FBlinkRate;
end;

procedure TCursorTimer.DisableTimer;
begin
  if assigned(FTimer) then
    FTimer.Enabled:=False;
end;

procedure TCursorTimer.EnableTimer;
begin
  if not Assigned(FTimer) then
    begin
    FTimer:=TFPTimer.Create(Nil);
    FTimer.Interval:=FBlinkRate;
    FTimer.OnTimer:=@HandleBlink;
    end;
  FTimer.Enabled:=True;
end;

procedure TCursorTimer.Blink;
begin
  FVisible:=Not FVisible;
  if Assigned(FBlinkControl) then
    FBlinkControl.Blink(FVisible);
end;

class constructor TCursorTimer.Init;
begin
  _Instance:=TCursorTimer.Create;
end;

class destructor TCursorTimer.Done;
begin
  FreeAndNil(_instance);
end;

constructor TCursorTimer.Create;
begin
  FBlinkRate:=500;
end;

procedure TCursorTimer.Restart;
begin
  if FTimer=nil then exit;
  if FTimer.Enabled then
    begin
    FTimer.Enabled:=false;
    FTimer.Enabled:=true;
    end;
  if not FVisible then
    Blink;
end;

end.

