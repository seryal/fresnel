unit fresnel.keys;

{$mode ObjFPC}{$H+}
{$modeswitch advancedrecords}

interface

Type
  TKeyNames = Record
  Const
    Alt = 'Alt';
    AltGraph = 'AltGraph';
    CapsLock = 'CapsLock';
    Control = 'Control';
    Fn = 'Fn';
    FnLock = 'FnLock';
    Hyper = 'Hyper';
    Meta = 'Meta';
    NumLock = 'NumLock';
    ScrollLock = 'ScrollLock';
    Shift = 'Shift';
    Super = 'Super';
    Symbol = 'Symbol';
    SymbolLock = 'SymbolLock';
    Enter = 'Enter';
    Tab = 'Tab';
    Space = 'Space';
    ArrowDown = 'ArrowDown';
    ArrowLeft = 'ArrowLeft';
    ArrowRight = 'ArrowRight';
    ArrowUp = 'ArrowUp';
    End_ = 'End';
    Home = 'Home';
    PageDown = 'PageDown';
    PageUp = 'PageUp';
    BackSpace = 'Backspace';
    Clear = 'Clear';
    Copy = 'Copy';
    CrSel = 'CrSel';
    Cut = 'Cut';
    Delete = 'Delete';
    EraseEof = 'EraseEof';
    ExSel = 'ExSel';
    Insert = 'Insert';
    Paste = 'Paste';
    Redo = 'Redo';
    Undo = 'Undo';
    Accept = 'Accept';
    Again = 'Again';
    Attn = 'Attn';
    Cancel = 'Cancel';
    ContextMenu = 'Contextmenu';
    Escape = 'Escape';
    Execute = 'Execute';
    Find = 'Find';
    Finish = 'Finish';
    Help = 'Help';
    Pause = 'Pause';
    Play = 'Play';
    Props = 'Props';
    Select = 'Select';
    ZoomIn = 'ZoomIn';
    ZoomOut = 'ZoomOut';
    BrightnessDown = 'BrightnessDown';
    BrightnessUp = 'BrightnessUp';
    Eject = 'Eject';
    LogOff = 'LogOff';
    Power = 'Power';
    PowerOff = 'PowerOff';
    PrintScreen = 'PrintScreen';
    Hibernate = 'Hibernate';
    Standby = 'Standby';
    WakeUp = 'WakeUp';
    AllCandidates = 'AllCandidates';
    Alphanumeric =  'Alphanumeric';
    CodeInput = 'CodeInput';
    Compose = 'Compose';
    Convert = 'Convert';
    Dead = 'Dead';
    FinalMode = 'FinalMode';
    GroupFirst = 'GroupFirst';
    GroupLast = 'GroupLast';
    GroupNext = 'GroupNext';
    GroupPrevious = 'GroupPrevious';
    ModeChange = 'ModeChange';
    NextCandidate = 'NextCandidate';
    NonConvert = 'NonConvert';
    PreviousCandidate = 'PreviousCandidate';
    Process = 'Process';
    SingleCandidate = 'SingleCandidate';
    HangulMode = 'HangulMode';
    HanjaMode = 'HanjaMode';
    JunjaMode = 'JunjaMode';
    Eisu = 'Eisu';
    Hankaku = 'Hankaku';
    Hiranga = 'Hiranga';
    HirangaKatakana = 'HirangaKatakana';
    KanaMode = 'KanaMode';
    Katakana = 'Katakana';
    Romaji = 'Romaji';
    Zenkaku = 'Zenkaku';
    ZenkakuHanaku = 'ZenkakuHanaku';
    F1 = 'F1';
    F2 = 'F2';
    F3 = 'F3';
    F4 = 'F4';
    F5 = 'F5';
    F6 = 'F6';
    F7 = 'F7';
    F8 = 'F8';
    F9 = 'F9';
    F10 = 'F10';
    F11 = 'F11';
    F12 = 'F12';
    F13 = 'F13';
    F14 = 'F14';
    F15 = 'F15';
    F16 = 'F16';
    F17 = 'F17';
    F18 = 'F18';
    F19 = 'F19';
    F20 = 'F20';
    Soft1 = 'Soft1';
    Soft2 = 'Soft2';
    Soft3 = 'Soft3';
    Soft4 = 'Soft4';
    Decimal = 'Decimal';
    Key11 = 'Key11';
    Key12 = 'Key12';
    Multiply = 'Multiply';
    Add = 'Add';
    NumClear = 'Clear';
    Divide = 'Divide';
    Subtract = 'Subtract';
    Separator = 'Separator';
    AppSwitch = 'AppSwitch';
    Call = 'Call';
    Camera = 'Camera';
    CameraFocus = 'CameraFocus';
    EndCall = 'EndCall';
    GoBack = 'GoBack';
    GoHome = 'GoHome';
    HeadsetHook = 'HeadsetHook';
    LastNumberRedial = 'LastNumberRedial';
    Notification = 'Notification';
    MannerMode = 'MannerMode';
    VoiceDial = 'VoiceDial';

    // Multimedia keys
    ChannelDown = 'ChannelDown';
    ChannelUp = 'ChannelUp';
    MediaFastForward = 'MediaFastForward';
    MediaPause = 'MediaPause';
    MediaPlay = 'MediaPlay';
    MediaPlayPause = 'MediaPlayPause';
    MediaRecord = 'MediaRecord';
    MediaRewind = 'MediaRewind';
    MediaStop = 'MediaStop';
    MediaTrackNext = 'MediaTrackNext';
    MediaTrackPrevious = 'MediaTrackPrevious';
    // Audio control keys
    AudioBalanceLeft = 'AudioBalanceLeft';
    AudioBalanceRight = 'AudioBalanceRight';
    AudioBassDown = 'AudioBassDown';
    AudioBassBoostDown = 'AudioBassBoostDown';
    AudioBassBoostToggle = 'AudioBassBoostToggle';
    AudioBassBoostUp = 'AudioBassBoostUp';
    AudioBassUp = 'AudioBassUp';
    AudioFaderFront = 'AudioFaderFront';
    AudioFaderRear = 'AudioFaderRear';
    AudioSurroundModeNext = 'AudioSurroundModeNext';
    AudioTrebleDown = 'AudioTrebleDown';
    AudioTrebleUp = 'AudioTrebleUp';
    AudioVolumeDown = 'AudioVolumeDown';
    AudioVolumeMute = 'AudioVolumeMute';
    AudioVolumeUp = 'AudioVolumeUp';
    MicrophoneToggle = 'MicrophoneToggle';
    MicrophoneVolumeDown = 'MicrophoneVolumeDown';
    MicrophoneVolumeMute = 'MicrophoneVolumeMute';
    MicrophoneVolumeUp = 'MicrophoneVolumeUp';
    // TV Control keys
    TV = 'TV';
    TV3DMode = 'TV3DMode';
    TVAntennaCable = 'TVAntennaCable';
    TVAudioDescription = 'TVAudioDescription';
    TVAudioDescriptionMixDown = 'TVAudioDescriptionMixDown';
    TVAudioDescriptionMixUp = 'TVAudioDescriptionMixUp';
    TVContentsMenu = 'TVContentsMenu';
    TVDataService = 'TVDataService';
    TVInput = 'TVInput';
    TVInputComponent1 = 'TVInputComponent1';
    TVInputComponent2 = 'TVInputComponent2';
    TVInputComposite1 = 'TVInputComposite1';
    TVInputComposite2 = 'TVInputComposite2';
    TVInputHDMI1 = 'TVInputHDMI1';
    TVInputHDMI2 = 'TVInputHDMI2';
    TVInputHDMI3 = 'TVInputHDMI3';
    TVInputHDMI4 = 'TVInputHDMI4';
    TVInputVGA1 = 'TVInputVGA1';
    TVMediaContext = 'TVMediaContext';
    TVNetwork = 'TVNetwork';
    TVNumberEntry = 'TVNumberEntry';
    TVPower = 'TVPower';
    TVRadioService = 'TVRadioService';
    TVSatellite = 'TVSatellite';
    TVSatelliteBS = 'TVSatelliteBS';
    TVSatelliteCS = 'TVSatelliteCS';
    TVSatelliteToggle = 'TVSatelliteToggle';
    TVTerrestrialAnalog = 'TVTerrestrialAnalog';
    TVTerrestrialDigital = 'TVTerrestrialDigital';
    TVTimer = 'TVTimer';
     // Media controller keys
    AVRInput = 'AVRInput';
    AVRPower = 'AVRPower';
    ColorF0Red = 'ColorF0Red';
    ColorF1Green = 'ColorF1Green';
    ColorF2Yellow = 'ColorF2Yellow';
    ColorF3Blue = 'ColorF3Blue';
    ColorF4Grey = 'ColorF4Grey';
    ColorF5Brown = 'ColorF5Brown';
    ClosedCaptionToggle = 'ClosedCaptionToggle';
    Dimmer = 'Dimmer';
    DisplaySwap = 'DisplaySwap';
    DVR = 'DVR';
    Exit = 'Exit';
    FavoriteClear0 = 'FavoriteClear0';
    FavoriteClear1 = 'FavoriteClear1';
    FavoriteClear2 = 'FavoriteClear2';
    FavoriteClear3 = 'FavoriteClear3';
    FavoriteRecall0 = 'FavoriteRecall0';
    FavoriteRecall1 = 'FavoriteRecall1';
    FavoriteRecall2 = 'FavoriteRecall2';
    FavoriteRecall3 = 'FavoriteRecall3';
    FavoriteStore0 = 'FavoriteStore0';
    FavoriteStore1 = 'FavoriteStore1';
    FavoriteStore2 = 'FavoriteStore2';
    FavoriteStore3 = 'FavoriteStore3';
    Guide = 'Guide';
    GuideNextDay = 'GuideNextDay';
    GuidePreviousDay = 'GuidePreviousDay';
    Info = 'Info';
    InstantReplay = 'InstantReplay';
    Link = 'Link';
    ListProgram = 'ListProgram';
    LiveContent = 'LiveContent';
    Lock = 'Lock';
    MediaApps = 'MediaApps';
    MediaAudioTrack = 'MediaAudioTrack';
    MediaLast = 'MediaLast';
    MediaSkipBackward = 'MediaSkipBackward';
    MediaSkipForward = 'MediaSkipForward';
    MediaStepBackward = 'MediaStepBackward';
    MediaStepForward = 'MediaStepForward';
    MediaTopMenu = 'MediaTopMenu';
    NavigateIn = 'NavigateIn';
    NavigateNext = 'NavigateNext';
    NavigateOut = 'NavigateOut';
    NavigatePrevious = 'NavigatePrevious';
    NextFavoriteChannel = 'NextFavoriteChannel';
    NextUserProfile = 'NextUserProfile';
    OnDemand = 'OnDemand';
    Pairing = 'Pairing';
    PinPDown = 'PinPDown';
    PinPMove = 'PinPMove';
    PinPToggle = 'PinPToggle';
    PinPUp = 'PinPUp';
    PlaySpeedDown = 'PlaySpeedDown';
    PlaySpeedReset = 'PlaySpeedReset';
    PlaySpeedUp = 'PlaySpeedUp';
    RandomToggle = 'RandomToggle';
    RcLowBattery = 'RcLowBattery';
    RecordSpeedNext = 'RecordSpeedNext';
    RfBypass = 'RfBypass';
    ScanChannelsToggle = 'ScanChannelsToggle';
    ScreenModeNext = 'ScreenModeNext';
    Settings = 'Settings';
    SplitScreenToggle = 'SplitScreenToggle';
    STBInput = 'STBInput';
    STBPower = 'STBPower';
    Subtitle = 'Subtitle';
    Teletext = 'Teletext';
    VideoModeNext = 'VideoModeNext';
    Wink = 'Wink';
    ZoomToggle = 'ZoomToggle';
    // Speech recognition keys
    SpeechCorrectionList = 'SpeechCorrectionList';
    SpeechInputToggle = 'SpeechInputToggle';

    // Document keys
    Close = 'Close';
    New = 'New';
    Open = 'Open';
    Print = 'Print';
    Save = 'Save';
    SpellCheck = 'SpellCheck';
    MailForward = 'MailForward';
    MailReply = 'MailReply';
    MailSend = 'MailSend';
    // Application selector keys
    LaunchCalculator = 'LaunchCalculator';
    LaunchCalendar = 'LaunchCalendar';
    LaunchContacts = 'LaunchContacts';
    LaunchMail = 'LaunchMail';
    LaunchMediaPlayer = 'LaunchMediaPlayer';
    LaunchMusicPlayer = 'LaunchMusicPlayer';
    LaunchMyComputer = 'LaunchMyComputer';
    LaunchPhone = 'LaunchPhone';
    LaunchScreenSaver = 'LaunchScreenSaver';
    LaunchSpreadsheet = 'LaunchSpreadsheet';
    LaunchWebBrowser = 'LaunchWebBrowser';
    LaunchWebCam = 'LaunchWebCam';
    LaunchWordProcessor = 'LaunchWordProcessor';
    LaunchApplication1 = 'LaunchApplication1';
    LaunchApplication2 = 'LaunchApplication2';
    LaunchApplication3 = 'LaunchApplication3';
    LaunchApplication4 = 'LaunchApplication4';
    LaunchApplication5 = 'LaunchApplication5';
    LaunchApplication6 = 'LaunchApplication6';
    LaunchApplication7 = 'LaunchApplication7';
    LaunchApplication8 = 'LaunchApplication8';
    LaunchApplication9 = 'LaunchApplication9';
    LaunchApplication10 = 'LaunchApplication10';
    LaunchApplication11 = 'LaunchApplication11';
    LaunchApplication12 = 'LaunchApplication12';
    LaunchApplication13 = 'LaunchApplication13';
    LaunchApplication14 = 'LaunchApplication14';
    LaunchApplication15 = 'LaunchApplication15';
    LaunchApplication16 = 'LaunchApplication16';

     // Browser Control keys
    BrowserBack = 'BrowserBack';
    BrowserFavorites = 'BrowserFavorites';
    BrowserForward = 'BrowserForward';
    BrowserHome = 'BrowserHome';
    BrowserRefresh = 'BrowserRefresh';
    BrowserSearch = 'BrowserSearch';
    BrowserStop = 'BrowserStop';
  end;

  TKeyCodeName = record
    Code : Integer;
    Name : string;
  end;
  TKeyCodeNameArray = Array of TKeyCodeName;

  TEnumKeysCallback = Procedure(aCode : Integer; aName : string) of object;

  TSpecialKeyEnumerator = class(TObject)
  Protected
    Procedure EnumKey(aCode : Integer; aName : string); virtual; abstract;
  end;

  { TKeyCodes }

  TKeyCodes = Record
  Private
    class var FSortedKeys : TKeyCodeNameArray;
    class function indexOf(aKeyCode : Integer) : Integer; static;
    class function GetKeyName(aKeyCode : Integer) : String; static;
  Public
  Const
    // Specials...
    SpecialKeysOffset          = -1024;
    // DoStart
    Alt                        = SpecialKeysOffset - 0000;
    AltGraph                   = SpecialKeysOffset - 0001;
    CapsLock                   = SpecialKeysOffset - 0002;
    Control                    = SpecialKeysOffset - 0003;
    Fn                         = SpecialKeysOffset - 0004;
    FnLock                     = SpecialKeysOffset - 0005;
    Hyper                      = SpecialKeysOffset - 0006;
    Meta                       = SpecialKeysOffset - 0007;
    NumLock                    = SpecialKeysOffset - 0008;
    ScrollLock                 = SpecialKeysOffset - 0009;
    Shift                      = SpecialKeysOffset - 0010;
    Super                      = SpecialKeysOffset - 0011;
    Symbol                     = SpecialKeysOffset - 0012;
    SymbolLock                 = SpecialKeysOffset - 0013;
    Enter                      = SpecialKeysOffset - 0014;
    Tab                        = SpecialKeysOffset - 0015;
    Space                      = SpecialKeysOffset - 0016;
    ArrowDown                  = SpecialKeysOffset - 0017;
    ArrowLeft                  = SpecialKeysOffset - 0018;
    ArrowRight                 = SpecialKeysOffset - 0019;
    ArrowUp                    = SpecialKeysOffset - 0020;
    End_                       = SpecialKeysOffset - 0021;
    Home                       = SpecialKeysOffset - 0022;
    PageDown                   = SpecialKeysOffset - 0023;
    PageUp                     = SpecialKeysOffset - 0024;
    BackSpace                  = SpecialKeysOffset - 0025;
    Clear                      = SpecialKeysOffset - 0026;
    Copy                       = SpecialKeysOffset - 0027;
    CrSel                      = SpecialKeysOffset - 0028;
    Cut                        = SpecialKeysOffset - 0029;
    Delete                     = SpecialKeysOffset - 0030;
    EraseEof                   = SpecialKeysOffset - 0031;
    ExSel                      = SpecialKeysOffset - 0032;
    Insert                     = SpecialKeysOffset - 0033;
    Paste                      = SpecialKeysOffset - 0034;
    Redo                       = SpecialKeysOffset - 0035;
    Undo                       = SpecialKeysOffset - 0036;
    Accept                     = SpecialKeysOffset - 0037;
    Again                      = SpecialKeysOffset - 0038;
    Attn                       = SpecialKeysOffset - 0039;
    Cancel                     = SpecialKeysOffset - 0040;
    ContextMenu                = SpecialKeysOffset - 0041;
    Escape                     = SpecialKeysOffset - 0042;
    Execute                    = SpecialKeysOffset - 0043;
    Find                       = SpecialKeysOffset - 0044;
    Finish                     = SpecialKeysOffset - 0045;
    Help                       = SpecialKeysOffset - 0046;
    Pause                      = SpecialKeysOffset - 0047;
    Play                       = SpecialKeysOffset - 0048;
    Props                      = SpecialKeysOffset - 0049;
    Select                     = SpecialKeysOffset - 0050;
    ZoomIn                     = SpecialKeysOffset - 0051;
    ZoomOut                    = SpecialKeysOffset - 0052;
    BrightnessDown             = SpecialKeysOffset - 0053;
    BrightnessUp               = SpecialKeysOffset - 0054;
    Eject                      = SpecialKeysOffset - 0055;
    LogOff                     = SpecialKeysOffset - 0056;
    Power                      = SpecialKeysOffset - 0057;
    PowerOff                   = SpecialKeysOffset - 0058;
    PrintScreen                = SpecialKeysOffset - 0059;
    Hibernate                  = SpecialKeysOffset - 0060;
    Standby                    = SpecialKeysOffset - 0061;
    WakeUp                     = SpecialKeysOffset - 0062;
    AllCandidates              = SpecialKeysOffset - 0063;
    Alphanumeric               = SpecialKeysOffset - 0064;
    CodeInput                  = SpecialKeysOffset - 0065;
    Compose                    = SpecialKeysOffset - 0066;
    Convert                    = SpecialKeysOffset - 0067;
    Dead                       = SpecialKeysOffset - 0068;
    FinalMode                  = SpecialKeysOffset - 0069;
    GroupFirst                 = SpecialKeysOffset - 0070;
    GroupLast                  = SpecialKeysOffset - 0071;
    GroupNext                  = SpecialKeysOffset - 0072;
    GroupPrevious              = SpecialKeysOffset - 0073;
    ModeChange                 = SpecialKeysOffset - 0074;
    NextCandidate              = SpecialKeysOffset - 0075;
    NonConvert                 = SpecialKeysOffset - 0076;
    PreviousCandidate          = SpecialKeysOffset - 0077;
    Process                    = SpecialKeysOffset - 0078;
    SingleCandidate            = SpecialKeysOffset - 0079;
    HangulMode                 = SpecialKeysOffset - 0080;
    HanjaMode                  = SpecialKeysOffset - 0081;
    JunjaMode                  = SpecialKeysOffset - 0082;
    Eisu                       = SpecialKeysOffset - 0083;
    Hankaku                    = SpecialKeysOffset - 0084;
    Hiranga                    = SpecialKeysOffset - 0085;
    HirangaKatakana            = SpecialKeysOffset - 0086;
    KanaMode                   = SpecialKeysOffset - 0087;
    KanjiMode                  = SpecialKeysOffset - 0340; // Forgotten
    Katakana                   = SpecialKeysOffset - 0088;
    Romaji                     = SpecialKeysOffset - 0089;
    Zenkaku                    = SpecialKeysOffset - 0090;
    ZenkakuHankaku             = SpecialKeysOffset - 0091;
    F1                         = SpecialKeysOffset - 0092;
    F2                         = SpecialKeysOffset - 0093;
    F3                         = SpecialKeysOffset - 0094;
    F4                         = SpecialKeysOffset - 0095;
    F5                         = SpecialKeysOffset - 0096;
    F6                         = SpecialKeysOffset - 0097;
    F7                         = SpecialKeysOffset - 0098;
    F8                         = SpecialKeysOffset - 0099;
    F9                         = SpecialKeysOffset - 0100;
    F10                        = SpecialKeysOffset - 0101;
    F11                        = SpecialKeysOffset - 0102;
    F12                        = SpecialKeysOffset - 0103;
    F13                        = SpecialKeysOffset - 0104;
    F14                        = SpecialKeysOffset - 0105;
    F15                        = SpecialKeysOffset - 0106;
    F16                        = SpecialKeysOffset - 0107;
    F17                        = SpecialKeysOffset - 0108;
    F18                        = SpecialKeysOffset - 0109;
    F19                        = SpecialKeysOffset - 0110;
    F20                        = SpecialKeysOffset - 0111;
    Soft1                      = SpecialKeysOffset - 0112;
    Soft2                      = SpecialKeysOffset - 0113;
    Soft3                      = SpecialKeysOffset - 0114;
    Soft4                      = SpecialKeysOffset - 0115;
    Decimal                    = SpecialKeysOffset - 0116;
    Key11                      = SpecialKeysOffset - 0117;
    Key12                      = SpecialKeysOffset - 0118;
    Multiply                   = SpecialKeysOffset - 0119;
    Add                        = SpecialKeysOffset - 0120;
    NumClear                   = SpecialKeysOffset - 0121;
    Divide                     = SpecialKeysOffset - 0122;
    Subtract                   = SpecialKeysOffset - 0123;
    Separator                  = SpecialKeysOffset - 0124;
    AppSwitch                  = SpecialKeysOffset - 0125;
    Call                       = SpecialKeysOffset - 0126;
    Camera                     = SpecialKeysOffset - 0127;
    CameraFocus                = SpecialKeysOffset - 0128;
    EndCall                    = SpecialKeysOffset - 0129;
    GoBack                     = SpecialKeysOffset - 0130;
    GoHome                     = SpecialKeysOffset - 0131;
    HeadsetHook                = SpecialKeysOffset - 0132;
    LastNumberRedial           = SpecialKeysOffset - 0133;
    Notification               = SpecialKeysOffset - 0134;
    MannerMode                 = SpecialKeysOffset - 0135;
    VoiceDial                  = SpecialKeysOffset - 0136;

    // Multimedia keys
    ChannelDown                = SpecialKeysOffset - 0137;
    ChannelUp                  = SpecialKeysOffset - 0138;
    MediaFastForward           = SpecialKeysOffset - 0139;
    MediaPause                 = SpecialKeysOffset - 0140;
    MediaPlay                  = SpecialKeysOffset - 0141;
    MediaPlayPause             = SpecialKeysOffset - 0142;
    MediaRecord                = SpecialKeysOffset - 0143;
    MediaRewind                = SpecialKeysOffset - 0144;
    MediaStop                  = SpecialKeysOffset - 0145;
    MediaTrackNext             = SpecialKeysOffset - 0146;
    MediaTrackPrevious         = SpecialKeysOffset - 0147;
    // Audio control keys
    AudioBalanceLeft           = SpecialKeysOffset - 0148;
    AudioBalanceRight          = SpecialKeysOffset - 0149;
    AudioBassDown              = SpecialKeysOffset - 0150;
    AudioBassBoostDown         = SpecialKeysOffset - 0151;
    AudioBassBoostToggle       = SpecialKeysOffset - 0152;
    AudioBassBoostUp           = SpecialKeysOffset - 0153;
    AudioBassUp                = SpecialKeysOffset - 0154;
    AudioFaderFront            = SpecialKeysOffset - 0155;
    AudioFaderRear             = SpecialKeysOffset - 0156;
    AudioSurroundModeNext      = SpecialKeysOffset - 0157;
    AudioTrebleDown            = SpecialKeysOffset - 0158;
    AudioTrebleUp              = SpecialKeysOffset - 0159;
    AudioVolumeDown            = SpecialKeysOffset - 0160;
    AudioVolumeMute            = SpecialKeysOffset - 0161;
    AudioVolumeUp              = SpecialKeysOffset - 0162;
    MicrophoneToggle           = SpecialKeysOffset - 0163;
    MicrophoneVolumeDown       = SpecialKeysOffset - 0164;
    MicrophoneVolumeMute       = SpecialKeysOffset - 0165;
    MicrophoneVolumeUp         = SpecialKeysOffset - 0166;
    // TV Control keys
    TV                         = SpecialKeysOffset - 0167;
    TV3DMode                   = SpecialKeysOffset - 0168;
    TVAntennaCable             = SpecialKeysOffset - 0169;
    TVAudioDescription         = SpecialKeysOffset - 0170;
    TVAudioDescriptionMixDown  = SpecialKeysOffset - 0171;
    TVAudioDescriptionMixUp    = SpecialKeysOffset - 0172;
    TVContentsMenu             = SpecialKeysOffset - 0173;
    TVDataService              = SpecialKeysOffset - 0174;
    TVInput                    = SpecialKeysOffset - 0175;
    TVInputComponent1          = SpecialKeysOffset - 0176;
    TVInputComponent2          = SpecialKeysOffset - 0177;
    TVInputComposite1          = SpecialKeysOffset - 0178;
    TVInputComposite2          = SpecialKeysOffset - 0179;
    TVInputHDMI1               = SpecialKeysOffset - 0180;
    TVInputHDMI2               = SpecialKeysOffset - 0181;
    TVInputHDMI3               = SpecialKeysOffset - 0182;
    TVInputHDMI4               = SpecialKeysOffset - 0183;
    TVInputVGA1                = SpecialKeysOffset - 0184;
    TVMediaContext             = SpecialKeysOffset - 0185;
    TVNetwork                  = SpecialKeysOffset - 0186;
    TVNumberEntry              = SpecialKeysOffset - 0187;
    TVPower                    = SpecialKeysOffset - 0188;
    TVRadioService             = SpecialKeysOffset - 0189;
    TVSatellite                = SpecialKeysOffset - 0190;
    TVSatelliteBS              = SpecialKeysOffset - 0191;
    TVSatelliteCS              = SpecialKeysOffset - 0192;
    TVSatelliteToggle          = SpecialKeysOffset - 0193;
    TVTerrestrialAnalog        = SpecialKeysOffset - 0194;
    TVTerrestrialDigital       = SpecialKeysOffset - 0195;
    TVTimer                    = SpecialKeysOffset - 0196;
     // Media controller keys
    AVRInput                   = SpecialKeysOffset - 0197;
    AVRPower                   = SpecialKeysOffset - 0198;
    ColorF0Red                 = SpecialKeysOffset - 0199;
    ColorF1Green               = SpecialKeysOffset - 0200;
    ColorF2Yellow              = SpecialKeysOffset - 0201;
    ColorF3Blue                = SpecialKeysOffset - 0202;
    ColorF4Grey                = SpecialKeysOffset - 0203;
    ColorF5Brown               = SpecialKeysOffset - 0204;
    ClosedCaptionToggle        = SpecialKeysOffset - 0205;
    Dimmer                     = SpecialKeysOffset - 0206;
    DisplaySwap                = SpecialKeysOffset - 0207;
    DVR                        = SpecialKeysOffset - 0208;
    Exit                       = SpecialKeysOffset - 0209;
    FavoriteClear0             = SpecialKeysOffset - 0210;
    FavoriteClear1             = SpecialKeysOffset - 0211;
    FavoriteClear2             = SpecialKeysOffset - 0212;
    FavoriteClear3             = SpecialKeysOffset - 0213;
    FavoriteRecall0            = SpecialKeysOffset - 0214;
    FavoriteRecall1            = SpecialKeysOffset - 0215;
    FavoriteRecall2            = SpecialKeysOffset - 0216;
    FavoriteRecall3            = SpecialKeysOffset - 0217;
    FavoriteStore0             = SpecialKeysOffset - 0218;
    FavoriteStore1             = SpecialKeysOffset - 0219;
    FavoriteStore2             = SpecialKeysOffset - 0220;
    FavoriteStore3             = SpecialKeysOffset - 0221;
    Guide                      = SpecialKeysOffset - 0222;
    GuideNextDay               = SpecialKeysOffset - 0223;
    GuidePreviousDay           = SpecialKeysOffset - 0224;
    Info                       = SpecialKeysOffset - 0225;
    InstantReplay              = SpecialKeysOffset - 0226;
    Link                       = SpecialKeysOffset - 0227;
    ListProgram                = SpecialKeysOffset - 0228;
    LiveContent                = SpecialKeysOffset - 0229;
    Lock                       = SpecialKeysOffset - 0230;
    MediaApps                  = SpecialKeysOffset - 0231;
    MediaAudioTrack            = SpecialKeysOffset - 0232;
    MediaLast                  = SpecialKeysOffset - 0233;
    MediaSkipBackward          = SpecialKeysOffset - 0234;
    MediaSkipForward           = SpecialKeysOffset - 0235;
    MediaStepBackward          = SpecialKeysOffset - 0236;
    MediaStepForward           = SpecialKeysOffset - 0237;
    MediaTopMenu               = SpecialKeysOffset - 0238;
    NavigateIn                 = SpecialKeysOffset - 0239;
    NavigateNext               = SpecialKeysOffset - 0240;
    NavigateOut                = SpecialKeysOffset - 0241;
    NavigatePrevious           = SpecialKeysOffset - 0242;
    NextFavoriteChannel        = SpecialKeysOffset - 0243;
    NextUserProfile            = SpecialKeysOffset - 0244;
    OnDemand                   = SpecialKeysOffset - 0245;
    Pairing                    = SpecialKeysOffset - 0246;
    PinPDown                   = SpecialKeysOffset - 0247;
    PinPMove                   = SpecialKeysOffset - 0248;
    PinPToggle                 = SpecialKeysOffset - 0249;
    PinPUp                     = SpecialKeysOffset - 0250;
    PlaySpeedDown              = SpecialKeysOffset - 0251;
    PlaySpeedReset             = SpecialKeysOffset - 0252;
    PlaySpeedUp                = SpecialKeysOffset - 0253;
    RandomToggle               = SpecialKeysOffset - 0254;
    RcLowBattery               = SpecialKeysOffset - 0255;
    RecordSpeedNext            = SpecialKeysOffset - 0256;
    RfBypass                   = SpecialKeysOffset - 0257;
    ScanChannelsToggle         = SpecialKeysOffset - 0258;
    ScreenModeNext             = SpecialKeysOffset - 0259;
    Settings                   = SpecialKeysOffset - 0260;
    SplitScreenToggle          = SpecialKeysOffset - 0261;
    STBInput                   = SpecialKeysOffset - 0262;
    STBPower                   = SpecialKeysOffset - 0263;
    Subtitle                   = SpecialKeysOffset - 0264;
    Teletext                   = SpecialKeysOffset - 0265;
    VideoModeNext              = SpecialKeysOffset - 0266;
    Wink                       = SpecialKeysOffset - 0267;
    ZoomToggle                 = SpecialKeysOffset - 0268;
    // Speech recognition keys
    SpeechCorrectionList       = SpecialKeysOffset - 0269;
    SpeechInputToggle          = SpecialKeysOffset - 0270;

    // Document keys
    Close                      = SpecialKeysOffset - 0271;
    New                        = SpecialKeysOffset - 0272;
    Open                       = SpecialKeysOffset - 0273;
    Print                      = SpecialKeysOffset - 0274;
    Save                       = SpecialKeysOffset - 0275;
    SpellCheck                 = SpecialKeysOffset - 0276;
    MailForward                = SpecialKeysOffset - 0277;
    MailReply                  = SpecialKeysOffset - 0278;
    MailSend                   = SpecialKeysOffset - 0279;
    // Application selector keys
    LaunchCalculator           = SpecialKeysOffset - 0280;
    LaunchCalendar             = SpecialKeysOffset - 0281;
    LaunchContacts             = SpecialKeysOffset - 0282;
    LaunchMail                 = SpecialKeysOffset - 0283;
    LaunchMediaPlayer          = SpecialKeysOffset - 0284;
    LaunchMusicPlayer          = SpecialKeysOffset - 0285;
    LaunchMyComputer           = SpecialKeysOffset - 0286;
    LaunchPhone                = SpecialKeysOffset - 0287;
    LaunchScreenSaver          = SpecialKeysOffset - 0288;
    LaunchSpreadsheet          = SpecialKeysOffset - 0289;
    LaunchWebBrowser           = SpecialKeysOffset - 0290;
    LaunchWebCam               = SpecialKeysOffset - 0291;
    LaunchWordProcessor        = SpecialKeysOffset - 0292;
    LaunchApplication1         = SpecialKeysOffset - 0293;
    LaunchApplication2         = SpecialKeysOffset - 0294;
    LaunchApplication3         = SpecialKeysOffset - 0295;
    LaunchApplication4         = SpecialKeysOffset - 0296;
    LaunchApplication5         = SpecialKeysOffset - 0297;
    LaunchApplication6         = SpecialKeysOffset - 0298;
    LaunchApplication7         = SpecialKeysOffset - 0299;
    LaunchApplication8         = SpecialKeysOffset - 0300;
    LaunchApplication9         = SpecialKeysOffset - 0301;
    LaunchApplication10        = SpecialKeysOffset - 0302;
    LaunchApplication11        = SpecialKeysOffset - 0303;
    LaunchApplication12        = SpecialKeysOffset - 0304;
    LaunchApplication13        = SpecialKeysOffset - 0305;
    LaunchApplication14        = SpecialKeysOffset - 0306;
    LaunchApplication15        = SpecialKeysOffset - 0307;
    LaunchApplication16        = SpecialKeysOffset - 0308;

     // Browser Control keys
    BrowserBack                = SpecialKeysOffset - 0309;
    BrowserFavorites           = SpecialKeysOffset - 0310;
    BrowserForward             = SpecialKeysOffset - 0311;
    BrowserHome                = SpecialKeysOffset - 0312;
    BrowserRefresh             = SpecialKeysOffset - 0313;
    BrowserSearch              = SpecialKeysOffset - 0314;
    BrowserStop                = SpecialKeysOffset - 0315;

    // Numeric keypad
    Numpad0                    = SpecialKeysOffset - 0316;
    Numpad1                    = SpecialKeysOffset - 0317;
    Numpad2                    = SpecialKeysOffset - 0318;
    Numpad3                    = SpecialKeysOffset - 0319;
    Numpad4                    = SpecialKeysOffset - 0320;
    Numpad5                    = SpecialKeysOffset - 0321;
    Numpad6                    = SpecialKeysOffset - 0322;
    Numpad7                    = SpecialKeysOffset - 0323;
    Numpad8                    = SpecialKeysOffset - 0324;
    Numpad9                    = SpecialKeysOffset - 0325;
    NumpadMultiply             = SpecialKeysOffset - 0326;
    NumpadAdd                  = SpecialKeysOffset - 0327;
    NumpadSeparator            = SpecialKeysOffset - 0328;
    NumpadSubtract             = SpecialKeysOffset - 0329;
    NumpadDecimal              = SpecialKeysOffset - 0330;
    NumpadDivide               = SpecialKeysOffset - 0331;


    // Left/Right
    LeftMeta                   = Meta;
    LeftAlta                   = Alt;
    LeftShift                  = Shift;
    LeftControl                = Control;
    RightMeta                  = SpecialKeysOffset - 0332;
    RightAlt                   = SpecialKeysOffset - 0333;
    RightShift                 = SpecialKeysOffset - 0334;
    RightControl               = SpecialKeysOffset - 0335;

    F21                        = SpecialKeysOffset - 0336;
    F22                        = SpecialKeysOffset - 0337;
    F23                        = SpecialKeysOffset - 0338;
    F24                        = SpecialKeysOffset - 0339;
    // 0340 is kanjimode (see above)
    class Property Names[aKey : Integer] : string read GetKeyName;
  end;

  { TCountSpecialKeys }

  TCountSpecialKeys = class(TSpecialKeyEnumerator)
  private
    class var _cached_count : Integer;
  private
    FCount : integer;
  Protected
    Procedure EnumKey(aCode : Integer; aName : string); override;
  public
    class function GetCount : integer;
  end;


  { TSpecialKeysArrayCreator }

  TSpecialKeysArrayCreator = class(TSpecialKeyEnumerator)
  private
    class var _keyarray : TKeyCodeNameArray;
  private
    FIdx : integer;
    FArray : TKeyCodeNameArray;
  Protected
    Procedure EnumKey(aCode : Integer; aName : string); override;
  public
    class function GetArray : TKeyCodeNameArray;
  end;


procedure EnumSpecialKeys(aCallback : TEnumKeysCallback);
function GetSpecialKeyCount : Integer;
function GetSpecialKeyArray : TKeyCodeNameArray;
function GetSpecialKeyName(aCode : Integer) : String;

implementation

uses UTF8Utils;

function GetSpecialKeyCount : Integer;

begin
  Result:=TCountSpecialKeys.GetCount;
end;

function GetSpecialKeyArray : TKeyCodeNameArray;

begin
  Result:=TSpecialKeysArrayCreator.GetArray;
end;

function GetSpecialKeyName(aCode : Integer) : String;

begin
  Result:=TKeyCodes.Names[aCode];
end;

procedure EnumSpecialKeys(aCallback : TEnumKeysCallback);

begin
  aCallBack(TKeyCodes.Alt ,TKeyNames.Alt );
  aCallBack(TKeyCodes.AltGraph ,TKeyNames.AltGraph );
  aCallBack(TKeyCodes.CapsLock ,TKeyNames.CapsLock );
  aCallBack(TKeyCodes.Control ,TKeyNames.Control );
  aCallBack(TKeyCodes.Fn ,TKeyNames.Fn );
  aCallBack(TKeyCodes.FnLock ,TKeyNames.FnLock );
  aCallBack(TKeyCodes.Hyper ,TKeyNames.Hyper );
  aCallBack(TKeyCodes.Meta ,TKeyNames.Meta );
  aCallBack(TKeyCodes.NumLock ,TKeyNames.NumLock );
  aCallBack(TKeyCodes.ScrollLock ,TKeyNames.ScrollLock );
  aCallBack(TKeyCodes.Shift ,TKeyNames.Shift );
  aCallBack(TKeyCodes.Super ,TKeyNames.Super );
  aCallBack(TKeyCodes.Symbol ,TKeyNames.Symbol );
  aCallBack(TKeyCodes.SymbolLock ,TKeyNames.SymbolLock );
  aCallBack(TKeyCodes.Enter ,TKeyNames.Enter );
  aCallBack(TKeyCodes.Tab ,TKeyNames.Tab );
  aCallBack(TKeyCodes.Space ,TKeyNames.Space );
  aCallBack(TKeyCodes.ArrowDown ,TKeyNames.ArrowDown );
  aCallBack(TKeyCodes.ArrowLeft ,TKeyNames.ArrowLeft );
  aCallBack(TKeyCodes.ArrowRight ,TKeyNames.ArrowRight );
  aCallBack(TKeyCodes.ArrowUp ,TKeyNames.ArrowUp );
  aCallBack(TKeyCodes.End_ ,TKeyNames.End_ );
  aCallBack(TKeyCodes.Home ,TKeyNames.Home );
  aCallBack(TKeyCodes.PageDown ,TKeyNames.PageDown );
  aCallBack(TKeyCodes.PageUp ,TKeyNames.PageUp );
  aCallBack(TKeyCodes.BackSpace ,TKeyNames.BackSpace );
  aCallBack(TKeyCodes.Clear ,TKeyNames.Clear );
  aCallBack(TKeyCodes.Copy ,TKeyNames.Copy );
  aCallBack(TKeyCodes.CrSel ,TKeyNames.CrSel );
  aCallBack(TKeyCodes.Cut ,TKeyNames.Cut );
  aCallBack(TKeyCodes.Delete ,TKeyNames.Delete );
  aCallBack(TKeyCodes.EraseEof ,TKeyNames.EraseEof );
  aCallBack(TKeyCodes.ExSel ,TKeyNames.ExSel );
  aCallBack(TKeyCodes.Insert ,TKeyNames.Insert );
  aCallBack(TKeyCodes.Paste ,TKeyNames.Paste );
  aCallBack(TKeyCodes.Redo ,TKeyNames.Redo );
  aCallBack(TKeyCodes.Undo ,TKeyNames.Undo );
  aCallBack(TKeyCodes.Accept ,TKeyNames.Accept );
  aCallBack(TKeyCodes.Again ,TKeyNames.Again );
  aCallBack(TKeyCodes.Attn ,TKeyNames.Attn );
  aCallBack(TKeyCodes.Cancel ,TKeyNames.Cancel );
  aCallBack(TKeyCodes.ContextMenu ,TKeyNames.ContextMenu );
  aCallBack(TKeyCodes.Escape ,TKeyNames.Escape );
  aCallBack(TKeyCodes.Execute ,TKeyNames.Execute );
  aCallBack(TKeyCodes.Find ,TKeyNames.Find );
  aCallBack(TKeyCodes.Finish ,TKeyNames.Finish );
  aCallBack(TKeyCodes.Help ,TKeyNames.Help );
  aCallBack(TKeyCodes.Pause ,TKeyNames.Pause );
  aCallBack(TKeyCodes.Play ,TKeyNames.Play );
  aCallBack(TKeyCodes.Props ,TKeyNames.Props );
  aCallBack(TKeyCodes.Select ,TKeyNames.Select );
  aCallBack(TKeyCodes.ZoomIn ,TKeyNames.ZoomIn );
  aCallBack(TKeyCodes.ZoomOut ,TKeyNames.ZoomOut );
  aCallBack(TKeyCodes.BrightnessDown ,TKeyNames.BrightnessDown );
  aCallBack(TKeyCodes.BrightnessUp ,TKeyNames.BrightnessUp );
  aCallBack(TKeyCodes.Eject ,TKeyNames.Eject );
  aCallBack(TKeyCodes.LogOff ,TKeyNames.LogOff );
  aCallBack(TKeyCodes.Power ,TKeyNames.Power );
  aCallBack(TKeyCodes.PowerOff ,TKeyNames.PowerOff );
  aCallBack(TKeyCodes.PrintScreen ,TKeyNames.PrintScreen );
  aCallBack(TKeyCodes.Hibernate ,TKeyNames.Hibernate );
  aCallBack(TKeyCodes.Standby ,TKeyNames.Standby );
  aCallBack(TKeyCodes.WakeUp ,TKeyNames.WakeUp );
  aCallBack(TKeyCodes.AllCandidates ,TKeyNames.AllCandidates );
  aCallBack(TKeyCodes.Alphanumeric ,TKeyNames.Alphanumeric );
  aCallBack(TKeyCodes.CodeInput ,TKeyNames.CodeInput );
  aCallBack(TKeyCodes.Compose ,TKeyNames.Compose );
  aCallBack(TKeyCodes.Convert ,TKeyNames.Convert );
  aCallBack(TKeyCodes.Dead ,TKeyNames.Dead );
  aCallBack(TKeyCodes.FinalMode ,TKeyNames.FinalMode );
  aCallBack(TKeyCodes.GroupFirst ,TKeyNames.GroupFirst );
  aCallBack(TKeyCodes.GroupLast ,TKeyNames.GroupLast );
  aCallBack(TKeyCodes.GroupNext ,TKeyNames.GroupNext );
  aCallBack(TKeyCodes.GroupPrevious ,TKeyNames.GroupPrevious );
  aCallBack(TKeyCodes.ModeChange ,TKeyNames.ModeChange );
  aCallBack(TKeyCodes.NextCandidate ,TKeyNames.NextCandidate );
  aCallBack(TKeyCodes.NonConvert ,TKeyNames.NonConvert );
  aCallBack(TKeyCodes.PreviousCandidate ,TKeyNames.PreviousCandidate );
  aCallBack(TKeyCodes.Process ,TKeyNames.Process );
  aCallBack(TKeyCodes.SingleCandidate ,TKeyNames.SingleCandidate );
  aCallBack(TKeyCodes.HangulMode ,TKeyNames.HangulMode );
  aCallBack(TKeyCodes.HanjaMode ,TKeyNames.HanjaMode );
  aCallBack(TKeyCodes.JunjaMode ,TKeyNames.JunjaMode );
  aCallBack(TKeyCodes.Eisu ,TKeyNames.Eisu );
  aCallBack(TKeyCodes.Hankaku ,TKeyNames.Hankaku );
  aCallBack(TKeyCodes.Hiranga ,TKeyNames.Hiranga );
  aCallBack(TKeyCodes.HirangaKatakana ,TKeyNames.HirangaKatakana );
  aCallBack(TKeyCodes.KanaMode ,TKeyNames.KanaMode );
  aCallBack(TKeyCodes.Katakana ,TKeyNames.Katakana );
  aCallBack(TKeyCodes.Romaji ,TKeyNames.Romaji );
  aCallBack(TKeyCodes.Zenkaku ,TKeyNames.Zenkaku );
  aCallBack(TKeyCodes.ZenkakuHankaku ,TKeyNames.ZenkakuHanaku );
  aCallBack(TKeyCodes.F1 ,TKeyNames.F1 );
  aCallBack(TKeyCodes.F2 ,TKeyNames.F2 );
  aCallBack(TKeyCodes.F3 ,TKeyNames.F3 );
  aCallBack(TKeyCodes.F4 ,TKeyNames.F4 );
  aCallBack(TKeyCodes.F5 ,TKeyNames.F5 );
  aCallBack(TKeyCodes.F6 ,TKeyNames.F6 );
  aCallBack(TKeyCodes.F7 ,TKeyNames.F7 );
  aCallBack(TKeyCodes.F8 ,TKeyNames.F8 );
  aCallBack(TKeyCodes.F9 ,TKeyNames.F9 );
  aCallBack(TKeyCodes.F10 ,TKeyNames.F10 );
  aCallBack(TKeyCodes.F11 ,TKeyNames.F11 );
  aCallBack(TKeyCodes.F12 ,TKeyNames.F12 );
  aCallBack(TKeyCodes.F13 ,TKeyNames.F13 );
  aCallBack(TKeyCodes.F14 ,TKeyNames.F14 );
  aCallBack(TKeyCodes.F15 ,TKeyNames.F15 );
  aCallBack(TKeyCodes.F16 ,TKeyNames.F16 );
  aCallBack(TKeyCodes.F17 ,TKeyNames.F17 );
  aCallBack(TKeyCodes.F18 ,TKeyNames.F18 );
  aCallBack(TKeyCodes.F19 ,TKeyNames.F19 );
  aCallBack(TKeyCodes.F20 ,TKeyNames.F20 );
  aCallBack(TKeyCodes.Soft1 ,TKeyNames.Soft1 );
  aCallBack(TKeyCodes.Soft2 ,TKeyNames.Soft2 );
  aCallBack(TKeyCodes.Soft3 ,TKeyNames.Soft3 );
  aCallBack(TKeyCodes.Soft4 ,TKeyNames.Soft4 );
  aCallBack(TKeyCodes.Decimal ,TKeyNames.Decimal );
  aCallBack(TKeyCodes.Key11 ,TKeyNames.Key11 );
  aCallBack(TKeyCodes.Key12 ,TKeyNames.Key12 );
  aCallBack(TKeyCodes.Multiply ,TKeyNames.Multiply );
  aCallBack(TKeyCodes.Add ,TKeyNames.Add );
  aCallBack(TKeyCodes.NumClear ,TKeyNames.NumClear );
  aCallBack(TKeyCodes.Divide ,TKeyNames.Divide );
  aCallBack(TKeyCodes.Subtract ,TKeyNames.Subtract );
  aCallBack(TKeyCodes.Separator ,TKeyNames.Separator );
  aCallBack(TKeyCodes.AppSwitch ,TKeyNames.AppSwitch );
  aCallBack(TKeyCodes.Call ,TKeyNames.Call );
  aCallBack(TKeyCodes.Camera ,TKeyNames.Camera );
  aCallBack(TKeyCodes.CameraFocus ,TKeyNames.CameraFocus );
  aCallBack(TKeyCodes.EndCall ,TKeyNames.EndCall );
  aCallBack(TKeyCodes.GoBack ,TKeyNames.GoBack );
  aCallBack(TKeyCodes.GoHome ,TKeyNames.GoHome );
  aCallBack(TKeyCodes.HeadsetHook ,TKeyNames.HeadsetHook );
  aCallBack(TKeyCodes.LastNumberRedial ,TKeyNames.LastNumberRedial );
  aCallBack(TKeyCodes.Notification ,TKeyNames.Notification );
  aCallBack(TKeyCodes.MannerMode ,TKeyNames.MannerMode );
  aCallBack(TKeyCodes.VoiceDial ,TKeyNames.VoiceDial );
  aCallBack(TKeyCodes.ChannelDown ,TKeyNames.ChannelDown );
  aCallBack(TKeyCodes.ChannelUp ,TKeyNames.ChannelUp );
  aCallBack(TKeyCodes.MediaFastForward ,TKeyNames.MediaFastForward );
  aCallBack(TKeyCodes.MediaPause ,TKeyNames.MediaPause );
  aCallBack(TKeyCodes.MediaPlay ,TKeyNames.MediaPlay );
  aCallBack(TKeyCodes.MediaPlayPause ,TKeyNames.MediaPlayPause );
  aCallBack(TKeyCodes.MediaRecord ,TKeyNames.MediaRecord );
  aCallBack(TKeyCodes.MediaRewind ,TKeyNames.MediaRewind );
  aCallBack(TKeyCodes.MediaStop ,TKeyNames.MediaStop );
  aCallBack(TKeyCodes.MediaTrackNext ,TKeyNames.MediaTrackNext );
  aCallBack(TKeyCodes.MediaTrackPrevious ,TKeyNames.MediaTrackPrevious );
  aCallBack(TKeyCodes.AudioBalanceLeft ,TKeyNames.AudioBalanceLeft );
  aCallBack(TKeyCodes.AudioBalanceRight ,TKeyNames.AudioBalanceRight );
  aCallBack(TKeyCodes.AudioBassDown ,TKeyNames.AudioBassDown );
  aCallBack(TKeyCodes.AudioBassBoostDown ,TKeyNames.AudioBassBoostDown );
  aCallBack(TKeyCodes.AudioBassBoostToggle ,TKeyNames.AudioBassBoostToggle );
  aCallBack(TKeyCodes.AudioBassBoostUp ,TKeyNames.AudioBassBoostUp );
  aCallBack(TKeyCodes.AudioBassUp ,TKeyNames.AudioBassUp );
  aCallBack(TKeyCodes.AudioFaderFront ,TKeyNames.AudioFaderFront );
  aCallBack(TKeyCodes.AudioFaderRear ,TKeyNames.AudioFaderRear );
  aCallBack(TKeyCodes.AudioSurroundModeNext ,TKeyNames.AudioSurroundModeNext );
  aCallBack(TKeyCodes.AudioTrebleDown ,TKeyNames.AudioTrebleDown );
  aCallBack(TKeyCodes.AudioTrebleUp ,TKeyNames.AudioTrebleUp );
  aCallBack(TKeyCodes.AudioVolumeDown ,TKeyNames.AudioVolumeDown );
  aCallBack(TKeyCodes.AudioVolumeMute ,TKeyNames.AudioVolumeMute );
  aCallBack(TKeyCodes.AudioVolumeUp ,TKeyNames.AudioVolumeUp );
  aCallBack(TKeyCodes.MicrophoneToggle ,TKeyNames.MicrophoneToggle );
  aCallBack(TKeyCodes.MicrophoneVolumeDown ,TKeyNames.MicrophoneVolumeDown );
  aCallBack(TKeyCodes.MicrophoneVolumeMute ,TKeyNames.MicrophoneVolumeMute );
  aCallBack(TKeyCodes.MicrophoneVolumeUp ,TKeyNames.MicrophoneVolumeUp );
  aCallBack(TKeyCodes.TV ,TKeyNames.TV );
  aCallBack(TKeyCodes.TV3DMode ,TKeyNames.TV3DMode );
  aCallBack(TKeyCodes.TVAntennaCable ,TKeyNames.TVAntennaCable );
  aCallBack(TKeyCodes.TVAudioDescription ,TKeyNames.TVAudioDescription );
  aCallBack(TKeyCodes.TVAudioDescriptionMixDown ,TKeyNames.TVAudioDescriptionMixDown );
  aCallBack(TKeyCodes.TVAudioDescriptionMixUp ,TKeyNames.TVAudioDescriptionMixUp );
  aCallBack(TKeyCodes.TVContentsMenu ,TKeyNames.TVContentsMenu );
  aCallBack(TKeyCodes.TVDataService ,TKeyNames.TVDataService );
  aCallBack(TKeyCodes.TVInput ,TKeyNames.TVInput );
  aCallBack(TKeyCodes.TVInputComponent1 ,TKeyNames.TVInputComponent1 );
  aCallBack(TKeyCodes.TVInputComponent2 ,TKeyNames.TVInputComponent2 );
  aCallBack(TKeyCodes.TVInputComposite1 ,TKeyNames.TVInputComposite1 );
  aCallBack(TKeyCodes.TVInputComposite2 ,TKeyNames.TVInputComposite2 );
  aCallBack(TKeyCodes.TVInputHDMI1 ,TKeyNames.TVInputHDMI1 );
  aCallBack(TKeyCodes.TVInputHDMI2 ,TKeyNames.TVInputHDMI2 );
  aCallBack(TKeyCodes.TVInputHDMI3 ,TKeyNames.TVInputHDMI3 );
  aCallBack(TKeyCodes.TVInputHDMI4 ,TKeyNames.TVInputHDMI4 );
  aCallBack(TKeyCodes.TVInputVGA1 ,TKeyNames.TVInputVGA1 );
  aCallBack(TKeyCodes.TVMediaContext ,TKeyNames.TVMediaContext );
  aCallBack(TKeyCodes.TVNetwork ,TKeyNames.TVNetwork );
  aCallBack(TKeyCodes.TVNumberEntry ,TKeyNames.TVNumberEntry );
  aCallBack(TKeyCodes.TVPower ,TKeyNames.TVPower );
  aCallBack(TKeyCodes.TVRadioService ,TKeyNames.TVRadioService );
  aCallBack(TKeyCodes.TVSatellite ,TKeyNames.TVSatellite );
  aCallBack(TKeyCodes.TVSatelliteBS ,TKeyNames.TVSatelliteBS );
  aCallBack(TKeyCodes.TVSatelliteCS ,TKeyNames.TVSatelliteCS );
  aCallBack(TKeyCodes.TVSatelliteToggle ,TKeyNames.TVSatelliteToggle );
  aCallBack(TKeyCodes.TVTerrestrialAnalog ,TKeyNames.TVTerrestrialAnalog );
  aCallBack(TKeyCodes.TVTerrestrialDigital ,TKeyNames.TVTerrestrialDigital );
  aCallBack(TKeyCodes.TVTimer ,TKeyNames.TVTimer );
  aCallBack(TKeyCodes.AVRInput ,TKeyNames.AVRInput );
  aCallBack(TKeyCodes.AVRPower ,TKeyNames.AVRPower );
  aCallBack(TKeyCodes.ColorF0Red ,TKeyNames.ColorF0Red );
  aCallBack(TKeyCodes.ColorF1Green ,TKeyNames.ColorF1Green );
  aCallBack(TKeyCodes.ColorF2Yellow ,TKeyNames.ColorF2Yellow );
  aCallBack(TKeyCodes.ColorF3Blue ,TKeyNames.ColorF3Blue );
  aCallBack(TKeyCodes.ColorF4Grey ,TKeyNames.ColorF4Grey );
  aCallBack(TKeyCodes.ColorF5Brown ,TKeyNames.ColorF5Brown );
  aCallBack(TKeyCodes.ClosedCaptionToggle ,TKeyNames.ClosedCaptionToggle );
  aCallBack(TKeyCodes.Dimmer ,TKeyNames.Dimmer );
  aCallBack(TKeyCodes.DisplaySwap ,TKeyNames.DisplaySwap );
  aCallBack(TKeyCodes.DVR ,TKeyNames.DVR );
  aCallBack(TKeyCodes.Exit ,TKeyNames.Exit );
  aCallBack(TKeyCodes.FavoriteClear0 ,TKeyNames.FavoriteClear0 );
  aCallBack(TKeyCodes.FavoriteClear1 ,TKeyNames.FavoriteClear1 );
  aCallBack(TKeyCodes.FavoriteClear2 ,TKeyNames.FavoriteClear2 );
  aCallBack(TKeyCodes.FavoriteClear3 ,TKeyNames.FavoriteClear3 );
  aCallBack(TKeyCodes.FavoriteRecall0 ,TKeyNames.FavoriteRecall0 );
  aCallBack(TKeyCodes.FavoriteRecall1 ,TKeyNames.FavoriteRecall1 );
  aCallBack(TKeyCodes.FavoriteRecall2 ,TKeyNames.FavoriteRecall2 );
  aCallBack(TKeyCodes.FavoriteRecall3 ,TKeyNames.FavoriteRecall3 );
  aCallBack(TKeyCodes.FavoriteStore0 ,TKeyNames.FavoriteStore0 );
  aCallBack(TKeyCodes.FavoriteStore1 ,TKeyNames.FavoriteStore1 );
  aCallBack(TKeyCodes.FavoriteStore2 ,TKeyNames.FavoriteStore2 );
  aCallBack(TKeyCodes.FavoriteStore3 ,TKeyNames.FavoriteStore3 );
  aCallBack(TKeyCodes.Guide ,TKeyNames.Guide );
  aCallBack(TKeyCodes.GuideNextDay ,TKeyNames.GuideNextDay );
  aCallBack(TKeyCodes.GuidePreviousDay ,TKeyNames.GuidePreviousDay );
  aCallBack(TKeyCodes.Info ,TKeyNames.Info );
  aCallBack(TKeyCodes.InstantReplay ,TKeyNames.InstantReplay );
  aCallBack(TKeyCodes.Link ,TKeyNames.Link );
  aCallBack(TKeyCodes.ListProgram ,TKeyNames.ListProgram );
  aCallBack(TKeyCodes.LiveContent ,TKeyNames.LiveContent );
  aCallBack(TKeyCodes.Lock ,TKeyNames.Lock );
  aCallBack(TKeyCodes.MediaApps ,TKeyNames.MediaApps );
  aCallBack(TKeyCodes.MediaAudioTrack ,TKeyNames.MediaAudioTrack );
  aCallBack(TKeyCodes.MediaLast ,TKeyNames.MediaLast );
  aCallBack(TKeyCodes.MediaSkipBackward ,TKeyNames.MediaSkipBackward );
  aCallBack(TKeyCodes.MediaSkipForward ,TKeyNames.MediaSkipForward );
  aCallBack(TKeyCodes.MediaStepBackward ,TKeyNames.MediaStepBackward );
  aCallBack(TKeyCodes.MediaStepForward ,TKeyNames.MediaStepForward );
  aCallBack(TKeyCodes.MediaTopMenu ,TKeyNames.MediaTopMenu );
  aCallBack(TKeyCodes.NavigateIn ,TKeyNames.NavigateIn );
  aCallBack(TKeyCodes.NavigateNext ,TKeyNames.NavigateNext );
  aCallBack(TKeyCodes.NavigateOut ,TKeyNames.NavigateOut );
  aCallBack(TKeyCodes.NavigatePrevious ,TKeyNames.NavigatePrevious );
  aCallBack(TKeyCodes.NextFavoriteChannel ,TKeyNames.NextFavoriteChannel );
  aCallBack(TKeyCodes.NextUserProfile ,TKeyNames.NextUserProfile );
  aCallBack(TKeyCodes.OnDemand ,TKeyNames.OnDemand );
  aCallBack(TKeyCodes.Pairing ,TKeyNames.Pairing );
  aCallBack(TKeyCodes.PinPDown ,TKeyNames.PinPDown );
  aCallBack(TKeyCodes.PinPMove ,TKeyNames.PinPMove );
  aCallBack(TKeyCodes.PinPToggle ,TKeyNames.PinPToggle );
  aCallBack(TKeyCodes.PinPUp ,TKeyNames.PinPUp );
  aCallBack(TKeyCodes.PlaySpeedDown ,TKeyNames.PlaySpeedDown );
  aCallBack(TKeyCodes.PlaySpeedReset ,TKeyNames.PlaySpeedReset );
  aCallBack(TKeyCodes.PlaySpeedUp ,TKeyNames.PlaySpeedUp );
  aCallBack(TKeyCodes.RandomToggle ,TKeyNames.RandomToggle );
  aCallBack(TKeyCodes.RcLowBattery ,TKeyNames.RcLowBattery );
  aCallBack(TKeyCodes.RecordSpeedNext ,TKeyNames.RecordSpeedNext );
  aCallBack(TKeyCodes.RfBypass ,TKeyNames.RfBypass );
  aCallBack(TKeyCodes.ScanChannelsToggle ,TKeyNames.ScanChannelsToggle );
  aCallBack(TKeyCodes.ScreenModeNext ,TKeyNames.ScreenModeNext );
  aCallBack(TKeyCodes.Settings ,TKeyNames.Settings );
  aCallBack(TKeyCodes.SplitScreenToggle ,TKeyNames.SplitScreenToggle );
  aCallBack(TKeyCodes.STBInput ,TKeyNames.STBInput );
  aCallBack(TKeyCodes.STBPower ,TKeyNames.STBPower );
  aCallBack(TKeyCodes.Subtitle ,TKeyNames.Subtitle );
  aCallBack(TKeyCodes.Teletext ,TKeyNames.Teletext );
  aCallBack(TKeyCodes.VideoModeNext ,TKeyNames.VideoModeNext );
  aCallBack(TKeyCodes.Wink ,TKeyNames.Wink );
  aCallBack(TKeyCodes.ZoomToggle ,TKeyNames.ZoomToggle );
  aCallBack(TKeyCodes.SpeechCorrectionList ,TKeyNames.SpeechCorrectionList );
  aCallBack(TKeyCodes.SpeechInputToggle ,TKeyNames.SpeechInputToggle );
  aCallBack(TKeyCodes.Close ,TKeyNames.Close );
  aCallBack(TKeyCodes.New ,TKeyNames.New );
  aCallBack(TKeyCodes.Open ,TKeyNames.Open );
  aCallBack(TKeyCodes.Print ,TKeyNames.Print );
  aCallBack(TKeyCodes.Save ,TKeyNames.Save );
  aCallBack(TKeyCodes.SpellCheck ,TKeyNames.SpellCheck );
  aCallBack(TKeyCodes.MailForward ,TKeyNames.MailForward );
  aCallBack(TKeyCodes.MailReply ,TKeyNames.MailReply );
  aCallBack(TKeyCodes.MailSend ,TKeyNames.MailSend );
  aCallBack(TKeyCodes.LaunchCalculator ,TKeyNames.LaunchCalculator );
  aCallBack(TKeyCodes.LaunchCalendar ,TKeyNames.LaunchCalendar );
  aCallBack(TKeyCodes.LaunchContacts ,TKeyNames.LaunchContacts );
  aCallBack(TKeyCodes.LaunchMail ,TKeyNames.LaunchMail );
  aCallBack(TKeyCodes.LaunchMediaPlayer ,TKeyNames.LaunchMediaPlayer );
  aCallBack(TKeyCodes.LaunchMusicPlayer ,TKeyNames.LaunchMusicPlayer );
  aCallBack(TKeyCodes.LaunchMyComputer ,TKeyNames.LaunchMyComputer );
  aCallBack(TKeyCodes.LaunchPhone ,TKeyNames.LaunchPhone );
  aCallBack(TKeyCodes.LaunchScreenSaver ,TKeyNames.LaunchScreenSaver );
  aCallBack(TKeyCodes.LaunchSpreadsheet ,TKeyNames.LaunchSpreadsheet );
  aCallBack(TKeyCodes.LaunchWebBrowser ,TKeyNames.LaunchWebBrowser );
  aCallBack(TKeyCodes.LaunchWebCam ,TKeyNames.LaunchWebCam );
  aCallBack(TKeyCodes.LaunchWordProcessor ,TKeyNames.LaunchWordProcessor );
  aCallBack(TKeyCodes.LaunchApplication1 ,TKeyNames.LaunchApplication1 );
  aCallBack(TKeyCodes.LaunchApplication2 ,TKeyNames.LaunchApplication2 );
  aCallBack(TKeyCodes.LaunchApplication3 ,TKeyNames.LaunchApplication3 );
  aCallBack(TKeyCodes.LaunchApplication4 ,TKeyNames.LaunchApplication4 );
  aCallBack(TKeyCodes.LaunchApplication5 ,TKeyNames.LaunchApplication5 );
  aCallBack(TKeyCodes.LaunchApplication6 ,TKeyNames.LaunchApplication6 );
  aCallBack(TKeyCodes.LaunchApplication7 ,TKeyNames.LaunchApplication7 );
  aCallBack(TKeyCodes.LaunchApplication8 ,TKeyNames.LaunchApplication8 );
  aCallBack(TKeyCodes.LaunchApplication9 ,TKeyNames.LaunchApplication9 );
  aCallBack(TKeyCodes.LaunchApplication10 ,TKeyNames.LaunchApplication10 );
  aCallBack(TKeyCodes.LaunchApplication11 ,TKeyNames.LaunchApplication11 );
  aCallBack(TKeyCodes.LaunchApplication12 ,TKeyNames.LaunchApplication12 );
  aCallBack(TKeyCodes.LaunchApplication13 ,TKeyNames.LaunchApplication13 );
  aCallBack(TKeyCodes.LaunchApplication14 ,TKeyNames.LaunchApplication14 );
  aCallBack(TKeyCodes.LaunchApplication15 ,TKeyNames.LaunchApplication15 );
  aCallBack(TKeyCodes.LaunchApplication16 ,TKeyNames.LaunchApplication16 );
  aCallBack(TKeyCodes.BrowserBack ,TKeyNames.BrowserBack );
  aCallBack(TKeyCodes.BrowserFavorites ,TKeyNames.BrowserFavorites );
  aCallBack(TKeyCodes.BrowserForward ,TKeyNames.BrowserForward );
  aCallBack(TKeyCodes.BrowserHome ,TKeyNames.BrowserHome );
  aCallBack(TKeyCodes.BrowserRefresh ,TKeyNames.BrowserRefresh );
  aCallBack(TKeyCodes.BrowserSearch ,TKeyNames.BrowserSearch );
  aCallBack(TKeyCodes.BrowserStop ,TKeyNames.BrowserStop );
end;

{ TKeyCodes }

// Once we have sortbase, this can disappear.
type
  TIntegerArray = array of Integer;

procedure Swap(var a, b: TKeyCodeName); inline;
var
  temp: TKeyCodeName;
begin
  temp := a;
  a := b;
  b := temp;
end;

function Partition(var arr: TKeyCodeNameArray; low, high: Integer): Integer;
var
  pivot : TKeyCodeName;
  i, j: Integer;
begin
  pivot := arr[high];
  i := low - 1;

  for j := low to high - 1 do
  begin
    if arr[j].Code <= pivot.code then
    begin
      Inc(i);
      Swap(arr[i], arr[j]);
    end;
  end;

  Swap(arr[i + 1], arr[high]);
  Result := i + 1;
end;

procedure QuickSort(var arr: TKeyCodeNameArray; low, high: Integer);
var
  pi: Integer;
begin
  if low < high then
  begin
    pi := Partition(arr, low, high);
    QuickSort(arr, low, pi - 1);
    QuickSort(arr, pi + 1, high);
  end;
end;

class function TKeyCodes.indexOf(aKeyCode: Integer): Integer;

var
  left, right, mid: Integer;
begin
  left:=0;
  right:=Length(FSortedKeys)-1;
  while (left<=right) do
    begin
    mid:=left+(right-left) div 2;
    if (FSortedKeys[mid].Code=aKeyCode) then
      System.Exit(mid); // Target found, return index
    if FSortedKeys[mid].code < aKeyCode then
      left := mid + 1
    else
      right := mid - 1;
    end;
    Result := -1; // Target not found
end;

class function TKeyCodes.GetKeyName(aKeyCode: Integer): String;
var
  Idx : integer;
begin
  if aKeyCode>0 then
    Result:=UTF8Utils.UnicodeToUTF8(aKeyCode)
  else
    begin
    Result:='';
    if FSortedKeys=Nil then
      begin
      FSortedKeys:=GetSpecialKeyArray;
      QuickSort(FSortedKeys,0,Length(FSortedKeys));
      end;
    Idx:=IndexOf(aKeyCode);
    if Idx<>-1 then
      Result:=FSortedKeys[Idx].Name
    end;
end;

{ TCountSpecialKeys }

procedure TCountSpecialKeys.EnumKey(aCode: Integer; aName: string);
begin
  inc(FCount);
end;

class function TCountSpecialKeys.GetCount: integer;
begin
  if (_cached_count=0) then
    With TCountSpecialKeys.Create do
      try
        EnumSpecialKeys(@EnumKey);
        _cached_count:=FCount;
      finally
        Free;
      end;
  Result:=_cached_count;
end;

{ TSpecialKeysArrayCreator }

procedure TSpecialKeysArrayCreator.EnumKey(aCode: Integer; aName: string);
begin
  FArray[FIdx].Code:=ACode;
  FArray[FIdx].Name:=AName;
  Inc(FIdx);
end;

class function TSpecialKeysArrayCreator.GetArray: TKeyCodeNameArray;
begin
  if _keyarray=Nil then
    With TSpecialKeysArrayCreator.Create do
      try
        SetLength(FArray,GetSpecialKeyCount);
        EnumSpecialKeys(@EnumKey);
        _KeyArray:=FArray;
      finally
        Free;
      end;
  Result:=_keyArray;
end;


end.

