{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Image handling classes for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}
unit Fresnel.Images;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, StrUtils, Contnrs, Fresnel.Classes, fpImage;

Const
  DefaultImageExtension = '.png';
  DefaultImageResolution = 96;
  DefaultIconSize = 24;

Type
  EImageData = class(EFresnel);

  { TImagesConfig }

  TImageConfigOption = (icoSizeDir,icoResolutionDir,icoSizeFirst);
  TImageConfigOptions = set of TImageConfigOption;

  TImagesConfig = Class(TPersistent)
  private
    FIconSize: Word;
    FImageClass: TFPCustomImageClass;
    FImageDir: String;
    FIMageExtension: String;
    FResolution: Word;
    FOptions: TImageConfigOptions;
    function GetClass: TFPCustomImageClass;
    procedure SetIconSize(AValue: Word);
    procedure SetImageDir(AValue: String);
    procedure SetImageExtension(AValue: String);
    procedure SetResolution(AValue: Word);
  Protected
    Function GetDefaultResolution : Word; virtual;
    Function GetDefaultImageDir : String; virtual;
    Function GetDefaultExtension : String; virtual;
    Function GetDefaultIconSize : Word; virtual;
    function GetSizedImageDir(aSize: Integer; aResolution: word): String; virtual;
    function DoGetSizedImageFileName(aImage: String; aSize: Integer; aResolution: word): String; virtual;
  Public
    Constructor Create; virtual;
    Property ImageClass : TFPCustomImageClass Read GetClass Write FImageClass;
    Function GetImageFileName(aImage : String; aResolution : Word = 0) : String;
    Function GetSizedImageFileName(aImage : String; aSize : Integer = 0; aResolution : word = 0) : String;
    Property Resolution : Word Read FResolution Write SetResolution;
    Property ImageDir : String Read FImageDir Write SetImageDir;
    Property ImageExtension : String Read FIMageExtension Write SetImageExtension;
    Property IconSize : Word Read FIconSize Write SetIconSize;
    Property Options : TImageConfigOptions Read FOptions Write Foptions;
  end;

  { TResolution }

  TResolution = class(TCollectionItem)
  private
    FDefault: Boolean;
    FResolution: Word;
    procedure SetDefault(AValue: Boolean);
    procedure SetResolution(AValue: Word);
  Published
    Property Resolution : Word Read FResolution Write SetResolution;
    Property Default : Boolean Read FDefault Write SetDefault;
  end;

  { TResolutionList }

  TResolutionList = class(TOwnedCollection)
  private
    function GetR(aIndex : Integer): TResolution;
    procedure SetR(aIndex : Integer; AValue: TResolution);
  Public
    Function FindDefaultResolution : TResolution;
    Function IndexOfResolution(aResolution :  Word) : Integer;
    Function AddResolution(aResolution : Word) : TResolution;
    Property Resolutions [aIndex : Integer] : TResolution Read GetR Write SetR; default;
  end;

  { TFPImageList }

  TFPImageList = Class(TObject)
  Private
    FList : TFPObjectList;
    function GetCount: Integer;
    function GetImage(Index: Integer): TFPCustomImage;
    procedure setCount(AValue: Integer);
    procedure SetImage(Index: Integer; AValue: TFPCustomImage);
  Public
    Constructor Create(aOwnsImages : Boolean);
    Destructor Destroy; override;
    Property Count : Integer Read GetCount Write setCount;
    Property Images[Index: Integer] : TFPCustomImage Read GetImage Write SetImage; default;
  end;

  { TBaseCustomImageList - This class provides no storage }

  TBaseCustomImageList = Class(TComponent)
  Private
    FAutoCreateMissingResolution: Boolean;
    FHeight: Word;
    FResolutions: TResolutionList;
    FWidth: Word;
    function GetImageCount: Integer;
    procedure SetDefaultResolution(AValue: Word);
    procedure SetHeight(AValue: Word);
    procedure SetResolutions(AValue: TResolutionList);
    procedure SetWidth(AValue: Word);
    procedure SetWidthHeight(aWidth, aHeight: Word);
  Protected
    Function DoGetImageCount(aResolution : Word) : Integer; virtual; abstract;
    Function DoIsValidImageIndex(aIndex : Integer; aResolution : Word) : Boolean; virtual; abstract;
    Function DoGetRawImage(aIndex : Integer; aResolution : Word; AllowCreate : Boolean = False) : TFPCustomImage; virtual; abstract;
    function CreateResolutions: TResolutionList; virtual;
    function DetermineApplicationResolution : Word; virtual;
    Function GetDefaultResolution : Word;
    Function GetIndexOfResolution (aResolution : Word) : Integer;
    Function CreateImage : TFPCustomImage;
  Public
    Constructor Create(aOwner : TComponent); override;
    Destructor Destroy; override;
    // Return true if the index is valid for default resolution
    Function IsValidImageIndex(aIndex : Integer) : Boolean;
    // Return true if the index is valid for given resolution
    Function IsValidImageIndex(aIndex : Integer; aResolution : Word) : Boolean;
    // Return original image with given index for default resolution. May be nil.
    // You may not free this image.
    Function GetRawImage(aIndex : Integer; aResolution : Word = 0) : TFPCustomImage;
    // Return image with given index for default resolution. You must free this image.
    function GetImage(aIndex : Integer) : TFPCustomImage;
    // Copy image with given index for default resolution into aImage.
    Function GetImage(aIndex : Integer; aImage : TFPCustomImage) : Boolean;
    // Return image with given index for given resolution. You must free this image.
    // Note that the image is blank if no actual image exists.
    Function GetImage(aIndex : Integer; aResolution : Word) : TFPCustomImage;
    // Copy image with given index for given resolution.
    // If the image does not exist, false is returned.
    function GetImage(aIndex: Integer; aResolution: Word; aImage: TFPCustomImage): Boolean;
    // Copy imagedata from aImage to image with given index and resolution.
    Procedure SetImage(aIndex : Integer; aResolution : Word; aImage : TFPCustomImage);
    // Copy imagedata from aImage to image with given index and default resolution.
    Procedure SetImage(aIndex : Integer; aImage : TFPCustomImage);
    // Load an image from file
    Function LoadImageFromFile(aIndex : Integer; aResolution : Word; const aFileName : string) : TFPCustomImage;
    // Load an image from Stream
    Function LoadImageFromStream(aIndex : Integer; aResolution : Word; const aStream : TStream)  : TFPCustomImage;
    // Count of images for default resolution.
    Property ImageCount : Integer Read GetImageCount;
    // Default resolution. Defaults to application resolution.
    Property DefaultResolution : Word Read GetDefaultResolution Write SetDefaultResolution;
    // List of resolutions in this list.
    Property Resolutions : TResolutionList Read FResolutions Write SetResolutions;
    // Image width for default resolution.
    Property Width : Word Read FWidth Write SetWidth;
    // Image width for default resolution.
    Property Height : Word Read FHeight Write SetHeight;
    Property AutoCreateMissingResolution : Boolean Read FAutoCreateMissingResolution Write FAutoCreateMissingResolution default true;
  end;

  { TCustomImageList - Simple storage }

  TCustomImageList = Class(TBaseCustomImageList)
  Private
    FLists : Array of TFPImageList;
  Protected
    function DoGetImageCount(aResolution: Word): Integer; override;
    function DoIsValidImageIndex(aIndex: Integer; aResolution: Word): Boolean; override;
    function GetResolutionList(aResolution: Word; allowCreate: Boolean): TFPImageList;
  Protected
    Function DoGetRawImage(aIndex : Integer; aResolution : Word; AllowCreate : Boolean = False) : TFPCustomImage; override;
  Public
    Destructor destroy; override;
  end;

  { TImageList }

  TImageList = class(TCustomImageList)
  Published
    Property DefaultResolution;
    Property Resolutions;
    Property Width;
    Property Height;
    Property AutoCreateMissingResolution;
  end;

  { TBaseCustomImageStore }

  TBaseCustomImageStore = class(TComponent)
  private
    Type
      TBaseCustomImageStoreClass = class of TBaseCustomImageStore;
  Private
    FResolution: Word;
    FSize: Word;
    class var _Instance : TBaseCustomImageStore;
  Protected
    class function GetGlobalInstance: TBaseCustomImageStore; static;
    procedure SetSize(AValue: Word); virtual;
    procedure SetResolution(AValue: Word); virtual;
    Procedure StoreImage(const aName : String; aImage : TFPCustomImage); virtual; abstract;
    function RetrieveImage(const aName : string; out aImage : TFPCustomImage) : boolean; virtual; abstract;
    function DoGetImageData(const aImageName: String; out aImage: TFPCustomImage; CopyData: Boolean): Boolean; virtual;
  Public
    class var InstanceClass : TBaseCustomImageStoreClass;
    Class Property GlobalInstance : TBaseCustomImageStore Read GetGlobalInstance;
  Public
    Constructor Create(aOwner : TComponent); override;
    Function IsValidImageName(const aImageName : string) : Boolean;
    Function GetImageData(const aImageName : String; out aImage : TFPCustomImage; CopyData : Boolean = False) : Boolean;
    Property Size : Word Read FSize Write SetSize;
    Property Resolution : Word Read FResolution Write SetResolution;
  end;

  { TCustomImageStore }

  TCustomImageStore = class(TBaseCustomImageStore)
  Private
    FCache : TFPObjectHashTable;
    function GetImageCount: Cardinal;
  Protected
    procedure SetSize(AValue: Word); override;
    procedure SetResolution(AValue: Word); override;
    Procedure StoreImage(const aName : String; aImage : TFPCustomImage); override;
    Function RetrieveImage(const aName : string; out aImage : TFPCustomImage) : Boolean; override;
  Public
    Constructor Create(aOwner: TComponent); override;
    Destructor Destroy; override;
    Procedure ClearCache;
    Property CachedImageCount : Cardinal Read GetImageCount;
  end;

  { TImageStore }

  TImageStore = class(TCustomImageStore)
  published
    Property Size;
    Property Resolution;
  end;

  { TImageData }

  TImageData = class(TPersistent)
  private
    Type
      TImageSource = (isData,isFile,isImageList,isStore);
  private
    FData: TFPCustomImage;
    FImageFile: String;
    FImageIndex: Integer;
    FImageList: TBaseCustomImageList;
    FImageName: String;
    FImageSource: TImageSource;
    FOwner : TComponent;
    function GetData: TFPCustomImage;
    function GetFileStored: Boolean;
    function GetHasData: Boolean;
    function GetHeight: Word;
    class function GetImageFileDir: String; static;
    function GetImageListStored: Boolean;
    function GetImageNameStored: Boolean;
    function GetResolvedData: TFPCustomImage;
    function GetWidth: Word;
    procedure ReadData(Reader: TReader);
    procedure SetImageFile(const aValue: String);
    procedure SetImageIndex(aValue: Integer);
    procedure SetImageList(aValue: TImageList);
    procedure SetImageList(AValue: TBaseCustomImageList);
    procedure SetImageName(const AValue: String);
    procedure WriteData(Writer: TWriter);
  Protected
    // The widgetset can set this to whatever class is most suitable for getting raw data.
    class var DataClass : TFPCustomImageClass;
    class Function CreateData(aWidth,aHeight : Word) : TFPCustomImage; virtual;
    function GetOwnerName : String; virtual;
    Procedure AllocateData(aWidth,aHeight : Word);
    Procedure ReplaceData(aData : TFPCustomImage);
    Procedure FreeData;
    function DetermineStore: TBaseCustomImageStore; virtual;
    function LoadDataFromStore(RaiseError: Boolean): boolean; virtual;
    function LoadDataFromFile(RaiseError: Boolean): boolean; virtual;
    function LoadDataFromImageList(RaiseError: Boolean): boolean; virtual;
    function ResolveData(RaiseError: Boolean): TFPCustomImage;
    function ResolveFileName: string;
  Public
    Procedure DefineProperties(Filer: TFiler); override;
  Public
    Constructor Create(aOwner : TComponent); virtual;
    Destructor Destroy; override;
    Procedure LoadFromFile(const aFilename : String);
    Procedure SaveToFile(const aFilename : String);
    Procedure LoadFromStream(const aStream : TStream; Handler:TFPCustomImageReader = Nil);
    Procedure SaveToStream(const aStream : TStream; Handler:TFPCustomImageWriter = Nil);
    Procedure Assign(Source : TPersistent); override;
    Property Data : TFPCustomImage Read GetData;
    Property ResolvedData : TFPCustomImage Read GetResolvedData;
    Property Width : Word Read GetWidth;
    Property Height : Word Read GetHeight;
    Property HasData : Boolean Read GetHasData;
  Published
    Property FileName : String Read FImageFile Write SetImageFile Stored GetFileStored;
    Property ImageName : String Read FImageName Write SetImageName Stored GetImageNameStored;
    Property ImageList : TBaseCustomImageList Read FImageList Write SetImageList Stored GetImageListStored;
    Property ImageIndex : Integer Read FImageIndex Write SetImageIndex Stored GetImageListStored;
  end;
  TImageDataClass = class of TImageData;

var
  DefaultImageDataClass: TImageDataClass = TImageData;

Function GetImagesConfig : TImagesConfig;
Procedure SetImagesConfig (aConfig: TImagesConfig);
Property ImagesConfig : TImagesConfig Read GetImagesConfig Write SetImagesConfig;

implementation

resourceString
  SErrNoImageData = '%s: No image data available';
  SErrNoImageList = '%s: No image list available';
  SErrNoImageStore = '%s: No image store available';
  SErrInvalidImageIndex = '%s: Invalid image index %d';
  SErrInvalidImageName = '%s: Invalid image name %s';
  SErrDuplicateResolution = 'Duplicate resolution: %d';

var
  _ImagesConfig : TImagesConfig;

function TResolutionList.GetR(aIndex : Integer): TResolution;
begin
  Result:=TResolution(Items[aIndex])
end;

procedure TResolutionList.SetR(aIndex : Integer; AValue: TResolution);
begin
  Items[aIndex]:=aValue
end;

function TResolutionList.FindDefaultResolution: TResolution;

var
  I : Integer;
begin
  I:=0;
  Result:=Nil;
  While (Result=nil) and (I<Count) do
    begin
    Result:=GetR(i);
    if Not Result.Default then
      Result:=Nil;
    Inc(I);
    end;
end;

function TResolutionList.IndexOfResolution(aResolution: Word): Integer;
begin
  Result:=Count-1;
  While (Result>=0) and (GetR(Result).Resolution<>aResolution) do
    Dec(result);
end;

function TResolutionList.AddResolution(aResolution: Word): TResolution;
begin
  If IndexOfResolution(aResolution)<>-1 then
    Raise EImageData.CreateFmt(SErrDuplicateResolution,[aResolution]);
  Result:=Add as TResolution;
  Result.Resolution:=aResolution;
end;

{ TFPImageList }

function TFPImageList.GetCount: Integer;
begin
  Result:=FList.Count;
end;

function TFPImageList.GetImage(Index: Integer): TFPCustomImage;
begin
  Result:=TFPCustomImage(FList[Index]);
end;

procedure TFPImageList.setCount(AValue: Integer);
begin
  FList.Count:=aValue;
end;

procedure TFPImageList.SetImage(Index: Integer; AValue: TFPCustomImage);
begin
 FList.Items[Index]:=aValue;
end;

constructor TFPImageList.Create(aOwnsImages: Boolean);
begin
  FList:=TFPObjectList.Create(aOwnsImages);
end;

destructor TFPImageList.Destroy;
begin
  FreeAndNil(FList);
  inherited Destroy;
end;

{ TImagesConfig }

procedure TImagesConfig.SetImageDir(AValue: String);
begin
  if FImageDir=AValue then Exit;
  FImageDir:=AValue;
  if FImageDir<>'' then
    FImageDir:=IncludeTrailingBackslash(FImageDir);
end;

procedure TImagesConfig.SetIconSize(AValue: Word);
begin
  if FIconSize=AValue then Exit;
  FIconSize:=AValue;
  if FIconSize=0 then
    FIconSize:=GetDefaultIconsize;
end;

function TImagesConfig.GetClass: TFPCustomImageClass;
begin
  Result:=FImageClass;
  if Result=Nil then
    Result:=TFPCompactImgRGBA8Bit;
end;

procedure TImagesConfig.SetImageExtension(AValue: String);
begin
  if FIMageExtension=AValue then Exit;
  FImageExtension:=AValue;
  if FImageExtension='' then
    FImageExtension:=GetDefaultExtension;
  if (Length(FImageExtension)>0) and  (FImageExtension[1]<>'.') then
    FIMageExtension:='.'+FIMageExtension;
end;

procedure TImagesConfig.SetResolution(AValue: Word);
begin
  if FResolution=AValue then Exit;
  FResolution:=AValue;
  If FResolution=0 then
    FResolution:=GetDefaultResolution;
end;

function TImagesConfig.GetDefaultResolution: Word;
begin
  Result:=DefaultImageResolution;
end;

function TImagesConfig.GetDefaultImageDir: String;
begin
  Result:=ExtractFilePath(ParamStr(0));
end;

function TImagesConfig.GetDefaultExtension: String;
begin
  Result:=DefaultImageExtension;
end;

function TImagesConfig.GetDefaultIconSize: Word;
begin
  Result:=DefaultIconSize;
end;

constructor TImagesConfig.Create;
begin
  Resolution:=GetDefaultResolution;
  ImageExtension:=GetDefaultExtension;
  ImageDir:=IncludeTrailingPathDelimiter(GetDefaultImageDir);
  IconSize:=GetDefaultIconSize;
end;

function TImagesConfig.GetImageFileName(aImage: String; aResolution: Word): String;

begin
  Result:=DoGetSizedImageFileName(aImage,0,aResolution);
end;

function TImagesConfig.GetSizedImageDir(aSize: Integer; aResolution: word): String;

const
  AddSizeFirst = [icoSizeDir,icoSizeFirst];

var
  SizeAdded : Boolean;

  Procedure AddSize;

  begin
    if (aSize=0) or SizeAdded then
      exit;
    Result:=IncludeTrailingPathDelimiter(Result);
    Result:=Result+Format('%dx%d',[aSize,aSize])+PathDelim;
    SizeAdded:=True;
  end;

begin
  Result:=ImageDir;
  SizeAdded:=False;
  if (aResolution<>0) and (icoResolutionDir in Options) then
    begin
    if ((AddSizeFirst*Options)=AddSizeFirst) then
      AddSize;
    Result:=Result+IntToStr(aResolution)+PathDelim;
    end;
  if (icoSizeDir in Options) then
    AddSize;
end;

function TImagesConfig.GetSizedImageFileName(aImage: String; aSize: Integer; aResolution: word): String;

begin
  if aSize=0 then
    aSize:=IconSize;
  Result:=DoGetSizedImageFileName(aImage,aSize,aResolution);
end;

function TImagesConfig.DoGetSizedImageFileName(aImage: String; aSize: Integer; aResolution: word): String;

const
  AddSizeFirst = [icoSizeDir,icoSizeFirst];

var
  SizeAdded : Boolean;

  Procedure AddSize;

  begin
    if (aSize=0) or SizeAdded then exit;
    Result:=Result+'_';
    Result:=Result+Format('%dx%d',[aSize,aSize]);
    SizeAdded:=True;
  end;

begin
  Result:=GetSizedImageDir(aSize,aResolution);
  Result:=IncludeTrailingPathDelimiter(Result)+LowerCase(aImage);
  SizeAdded:=False;
  If (aResolution<>0) and not (icoResolutionDir in Options) Then
    begin
    if ((AddSizeFirst*Options)=[icoSizeFirst]) then
      AddSize;
    Result:=Result+'_'+IntToStr(aResolution);
    end;
  if not (icoSizeDir in Options) then
    AddSize;
  Result:=Result+LowerCase(ImageExtension);
end;

{ TResolution }


procedure TResolution.SetDefault(AValue: Boolean);

var
  Res : TResolution;

begin
  if FDefault=AValue then Exit;
  if aValue and (Collection is TResolutionList) then
    begin
    Res:=TResolutionList(Collection).FindDefaultResolution;
    if Assigned(Res) then
      Res.Default:=False;
    end;
  FDefault:=AValue;
end;

procedure TResolution.SetResolution(AValue: Word);
begin
  if FResolution=AValue then Exit;
  FResolution:=AValue;
end;


{ TBaseCustomImageList }


procedure TBaseCustomImageList.SetResolutions(AValue: TResolutionList);
begin
  if FResolutions=AValue then Exit;
  FResolutions.Assign(AValue);
end;

procedure TBaseCustomImageList.SetWidth(AValue: Word);
begin
  if FWidth=AValue then Exit;
  FWidth:=AValue;
end;

procedure TBaseCustomImageList.SetDefaultResolution(AValue: Word);
var
  I : Integer;
begin
  if aValue=GetDefaultResolution then exit;
  I:=Resolutions.IndexOfResolution(aValue);
  if I<>-1 then
    Resolutions[i].Default:=True;

end;

function TBaseCustomImageList.GetImageCount: Integer;

begin
  Result:=DoGetImageCount(DefaultResolution);
end;

procedure TBaseCustomImageList.SetWidthHeight(aWidth,aHeight: Word);

begin
  FWidth:=aWidth;
  FHeight:=aHeight;
end;

function TCustomImageList.DoGetImageCount(aResolution : Word): Integer;

var
  Idx :Integer;

begin
  Result:=0;
  Idx:=Resolutions.IndexOfResolution(aResolution);
  if (Idx<>-1) and (Idx<Length(FLists)) then
    if FLists[Idx]<>Nil then
       Result:=FLists[Idx].Count;
end;

function TCustomImageList.DoIsValidImageIndex(aIndex: Integer; aResolution: Word): Boolean;

var
  Idx :Integer;

begin
  Result:=False;
  if aIndex<0 then
    exit;
  if aResolution=0 then
    aResolution:=DefaultResolution;
  Idx:=GetIndexOfResolution(aResolution);
  if (Idx<>-1) and (Idx<Length(FLists)) then
    if FLists[Idx]<>Nil then
       Result:=aIndex<FLists[Idx].Count;
end;

function TCustomImageList.GetResolutionList(aResolution: Word; allowCreate : Boolean): TFPImageList;

var
  Idx : Integer;

begin
  Result:=Nil;
  if aResolution=0 then
    aResolution:=DefaultResolution;
  Idx:=GetIndexOfResolution(aResolution);
  if (Idx<0) then
    exit;
  if (Idx>=Length(FLists)) then
    if allowCreate then
      begin
      SetLength(FLists,Idx+1);
      FLists[Idx]:=TFPImageList.Create(True);
      end
    else
      Exit;
  Result:=FLists[Idx];
end;

function TCustomImageList.DoGetRawImage(aIndex: Integer; aResolution: Word; AllowCreate: Boolean): TFPCustomImage;

var
  aList : TFPImageList;

begin
  Result:=Nil;
  aList:=GetResolutionList(aResolution,AllowCreate);
  if aList=Nil then
    exit;
  if (aIndex>=aList.Count) then
    if AllowCreate then
      aList.Count:=aIndex+1
    else
      exit;
  if Not Assigned(aList[aIndex]) then
    if AllowCreate then
      aList[aIndex]:=CreateImage
    else
      Exit;
  Result:=aList[aIndex];
end;

destructor TCustomImageList.destroy;

var
  I : Integer;

begin
  For I:=0 to Length(FLists)-1 do
    FreeAndNil(FLists[I]);
  inherited destroy;
end;

procedure TBaseCustomImageList.SetHeight(AValue: Word);
begin
  if FHeight=AValue then Exit;
  FHeight:=AValue;
  SetWidthHeight(Width,FHeight);
end;

constructor TBaseCustomImageList.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FResolutions:=CreateResolutions;
  FResolutions.AddResolution(DetermineApplicationResolution).Default:=True;
  FWidth:=ImagesConfig.IconSize;
  FHeight:=FWidth;
end;

destructor TBaseCustomImageList.Destroy;
begin
  FreeAndNil(FResolutions);
  inherited Destroy;
end;

function TBaseCustomImageList.CreateResolutions: TResolutionList;

begin
  Result:=TResolutionList.Create(Self,TResolution);
end;

function TBaseCustomImageList.DetermineApplicationResolution: Word;
begin
  Result:=96;
end;

function TBaseCustomImageList.IsValidImageIndex(aIndex: Integer): Boolean;
begin
  Result:=IsValidImageIndex(aIndex,GetDefaultResolution);
end;

function TBaseCustomImageList.IsValidImageIndex(aIndex: Integer; aResolution: Word): Boolean;

begin
  Result:=DoIsValidImageIndex(aIndex,aResolution);
end;

function TBaseCustomImageList.GetRawImage(aIndex: Integer; aResolution: Word): TFPCustomImage;

begin
  Result:=Nil;
  if aIndex<0 then
    exit;
  if aResolution=0 then
    aResolution:=DefaultResolution;
  Result:=DoGetRawImage(aIndex,aResolution,False);
end;


procedure TBaseCustomImageList.SetImage(aIndex: Integer; aResolution: Word; aImage: TFPCustomImage);

var
  Img : TFPCustomImage;

begin
  Img:=DoGetRawImage(aIndex,aResolution,True);
  Img.Assign(aImage);
end;

procedure TBaseCustomImageList.SetImage(aIndex: Integer; aImage: TFPCustomImage);
begin
  SetImage(aIndex,DefaultResolution,aImage);
end;

function TBaseCustomImageList.LoadImageFromFile(aIndex: Integer; aResolution: Word; const aFileName: string) : TFPCustomImage;

begin
  if not FileExists(aFileName) then
    Raise EInOutError.CreateFmt('File "%s" does not exist',[aFileName]);
  Result:=DoGetRawImage(aIndex,aResolution,True);
  if not Result.LoadFromFile(aFileName) then
    Raise EInOutError.CreateFmt('Failed to load image file "%s"',[aFileName]);
end;

function TBaseCustomImageList.LoadImageFromStream(aIndex: Integer; aResolution: Word; const aStream: TStream) : TFPCustomImage;

begin
  Result:=GetRawImage(aIndex,aResolution);
  Result.LoadFromStream(aStream);
end;

function TBaseCustomImageList.GetDefaultResolution: Word;

var
  Res : TResolution;

begin
  Res:=Resolutions.FindDefaultResolution;
  if Assigned(Res) then
    Result:=Res.Resolution
  else
    Result:=DetermineApplicationResolution;
end;

function TBaseCustomImageList.GetIndexOfResolution(aResolution: Word): Integer;
begin
  Result:=FResolutions.IndexOfResolution(aResolution);
end;

function TBaseCustomImageList.CreateImage: TFPCustomImage;
begin
  Result:=DefaultImageDataClass.CreateData(Width,Height);
end;

function TBaseCustomImageList.GetImage(aIndex: Integer): TFPCustomIMage;
begin
  Result:=GetImage(aIndex,GetDefaultResolution)
end;

function TBaseCustomImageList.GetImage(aIndex: Integer; aImage: TFPCustomImage) : Boolean;
begin
  Result:=GetImage(aIndex,GetDefaultResolution,aImage);
end;

function TBaseCustomImageList.GetImage(aIndex: Integer; aResolution: Word): TFPCustomImage;
begin
  Result:=CreateImage;
  try
    GetImage(aIndex,aResolution,Result)
  except
    FreeAndNil(Result);
    Raise;
  end;
end;

Function TBaseCustomImageList.GetImage(aIndex: Integer; aResolution: Word; aImage: TFPCustomImage): Boolean;

var
  aRawImage : TFPCustomImage;

begin
  aRawImage:=GetRawImage(aIndex,aResolution);
  Result:=Assigned(aRawImage);
  if Result then
    aImage.Assign(aRawImage);
end;

{ TBaseCustomImageStore }

procedure TBaseCustomImageStore.SetResolution(AValue: Word);
begin
  if FResolution=AValue then Exit;
  FResolution:=AValue;
end;

class function TBaseCustomImageStore.GetGlobalInstance: TBaseCustomImageStore;

var
  aClass : TBaseCustomImageStoreClass;

begin

  if _Instance=Nil then
    begin
    aClass:=InstanceClass;
    if aClass=nil then
      aClass:=TCustomImageStore;
    _Instance:=aClass.Create(Nil);
    end;
  Result:=_Instance;
end;

procedure TBaseCustomImageStore.SetSize(AValue: Word);
begin
  if FSize=AValue then Exit;
  FSize:=AValue;
end;

constructor TBaseCustomImageStore.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FSize:=ImagesConfig.IconSize;
  FResolution:=ImagesConfig.Resolution;
end;

function TBaseCustomImageStore.IsValidImageName(const aImageName: string): Boolean;

var
  Img : TFPCustomImage;

begin
  Result:=RetrieveImage(aImageName,Img);
  if Result then
    exit;
  Result:=FileExists(ImagesConfig.GetSizedImageFileName(aImageName,Size,ImagesConfig.Resolution));
  if Result then
    exit;
  Result:=FileExists(ImagesConfig.GetSizedImageFileName(aImageName,Size));
end;

function TBaseCustomImageStore.GetImageData(const aImageName: String; out aImage: TFPCustomImage; CopyData: Boolean): Boolean;

begin
  Result:=DoGetImageData(aImageName,aImage,CopyData);
end;

function TBaseCustomImageStore.DoGetImageData(const aImageName: String; out aImage: TFPCustomImage; CopyData: Boolean): Boolean;

var
  fData : TFPCustomImage;
  FN : String;
  DoStore : Boolean;

begin
  Result:=RetrieveImage(aImageName,fData);
  DoStore:=Not Result;
  if Not Result then
    begin
    if Size<>0 then
      fn:=ImagesConfig.GetSizedImageFileName(aImageName,Size,Resolution)
    else
      fn:=ImagesConfig.GetImageFileName(aImageName,Resolution);
    Result:=FileExists(fn);
    if not Result then
      begin
      if Size<>0 then
        fn:=ImagesConfig.GetSizedImageFileName(aImageName,Size)
      else
        fn:=ImagesConfig.GetImageFileName(aImageName);
      Result:=FileExists(fn)
      end;
    if Result then
      begin
      FData:=DefaultImageDataClass.CreateData(0,0);
      try
        FData.LoadFromFile(fn);
        DoStore:=True;
      except
        FData.Free;
        Raise;
      end;
      end;
    end;
  if Result then
    begin
    if DoStore then
      StoreImage(aImageName,fData);
    if not CopyData then
      aImage:=fData
    else
      begin
      aImage:=DefaultImageDataClass.CreateData(fData.Width,fData.Height);
      aImage.Assign(fdata);
      end;
    end;
end;

{ TCustomImageStore }

function TCustomImageStore.GetImageCount: Cardinal;
begin
  Result:=FCache.Count;
end;

procedure TCustomImageStore.SetSize(AValue: Word);
begin
  inherited SetSize(AValue);
  ClearCache;
end;

procedure TCustomImageStore.SetResolution(AValue: Word);
begin
  inherited SetResolution(AValue);
  ClearCache;
end;

procedure TCustomImageStore.StoreImage(const aName: String; aImage: TFPCustomImage);
begin
  FCache.Add(aName,aImage);
end;

function TCustomImageStore.RetrieveImage(const aName: string; out aImage: TFPCustomImage): Boolean;

var
  Obj : TObject;

begin
  Obj:=FCache.Items[aName];
  Result:=Assigned(Obj) and Obj.InheritsFrom(TFPCustomImage);
  if Result then
    aImage:=TFPCustomImage(Obj)
  else
    aImage:=Nil;
end;

constructor TCustomImageStore.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FCache:=TFPObjectHashTable.Create(True);
end;

destructor TCustomImageStore.Destroy;
begin
  ClearCache;
  FreeAndNil(FCache);
  inherited Destroy;
end;

procedure TCustomImageStore.ClearCache;
begin
  FCache.Clear;
end;


{ TImageData }

function TImageData.GetHeight: Word;
begin
  if Assigned(FData) then
    Result:=FData.Height
  else
    Result:=0;
end;

class function TImageData.GetImageFileDir: String; static;
begin
  Result:=ImagesConfig.ImageDir;
  if Result='' then
    Result:=ExtractFilePath(ParamStr(0));
end;

function TImageData.GetImageListStored: Boolean;
begin
  Result:=(FImageSource=isImageList) and assigned(FImageList);
end;


function TImageData.GetImageNameStored: Boolean;
begin
  Result:=(FImageSource=isStore) and (FImageName<>'');

end;

function TImageData.GetResolvedData: TFPCustomImage;
begin
  Result:=ResolveData(True);
end;

function TImageData.GetHasData: Boolean;
begin
  Result:=Assigned(Data) and (Data.Width>0) and (Data.Height>0);
end;

function TImageData.GetFileStored: Boolean;
begin
  Result:=(FImageSource=isFile) and (FImageFile<>'');
end;

function TImageData.GetData: TFPCustomImage;
begin
  Result:=ResolveData(False);
end;

function TImageData.ResolveFileName : string;

begin
  if FileName=ExpandFileName(FileName) then
    Result:=FileName
  else
    Result:=GetImageFileDir+FileName;
end;

function TImageData.LoadDataFromFile(RaiseError: Boolean): boolean;
begin
  Result:=False;
  try
    AllocateData(0,0);
    LoadFromFile(ResolveFileName);
    Result:=True;
  except
    on E : Exception do
      if RaiseError then Raise;
  end;
end;

function TImageData.LoadDataFromImageList(RaiseError: Boolean): boolean;
begin
  Result:=False;
  If not Assigned(ImageList) then
    begin
    if RaiseError then
      Raise EImageData.CreateFmt(SErrNoImageList,[GetOwnerName]);
    Exit;
    end;
  if ImageList.IsValidImageIndex(FImageIndex) then
    begin
    if RaiseError then
      Raise EImageData.CreateFmt(SErrInvalidImageIndex,[GetOwnerName,ImageIndex]);
    Exit;
    end;
  try
    AllocateData(0,0);
    ImageList.GetImage(FimageIndex,FData);
    Result:=True;
  except
    on E : Exception do
     if RaiseError then Raise;
  end;
end;

function TImageData.GetOwnerName: String;

var
  P : TPersistent;
  C : TComponent absolute P;

begin
  P:=GetOwner;
  if not (P is TComponent) then
    Result:='<>'
  else
    begin
    Result:=C.Name;
    if Result='' then
      Result:='<'+C.ClassName+'>';
    end;
end;

function TImageData.DetermineStore : TBaseCustomImageStore;

begin
  Result:=TImageStore.GlobalInstance;
end;

function TImageData.LoadDataFromStore(RaiseError: Boolean): boolean;

var
  Store : TBaseCustomImageStore;

begin
  Result:=False;
  Store:=DetermineStore;
  If Not Assigned(Store) then
    begin
    if RaiseError then
      Raise EImageData.CreateFmt(SErrNoImageStore,[GetOwnerName]);
    Exit;
    end;
  if Store.IsValidImageName(ImageName) then
    begin
    if RaiseError then
      Raise EImageData.CreateFmt(SErrInvalidImageName,[GetOwnerName,ImageName]);
    Exit;
    end ;
  try
    AllocateData(0,0);
    Store.GetImageData(ImageName,FData);
    Result:=True;
  except
    on E : Exception do
     if RaiseError then Raise;
  end;
end;

function TImageData.ResolveData(RaiseError : Boolean): TFPCustomImage;

begin
  if FData=Nil then
    Case FImageSource of
      isFile : LoadDataFromFile(RaiseError);
      isImageList : LoadDataFromImageList(RaiseError);
      isStore : LoadDataFromStore(RaiseError);
    else
      if RaiseError then
        EImageData.Create(SErrNoImageData);
    end;
  Result:=FData;
end;

function TImageData.GetWidth: Word;
begin
  if Assigned(Data) then
    Result:=FData.Width
  else
    Result:=0;
end;

procedure TImageData.ReadData(Reader: TReader);

// SizePerPixel = 4*SizeOf(Word)*2, because 4 words for RGBA stored as hex

var
  S : AnsiString;
  Row,Col,Len,N : Integer;
  H,W : Word;
  C : TFPColor;

  Function GetWord: word; inline;

  begin
    HexToBin(PAnsiChar(@(S[N])),@Result{%H-},2);
    Inc(N,4);
  end;

  Function GetByte: word; inline;

  begin
    HexToBin(PChar(@(S[N])),@Result{%H-},1);
    Inc(N,2);
  end;

begin
  S:=Reader.ReadString;
  N:=1;
  if (GetWord<>1) then
    Raise EFresnel.Create('Invalid image stream format');
  Len:=GetByte;
  N:=N+Len;
  H:=GetWord;
  W:=GetWord;
  AllocateData(W,H);
  For Row:=0 to Height-1 do
    For Col:=0 to Width-1 do
      begin
      C:=Data.Colors[Col,Row];
      C.Alpha:=GetWord;
      C.Red:=GetWord;
      C.Blue:=GetWord;
      C.Green:=GetWord;
      Data.Colors[Col,Row]:=C;
      end;
end;

procedure TImageData.SetImageFile(const aValue: String);
begin
  if FImageFile=AValue then Exit;
  FImageFile:=AValue;
end;

procedure TImageData.SetImageIndex(aValue: Integer);
begin
  if FImageIndex=AValue then Exit;
  FImageIndex:=AValue;
end;

procedure TImageData.SetImageList(aValue: TImageList);
begin
  if FImageList=AValue then Exit;
  if assigned(FImageList) then
    FImageList.FPODetachObserver(Self);
  FImageList:=AValue;
  if assigned(FImageList) then
    FImageList.FPOAttachObserver(Self);
end;

procedure TImageData.SetImageList(AValue: TBaseCustomImageList);
begin
  if FImageList=AValue then Exit;
  FImageList:=AValue;
end;

procedure TImageData.SetImageName(const AValue: String);
begin
  if FImageName=AValue then Exit;
  FImageName:=AValue;
end;



procedure TImageData.WriteData(Writer: TWriter);

Const
  SizePerPixel = 4*SizeOf(Word)*2;

var
  S : AnsiString;
  Row,Col,N,Size : Integer;
  C : TFPColor;
  Hex : ShortString;

  Procedure AddWord(W: word); inline;

  begin
    SetLength(Hex,4);
    BinToHex(@W,PAnsiChar(@Hex[1]),SizeOf(W));
    Move(Hex[1],S[N],4);
    Inc(N,4);
  end;

begin
  SetLength(Hex,4);
  S:=HexStr(Length(FData.ClassName),2);
  S:='0001'+S+FData.ClassName; // One word for future flags.
  N:=Length(S)+1;
  Size:=Length(S)+(Width*Height)*SizePerPixel+4;
  SetLength(S,Size);
  AddWord(Data.Height);
  AddWord(Data.Width);
  For Row:=0 to Height-1 do
    For Col:=0 to Width-1 do
      begin
      C:=Data.Colors[Col,Row];
      AddWord(C.Alpha);
      AddWord(C.Red);
      AddWord(C.Blue);
      AddWord(C.Green);
      end;
  Writer.WriteString(S);
end;

class function TImageData.CreateData(aWidth, aHeight: Word): TFPCustomImage;

var
  C : TFPCustomImageClass;

begin
  C:=DataClass;
  if C=Nil then
    C:=ImagesConfig.ImageClass;
  Result:=C.Create(aWidth,aHeight);
end;

procedure TImageData.AllocateData(aWidth, aHeight: Word);
begin
  FreeData;
  FData:=CreateData(aWidth,aHeight);
end;

procedure TImageData.ReplaceData(aData: TFPCustomImage);
begin
  if aData=FData then
    exit;
  FreeData;
  FData:=aData;
end;

procedure TImageData.FreeData;
begin
  FreeAndNil(FData);
end;

procedure TImageData.DefineProperties(Filer: TFiler);
begin
  inherited DefineProperties(Filer);
  Filer.DefineProperty('Data',@ReadData,@WriteData,(FImageSource=isData) and (Width<>0) and (Height<>0));
end;

procedure TImageData.LoadFromFile(const aFilename: String);

var
  Img: TFPCustomImage;

begin
  Img:=CreateData(0,0);
  try
    Img.LoadFromFile(aFileName);
  except
    Img.Free;
    Raise;
  end;
  ReplaceData(Img);
end;

procedure TImageData.SaveToFile(const aFilename: String);
begin
  FData.SaveToFile(aFileName);
end;

procedure TImageData.LoadFromStream(const aStream: TStream; Handler:TFPCustomImageReader);

  {$IFDEF DebugFresnelImgData}
  procedure WriteAlpha;
  var
    sl: TStringList;
    y, x: Integer;
    Line: String;
    c: TFPColor;
  begin
    sl:=TStringList.Create;
    try
      for y:=0 to Img.Height-1 do begin
        Line:=HexStr(y,4)+' ';
        for x:=0 to Img.Width-1 do begin
          c:=Img.Colors[x,y];
          if c.Alpha=alphaOpaque then
            Line+='O'
          else if c.Alpha=alphaTransparent then
            Line+='T'
          else
            Line+=HexStr(c.Alpha shr 12,1);
        end;
        sl.Add(Line);
      end;
      sl.SaveToFile('TImageDataLoadFromStream.txt');
    finally
      sl.Free;
    end;
  end;
  {$ENDIF}

var
  Img: TFPCustomImage;
begin
  Img:=CreateData(0,0);
  try
    if Assigned(Handler) then
      Img.LoadFromStream(aStream,Handler)
    else
      Img.LoadFromStream(aStream);
    {$IFDEF DebugFresnelImgData}
    WriteAlpha;
    {$ENDIF}
  except
    Img.Free;
    Raise;
  end;
  ReplaceData(Img);
end;

procedure TImageData.SaveToStream(const aStream: TStream; Handler: TFPCustomImageWriter);
begin
  FData.SaveToStream(aStream,Handler);
end;

constructor TImageData.Create(aOwner: TComponent);
begin
  FOwner:=aOwner;
  // Nothing yet
end;

destructor TImageData.Destroy;
begin
  FreeData;
  Inherited;
end;

procedure TImageData.Assign(Source: TPersistent);
begin
  inherited Assign(Source);
end;

Function GetImagesConfig : TImagesConfig;

begin
  Result:=_ImagesConfig;
end;

Procedure SetImagesConfig (aConfig: TImagesConfig);

begin
  FreeAndNil(_ImagesConfig);
  _ImagesConfig:=aConfig;
end;

initialization
  ImagesConfig:=TImagesConfig.Create;

finalization
  ImagesConfig:=nil;
end.

