{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Basic Low-Level widget set for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel.WidgetSet;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Keys, Fresnel.Events, Fresnel.Classes, Fresnel.DOM, Fresnel.Renderer;

type
  TFreHandle = Pointer;

  { TFresnelWSForm }

  TFresnelWSForm = class(TComponent)
  private
    FRenderer: TFresnelRenderer;
  protected
    procedure SetRenderer(aValue: TFresnelRenderer);
    function GetCaption: TFresnelCaption; virtual; abstract;
    function GetFormBounds: TFresnelRect; virtual; abstract;
    function GetVisible: boolean; virtual; abstract;
    procedure SetCaption(AValue: TFresnelCaption); virtual; abstract;
    procedure SetFormBounds(const AValue: TFresnelRect); virtual; abstract;
    procedure SetVisible(const AValue: boolean); virtual; abstract;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    function NumKeyToInputType(aKey: Integer; aShift: TShiftState; out aType: TFresnelInputType): boolean; virtual;
  public
    function GetClientSize: TFresnelPoint; virtual; abstract;
    procedure Invalidate; virtual;
    procedure InvalidateRect(const aRect: TFresnelRect); virtual; abstract;
    property Caption: TFresnelCaption read GetCaption write SetCaption;
    property FormBounds: TFresnelRect read GetFormBounds write SetFormBounds;
    property Renderer: TFresnelRenderer read FRenderer;
    property Visible: boolean read GetVisible write SetVisible;
  end;
  TFresnelWSFormClass = class of TFresnelWSForm;

  { TFresnelWidgetSet }

  TWidgetSetOption = (
    wsClick,      // Widgetset sends click event.
    wsDoubleClick // Widgetset sends doubleclick event.
  );
  TWidgetSetOptions = set of TWidgetSetOption;

  TFresnelWidgetSet = class(TComponent)
  private
    FOptions: TWidgetSetOptions;
  protected
    FWSFormClass: TFresnelWSFormClass;
    procedure SetOptions(AValue: TWidgetSetOptions); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    procedure AppWaitMessage; virtual; abstract;
    procedure AppProcessMessages; virtual; abstract;
    procedure AppTerminate; virtual; abstract;
    procedure AppSetTitle(const ATitle: string); virtual;

    // Begin/End processing messages, which can be used to acquire/release
    // resources during message processing.
    // for example, on Cocoa, it needs to be used to release AutoReleasePool
    // to avoid resource leaks.
    function  BeginMessageProcess: TFreHandle; virtual;
    procedure EndMessageProcess(Context: TFreHandle); virtual;

    procedure CreateWSForm(aFresnelForm: TFresnelComponent); virtual; abstract;
    property WSFormClass: TFresnelWSFormClass read FWSFormClass;
    Property Options : TWidgetSetOptions Read FOptions Write SetOptions;
  end;

var
  WidgetSet: TFresnelWidgetSet;

implementation

{ TFresnelWSForm }

procedure TFresnelWSForm.SetRenderer(aValue: TFresnelRenderer);
begin
  if Assigned(Frenderer) then
    FRenderer.RemoveFreeNotification(Self);
  FRenderer:=aValue;
  if Assigned(Frenderer) then
    FRenderer.FreeNotification(Self);
end;

procedure TFresnelWSForm.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
    begin
    if aComponent=FRenderer then
      FRenderer:=Nil;
    end;
end;

procedure TFresnelWSForm.Invalidate;
var
  aRect: TFresnelRect;
begin
  aRect.Left:=0;
  aRect.Top:=0;
  aRect.BottomRight:=GetClientSize;
  InvalidateRect(aRect);
end;

function TFresnelWSForm.NumKeyToInputType(aKey : Integer; aShift: TShiftState; out aType : TFresnelInputType) : boolean;

begin
  // Return true if we have a correct input type
  Result:=True;
  case aKey of
    TKeyCodes.BackSpace : aType:=fitDeleteContentBackward;
    TKeyCodes.Delete : aType:=fitDeleteContentForward;
  else
    if aKey<0 then
      Result:=False
    else
      aType:=fitInsertText
  end;
end;


{ TFresnelWidgetSet }

procedure TFresnelWidgetSet.SetOptions(AValue: TWidgetSetOptions);
begin
  if FOptions=AValue then Exit;
  FOptions:=AValue;
end;

constructor TFresnelWidgetSet.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  if WidgetSet<>nil then
    raise Exception.Create('20230908113733');
  WidgetSet:=Self;
end;

destructor TFresnelWidgetSet.Destroy;
begin
  WidgetSet:=nil;
  inherited Destroy;
end;

procedure TFresnelWidgetSet.AppSetTitle(const ATitle: string);
begin
  if ATitle='' then ;
end;

function TFresnelWidgetSet.BeginMessageProcess: TFreHandle;
begin
  Result:=nil;
end;

procedure TFresnelWidgetSet.EndMessageProcess(Context: TFreHandle);
begin
  if Context<>nil then ;
end;

end.

