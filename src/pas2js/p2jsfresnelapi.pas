{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit p2jsfresnelapi;

{$warn 5023 off : no warning about unused units}
interface

uses
  fresnel.pas2js.wasmapi, fresnel.wasm.shared, fresnel.keys;

implementation

end.
