{$mode objfpc}
{$h+}
{$modeswitch externalclass}
{$modeswitch advancedrecords}

{$DEFINE IMAGE_USEOSC}

unit fresnel.pas2js.wasmapi;

interface
// Define this to disable API Logging altogether
{ $DEFINE NOLOGAPICALLS}

uses classes, js, web, webassembly, wasienv, fresnel.keys, fresnel.wasm.shared;

Const
  // These should probably move to weborworker
  MOUSE_PRIMARY   = 1;
  MOUSE_SECONDARY = 2;
  MOUSE_AUXILIARY = 4;
  MOUSE_EXTRA1    = 8;
  MOUSE_EXTRA2    = 16;

Type
  TWasmPointer = longint;
  TWasmFresnelApi = Class;
  TTimerTickCallback = Function (aTimerID : TTimerID; UserData : TWasmPointer) : Boolean;
  TKeyKind = record
    isSpecial : Boolean;
    KeyCode : Longint;
  end;
  TClipRect = record
    x,y,w,h : Single;
  end;
  TTransform = record
    m11,m12,m21,m22,m31,m32 : TFresnelFloat;
  end;

  { TFresnelHelper }

  TFresnelHelper = Class
  Private
    class var
     _CurrentID : TCanvasID;
     _CurrentMenuID : TMenuID;
  Public
    Class function FresnelColorToHTMLColor(aColor : TCanvasColor) : string;
    Class function FresnelColorToHTMLColor(aRed, aGreen, aBlue, aAlpha: TCanvasColorComponent): string;
    class Function MouseButtonToShiftState(aButton: Integer): TShiftStateEnum;
    Class Function ShiftStateToInt(aState : TShiftState) : Integer;
    Class Function AllocateCanvasID : TCanvasID;
    Class Function AllocateMenuID : TMenuID;
  end;

  { TCanvasEvent }

  TCanvasEvent = record
    CanvasID : TCanvasID;
    msg : TCanvasMessageID;
    param0 : TCanvasMessageParam;
    param1 : TCanvasMessageParam;
    param2 : TCanvasMessageParam;
    param3 : TCanvasMessageParam;
    constructor Create(aCanvasID : TCanvasID; aMsg : TCanvasMessageID);
    constructor Create(aCanvasID : TCanvasID; aMsg : TCanvasMessageID; p0 : TCanvasMessageParam; p1: TCanvasMessageParam = 0; p2: TCanvasMessageParam = 0;p3: TCanvasMessageParam = 0);
  end;

  { TMainMenuBuilder }
  TMenuFlag = (mfInvisible,mfChecked,mfRadio);
  TMenuFlags = set of TMenuFlag;

  TMainMenuBuilder = Class(TObject)
  private
    FApi: TWasmFresnelApi;
    FMenuParent: TJSHTMLElement;
  protected
    Procedure DoMenuClick(aEvent : TJSEvent);
    function DoAddMenuItem(aParentID,aMenuID : TMenuID; aCaption : String; Flags : TMenuFlags; ShortCut : Longint; aData : TWasmPointer) : TJSHTMLElement; virtual; abstract;
    function DoRemoveMenuItem(aMenuID : TMenuID) : Boolean; virtual; abstract;
    function DoUpdateMenuItem(aMenuID : TMenuID; aFlags : TMenuFlags; aFlagsToUpdate : TMenuFlags) : boolean; virtual; abstract;
  Public
    Constructor Create(aApi : TWasmFresnelApi; aMenuParent : TJSHTMLElement); virtual;
    function AddMenuItem(aParentID,aMenuID : TMenuID; aCaption : String; Flags : TMenuFlags; ShortCut : Longint; aData : TWasmPointer) : TJSHTMLElement;
    function RemoveMenuItem(aMenuID : TMenuID) : Boolean;
    function UpdateMenuItem(aMenuID : TMenuID; aFlags : TMenuFlags; aFlagsToUpdate : TMenuFlags) : boolean;
    Property MenuParent : TJSHTMLElement Read FMenuParent;
    Property API : TWasmFresnelApi Read FApi;
  end;

  { TDefaultMainMenuBuilder }
  TDefaultMainMenuBuilder = class(TMainMenuBuilder)
  Protected
    function FindMenuElement(aMenuID: Integer): TJSHTMLElement;
    function DoAddMenuItem(aParentID, aMenuID: TMenuID; aCaption: String; Flags : TMenuFlags; ShortCut : Longint; aData: TWasmPointer): TJSHTMLElement; override;
    function DoRemoveMenuItem(aMenuID : TMenuID) : Boolean; override;
    function DoUpdateMenuItem(aMenuID : TMenuID; aFlags : TMenuFlags; aFlagsToUpdate : TMenuFlags) : boolean; override;
  end;

  { TCanvasReference }
  TCanvasType = (ctElement,ctOffscreen);

  TCanvasReference = class (TObject)
  private
    FCanvasType: TCanvasType;
    FFillStyle: JSValue;
    FFont: String;
    FStrokeStyle: JSValue;
    FTitle: String;
    FClipRects : Array of TClipRect;
    FLastTransform : TTransform;
    procedure InitProperties;
    procedure SaveTransform(m11, m12, m21, m22, m31, m32: TFresnelFloat);
    procedure SetFillStyle(AValue: JSValue);
    procedure SetFont(AValue: String);
    procedure SetStrokeStyle(AValue: JSValue);
    procedure SetTextBaseLine(AValue: String);
    procedure SetClipPath;
    procedure AddClipRect(aRect : TClipRect);
    procedure SaveState;
    procedure RestoreState(aApplyProperties: Boolean);
    function SetTransform(m11,m12,m21,m22,m31,m32 : TFresnelFloat; DoReset : Boolean) : TCanvasError;
    function DoMouseClick(aEvent: TJSEvent): boolean;
    function DoMouseDblClick(aEvent: TJSEvent): boolean;
    function DoMouseDown(aEvent: TJSEvent): boolean;
    function DoMouseUp(aEvent: TJSEvent): boolean;
    function DoMouseMove(aEvent: TJSEvent): boolean;
    function DoMouseWheel(aEvent: TJSEvent): boolean;
    function MouseToEvent(aEvent: TJSMouseEvent; aMessageID: TCanvasMessageID): TCanvasEvent;
    procedure SetTitle(AValue: String);
  Public
    Class function EncodeShiftState(keyEvent: TJSKeyboardEvent): longint;
    API : TWasmFresnelApi;
    CanvasID : TCanvasID;
    canvascontext : TJSCanvasRenderingContext2D;
    canvas :TJSHTMLCanvasElement;
    windowtitle,
    canvasParent :TJSHTMLElement;
    FTextBaseLine : String;
    FMenuBuilder : TMainMenuBuilder;
    constructor Create(aID : TCanvasID; aAPI : TWasmFresnelApi; aCanvas : TJSHTMLCanvasElement; aParent : TJSHTMLElement);
    constructor CreateOffScreen(aID : TCanvasID;aAPI : TWasmFresnelApi; aWidth,aHeight: Longint);
    procedure PrepareCanvas;
    Procedure RemoveCanvas;
    Procedure SendEvent;
    procedure ApplyProperties;
    property textBaseLine : String Read FTextBaseLine Write SetTextBaseLine;
    property FillStyle : JSValue Read FFillStyle Write SetFillStyle;
    property StrokeStyle : JSValue Read FStrokeStyle Write SetStrokeStyle;
    property Font : String Read FFont Write SetFont;
    property Title : String Read FTitle Write SetTitle;
    property CanvasType : TCanvasType Read FCanvasType;
    property MenuBuilder : TMainMenuBuilder Read FMenuBuilder Write FMenuBuilder;
  end;


  { TWasmFresnelApi }

  TTimerCallback = Procedure (aCurrent,aPrevious : Double);
  TMenuClickCallback = Procedure (aMenuID : TMenuID; aUserData : TWasmPointer);
  TDebugApi = (daText,daClipRect);
  TDebugApis = Set of TDebugApi;



  TWasmFresnelApi = class(TImportExtension)
  Public
    class var
      _Keymap : TJSObject;
      _KeyCodeMap : TJSObject;
  Private
    FCanvases : TJSObject;
    FCanvasParent : TJSHTMLELement;
    FCreateDefaultCanvas: Boolean;
    FDebugApis: TDebugApis;
    FDefaultCanvas: TCanvasReference;
    FFocusedCanvas: TCanvasReference;
    FLastFocused: TCanvasReference;
    FLogAPICalls : Boolean;
    FMenuSupport: Boolean;
    FTimerID : NativeInt;
    FTimerInterval: NativeInt;
    FLastTick: TDateTime;
    FKeyMap : TJSObject;
    procedure SetCreateDefaultCanvas(AValue: Boolean);
    procedure SetFocusedCanvas(AValue: TCanvasReference);
  Protected
    FEvents : array of TCanvasEvent;
    Procedure LogCall(const Msg : String);
    Procedure LogCall(Const Fmt : String; const Args : Array of const);
    function GetCanvas(aID : TCanvasID) : TJSCanvasRenderingContext2D;
    function GetCanvasRef(aID: TCanvasID): TCanvasReference;
    function CreateMenuBuilder(aParent: TJSHTMLELement): TMainMenuBuilder; virtual;
    // Debug
    procedure DrawBaseLine(C: TJSCanvasRenderingContext2D; S: String; X, Y: Double);
    procedure DrawClipRect(Ref: TCanvasReference; aX, aY, aWidth, aHeight: double);
    // Canvas
    function allocatecanvas(SizeX, SizeY : Longint; aID: TWasmPointer): TCanvasError;
    function allocateoffscreencanvas(SizeX, SizeY : Longint; aBitmap : TWasmPointer; aID: TWasmPointer): TCanvasError;
    function deallocatecanvas(aID: TCanvasID): TCanvasError;
    function getcanvasbyid(aCanvasElementID: TWasmPointer; aElementIDLen: Longint; aID: TWasmPointer): TCanvasError;
    function moveto(aID : TCanvasID; X, Y : TFresnelFloat): TCanvasError;
    function lineto(aID : TCanvasID; X, Y : TFresnelFloat):  TCanvasError;
    function stroke(aID : TCanvasID): TCanvasError; 
    function beginpath(aID : TCanvasID):  TCanvasError; 
    function arc(aID : TCanvasID; X, Y, RadiusX, RadiusY, StartAngle, EndAngle, Rotate : TFresnelFloat; Flags : Longint):  TCanvasError;
    function fillrect(aID : TCanvasID; X, Y, Width, Height : TFresnelFloat): TCanvasError;
    function strokerect(aID : TCanvasID; X, Y, Width, Height : TFresnelFloat ):  TCanvasError;
    function clearrect(aID : TCanvasID; X, Y, Width, Height : TFresnelFloat ):  TCanvasError;
    function RoundRect(aID : TCanvasID; Flags : Longint; Data : PFresnelFloat) : TCanvasError;
    function StrokeText(aID : TCanvasID; X, Y : TFresnelFloat; aText : TWasmPointer; aTextLen : Longint ):  TCanvasError;
    function FillText(aID : TCanvasID; X,Y : TFresnelFloat; aText : TWasmPointer; aTextLen : Longint ):  TCanvasError;
    function GetCanvasSizes(aID: TCanvasID; aWidth, aHeight: PFresnelFloat): TCanvasError;
    function SetCanvasSizes(aID: TCanvasID; aWidth, aHeight: TFresnelFloat): TCanvasError;
    function SetFillStyle(aID: TCanvasID; aRed,aGreen,aBlue,aAlpha: TCanvasColorComponent): TCanvasError;
    function ClearCanvas(aID: TCanvasID; aRed,aGreen,aBlue,aAlpha: TCanvasColorComponent): TCanvasError;
    function SetLinearGradientFillStyle(aID: TCanvasID; aStartX, aStartY, aEndX, aEndY : TFresnelFloat; aColorPointCount : longint; aColorPoints : TWasmPointer) : TCanvasError;
    function SetImageFillStyle(aID: TCanvasID; Flags : Longint; aImageWidth, aImageHeight: Longint; aImageData: TWasmPointer) : TCanvasError;
    function SetLineCap(aID: TCanvasID; aCap: TCanvasLinecap): TCanvasError;
    function SetLineJoin(aID: TCanvasID; aJoin: TCanvasLineJoin): TCanvasError;
    function SetLineMiterLimit(aID: TCanvasID; aWidth: TCanvasLineMiterLimit): TCanvasError;
    function SetLineDash(aID: TCanvasID; aOffset : TFresnelFloat; aPatternCount : longint; aPattern : PFresnelFloat): TCanvasError;
    function SetLineWidth(aID: TCanvasID; aWidth: TCanvasLineWidth): TCanvasError;
    function SetTextBaseLine(aID: TCanvasID; aBaseLine: TCanvasTextBaseLine): TCanvasError;
    function SetStrokeStyle(aID: TCanvasID; aRed,aGreen,aBlue,aAlpha: TCanvasColorComponent): TCanvasError;
    function DrawImage(aID : TCanvasID; aX, aY, aWidth, aHeight: TFresnelFloat; aImageWidth, aImageHeight: Longint; aImageData: TWasmPointer) : TCanvasError;
    function DrawImageEx(aID: TCanvasID; DrawData : PFresnelFloat; aImageData: TWasmPointer): TCanvasError;
    function SetFont(aID : TCanvasID; aFontName : TWasmPointer; aFontNameLen : integer) : TCanvasError;
    function MeasureText(aID : TCanvasID; aText : TWasmPointer; aTextLen : integer; aMeasureData : TWasmPointer) : TCanvasError;
    function SetTextShadowParams (aID : TCanvasID;  aOffsetX, aOffsetY, aRadius : TFresnelFloat;  aRed,aGreen,aBlue,aAlpha : TCanvasColorComponent): TCanvasError;
    function DrawPath(aID : TCanvasID; aFlags : Longint; aPathCount : longint; aPath : PFresnelFloat) : TCanvasError;
    function PointInPath(aID : TCanvasID; aX,aY : TFresnelFloat; aPointCount : Integer; aPointData : PFresnelFloat; aRes : TWasmPointer): TCanvasError;
    function SetTransform(aID : TCanvasID; Flags : Longint; m11,m12,m21,m22,m31,m32 : TFresnelFloat) : TCanvasError;
    function GetViewPortSizes(Flags : Longint; aWidth, aHeight : PFresnelFloat) : TCanvasError;
    function SetWindowTitle(aID: TCanvasID; aTitle : TWasmPointer; aTitleLen : Longint): TCanvasError;
    function SetSpecialKeyMap(Map : TWasmPointer; aLen : longint) : TCanvasError;
    function SaveState(aID : TCanvasID; aFlags : Longint) : TCanvasError;
    function RestoreState(aID : TCanvasID; aFlags : Longint) : TCanvasError;
    function ClipAddRect(aID: TCanvasID; aX,aY,aWidth,aHeight: TFresnelFloat): TCanvasError;
    // Timer
    function AllocateTimer(ainterval : longint; userdata: TWasmPointer) : TTimerID;
    procedure DeallocateTimer(timerid: TTimerID);
    // Events
    function GetEvent(aID: TWasmPointer; aMsg: TWasmPointer; Data : TWasmPointer): TCanvasError;
    function GetEventCount(aCount: TWasmPointer): TCanvasError;
    // Menu
    Function HandleMenuClick(aMenuID : TMenuID; aData : TWasmPointer) : Boolean;
    function AddMenuItem(aCanvasID : TCanvasId; aParentID : TMenuID; aCaption : TWasmPointer; aCaptionLen : Longint; aData: TWasmPointer; aFlags : Longint; aShortCut : Longint; aMenuID : PMenuID) : TCanvasError;
    function DeleteMenuItem(aCanvasID : TCanvasId; aMenuID : TMenuID) : TCanvasError;
    function UpdateMenuItem(aCanvasID : TCanvasId; aMenuID : TMenuID; aFlags : Longint; aShortCut : Longint) : TCanvasError;
    // Key handlers are global
    function DoKeyDownEvent(aEvent: TJSEvent): boolean;
    function DoKeyUpEvent(aEvent: TJSEvent): boolean;
    // Click & enter/leave handlers to detect loss of focus.
    procedure DoGlobalClick(aEvent: TJSEvent);
    procedure DoGlobalEnter(aEvent: TJSEvent);
    procedure DoGlobalLeave(aEvent: TJSEvent);
    //
    procedure DoTimerTick; virtual;
    property DefaultCanvas : TCanvasReference Read FDefaultCanvas;
    property FocusedCanvas : TCanvasReference Read FFocusedCanvas Write SetFocusedCanvas;
  Public
    Constructor Create(aEnv : TPas2JSWASIEnvironment); override;
    Procedure InstallGlobalHandlers;
    Procedure FillImportObject(aObject : TJSObject); override;
    Procedure ProcessMessages;
    Procedure StartTimerTick;
    Procedure StopTimerTick;
    function KeyNameToKeyCode(aKey: String) : TKeyKind;
    class function GetGlobalKeyMap: TJSObject;
    class function KeyNameToKeyCode(aMap : TJSObject;aKey : string) : TKeyKind;
    class function CreateSpecialKeyNameMap : TJSObject;
    class function CreateSpecialKeyCodeMap : TJSObject;
    Function ImportName : String; override;
    Property CanvasParent : TJSHTMLELement Read FCanvasParent Write FCanvasParent;
    Property CreateDefaultCanvas : Boolean read FCreateDefaultCanvas Write SetCreateDefaultCanvas;
    Property LogAPICalls : Boolean Read FLogAPICalls Write FLogAPICalls;
    Property MenuSupport : Boolean Read FMenuSupport Write FMenuSupport;
    Property TimerInterval : NativeInt Read FTimerInterval Write FTimerInterval;
    Property DebugAPIs : TDebugApis Read FDebugApis Write FDebugApis;
  end;

Implementation

uses sysutils;

Function FlagsToMenuFlags(Flags : Longint) : TMenuFlags;

  procedure add(aFlag: Longint; aMenuFlag : TMenuFlag);

  begin
    if (Flags and aFlag)=aFlag then
      Include(Result,aMenuFlag);
  end;

begin
  Result:=[];
  Add(MENU_FLAGS_INVISIBLE,mfInvisible);
  Add(MENU_FLAGS_CHECKED,mfChecked);
  Add(MENU_FLAGS_RADIO,mfRadio);
end;

type

  { TJSKeyNameObjectCreator }

  TKeyNameObjectCreator = class(TSpecialKeyEnumerator)
    FObj : TJSObject;
    procedure EnumKey(aCode: Integer; aName: string); override;
  end;

  TKeyCodeObjectCreator = class(TSpecialKeyEnumerator)
    FObj : TJSObject;
    procedure EnumKey(aCode: Integer; aName: string); override;
  end;

{ TJSKeyNameObjectCreator }

procedure TKeyNameObjectCreator.EnumKey(aCode: Integer; aName: string);
begin
  FObj.Properties[aName]:=aCode;
end;

procedure TKeyCodeObjectCreator.EnumKey(aCode: Integer; aName: string);
begin
  FObj.Properties[IntToStr(aCode)]:=aName;
end;


{ ---------------------------------------------------------------------
  FresnelHelper
  ---------------------------------------------------------------------}

class function TFresnelHelper.FresnelColorToHTMLColor(aRed,aGreen,aBlue,aAlpha: TCanvasColorComponent): string;

begin
  Result:='rgb('+inttostr(aRed shr 8)+' '+IntToStr(aGreen shr 8)+' '+inttoStr(aBlue shr 8);
  if aAlpha<>$FFFF then
    Result:=Result+' / '+floatToStr(aAlpha/$FFFF);
  Result:=Result+')';
end;

class function TFresnelHelper.FresnelColorToHTMLColor(aColor: TCanvasColor): string;

Const
  Hex = '0123456789ABCDEF';

var
  I : Integer;

begin
  Result:='#';
  aColor:=aColor shr 8;
  for I:=1 to 6 do
    begin
    Result:=Result+Hex[(aColor and $F)+1];
    aColor:=aColor shr 4;
    end;
end;

class function TFresnelHelper.ShiftStateToInt(aState: TShiftState): Integer;

var
  S : TShiftStateEnum;

begin
  Result:=0;
  For S in TShiftstate do
    If (S in aState) then
      Result:=Result or (1 shl Ord(S));
end;


class function TFresnelHelper.MouseButtonToShiftState(aButton : Integer)  : TShiftStateEnum;

begin
  Case aButton of
    MOUSE_PRIMARY: Result:=ssLeft;
    MOUSE_SECONDARY : Result:=ssRight;
    MOUSE_AUXILIARY : Result:=ssMiddle;
    MOUSE_EXTRA1    : Result:=ssExtra1;
    MOUSE_EXTRA2    : Result:=ssExtra2;
  end;
end;


{ ---------------------------------------------------------------------
  TCanvasReference
  ---------------------------------------------------------------------}

procedure TWasmFresnelApi.ProcessMessages;

var
  Callback : JSValue;
begin
  if not assigned(InstanceExports) then
    Console.Error('No instance exports !')
  else
    begin
    Callback:=InstanceExports['__fresnel_process_message'];
    if Assigned(Callback) then
      begin
      TTimerCallback(CallBack)(FLastTick,Now);
      FLastTick:=Now;
      end
    else
      Console.Error('No processmessages callback !');
    end
end;

procedure TWasmFresnelApi.DoTimerTick;

var
  Callback : JSValue;
  T : TDateTime;

begin
  T:=FLastTick;
  FLastTick:=Now;
  if not assigned(InstanceExports) then
    Console.warn('No instance exports !')
  else
    begin
    Callback:=InstanceExports['__fresnel_tick'];
    if Assigned(Callback) then
      begin
      TTimerCallback(CallBack)(FLastTick,T);
      end
    else
      Console.warn('No tick callback !');
    end
end;

constructor TCanvasReference.Create(aID: TCanvasID; aAPI: TWasmFresnelApi; aCanvas: TJSHTMLCanvasElement; aParent: TJSHTMLElement);

begin
  FCanvasType:=ctElement;
  Canvas:=aCanvas;
  canvasParent:=aParent;
  API:=aAPI;
  CanvasID:=aID;
  PrepareCanvas;
end;

constructor TCanvasReference.CreateOffScreen(aID: TCanvasID; aAPI: TWasmFresnelApi; aWidth,aHeight: Longint);
begin
  FCanvasType:=ctOffscreen;
  API:=aAPI;
  CanvasID:=aID;
  Canvas:=TJSHTMLOffscreenCanvasElement.New(aWidth,aHeight);
  CanvasContext:=TJSCanvasRenderingContext2D(Canvas.getcontext('2d'));
  InitProperties;
end;

procedure TCanvasReference.SetFillStyle(AValue: JSValue);
begin
//  if FFillStyle=AValue then Exit;
  FFillStyle:=AValue;
  canvascontext.fillStyle:=FFillStyle;
end;

procedure TCanvasReference.SetFont(AValue: String);
begin
  if FFont=AValue then Exit;
  FFont:=AValue;
  canvascontext.font:=FFont;
end;

procedure TCanvasReference.SetStrokeStyle(AValue: JSValue);
begin
  if FStrokeStyle=AValue then Exit;
  FStrokeStyle:=AValue;
  canvascontext.strokeStyle:=aValue;
end;

procedure TCanvasReference.SetTextBaseLine(AValue: String);
begin
  if FTextBaseLine=aValue then Exit;
  FTextBaseLine:=aValue;
  canvascontext.textBaseline:=aValue;
end;

procedure TCanvasReference.SetClipPath;

var
  P : TJSPath2D;
  R : TClipRect;
begin
  RestoreState(True);
  SaveState;
  if Length(FClipRects)=0 then
    exit;
  P:=TJSPath2D.New;
  For R in FClipRects do
    P.Rect(R.X,R.Y,R.w,R.h);
  CanvasContext.Clip(P);
end;

procedure TCanvasReference.AddClipRect(aRect: TClipRect);
begin
  TJSArray(FClipRects).Push(aRect)
end;

procedure TCanvasReference.SaveState;
begin
  CanvasContext.Save;
end;

procedure TCanvasReference.RestoreState(aApplyProperties : Boolean);
begin
  CanvasContext.Restore;
  ApplyProperties;
end;

procedure TCanvasReference.SaveTransform(m11,m12,m21,m22,m31,m32: TFresnelFloat);

begin
  FLastTransform.m11:=m11;
  FLastTransform.m12:=m12;
  FLastTransform.m21:=m21;
  FLastTransform.m22:=m22;
  FLastTransform.m31:=m31;
  FLastTransform.m32:=m32;
end;

function TCanvasReference.SetTransform(m11, m12, m21, m22, m31, m32: TFresnelFloat; DoReset: Boolean): TCanvasError;
begin
  Result:=0;
  if DoReset  then
    begin
    CanvasContext.setTransform(m11,m12,m21,m22,m31,m32);
    SaveTransform(m11,m12,m21,m22,m31,m32);
    end
  else
    CanvasContext.transform(m11,m12,m21,m22,m31,m32);
end;

procedure TCanvasReference.PrepareCanvas;

begin
  CanvasContext:=TJSCanvasRenderingContext2D(Canvas.getcontext('2d'));
  Canvas.AddEventListener('mousedown',@DoMouseDown);
  Canvas.AddEventListener('mouseup',@DoMouseUp);
  Canvas.AddEventListener('mousemove',@DoMouseMove);
  Canvas.AddEventListener('click',@DoMouseClick);
  Canvas.AddEventListener('dblclick',@DoMouseDblClick);
  Canvas.AddEventListener('scroll',@DoMouseWheel);
  FClipRects:=[];
  InitProperties;
end;

procedure TCanvasReference.RemoveCanvas;

begin
  CanvasParent.removeChild(Canvas);
  Canvas:=Nil;
  CanvasParent:=nil;
  canvascontext:=nil;
end;

procedure TCanvasReference.SendEvent;
begin
  API.ProcessMessages;
end;

procedure TCanvasReference.InitProperties;

begin
  FTextBaseLine := canvascontext.textBaseline;
  FFillStyle    := canvascontext.fillStyle;
  FStrokeStyle  := canvascontext.strokeStyle;
  FFont         := canvascontext.font;
  FLastTransform.m11:=1;
  FLastTransform.m12:=0;
  FLastTransform.m21:=0;
  FLastTransform.m32:=1;
  FLastTransform.m31:=0;
  FLastTransform.m32:=0;
end;

procedure TCanvasReference.ApplyProperties;
begin
  canvascontext.textBaseline:=FTextBaseLine;
  canvascontext.fillStyle:=FFillStyle;
  canvascontext.strokeStyle:=FStrokeStyle;
  canvascontext.font:=FFont;
  With FLastTransform do
    canvascontext.setTransform(m11,m12,m21,m22,m31,m32);
end;

function TCanvasReference.MouseToEvent(aEvent : TJSMouseEvent;aMessageID : TCanvasMessageID) : TCanvasEvent;

var
  State : TShiftState;

  Procedure Check(aButton : Integer);
  begin
    if (aEvent.buttons and aButton)<>0 then
      include(State,TFresnelHelper.MouseButtonToShiftState(aButton));
  end;

begin
  Result.CanvasID:=Self.CanvasID;
  Result.msg:=aMessageID;
  Result.param0:=Round(aEvent.OffsetX);
  Result.param1:=Round(aEvent.OffsetY);
  State:=[];
  Check(MOUSE_PRIMARY);
  Check(MOUSE_SECONDARY);
  Check(MOUSE_AUXILIARY);
  Check(MOUSE_EXTRA1);
  Check(MOUSE_EXTRA2);
  If aEvent.altKey then
    Include(State,ssAlt);
  If aEvent.ctrlKey then
    Include(State,ssCtrl);
  If aEvent.shiftKey then
    Include(State,ssShift);
  if aEvent.metaKey then
    Include(State,ssMeta);
  Result.Param2:=TFresnelHelper.ShiftStateToInt(State);
end;

procedure TCanvasReference.SetTitle(AValue: String);
begin
  if FTitle=AValue then Exit;
  FTitle:=AValue;
  Windowtitle.InnerText:=FTitle;
end;

function TCanvasReference.DoMouseDown(aEvent: TJSEvent): boolean;

var
  Evt : TJSMouseEvent absolute aEvent;

begin
  API.FocusedCanvas:=Self;
  Result:=True;
  TJSArray(API.FEvents).Push(MouseToEvent(evt,WASMSG_MOUSEDOWN));
  SendEvent;
end;

function TCanvasReference.DoMouseMove(aEvent: TJSEvent): boolean;

var
  Evt : TJSMouseEvent absolute aEvent;

begin
  Result:=True;
  TJSArray(API.FEvents).Push(MouseToEvent(evt,WASMSG_MOVE));
  SendEvent;
end;

function TCanvasReference.DoMouseWheel(aEvent: TJSEvent): boolean;
var
  JSEvt : TJSWheelEvent absolute aEvent;
  CanvasEvt : TCanvasEvent;

begin
  Result:=True;
  CanvasEvt:=MouseToEvent(JSevt,WASMSG_WHEELY);
  Case JSEvt.deltaMode of
    0 : CanvasEvt.Param3:=Round(JSEvt.deltaY);
    1 : CanvasEvt.Param3:=Round(JSEvt.deltaY*12); // arbitrary
    2 : CanvasEvt.Param3:=Round(JSEvt.deltaY*600); // arbitrary
  end;
  TJSArray(API.FEvents).Push(CanvasEvt);
  SendEvent;
end;

class function TCanvasReference.EncodeShiftState(keyEvent: TJSKeyboardEvent): longint;


var
  aState : Longint;

  procedure DoAdd(add: boolean; aCode : Longint);

  begin
    if add then
      astate:=astate or aCode;
  end;

begin
  astate:=0;
  DoAdd(KeyEvent.shiftKey,WASM_KEYSTATE_SHIFT);
  DoAdd(KeyEvent.ctrlKey,WASM_KEYSTATE_CTRL);
  DoAdd(KeyEvent.altKey,WASM_KEYSTATE_ALT);
  DoAdd(KeyEvent.metaKey,WASM_KEYSTATE_META);
  Result:=aState;
end;

function TWasmFresnelApi.DoKeyDownEvent(aEvent: TJSEvent): boolean;

var
  evt : TCanvasEvent;
  keyEvent : TJSKeyboardEvent absolute aEvent;
  KeyKind : TKeyKind;

begin
  Result:=false;
  if Not Assigned(FocusedCanvas) then exit;
  evt.CanvasID:=FocusedCanvas.CanvasID;
  evt.msg:=WASMSG_KEYDOWN;
  KeyKind:=KeyNameToKeyCode(KeyEvent.Key);
  evt.param0:=KeyKind.KeyCode;
  evt.Param1:=Ord(KeyKind.isSpecial);
  evt.param2:=TCanvasReference.EncodeShiftState(KeyEvent);
  TJSArray(FEvents).Push(Evt);
  FocusedCanvas.SendEvent;
  aEvent.preventDefault;
  aEvent.cancelBubble:=True;
end;

function TWasmFresnelApi.DoKeyUpEvent(aEvent: TJSEvent): boolean;
var
  evt : TCanvasEvent;
  keyEvent : TJSKeyboardEvent absolute aEvent;
  KeyKind : TKeyKind;

begin
  Result:=false;
  if Not Assigned(FocusedCanvas) then exit;
  evt.CanvasID:=FocusedCanvas.CanvasID;
  evt.msg:=WASMSG_KEYUP;
  KeyKind:=KeyNameToKeyCode(KeyEvent.Key);
  evt.param0:=KeyKind.KeyCode;
  evt.Param1:=Ord(KeyKind.isSpecial);
  evt.param2:=TCanvasReference.EncodeShiftState(KeyEvent);
  TJSArray(FEvents).Push(Evt);
  FocusedCanvas.SendEvent;
  aEvent.preventDefault;
  aEvent.cancelBubble:=True;
end;

function TCanvasReference.DoMouseClick(aEvent: TJSEvent): boolean;

var
  Evt : TJSMouseEvent absolute aEvent;

begin
  Result:=True;
  TJSArray(API.FEvents).Push(MouseToEvent(evt,WASMSG_CLICK));
  SendEvent;
  aEvent.cancelBubble:=True;
end;

function TCanvasReference.DoMouseDblClick(aEvent: TJSEvent): boolean;
var
  Evt : TJSMouseEvent absolute aEvent;

begin
  Result:=True;
  TJSArray(API.FEvents).Push(MouseToEvent(evt,WASMSG_DBLCLICK));
  SendEvent;
end;


function TCanvasReference.DoMouseUp(aEvent: TJSEvent): boolean;

var
  Evt : TJSMouseEvent absolute aEvent;

begin
  Result:=True;
  TJSArray(API.FEvents).Push(MouseToEvent(evt,WASMSG_MOUSEUP));
  SendEvent;
end;

{ TMainMenuBuilder }

procedure TMainMenuBuilder.DoMenuClick(aEvent: TJSEvent);

var
  S : String;
  MenuID : integer;
  UserData : TWasmPointer;
  menuEl : TJSHTMLElement;

begin
  MenuEl:=TJSHTMLElement(aEvent.currentTargetHTMLElement.parentElement);
  S:=MenuEl.dataset['menuId'];
  MenuID:=StrToIntDef(S,-1);
  S:=MenuEl.dataset['menuUserData'];
  UserData:=StrToIntDef(S,-1);
  if (UserData<>-1) and (MenuID<>-1) then
    Api.HandleMenuClick(MenuID,UserData);
end;

constructor TMainMenuBuilder.Create(aApi: TWasmFresnelApi; aMenuParent: TJSHTMLElement);
begin
  FAPI:=aApi;
  FMenuParent:=aMenuParent;
end;

function TMainMenuBuilder.AddMenuItem(aParentID,aMenuID: TMenuID; aCaption: String; Flags : TMenuFlags; ShortCut : Longint; aData: TWasmPointer): TJSHTMLElement;
begin
  Result:=DoAddMenuItem(aParentID,aMenuID,aCaption,Flags,ShortCut,aData);
end;

function TMainMenuBuilder.RemoveMenuItem(aMenuID: TMenuID): Boolean;
begin
  Result:=DoRemoveMenuItem(aMenuID);
end;

function TMainMenuBuilder.UpdateMenuItem(aMenuID: TMenuID; aFlags: TMenuFlags; aFlagsToUpdate: TMenuFlags): boolean;
begin
  Result:=DoUpdateMenuItem(aMenuID,aFlags,aFlagsToUpdate);
end;

{ TDefaultMainMenuBuilder }

function TDefaultMainMenuBuilder.FindMenuElement(aMenuID : Integer) : TJSHTMLElement;

begin
  Result:=TJSHTMLElement(MenuParent.querySelector('li[data-menu-id="'+IntToStr(aMenuID)+'"]'))
end;

function TDefaultMainMenuBuilder.DoAddMenuItem(aParentID,aMenuID: TMenuID; aCaption: String; Flags : TMenuFlags; ShortCut : Longint; aData: TWasmPointer): TJSHTMLElement;

var
  CaptionEl,MenuEl,ListEl,parentEl : TJSHTMLElement;

begin
  Result:=nil;
  if AParentID=0 then
    ParentEl:=MenuParent
  else
    ParentEl:=FindMenuElement(aParentID);
  if Not assigned(ParentEl) then exit;
  ListEl:=TJSHTMLElement(ParentEl.QuerySelector('ul'));
  if Not Assigned(ListEl) then
    begin
    ListEl:=TJSHTMLElement(Document.createElement('ul'));
    if ParentEl=MenuParent then
      ListEl.classList.Add('fresnel-mainmenu')
    else
      ListEl.classList.Add('fresnel-submenu');
    ParentEl.appendChild(ListEl);
    end;
  MenuEl:=TJSHTMLElement(Document.CreateElement('li'));
  ListEl.appendChild(MenuEl);
  MenuEl.dataset.Map['menuId']:=IntToStr(aMenuID);
  MenuEl.dataset.Map['menuUserData']:=IntToStr(aData);
  if aCaption='-' then
    begin
    MenuEl.ClassList.add('fresnel-menu-separator');
    CaptionEl:=TJSHTMLElement(Document.CreateElement('hr'))
    end
  else
    begin
    MenuEl.classList.Add('fresnel-menu-item');
    CaptionEl:=TJSHTMLElement(Document.CreateElement('span'));
    if mfChecked in Flags then
      aCaption:=#$2611+' '+aCaption;
    CaptionEl.InnerText:=aCaption;
    CaptionEl.AddEventListener('click',@DoMenuClick);
    end;
  MenuEl.appendChild(CaptionEl);
  Result:=MenuEl;
end;

function TDefaultMainMenuBuilder.DoRemoveMenuItem(aMenuID: TMenuID) : Boolean;

var
  El : TJSHTMLElement;

begin
  El:=FindMenuElement(aMenuID);
  Result:=Assigned(El);
  if Result then
    El.parentElement.removeChild(El);
end;

function TDefaultMainMenuBuilder.DoUpdateMenuItem(aMenuID: TMenuID; aFlags: TMenuFlags; aFlagsToUpdate: TMenuFlags): boolean;
begin
  Result:=False;
end;


constructor TWasmFresnelApi.Create(aEnv: TPas2JSWASIEnvironment);

begin
  Inherited Create(aEnv);
  FCanvases:=TJSObject.New();
  FLogAPICalls:=True;
  FTimerInterval:=10;
  FLastTick:=Now;
  InstallGLobalHandlers;
end;

procedure TWasmFresnelApi.DoGlobalLeave(aEvent : TJSEvent);

begin
  FLastFocused:=FocusedCanvas;
  FocusedCanvas:=Nil;
end;

procedure TWasmFresnelApi.DoGlobalEnter(aEvent : TJSEvent);

begin
  FocusedCanvas:=FLastFocused;
end;


procedure TWasmFresnelApi.DoGlobalClick(aEvent : TJSEvent);

var
  EL : TJSElement;

begin
  El:=aEvent.targetElement;
  While Assigned(El) do
    begin
    if El.ClassList.contains('fresnel-window') then
      exit;
    El:=El.parentElement;
    end;
  FocusedCanvas:=Nil;
end;

procedure TWasmFresnelApi.InstallGlobalHandlers;
begin
  // Attach to summary
  Document.Body.AddEventListener('keydown',@DoKeyDownEvent);
  Document.Body.AddEventListener('keyup',@DoKeyUpEvent);
  Document.Body.AddEventListener('click',@DoGlobalClick);
  Document.Body.AddEventListener('mouseleave',@DoGlobalLeave);
  Document.Body.AddEventListener('mouseenter',@DoGlobalEnter);
end;

function TWasmFresnelApi.ImportName: String;

begin
  Result:='fresnel_api';
end;

function TWasmFresnelApi.GetCanvasRef(aID : TCanvasID) : TCanvasReference;

var
  JS : JSValue;

begin
  JS:=FCanvases[IntTostr(AID)];
  if IsObject(JS)  then
    Result:= TCanvasReference(JS)
  else
    Result:=nil;
end;

function TWasmFresnelApi.CreateMenuBuilder(aParent : TJSHTMLELement): TMainMenuBuilder;
begin
  Result:=TDefaultMainMenuBuilder.Create(Self,aParent);
end;

class function TFresnelHelper.AllocateCanvasID: TCanvasID;
begin
  Inc(_CurrentID);
  Result:=_CurrentID;
end;

class function TFresnelHelper.AllocateMenuID: TMenuID;
begin
  Inc(_CurrentMenuID);
  Result:=_CurrentMenuID;
end;

{ TCanvasEvent }

constructor TCanvasEvent.Create(aCanvasID: TCanvasID; aMsg: TCanvasMessageID);
begin
  CanvasID:=aCanvasID;
  msg:=aMsg;
end;

constructor TCanvasEvent.Create(aCanvasID: TCanvasID; aMsg: TCanvasMessageID;
  p0: TCanvasMessageParam; p1: TCanvasMessageParam; p2: TCanvasMessageParam;
  p3: TCanvasMessageParam);
begin
  CanvasID:=aCanvasID;
  msg:=aMsg;
  Param0:=p0;
  Param1:=p1;
  Param2:=p2;
  Param3:=p3;
end;

procedure TWasmFresnelApi.LogCall(const Msg: String);
begin
{$IFNDEF NOLOGAPICALLS}
  If not LogAPICalls then exit;
  Writeln(Msg);
{$ENDIF}
end;

procedure TWasmFresnelApi.LogCall(const Fmt: String; const Args: array of const);
begin
{$IFNDEF NOLOGAPICALLS}
  If not LogAPICalls then exit;
  Writeln(Format(Fmt,Args));
{$ENDIF}
end;

function TWasmFresnelApi.GetCanvas(aID : TCanvasID) : TJSCanvasRenderingContext2D;

Var
  Ref : TCanvasReference;

begin
  Ref:=GetCanvasRef(aID);
  if Assigned(Ref) then
    Result:= Ref.canvascontext
  else
    begin
    Console.Warn('Fresnel: Unknown canvas : ',aID);
    Result:=Nil;
    end;
end;

procedure TWasmFresnelApi.FillImportObject(aObject: TJSObject);

begin
  // Canvas
  aObject['canvas_allocate']:=@AllocateCanvas;
  aObject['canvas_allocate_offscreen']:=@AllocateOffscreenCanvas;
  aObject['canvas_deallocate']:=@DeAllocateCanvas;
  aObject['canvas_getbyid']:=@getcanvasbyid;
  aObject['canvas_getsizes']:=@getcanvassizes;
  aObject['canvas_setsizes']:=@setcanvassizes;
  aObject['canvas_moveto']:=@moveto;
  aObject['canvas_lineto']:=@LineTo;
  aObject['canvas_stroke']:=@stroke;
  aObject['canvas_beginpath']:=@beginpath;
  aObject['canvas_arc']:=@arc;
  aObject['canvas_fillrect']:=@fillrect;
  aObject['canvas_strokerect']:=@strokerect;
  aObject['canvas_clearrect']:=@clearrect;
  aObject['canvas_stroketext']:=@StrokeText;
  aObject['canvas_filltext']:=@FillText;
  aObject['canvas_set_fillstyle']:=@SetFillStyle;
  aObject['canvas_linear_gradient_fillstyle']:=@SetLinearGradientFillStyle;
  aObject['canvas_image_fillstyle']:=@SetImageFillStyle;
  aObject['canvas_set_strokestyle']:=@SetStrokeStyle;
  aObject['canvas_set_linewidth']:=@SetLineWidth;
  aObject['canvas_set_linecap']:=@SetLineCap;
  aObject['canvas_set_linejoin']:=@SetLineJoin;
  aObject['canvas_set_linemiterlimit']:=@SetLineMiterLimit;
  aObject['canvas_set_linedash']:=@SetLineDash;
  aObject['canvas_set_textbaseline']:=@SetTextBaseLine;
  aObject['canvas_draw_image']:=@DrawImage;
  aObject['canvas_draw_image_ex']:=@DrawImageEx;
  aObject['canvas_set_font']:=@SetFont;
  aObject['canvas_measure_text']:=@MeasureText;
  aObject['canvas_set_textshadow_params']:=@SetTextShadowParams;
  aObject['canvas_roundrect']:=@RoundRect;
  aObject['canvas_draw_path']:=@DrawPath;
  aObject['canvas_point_in_path']:=@PointInPath;
  aObject['canvas_set_transform']:=@SetTransForm;
  aObject['canvas_clear']:=@ClearCanvas;
  aObject['canvas_get_viewport_sizes']:=@GetViewPortSizes;
  aObject['canvas_set_title']:=@SetWindowTitle;
  aObject['canvas_save_state']:=@SaveState;
  aObject['canvas_restore_state']:=@RestoreState;
  aObject['canvas_clip_add_rect']:=@ClipAddRect;

  // Timer
  aObject['timer_allocate']:=@AllocateTimer;
  aObject['timer_deallocate']:=@DeAllocateTimer;
  // Event
  aObject['event_get']:=@GetEvent;
  aObject['event_count']:=@GetEventCount;
  aObject['event_set_special_keymap']:=@SetSpecialKeyMap;
  aObject['menu_add_item']:=@AddMenuItem;
  aObject['menu_remove_item']:=@DeleteMenuItem;
  aObject['menu_update_item']:=@UpdateMenuItem;
end;

procedure TWasmFresnelApi.StartTimerTick;
begin
  FTimerID:=Window.setInterval(@DoTimerTick,FTimerInterval);
end;

procedure TWasmFresnelApi.StopTimerTick;
begin
  Window.clearInterval(FTimerID);
end;

class function TWasmFresnelApi.GetGlobalKeyMap: TJSObject;

begin
  if _KeyMap=Nil then
    _KeyMap:=CreateSpecialKeyNameMap;
  Result:=_KeyMap;
end;


function TWasmFresnelApi.KeyNameToKeyCode(aKey: String): TKeyKind;

var
  lMap : TJSObject;

begin
  lMap:=FKeyMap;
  if lMap=Nil then
    lmap:=GetGlobalKeyMap;
  Result:=KeyNameToKeyCode(lMap,aKey);
//  Writeln('Mapped ',aKey,' to ',TJSJSON.StringIfy(Result));
end;

class function TWasmFresnelApi.KeyNameToKeyCode(aMap : TJSObject; aKey: string): TKeyKind;

var
  num: JSValue;

begin
  num:=aMap.Properties[aKey];
  Result.IsSpecial:=isDefined(num);
  if Result.IsSpecial then
    Result.KeyCode:=Integer(Num)
  else
    Result.KeyCode:=TJSString(JSValue(aKey)).codePointAt(0);
end;

class function TWasmFresnelApi.CreateSpecialKeyNameMap: TJSObject;

begin
  Result:=TJSObject.New;
  With TKeyNameObjectCreator.Create do
     try
       FObj:=Result;
       EnumSpecialKeys(@EnumKey);
     finally
       Free;
     end;
end;

class function TWasmFresnelApi.CreateSpecialKeyCodeMap: TJSObject;
begin
  Result:=TJSObject.New;
  With TKeyCodeObjectCreator.Create do
     try
       FObj:=Result;
       EnumSpecialKeys(@EnumKey);
     finally
       Free;
     end;

end;

function TWasmFresnelApi.GetCanvasSizes(aID: TCanvasID; aWidth, aHeight: PFresnelFloat): TCanvasError;

var
  Ref: TCanvasReference;
  v : TJSDataView;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.GetCanvasSizes(%d,[%x],[%x])',[aID,aWidth,aHeight]);
    end;
  {$ENDIF}
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  v:=getModuleMemoryDataView;
  v.setint32(aWidth,Ref.canvas.width,env.IsLittleEndian);
  v.setint32(aHeight,Ref.canvas.height,env.IsLittleEndian);
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.SetCanvasSizes(aID: TCanvasID; aWidth, aHeight: TFresnelFloat): TCanvasError;

var
  Ref: TCanvasReference;
  w,h : TFresnelFloat;

begin
  w:=aWidth;
  h:=aHeight;
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetCanvasSizes(%d,%g,%g)',[aID,w,h]);
    end;
  {$ENDIF}
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  Ref.canvas.width:=Round(w);
  Ref.canvas.Height:=Round(h);
  Ref.canvasParent.style.setProperty('max-width',intTostr(Round(w))+'px');

  Ref.ApplyProperties;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.SetFillStyle(aID: TCanvasID; aRed, aGreen, aBlue, aAlpha: TCanvasColorComponent): TCanvasError;

var
  Ref : TCanvasReference;
  S : String;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetFillStyle(%d,%d,%d,%d,%d)',[aID,aRed,aGreen,aBlue,aAlpha]);
    end;
  {$ENDIF}
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  S:=TFresnelHelper.FresnelColorToHTMLColor(aRed,aGreen,aBlue,aAlpha);
  Ref.fillStyle:=S;
  Exit(ECANVAS_SUCCESS);
end;

function TWasmFresnelApi.ClearCanvas(aID: TCanvasID; aRed, aGreen, aBlue,
  aAlpha: TCanvasColorComponent): TCanvasError;

var
  Ref : TCanvasReference;
  S : String;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetFillStyle(%d,%d,%d,%d,%d)',[aID,aRed,aGreen,aBlue,aAlpha]);
    end;
  {$ENDIF}
  Ref:=GetCanvasRef(aID);
  if Not Assigned(ref) then
    Exit(ECANVAS_NOCANVAS);
  S:=TFresnelHelper.FresnelColorToHTMLColor(aRed,aGreen,aBlue,aAlpha);
  Ref.canvascontext.fillStyle:=S;
  Ref.canvascontext.FillRect(0,0,Ref.canvas.width,Ref.canvas.height);
  Exit(ECANVAS_SUCCESS);
end;

function TWasmFresnelApi.SetLinearGradientFillStyle(aID: TCanvasID; aStartX, aStartY, aEndX, aEndY: TFresnelFloat;
  aColorPointCount: longint; aColorPoints: TWasmPointer): TCanvasError;

var
  I,P : Longint;
  Red,Green,Blue,Alpha: Longint;
  offset : double;
  G : TJSCanvasGradient;
  Canv : TJSCanvasRenderingContext2D;
  V : TJSDataView;
  S : String;

  function GetLongint: longint;
  begin
    Result:=V.getInt32(P,Env.IsLittleEndian);
    Inc(P,4);
  end;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetLinearGradientFillStyle(%d,(%g,%g),(%g,%g),%d,[%x])',[aID,aStartX, aStartY, aEndX, aEndY, aColorPointCount,aColorPoints]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  G:=Canv.createLinearGradient(aStartX,aStartY,aEndX,aEndY);
  V:=getModuleMemoryDataView;
  P:=aColorPoints;
  For I:=0 to aColorPointCount-1 do
    begin
    Red:=GetLongint;
    Green:=GetLongint;
    Blue:=GetLongint;
    Alpha:=GetLongint;
    offset:=GetLongint/10000;
    S:=TFresnelHelper.FresnelColorToHTMLColor(Red,Green,Blue,Alpha);
    G.addColorStop(offset,S);
    end;
  Canv.fillStyleAsGradient:=G;
  Exit(ECANVAS_SUCCESS);
end;

function TWasmFresnelApi.SetImageFillStyle(aID: TCanvasID; Flags: Longint;
  aImageWidth, aImageHeight: Longint; aImageData: TWasmPointer): TCanvasError;



var
  OSC : TJSHTMLOffscreenCanvasElement;
  ImgData : TJSImageData;
//  OSCImgBitmap : TJSImageBitmap;
  Canv,Canv2 : TJSCanvasRenderingContext2D;
  D : TJSUint8ClampedArray;
  V : TJSDataView;
  S : String;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetImageFillStyle(%d,%d,(%d,%d),[%x])',[aID,flags,aImageWidth,aImageHeight,aImageData]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  V:=getModuleMemoryDataView;
  D:=TJSUint8ClampedArray.New(V.Buffer,aImageData,aImageWidth*aImageWidth*4);
  ImgData:=TJSImageData.new(D,aImageWidth,aImageWidth);
  OSC:=TJSHTMLOffscreenCanvasElement.New(aImageWidth,aImageHeight);
  Canv2:=OSC.getContextAs2DContext('2d');
  Canv2.clearRect(0,0,aImageWidth,aImageHeight);
  Canv2.putImageData(ImgData,0,0);
  Case flags and 3 of
    IMAGEFILLSTYLE_NOREPEAT : s:='no-repeat';
    IMAGEFILLSTYLE_REPEAT   : s:='repeat';
    IMAGEFILLSTYLE_REPEATX  : s:='repeat-x';
    IMAGEFILLSTYLE_REPEATY  : s:='repeat-y';
  end;
  Canv.fillStyleAsPattern:=Canv.createPattern(OSC,S);
end;

function TWasmFresnelApi.SetStrokeStyle(aID: TCanvasID; aRed, aGreen, aBlue, aAlpha: TCanvasColorComponent): TCanvasError;

var
  Ref : TCanvasReference;
  S : String;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetStrokeStyle(%d,%d,%d,%d,%d)',[aID,aRed,aGreen,aBlue,aAlpha]);
    end;
  {$ENDIF}
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  S:=TFresnelHelper.FresnelColorToHTMLColor(aRed,aGreen,aBlue,aAlpha);
  Ref.StrokeStyle:=S;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.DrawImage(aID: TCanvasID; aX, aY, aWidth, aHeight: TFresnelFloat; aImageWidth, aImageHeight: Longint; aImageData: TWasmPointer): TCanvasError;

var
  V : TJSDataView;
  D : TJSUint8ClampedArray;
  ImgData : TJSImageData;
  Canv : TJSCanvasRenderingContext2D;

{$IFDEF IMAGE_USEOSC}
  Canv2 : TJSCanvasRenderingContext2D;
  OSC : TJSHTMLOffscreenCanvasElement;
{$ENDIF}

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.DrawImage(%d,(%g,%g),(%gx%g),(%dx%d)',[aID,aX,aY,aWidth,aHeight,aImageWidth,aImageHeight]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  V:=getModuleMemoryDataView;
  D:=TJSUint8ClampedArray.New(V.Buffer,aImageData,aImageWidth*aImageWidth*4);
  ImgData:=TJSImageData.new(D,aImageWidth,aImageWidth);
{$IFDEF IMAGE_USEOSC}
  OSC:=TJSHTMLOffscreenCanvasElement.New(aImageWidth,aImageHeight);
  Canv2:=OSC.getContextAs2DContext('2d');
  Canv2.clearRect(0,0,aImageWidth,aImageHeight);
  Canv2.putImageData(ImgData,0,0);
  Canv.drawImage(OSC,aX,aY,aWidth,aHeight);
{$ELSE}
Window.createImageBitmap(ImgData)._then(
    function (res : jsvalue) : JSValue
    var
      ImgBitmap : TJSImageBitmap absolute res;
    begin
      Canv.drawImage(ImgBitmap,aX,aY,aWidth,aHeight);
    end);
{$ENDIF}
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.DrawImageEx(aID: TCanvasID; DrawData: PFresnelFloat; aImageData: TWasmPointer): TCanvasError;

var
  V : TJSDataView;
  D : TJSUint8ClampedArray;
  ImgData : TJSImageData;
  Canv : TJSCanvasRenderingContext2D;
  aSrcX,aSrcY,aSrcWidth,aSrcHeight,aDestX,aDestY,aDestWidth,aDestHeight :Double;
  aImageWidth,aImageHeight : Longint;

{$IFDEF IMAGE_USEOSC}
  Canv2 : TJSCanvasRenderingContext2D;
  OSC : TJSHTMLOffscreenCanvasElement;
{$ENDIF}

  Function GetD(aIdx : Integer) : LongInt;

  begin
    Result:=Round(V.getFloat32(DrawData+aIdx*SizeFloat32,Env.IsLittleEndian));
  end;

  Function GetS(aIdx : Integer) : Double;

  begin
    Result:=v.getFloat32(DrawData+aIdx*SizeFloat32,Env.IsLittleEndian);
  end;


begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.DrawImageEx(%d,[%x],[%x])',[aID,DrawData,aImageData]);
    end;
  {$ENDIF}

  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  V:=getModuleMemoryDataView;
  aDestX:=GetS(DRAWIMAGE_DESTX);
  aDestY:=GetS(DRAWIMAGE_DESTY);
  aDestWidth:=GetS(DRAWIMAGE_DESTWIDTH);
  aDestHeight:=GetS(DRAWIMAGE_DESTHEIGHT);
  aSrcX:=GetS(DRAWIMAGE_SRCX);
  aSrcY:=GetS(DRAWIMAGE_SRCY);
  aSrcWidth:=GetS(DRAWIMAGE_SRCWIDTH);
  aSrcHeight:=GetS(DRAWIMAGE_SRCHEIGHT);
  aImageWidth:=GetD(DRAWIMAGE_IMAGEWIDTH);
  aImageHeight:=GetD(DRAWIMAGE_IMAGEHEIGHT);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.DrawImage(%d,[(%g,%g) - (%gx%g)],[(%g,%g) - (%gx%g)],[%dx%d])',[aID,aSrcX,aSrcY,aSrcWidth,aSrcHeight,aDestX,aDestY,aDestWidth,aDestHeight,aImageWidth,aImageWidth]);
    end;
  {$ENDIF}
  D:=TJSUint8ClampedArray.New(V.Buffer,aImageData,aImageWidth*aImageWidth*4);
  ImgData:=TJSImageData.new(D,aImageWidth,aImageWidth);

{$IFDEF IMAGE_USEOSC}
  OSC:=TJSHTMLOffscreenCanvasElement.New(aImageWidth,aImageHeight);
  Canv2:=OSC.getContextAs2DContext('2d');
  Canv2.clearRect(0,0,aImageWidth,aImageHeight);
  Canv2.putImageData(ImgData,0,0);
  Canv.drawImage(OSC,aSrcX,aSrcY,aSrcWidth,aSrcHeight,aDestX,aDestY,aDestWidth,aDestHeight);
{$ELSE}
Window.createImageBitmap(ImgData)._then(
    function (res : jsvalue) : JSValue
    var
      ImgBitmap : TJSImageBitmap absolute res;
    begin
      Canv.drawImage(ImgBitmap,aX,aY,aWidth,aHeight);
    end);
{$ENDIF}
  Result:=ECANVAS_SUCCESS;
end;


function TWasmFresnelApi.SetFont(aID: TCanvasID; aFontName: TWasmPointer; aFontNameLen: integer): TCanvasError;

var
  S : String;
  Ref : TCanvasReference;
begin
  S:=Env.GetUTF8StringFromMem(aFontName,aFontNameLen);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetFont(%d,"%s")',[aID,S]);
    end;
  {$ENDIF}
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  Ref.font:=S;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.MeasureText(aID: TCanvasID; aText: TWasmPointer; aTextLen: integer; aMeasureData : TWasmPointer): TCanvasError;

var
  S : String;
  Canv:TJSCanvasRenderingContext2D;
  M : TJSTextMetrics;
  V : TJSDataView;
  W,H,Asc,Desc : Double;
  D: TWasmPointer;

begin
  S:=Env.GetUTF8StringFromMem(aText,aTextLen);
  {$IFNDEF NOLOGAPICALLS}
  // If LogAPICalls then
    begin
    LogCall('Canvas.MeasureText(%d,"%s")',[aID,S]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  M:=Canv.measureText(S);
  W:=M.width;
  Asc:=M.actualBoundingBoxAscent;
  Desc:=M.actualBoundingBoxDescent;
  H:=Asc+Desc;
  V:=getModuleMemoryDataView;
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.MeasureText(%d,"%s") : [W: %g, H: %g, Asc: %g, Desc: %g]',[aID,S,W,H,Asc,Desc]);
    end;
  {$ENDIF}
  D:=aMeasureData+WASMMEASURE_WIDTH*SizeFloat32;
  v.setfloat32(D,W,env.IsLittleEndian);
  D:=aMeasureData+WASMMEASURE_HEIGHT*SizeFloat32;
  v.setfloat32(D,H,env.IsLittleEndian);
  D:=aMeasureData+WASMMEASURE_ASCENDER*SizeFloat32;
  v.setfloat32(D,Asc,env.IsLittleEndian);
  D:=aMeasureData+WASMMEASURE_DESCENDER*SizeFloat32;
  v.setfloat32(D,Desc,env.IsLittleEndian);
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.SetTextShadowParams (aID : TCanvasID;  aOffsetX,aOffsetY,aRadius : TFresnelFloat;  aRed,aGreen,aBlue,aAlpha : TCanvasColorComponent): TCanvasError;

var
  Canv:TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetTextShadowParams(%d,(%g,%g),%g,"%s")',[aID,aOffsetX,aOffsetY,aRadius,TFresnelHelper.FresnelColorToHTMLColor(aRed,aGreen,aBlue,aAlpha)]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  Canv.shadowOffsetX:=aOffsetX;
  Canv.shadowOffsetY:=aOffsetY;
  Canv.shadowBlur:=aRadius;
  Canv.shadowColor:=TFresnelHelper.FresnelColorToHTMLColor(aRed,aGreen,aBlue,aAlpha);
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.DrawPath(aID: TCanvasID; aFlags: Longint; aPathCount: longint; aPath: PFresnelFloat): TCanvasError;
var
  Canv:TJSCanvasRenderingContext2D;
  P2D : TJSPath2D;
  aType,X,Y,X1,Y1,X2,Y2,X3,Y3,I : Integer;
  V : TJSDataView;
  P : TWasmPointer;
  WasClosed : Boolean;

  Procedure GetTriple;

  begin
    aType:=V.getInt32(P,env.IsLittleEndian);
    Inc(P);
    X:=V.getInt32(P,env.IsLittleEndian);
    inc(P);
    Y:=V.getInt32(P,env.IsLittleEndian);
    inc(P);
  end;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.DrawPath(%d,%d,%d,[%x])',[aID,aFlags,aPathCount,aPath]);
    end;
  {$ENDIF}
  WasClosed:=False;
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  if aPathCount=0 then
    Exit(ECANVAS_INVALIDPATH);
  V:=getModuleMemoryDataView;
  P:=aPath;
  P2D:=TJSPath2D.New;
  For I:=1 to aPathCount-1 do
    begin
    GetTriple;
    WasClosed:=aType<>DRAWPATH_TYPECLOSE;
    Case aType of
      DRAWPATH_TYPEMOVETO : P2D.MoveTo(X,Y);
      DRAWPATH_TYPELINETO : P2D.LineTo(X,Y);
      DRAWPATH_TYPECURVETO :
        begin
        X1:=X;
        Y1:=Y;
        GetTriple;
        if aType<>DRAWPATH_TYPECURVETO then
          begin
          Console.Error('Invalid path data 2, expected CURVETO (',DRAWPATH_TYPECURVETO,'), got: ',aType);
          exit;
          end;
        X2:=X;
        Y2:=Y;
        GetTriple;
        if aType<>DRAWPATH_TYPECURVETO then
          begin
          Console.Error('Invalid path data 3, expected CURVETO (',DRAWPATH_TYPECURVETO,'), got: ',aType);
          exit;
          end;
        X3:=X;
        Y3:=Y;
        P2D.bezierCurveTo(X1,Y1,X2,Y2,X3,Y3);
        end;
      DRAWPATH_TYPECLOSE :
        P2D.ClosePath;
      end;
    end;
  if not WasClosed and ((aFlags and DRAWPATH_CLOSEPATH)<>0) then
    P2D.closePath;
  if (aFlags and DRAWPATH_FILLPATH)<>0 then
    Canv.Fill(P2D);
  if (aFlags and DRAWPATH_STROKEPATH)<>0 then
    Canv.Stroke(P2D);
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.PointInPath(aID: TCanvasID; aX, aY: TFresnelFloat; aPointCount: Integer; aPointData: PFresnelFloat;
  aRes: TWasmPointer): TCanvasError;
var
  Canv:TJSCanvasRenderingContext2D;
  P2D : TJSPath2D;
  aType, I : integer;
  X,Y,X1,Y1,X2,Y2,X3,Y3 : TFresnelFloat;
  V : TJSDataView;
  P : TWasmPointer;
  WasClosed : Boolean;
  Res : Boolean;

  Procedure GetTriple;

  begin
    aType:=Round(V.getFloat32(P,env.IsLittleEndian));
    Inc(P,SizeFloat32);
    X:=V.getFloat32(P,env.IsLittleEndian);
    inc(P,SizeFloat32);
    Y:=V.getFloat32(P,env.IsLittleEndian);
    inc(P,SizeFloat32);
  end;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.PointInPath(%d,(%d,%d),%d,[%x],[%x])',[aID,aX,aY,aPointCount,aPointData,aRes]);
    end;
  {$ENDIF}
  WasClosed:=False;
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  if aPointCount=0 then
    Exit(ECANVAS_INVALIDPATH);
  V:=getModuleMemoryDataView;
  P:=aPointData;
  P2D:=TJSPath2D.New;
  For I:=1 to aPointCount-1 do
    begin
    GetTriple;
    WasClosed:=aType<>DRAWPATH_TYPECLOSE;
    Case aType of
      DRAWPATH_TYPEMOVETO : P2D.MoveTo(X,Y);
      DRAWPATH_TYPELINETO : P2D.LineTo(X,Y);
      DRAWPATH_TYPECURVETO :
        begin
        X1:=X;
        Y1:=Y;
        GetTriple;
        if aType<>DRAWPATH_TYPECURVETO then
          begin
          Console.Error('Invalid path data 2, expected CURVETO (',DRAWPATH_TYPECURVETO,'), got: ',aType);
          exit(ECANVAS_INVALIDPATH);
          end;
        X2:=X;
        Y2:=Y;
        GetTriple;
        if aType<>DRAWPATH_TYPECURVETO then
          begin
          Console.Error('Invalid path data 3, expected CURVETO (',DRAWPATH_TYPECURVETO,'), got: ',aType);
          exit(ECANVAS_INVALIDPATH);
          end;
        X3:=X;
        Y3:=Y;
        P2D.bezierCurveTo(X1,Y1,X2,Y2,X3,Y3);
        end;
      DRAWPATH_TYPECLOSE :
        P2D.ClosePath;
      end;
    end;
  if not WasClosed then
    P2D.closePath;
  Res:=Canv.isPointInPath(P2D,aX,aY);
  v.setInt8(aRes,Ord(Res));
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.SetTransform(aID: TCanvasID; Flags: Longint; m11, m12,
  m21, m22, m31, m32: TFresnelFloat): TCanvasError;

var
  Ref : TCanvasReference;
  DoReset : Boolean;
begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetTransform(%d,%d,[%g,%g,%g,%g,%g,%g])',[aID,Flags,m11,m12,m21,m22,m31,m32]);
    end;
  {$ENDIF}
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  DoReset:=(Flags and TRANSFORM_RESET)<>0;
//  canv.setTransform(m11,m12,m21,m22,m31,m32)
  Ref.SetTransform(m11,m12,m21,m22,m31,m32,DoReset);
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.GetViewPortSizes(Flags: Longint; aWidth, aHeight: PFresnelFloat): TCanvasError;

var
  W,H : Double;
  v : TJSDataView;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.GetViewPortSizes(%d,[%x],[%x])',[Flags,aWidth,aHeight]);
    end;
  {$ENDIF}
  W:=0;
  H:=0;
  if Flags=GETVIEWPORTSIZE_CLIENT then
    begin
    W:=document.documentElement.clientWidth;
    H:=document.documentElement.clientHeight;
    end
  else if FLAGS=GETVIEWPORTSIZE_WINDOW then
    begin
    W:=Window.innerWidth;
    H:=document.documentElement.clientHeight;
    end;
  if (W=0) or (H=0) then
    Result:=ECANVAS_INVALIDPARAM
  else
    begin
    V:=getModuleMemoryDataView;
    v.setFloat32(aWidth,W,env.IsLittleEndian);
    v.setFloat32(aHeight,H,env.IsLittleEndian);
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.SetWindowTitle(aID: TCanvasID; aTitle: TWasmPointer; aTitleLen: Longint): TCanvasError;
Var
  Ref : TCanvasReference;
  S : String;

begin
  S:=Env.GetUTF8StringFromMem(aTitle,aTitleLen);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetWindowTitle(%d,''%s'')',[aID,S]);
    end;
  {$ENDIF}
  if aID=0 then
    begin
    Document.title:=S;
    Exit(ECANVAS_SUCCESS);
    end
  else
    begin
    Result:=ECANVAS_NOCANVAS;
    Ref:=GetCanvasRef(aID);
    if Not Assigned(Ref) then
      exit;
    Ref.title:=S;
    Exit(ECANVAS_SUCCESS);
    end;
end;

function TWasmFresnelApi.SetSpecialKeyMap(Map: TWasmPointer; aLen: longint
  ): TCanvasError;

var
  Ptr : TWasmPointer;
  V : TJSDataView;
  i,old,new : Integer;
  jsname : jsValue;
  name: string absolute jsname;
  inv : TJSObject;

begin
  Result:=0;
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetSpecialKeyMap([%x],%d)',[Map,aLen]);
    end;
  {$ENDIF}
  FKeyMap:=CreateSpecialKeyNameMap;
  V:=getModuleMemoryDataView;
  Ptr:=Map;
  inv:=CreateSpecialKeyCodeMap;
  For I:=1 to aLen do
    begin
    old:=v.getInt32(Ptr,env.IsLittleEndian);
    inc(Ptr,SizeInt32);
    new:=v.getInt32(Ptr,env.IsLittleEndian);
    inc(Ptr,SizeInt32);
    jsname:=inv.Properties[IntToStr(old)];
    if IsString(jsname) then
      FKeyMap.Properties[name]:=New;
    end;
end;

function TWasmFresnelApi.SaveState(aID: TCanvasID; aFlags: Longint): TCanvasError;
Var
  Ref : TCanvasReference;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SaveState(%d)',[aID]);
    end;
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    exit;
  Ref.SaveState;
  Ref.FClipRects:=[];
  if (aFlags and STATE_FLAGS_RESTORE_PROPS)<>0 then
    Ref.ApplyProperties;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.RestoreState(aID: TCanvasID; aFlags: Longint): TCanvasError;

Var
  Ref : TCanvasReference;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.RestoreState(%d)',[aID]);
    end;
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    exit;
  Ref.RestoreState((aFlags and STATE_FLAGS_RESTORE_PROPS)<>0);
  Result:=ECANVAS_SUCCESS;
end;

procedure TWasmFresnelApi.DrawClipRect(Ref: TCanvasReference; aX, aY, aWidth, aHeight: double);

var
  col : jsvalue;
  lw : double;

begin
  With Ref.canvascontext do
     begin
     col:=strokeStyle;
     strokeStyle:='rgb(0 255 0)';
     lw:=linewidth;
     lineWidth:=1;
     strokeRect(aX-1,aY-1,aWidth+2,aHeight+2);
     //Stroke;
     strokeStyle:=Col;
     lineWidth:=lw;
     end;
end;

function TWasmFresnelApi.ClipAddRect(aID: TCanvasID; aX,aY,aWidth,aHeight: TFresnelFloat): TCanvasError;

var
  Ref : TCanvasReference;
  Rect : TClipRect;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.ClipAddRect(%d,(%g,%g)-(%gx%g))',[aID,aX,aY,aWidth,aHeight]);
    end;
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    exit;
  if daClipRect in DebugAPIs then
    DrawClipRect(Ref,aX,aY,aWidth,aHeight);
  Rect.X:=aX;
  Rect.Y:=aY;
  Rect.W:=aWidth;
  Rect.H:=aHeight;
  Ref.AddClipRect(Rect);
  Ref.SetClipPath;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.AllocateTimer(ainterval: longint; userdata: TWasmPointer): TTimerID;

var
  aTimerID : TTimerID;
  CallBack:jsvalue;


  Procedure HandleTimer;

  var
    Continue : boolean;

  begin
    // The instance/timer could have disappeared
    Callback:=InstanceExports['__fresnel_timer_tick'];
    Continue:=Assigned(Callback);
    if Continue then
      Continue:=TTimerTickCallback(CallBack)(aTimerID,userData)
    else
      Console.Error('No more tick callback !');
    if not Continue then
      begin
      {$IFNDEF NOLOGAPICALLS}
      If LogAPICalls then
        LogCall('FresnelAPi.TimerTick(%d), return value false, deactivate',[aTimerID]);
      {$ENDIF}
      DeAllocateTimer(aTimerID);
      end;
  end;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('FresnelApi.AllocateTimer(%d,[%x])',[aInterval,UserData]);
  {$ENDIF}
  Callback:=InstanceExports['__fresnel_timer_tick'];
  if Not Assigned(Callback) then
    Exit(0);
  aTimerID:=Window.setInterval(@HandleTimer,aInterval);
  Result:=aTimerID;
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('FresnelApi.AllocateTimer(%d,[%x] => %d)',[aInterval,UserData,Result]);
  {$ENDIF}
end;

procedure TWasmFresnelApi.DeallocateTimer(timerid: TTimerID);
begin
  If LogAPICalls then
    LogCall('FresnelApi.DeAllocateTimer(%d)',[TimerID]);
  window.clearInterval(TimerID);
end;


function TWasmFresnelApi.SetLineWidth(aID : TCanvasID;aWidth : TCanvasLineWidth):  TCanvasError;

var
  Canv:TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetLineWidth(%d,%g)',[aID,aWidth]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  Canv.LineWidth:=aWidth;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.SetTextBaseLine(aID: TCanvasID; aBaseLine: TCanvasTextBaseLine): TCanvasError;
var
  Ref :TCanvasReference;
  S : String;

begin
  S:=TextBaseLineToString(aBaseLine);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetTextBaseLine(%d,%s)',[aID,S]);
    end;
  {$ENDIF}
  Ref:=GetCanvasRef(aID);
  if Not Assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  Ref.TextBaseLine:=S;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.SetLineCap(aID : TCanvasID; aCap : TCanvasLinecap):  TCanvasError;

var
  Canv:TJSCanvasRenderingContext2D;
  S : String;

begin
  S:=LineCapToString(aCap);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetLineCap(%d,%s)',[aID,S]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  Canv.lineCap:=S;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.SetLineJoin(aID : TCanvasID; aJoin : TCanvasLineJoin):  TCanvasError;

var
  Canv:TJSCanvasRenderingContext2D;
  S : String;

begin
  S:=LineJoinToString(aJoin);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetLineJoin(%d,%s)',[aID,S]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  Canv.lineJoin:=S;
  Result:=ECANVAS_SUCCESS;
end;


function TWasmFresnelApi.SetLineMiterLimit(aID : TCanvasID; aWidth : TCanvasLineMiterLimit):  TCanvasError;

var
  Canv:TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetLineMiterLimit(%d,%d)',[aID,aWidth]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  Canv.miterLimit:=aWidth;
  Result:=ECANVAS_SUCCESS;
  LogCall('Canvas.SetLineMiterLimit not implemented');
end;

function TWasmFresnelApi.SetLineDash(aID: TCanvasID; aOffset: TFresnelFloat;
  aPatternCount: longint; aPattern: PFresnelFloat): TCanvasError;

var
  Dashes : TJSArray;
  V : TJSDataView;
  I : Integer;
  P : TWasmPointer;
  Canv:TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.SetLineDash(%d,%g,%d,[%x])',[aID,aOffset,aPatternCount,aPattern]);
    end;
  {$ENDIF}
  Canv:=GetCanvas(aID);
  if Not Assigned(Canv) then
    Exit(ECANVAS_NOCANVAS);
  Dashes:=TJSArray.New;
  if aPatternCount>0 then
    begin
    V:=getModuleMemoryDataView;
    P:=aPattern;
    for I:=0 to APatternCount-1 do
      begin
      Dashes.Push(v.GetFloat32(P,env.IsLittleEndian));
      Inc(P,SizeFloat32);
      end;
    end;
  Canv.lineDashOffset:=aOffset;
  Canv.setLineDash(Dashes);
end;


{ ---------------------------------------------------------------------
  Event API
  ---------------------------------------------------------------------}

// note that the events are for a single canvas !

function TWasmFresnelApi.GetEvent(aID: TWasmPointer; aMsg: TWasmPointer; Data: TWasmPointer): TCanvasError;

var
  V : TJSDataView;
  Evt : TCanvasEvent;

begin
  if Length(FEvents)=0 then
    Exit(EWASMEVENT_NOEVENT);
  Evt:=FEvents[0];
  Delete(FEvents,0,1);
  V:=getModuleMemoryDataView;
  v.setint32(aID,Evt.CanvasID,env.IsLittleEndian);
  v.setint32(aMsg,Evt.Msg,env.IsLittleEndian);
  v.setint32(Data,Evt.param0,env.IsLittleEndian);
  inc(Data,SizeInt32);
  v.setint32(Data,Evt.param1,env.IsLittleEndian);
  inc(Data,SizeInt32);
  v.setint32(Data,Evt.param2,env.IsLittleEndian);
  inc(Data,SizeInt32);
  v.setint32(Data,Evt.param3,env.IsLittleEndian);
  Result:=EWASMEVENT_SUCCESS;
end;

function TWasmFresnelApi.GetEventCount(aCount: TWasmPointer): TCanvasError;

var
  V : TJSDataView;

begin
  V:=getModuleMemoryDataView;
  v.setint32(aCount,Length(FEvents),env.IsLittleEndian);
  Result:=EWASMEVENT_SUCCESS;
end;

function TWasmFresnelApi.HandleMenuClick(aMenuID: TMenuID; aData: TWasmPointer): Boolean;

var
  Callback : JSValue;

begin
  Result:=False;
  if not assigned(InstanceExports) then
    Console.warn('No instance exports !')
  else
    begin
    Callback:=InstanceExports['__fresnel_menu_click'];
    if Assigned(Callback) then
      begin
      TMenuClickCallback(CallBack)(aMenuID,AData);
      Result:=True;
      end
    else
      Console.warn('No menu click callback !');
    end
end;

function TWasmFresnelApi.AddMenuItem(aCanvasID: TCanvasId; aParentID: TMenuID; aCaption: TWasmPointer; aCaptionLen: Longint;
  aData: TWasmPointer; aFlags: Longint; aShortCut: Longint; aMenuID: PMenuID): TCanvasError;

var
  S : String;
  Ref : TCanvasReference;
  lMenuID : TMenuID;
  el : TJSHTMLElement;
  lFlags : TMenuFlags;

begin
  S:=Env.GetUTF8StringFromMem(aCaption,aCaptionLen);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('FresnelAPI.AddMenuItem(%d,"%s",%d,[%x],[%x])',[aCanvasID,S,aParentID,aData,aMenuID]);
    end;
  {$ENDIF}
  if not MenuSupport then
    Exit(ECANVAS_NOMENUSUPPORT);
  Ref:=GetCanvasRef(aCanvasID);
  if not assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  if not Assigned(Ref.MenuBuilder)then
    Exit(ECANVAS_NOMENUSUPPORT);
  lMenuID:=TFresnelHelper.AllocateMenuID;
  LFlags:=FlagsToMenuFlags(aFlags);
  if Ref.MenuBuilder.AddMenuItem(aParentID,lMenuID,S,lFlags,aShortCut,aData)<>Nil then
    begin
    Env.SetMemInfoInt32(aMenuID,lMenuID);
    El:=TJSHTMLELement(Document.GetElementByID('ffm'+IntToStr(aCanvasID)));
    if assigned(el) then
      el.style.removeProperty('display');
    end;
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.DeleteMenuItem(aCanvasID: TCanvasId; aMenuID: TMenuID): TCanvasError;
var
  S : String;
  Ref : TCanvasReference;
  lMenuID : TMenuID;
  el : TJSHTMLElement;
  lFlags : TMenuFlags;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('FresnelAPI.AddMenuItem(%d,%d)',[aCanvasID,aMenuID]);
    end;
  {$ENDIF}
  if not MenuSupport then
    Exit(ECANVAS_NOMENUSUPPORT);
  Ref:=GetCanvasRef(aCanvasID);
  if not assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  if not Assigned(Ref.MenuBuilder)then
    Exit(ECANVAS_NOMENUSUPPORT);
  Ref.MenuBuilder.RemoveMenuItem(aMenuID);
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.UpdateMenuItem(aCanvasID: TCanvasId; aMenuID: TMenuID; aFlags: Longint; aShortCut: Longint): TCanvasError;
begin

end;

function TWasmFresnelApi.getcanvasbyid(aCanvasElementID: TWasmPointer; aElementIDLen: Longint; aID: TWasmPointer): TCanvasError;

var
  S : String;
  El : TJSElement;
  V : TJSDataView;
  aCanvasID : TCanvasID;

begin
  S:=Env.GetUTF8StringFromMem(aCanvasElementID,aElementIDLen);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.GetCanvasByID(''%s'')',[S]);
    end;
  {$ENDIF}
  el:=Nil;
  if (S<>'') then
    El:=Window.Document.getElementById(S);
  if (El=Nil) then
    Exit(ECANVAS_NOCANVAS);
  if not Sametext(el.tagName,'CANVAS') then
    Exit(ECANVAS_NOCANVAS);
  V:=getModuleMemoryDataView;
  aCanvasID:=TFresnelHelper.AllocateCanvasID;
  FCanvases[IntToStr(aCanvasID)]:=TCanvasReference.Create(aID,Self,TJSHTMLCanvasElement(el),TJSHTMLElement(el.parentElement));
  v.setUint32(aID, aCanvasID, env.IsLittleEndian);
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.deallocatecanvas(aID: TCanvasID): TCanvasError;


var
  Ref : TCanvasReference;
begin
  Ref:=GetCanvasRef(aID);
  if not assigned(Ref) then
    Exit(ECANVAS_NOCANVAS);
  Ref.RemoveCanvas;
  FCanvases[IntToStr(aID)]:=Undefined;
  Ref.Free;
  Result:=ECANVAS_SUCCESS;    
end;


function TWasmFresnelApi.allocatecanvas(SizeX, SizeY: Longint; aID: TWasmPointer): TCanvasError;

Var
  CMenu,CTitle,CParent : TJSHTMLElement;
  Canv : TJSHTMLCanvasElement;
  Ref : TCanvasReference;
  V : TJSDataView;
  aCanvasID : TCanvasID;
  SID: String;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.AllocateCanvas(%d,%d)',[SizeX,SizeY]);
  {$ENDIF}
  aCanvasID:=TFresnelHelper.AllocateCanvasID;
  sID:=IntToStr(aCanvasID);
  CParent:=TJSHTMLElement(document.createElement('div'));
  CParent.id:='ffp'+sID;
  CParent.className:='fresnel-window';
  CParent.style.setProperty('max-width',intTostr(SizeX)+'px');
  CanvasParent.AppendChild(CParent);
  CTitle:=TJSHTMLElement(document.createElement('div'));
  CTitle.id:='fft'+sID;
  CTitle.className:='fresnel-window-title';
  CParent.AppendChild(CTitle);
  if MenuSupport then
    begin
    CMenu:=TJSHTMLElement(document.createElement('div'));
    CMenu.id:='ffm'+sID;
    CMenu.className:='fresnel-window-menu';
    CMenu.style.setProperty('display','none');
    CParent.AppendChild(CMenu);
    end;
  Canv:=TJSHTMLCanvasElement(document.createElement('CANVAS'));
  Canv.id:='ffc'+sID;
  Canv.className:='fresnel-window-client';
  Canv.width:=Round(SizeX);
  Canv.height:=Round(SizeY);
  Canv.Style.setProperty('display','block');
  CParent.AppendChild(Canv);
  V:=getModuleMemoryDataView;
  Ref:=TCanvasReference.Create(aCanvasID,Self,Canv,CParent);
  Ref.WindowTitle:=CTitle;
  Ref.textBaseline:='top';
  If MenuSupport then
    Ref.MenuBuilder:=CreateMenuBuilder(CMenu);
//  Writeln('Set Ref.textBaseline ',Ref.textBaseline,' to ',Ref.canvascontext.textBaseline);
  FCanvases[sID]:=Ref;
  v.setUint32(aID, aCanvasID, env.IsLittleEndian);
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.allocateoffscreencanvas(SizeX, SizeY: Longint; aBitmap: TWasmPointer; aID: TWasmPointer): TCanvasError;

var
  aCanvasID : TCanvasID;
  SID: String;
  Ref : TCanvasReference;
  V : TJSDataView;
  D : TJSUint8ClampedArray;
  ImgData: TJSImageData;

begin
  Result:=0;
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.AllocateOffScreenCanvas(%d,%d,[%x])',[SizeX,SizeY,aBitMap]);
  {$ENDIF}
  aCanvasID:=TFresnelHelper.AllocateCanvasID;
  sID:=IntToStr(aCanvasID);
  Ref:=TCanvasReference.CreateOffScreen(aCanvasID,Self,SizeX,SizeY);
  Ref.textBaseline:='top';
  FCanvases[sID]:=Ref;
  V:=getModuleMemoryDataView;
  if aBitmap<>0 then
    begin
    D:=TJSUint8ClampedArray.New(V.Buffer,aBitmap,SizeX*SizeY*4);
    ImgData:=TJSImageData.new(D,SizeX,SizeY);
    Ref.canvascontext.putImageData(ImgData,0,0);
    end;
  v.setUint32(aID, aCanvasID, env.IsLittleEndian);
end;

function TWasmFresnelApi.moveto(aID : TCanvasID; X, Y : TFresnelFloat): TCanvasError;

Var
  C : TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.MoveTo(%d,%g,%g)',[aID,X,Y]);
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.moveto(X,Y);
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.lineto(aID : TCanvasID; X, Y : TFresnelFloat ):  TCanvasError;

Var
  C : TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.LineTo(%d,%g,%g)',[aID,X,Y]);
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.lineto(X,Y);
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.stroke(aID : TCanvasID): TCanvasError; 

Var
  C : TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.Stroke(%d)',[aID]);
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.Stroke;
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.beginpath(aID : TCanvasID):  TCanvasError; 

Var
  C : TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.BeginPath(%d)',[aID]);
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.beginPath;
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.arc(aID: TCanvasID; X, Y, RadiusX, RadiusY, StartAngle, EndAngle, Rotate: TFresnelFloat;
  Flags: Longint): TCanvasError;

Var
  C : TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.Arc(%d,%g,%g,%g,%g,%g,%g)',[aID,X,Y,RadiusX,RadiusY,StartAngle,EndAngle]);
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.beginPath;
    if RadiusX=RadiusY then
      C.Arc(X,y,RadiusX,Startangle,EndAngle)
    else
      C.Ellipse(X,y,RadiusX,RadiusY,Rotate,Startangle,EndAngle);
    if ((Flags and ARC_FILL)<>0) then
      C.fill();
    C.stroke();
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.fillrect(aID : TCanvasID; X, Y, Width, Height : TFresnelFloat): TCanvasError;

Var
  C : TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.FillRect(%d,%g,%g,%g,%g)',[aID,X,Y,Width,Height]);
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.FillRect(X,y,width,Height);
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.strokerect(aID : TCanvasID; X, Y, Width, Height : TFresnelFloat):  TCanvasError;

Var
  C : TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.StrokeRect(%d,%g,%g,%g,%g)',[aID,X,Y,Width,Height]);
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.StrokeRect(X,Y,Width,Height);
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.clearrect(aID : TCanvasID; X, Y, Width, Height : TFresnelFloat):  TCanvasError;

Var
  C : TJSCanvasRenderingContext2D;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.ClearRect(%d,%g,%g,%g,%g)',[aID,X,Y,Width,Height]);
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.ClearRect(X,Y,Width,Height);
    Result:=ECANVAS_SUCCESS;
    end;
end;

function TWasmFresnelApi.RoundRect(aID: TCanvasID; Flags: Longint; Data: PFresnelFloat): TCanvasError;
Var
  C : TJSCanvasRenderingContext2D;
  V : TJSDataView;
  X,Y,W,H : TFresnelFloat;
  Radii : TJSArray;
  Fill : Boolean;

  function GetElement(aOffset : Longint) : TFresnelFloat;
  begin
    Result:=V.getFloat32(Data+(aOffset*SizeFloat32),Env.IsLittleEndian);
  end;

  Procedure AddRadius(aRX,aRY : Double);
  begin
    Radii.Push(New(['x',aRX,'y',aRY]))
  end;

begin
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    LogCall('Canvas.RoundRect(%d,%d,[%d])',[aID,Flags,Data]);
  {$ENDIF}
  C:=GetCanvas(aID);
  if not Assigned(C) then
    Exit(ECANVAS_NOCANVAS);
  V:=getModuleMemoryDataView;
  X:=GetElement(ROUNDRECT_BOXTOPLEFTX);
  Y:=GetElement(ROUNDRECT_BOXTOPLEFTY);
  W:=GetElement(ROUNDRECT_BOXBOTTOMRIGHTX)-X;
  H:=GetElement(ROUNDRECT_BOXBOTTOMRIGHTY)-Y;
  Fill:=(Flags and ROUNDRECT_FLAG_FILL)<>0;
  Radii:=TJSArray.New;
  AddRadius(GetElement(ROUNDRECT_RADIITOPLEFTX),GetElement(ROUNDRECT_RADIITOPLEFTY));
  AddRadius(GetElement(ROUNDRECT_RADIITOPRIGHTX),GetElement(ROUNDRECT_RADIITOPRIGHTY));
  AddRadius(GetElement(ROUNDRECT_RADIIBOTTOMRIGHTX),GetElement(ROUNDRECT_RADIIBOTTOMRIGHTY));
  AddRadius(GetElement(ROUNDRECT_RADIIBOTTOMLEFTX),GetElement(ROUNDRECT_RADIIBOTTOMLEFTY));
  C.BeginPath;
  C.roundRect(X,Y,W,H,Radii);
  if Fill then
    C.fill;
  C.stroke();
  Result:=ECANVAS_SUCCESS;
end;

function TWasmFresnelApi.StrokeText(aID: TCanvasID; X,Y: TFresnelFloat; aText: TWasmPointer; aTextLen: Longint): TCanvasError;

Var
  C : TJSCanvasRenderingContext2D;
  S : String;

begin
  S:=Env.GetUTF8StringFromMem(aText,aTextLen);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.StrokeText(%d,(%g,%g),''%s'')',[aID,X,Y,S]);
    end;
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    C.StrokeText(S,X,Y);
    if daText in DebugApis then
      DrawBaseLine(C,S,X,Y);
    Result:=ECANVAS_SUCCESS;
    end;
end;

procedure TWasmFresnelApi.DrawBaseLine(C: TJSCanvasRenderingContext2D; S: String; X, Y: Double);

var
  M : TJSTextMetrics;
  Style : JSValue;


begin
  if daText in DebugApis then
    begin
    M:=C.measureText(S);
    Style:=C.StrokeStyle;
    C.StrokeStyle:='rgb(255 0 0 /1)';
    C.beginPath;
    C.moveTo(X,Y);
    C.lineTo(X+M.width,y);
    C.stroke;
    C.StrokeStyle:=Style;
    end;
end;

procedure TWasmFresnelApi.SetCreateDefaultCanvas(AValue: Boolean);
begin
  if FCreateDefaultCanvas=AValue then Exit;
  FCreateDefaultCanvas:=AValue;
  if FCreateDefaultCanvas and (DefaultCanvas=Nil) then
    begin
    FDefaultCanvas:=TCanvasReference.CreateOffScreen(0,Self,640,480);
    FCanvases['0']:=FDefaultCanvas;
    end;
end;

procedure TWasmFresnelApi.SetFocusedCanvas(AValue: TCanvasReference);

var
  OtherID : TCanvasID;
  Evt : TCanvasEvent;

begin
  if FFocusedCanvas=AValue then Exit;
  OtherID:=0;
  if Assigned(FFocusedCanvas) then
    begin
    if assigned(aValue) then
      OtherID:=aValue.CanvasID;
    Evt:=TCanvasEvent.Create(FFocusedCanvas.CanvasID,WASMSG_DEACTIVATE,OtherID);
    TJSArray(FEvents).Push(Evt);
    ProcessMessages;
    OtherID:=FFocusedCanvas.CanvasID;
    end;
  FFocusedCanvas:=AValue;
  if Assigned(FFocusedCanvas) then
    begin
    Evt:=TCanvasEvent.Create(FFocusedCanvas.CanvasID,WASMSG_ACTIVATE,OtherID);
    TJSArray(FEvents).Push(Evt);
    ProcessMessages;
    end;
end;


function TWasmFresnelApi.FillText(aID: TCanvasID; X,Y : TFresnelFloat; aText: TWasmPointer; aTextLen: Longint): TCanvasError;
Var
  C : TJSCanvasRenderingContext2D;
  S : String;

begin
  S:=Env.GetUTF8StringFromMem(aText,aTextLen);
  {$IFNDEF NOLOGAPICALLS}
  If LogAPICalls then
    begin
    LogCall('Canvas.FillText(%d,(%g,%g),''%s'')',[aID,X,Y,S]);
    end;
  {$ENDIF}
  Result:=ECANVAS_NOCANVAS;
  C:=GetCanvas(aID);
  if Assigned(C) then
    begin
    S:=Env.GetUTF8StringFromMem(aText,aTextLen);
    C.FillText(S,X,Y);
    if daText in DebugApis then
      DrawBaseLine(C,S,X,Y);
    Result:=ECANVAS_SUCCESS;
    end;
end;

end.  
