{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Skia Rendering backend for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel.SkiaRenderer;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Types, math,
  {$IFDEF Linux}
  dynlibs,
  {$ENDIF}
  AVL_Tree, FpImage, System.UITypes,
  System.Skia, Fresnel.Classes, Fresnel.DOM, Fresnel.Renderer, Fresnel.Images;

const
  CSSToSkRoundRectCorner: array[TFresnelCSSCorner] of TSkRoundRectCorner = (
    TSkRoundRectCorner.UpperLeft,
    TSkRoundRectCorner.UpperRight,
    TSkRoundRectCorner.LowerLeft,
    TSkRoundRectCorner.LowerRight
    );

type
  TFresnelSkiaFontEngine = class;

  { TFresnelSkiaTypeFace }

  TFresnelSkiaTypeFace = class
  private
    FRefCount: integer;
  public
    Engine: TFresnelSkiaFontEngine;
    SKTypeFace: ISkTypeface;
    CSSFamily: string;
    CSSKerning: TFresnelCSSKerning;
    CSSStyle: string;
    CSSWeight: TFresnelLength;
    procedure AddRef;
    procedure Release;
    property RefCount: integer read FRefCount;
  end;

  { TFresnelSkiaFont }

  TFresnelSkiaFont = class(TInterfacedObject,IFresnelFont)
  private
    FTypeFace: TFresnelSkiaTypeFace;
    procedure SetTypeFace(const AValue: TFresnelSkiaTypeFace);
  public
    Engine: TFresnelSkiaFontEngine;
    SKFont: ISkFont;
    SKMetrics: TSkFontMetrics;
    CSSSize: TFresnelLength;
    CSSWidth: TFresnelLength;
    destructor Destroy; override;
    function GetAlternates: string;
    function GetCaps: TFresnelCSSFontVarCaps;
    function GetEastAsians: TFresnelCSSFontVarEastAsians;
    function GetEmoji: TFresnelCSSFontVarEmoji;
    function GetFamily: string;
    function GetKerning: TFresnelCSSKerning;
    function GetLigatures: TFresnelCSSFontVarLigaturesSet;
    function GetNumerics: TFresnelCSSFontVarNumerics;
    function GetPosition: TFresnelCSSFontVarPosition;
    function GetSize: TFresnelLength;
    function GetStyle: string;
    function GetWeight: TFresnelLength;
    function GetWidth: TFresnelLength;
    function TextSize(const aText: string): TFresnelPoint; virtual;
    function TextSizeMaxWidth(const aText: string; MaxWidth: TFresnelLength
      ): TFresnelPoint; virtual;
    function GetTool: TObject;
    function GetDescription : string;
    property TypeFace: TFresnelSkiaTypeFace read FTypeFace write SetTypeFace;
  end;

  { TFresnelSkiaFontEngine - singleton in the widgetset }

  TFresnelSkiaFontEngine = class(TFresnelFontEngine)
  private
    FTypeFaces: TAVLTree; // tree of TFresnelSkiaTypeFace sorted with CompareFresnelSkiaTypeFace
    FFonts: TAVLTree; // tree of TFresnelSkiaFont sorted with CompareFresnelSkiaFont
  protected
    procedure TypeFaceRefCount0(aTypeFace: TFresnelSkiaTypeFace);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function FindFont(const Desc: TFresnelFontDesc): TFresnelSkiaFont; virtual;
    function FindTypeFace(const Desc: TFresnelFontDesc): TFresnelSkiaTypeFace; virtual;
    function Allocate(const Desc: TFresnelFontDesc): IFresnelFont; override;
    function TextSize(aFont: TFresnelSkiaFont; const aText: string): TFresnelPoint; virtual;
    function TextSizeMaxWidth(aFont: TFresnelSkiaFont; const aText: string; const MaxWidth: TFresnelLength): TFresnelPoint; virtual;
    class function GetFont(const FontIntf: IFresnelFont; out FreSkiaFont: TFresnelSkiaFont): boolean; virtual;
  end;

  { TFresnelSkiaFPImage }

  TFresnelSkiaFPImage = class(TFPCustomImage)
  protected
    FBytesPerLine: PtrUInt;
    FData: PByte;
    FDataSize: PtrUInt;
    FSkImage: ISkImage;
    function GetInternalColor(x, y: integer): TFPColor; override;
    procedure SetInternalColor(x, y: integer; const Value: TFPColor); override;
    function GetInternalPixel(x, y: integer): integer; override;
    procedure SetInternalPixel(x, y: integer; Value: integer); override;
    function GetSkImage: ISkImage; virtual;
  public
    constructor Create(AWidth, AHeight: integer); override;
    destructor Destroy; override;
    procedure SetSize(AWidth, AHeight: integer); override;
    property SkImage: ISkImage read GetSkImage;
  end;

  { TFresnelSkiaRenderer - one instance per viewport }

  TFresnelSkiaRenderer = class(TFresnelRenderer)
  private
    FCanvas: ISkCanvas;
    class constructor InitSkiaRenderer;
  protected
    Type
      TSkiaBorderAndBackground = class (TBorderAndBackground)
        Radii: TSkRoundRectRadii;
        procedure CalcRadii;
      end;
  protected
    function PrepareBackgroundBorder(El: TFresnelElement; Params: TBorderAndBackground): Boolean; override;
    function CreateBorderAndBackGround : TBorderAndBackground; override;
    procedure DrawElBackground(El: TFresnelElement; Params: TBorderAndBackground); override;
    procedure DrawElBorder(El: TFresnelElement; Params: TBorderAndBackground); override;
    procedure DrawTextShadow(const aLeft, aTop: TFresnelLength; const FreSkiaFont: TFresnelSkiaFont;
      const aColor: TFPColor; const aRadius: TFresnelLength; const aTextBlob: ISkTextBlob); virtual;
 Public
    procedure Arc(const aColor: TFPColor; const aCenter, aRadii: TFresnelPoint;
      aStartAngle: TFresnelLength = 0; aStopAngle: TFresnelLength = DoublePi); override;
    procedure DrawImage(const aLeft, aTop, aWidth, aHeight: TFresnelLength;
      const aImage: TFPCustomImage); override;
    procedure FillRect(const aColor: TFPColor; const aRect: TFresnelRect); override;
    procedure Line(const aColor: TFPColor; const x1, y1, x2, y2: TFresnelLength); override;
    procedure RoundRect(const aColor: TFPColor; const aRect: TFresnelRoundRect;
      Fill: Boolean); override;
    procedure TextOut(const aLeft, aTop: TFresnelLength;
      const aFont: IFresnelFont; const aColor: TFPColor;
      const aText: string); override;
  public
    constructor Create(AOwner: TComponent); override;
    property Canvas: ISkCanvas read FCanvas write FCanvas;
  end;

function FPColorToSkia(const c: TFPColor): TAlphaColor;

function CompareFresnelSkiaFont(Item1, Item2: Pointer): integer;
function CompareFresnelSkiaTypeFace(Item1, Item2: Pointer): integer;
function CompareFresnelFontDescWithSkiaFont(Key, Item: Pointer): integer;
function CompareFresnelFontDescWithSkiaTypeFace(Key, Item: Pointer): integer;

implementation

function FPColorToSkia(const c: TFPColor): TAlphaColor;
var
  a: TAlphaColorRec;
begin
  a.R:=c.Red shr 8;
  a.G:=c.Green shr 8;
  a.B:=c.Blue shr 8;
  a.A:=c.Alpha shr 8;
  Result:=TAlphaColor(a);
end;

function CompareFresnelSkiaFont(Item1, Item2: Pointer): integer;
var
  Font1: TFresnelSkiaFont absolute Item1;
  Font2: TFresnelSkiaFont absolute Item2;
begin
  Result:=CompareFresnelSkiaTypeFace(Font1.TypeFace,Font2.TypeFace);
  if Result<>0 then exit;
  Result:=CompareValue(Font1.CSSSize,Font2.CSSSize);
  if Result<>0 then exit;
  Result:=CompareValue(Font1.CSSWidth,Font2.CSSWidth);
end;

function CompareFresnelSkiaTypeFace(Item1, Item2: Pointer): integer;
var
  Face1: TFresnelSkiaTypeFace absolute Item1;
  Face2: TFresnelSkiaTypeFace absolute Item2;
begin
  Result:=CompareText(Face1.CSSFamily,Face2.CSSFamily);
  if Result<>0 then exit;
  Result:=CompareValue(ord(Face1.CSSKerning),ord(Face2.CSSKerning));
  if Result<>0 then exit;
  Result:=CompareText(Face1.CSSStyle,Face2.CSSStyle);
  if Result<>0 then exit;
  Result:=CompareValue(Face1.CSSWeight,Face2.CSSWeight);
end;

function CompareFresnelFontDescWithSkiaFont(Key, Item: Pointer): integer;
var
  Desc: PFresnelFontDesc absolute Key;
  aFont: TFresnelSkiaFont absolute Item;
begin
  Result:=CompareFresnelFontDescWithSkiaTypeFace(Key,aFont.TypeFace);
  if Result<>0 then exit;
  Result:=CompareValue(Desc^.Size,aFont.CSSSize);
end;

function CompareFresnelFontDescWithSkiaTypeFace(Key, Item: Pointer): integer;
var
  Desc: PFresnelFontDesc absolute Key;
  Face: TFresnelSkiaTypeFace absolute Item;
begin
  Result:=CompareText(Desc^.Family,Face.CSSFamily);
  if Result<>0 then exit;
  Result:=CompareValue(ord(Desc^.Kerning),ord(Face.CSSKerning));
  if Result<>0 then exit;
  Result:=CompareText(Desc^.Style,Face.CSSStyle);
  if Result<>0 then exit;
  Result:=CompareValue(Desc^.Weight,Face.CSSWeight);
end;

{ TFresnelSkiaTypeFace }

procedure TFresnelSkiaTypeFace.AddRef;
begin
  inc(FRefCount);
end;

procedure TFresnelSkiaTypeFace.Release;
begin
  if FRefCount=0 then
    raise Exception.Create('TFresnelSkiaTypeFace.Release');
  dec(FRefCount);
  if FRefCount=0 then
    Engine.TypeFaceRefCount0(Self);
end;

{ TFresnelSkiaFont }

procedure TFresnelSkiaFont.SetTypeFace(const AValue: TFresnelSkiaTypeFace);
begin
  if FTypeFace=AValue then Exit;
  if FTypeFace<>nil then
    FTypeFace.Release; // this might free FTypeFace
  FTypeFace:=AValue;
  if FTypeFace<>nil then
    FTypeFace.AddRef;
end;

destructor TFresnelSkiaFont.Destroy;
begin
  TypeFace:=nil;
  inherited Destroy;
end;

function TFresnelSkiaFont.GetAlternates: string;
begin
  Result:='normal';
end;

function TFresnelSkiaFont.GetCaps: TFresnelCSSFontVarCaps;
begin
  Result:=ffvcNormal;
end;

function TFresnelSkiaFont.GetEastAsians: TFresnelCSSFontVarEastAsians;
begin
  Result:=[ffveaNormal];
end;

function TFresnelSkiaFont.GetEmoji: TFresnelCSSFontVarEmoji;
begin
  Result:=ffveNormal;
end;

function TFresnelSkiaFont.GetFamily: string;
begin
  Result:=TypeFace.CSSFamily;
end;

function TFresnelSkiaFont.GetKerning: TFresnelCSSKerning;
begin
  Result:=Typeface.CSSKerning;
end;

function TFresnelSkiaFont.GetLigatures: TFresnelCSSFontVarLigaturesSet;
begin
  Result:=[ffvlNormal];
end;

function TFresnelSkiaFont.GetNumerics: TFresnelCSSFontVarNumerics;
begin
  Result:=[ffvnNormal];
end;

function TFresnelSkiaFont.GetPosition: TFresnelCSSFontVarPosition;
begin
  Result:=ffvpNormal;
end;

function TFresnelSkiaFont.GetSize: TFresnelLength;
begin
  Result:=CSSSize;
end;

function TFresnelSkiaFont.GetStyle: string;
begin
  Result:=TypeFace.CSSStyle;
end;

function TFresnelSkiaFont.GetWeight: TFresnelLength;
begin
  Result:=TypeFace.CSSWeight;
end;

function TFresnelSkiaFont.GetWidth: TFresnelLength;
begin
  Result:=CSSWidth;
end;

function TFresnelSkiaFont.TextSize(const aText: string): TFresnelPoint;
begin
  Result:=Engine.TextSize(Self,aText);
end;

function TFresnelSkiaFont.TextSizeMaxWidth(const aText: string;
  MaxWidth: TFresnelLength): TFresnelPoint;
begin
  Result:=Engine.TextSizeMaxWidth(Self,aText,MaxWidth);
end;

function TFresnelSkiaFont.GetTool: TObject;
begin
  Result:=Self;
end;

function TFresnelSkiaFont.GetDescription: string;
begin
  Result:=GetFamily;
end;

{ TFresnelSkiaFontEngine }

procedure TFresnelSkiaFontEngine.TypeFaceRefCount0(
  aTypeFace: TFresnelSkiaTypeFace);
begin
  FTypeFaces.Remove(aTypeFace);
end;

constructor TFresnelSkiaFontEngine.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFonts:=TAVLTree.Create(@CompareFresnelSkiaFont);
  FTypeFaces:=TAVLTree.Create(@CompareFresnelSkiaTypeFace);
end;

destructor TFresnelSkiaFontEngine.Destroy;
var
  Node: TAVLTreeNode;
  aFont: TFresnelSkiaFont;
begin
  Node:=FFonts.Root;
  while Node<>nil do
  begin
    aFont:=TFresnelSkiaFont(Node.Data);
    Node.Data:=nil;
    if aFont.RefCount>1 then
      raise Exception.Create('TFresnelSkiaFontEngine.Destroy RefCount='+IntToStr(aFont.RefCount)+' '+aFont.GetDescription);
    aFont.Engine:=nil;
    aFont._Release;
    Node:=Node.Successor;
  end;
  FreeAndNil(FFonts);

  FTypeFaces.FreeAndClear;
  FreeAndNil(FTypeFaces);

  inherited Destroy;
end;

function TFresnelSkiaFontEngine.FindFont(const Desc: TFresnelFontDesc
  ): TFresnelSkiaFont;
var
  Node: TAVLTreeNode;
begin
  Node:=FFonts.FindKey(@Desc,@CompareFresnelFontDescWithSkiaFont);
  if Node=nil then
    Result:=nil
  else
    Result:=TFresnelSkiaFont(Node.Data);
end;

function TFresnelSkiaFontEngine.FindTypeFace(const Desc: TFresnelFontDesc
  ): TFresnelSkiaTypeFace;
var
  Node: TAVLTreeNode;
begin
  Node:=FFonts.FindKey(@Desc,@CompareFresnelFontDescWithSkiaTypeFace);
  if Node=nil then
    Result:=nil
  else
    Result:=TFresnelSkiaTypeFace(Node.Data);
end;

function TFresnelSkiaFontEngine.Allocate(const Desc: TFresnelFontDesc
  ): IFresnelFont;
var
  aFont: TFresnelSkiaFont;
  aTypeFace: TFresnelSkiaTypeFace;
  aFontStyle: TSkFontStyle;
  SkWeight: integer;
  SkWidth: Integer;
  SkSlant: TSkFontSlant;
  SkFamily, SkDefaultFamilyname: String;
begin
  aFont:=FindFont(Desc);
  if aFont<>nil then
    exit(aFont);

  aTypeFace:=FindTypeFace(Desc);
  if aTypeFace=nil then
  begin
    aTypeFace:=TFresnelSkiaTypeFace.Create;
    aTypeFace.Engine:=Self;
    aTypeFace.CSSFamily:=Desc.Family;
    aTypeFace.CSSKerning:=Desc.Kerning;
    aTypeFace.CSSStyle:=Desc.Style;
    aTypeFace.CSSWeight:=Desc.Weight;
    FTypeFaces.Add(aTypeFace);

    aFontStyle:=TSkFontStyle.Normal;
    SkWeight:=round(Min(Max(aTypeFace.CSSWeight,0),ord(TSkFontWeight.ExtraBlack)));
    SkWidth:=aFontStyle.Width;
    SkSlant:=aFontStyle.Slant;
    if SkWeight<>aFontStyle.Weight then
      aFontStyle:=TSkFontStyle.Create(SkWeight,SkWidth,SkSlant);

    SkDefaultFamilyname:='sans-serif';
    SkFamily:=aTypeFace.CSSFamily;
    if SkFamily='' then
      SkFamily:=SkDefaultFamilyname;
    aTypeface.SKTypeFace := TSkTypeface.MakeFromName(UnicodeString(SkFamily), aFontStyle);
    if aTypeface.SKTypeFace=nil then
      aTypeface.SKTypeFace := TSkTypeface.MakeFromName(UnicodeString(SkDefaultFamilyname), aFontStyle);
  end;

  aFont:=TFresnelSkiaFont.Create;
  aFont.Engine:=Self;
  aFont._AddRef;
  aFont.CSSSize:=Desc.Size;
  aFont.CSSWidth:=Desc.Width;
  aFont.TypeFace:=aTypeFace;
  FFonts.Add(aFont);
  aFont.SKFont := TSkFont.Create(aTypeface.SKTypeFace, Desc.Size, Desc.Width);
  aFont.SKFont.Edging := TSkFontEdging.AntiAlias;
  aFont.SKFont.GetMetrics(aFont.SKMetrics);

  Result:=aFont;
end;

function TFresnelSkiaFontEngine.TextSize(aFont: TFresnelSkiaFont;
  const aText: string): TFresnelPoint;
var
  aRect: TRectF;
begin
  aFont.SKFont.MeasureText(UnicodeString(aText),aRect);
  // writeln('TFresnelSkiaFontEngine.TextSize "',aText,'" CSSSize=',aFont.CSSSize,' Size=',aFont.SKFont.GetSize,' ',aRect.ToString(True));
  Result.X:=aRect.Width;
  Result.Y:=aFont.CSSSize;
end;

function TFresnelSkiaFontEngine.TextSizeMaxWidth(aFont: TFresnelSkiaFont;
  const aText: string; const MaxWidth: TFresnelLength): TFresnelPoint;
var
  aRect: TRectF;
begin
  aFont.SKFont.MeasureText(UnicodeString(aText),aRect);
  Result.X:=aRect.Width;
  Result.Y:=aFont.CSSSize;
  if MaxWidth>Result.X then
    raise Exception.Create('TFresnelSkiaFontEngine.TextSizeMaxWidth ToDo 20230917201535');
end;

class function TFresnelSkiaFontEngine.GetFont(const FontIntf: IFresnelFont; out
  FreSkiaFont: TFresnelSkiaFont): boolean;
var
  Obj: TObject;
begin
  Result:=false;
  FreSkiaFont:=nil;
  if FontIntf=nil then exit;
  Obj:=FontIntf.GetTool;
  if Obj is TFresnelSkiaFont then
  begin
    FreSkiaFont:=TFresnelSkiaFont(Obj);
    Result:=true;
  end;
end;

{ TFresnelSkiaFPImage }

function TFresnelSkiaFPImage.GetSkImage: ISkImage;
begin
  if (FSkImage=nil) and (FData<>nil) then
    FSkImage:=TSkImage.MakeFromRaster(
      TSkImageInfo.Create(Width, Height, SkNative32ColorType, TSkAlphaType.Unpremul),
      FData,FBytesPerLine);
  Result:=FSkImage;
end;

function TFresnelSkiaFPImage.GetInternalColor(x, y: integer): TFPColor;
var
  p: PByte;
  v: DWORD;
  c: word;
begin
  if (x>=0) and (y>=0) and (x<Width) and (y<Height) then
  begin
    p:=FData+FBytesPerLine*y+x*4;
    v:=PDWord(p)^; // reading a DWord solves the big/little endianess
    c:=v and $ff;
    Result.Blue:=c or (c shl 8);
    v:=v shr 8;
    c:=v and $ff;
    Result.Green:=c or (c shl 8);
    v:=v shr 8;
    c:=v and $ff;
    Result.Red:=c or (c shl 8);
    v:=v shr 8;
    Result.Alpha:=c or (c shl 8);
  end else begin
    Result:=colBlack;
  end;
end;

procedure TFresnelSkiaFPImage.SetInternalColor(x, y: integer;
  const Value: TFPColor);
var
  p: PByte;
  v: DWord;
begin
  if (x>=0) and (y>=0) and (x<Width) and (y<Height) then
  begin
    v:=Value.Alpha shr 8;
    v:=v shl 8;
    v:=v or Value.Red shr 8;
    v:=v shl 8;
    v:=v or Value.Green shr 8;
    v:=v shl 8;
    v:=v or Value.Blue shr 8;
    p:=FData+FBytesPerLine*y+x*4;
    PDWord(p)^:=v; // writing a DWord solves the big/little endianess
  end;
end;

function TFresnelSkiaFPImage.GetInternalPixel(x, y: integer): integer;
begin
  if x=0 then exit;
  if y=0 then exit;
  Result:=0;
end;

procedure TFresnelSkiaFPImage.SetInternalPixel(x, y: integer; Value: integer);
begin
  if x=0 then exit;
  if y=0 then exit;
  if Value=0 then exit;
end;

constructor TFresnelSkiaFPImage.Create(AWidth, AHeight: integer);
begin
  inherited Create(AWidth,AHeight);
end;

destructor TFresnelSkiaFPImage.Destroy;
begin
  inherited Destroy;
end;

procedure TFresnelSkiaFPImage.SetSize(AWidth, AHeight: integer);
begin
  AWidth:=Max(0,AWidth);
  AHeight:=Max(0,AHeight);
  if (Width=AWidth) and (Height=AHeight) then exit;

  case SkNative32ColorType of
  TSkColorType.BGRA8888: ;
  TSkColorType.RGBA8888: ;
  else
    raise EFresnel.Create('TFresnelSkiaFPImage.Resize SkNative32ColorType not supported: '+IntToStr(ord(SkNative32ColorType)));
  end;

  FSkImage:=nil;

  inherited SetSize(AWidth, AHeight);

  FBytesPerLine:=AWidth*4;
  FDataSize:=FBytesPerLine*AHeight;
  ReAllocMem(FData,FDataSize);
end;

{ TFresnelSkiaRenderer }

class constructor TFresnelSkiaRenderer.InitSkiaRenderer;
begin
  ImagesConfig.ImageClass := TFresnelSkiaFPImage;
end;

function TFresnelSkiaRenderer.PrepareBackgroundBorder(El: TFresnelElement; Params: TBorderAndBackground): Boolean;

var
  Minwidth : TFresnelLength;

begin
  Result:=inherited PrepareBackgroundBorder(El, Params);
  if not Result then
    exit;
  MinWidth:=GetMinStrokeWidth;
  With Params.BoundingBox.Box do
    Result:=(Width>MinWidth) and (Height>MinWidth);
  if Result then
    if Params.HasRadius then
      (Params as TSkiaBorderAndBackground).CalcRadii;
end;

function TFresnelSkiaRenderer.CreateBorderAndBackGround: TBorderAndBackground;
begin
  Result:=TSkiaBorderAndBackground.Create(Self);
end;


procedure TFresnelSkiaRenderer.DrawElBackground(El: TFresnelElement; Params: TBorderAndBackground);
var
  r: TRectF;
  LinGrad: TFresnelCSSLinearGradient;
  StartP, EndP: TPointF;
  Color1, Color2: TAlphaColor;
  Shader: ISkShader;
  SkPaint: ISkPaint;
  RoundR: ISkRoundRect;
  SkiaParams : TSkiaBorderAndBackground Absolute Params;

begin
  if El=nil then ;

  With Params do
    begin
    if HasBorder then begin
      // with border
        r:=RectF(Width[ffsLeft]/2,
                 Width[ffsTop]/2,
                 BoundingBox.Box.Width-Width[ffsRight]/2,
                 BoundingBox.Box.Height-Width[ffsBottom]/2);
    end else begin
      // no border
      r:=RectF(0,0,BoundingBox.Box.Width,BoundingBox.Box.Height);
    end;
    r.Offset(BoundingBox.Box.Left+Origin.X,BoundingBox.Box.Top+Origin.Y);
    end;

  if Params.BackgroundImage is TFresnelCSSLinearGradient then
  begin
    // draw linear gradient background
    LinGrad:=TFresnelCSSLinearGradient(Params.BackgroundImage);
    StartP:=PointF(LinGrad.StartPoint.X,LinGrad.StartPoint.Y);
    EndP:=PointF(LinGrad.EndPoint.X,LinGrad.EndPoint.Y);
    Color1:=FPColorToSkia(LinGrad.Colors[0].Color);
    if length(LinGrad.Colors)=1 then
      Color2:=Color1
    else
      Color2:=FPColorToSkia(LinGrad.Colors[1].Color);
    SkPaint:=TSkPaint.Create(TSkPaintStyle.Fill);
    Shader:=TSkShader.MakeGradientLinear(StartP,EndP,Color1,Color2);
    SkPaint.Shader:=Shader;

  end else if Params.BackgroundColorFP.Alpha>alphaTransparent then
  begin
    // draw background color
    SkPaint:=TSkPaint.Create(TSkPaintStyle.Fill);
    SkPaint.setColor(FPColorToSkia(Params.BackgroundColorFP));
    SkPaint.SetAntiAlias(true);
  end else
    exit;

  if Params.HasRadius then
  begin
    RoundR:=TSkRoundRect.Create;
    RoundR.SetRect(r,SkiaParams.Radii);
    Canvas.DrawRoundRect(RoundR, SkPaint);
  end else begin
    Canvas.DrawRect(r, SkPaint);
  end;
end;

procedure TFresnelSkiaRenderer.DrawElBorder(El: TFresnelElement; Params: TBorderAndBackground);

var
  r: TRectF;
  SkPaint: ISkPaint;
  Oval: ISkRoundRect;
  SkiaParams : TSkiaBorderAndBackground Absolute Params;

begin
  if El=nil then ;
  // Radii are calculated in prepare step.

  With Params do
    begin
    if HasBorder then begin
      // with border
        r:=RectF(Width[ffsLeft]/2,
                 Width[ffsTop]/2,
                 BoundingBox.Box.Width-Width[ffsRight]/2,
                 BoundingBox.Box.Height-Width[ffsBottom]/2);
    end else begin
      // no border
      r:=RectF(0,0,BoundingBox.Box.Width,BoundingBox.Box.Height);
    end;
    r.Offset(BoundingBox.Box.Left+Origin.X,BoundingBox.Box.Top+Origin.Y);
    end;
  //writeln('TFresnelSkiaRenderer.DrawElBorder ',El.GetPath,' Box=',Params.Box.ToString,' Origin=',Origin.ToString,' r=',r.Left,',',r.Top,',',r.Right,',',r.Bottom);

  // draw border
  if Params.HasBorder then
  begin
    if not Params.SameBorderWidth then ; // todo
    SkPaint:=TSkPaint.Create(TSkPaintStyle.Stroke);
    SkPaint.setColor(FPColorToSkia(Params.Color[ffsLeft]));
    SkPaint.SetStrokeWidth(Params.Width[ffsLeft]);
    SkPaint.SetAntiAlias(true);
    if Params.HasRadius then
    begin
      Oval:=TSkRoundRect.Create;
      Oval.SetRect(r,SkiaParams.Radii);
      Canvas.DrawRoundRect(Oval, SkPaint);
    end else begin
      Canvas.DrawRect(r, SkPaint);
    end;
  end;
end;

procedure TFresnelSkiaRenderer.DrawImage(const aLeft, aTop, aWidth,
  aHeight: TFresnelLength; const aImage: TFPCustomImage);
var
  SkiaImg: TFresnelSkiaFPImage;
  SkImage: ISkImage;
  r: TRectF;
begin
  if not (aImage is TFresnelSkiaFPImage) then
  begin
    if aImage=nil then exit;;
    raise EFresnel.Create('TFresnelSkiaRenderer.DrawImage not supported: '+aImage.ClassName);
  end;
  //writeln('TFresnelSkiaRenderer.DrawImage ',FloatToStr(aLeft),' ',FloatToStr(aTop),' ',FloatToStr(aWidth),' ',FloatToStr(aHeight));
  SkiaImg:=TFresnelSkiaFPImage(aImage);
  SkImage:=SkiaImg.SkImage;
  if SkImage=nil then exit;

  r:=RectF(aLeft,aTop,aLeft+aWidth,aTop+aHeight);
  r.Offset(Origin.X,Origin.Y);
  Canvas.DrawImageRect(SkImage,r);
end;

procedure TFresnelSkiaRenderer.FillRect(const aColor: TFPColor;
  const aRect: TFresnelRect);
var
  SkPaint: ISkPaint;
  r: TRectF;
begin
  //DebugLn(['TFresnelSkiaRenderer.FillRect ',dbgs(aColor),' ',dbgs(aRect)]);
  SkPaint:=TSkPaint.Create(TSkPaintStyle.Fill);
  SkPaint.setColor(FPColorToSkia(aColor));
  r:=aRect.GetRectF;
  r.Offset(Origin.X,Origin.Y);
  Canvas.DrawRect(r, SkPaint);
end;

procedure TFresnelSkiaRenderer.Line(const aColor: TFPColor; const x1, y1, x2,
  y2: TFresnelLength);
var
  SkPaint: ISkPaint;
begin
  SkPaint:=TSkPaint.Create(TSkPaintStyle.Stroke);
  SkPaint.setColor(FPColorToSkia(aColor));
  Canvas.DrawLine(Origin.X+x1,Origin.Y+y1,Origin.X+x2,Origin.Y+y2, SkPaint);
end;

procedure TFresnelSkiaRenderer.RoundRect(const aColor: TFPColor;
  const aRect: TFresnelRoundRect; Fill: Boolean);
var
  c: TFresnelCSSCorner;
  Radii: TSkRoundRectRadii;
  SkPaint: ISkPaint;
  RR: TRectF;
  RoundR: ISkRoundRect;
begin
  if Fill then
    SkPaint:=TSkPaint.Create(TSkPaintStyle.Fill)
  else
    SkPaint:=TSkPaint.Create(TSkPaintStyle.Stroke);
  SkPaint.setColor(FPColorToSkia(aColor));

  for c in TFresnelCSSCorner do
    Radii[CSSToSkRoundRectCorner[c]]:=aRect.Radii[c].GetPointF;
  RR:=aRect.Box.GetRectF;

  RoundR:=TSkRoundRect.Create;
  RoundR.SetRect(RR,Radii);
  Canvas.DrawRoundRect(RoundR, SkPaint);
end;

procedure TFresnelSkiaRenderer.TextOut(const aLeft, aTop: TFresnelLength;
  const aFont: IFresnelFont; const aColor: TFPColor; const aText: string);
var
  FreSkiaFont: TFresnelSkiaFont;
  SkPaint: ISkPaint;
  aTextBlob: ISkTextBlob;
  X, Y: TFresnelLength;
  P : PFresnelTextShadow;
  I : Integer;

begin
  if not TFresnelSkiaFontEngine.GetFont(aFont,FreSkiaFont) then exit;
  //writeln('TFresnelSkiaRenderer.TextOut Origin=',Origin.ToString,' Left=',FloatToStr(aLeft),' Top=',FloatToStr(aTop),' FontSize=',FloatToStr(FreSkiaFont.SKFont.Size),' Color=',dbgs(aColor),' "',aText,'" FontTop=',FloatToStr(FreSkiaFont.SKMetrics.Top),' Ascent=',FloatToStr(FreSkiaFont.SKMetrics.Ascent),' Descent=',FloatToStr(FreSkiaFont.SKMetrics.Descent),' FontBottom=',FloatToStr(FreSkiaFont.SKMetrics.Bottom),' CapHeight=',FloatToStr(FreSkiaFont.SKMetrics.CapHeight));

  aTextBlob:=TSkTextBlob.MakeFromText(UnicodeString(aText),FreSkiaFont.SKFont);

  for I:=0 to TextShadowCount-1 do
    begin
    P:=TextShadow[i];
    DrawTextShadow(aLeft+P^.Offset.X,aLeft+P^.Offset.Y,
                   FreSkiaFont,P^.Color,P^.Radius,aTextBlob);
    end;

  SkPaint:=TSkPaint.Create;
  SkPaint.setColor(FPColorToSkia(aColor));
  X:=Origin.X+aLeft;
  Y:=Origin.Y+aTop - FreSkiaFont.SKMetrics.Ascent;
  Canvas.DrawTextBlob(aTextBlob, X, Y, SkPaint);
end;

procedure TFresnelSkiaRenderer.DrawTextShadow(const aLeft, aTop: TFresnelLength;
  const FreSkiaFont: TFresnelSkiaFont; const aColor: TFPColor;
  const aRadius: TFresnelLength; const aTextBlob: ISkTextBlob);
var
  SkPaint: ISkPaint;
  X, Y: TFresnelLength;
begin
  //writeln('TFresnelSkiaRenderer.TextShadow Origin=',Origin.ToString,' Left=',FloatToStr(aLeft),' Top=',FloatToStr(aTop),' FontSize=',FloatToStr(FreSkiaFont.SKFont.Size),' Color=',dbgs(aColor),' Radius=',FloatToStr(aRadius),' "',aText,'" FontTop=',FloatToStr(FreSkiaFont.SKMetrics.Top),' Ascent=',FloatToStr(FreSkiaFont.SKMetrics.Ascent),' Descent=',FloatToStr(FreSkiaFont.SKMetrics.Descent),' FontBottom=',FloatToStr(FreSkiaFont.SKMetrics.Bottom),' CapHeight=',FloatToStr(FreSkiaFont.SKMetrics.CapHeight));
  SkPaint:=TSkPaint.Create;
  SkPaint.setColor(FPColorToSkia(aColor));
  //SkPaint.MaskFilter:=TSkMaskFilter.MakeBlur(TSkBlurStyle.Normal,);
  SkPaint.ImageFilter:=TSkImageFilter.MakeBlur(aRadius,aRadius);
  X:=Origin.X+aLeft;
  Y:=Origin.Y+aTop - FreSkiaFont.SKMetrics.Ascent;
  Canvas.DrawTextBlob(aTextBlob, X, Y, SkPaint);
end;

procedure TFresnelSkiaRenderer.Arc(const aColor: TFPColor; const aCenter,
  aRadii: TFresnelPoint; aStartAngle: TFresnelLength; aStopAngle: TFresnelLength
  );
var
  Oval: TRectF;
  SkPaint: ISkPaint;
  SkStartAngle, SkSweepAngle: single;
begin
  SkPaint:=TSkPaint.Create(TSkPaintStyle.Stroke);
  SkPaint.setColor(FPColorToSkia(aColor));
  Oval:=RectF(aCenter.X-aRadii.X,aCenter.Y-aRadii.Y,aCenter.X+aRadii.X,aCenter.Y+aRadii.Y);
  SkStartAngle:=RadToDeg(aStartAngle);
  SkSweepAngle:=RadToDeg(aStopAngle-aStartAngle);
  Canvas.DrawArc(Oval,SkStartAngle,SkSweepAngle,false,SkPaint);
end;

constructor TFresnelSkiaRenderer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SubPixel:=true;
end;

{ TFresnelSkiaRenderer.TSkiaBorderAndBackground }

procedure TFresnelSkiaRenderer.TSkiaBorderAndBackground.CalcRadii;

var
  Corner : TFresnelCSSCorner;
  SkCorner : TSkRoundRectCorner;

begin
  Radii:=Default(TSkRoundRectRadii);
  for Corner in TFresnelCSSCorner do
  begin
    SkCorner:=CSSToSkRoundRectCorner[Corner];
    if (BoundingBox.Radii[Corner].X>0) and (BoundingBox.Radii[Corner].Y>0) then
    begin
      Radii[SkCorner].x:=BoundingBox.Radii[Corner].X;
      Radii[SkCorner].y:=BoundingBox.Radii[Corner].Y;
    end;
  end;
end;

end.

