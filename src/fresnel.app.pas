{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Application class for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit Fresnel.App;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, CustApp, Fresnel.Classes, Fresnel.Events, Fresnel.Forms,
  Fresnel.WidgetSet;

type
  TApplicationFlag = (
    AppWaiting,
    AppIdleEndSent,
    AppNoExceptionMessages,
    AppActive, // application has focus
    AppDoNotCallAsyncQueue,
    AppInitialized // initialization of application was done
    );
  TApplicationFlags = set of TApplicationFlag;


  { TFresnelApplication }

  TFresnelApplication = class(TFresnelBaseApplication)
  private
    FCaptureExceptions: Boolean;
    FComponentsToRelease: TFPList;
    FComponentsReleasing: TFPList;
    FCreatingForm: TFresnelForm;
    FFindGlobalComponentEnabled: Boolean;
    FMainForm: TFresnelForm;
    FOldExceptProc: TExceptProc;
    procedure Activate(Data: Pointer);
    procedure Deactivate(Data: Pointer);
  protected
  protected
    FFlags: TApplicationFlags;
    procedure DoBeforeFinalization; virtual;
    procedure DoRun; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure NotifyActivateHandler; virtual;
    procedure NotifyDeactivateHandler; virtual;
    procedure NotifyIdleEndHandler; virtual;
    procedure NotifyIdleHandler; virtual;
    procedure WSAppActivate(Async: Boolean = False); virtual;
    procedure WSAppDeactivate(Async: Boolean = False); virtual;
    procedure WSAppMinimize; virtual;
    procedure WSAppMaximize; virtual;
    procedure WSAppRestore; virtual;
    procedure ReleaseComponent(aComponent : TComponent);

    procedure ProcessAsyncCallQueue; virtual;

    procedure SetCaptureExceptions(const AValue: Boolean); virtual;
    procedure SetFlags(const AValue: TApplicationFlags); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure CreateForm(aClass: TComponentClass; out FormVariable); override;
    procedure UpdateMainForm(AForm: TFresnelForm); virtual;

    // events, queues
    procedure HandleException(Sender: TObject); override;
    procedure HandleMessage; virtual;
    procedure ProcessMessages; override;
    procedure Idle(Wait: Boolean); virtual;

    property CaptureExceptions: Boolean read FCaptureExceptions write SetCaptureExceptions;
    property FindGlobalComponentEnabled: Boolean read FFindGlobalComponentEnabled
                                               write FFindGlobalComponentEnabled;
    property Flags: TApplicationFlags read FFlags write SetFlags;
    property MainForm: TFresnelForm read FMainForm;
    //property MainFormHandle: TFreHandle read GetMainFormHandle;
  end;

var
  FresnelApplication: TFresnelApplication;

implementation

var
  HandlingException: Boolean = False;
  HaltingProgram: Boolean = False;

procedure ExceptionOccurred(Sender: TObject; Addr: Pointer; FrameCount: Longint;
  Frames: PPointer);
Begin
  FLLog(etDebug,'[Fresnel.Forms] ExceptionOccurred ');
  if HaltingProgram or HandlingException then Halt;
  if Addr=nil then ;
  if FrameCount=0 then ;
  if Frames=nil then ;

  HandlingException:=true;
  if Sender<>nil then
  begin
    FLLog(etDebug,'  Sender='+Sender.Tostring);
  end else
      FLLog(etDebug,'  Sender=nil');
  if FresnelApplication<>nil then
    FresnelApplication.HandleException(Sender);
  HandlingException:=false;
end;

// Callback function for RegisterFindGlobalComponentProc
function FindApplicationComponent(const ComponentName: string): TComponent;
// Note: this function is used by TReader to auto rename forms to unique names.
begin
  if FresnelApplication.FindGlobalComponentEnabled then
  begin
    // ignore designer forms (the IDE registers its own functions to handle them)
    Result:=FresnelApplication.FindComponent(ComponentName);
    //if Result=nil then
    //  Result:=Screen.FindNonDesignerForm(ComponentName);
    //if Result=nil then
    //  Result:=Screen.FindNonDesignerDataModule(ComponentName);
  end
  else
    Result:=nil;
  //debugln('FindApplicationComponent ComponentName="',ComponentName,'" Result=',DbgSName(Result));
end;

// Callback function for SysUtils.OnGetApplicationName
function GetApplicationName: string;
begin
  if Assigned(FresnelApplication) then
    Result := FresnelApplication.Title
  else
    Result := '';
end;

procedure BeforeFinalization;
// This is our ExitProc handler.
begin
  FresnelApplication.DoBeforeFinalization;
end;

{ TFresnelApplication }

procedure TFresnelApplication.SetCaptureExceptions(const AValue: Boolean);
begin
  if FCaptureExceptions=AValue then Exit;
  FCaptureExceptions:=AValue;
  if FCaptureExceptions then begin
    // capture exceptions
    // store old exceptproc
    if FOldExceptProc=nil then
      FOldExceptProc:=ExceptProc;
    ExceptProc:=@ExceptionOccurred;
  end else begin
    // do not capture exceptions
    if ExceptProc=@ExceptionOccurred then begin
      // restore old exceptproc
      ExceptProc:=FOldExceptProc;
      FOldExceptProc:=nil;
    end;
  end;
end;

procedure TFresnelApplication.SetFlags(const AValue: TApplicationFlags);
begin
  // Only allow AppNoExceptionMessages to be changed
  FFlags := Flags - [AppNoExceptionMessages] + AValue*[AppNoExceptionMessages];
end;

procedure TFresnelApplication.Activate(Data: Pointer);
begin
  if AppActive in FFlags then exit;
  Include(FFlags, AppActive);
  NotifyActivateHandler;
  if Data=nil then ;
end;

procedure TFresnelApplication.Deactivate(Data: Pointer);
begin
  if not (AppActive in FFlags) then exit;
  Exclude(FFlags, AppActive);
  NotifyDeactivateHandler;
  if Data=nil then ;
end;


procedure TFresnelApplication.DoBeforeFinalization;
var
  i: Integer;
begin
  if Self=nil then exit;
  for i := ComponentCount - 1 downto 0 do
  begin
    // DebugLn('TApplication.DoBeforeFinalization ',DbgSName(Components[i]));
    if i < ComponentCount then
      Components[i].Free;
  end;
end;

procedure TFresnelApplication.DoRun;
begin
  inherited DoRun;
  if CaptureExceptions then
    try // run with try..except
      HandleMessage;
    except
      HandleException(Self);
    end
  else
    HandleMessage; // run without try..except
end;

procedure TFresnelApplication.ProcessAsyncCallQueue;
// Call all methods queued to be called (QueueAsyncCall)

begin
  DoHandleAsyncCalls;
end;

constructor TFresnelApplication.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  if FresnelApplication<>nil then
    raise Exception.Create('20230908115659');
  FresnelApplication:=Self;
  CustomApplication:=Self;

  RegisterFindGlobalComponentProc(@FindApplicationComponent);
  OnGetApplicationName := @GetApplicationName;

  CaptureExceptions:=true;
  AddExitProc(@BeforeFinalization);
end;

destructor TFresnelApplication.Destroy;

begin
  ProcessAsyncCallQueue;
  UnregisterFindGlobalComponentProc(@FindApplicationComponent);

  inherited Destroy;

  Include(FFlags,AppDoNotCallAsyncQueue);
  ProcessAsyncCallQueue;

  // restore exception handling
  CaptureExceptions:=false;
  OnGetApplicationName := nil;

  FresnelApplication:=nil;
  CustomApplication:=nil;
end;

procedure TFresnelApplication.CreateForm(aClass: TComponentClass; out
  FormVariable);
var
  Instance: TComponent;
  ok: boolean;
  AForm: TFresnelForm;
begin
  // Allocate the instance, without calling the constructor
  Instance := TComponent(aClass.NewInstance);
  // set the Reference before the constructor is called, so that
  // events and constructors can refer to it
  TComponent(FormVariable) := Instance;

  if Instance is TFresnelForm then
    AForm := TFresnelForm(Instance)
  else
    AForm:=nil;

  ok:=false;
  try
    if (FCreatingForm=nil) and (AForm<>nil) then
      FCreatingForm:=AForm;
    Instance.Create(Self);

    if AForm<>nil then
    begin
      UpdateMainForm(AForm);
      if FMainForm = AForm then
        AForm.CreateWSForm;
      if AForm.FormStyle = fsSplash then
      begin
        // show the splash form and handle the paint message
        AForm.Show;
        AForm.Invalidate;
        ProcessMessages;
      end;
    end;

    ok:=true;
  finally
    if not ok then begin
      TComponent(FormVariable) := nil;
    end;
    if FCreatingForm=Instance then
      FCreatingForm:=nil;
  end;
end;

procedure TFresnelApplication.UpdateMainForm(AForm: TFresnelForm);
begin
  if (FMainForm = nil)
  and (FCreatingForm=AForm)
  and (not (csDestroying in ComponentState))
  and not (AForm.FormStyle in [fsSplash])
  then
    FMainForm := AForm;
end;

procedure TFresnelApplication.HandleException(Sender: TObject);
begin
  // todo
  inherited HandleException(Sender);
end;

procedure TFresnelApplication.HandleMessage;
var
  Context: TFreHandle;
begin
  Context := WidgetSet.BeginMessageProcess;
  try
    WidgetSet.AppProcessMessages; // process all events
    if not Terminated then Idle(true);
  finally
    WidgetSet.EndMessageProcess(Context);
  end;
end;

procedure TFresnelApplication.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FComponentsToRelease<>nil then
      FComponentsToRelease.Remove(AComponent);
    if FComponentsReleasing<>nil then
      FComponentsReleasing.Remove(AComponent);
    if AComponent = MainForm then begin
      FMainForm:= nil;
      Terminate;
    end;
  end;
end;

procedure TFresnelApplication.NotifyActivateHandler;
begin
  EventDispatcher.DispatchEvent(evtActivate);
end;

procedure TFresnelApplication.NotifyDeactivateHandler;
begin
  EventDispatcher.DispatchEvent(evtDeActivate);
end;

procedure TFresnelApplication.NotifyIdleEndHandler;
begin
  EventDispatcher.DispatchEvent(evtIdleEnd);
end;

procedure TFresnelApplication.NotifyIdleHandler;
begin
  EventDispatcher.DispatchEvent(evtIdle);
end;

procedure TFresnelApplication.ProcessMessages;
var
  Context: TFreHandle;
begin
  if Self=nil then begin
    // when the programmer did a mistake, avoid getting strange errors
    raise Exception.Create('Application=nil');
  end;
  Context := WidgetSet.BeginMessageProcess;
  try
    WidgetSet.AppProcessMessages;
    if not Terminated then
      ProcessAsyncCallQueue;
  finally
    WidgetSet.EndMessageProcess(Context);
  end;
end;

procedure TFresnelApplication.Idle(Wait: Boolean);
begin
  ProcessAsyncCallQueue;
  //NotifyIdleHandler;

  // wait till something happens
  Include(FFlags,AppWaiting);
  Exclude(FFlags,AppIdleEndSent);
  if Wait then
    WidgetSet.AppWaitMessage;
  Exclude(FFlags,AppWaiting);
end;

procedure TFresnelApplication.WSAppActivate(Async: Boolean);
begin
  if Async then
    QueueAsyncCall(@Activate, Self)
  else
    Activate(Self);
end;

procedure TFresnelApplication.WSAppDeactivate(Async: Boolean);
begin
  if Async then
    QueueAsyncCall(@Deactivate, Self)
  else
    Deactivate(Self);
end;

procedure TFresnelApplication.WSAppMinimize;
begin
  EventDispatcher.DispatchEvent(evtMinimize);
end;

procedure TFresnelApplication.WSAppMaximize;
begin
  EventDispatcher.DispatchEvent(evtMaximize);
end;

procedure TFresnelApplication.WSAppRestore;
begin
  //Screen.RestoreLastActive;
  EventDispatcher.DispatchEvent(evtRestore);
end;

procedure TFresnelApplication.ReleaseComponent(aComponent: TComponent);

begin
  if csDestroying in AComponent.ComponentState then exit;
  //DebugLn(['TApplication.ReleaseComponent ',DbgSName(AComponent)]);
  if csDesigning in ComponentState then begin
    // free immediately
    AComponent.Free;
  end else
    AsyncCalls.QueueAsyncCall(Nil,aComponent,True);
end;

end.

