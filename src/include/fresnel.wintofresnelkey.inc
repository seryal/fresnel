Function KeyCodeToUnicode(aKey,aFlags : DWord) : Word;

var
  lState : array[0..255] of byte;
  lChars : array[0..1] of unicodechar; // Buffer for Unicode character (including null terminator)
  lRes : Integer;
//  lLayout : HKL;

begin
  if (aKey>VK_0) and (aKey<VK_Z) then
    Result:=Ord(akey)
  else
    Result:=0;
(*
  Result:=0;
  if (GetKeyboardState(@lState)<>0)  then
    exit;
  lLayout:=GetKeyboardLayout(0); // 0 means the current thread's input locale.
  lRes:=ToUnicodeEx(
        aKey,          // Virtual-key code
        ((aFlags shr 16) and $FF), // Scan code
        @lState,   // Keyboard-state array
        @lChars,     // Buffer for translated character
        2,               // Size of buffer
        1,               // Flags (1 means translate dead keys)
        lLayout  // Current keyboard layout
  );
  if (lRes=1) then
    Result:=Ord(lChars[0]); // Return the Unicode character
  else if (lRes = -1) then
    begin
    // Dead-key character; ToUnicodeEx will have stored the dead key state.
    // So we must call ToUnicodeEx again with a space to get the composed character.
    lRes:=ToUnicodeEx(
        VK_SPACE,
        MapVirtualKeyEx(VK_SPACE, MAPVK_VK_TO_VSC, lLayout),
        @lState,
        @lChars,
        2,
        1,
        lLayout
    );
    if (res=1) then
      Result:=Ord(lChars([0])); // Return the composed character.
    end;
*)
end;

Function WinKeyToFresnelKey(aKey,aFlags : DWord) : Integer;

begin
  Result:=KeyCodeToUnicode(aKey,aFlags);
  if Result<>0 then
    exit;
  Case aKey of
    VK_BACK : Result:=TKeyCodes.BackSpace;
    VK_TAB : Result:=TKeyCodes.Tab;
    VK_CLEAR : Result:=TKeyCodes.Clear;
    VK_RETURN : Result:=TKeyCodes.Enter;
    VK_LSHIFT,
    VK_RSHIFT,
    VK_SHIFT : Result:=TKeyCodes.Shift;
    VK_LCONTROL,
    VK_RCONTROL,
    VK_CONTROL 	: Result:=TKeyCodes.Control;
    VK_LMENU,
    VK_RMENU,
    VK_MENU 	: Result:=TKeyCodes.Alt;
    VK_PAUSE 	: Result:=TKeyCodes.Pause;
    VK_CAPITAL 	: Result:=TKeyCodes.CapsLock;
    VK_KANA 	: Result:=TKeyCodes.KanaMode;
//    VK_HANGUL 	: Result:=TKeyCodes.HangulMode;
    VK_JUNJA 	: Result:=TKeyCodes.JunjaMode;
    VK_FINAL 	: Result:=TKeyCodes.FinalMode;
    VK_HANJA 	: Result:=TKeyCodes.HanjaMode;
//    VK_KANJI 	: Result:=TKeyCodes.KanjiMode;
    VK_ESCAPE 	: Result:=TKeyCodes.Escape;
    VK_CONVERT 	: Result:=TKeyCodes.Convert ;
    VK_NONCONVERT 	: Result:=TKeyCodes.NonConvert;
    VK_ACCEPT 	: Result:=TKeyCodes.Accept ;
    VK_MODECHANGE 	: Result:=TKeyCodes.ModeChange;
    VK_SPACE 	: Result:=Ord(' ');
    VK_PRIOR 	: Result:=TKeyCodes.PageUp;
    VK_NEXT 	: Result:=TKeyCodes.PageDown;
    VK_END 	: Result:=TKeyCodes.End_;
    VK_HOME 	: Result:=TKeyCodes.Home;
    VK_LEFT 	: Result:=TKeyCodes.ArrowLeft;
    VK_UP 	: Result:=TKeyCodes.ArrowUp;
    VK_RIGHT 	: Result:=TKeyCodes.ArrowRight;
    VK_DOWN 	: Result:=TKeyCodes.ArrowDown;
    VK_SELECT 	: Result:=TKeyCodes.Select ;
    VK_EXECUTE 	: Result:=TKeyCodes.Execute ;
    VK_SNAPSHOT 	: Result:=TKeyCodes.PrintScreen ;
    VK_INSERT 	: Result:=TKeyCodes.Insert ;
    VK_DELETE 	: Result:=TKeyCodes.Delete ;
    VK_HELP 	: Result:=TKeyCodes.Help ;
    VK_LWIN 	: Result:=TKeyCodes.Meta ;
    VK_RWIN 	: Result:=TKeyCodes.Meta ;
    VK_APPS 	: Result:=TKeyCodes.Contextmenu ;
    VK_SLEEP 	: Result:=TKeyCodes.StandBy ;
    VK_NUMPAD0 	: Result:=Ord('0');
    VK_NUMPAD1 	: Result:=Ord('1')+(aKey-VK_NUMPAD1);
    VK_MULTIPLY 	: Result:=TKeyCodes.Multiply ;
    VK_ADD 	: Result:=TKeyCodes.Add ;
    VK_SEPARATOR 	: Result:=TKeyCodes.Separator ;
    VK_SUBTRACT 	: Result:=TKeyCodes.Subtract ;
    VK_DECIMAL 	: Result:=TKeyCodes.Decimal ;
    VK_DIVIDE 	: Result:=TKeyCodes.Divide ;
    VK_F1..VK_F24 	: Result:=TKeyCodes.F1+(aKey-VK_F1);
    VK_NUMLOCK 	: Result:=TKeyCodes.NumLock ;
    VK_SCROLL 	: Result:=TKeyCodes.ScrollLock ;
    VK_BROWSER_BACK 	: Result:=TKeyCodes.BrowserBack;
    VK_BROWSER_FORWARD 	: Result:=TKeyCodes.BrowserForward;
    VK_BROWSER_REFRESH 	: Result:=TKeyCodes.BrowserRefresh;
    VK_BROWSER_STOP 	: Result:=TKeyCodes.BrowserStop;
    VK_BROWSER_SEARCH 	: Result:=TKeyCodes.Browsersearch;
    VK_BROWSER_FAVORITES 	: Result:=TKeyCodes.BrowserFavorites;
    VK_BROWSER_HOME 	: Result:=TKeyCodes.BrowserHome;
    VK_VOLUME_MUTE 	: Result:=TKeyCodes.AudioVolumeMute;
    VK_VOLUME_DOWN 	: Result:=TKeyCodes.AudioVolumeDown;
    VK_VOLUME_UP 	: Result:=TKeyCodes.AudioVolumeUp;
    VK_MEDIA_NEXT_TRACK 	: Result:=TKeyCodes.MediaTrackNext;
    VK_MEDIA_PREV_TRACK 	: Result:=TKeyCodes.MediaTrackPrevious;
    VK_MEDIA_STOP 	: Result:=TKeyCodes.MediaStop;
    VK_MEDIA_PLAY_PAUSE 	: Result:=TKeyCodes.MediaPlayPause;
    VK_LAUNCH_MAIL 	: Result:=TKeyCodes.LaunchMail ;
    VK_LAUNCH_MEDIA_SELECT 	: Result:=TKeyCodes.LaunchMediaPlayer ;
    VK_LAUNCH_APP1 	: Result:=TKeyCodes.LaunchApplication1 ;
    VK_LAUNCH_APP2 	: Result:=TKeyCodes.LaunchApplication2 ;
    VK_PROCESSKEY 	: Result:=TKeyCodes.Process ;
    VK_ATTN 	: Result:=TKeyCodes.KanaMode ;
    VK_CRSEL 	: Result:=TKeyCodes.CrSel ;
    VK_EXSEL 	: Result:=TKeyCodes.ExSel ;
    VK_EREOF 	: Result:=TKeyCodes.EraseEof ;
    VK_PLAY 	: Result:=TKeyCodes.Play ;
    VK_ZOOM 	: Result:=TKeyCodes.ZoomToggle ;
    VK_OEM_CLEAR 	: Result:=TKeyCodes.Clear ;
  else
    Result:=0;
  { // Not supported
    // VK_CANCEL :      : Result:=TKeyCodes. ;
    // VK_IME_ON 	: Result:=TKeyCodes. ;
    // VK_IME_OFF 	: Result:=TKeyCodes. ;
    // VK_PRINT 	: Result:=TKeyCodes. ;
    // VK_PACKET 	: Result:=TKeyCodes. ;
    // VK_NONAME 	: Result:=TKeyCodes. ;
    // VK_PA1 	        : Result:=TKeyCodes. ;
    // VK_OEM_1 	: Result:=TKeyCodes. ;
    // VK_OEM_PLUS 	: Result:=TKeyCodes. ;
    // VK_OEM_COMMA 	: Result:=TKeyCodes. ;
    // VK_OEM_MINUS 	: Result:=TKeyCodes. ;
    // VK_OEM_PERIOD 	: Result:=TKeyCodes. ;
    // VK_OEM_2 	: Result:=TKeyCodes. ;
    // VK_OEM_3 	: Result:=TKeyCodes. ;
    // VK_OEM_4 	: Result:=TKeyCodes. ;
    // VK_OEM_5 	: Result:=TKeyCodes. ;
    // VK_OEM_6 	: Result:=TKeyCodes. ;
    // VK_OEM_7 	: Result:=TKeyCodes. ;
    // VK_OEM_8 	: Result:=TKeyCodes. ;
    // VK_OEM_102 	: Result:=TKeyCodes. ;

  }
  end;
end;


