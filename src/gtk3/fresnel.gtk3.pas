unit Fresnel.Gtk3;

{$mode objfpc}{$H+}
{$IF FPC_FULLVERSION>30300}
{$WARN 6060 off : Case statement does not handle all possible cases}
{$WARN 6058 off : Call to subroutine "$1" marked as inline is not inlined}
{$ENDIF}

interface

uses
  Math, Types, Classes, BaseUnix, Unix, sysutils, System.UITypes,
  // gtk3
  LazGtk3, LazGObject2, LazGLib2, LazGio2, Lazcairo1, LazGdk3,
  {$IFDEF FresnelSkia}
  // skia
  {$IFDEF Linux}
  dynlibs,
  {$ENDIF}
  System.Skia, Fresnel.SkiaRenderer,
  {$ENDIF}
  // fresnel
  Fresnel.Classes, Fresnel.Forms, Fresnel.WidgetSet, Fresnel.DOM,
  Fresnel.Events, FCL.Events, Fresnel.Keys;

const
  GTK3_LEFT_BUTTON = 1;
  GTK3_MIDDLE_BUTTON = 2;
  GTK3_RIGHT_BUTTON = 3;

type
  {$IFDEF FresnelSkia}
  TGtk3FontEngine = TFresnelSkiaFontEngine;
  TGtk3Renderer = TFresnelSkiaRenderer;
  {$ENDIF}

  { TGtk3WSForm }

  TGtk3WSForm = class(TFresnelWSForm)
  private
    FClientRect: TRect;
    FForm: TFresnelCustomForm;
    FWindow: PGtkWindow;
    FIMContext : PGtkIMMulticontext;
    procedure SetForm(const AValue: TFresnelCustomForm);
    procedure CreateIMContext;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation);
      override;
    function GetFormBounds: TFresnelRect; override;
    function GetCaption: TFresnelCaption; override;
    function GetVisible: boolean; override;
    procedure SetFormBounds(const AValue: TFresnelRect); override;
    procedure SetCaption(AValue: TFresnelCaption); override;
    procedure SetVisible(const AValue: boolean); override;

  public
    function GetClientSize: TFresnelPoint; override;
    function GtkEventDraw(AContext: Pcairo_t): Boolean; virtual;
    function GtkIMECommit(aText : string) : Boolean;
    function GtkEventKeyDown(Event: PGdkEvent): Boolean; virtual;
    function GtkEventKeyUp(Event: PGdkEvent): Boolean; virtual;
    function GtkEventMouseXY(Event: PGdkEvent): Boolean; virtual; // MouseUp/Move/Down
    function GtkEventMouseEnter(Event: PGdkEvent): Boolean; virtual;
    function GtkEventMouseLeave(Event: PGdkEvent): Boolean; virtual;
    function GtkEvent_Event(Event: PGdkEvent): Boolean; virtual;
    procedure GtkEventDestroy; virtual;
    procedure GtkEventHide; virtual;
    procedure GtkEventMap; virtual;
    procedure GtkEventShow; virtual;
    procedure GtkEventSizeAllocate(aRect: PGdkRectangle); virtual;
    procedure Invalidate; override;
    procedure InvalidateRect(const aRect: TFresnelRect); override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    function CreateGtkWindow: PGtkWindow; virtual;
    function GetWindowState: TGdkWindowState; virtual;
    property Window: PGtkWindow read FWindow;
    property IMContext : PGtkIMMulticontext Read FIMContext;

    property Form: TFresnelCustomForm read FForm write SetForm;
  end;

  { TGtk3WidgetSet }

  TGtk3WidgetSet = class(TFresnelWidgetSet)
  private
    FFontEngine: TGtk3FontEngine;
    FGtk3Application: PGtkApplication;
  protected
    procedure InitSynchronizeSupport; virtual;
    procedure PrepareSynchronize({%H-}AObject: TObject); virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AppWaitMessage; override;
    procedure AppProcessMessages; override;
    procedure AppTerminate; override;
    procedure AppSetTitle(const ATitle: string); override;

    procedure CreateWSForm(aFresnelForm: TFresnelComponent); override;
    property Gtk3Application: PGtkApplication read FGtk3Application;
    property FontEngineGtk3: TGtk3FontEngine read FFontEngine;
  end;

var
  Gtk3WidgetSet: TGtk3WidgetSet;

function RectFromGdkRect(const AGdkRect: TGdkRectangle): TRect;
function FresnelRectFromGdkRect(const AGdkRect: TGdkRectangle): TFresnelRect;
function GdkRectFromRect(const R: TRect): TGdkRectangle;
function GdkRectFromFresnelRect(const R: TFresnelRect): TGdkRectangle;

function GdkModifierStateToShiftState(const AState: TGdkModifierType): TShiftState;

implementation

uses UTF8Utils;

var
  ThreadSync_PipeIn, ThreadSync_PipeOut: cint;
  ThreadSync_GIOChannel: PGIOChannel;

function ThreadSync_IOCallback({%H-}Source: PGIOChannel; {%H-}Condition: TGIOCondition;
  {%H-}Data: gpointer): gboolean; cdecl;
var
  ThrashSpace: array[1..1024] of byte;
begin
  // read the sent bytes
  FpRead(ThreadSync_PipeIn, {%H-}ThrashSpace[1], 1);

  // execute the to-be synchronized method
  if IsMultiThread then
    CheckSynchronize;

  Result := true;
end;

procedure Gtk3WidgetDestroy(AWidget: PGtkWidget; Data: gpointer); cdecl;
var
  aForm: TGtk3WSForm;
begin
  if AWidget=nil then ;
  if Data = nil then exit;
  aForm := TGtk3WSForm(Data);
  aForm.GtkEventDestroy;
end;

function Gtk3DrawWidget(AWidget: PGtkWidget; AContext: Pcairo_t; Data: gpointer): gboolean; cdecl;
var
  aForm: TGtk3WSForm;
begin
  Result := False;
  if AWidget=nil then ;
  if Data = nil then exit;
  aForm := TGtk3WSForm(Data);
  Result := aForm.GtkEventDraw(AContext);
end;

function Gtk3WidgetEvent(AWidget: PGtkWidget; Event: PGdkEvent; Data: gpointer): gboolean; cdecl;
var
  aForm: TGtk3WSForm;
begin
  Result := False;
  if AWidget=nil then ;
  if Data = nil then exit;
  aForm := TGtk3WSForm(Data);
  Result := aForm.GtkEvent_Event(Event);
end;

function Gtk3WidgetRealize(AWidget: PGtkWidget; Data: gpointer): gboolean; cdecl;

var
  lForm : TGtk3WSForm;

begin
  if AWidget=nil then ;
  if Data = nil then exit;
  lForm := TGtk3WSForm(Data);
  lForm.CreateIMContext;
  result:=true;
end;

procedure Gtk3WidgetHide({%H-}AWidget: PGtkWidget; Data: gpointer); cdecl;
var
  aForm: TGtk3WSForm;
begin
  if AWidget=nil then ;
  if Data = nil then exit;
  aForm := TGtk3WSForm(Data);
  aForm.GtkEventHide;
end;

procedure Gtk3WidgetMap({%H-}AWidget: PGtkWidget; Data: gpointer); cdecl;
var
  aForm: TGtk3WSForm;
begin
  if AWidget=nil then ;
  if Data = nil then exit;
  aForm := TGtk3WSForm(Data);
  aForm.GtkEventMap;
end;

procedure Gtk3WidgetShow({%H-}AWidget: PGtkWidget; Data: gpointer); cdecl;
var
  aForm: TGtk3WSForm;
begin
  if AWidget=nil then ;
  if Data = nil then exit;
  aForm := TGtk3WSForm(Data);
  aForm.GtkEventShow;
end;

procedure Gtk3WidgetSizeAllocate(AWidget: PGtkWidget; AGdkRect: PGdkRectangle; Data: gpointer); cdecl;
var
  aForm: TGtk3WSForm;
begin
  if AWidget=nil then ;
  if Data = nil then exit;
  aForm := TGtk3WSForm(Data);
  aForm.GtkEventSizeAllocate(AGdkRect);
end;


procedure Gtk3IMContextCommit(aContext: PGtkIMMulticontext; aStr : PAnsiChar; aData: gpointer);

var
  aForm: TGtk3WSForm;
  lStr : String;

begin
  if aContext=nil then ;
  if aData = nil then exit;
  if astr=nil then exit;
  lStr:=aStr;
  aForm:=TGtk3WSForm(aData);
  aForm.GtkIMECommit(lStr);
end;




function RectFromGdkRect(const AGdkRect: TGdkRectangle): TRect;
begin
  with AGdkRect do
  begin
    Result.Left := x;
    Result.Top := y;
    Result.Right := Width + x;
    Result.Bottom := Height + y;
  end;
end;

function FresnelRectFromGdkRect(const AGdkRect: TGdkRectangle): TFresnelRect;
begin
  with AGdkRect do
  begin
    Result.Left := x;
    Result.Top := y;
    Result.Right := Width + x;
    Result.Bottom := Height + y;
  end;
end;

function GdkRectFromRect(const R: TRect): TGdkRectangle;
begin
  with Result do
  begin
    x := R.Left;
    y := R.Top;
    width := R.Right-R.Left;
    height := R.Bottom-R.Top;
  end;
end;

function GdkRectFromFresnelRect(const R: TFresnelRect): TGdkRectangle;
begin
  with Result do
  begin
    x := round(R.Left);
    y := round(R.Top);
    width := round(R.Right-R.Left);
    height := round(R.Bottom-R.Top);
  end;
end;

function GdkKeyToFresnelKey(AValue: cuint32): Integer;
begin
  if (AValue <= $FF) then
    Exit(AValue);
  Result := 0;
  case AValue of
    GDK_KEY_Return,
    GDK_KEY_KP_Enter,
    GDK_KEY_ISO_Enter,
    GDK_KEY_3270_Enter: Result := TKeyCodes.Enter;
    GDK_KEY_Escape: Result := TKeyCodes.Escape;
    GDK_KEY_Insert: Result := TKeyCodes.Insert;
    GDK_KEY_Delete: Result := TKeyCodes.Delete;
    GDK_KEY_BackSpace: Result := TKeyCodes.BackSpace;
    GDK_KEY_Home: Result := TKeyCodes.Home;
    GDK_KEY_End: Result := TKeyCodes.End_;
    GDK_KEY_KP_Page_Up,
    GDK_KEY_Page_Up: Result := TKeyCodes.PageUp;
    GDK_KEY_KP_Page_Down,
    GDK_KEY_Page_Down: Result := TKeyCodes.PageDown;
    GDK_KEY_Left,
    GDK_KEY_KP_LEFT: Result := TKeyCodes.ArrowLeft;
    GDK_KEY_Up,
    GDK_KEY_KP_UP: Result := TKeyCodes.ArrowUp;
    GDK_KEY_Right,
    GDK_KEY_KP_Right: Result := TKeyCodes.ArrowRight;
    GDK_KEY_Down,
    GDK_KEY_KP_Down: Result := TKeyCodes.ArrowDown;
    GDK_KEY_Menu: Result := TKeyCodes.ContextMenu;
    GDK_KEY_Tab,
    GDK_KEY_3270_BackTab,
    GDK_KEY_ISO_Left_Tab: Result := TKeyCodes.Tab;
    GDK_KEY_Shift_L,
    GDK_KEY_Shift_R: Result := TKeyCodes.Shift;
    GDK_KEY_Control_L,
    GDK_KEY_Control_R: Result := TKeyCodes.Control;
    GDK_KEY_F1 .. GDK_KEY_F24:
      Result:= TKeyCodes.F1 + (AValue - GDK_KEY_F1);
    GDK_KEY_Copy : Result := TKeyCodes.Copy;
    GDK_KEY_Caps_Lock : Result := TKeyCodes.CapsLock;
    GDK_KEY_Hyper_L,
    GDK_KEY_Hyper_R : Result:=TKeyCodes.Hyper;
    GDK_KEY_Meta_L,
    GDK_KEY_Meta_R : Result:=TKeyCodes.Meta;
    GDK_KEY_Num_Lock : Result:=TKeyCodes.NumLock;
    GDK_KEY_Scroll_Lock : Result:=TKeyCodes.ScrollLock;
    GDK_KEY_Super_L,
    GDK_KEY_Super_R : Result:=TKeyCodes.Super;
    GDK_KEY_KP_Space : Result:=Ord(' ');
    GDK_KEY_Clear : Result:=TKeyCodes.Clear;
    GDK_KEY_3270_CursorSelect : Result:=TKeyCodes.CrSel;
    GDK_KEY_3270_EraseEOF : Result:=TKeyCodes.EraseEof;
    GDK_KEY_3270_ExSelect : Result:=TKeyCodes.ExSel;
    GDK_KEY_Paste : Result:=TKeyCodes.Paste;
    GDK_KEY_Redo : Result:=TKeyCodes.Redo;
    GDK_KEY_Undo : Result:=TKeyCodes.Undo;
    GDK_KEY_3270_Attn : Result:=TKeyCodes.Attn;
    GDK_KEY_Cancel : Result:=TKeyCodes.Cancel;
    GDK_KEY_Execute : Result:=TKeyCodes.Execute;
    GDK_KEY_Find : Result:=TKeyCodes.Find;
    GDK_KEY_Help : Result:=TKeyCodes.Help;
    GDK_KEY_Pause,
    GDK_KEY_Break : Result:=TKeyCodes.Pause;
    GDK_KEY_3270_Play : Result:=TKeyCodes.Play;
    GDK_KEY_Select : Result:=TKeyCodes.Select;
    GDK_KEY_ZoomIn : Result:=TKeyCodes.ZoomIn;
    GDK_KEY_ZoomOut : Result:=TKeyCodes.ZoomOut;
    GDK_KEY_MonBrightnessDown : Result:=TKeyCodes.BrightnessDown;
    GDK_KEY_MonBrightnessUp : Result:=TKeyCodes.BrightnessUp;
    GDK_KEY_Eject : Result:=TKeyCodes.Eject;
    GDK_KEY_PowerDown,
    GDK_KEY_PowerOff : Result:=TKeyCodes.PowerOff;
    GDK_KEY_3270_PrintScreen,
    GDK_KEY_Sys_Req : Result:=TKeyCodes.PrintScreen;
    GDK_KEY_Hibernate : Result:=TKeyCodes.Hibernate;
    GDK_KEY_Standby,
    GDK_KEY_Suspend,
    GDK_KEY_Sleep : Result:=TKeyCodes.Standby;
    GDK_KEY_WakeUp : Result:=TKeyCodes.WakeUp;
    GDK_KEY_Eisu_toggle,
    GDK_KEY_Eisu_Shift : Result:=TKeyCodes.Alphanumeric;
    GDK_KEY_Codeinput : Result:=TKeyCodes.CodeInput;
    GDK_KEY_Multi_key  : Result:=TKeyCodes.Compose;
    GDK_KEY_Henkan : Result:=TKeyCodes.Convert;
    GDK_KEY_ISO_First_Group : Result:=TKeyCodes.GroupFirst;
    GDK_KEY_ISO_Last_Group : Result:=TKeyCodes.GroupLast;
    GDK_KEY_ISO_Next_Group : Result:=TKeyCodes.GroupNext;
    GDK_KEY_script_switch : Result:=TKeyCodes.ModeChange;
    {GDK_KEY_Mode_switch : Result:=TKeyCodes.ModeChange; } // duplicate
    GDK_KEY_Muhenkan : Result:=TKeyCodes.NonConvert;
    GDK_KEY_PreviousCandidate : Result:=TKeyCodes.PreviousCandidate;
    GDK_KEY_SingleCandidate : Result:=TKeyCodes.SingleCandidate;
    GDK_KEY_Hangul : Result:=TKeyCodes.HangulMode;
    GDK_KEY_Hangul_Hanja : Result:=TKeyCodes.HanjaMode;
    GDK_KEY_Hangul_Jeonja : Result:=TKeyCodes.HanjaMode;
    {GDK_KEY_Eisu_toggle: Result:=TKeyCodes.Eisu;}
    GDK_KEY_hankaku: Result:=TKeyCodes.Hankaku;
    GDK_KEY_hiragana: Result:=TKeyCodes.Hiranga;
    GDK_KEY_Hiragana_Katakana: Result:=TKeyCodes.HirangaKatakana;
    GDK_KEY_Kana_Lock,
    GDK_KEY_Kana_Shift : Result:=TKeyCodes.KanaMode;
    GDK_KEY_Kanji  : Result:=TKeyCodes.KanjiMode;
    GDK_KEY_Katakana : Result:=TKeyCodes.Katakana;
    GDK_KEY_Romaji : Result:=TKeyCodes.Romaji;
    GDK_KEY_Zenkaku  : Result:=TKeyCodes.Zenkaku;
    GDK_KEY_Zenkaku_Hankaku : Result:=TKeyCodes.ZenkakuHankaku;
    GDK_KEY_AudioForward : Result:=TKeyCodes.MediaFastForward;
    GDK_KEY_AudioPause : Result:=TKeyCodes.MediaPause;
    GDK_KEY_AudioPlay : Result:=TKeyCodes.MediaPlay;
    GDK_KEY_AudioRecord : Result:=TKeyCodes.MediaRecord;
    GDK_KEY_AudioRewind : Result:=TKeyCodes.MediaRewind;
    GDK_KEY_AudioNext : Result:=TKeyCodes.MediaTrackNext;
    GDK_KEY_AudioPrev : Result:=TKeyCodes.MediaTrackPrevious;
    GDK_KEY_AudioLowerVolume : Result:=TKeyCodes.AudioVolumeDown;
    GDK_KEY_AudioRaiseVolume : Result:=TKeyCodes.AudioVolumeUp;
    GDK_KEY_AudioMute : Result:=TKeyCodes.AudioVolumeMute;
    GDK_KEY_AudioMicMute : Result:=TKeyCodes.MicrophoneVolumeMute;
    GDK_KEY_BrightnessAdjust : Result:=TKeyCodes.Dimmer;
    GDK_KEY_AudioCycleTrack : Result:=TKeyCodes.MediaAudioTrack;
    GDK_KEY_AudioRandomPlay : Result:=TKeyCodes.RandomToggle;
    GDK_KEY_SplitScreen : Result:=TKeyCodes.SplitScreenToggle;
    GDK_KEY_Subtitle : Result:=TKeyCodes.Subtitle;
    GDK_KEY_Next_VMode :  Result:=TKeyCodes.VideoModeNext;
    GDK_KEY_Close : Result:=TKeyCodes.Close;
    GDK_KEY_New : Result:=TKeyCodes.New;
    GDK_KEY_Open : Result:=TKeyCodes.Open;
    GDK_KEY_Print : Result:=TKeyCodes.Print;
    GDK_KEY_Save : Result:=TKeyCodes.Save;
    GDK_KEY_Spell : Result:=TKeyCodes.SpellCheck;
    GDK_KEY_MailForward : Result:=TKeyCodes.MailForward;
    GDK_KEY_Reply : Result:=TKeyCodes.MailReply;
    GDK_KEY_Send : Result:=TKeyCodes.MailSend;
    GDK_KEY_Calculator : Result:=TKeyCodes.LaunchCalculator;
    GDK_KEY_Calendar : Result:=TKeyCodes.LaunchCalendar;
    GDK_KEY_Mail :  Result:=TKeyCodes.LaunchMail;
    GDK_KEY_CD,
    GDK_KEY_Video,
    GDK_KEY_AudioMedia : Result:=TKeyCodes.LaunchMediaPlayer;
    GDK_KEY_Music : Result:=TKeyCodes.LaunchMusicPlayer;
    GDK_KEY_Explorer : Result:=TKeyCodes.LaunchMyComputer;
    GDK_KEY_ScreenSaver : Result:=TKeyCodes.LaunchScreenSaver;
    GDK_KEY_Phone : Result:=TKeyCodes.LaunchPhone;
    GDK_KEY_Excel : Result:=TKeyCodes.LaunchSpreadsheet;
    GDK_KEY_WWW : Result:=TKeyCodes.LaunchWebBrowser;
    GDK_KEY_WebCam : Result:=TKeyCodes.LaunchWebCam;
    GDK_KEY_Word : Result:=TKeyCodes.LaunchWordProcessor;
    GDK_KEY_Launch0..GDK_KEY_LaunchF : Result:=TKeyCodes.LaunchApplication1+(AValue-GDK_KEY_Launch0);
  else
    Result := gdk_keyval_to_unicode(AValue);
  end;
end;

function GdkModifierStateToShiftState(const AState: TGdkModifierType
  ): TShiftState;
begin
  Result := [];
  if GDK_BUTTON1_MASK in AState  then
    Include(Result,ssLeft);

  if GDK_BUTTON2_MASK in AState  then
    Include(Result,ssMiddle);

  if GDK_BUTTON3_MASK in AState  then
    Include(Result,ssRight);

  if GDK_BUTTON4_MASK in AState  then
    Include(Result,ssExtra1);

  if GDK_BUTTON5_MASK in AState  then
    Include(Result,ssExtra2);

  if GDK_SHIFT_MASK in AState  then
    Include(Result,ssShift);

  if GDK_CONTROL_MASK in AState  then
    Include(Result,ssCtrl);
end;

Function GDKKeyEventToKeyEventInit(Event: PGdkEvent) : TFresnelKeyEventInit;

begin
  Result:=Default(TFresnelKeyEventInit);
  Result.ShiftState:=GdkModifierStateToShiftState(Event^.key.state);
  // This will handle unicode.
  Result.NumKey:=GdkKeyToFresnelKey(Event^.Key.keyval);
  if Result.NumKey>0 then
    Result.Key:=UnicodeToUTF8(Result.NumKey)
  else
    Result.Key:=TKeyCodes.Names[Result.NumKey];
end;


{ TGtk3WSForm }

procedure TGtk3WSForm.SetFormBounds(const AValue: TFresnelRect);
var
  aRect: TGdkRectangle;
begin
  if Window=nil then
    raise Exception.Create('TGtk3WSForm.SetBoundsRect Window=nil');

  aRect:=GdkRectFromFresnelRect(AValue);
  Window^.set_allocation(@ARect);
end;

procedure TGtk3WSForm.SetCaption(AValue: TFresnelCaption);
begin
  raise Exception.Create('TGtk3WSForm.SetCaption ToDo');
end;

constructor TGtk3WSForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SetRenderer(TGtk3Renderer.Create(Self));
end;

destructor TGtk3WSForm.Destroy;
begin
  SetRenderer(Nil);
  inherited Destroy;
end;

procedure TGtk3WSForm.SetForm(const AValue: TFresnelCustomForm);
begin
  if FForm=AValue then Exit;
  FForm:=AValue;
  if FForm<>nil then
    FreeNotification(FForm);
end;

procedure TGtk3WSForm.CreateIMContext;
begin
  FIMContext:=gtk_im_multicontext_new();
  gtk_im_context_set_client_window(FIMContext, gtk_widget_get_window(FWindow));
  g_signal_connect_data(FIMContext, 'commit', TGCallback(@Gtk3IMContextCommit), Self, Nil, G_CONNECT_DEFAULT);
end;

function TGtk3WSForm.GetVisible: boolean;
begin
  Result:=(FWindow^.window<>nil)
       and gdk_window_is_visible(FWindow^.window);
end;

procedure TGtk3WSForm.GtkEventDestroy;
begin
  FWindow:=Nil;
  g_object_unref(FIMContext);
  FreeAndNil(FForm);
end;

function TGtk3WSForm.GtkEventDraw(AContext: Pcairo_t): Boolean;
var
  ARect: TGdkRectangle;
  W, H: integer;
  {$IFDEF FresnelSkia}
  SkiaRenderer: TFresnelSkiaRenderer;
  SkSurface: ISkSurface;
  SkCanvas: ISkCanvas;
  CairoSurface: Pcairo_surface_t;
  Pixels: Pointer;
  {$ENDIF}
begin
  Result:=true;
  if FForm=nil then exit;

  gdk_cairo_get_clip_rectangle(AContext, @ARect);
  //DebugLn('TGtk3WSForm.GtkEventDraw ',dbgsName(Self),' clip=',dbgs(RectFromGdkRect(ARect)),' ',DbgSName(Form.Renderer));

  W:=ARect.width;
  H:=ARect.height;
  if (W<1) or (H<1) then exit;

  {$IFDEF FresnelSkia}
  if Form.Renderer is TFresnelSkiaRenderer then
  begin
    SkiaRenderer:=TFresnelSkiaRenderer(Form.Renderer);
    Pixels:=GetMem(W*H*4);
    try
      SkSurface := TSkSurface.MakeRasterDirect(TSkImageInfo.Create(W, H), Pixels, W*4);
      SkCanvas:=SkSurface.Canvas;
      SkiaRenderer.Canvas:=SkCanvas;

      Form.WSDraw;

      CairoSurface:=cairo_image_surface_create_for_data(Pixels,CAIRO_FORMAT_ARGB32,W,H,W*4);
      cairo_surface_mark_dirty(CairoSurface);
      cairo_set_source_surface(AContext,CairoSurface,0,0);
      cairo_paint(AContext);

    finally
      SkiaRenderer.Canvas:=nil;
      FreeMem(Pixels);
    end;
  end;
  {$ENDIF}
end;

function TGtk3WSForm.GtkIMECommit(aText: string): Boolean;

var
  lInput : TFresnelInputEventInit;

begin
  lInput:=Default(TFresnelInputEventInit);
  lInput.InputType:=fitInsertText;
  lInput.Data:=aText;
  Result:=Form.WSInput(lInput);
end;


function TGtk3WSForm.GtkEventKeyDown(Event: PGdkEvent): Boolean;
var
  lInit : TFresnelKeyEventInit;
  lInput : TFresnelInputEventInit;

begin
  if Event=nil then exit;
  if Assigned(FIMContext) then
    if (gtk_im_context_filter_keypress(FIMContext,PGdkEventKey(Event))) then
      Exit(FALSE);
  lInit:=GDKKeyEventToKeyEventInit(Event);
  if not Form.WSKey(lInit,evtKeyDown) then
    begin
    lInput:=Default(TFresnelInputEventInit);
    if NumKeyToInputType(lInit.NumKey,lInit.ShiftState,lInput.InputType) then
      begin
      if (lInit.NumKey>0) then
        lInput.Data:=UnicodeToUTF8(lInit.NumKey);
      Form.WSInput(lInput);
      end;
    end;
  Result:=True;
end;

function TGtk3WSForm.GtkEventKeyUp(Event: PGdkEvent): Boolean;
var
  lInit : TFresnelKeyEventInit;

begin
  if Event=nil then exit;
  if Assigned(FIMContext) then
    if (gtk_im_context_filter_keypress(FIMContext,PGdkEventKey(Event))) then
      Exit(FALSE);
  lInit:=GDKKeyEventToKeyEventInit(Event);
  Form.WSKey(lInit,evtKeyUp);
  Result:=True;
end;

function TGtk3WSForm.GtkEventMouseXY(Event: PGdkEvent): Boolean;
var
  WSData: TFresnelMouseEventInit;
  EventId: TEventID;
  MButton: guint;

  procedure PressRelease;
  begin
    if MButton = GTK3_LEFT_BUTTON then
    begin
      WSData.Button:=TMouseButton.mbLeft;
      Include(WSData.Shiftstate,ssLeft);
    end
    else
    if MButton = GTK3_RIGHT_BUTTON then
    begin
      WSData.Button:=TMouseButton.mbRight;
      Include(WSData.Shiftstate,ssRight);
    end
    else
    if MButton = GTK3_MIDDLE_BUTTON then
    begin
      WSData.Button:=TMouseButton.mbMiddle;
      Include(WSData.Shiftstate,ssMiddle);
    end;
  end;

begin
  Result:=false;
  WSData:=Default(TFresnelMouseEventInit);
  MButton := Event^.button.button;

  // todo: WSData.ScreenPos
  WSData.PagePos.X:=Event^.button.x;
  WSData.PagePos.Y:=Event^.button.y;

  WSData.Shiftstate:=GdkModifierStateToShiftState(Event^.button.state);

  case Event^.type_ of
  GDK_MOTION_NOTIFY:
    begin
      EventId:=evtMouseMove;
    end;
  GDK_BUTTON_PRESS,GDK_DOUBLE_BUTTON_PRESS,GDK_TRIPLE_BUTTON_PRESS:
    begin
      Result:=true;
      EventId:=evtMouseDown;
      //writeln('TGtk3WSForm.GtkEventMouseXY ',Event^.type_,' MButton=',MButton,' button.window=',hexstr(ptruint(Event^.button.window),16),' send_event=',Event^.button.send_event,' state=',integer(Event^.button.state),' time=',Event^.button.time);
      PressRelease;
      if MButton = GTK3_LEFT_BUTTON then
      begin
        case Event^.type_ of
        GDK_DOUBLE_BUTTON_PRESS: Include(WSData.Shiftstate,ssDouble);
        GDK_TRIPLE_BUTTON_PRESS: Include(WSData.Shiftstate,ssTriple);
        end;
      end;
    end;
  GDK_BUTTON_RELEASE:
    begin
      Result:=true;
      EventId:=evtMouseUp;
      PressRelease;
    end;
  else
    raise Exception.Create('TGtk3WSForm.GtkEventMouseXY '+IntToStr(ord(Event^.type_)));
  end;

  // update WSData.Buttons
  if ssLeft in WSData.Shiftstate then
    Include(WSData.Buttons,mbLeft);
  if ssRight in WSData.Shiftstate then
    Include(WSData.Buttons,mbRight);
  if ssMiddle in WSData.Shiftstate then
    Include(WSData.Buttons,mbMiddle);

  //debugln(['TGtk3WSForm.GtkEventMouseXY EventId=',EventId,' PagePos=',dbgs(WSData.PagePos)]);
  Form.WSMouseXY(WSData,EventId);
end;

function TGtk3WSForm.GtkEventMouseEnter(Event: PGdkEvent): Boolean;
begin
  if Event=nil then ;
  Result:=false;
end;

function TGtk3WSForm.GtkEventMouseLeave(Event: PGdkEvent): Boolean;
begin
  if Event=nil then ;
  Result:=false;
end;

function TGtk3WSForm.GtkEvent_Event(Event: PGdkEvent): Boolean;
begin
  Result:=false; // true if handled
  //debugln(['TGtk3WSForm.GtkEvent_Event ',Event^.type_]);
  case Event^.type_ of
  GDK_MOTION_NOTIFY,
  GDK_BUTTON_PRESS,
  GDK_DOUBLE_BUTTON_PRESS,
  GDK_TRIPLE_BUTTON_PRESS,
  GDK_BUTTON_RELEASE:
    Result:=GtkEventMouseXY(Event);
  GDK_KEY_PRESS:
    Result := GtkEventKeyDown(Event);
  GDK_KEY_RELEASE:
    Result := GtkEventKeyUp(Event);
  GDK_ENTER_NOTIFY:
    Result := GtkEventMouseEnter(Event);
  GDK_LEAVE_NOTIFY:
    Result := GtkEventMouseLeave(Event);
  GDK_FOCUS_CHANGE:
    ; //FLLog(etDebug,'TSkiaGtk3WSForm.GtkEvent_Event GDK_FOCUS_CHANGE %s',[Self.ToString]);
  GDK_CONFIGURE:
    ; //Result := GtkEventResize(Event);
  GDK_VISIBILITY_NOTIFY:
    ; //FLLog(etDebug,'TSkiaGtk3WSForm.GtkEvent_Event GDK_VISIBILITY_NOTIFY %s',[Self.ToString]);
  end;
end;

procedure TGtk3WSForm.GtkEventHide;
begin
  Form.Hide;
end;

procedure TGtk3WSForm.GtkEventMap;
begin

end;

procedure TGtk3WSForm.GtkEventShow;
begin
  Form.Show;
end;

procedure TGtk3WSForm.GtkEventSizeAllocate(aRect: PGdkRectangle);
var
  FreRect: TFresnelRect;
begin
  // aRect is the inner rect (at 0,0 and without frame and title)
  if aRect=nil then ;
  FClientRect:=RectFromGdkRect(aRect^);

  // retrieve the current window
  FreRect:=GetFormBounds;
  //debugln(['TGtk3WSForm.GtkEventSizeAllocate ',DbgSName(Form),' ',dbgs(FreRect),' Allocate=',aRect^.width,',',aRect^.height]);
  Form.WSResize(FreRect,FClientRect.Right,FClientRect.Bottom);
end;

procedure TGtk3WSForm.Invalidate;
begin
  FWindow^.queue_draw;
end;

procedure TGtk3WSForm.InvalidateRect(const aRect: TFresnelRect);
begin
  FWindow^.queue_draw_area(
    floor(aRect.Left),
    floor(aRect.Top),
    ceil(aRect.Width),
    ceil(aRect.Height));
end;

procedure TGtk3WSForm.Notification(AComponent: TComponent; Operation: TOperation
  );
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FForm=AComponent then
      FForm:=nil;
  end;
end;

function TGtk3WSForm.GetFormBounds: TFresnelRect;
var
  ARect: TGdkRectangle;
begin
  gtk_window_get_position(Window,@ARect.x,@ARect.y);
  gtk_window_get_size(Window,@ARect.width,@ARect.height);
  Result:=FresnelRectFromGdkRect(ARect);
end;

function TGtk3WSForm.GetCaption: TFresnelCaption;
begin
  Result:='';
  raise Exception.Create('TGtk3WSForm.GetCaption ToDo');
end;

procedure TGtk3WSForm.SetVisible(const AValue: boolean);
begin
  //FLLog(etDebug,'TGtk3WSForm.SetVisible %s AValue= %b',[Form.ToString,AValue]);
  if FWindow=nil then
    raise Exception.Create('TGtk3WSForm.SetVisible Window=nil');
  if AValue then
    FWindow^.show
  else
    FWindow^.hide;
end;

function TGtk3WSForm.GetClientSize: TFresnelPoint;
begin
  Result.X:=FClientRect.Right;
  Result.Y:=FClientRect.Bottom;
end;

function TGtk3WSForm.CreateGtkWindow: PGtkWindow;
const
  GDK_DEFAULT_EVENTS_MASK = [
    GDK_EXPOSURE_MASK,
    GDK_POINTER_MOTION_MASK,
    GDK_POINTER_MOTION_HINT_MASK,
    GDK_BUTTON_MOTION_MASK,
    GDK_BUTTON1_MOTION_MASK,
    GDK_BUTTON2_MOTION_MASK,
    GDK_BUTTON3_MOTION_MASK,
    GDK_BUTTON_PRESS_MASK,
    GDK_BUTTON_RELEASE_MASK,
    GDK_KEY_PRESS_MASK,
    GDK_KEY_RELEASE_MASK,
    GDK_ENTER_NOTIFY_MASK,
    GDK_LEAVE_NOTIFY_MASK,
    GDK_FOCUS_CHANGE_MASK,
    GDK_STRUCTURE_MASK,
    GDK_PROPERTY_CHANGE_MASK,
    GDK_VISIBILITY_NOTIFY_MASK,
    GDK_PROXIMITY_IN_MASK,
    GDK_PROXIMITY_OUT_MASK,
    GDK_SUBSTRUCTURE_MASK,
    GDK_SCROLL_MASK,
    GDK_TOUCH_MASK
  ];
var
  ARect: TGdkRectangle;
begin
  //FLLog(etDebug,'TGtk3WSForm.CreateGtkWindow Bounds= %s',[Form.FormBounds.ToString]);
  FWindow := TGtkWindow.new(GTK_WINDOW_TOPLEVEL);
  Result:=Window;

  FWindow^.set_events(GDK_DEFAULT_EVENTS_MASK);

  g_signal_connect_data(Window,'destroy', TGCallback(@Gtk3WidgetDestroy), Self, nil, G_CONNECT_DEFAULT);
  g_signal_connect_data(Window,'draw', TGCallback(@Gtk3DrawWidget), Self, nil, G_CONNECT_DEFAULT);
  g_signal_connect_data(Window,'event', TGCallback(@Gtk3WidgetEvent), Self, nil, G_CONNECT_DEFAULT);
  g_signal_connect_data(Window,'realize', TGCallback(@Gtk3WidgetRealize), Self, nil, G_CONNECT_DEFAULT);
  //g_signal_connect_data(Window,'button-press-event', TGCallback(@Gtk3WidgetButtonPressEvent), Self, nil, G_CONNECT_DEFAULT);
  g_signal_connect_data(Window,'hide', TGCallback(@Gtk3WidgetHide), Self, nil, G_CONNECT_DEFAULT);
  g_signal_connect_data(Window,'map', TGCallback(@Gtk3WidgetMap), Self, nil, G_CONNECT_DEFAULT);
  g_signal_connect_data(Window,'show', TGCallback(@Gtk3WidgetShow), Self, nil, G_CONNECT_DEFAULT);
  g_signal_connect_data(Window,'size-allocate',TGCallback(@Gtk3WidgetSizeAllocate), Self, nil, G_CONNECT_DEFAULT);

  aRect:=GdkRectFromFresnelRect(Form.FormBounds);
  //Window^.set_allocation(@ARect);
  gtk_window_move(Window,aRect.x,aRect.y);
  gtk_window_resize(Window,aRect.width,aRect.height);
  FClientRect.Right:=Window^.get_allocated_width;
  FClientRect.Bottom:=Window^.get_allocated_height;
end;

function TGtk3WSForm.GetWindowState: TGdkWindowState;
begin
  if (Window<>nil) and (Window^.window<>nil) then
    Result:=Window^.window^.get_state
  else
    Result:=[];
end;

procedure TGtk3WidgetSet.InitSynchronizeSupport;
begin
  WakeMainThread := @PrepareSynchronize;
  AssignPipe(ThreadSync_PipeIn, ThreadSync_PipeOut);
  ThreadSync_GIOChannel := g_io_channel_unix_new(ThreadSync_PipeIn);
  g_io_add_watch(ThreadSync_GIOChannel, [G_IO_IN], @ThreadSync_IOCallback, Self);
end;

procedure TGtk3WidgetSet.PrepareSynchronize(AObject: TObject);
var
  {%H-}thrash: char;
begin
  // wake up GUI thread by sending a byte through the threadsync pipe
  thrash:='l';
  fpwrite(ThreadSync_PipeOut, thrash, 1);
end;

constructor TGtk3WidgetSet.Create(AOwner: TComponent);
var
  AGtkThread: PGThread;
  AId: String;
begin
  inherited Create(AOwner);

  Gtk3WidgetSet:=Self;
  SetExceptionMask([exInvalidOp, exDenormalized, exZeroDivide, exOverflow, exUnderflow, exPrecision]);
  FWSFormClass:=TGtk3WSForm;

  g_type_init;
  gtk_init(@argc, @argv);

  if not IsLibrary then
  begin
    AGtkThread := g_thread_self();
    AId := 'org.fresnelskiagtk3.thread_' + hexstr({%H-}AGtkThread);
    FGtk3Application := TGtkApplication.new(PgChar(AId), [G_APPLICATION_NON_UNIQUE]);
    FGtk3Application^.register(nil, nil);
  end;

  {$IFDEF UNIX}
  InitSynchronizeSupport;
  {$ELSE}
  //WakeMainThread := @DoWakeMainThread;
  {$ENDIF}

  FFontEngine:=TGtk3FontEngine.Create(nil);
  TFresnelFontEngine.WSEngine:=FFontEngine;
end;

destructor TGtk3WidgetSet.Destroy;
begin
  TFresnelFontEngine.WSEngine:=nil;
  FreeAndNil(FFontEngine);
  Gtk3WidgetSet:=nil;
  inherited Destroy;
end;

procedure TGtk3WidgetSet.AppWaitMessage;
begin
  gtk_main_iteration;
end;

procedure TGtk3WidgetSet.AppProcessMessages;
begin
  while gtk_events_pending do
    gtk_main_iteration_do(False);
end;

procedure TGtk3WidgetSet.AppTerminate;
var
  AList: PGList;
begin
  if Assigned(FGtk3Application) then
  begin
    FGtk3Application^.quit;
    AList := FGtk3Application^.get_windows;
    if Assigned(AList) then
    begin
      {$IFDEF VerboseGTK3}
      FLLog(etDebug,'TSkiaGtk3App.AppTerminate Windows list %d',[g_list_length(AList)]);
      {$ENDIF}
      g_list_free(AList);
    end else
    begin
      {$IFDEF VerboseGTK3}
      FLLog(etDebug,'TSkiaGtk3App.AppTerminate Windows list is null ');
      {$ENDIF}
    end;
    FGtk3Application^.release;
    FGtk3Application^.unref;
    FGtk3Application := nil;
  end;
  if gtk_main_level > 0 then
    gtk_main_quit;
end;

procedure TGtk3WidgetSet.AppSetTitle(const ATitle: string);
begin
  inherited AppSetTitle(ATitle);
end;

procedure TGtk3WidgetSet.CreateWSForm(aFresnelForm: TFresnelComponent);
var
  aForm: TFresnelCustomForm;
  aWSForm: TGtk3WSForm;
begin
  if not (aFresnelForm is TFresnelCustomForm) then
    raise Exception.Create('TGtk3WidgetSet.CreateWSForm '+aFresnelForm.ToString);
  aForm:=TFresnelCustomForm(aFresnelForm);
  aForm.FontEngine:=FontEngineGtk3;

  aWSForm:=TGtk3WSForm.Create(aForm);
  aWSForm.Form:=aForm;
  aForm.WSForm:=aWSForm;
  aWSForm.CreateGtkWindow;
end;

initialization
  TGtk3WidgetSet.Create(nil);
finalization
  Gtk3WidgetSet.Free; // it will nil itself

end.

