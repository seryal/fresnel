unit Fresnel.Win32;

{$mode objfpc}{$H+}
{$IF FPC_FULLVERSION>30300}
{$WARN 6060 off : Case statement does not handle all possible cases}
{$WARN 6058 off : Call to subroutine "$1" marked as inline is not inlined}
{$ENDIF}

interface

uses
  Classes, SysUtils,
  Windows, JwaWinGDI, System.UITypes,
  {$IFDEF FresnelSkia}
  // skia
  System.Skia, Fresnel.SkiaRenderer,
  {$ENDIF}
  // fresnel
  UTF8Utils,
  Fresnel.Classes, Fresnel.Forms, Fresnel.WidgetSet, Fresnel.DOM,
  Fresnel.Events, FCL.Events;

type
  {$IFDEF FresnelSkia}
  TWin32FontEngine = TFresnelSkiaFontEngine;
  TWin32Renderer = TFresnelSkiaRenderer;
  {$ENDIF}

  { TWin32WSForm }

  TWin32WSForm = class(TFresnelWSForm)
  private
    FForm: TFresnelCustomForm;
    FWindow: HWND;
    procedure SetForm(AValue: TFresnelCustomForm);
  protected
    FDrawBuffer: HBITMAP;
    FDrawBufferData: Pointer;
    FDrawBufferStride: integer;
    FDrawBufferSize: TSize;
    function GetCaption: TFresnelCaption; override;
    function GetFormBounds: TFresnelRect; override;
    function GetVisible: boolean; override;
    procedure DeleteDrawBuffer; virtual;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure RegisterWindowClass(const aClassName: PChar); virtual;
    procedure SetCaption(AValue: TFresnelCaption); override;
    procedure SetFormBounds(const AValue: TFresnelRect); override;
    procedure SetVisible(const AValue: boolean); override;
  public
    function GetClientSize: TFresnelPoint; override;
    procedure Invalidate; override;
    procedure InvalidateRect(const aRect: TFresnelRect); override;
    procedure HandleMouseMsg(XPos,YPos: longint; MouseEventId: TEventID;
      Button: TMouseButton; Shiftstate: TShiftState); virtual;
    function HandlePaintMsg: LRESULT; virtual;
    procedure HandleSizeMsg; virtual;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;

    function CreateWSWindow: HWND; virtual;
    procedure DestroyWSWindow; virtual;
    property Window: HWND read FWindow;
    property Form: TFresnelCustomForm read FForm write SetForm;
  end;

  { TWin32WidgetSet }

  TWin32WidgetSet = class(TFresnelWidgetSet)
  private
    FFontEngine: TWin32FontEngine;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure AppWaitMessage; override;
    procedure AppProcessMessages; override;
    procedure AppTerminate; override;
    procedure AppSetTitle(const ATitle: string); override;

    procedure CreateWSForm(aFresnelForm: TFresnelComponent); override;
    property FontEngineWin32: TWin32FontEngine read FFontEngine;
  end;

type
  TWin32WindowInfo = record
    Form: TWin32WSForm;
  end;
  PWin32WindowInfo = ^TWin32WindowInfo;

var
  Win32WidgetSet: TWin32WidgetSet;
  WindowInfoAtom: ATOM;
  WindowInfoAtomStr: LPCSTR;

function KeyboardStateToShiftState: TShiftState;
procedure CreateDrawBuffer(const aMemDC: HDC; const aWidth, aHeight: Integer;
  out aBuffer: HBITMAP; out aData: Pointer; out aStride: Integer);

implementation

function KeyboardStateToShiftState: TShiftState;
begin
  Result := [];
  if GetKeyState(VK_SHIFT) < 0 then Include(Result, ssShift);
  if GetKeyState(VK_CONTROL) < 0 then Include(Result, ssCtrl);
  if GetKeyState(VK_MENU) < 0 then Include(Result, ssAlt);
  if (GetKeyState(VK_LWIN) < 0) or
     (GetKeyState(VK_RWIN) < 0) then Include(Result, ssMeta);
end;

procedure CreateDrawBuffer(const aMemDC: HDC; const aWidth, aHeight: Integer;
  out aBuffer: HBITMAP; out aData: Pointer; out aStride: Integer);
const
  {%H-}ColorMasks: array[0..2] of DWord = ($ff0000, $00ff00, $0000ff);
var
  aBitmapInfo: PBITMAPINFO;
begin
  aStride:=aWidth*4;
  aBuffer:=0;
  aData:=nil;
  aBitmapInfo:=AllocMem(SizeOf(TBITMAPINFOHEADER)+SizeOf(ColorMasks));
  try
    with aBitmapInfo^.bmiHeader do
    begin
      biSize:=SizeOf(TBITMAPINFOHEADER);
      biWidth:=aWidth;
      biHeight:=-aHeight;
      biPlanes:=1;
      biBitCount:=32;
      biCompression:=BI_BITFIELDS;
      biSizeImage:=aStride*aHeight;
    end;
    System.Move(ColorMasks[0],aBitmapInfo^.bmiColors[0],SizeOf(ColorMasks));
    aBuffer:=CreateDIBSection(aMemDC,aBitmapInfo,DIB_RGB_COLORS, @aData,0,0);
    if aBuffer<>0 then
      GdiFlush;
  finally
    Freemem(aBitmapInfo);
  end;
end;

function AllocWindowInfo(Window: HWND): PWin32WindowInfo;
var
  WindowInfo: PWin32WindowInfo;
begin
  New(WindowInfo);
  WindowInfo^ := Default(TWin32WindowInfo);
  Windows.SetProp(Window, WindowInfoAtomStr, {%H-}PtrUInt(WindowInfo));
  Result := WindowInfo;
end;

function DisposeWindowInfo(Window: HWND): boolean;
var
  WindowInfo: PWin32WindowInfo;
begin
  WindowInfo := {%H-}PWin32WindowInfo(Windows.GetProp(Window, WindowInfoAtomStr));
  Result := Windows.RemoveProp(Window, WindowInfoAtomStr) <> 0;
  if Result then
  begin
    Dispose(WindowInfo);
  end;
end;

function GetWin32WindowInfo(Window: HWND): PWin32WindowInfo;
begin
  Result := {%H-}PWin32WindowInfo(Windows.GetProp(Window, WindowInfoAtomStr));
end;

function WindowProc(aHandle: HWND; aMsg: UINT; aWParam: WPARAM; aLParam: LPARAM): LRESULT; stdcall;
var
  aForm: TWin32WSForm;

  function DoMouseMsg(MouseEvt: TEventID; Button: TMouseButton): LRESULT;
  var
    XPos, YPos: LongInt;
    ShiftState: TShiftState;
  begin
    XPos := GET_X_LPARAM(aLParam);
    YPos := GET_Y_LPARAM(aLParam);
    ShiftState:=KeyboardStateToShiftState;
    if MK_LBUTTON and aWParam>0 then
      Include(ShiftState,ssLeft);
    if MK_RBUTTON and aWParam>0 then
      Include(ShiftState,ssRight);
    if MK_MBUTTON and aWParam>0 then
      Include(ShiftState,ssMiddle);
    aForm.HandleMouseMsg(XPos,YPos,MouseEvt,Button,ShiftState);
    Result:=0;
  end;

var
  aWndInfo: PWin32WindowInfo;
begin
  aWndInfo:=GetWin32WindowInfo(aHandle);
  if aWndInfo<>nil then
  begin
    aForm:=aWndInfo^.Form;

    case aMsg of
    WM_PAINT:
      begin
        Result:=aForm.HandlePaintMsg;
        exit;
      end;
    WM_SIZE:
      begin
        InvalidateRect(aHandle,nil,false);
        aForm.HandleSizeMsg;
      end;
    WM_KEYDOWN:
      begin
        writeln('KeyDown: ',aWParam);
        if aWParam=VK_ESCAPE then
          PostQuitMessage(0);
        exit(0);
      end;
    WM_KEYUP:
      begin
        writeln('KeyUp: ',aWParam);
        exit(0);
      end;

    //WM_LBUTTONDBLCLK: exit(DoMouseMsg(1, True, True));
    WM_LBUTTONDOWN:   exit(DoMouseMsg(evtMouseDown,mbLeft));
    WM_LBUTTONUP:     exit(DoMouseMsg(evtMouseUp,mbLeft));
    //WM_RBUTTONDBLCLK: exit(DoMouseMsg(2, True, True));
    WM_RBUTTONDOWN:   exit(DoMouseMsg(evtMouseDown,mbRight));
    WM_RBUTTONUP:     exit(DoMouseMsg(evtMouseUp,mbRight));
    //WM_MBUTTONDBLCLK: exit(DoMouseMsg(3, True, True));
    WM_MBUTTONDOWN:   exit(DoMouseMsg(evtMouseDown,mbMiddle));
    WM_MBUTTONUP:     exit(DoMouseMsg(evtMouseUp,mbMiddle));
    //WM_XBUTTONDBLCLK: exit(DoMouseMsg(4, True, True));
    WM_XBUTTONDOWN:   exit(DoMouseMsg(evtMouseDown,mbFourth));
    WM_XBUTTONUP:     exit(DoMouseMsg(evtMouseUp,mbFourth));

    WM_MOUSEMOVE:  exit(DoMouseMsg(evtMouseMove,mbLeft));

    WM_CLOSE:
      begin
        PostQuitMessage(0);
        exit(0);
      end;
    end;
  end;

  Result:=DefWindowProc(aHandle,aMsg,aWParam,aLParam);
end;


{ TWin32WSForm }

procedure TWin32WSForm.SetForm(AValue: TFresnelCustomForm);
begin
  if FForm=AValue then Exit;
  FForm:=AValue;
  if FForm<>nil then
    FreeNotification(FForm);
end;

procedure TWin32WSForm.Notification(AComponent: TComponent; Operation: TOperation
  );
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FForm=AComponent then
      FForm:=nil;
  end;
end;

function TWin32WSForm.GetFormBounds: TFresnelRect;
var
  r: TRect;
begin
  r:=Default(TRect);
  if not GetWindowRect(FWindow,r) then
    if Form<>nil then
      r:=Form.FormBounds.GetRect;
  Result.SetRect(r);
end;

function TWin32WSForm.GetCaption: TFresnelCaption;
begin
  Result:='';
  // todo: TWin32WSForm.GetCaption
  raise Exception.Create('TWin32WSForm.GetCaption ToDo');
end;

function TWin32WSForm.GetVisible: boolean;
begin
  // todo: TWin32WSForm.GetVisible
  Result:=true;
end;

procedure TWin32WSForm.DeleteDrawBuffer;
begin
  if FDrawBuffer=0 then exit;
  DeleteObject(FDrawBuffer);
  FDrawBuffer:=0;
  FDrawBufferData:=nil;
end;

function TWin32WSForm.HandlePaintMsg: LRESULT;
const
  BlendFunction: TBlendFunction = (
    BlendOp: AC_SRC_OVER;
    BlendFlags: 0;
    SourceConstantAlpha: 255;
    AlphaFormat: AC_SRC_ALPHA
    );
var
  ps: TPAINTSTRUCT;
  dc, BufDC: HDC;
  r: TRect;
  OldBmp: HGDIOBJ;
  SkSurface: ISkSurface;
  SkCanvas: ISkCanvas;
  W, H: LongInt;
  SkiaRenderer: TFresnelSkiaRenderer;
begin
  r:=Default(TRect);
  GetClientRect(FWindow,r);
  W:=r.Width;
  H:=r.Height;
  if (W<=0) or (H<=0) then exit;

  ps:=Default(TPAINTSTRUCT);
  dc:=BeginPaint(FWindow,ps);
  try
    BufDC:=CreateCompatibleDC(0);
    if BufDC=0 then exit;
    OldBmp:=0;
    try
      if (FDrawBuffer>0)
          and ((FDrawBufferSize.Width<>W) or (FDrawBufferSize.Height<>H)) then
        DeleteDrawBuffer;
      if FDrawBuffer=0 then
        CreateDrawBuffer(BufDC,W,H,
                         FDrawBuffer,FDrawBufferData,FDrawBufferStride);
      if FDrawBuffer=0 then
        exit;
      OldBmp:=SelectObject(BufDC,FDrawBuffer);
      {$IFDEF FresnelSkia}
      if Form.Renderer is TFresnelSkiaRenderer then
      begin
        SkiaRenderer:=TFresnelSkiaRenderer(Form.Renderer);
        try
          SkSurface:=TSkSurface.MakeRasterDirect(TSkImageInfo.Create(W,H),
            FDrawBufferData,FDrawBufferStride);
          SkCanvas:=SkSurface.Canvas;
          SkiaRenderer.Canvas:=SkCanvas;

          Form.WSDraw;

          AlphaBlend(dc,0,0,r.Width,r.Height,BufDC,0,0,W,H,BlendFunction);

        finally
          SkiaRenderer.Canvas:=nil;
        end;
      end;
      {$ENDIF}
    finally
      if OldBmp<>0 then
        SelectObject(BufDC,OldBmp);
      DeleteDC(BufDC);
    end;
  finally
    EndPaint(FWindow,ps);
  end;
  Result:=0;
end;

procedure TWin32WSForm.RegisterWindowClass(const aClassName: PChar);
var
  wc: TWNDCLASS;
begin
  wc:=Default(TWNDCLASS);
  wc.lpfnWndProc:=@WindowProc;
  wc.hInstance:=HINSTANCE;
  wc.hbrBackground:=GetStockObject(WHITE_BRUSH);
  wc.lpszClassName:=aClassName;
  wc.hCursor:=LoadCursor(0,IDC_ARROW);
  RegisterClass(wc);
end;

procedure TWin32WSForm.SetFormBounds(const AValue: TFresnelRect);
var
  r: TRect;
begin
  r:=AValue.GetRect;
  if not SetWindowPos(FWindow,0,r.Left,r.Top,r.Width,r.Height,0) then
  begin
    // todo: handle TWin32WSForm.SetFormBounds failed
  end;
end;

procedure TWin32WSForm.SetCaption(AValue: TFresnelCaption);
begin
  // todo: TWin32WSForm.SetCaption
  if AValue='' then ;
end;

procedure TWin32WSForm.SetVisible(const AValue: boolean);
var
  CmdShow: longint;
begin
  if AValue then
    CmdShow:=SW_SHOW
  else
    CmdShow:=SW_HIDE;
  ShowWindow(FWindow,CmdShow);
end;

function TWin32WSForm.GetClientSize: TFresnelPoint;
var
  r: TRect;
begin
  r:=Default(TRect);
  GetClientRect(FWindow,r);
  Result.X:=r.Width;
  Result.Y:=r.Height;
end;

procedure TWin32WSForm.Invalidate;
begin
  Windows.InvalidateRect(FWindow,nil,false);
end;

procedure TWin32WSForm.InvalidateRect(const aRect: TFresnelRect);
begin
  Windows.InvalidateRect(FWindow,aRect.GetRect,false);
end;

procedure TWin32WSForm.HandleMouseMsg(XPos, YPos: longint;
  MouseEventId: TEventID; Button: TMouseButton; Shiftstate: TShiftState);
var
  WSData: TFresnelMouseEventInit;
  p: TPoint;
begin
  WSData:=Default(TFresnelMouseEventInit);
  WSData.Button:=Button;
  WSData.PagePos.X:=XPos;
  WSData.PagePos.Y:=YPos;
  // Note: WSData.ControlPos is set by Form.WSMouseXY
  Windows.GetCursorPos(p);
  WSData.ScreenPos.SetLocation(p);
  WSData.Shiftstate:=ShiftState;
  if ssLeft in Shiftstate then
    Include(WSData.Buttons,mbLeft);
  if ssRight in Shiftstate then
    Include(WSData.Buttons,mbRight);
  if ssMiddle in Shiftstate then
    Include(WSData.Buttons,mbMiddle);

  //writeln('TWin32WSForm.HandleMouseMsg ',XPos,',',YPos,' ',MouseEventId,' ',Button);
  Form.WSMouseXY(WSData,MouseEventId);
end;

procedure TWin32WSForm.HandleSizeMsg;
var
  aFormRect, aClientRect: TRect;
  FreRect: TFresnelRect;
begin
  aFormRect:=Default(TRect);
  if not GetWindowRect(FWindow,aFormRect) then
    exit;
  if (aFormRect.Width<=0) or (aFormRect.Height<=0) then exit;

  aClientRect:=Default(TRect);
  if not GetClientRect(FWindow,aClientRect) then
    exit;
  if (aClientRect.Right<=0) or (aClientRect.Bottom<=0) then exit;

  FreRect.SetRect(aFormRect);
  Form.WSResize(FreRect,aClientRect.Right,aClientRect.Bottom);
end;

constructor TWin32WSForm.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  SetRenderer(TWin32Renderer.Create(Self));
end;

destructor TWin32WSForm.Destroy;
begin
  SetRenderer(Nil);
  DestroyWSWindow;

  inherited Destroy;
end;

function TWin32WSForm.CreateWSWindow: HWND;
const
  // todo: create unique classname
  aClassName = 'FresnelWindow';
var
  aRect: TRect;
  Info: PWin32WindowInfo;
begin
  aRect:=Form.FormBounds.GetRect;

  RegisterWindowClass(aClassName);
  FWindow:= CreateWindowEx(0,aClassName,'FormTitle',
    WS_OVERLAPPEDWINDOW,
    aRect.Left,aRect.Top,aRect.Width,aRect.Height, 0,0, HINSTANCE, nil);
  Result:=FWindow;
  Info:=AllocWindowInfo(FWindow);
  Info^.Form:=Self;

  ShowWindow(FWindow,SW_SHOW);
end;

procedure TWin32WSForm.DestroyWSWindow;
begin
  if FWindow=0 then exit;
  DisposeWindowInfo(FWindow);
  //  todo: TWin32WSForm.DestroyWSWindow
end;

{ TWin32WidgetSet }

constructor TWin32WidgetSet.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  Win32WidgetSet:=Self;
  FWSFormClass:=TWin32WSForm;

  FFontEngine:=TWin32FontEngine.Create(nil);
  TFresnelFontEngine.WSEngine:=FFontEngine;
end;

destructor TWin32WidgetSet.Destroy;
begin
  TFresnelFontEngine.WSEngine:=nil;
  FreeAndNil(FFontEngine);
  Win32WidgetSet:=nil;
  inherited Destroy;
end;

procedure TWin32WidgetSet.AppWaitMessage;
var
  aWaitHandleCount, timeout, retVal: DWORD;
  pHandles: Windows.LPHANDLE;
begin
  //writeln('TWin32WidgetSet.AppWaitMessage START');
  aWaitHandleCount:=0;
  pHandles:=nil;
  timeout := INFINITE;
  retVal := Windows.MsgWaitForMultipleObjects(aWaitHandleCount, pHandles,
    false, timeout, QS_ALLINPUT);
  if retVal>0 then ;
  //writeln('TWin32WidgetSet.AppWaitMessage END ',retVal);
end;

procedure TWin32WidgetSet.AppProcessMessages;
var
  aMessage: TMsg;
begin
  //writeln('TWin32WidgetSet.AppProcessMessages START');
  AMessage := Default(TMsg);
  while PeekMessage(AMessage, 0, 0, 0, PM_REMOVE) do
  begin
    if AMessage.message = WM_QUIT then
    begin
      PostQuitMessage(AMessage.wParam);
      AppTerminate;
      break;
    end;
    TranslateMessage(aMessage);
    DispatchMessage(aMessage);
  end;
  //writeln('TWin32WidgetSet.AppProcessMessages END');
end;

procedure TWin32WidgetSet.AppTerminate;
begin
  Application.Terminate;
end;

procedure TWin32WidgetSet.AppSetTitle(const ATitle: string);
begin
  if ATitle='' then ;
  //if FAppHandle <> 0 then
  //begin
  //  ws:=UTF8ToUTF16(ATitle);
    // todo: TWin32WidgetSet.AppSetTitle
    //Windows.SetWindowTextW(FAppHandle, PWideChar(ws));
  //end;
end;

procedure TWin32WidgetSet.CreateWSForm(aFresnelForm: TFresnelComponent);
var
  aForm: TFresnelCustomForm;
  aWSForm: TWin32WSForm;
begin
  if not (aFresnelForm is TFresnelCustomForm) then
    raise Exception.Create('TWin32WidgetSet.CreateWSForm '+aFresnelForm.ToString);
  aForm:=TFresnelCustomForm(aFresnelForm);
  aForm.FontEngine:=FontEngineWin32;

  aWSForm:=TWin32WSForm.Create(aForm);
  aWSForm.Form:=aForm;
  aForm.WSForm:=aWSForm;
  aWSForm.CreateWSWindow;
end;

initialization
  WindowInfoAtom := Windows.GlobalAddAtom('WindowInfo');
  WindowInfoAtomStr:={%H-}lpcstr(PtrUint(WindowInfoAtom));
  TWin32WidgetSet.Create(nil);

finalization
  Windows.GlobalDeleteAtom(WindowInfoAtom);
  WindowInfoAtom := 0;
  WindowInfoAtomStr:=nil;
  Win32WidgetSet.Free; // it will nil itself

end.

