{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Webassembly application & widgetset classes for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit fresnel.wasm.app;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, CustApp, Fresnel.Forms, Fresnel.Classes, Fresnel.WidgetSet,
  System.UITypes, fresnel.Events, fresnel.wasm.shared, fresnel.wasm.render;

Type
  EWasmFresnel = class(EFresnel);


  { TFresnelWasmForm }

  TFresnelWasmForm = class(TFresnelWSForm)
  private
    FCanvasID: TCanvasID;
    FForm: TFresnelCustomForm;
  protected
    procedure SetForm(aForm : TFresnelCustomForm);
    function GetCaption: TFresnelCaption; override;
    function GetFormBounds: TFresnelRect; override;
    function GetVisible: boolean; override;
    procedure SetCaption(AValue: TFresnelCaption); override;
    procedure SetFormBounds(const AValue: TFresnelRect); override;
    procedure SetVisible(const AValue: boolean); override;
    function GetFresnelRenderer : TWasmFresnelRenderer;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
  Public
    constructor Create(AOwner: TComponent); override;
    function GetClientSize: TFresnelPoint; override;
    Procedure InitForm(aForm : TFresnelCustomForm);
    procedure InvalidateRect(const aRect: TFresnelRect); override;
    property CanvasID : TCanvasID read FCanvasID;
    property form : TFresnelCustomForm Read FForm;
    property Renderer : TWasmFresnelRenderer Read GetFresnelRenderer;
  end;

  { TFresnelWasmWidgetSet }

  TFresnelWasmWidgetSet = class(TFresnelWidgetSet)
  Private
    FForms : TFPList;
    function GetWasmForm(aIndex : Cardinal): TFresnelWasmForm;
    function GetWasmFormCount: Cardinal;
    // Event handling
    procedure HandleFresnelEnterEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelKeyDownEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelKeyUpEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelLeaveEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelMouseClickEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelMouseDoubleClickEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelMouseDownEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelMouseMoveEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelMouseScrollEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelMouseUpEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);
    procedure HandleFresnelEvents(aForm: TFresnelWasmForm; Msg: TCanvasMessageID; Data: PCanvasMessageData);
    class procedure InitMouseXYEvent(out EvtInit: TFresnelMouseEventInit; Data: PCanvasMessageData);
  public
    Constructor Create(AOwner: TComponent); override;
    Destructor Destroy; override;
    procedure AppProcessMessages; override;
    procedure AppTerminate; override;
    procedure AppWaitMessage; override;
    Function FindFormByCanvasId(ID : TCanvasID) : TFresnelWasmForm;
    procedure CreateWSForm(aFresnelForm: TFresnelComponent); override;
    Property WasmForms[aIndex : Cardinal] : TFresnelWasmForm Read GetWasmForm;
    Property WasmFormCount : Cardinal Read GetWasmFormCount;
  end;

  { TFresnelWasmApplication }

  TFresnelWasmApplication = class(TFresnelBaseApplication)
  private
    FLastTick: Int64;
    FPrevTick: Int64;
    procedure CheckMessages;
    procedure HandleProcessMessages(aCurrent, aPrevious: Double);
  protected
    procedure DoTick(aCurrent, aPrevious: Double); virtual;
    Procedure DoLog(EventType : TEventType; const Msg : String);  override;
    procedure CreateWidgetSet; virtual;
    procedure SetTickHook; virtual;
    procedure DoRun; override;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    property LastTick : Int64 Read FLastTick;
    property PrevTick : Int64 Read FPrevTick;
  end;

Procedure InitWasmApplication;
Procedure DoneWasmApplication;

implementation

uses fresnel.Images, fresnel.wasm.font, fresnel.wasm.api;

{ TFresnelWasmForm }

procedure TFresnelWasmForm.SetForm(aForm: TFresnelCustomForm);
begin
  if Assigned(FForm) then
    FForm.RemoveFreeNotification(Self);
  FForm:=aForm;
  if Assigned(FForm) then
    FForm.FreeNotification(Self);
end;

function TFresnelWasmForm.GetCaption: TFresnelCaption;
begin
  Result:='';
end;

function TFresnelWasmForm.GetFormBounds: TFresnelRect;

var
  aWidth,aHeight : Longint;

begin
  Result:=Default(TFresnelRect);
  if __fresnel_canvas_getsizes(CanvasID,@aWidth,@aHeight)=ECANVAS_SUCCESS then
    begin
    Result.Right:=aWidth;
    Result.Bottom:=aHeight;
    end;
end;

function TFresnelWasmForm.GetVisible: boolean;
begin
  FLLog(etWarning,'TFresnelWasmForm.GetVisible not implemented');
  Result:=True;
end;

procedure TFresnelWasmForm.SetCaption(AValue: TFresnelCaption);
begin
  FLLog(etWarning,'TFresnelWasmForm.SetCaption(''%s'') not implemented',[aValue]);
end;

procedure TFresnelWasmForm.SetFormBounds(const AValue: TFresnelRect);
begin
  FLLog(etWarning,'TFresnelWasmForm.SetFormBounds(''%s'') not implemented',[aValue.ToString]);
end;

procedure TFresnelWasmForm.SetVisible(const AValue: boolean);
begin
  FLLog(etWarning,'TFresnelWasmForm.SetVisible(%b) not implemented',[aValue]);
end;

function TFresnelWasmForm.GetFresnelRenderer: TWasmFresnelRenderer;
begin
  Result:=TWasmFresnelRenderer(Inherited Renderer);
end;

procedure TFresnelWasmForm.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if (Operation=opRemove) then
    begin
    if aComponent=FForm then
      FForm:=nil;
    end;
end;

constructor TFresnelWasmForm.Create(AOwner: TComponent);

begin
  Inherited;
  SetRenderer(TWasmFresnelRenderer.Create(Self));
end;

function TFresnelWasmForm.GetClientSize: TFresnelPoint;
begin
  Result:=TFresnelPoint.Create(Form.Width,Form.Height);
end;

procedure TFresnelWasmForm.InitForm(aForm: TFresnelCustomForm);

var
  aWidth,aHeight : Longint;
  aFontEngine: TFresnelWasmFontEngine;

begin
  FLLog(etDebug,'InitForm(%s) ',[aForm.ClassName]);
  SetForm(aForm);
  aForm.WSForm:=Self;
  aWidth:=Round(aForm.Width);
  if aWidth=0 then
    aWidth:=640;
  aHeight:=Round(aForm.Height);
  if aHeight=0 then
    aHeight:=480;
  if __fresnel_canvas_allocate(aWidth,aHeight,@FCanvasID)<>ECANVAS_SUCCESS then
     Raise EWasmFresnel.Create('Failed to allocate canvas');
  Renderer.Canvas:=FCanvasID;
  Form.WSDraw;
  aFontEngine:=TFresnelWasmFontEngine.Create(Self);
  aFontEngine.CanvasID:=Self.CanvasID;
  aForm.FontEngine:=aFontEngine;

end;

procedure TFresnelWasmForm.InvalidateRect(const aRect: TFresnelRect);
begin
  FLLog(etDebug,'InvalidateRect(%s)',[aRect.ToString]);
  Form.WSDraw;
end;

{ TFresnelWasmWidgetSet }

function TFresnelWasmWidgetSet.GetWasmForm(aIndex : Cardinal): TFresnelWasmForm;
begin
  Result:=TFresnelWasmForm(FForms[aIndex]);
end;

function TFresnelWasmWidgetSet.GetWasmFormCount: Cardinal;
begin
  Result:=FForms.Count;
end;

constructor TFresnelWasmWidgetSet.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  Options:=[wsClick,wsDoubleClick];
  FForms:=TFPList.Create;
end;


destructor TFresnelWasmWidgetSet.Destroy;
begin
  FreeAndNil(FForms);
  inherited Destroy;
end;

Function IntToShiftState(aInt : LongInt) : TShiftState;

var
  S : TShiftStateEnum;

begin
  Result:=[];
  For S in TShiftstate do
    If (aInt and (1 shl Ord(S)))<>0 then
      Include(Result,S);
end;

class procedure TFresnelWasmWidgetSet.InitMouseXYEvent(out EvtInit: TFresnelMouseEventInit; Data : PCanvasMessageData);

var
  Shift : TShiftState;

begin
  EvtInit:=Default(TFresnelMouseEventInit);
  Shift:=IntToShiftState(Data^[WASMSG_MOUSESTATE_STATE]);
  evtInit.Button:=TMouseButton(Data^[WASMSG_MOUSESTATE_BUTTON]);
  if ssLeft in Shift then
    Include(EvtInit.Buttons,mbLeft);
  if ssMiddle in Shift then
    Include(EvtInit.Buttons,mbMiddle);
  if ssRight in Shift then
    Include(EvtInit.Buttons,mbRight);
  if ssExtra1 in Shift then
    Include(EvtInit.Buttons,mbExtra1);
  if ssExtra2 in Shift then
    Include(EvtInit.Buttons,mbExtra2);
  EvtInit.ScreenPos.SetLocation(TFresnelPoint.Create(Data^[WASMSG_MOUSESTATE_X],Data^[WASMSG_MOUSESTATE_Y]));
  EvtInit.PagePos.X:=EvtInit.ScreenPos.X;
  EvtInit.PagePos.Y:=EvtInit.ScreenPos.Y;
  EvtInit.Shiftstate:=Shift;
end;



procedure TFresnelWasmWidgetSet.HandleFresnelMouseMoveEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

var
  Init : TFresnelMouseEventInit;

begin
  InitMouseXYEvent(Init,Data);
  aForm.form.WSMouseXY(Init,evtMouseMove);
end;


Procedure TFresnelWasmWidgetSet.HandleFresnelMouseDownEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

var
  Init : TFresnelMouseEventInit;

begin
  InitMouseXYEvent(Init,Data);
  aForm.form.WSMouseXY(Init,evtMouseDown);
end;


procedure TFresnelWasmWidgetSet.HandleFresnelMouseUpEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

var
  Init : TFresnelMouseEventInit;

begin
  InitMouseXYEvent(Init,Data);
  aForm.form.WSMouseXY(Init,evtMouseUp);
end;


procedure TFresnelWasmWidgetSet.HandleFresnelMouseScrollEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

var
  Init : TFresnelMouseEventInit;

begin
  InitMouseXYEvent(Init,Data);
  aForm.form.WSMouseXY(Init,evtMouseWheel);
end;


procedure TFresnelWasmWidgetSet.HandleFresnelMouseClickEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

var
  Init : TFresnelMouseEventInit;

begin
  InitMouseXYEvent(Init,Data);
  aForm.form.WSMouseXY(Init,evtClick);
end;


Procedure TFresnelWasmWidgetSet.HandleFresnelMouseDoubleClickEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

var
  Init : TFresnelMouseEventInit;
begin
  InitMouseXYEvent(Init,Data);
  aForm.form.WSMouseXY(Init,evtDblClick);
end;


procedure TFresnelWasmWidgetSet.HandleFresnelEnterEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

begin
  FLLog(etWarning,'TFresnelWasmWidgetSet.HandleFresnelEnterEvent not implemented');
end;


procedure TFresnelWasmWidgetSet.HandleFresnelLeaveEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

begin
  FLLog(etWarning,'TFresnelWasmWidgetSet.HandleFresnelLeaveEvent not implemented');
end;


Procedure TFresnelWasmWidgetSet.HandleFresnelKeyUpEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

begin
  FLLog(etWarning,'TFresnelWasmWidgetSet.HandleFresnelKeyEvent not implemented');
end;

Procedure TFresnelWasmWidgetSet.HandleFresnelKeyDownEvent(aForm: TFresnelWasmForm; Data: PCanvasMessageData);

begin
  FLLog(etWarning,'TFresnelWasmWidgetSet.HandleFresnelKeyEvent not implemented');
end;

procedure TFresnelWasmWidgetSet.HandleFresnelEvents(aForm: TFresnelWasmForm; Msg: TCanvasMessageID; Data: PCanvasMessageData);

begin
  Case Msg of
    WASMSG_NONE : ;
    WASMSG_MOVE : HandleFresnelMouseMoveEvent(aForm,Data);
    WASMSG_MOUSEDOWN : HandleFresnelMouseDownEvent(aForm,Data);
    WASMSG_MOUSEUP : HandleFresnelMouseUpEvent(aForm,Data);
    WASMSG_MOUSESCROLL : HandleFresnelMouseScrollEvent(aForm,Data);
    WASMSG_CLICK : HandleFresnelMouseClickEvent(aForm,Data);
    WASMSG_DBLCLICK : HandleFresnelMouseDoubleClickEvent(aForm,Data);
    WASMSG_ENTER : HandleFresnelEnterEvent(aForm,Data);
    WASMSG_LEAVE : HandleFresnelLeaveEvent(aForm,Data);
    WASMSG_KEYUP : HandleFresnelKeyUpEvent(aForm,Data);
    WASMSG_KEYDOWN : HandleFresnelKeyDownEvent(aForm,Data);
  else
    FLLog(etWarning,'Unknown message type: %d',[Msg]);
  end;
end;

procedure TFresnelWasmWidgetSet.AppProcessMessages;

var
  Msg : TCanvasMessageID;
  canvasID : TCanvasID;
  MsgData : TCanvasMessageData;
  F : TFresnelWasmForm;
  E : TFresnelEvent;

begin
  While __fresnel_event_get(@CanvasID,@Msg,@MsgData)=EWASMEVENT_SUCCESS do
    begin
    F:=FindFormByCanvasId(CanvasID);
    if not Assigned(F) then
      FLLog(etWarning,'Got message with canvas ID %d, no matching form found',[CanvasID])
    else
      HandleFresnelEvents(F,Msg,@MsgData);
    end;
end;

procedure TFresnelWasmWidgetSet.AppTerminate;
begin
  //
end;

procedure TFresnelWasmWidgetSet.AppWaitMessage;
begin

end;

function TFresnelWasmWidgetSet.FindFormByCanvasId(ID: TCanvasID): TFresnelWasmForm;

var
  I : Integer;

begin
//  FLLog(etDebug,'Finding form with ID %d',[ID]);
  Result:=nil;
  I:=FForms.Count-1;
  While (I>=0) and (Result=Nil) do
    begin
    Result:=TFresnelWasmForm(FForms[i]);
    If Result.CanvasID<>ID then
      Result:=Nil;
    Dec(I);
    end;
end;

procedure TFresnelWasmWidgetSet.CreateWSForm(aFresnelForm: TFresnelComponent);

var
  WF : TFresnelWasmForm;

begin
  if aFresnelForm.InheritsFrom(TFresnelCustomForm) then
    begin
    WF:=TFresnelWasmForm.Create(Self);
    FForms.Add(WF);
    WF.InitForm(TFresnelCustomForm(aFresnelForm));
    end;
end;

{ TFresnelWasmApplication }

procedure TFresnelWasmApplication.CheckMessages;

begin
  WidgetSet.AppProcessMessages;
end;


procedure TFresnelWasmApplication.DoLog(EventType: TEventType; const Msg: String);
begin
  Writeln('Wasm log[',EventType,'] ',Msg);
end;

procedure TFresnelWasmApplication.DoRun;
begin
  // Show main form.
  ShowMainForm;
  // We do nothing any more. The timer tick will be called from now on
  AbortRun;
end;

procedure TFresnelWasmApplication.CreateWidgetSet;
begin
  TFresnelWasmWidgetSet.Create(Nil);
end;

procedure TFresnelWasmApplication.HandleProcessMessages(aCurrent, aPrevious: Double);

begin
  FLastTick:=Round(aCurrent);
  FPrevTick:=Round(aPrevious);
  try
    ProcessMessages;
  except
    On E : Exception do
      begin
      FLLog(etError,'Exception %s during process messages: %s',[E.ClassName,E.Message]);
      ShowException(E);
      end;
  end;
end;

procedure TFresnelWasmApplication.DoTick(aCurrent, aPrevious: Double);

begin
  FLastTick:=Round(aCurrent);
  FPrevTick:=Round(aPrevious);
  try
    ProcessMessages;
  except
    On E : Exception do
      begin
      FLLog(etError,'Exception %s during timer tick: %s',[E.ClassName,E.Message]);
      ShowException(E);
      end;
  end;
end;

procedure TFresnelWasmApplication.SetTickHook;
begin
  OnFresnelWasmTick:=@DoTick;
  OnFresnelProcessMessage:=@HandleProcessMessages;
end;

constructor TFresnelWasmApplication.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  CreateWidgetSet;
  SetTickHook;
end;

destructor TFresnelWasmApplication.Destroy;
begin
  FreeAndNil(WidgetSet);
  inherited Destroy;
end;

procedure InitWasmApplication;

begin
  ImagesConfig.ImageClass:=TWASMImage;
  TFresnelWasmApplication.Create(Nil);
end;

procedure DoneWasmApplication;

begin
  FreeAndNil(Application);
end;

initialization
  InitWasmApplication;

finalization
  DoneWasmApplication;
end.

