{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Webassembly font handling classes for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit fresnel.wasm.font;

{$mode objfpc}{$H+}
{ $DEFINE DEBUGWASMFONT}

interface

uses
  Math, Classes, SysUtils, avl_tree, Fresnel.Classes, Fresnel.DOM, fresnel.wasm.shared, fresnel.wasm.api, Types;

Type
  TFresnelWasmFontEngine = Class;


  { TFresnelWasmFont }

  TFresnelWasmFont = class(TInterfacedObject,IFresnelFont)
  public
    Engine: TFresnelWasmFontEngine;
    Family: string;
    Kerning: TFresnelCSSKerning;
    Size: double;
    Style: string;
    Weight: double;
    Width: double;
    function GetAlternates: string;
    function GetCaps: TFresnelCSSFontVarCaps;
    function GetEastAsians: TFresnelCSSFontVarEastAsians;
    function GetEmoji: TFresnelCSSFontVarEmoji;
    function GetFamily: string;
    function GetKerning: TFresnelCSSKerning;
    function GetLigatures: TFresnelCSSFontVarLigaturesSet;
    function GetNumerics: TFresnelCSSFontVarNumerics;
    function GetPosition: TFresnelCSSFontVarPosition;
    function GetSize: TFresnelLength;
    function GetStyle: string;
    function GetWeight: TFresnelLength;
    function GetWidth: TFresnelLength;
    function TextSize(const aText: string): TFresnelPoint; virtual;
    function TextSizeMaxWidth(const aText: string; MaxWidth: TFresnelLength): TFresnelPoint; virtual;
    function GetTool: TObject;
    function GetDescription : String;
  end;

  { TFresnelWasmFontEngine }

  TFresnelWasmFontEngine = class(TFresnelFontEngine)
  private
    FCanvasID: TCanvasID;
    FFonts: TAvlTree; // tree of TFresnelLCLFont sorted with CompareFresnelWasmFont
    FLastFontName : String; // Last used font.
    function MaybeSetFont(aFont: TFresnelWasmFont): Boolean;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function FindFont(const Desc: TFresnelFontDesc): TFresnelWasmFont; virtual;
    function Allocate(const Desc: TFresnelFontDesc): IFresnelFont; override;
    function TextSize(aFont: TFresnelWasmFont; const aText: string): TPoint; virtual;
    function TextSizeMaxWidth(aFont: TFresnelWasmFont; const aText: string; MaxWidth: integer): TPoint; virtual;
    Function FontToHTML(aFont : TFresnelWasmFont) : String;
    property CanvasID: TCanvasID read FCanvasID write FCanvasID;
  end;

implementation

function CompareFresnelWasmFont(Item1, Item2: Pointer): integer;
var
  Font1: TFresnelWasmFont absolute Item1;
  Font2: TFresnelWasmFont absolute Item2;
begin
  Result:=CompareText(Font1.Family,Font2.Family);
  if Result<>0 then exit;
  Result:=CompareValue(Font1.Size,Font2.Size);
  if Result<>0 then exit;
  Result:=CompareText(Font1.Style,Font2.Style);
  if Result<>0 then exit;
  Result:=CompareValue(Font1.Weight,Font2.Weight);
  if Result<>0 then exit;
  Result:=CompareValue(Font1.Width,Font2.Width);
  if Result<>0 then exit;
  Result:=CompareValue(ord(Font1.Kerning),ord(Font2.Kerning));
end;

function CompareFresnelFontDescWithWasmFont(Key, Item: Pointer): integer;

Const
  Delta = 0.1;

var
  Desc: PFresnelFontDesc absolute Key;
  aFont: TFresnelWasmFont absolute Item;
begin
  Result:=CompareText(Desc^.Family,aFont.Family);
  if Result<>0 then exit;
  Result:=CompareValue(Desc^.Size,aFont.Size);
  if Result<>0 then exit;
  Result:=CompareText(Desc^.Style,aFont.Style);
  if Result<>0 then exit;
  Result:=CompareValue(Desc^.Weight,aFont.Weight);
  if Result<>0 then exit;
  Result:=CompareValue(Desc^.Width,aFont.Width);
  if Result<>0 then exit;
  Result:=CompareValue(ord(Desc^.Kerning),ord(aFont.Kerning));
end;



{ TFresnelWasmFont }

function TFresnelWasmFont.GetFamily: string;
begin
  Result:=Family;
end;

function TFresnelWasmFont.GetAlternates: string;
begin
  Result:='normal';
end;

function TFresnelWasmFont.GetCaps: TFresnelCSSFontVarCaps;
begin
  Result:=ffvcNormal;
end;

function TFresnelWasmFont.GetEastAsians: TFresnelCSSFontVarEastAsians;
begin
  Result:=[ffveaNormal];
end;

function TFresnelWasmFont.GetEmoji: TFresnelCSSFontVarEmoji;
begin
  Result:=ffveNormal;
end;

function TFresnelWasmFont.GetKerning: TFresnelCSSKerning;
begin
  Result:=Kerning;
end;

function TFresnelWasmFont.GetLigatures: TFresnelCSSFontVarLigaturesSet;
begin
  Result:=[ffvlNormal];
end;

function TFresnelWasmFont.GetNumerics: TFresnelCSSFontVarNumerics;
begin
  Result:=[ffvnNormal];
end;

function TFresnelWasmFont.GetPosition: TFresnelCSSFontVarPosition;
begin
  Result:=ffvpNormal;
end;

function TFresnelWasmFont.GetSize: TFresnelLength;
begin
  Result:=Size;
end;

function TFresnelWasmFont.GetStyle: string;
begin
  Result:=Style;
end;

function TFresnelWasmFont.GetWeight: TFresnelLength;
begin
  Result:=Weight;
end;

function TFresnelWasmFont.GetWidth: TFresnelLength;
begin
  Result:=Width;
end;

function TFresnelWasmFont.TextSize(const aText: string): TFresnelPoint;
var
  p: TPoint;
begin
  p:=Engine.TextSize(Self,aText);
  Result.X:=p.X;
  Result.Y:=p.Y;
end;

function TFresnelWasmFont.TextSizeMaxWidth(const aText: string;
  MaxWidth: TFresnelLength): TFresnelPoint;
var
  p: TPoint;
begin
  p:=Engine.TextSizeMaxWidth(Self,aText,Trunc(Max(1,MaxWidth)));
  Result.X:=p.X;
  Result.Y:=p.Y;
end;

function TFresnelWasmFont.GetTool: TObject;
begin
  Result:=Self;
end;

function TFresnelWasmFont.GetDescription: String;
begin
  Result:=Engine.FontToHTML(Self);
end;

{ TFresnelWasmFontEngine }

constructor TFresnelWasmFontEngine.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
  FFonts:=TAvlTree.Create(@CompareFresnelWasmFont);
end;

destructor TFresnelWasmFontEngine.Destroy;
var
  Node: TAvlTreeNode;
  aFont: TFresnelWasmFont;
begin
  Node:=FFonts.Root;
  while Node<>nil do
  begin
    aFont:=TFresnelWasmFont(Node.Data);
    Node.Data:=nil;
    aFont._Release;
    Node:=Node.Successor;
  end;
  FreeAndNil(FFonts);
  inherited Destroy;
end;

function TFresnelWasmFontEngine.FindFont(const Desc: TFresnelFontDesc
  ): TFresnelWasmFont;
var
  Node: TAvlTreeNode;
begin
  Node:=FFonts.FindKey(@Desc,@CompareFresnelFontDescWithWasmFont);
  if Node=nil then
    Result:=nil
  else
    Result:=TFresnelWasmFont(Node.Data);
end;

function TFresnelWasmFontEngine.Allocate(const Desc: TFresnelFontDesc
  ): IFresnelFont;
var
  aFont: TFresnelWasmFont;
begin
  aFont:=FindFont(Desc);
  if aFont<>nil then
    exit(aFont);
  aFont:=TFresnelWasmFont.Create;
  aFont.Engine:=Self;
  aFont._AddRef;
  aFont.Family:=Desc.Family;
  aFont.Kerning:=Desc.Kerning;
  aFont.Size:=Desc.Size;
  aFont.Style:=Desc.Style;
  aFont.Weight:=Desc.Weight;
  aFont.Width:=Desc.Width;
  FFonts.Add(aFont);
  Result:=aFont;
end;

function TFresnelWasmFontEngine.MaybeSetFont(aFont: TFresnelWasmFont) : Boolean;

var
  aFontName : UTF8String;

begin
  aFontName:=FontToHTML(aFont);
  Result:=aFontName<>FLastFontName;
  if Result then
    if __fresnel_canvas_set_font(CanvasID,PByte(aFontName),Length(aFontName))<>ECANVAS_SUCCESS then
      FLLog(etError,'Failed to set font name to '+aFontName);
end;

function TFresnelWasmFontEngine.TextSize(aFont: TFresnelWasmFont; const aText: string): TPoint;
var
  aWidth,aHeight : Longint;

begin
{$IFDEF DEBUGWASMFONT} FLLog('Enter TFresnelWasmFontEngine.TextSize');{$ENDIF}
  MaybeSetFont(aFont);
  if __fresnel_canvas_measure_text(CanvasID,PByte(aText),Length(aText),@aWidth,@aHeight)<>ECANVAS_SUCCESS then
    begin
    aWidth:=Length(aText)*10;
    aHeight:=12;
    end;
  Result:=TPoint.Create(aWidth,aHeight);
{$IFDEF DEBUGWASMFONT}  FLLog('Exit TFresnelWasmFontEngine.TextSize: (%d,%d)',[Result.X,Result.Y]));{$ENDIF}
end;

function TFresnelWasmFontEngine.TextSizeMaxWidth(aFont: TFresnelWasmFont;
  const aText: string; MaxWidth: integer): TPoint;

begin
  MaybeSetFont(aFont);
  Result:=TextSize(aFont,aText);
  if Result.X>MaxWidth then
    begin
    Result.X:=0;
    Result.Y:=0;
    end;
end;

function TFresnelWasmFontEngine.FontToHTML(aFont: TFresnelWasmFont): String;

  Function AddTo(res,aValue : String) : string;
  begin
    Result:=Res;
    if (res<>'') and (aValue<>'') then
      Result:=Result+' ';
    Result:=Result+aValue
  end;

begin
  Result:=aFont.Style;
  Result:=AddTo(Result,FormatFloat('##00',aFont.Weight));
  Result:=AddTo(Result,FormatFloat('##00',aFont.Size)+'px');
  if aFont.Family='' then
    Result:=AddTo(Result,'caption')
  else
    Result:=AddTo(Result,aFont.Family);
end;



end.

