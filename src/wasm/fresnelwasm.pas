{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit fresnelwasm;

{$warn 5023 off : no warning about unused units}
interface

uses
  fresnel.wasm.api, fresnel.wasm.shared, fresnel.wasm.render, fresnel.wasm.app, fresnel.wasm.font;

implementation

end.
