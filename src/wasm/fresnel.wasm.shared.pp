{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Webassembly rendering - common consts for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit fresnel.wasm.shared;

{$mode objfpc}{$H+}
{$modeswitch typehelpers}
{$modeswitch advancedrecords}

interface

uses
  {$IFDEF FPC_DOTTEDUNITS}
  System.Classes, System.SysUtils;
  {$ELSE}
  Classes, SysUtils;
  {$ENDIF}

const
  CanvasMsgSize = 4;
  CanvasMeasureTextSize = 4;
  FresnelScaleFactor = 100;

Type
  {$IFNDEF PAS2JS}
  TFresnelFloat = single;
  {$ELSE}
  TFresnelFloat = double;
  {$ENDIF}
  TFresnelFloatArray = Array of TFresnelFloat;

  TCanvasError = longint;
  TCanvasID = longint;
  TCanvasColorComponent = Word; // one of R G B A
  TCanvasColor = longint;
  TCanvasLineWidth = TFresnelFloat;
  TCanvasLineCap = byte;
  TCanvasLineJoin = byte;
  TCanvasTextBaseLine = Byte;
  TCanvasLineMiterLimit = TFresnelFloat;
  TMenuID = longint;


  TCanvasMessageID = longint;
  TCanvasMessageParam = longint;
  TCanvasMessageData = Array[0..CanvasMsgSize-1] of TCanvasMessageParam;

  TCanvasMeasureTextParam = TFresnelFloat;
  TCanvasMeasureTextData = Array[0..CanvasMeasureTextSize] of TCanvasMeasureTextParam;

  TLineDashPattern = TFresnelFloat;
  TLineDashPatternData = Array of TLineDashPattern;

  {$IFDEF PAS2JS}
  UTF8String = String;
  {$ENDIF}

  TTimerID = longint;
  TCanvasRoundRectData = Array[0..11] of TFresnelFloat;

  { TGradientColorPoint }

  TGradientColorPoint = record
    Red,Green,Blue,Alpha : longint;
    Percentage : longint; // Scaled 100
    function ToString : string;
  end;
  TGradientColorPoints = Array of TGradientColorPoint;

  { TCanvasMessageDataHelper }

  TCanvasMessageDataHelper = type helper for TCanvasMessageData
    Function ToString : UTF8String;
  end;

  {$IFNDEF PAS2JS}
  PFresnelFloat = ^TFresnelFloat;
  PCanvasID = ^TCanvasID;
  PCanvasColor = ^TCanvasColor;
  PCanvasMessageID = ^TCanvasMessageID;
  PCanvasMessageData = ^TCanvasMessageData;
  PCanvasMeasureTextData = ^TCanvasMeasureTextData;
  PCanvasRoundRectData = ^TCanvasRoundRectData;
  PGradientColorPoints = ^TGradientColorPoint;
  PLineDashPatternData = ^TLineDashPattern;
  PKeyMap = PLongint;
  TWasmPointer = Pointer;
  PMenuID = ^TMenuID;
  {$ELSE}
  TWasmPointer = Longint;
  PFresnelFloat = TWasmPointer;
  PCanvasID = TWasmPointer;
  PCanvasColor = TWasmPointer;
  PCanvasMessageID = TWasmPointer;
  PCanvasMessageData = TWasmPointer;
  PCanvasMeasureTextData = TWasmPointer;
  PCanvasRoundRectData = TWasmPointer;
  PGradientColorPoints = TWasmPointer;
  PLineDashPatternData = TWasmPointer;
  PKeyMap = TWasmPointer;
  PMenuID = TWasmPointer;
  {$ENDIF}

Const
  ECANVAS_SUCCESS      = 0;
  ECANVAS_NOCANVAS     = 1;
  ECANVAS_INVALIDPATH  = 2;
  ECANVAS_INVALIDPARAM = 3;
  ECANVAS_NOMENUSUPPORT = 4;
  ECANVAS_UNSPECIFIED  = -1;

  CANVAS_LINECAP_BUTT   = 0;
  CANVAS_LINECAP_ROUND  = 1;
  CANVAS_LINECAP_SQUARE = 2;

  CANVAS_LINEJOIN_ROUND = 0;
  CANVAS_LINEJOIN_BEVEL = 1;
  CANVAS_LINEJOIN_MITER = 2;

  EWASMEVENT_SUCCESS  = 0;
  EWASMEVENT_NOEVENT = 1;
  EWASMEVENT_NOCANVAS = 2;
  EWASMEVENT_ERROR    = 3;

  // Key state, Based on TShiftStateEnum
  WASM_KEYSTATE_SHIFT   = 1 shl Ord(ssShift);
  WASM_KEYSTATE_CTRL    = 1 shl Ord(ssAlt);
  WASM_KEYSTATE_ALT     = 1 shl Ord(ssCtrl);
  WASM_KEYSTATE_LEFT    = 1 shl Ord(ssLeft);
  WASM_KEYSTATE_RIGHT   = 1 shl Ord(ssRight);
  WASM_KEYSTATE_MIDDLE  = 1 shl Ord(ssMiddle);
  WASM_KEYSTATE_META    = 1 shl Ord(ssMeta);
  WASM_KEYSTATE_SUPER   = 1 shl Ord(ssSuper);
  WASM_KEYSTATE_HYPER   = 1 shl Ord(ssHyper);
  WASM_KEYSTATE_ALTGR   = 1 shl Ord(ssAltGr);

  // Location of mouse state data in parameters array
  WASMSG_MOUSESTATE_X        = 0;
  WASMSG_MOUSESTATE_Y        = 1;
  WASMSG_MOUSESTATE_STATE    = 2;
  WASMSG_MOUSESTATE_BUTTON   = 3;
  WASMSG_MOUSESTATE_DISTANCE = 3;

  // location of key state data in parameters array
  WASMSG_KEYSTATE_KEYCODE    = 0;
  WASMSG_KEYSTATE_KIND       = 1;
  WASMSG_KEYSTATE_SHIFTSTATE = 2;

  WASMSG_KEYKIND_CHAR    = 0;
  WASMSG_KEYKIND_SPECIAL = 1;

  // Location of data in measuretext array
  WASMMEASURE_WIDTH     = 0;
  WASMMEASURE_HEIGHT    = 1;
  WASMMEASURE_ASCENDER  = 2;
  WASMMEASURE_DESCENDER = 3;

Const
  WASMSG_NONE        = 0;
  WASMSG_MOVE        = 1; // Params[0]= X, [1]=Y, [2]=State
  WASMSG_MOUSEDOWN   = 2; // Params[0]= X, [1]=Y, [2]=State
  WASMSG_MOUSEUP     = 3; // Params[0]= X, [1]=Y, [2]=State
  WASMSG_MOUSESCROLL = 4; // Params[0]= X, [1]=Y, [2]=State
  WASMSG_CLICK       = 5; // Params[0]= X, [1]=Y, [2]=State
  WASMSG_WHEELY      = 6; // Params[0]= X, [1]=Y, [2]=State [3]=Distance
  WASMSG_DBLCLICK    = 7;
  WASMSG_ENTER       = 8;
  WASMSG_LEAVE       = 9;
  WASMSG_KEYDOWN     = 10;
  WASMSG_KEYUP       = 11;
  WASMSG_ACTIVATE    = 12;
  WASMSG_DEACTIVATE  = 13;

  // Roundrect flags
  ROUNDRECT_FLAG_FILL         = 1;

  // Indexes for roundrect data array.

  ROUNDRECT_BOXTOPLEFTX       = 0;
  ROUNDRECT_BOXTOPLEFTY       = 1;
  ROUNDRECT_BOXBOTTOMRIGHTX   = 2;
  ROUNDRECT_BOXBOTTOMRIGHTY   = 3;
  ROUNDRECT_RADIITOPLEFTX     = 4;
  ROUNDRECT_RADIITOPLEFTY     = 5;
  ROUNDRECT_RADIITOPRIGHTX    = 6;
  ROUNDRECT_RADIITOPRIGHTY    = 7;
  ROUNDRECT_RADIIBOTTOMLEFTX  = 8;
  ROUNDRECT_RADIIBOTTOMLEFTY  = 9;
  ROUNDRECT_RADIIBOTTOMRIGHTX = 10;
  ROUNDRECT_RADIIBOTTOMRIGHTY = 11;

  // Flags for SetImageFillStyle
  IMAGEFILLSTYLE_NOREPEAT  = 0;
  IMAGEFILLSTYLE_REPEAT    = 1;
  IMAGEFILLSTYLE_REPEATX   = 2;
  IMAGEFILLSTYLE_REPEATY   = 3;

  DRAWIMAGE_DESTX       = 0;
  DRAWIMAGE_DESTY       = 1;
  DRAWIMAGE_DESTWIDTH   = 2;
  DRAWIMAGE_DESTHEIGHT  = 3;
  DRAWIMAGE_SRCX        = 4;
  DRAWIMAGE_SRCY        = 5;
  DRAWIMAGE_SRCWIDTH    = 6;
  DRAWIMAGE_SRCHEIGHT   = 7;
  DRAWIMAGE_IMAGEWIDTH  = 8;
  DRAWIMAGE_IMAGEHEIGHT = 9;

  // Set_transform_flags
  TRANSFORM_RESET = 1;

  // Flags for Arc
  ARC_FILL   = 1;
  ARC_ROTATE = 2;

  // Flags for DrawPath
  DRAWPATH_CLOSEPATH  = 1;
  DRAWPATH_FILLPATH   = 2;
  DRAWPATH_STROKEPATH = 4;

  // Point types in DrawPath
  DRAWPATH_TYPEMOVETO  = 0;
  DRAWPATH_TYPELINETO  = 1;
  DRAWPATH_TYPECURVETO = 2;
  DRAWPATH_TYPECLOSE   = 3;

  // Which window size ?
  GETVIEWPORTSIZE_CLIENT = 0;
  GETVIEWPORTSIZE_WINDOW = 1;

  // Baseline for text
  TEXTBASELINE_TOP         = 0;
  TEXTBASELINE_HANGING     = 1;
  TEXTBASELINE_MIDDLE      = 2;
  TEXTBASELINE_ALPHABETIC  = 3;
  TEXTBASELINE_IDEOGRAPHIC = 4;
  TEXTBASELINE_BOTTOM      = 5;

  // Save/Restore
  STATE_FLAGS_RESTORE_PROPS = 1;

  // Menu flags
  MENU_FLAGS_INVISIBLE = 1;
  MENU_FLAGS_CHECKED = 2;
  MENU_FLAGS_RADIO   = 4;

Function LineCapToString(aCap: TCanvasLineCap) : String;
Function LineJoinToString(aJoin: TCanvasLineJoin) : String;
Function TextBaseLineToString(aBaseLine : TCanvasTextBaseLine) : String;

{
Function FresnelUnScale(aLen : Longint) : TFresnelFloat;
Function FresnelScale(aLen : TFresnelFloat) : Longint;
}

implementation

{
Function FresnelUnScale(aLen : Longint) : TFresnelFloat;

begin
  Result:=aLen/FresnelScaleFactor;
end;

Function FresnelScale(aLen : TFresnelFloat) : Longint;

begin
  Result:=Round(aLen*FresnelScaleFactor);
end;
}

function LineCapToString(aCap: TCanvasLineCap): String;

begin
  Case aCap of
    CANVAS_LINECAP_BUTT : Result:='butt';
    CANVAS_LINECAP_ROUND : Result:='round';
    CANVAS_LINECAP_SQUARE : Result:='square';
  else
    Result:='butt';
  end;
end;

Function LineJoinToString(aJoin: TCanvasLineJoin) : String;

begin
  Case aJoin of
    CANVAS_LINEJOIN_ROUND : Result:='round';
    CANVAS_LINEJOIN_MITER : Result:='miter';
    CANVAS_LINEJOIN_BEVEL : Result:='bevel';
  else
    Result:='round';
  end;
end;

Function TextBaseLineToString(aBaseLine : TCanvasTextBaseLine) : String;

begin
  Case aBaseLine of
    TEXTBASELINE_TOP : Result:='top';
    TEXTBASELINE_HANGING : Result:='hanging';
    TEXTBASELINE_MIDDLE : Result:='middle';
    TEXTBASELINE_ALPHABETIC : Result:='alphabetic';
    TEXTBASELINE_IDEOGRAPHIC : Result:='ideographic';
    TEXTBASELINE_BOTTOM : Result:='bottom';
  else
    Result:='alphabetic';
  end;
end;

{ TGradientColorPoint }

function TGradientColorPoint.ToString: string;
begin
  Result:=Format('{%g%% (r:%d, g:%d, b:%d / %d)}',[Percentage/100,Red,Green,Blue,Alpha]);
end;

{ TCanvasMessageDataHelper }

function TCanvasMessageDataHelper.ToString: UTF8String;

var
  I : Integer;
begin
  Result:=IntToStr(Self[0]);
  For I:=1 to CanvasMsgSize-1 do
    begin
    Result:=Result+',';
    Result:=Result+IntToStr(Self[I])
    end;
  Result:='['+Result+']';
end;

end.

