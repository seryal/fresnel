{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Webassembly rendering classes for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}

unit fresnel.wasm.render;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, fpImage, fresnel.classes, fresnel.dom, fresnel.renderer, fresnel.wasm.shared, fresnel.wasm.api;

Type

  { TWasmFresnelRenderer }

  TWasmFresnelRenderer = Class(TFresnelRenderer)
  private
    FCanvas: TCanvasID;
    FFillReset : Boolean;
    FLastFillColor : TFPColor;
    FLastStrokeColor : TFPColor;
    FLastFontName : String;
    function CheckFillColor(aColor: TFPColor): Boolean;
    function CheckFont(aFont: IFresnelFont): boolean;
    function CheckStrokeColor(aColor: TFPColor): Boolean;
  protected
    procedure DoFillRect(const aColor: TFPColor; const aRect: TFresnelRect; CheckFill: Boolean);
    procedure DoRoundRect(const aColor: TFPColor; const aRect: TFresnelRoundRect; Flags: Integer);
    // renderer methods
    procedure SetGradientFillStyle(aGradient : TFresnelCSSLinearGradient);
    procedure RoundRect(const aColor: TFPColor; const aRect: TFresnelRoundRect; Fill : Boolean); override;
    procedure FillRect(const aColor: TFPColor; const aRect: TFresnelRect); override;
    procedure Line(const aColor: TFPColor; const x1, y1, x2, y2: TFresnelLength); override;
    procedure TextOut(const aLeft, aTop: TFresnelLength;
      const aFont: IFresnelFont; const aColor: TFPColor;
      const aText: string); override;
    procedure DrawImage(const aLeft, aTop, aWidth, aHeight: TFresnelLength; const aImage: TFPCustomImage); override;
    procedure DrawElBackground(El: TFresnelElement; Params: TBorderAndBackground); override;
    procedure DrawElBorder(El: TFresnelElement; Params: TBorderAndBackground); override;
  public
    constructor Create(AOwner: TComponent); override;
    property Canvas: TCanvasID read FCanvas write FCanvas;
  end;

  { TWASMImage }

  TWASMImage = Class(TFPCompactImgRGBA8Bit)
  private
    function GetData: PByte;
  Public
    Property RawData : PByte Read GetData;
  end;

implementation

{ TWasmFresnelRenderer }

function TWasmFresnelRenderer.CheckFillColor(aColor : TFPColor) : Boolean;

begin
  Result:=not (aColor=FLastFillColor) or FFillReset;
  if Result then
    begin
    With aColor do
      begin
      FLLog(etDebug,'Fill color (%d, %d, %d - %d)',[Red,Green,Blue,Alpha]);
      __fresnel_canvas_set_fillstyle(Canvas,Red,Green,Blue,Alpha);
      end;
    FLastFillColor:=aColor;
    FFillReset:=False;
    end;
end;

function TWasmFresnelRenderer.CheckStrokeColor(aColor : TFPColor) : Boolean;

begin
  Result:=not (aColor=FLastStrokeColor);
  if Result then
    begin
    With aColor do
      begin
      FLLog(etDebug,'Stroke color (%d, %d, %d - %d)',[Red,Green,Blue,Alpha]);
      __fresnel_canvas_set_strokestyle(Canvas,Red,Green,Blue,Alpha);
      end;
    FLastStrokeColor:=aColor;
    end;
end;

procedure TWasmFresnelRenderer.SetGradientFillStyle(aGradient: TFresnelCSSLinearGradient);

var
  Colors : TGradientColorPoints;
  aX1,aY1,aX2,aY2 : TFresnelFLoat;
  P : TGradientColorPoint;
  Len,I : Integer;

begin
  aX1:=aGradient.StartPoint.X;
  aY1:=aGradient.StartPoint.Y;
  aX2:=aGradient.EndPoint.X;
  aY2:=aGradient.EndPoint.Y;
  Len:=Length(aGradient.Colors);
  Colors:=[];
  if Len=1 then
    SetLength(Colors,Len+1)
  else
    SetLength(Colors,Len);
  SetLength(Colors,Len);
  For I:=0 to Length(aGradient.Colors)-1 do
    begin
    P.Percentage:=Round(aGradient.Colors[I].Percentage*100);
    P.Red:=aGradient.Colors[I].Color.Red;
    P.Green:=aGradient.Colors[I].Color.Green;
    P.Blue:=aGradient.Colors[I].Color.Blue;
    P.Alpha:=aGradient.Colors[I].Color.Alpha;
    Colors[i]:=P;
    end;
  if Len=1 then
    begin
    P.Percentage:=100;
    Colors[1]:=P;
    end;
  __fresnel_canvas_linear_gradient_fillstyle(Canvas,aX1,aY1,aX2,aY2,Length(Colors),PGradientColorPoints(Colors));
  FFillReset:=True;
end;

procedure TWasmFresnelRenderer.DoRoundRect(const aColor: TFPColor; const aRect: TFresnelRoundRect; Flags: Integer);

var
  RR : TCanvasRoundRectData; // Array[0..11] of TfresnelFLoat;
  Idx : integer;
  Corner : TFresnelCSSCorner;

 procedure AddP(apoint : TFresnelPoint; OffSetOrigin : Boolean = False);
 begin
   RR[Idx]:=aPoint.X+(Ord(OffSetOrigin)*Origin.X);
   Inc(Idx);
   RR[Idx]:=aPoint.Y+(Ord(OffSetOrigin)*Origin.Y);
   Inc(Idx);
 end;

begin
  CheckStrokeColor(aColor);
  FLLog(etDebug,'RoundRect(%d,{%s})',[Canvas,aRect.ToString]);
  Idx:=0;
  AddP(aRect.Box.TopLeft,True);
  AddP(aRect.Box.BottomRight,True);
  For Corner in TFresnelCSSCorner do
    AddP(aRect.Radii[Corner]);
  if __fresnel_canvas_roundrect(Canvas,Flags,@RR)<>ECANVAS_SUCCESS then
    FLLog(etError,'failed to draw round rectangle on canvas %d',[Canvas]);
end;

procedure TWasmFresnelRenderer.RoundRect(const aColor: TFPColor; const aRect: TFresnelRoundRect; Fill: Boolean);

var
  Flags : Integer;

begin
  Flags:=0;
  if Fill then
    begin
    CheckFillColor(aColor);
    FLags:=ROUNDRECT_FLAG_FILL;
    end;
  DoRoundRect(aColor,aRect,Flags);
end;

function TWasmFresnelRenderer.CheckFont(aFont : IFresnelFont) : boolean;

var
  aFontName : string;

begin
  aFontName:=aFont.GetDescription;
  Result:=aFontName<>FLastFontName;
  if Result then
    begin
    if __fresnel_canvas_set_font(Canvas,PByte(aFontName),Length(aFontName))<>ECANVAS_SUCCESS then
      FLLog(etError,'failed to set canvas %d font to "%s"',[Canvas,aFontName]);
    FLastFontName:=aFontName;
    end;
end;

procedure TWasmFresnelRenderer.DoFillRect(const aColor: TFPColor; const aRect: TFresnelRect; CheckFill : Boolean);

begin
  if CheckFill then
    CheckFillColor(aColor);
  FLLog(etDebug,'__fresnel_canvas_fillrect(%d,%d,%d,%d,%d)',[Canvas,aRect.Left+Origin.X,aRect.Top+Origin.Y,aRect.Width,aRect.Height]);
  __fresnel_canvas_fillrect(Canvas,(aRect.Left+Origin.X),(aRect.Top+Origin.Y),(aRect.Width),(aRect.Height));
end;

procedure TWasmFresnelRenderer.FillRect(const aColor: TFPColor; const aRect: TFresnelRect);
begin
  DoFillRect(aColor,aRect,True);
end;

procedure TWasmFresnelRenderer.Line(const aColor: TFPColor; const x1, y1, x2, y2: TFresnelLength);

begin
  CheckStrokeColor(aColor);
  __fresnel_canvas_set_linewidth(Canvas,200);
  __fresnel_canvas_beginpath(Canvas);
  __fresnel_canvas_moveto(Canvas, (x1+Origin.X), (y1+Origin.Y));
  __fresnel_canvas_lineto(Canvas, (x2+Origin.X), (y2+Origin.Y));
  __fresnel_canvas_stroke(Canvas);
end;

procedure TWasmFresnelRenderer.TextOut(const aLeft, aTop: TFresnelLength; const aFont: IFresnelFont; const aColor: TFPColor;
  const aText: string);

var
  I,Count : integer;
  aShadow : PFresnelTextShadow;

begin
  CheckFillColor(aColor);
  CheckFont(aFont);
  Count:=TextShadowCount;
  if Count=0 then
    begin
    // X,Y,Radius,r,g,b,a)
    if __fresnel_canvas_set_textshadow_params(Canvas,0,0,0,0,0,0,0)<>ECANVAS_SUCCESS then
      FLLog(etError,'failed to clear shadow params for text "%s"',[Canvas,aText]);
    if __fresnel_canvas_filltext(Canvas,(aLeft+Origin.X),(aTop+Origin.Y),PByte(aText),Length(aText))<>ECANVAS_SUCCESS then
      FLLog(etError,'failed to draw canvas %d text "%s"',[Canvas,aText]);
    end
  else
    for I:=0 to TextShadowCount-1 do
      begin
      aShadow:=Self.TextShadow[I];
      With aShadow^ do
        if __fresnel_canvas_set_textshadow_params(Canvas,(Offset.X),(Offset.Y),Radius,Color.Red,Color.Green,Color.Blue,Color.Alpha)<>ECANVAS_SUCCESS then
          FLLog(etError,'failed to set shadow params for text "%s"',[Canvas,aText]);
      if __fresnel_canvas_filltext(Canvas,(aLeft+Origin.X),(aTop+Origin.Y),PByte(aText),Length(aText))<>ECANVAS_SUCCESS then
        FLLog(etError,'failed to draw canvas %d text "%s"',[Canvas,aText]);
      if __fresnel_canvas_set_textshadow_params(Canvas,0,0,0,0,0,0,0)<>ECANVAS_SUCCESS then
        FLLog(etError,'failed to clear shadow params for text "%s"',[Canvas,aText]);
      end;
end;

procedure TWasmFresnelRenderer.DrawImage(const aLeft, aTop, aWidth, aHeight: TFresnelLength; const aImage: TFPCustomImage);

var
  Img : TWASMImage;

begin
  if aImage is TWASMImage then
    Img:=TWasmImage(aImage)
  else
    Img:=TWASMImage.Create(aImage.Width,aImage.Width);
  try
    if Img<>aImage then
      Img.Assign(aImage);
    __fresnel_canvas_draw_image(Canvas,
                                aLeft+Origin.X,
                                aTop+Origin.Y,
                                aWidth,
                                aHeight,
                                Img.Width,
                                Img.Height,
                                Img.RawData);
  finally
    if Img<>aImage then
      FreeAndNil(Img);
  end;
end;

procedure TWasmFresnelRenderer.DrawElBackground(El: TFresnelElement; Params: TBorderAndBackground);

begin
  if el=nil then;
  if Params.BackgroundImage is TFresnelCSSLinearGradient then
  begin
    SetGradientFillStyle(Params.BackgroundImage as TFresnelCSSLinearGradient);
    if Params.HasRadius then
      DoRoundRect(Params.BackgroundColorFP,Params.BoundingBox,ROUNDRECT_FLAG_FILL)
    else
      DoFillRect(Params.BackgroundColorFP,Params.BoundingBox.Box,False);
  end else if Params.BackgroundColorFP.Alpha>alphaTransparent then
  begin
    //FLLog(etDebug,'TFresnelRenderer.DrawElBorder drawing background %s',[El.GetPath]);
    __fresnel_canvas_set_linewidth(Canvas,Params.Width[ffsLeft]);
    if Params.HasRadius then
      RoundRect(Params.BackgroundColorFP,Params.BoundingBox,True)
    else
      FillRect(Params.BackgroundColorFP,Params.BoundingBox.Box);
  end;

end;

procedure TWasmFresnelRenderer.DrawElBorder(El: TFresnelElement; Params: TBorderAndBackground);

var
  BB : TFresnelRoundRect;
  HalfWidth : TFresnelLength;
  C : TFresnelCSSCorner;

begin
  FLLog(etDebug,'TWasmFresnelRenderer.DrawElBorder("%s")',[El.GetPath]);
  if Not Params.HasRadius then
    Inherited DrawElBorder(El,Params)
  else if Params.SameBorderWidth then
    begin
    __fresnel_canvas_set_linewidth(Canvas,Params.Width[ffsLeft]);
    BB:=Params.BoundingBox;
    With Params do
      begin
      HalfWidth:=Width[ffsLeft]/2;
      BB.Box.TopLeft.OffSet(+HalfWidth,+HalfWidth);
      BB.Box.BottomRight.OffSet(-HalfWidth,-HalfWidth);
      For C in TFresnelCSSCorner do
        begin
        if BB.Radii[C].X>=HalfWidth then
          BB.Radii[C].X:=BB.Radii[C].X-HalfWidth;
        if BB.Radii[C].Y>=HalfWidth then
          BB.Radii[C].Y:=BB.Radii[C].Y-HalfWidth;
        end
      end;
    RoundRect(Params.Color[Low(TFresnelCSSSide)],BB,False);
    end
  else
    begin
    // Todo
    end;
end;

constructor TWasmFresnelRenderer.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

{ TWASMImage }

function TWASMImage.GetData: PByte;
begin
  Result:=PByte(FData);
end;

end.

