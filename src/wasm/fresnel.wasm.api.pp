{
    This file is part of the Fresnel Library.
    Copyright (c) 2024 by the FPC & Lazarus teams.

    Webassembly browser rendering API for Fresnel

    See the file COPYING.modifiedLGPL.txt, included in this distribution,
    for details about the copyright.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.

 **********************************************************************}


unit fresnel.wasm.api;

{$mode objfpc}{$H+}

// Define this if you want to remove logging code.
{ $DEFINE NOFRESNELLOG}

interface

uses fresnel.wasm.shared;

{ ---------------------------------------------------------------------
  Canvas API
  ---------------------------------------------------------------------}

function __fresnel_canvas_allocate(
  SizeX : Longint;
  SizeY : Longint;
  aID: PCanvasID
): TCanvasError; external 'fresnel_api' name 'canvas_allocate';

function __fresnel_canvas_allocate_offscreen(
  SizeX : Longint;
  SizeY : Longint;
  aImageData : PByte;
  aID: PCanvasID
): TCanvasError; external 'fresnel_api' name 'canvas_allocate_offscreen';



function __fresnel_canvas_deallocate(
  aID: TCanvasID
): TCanvasError; external 'fresnel_api' name 'canvas_deallocate';


function __fresnel_canvas_getbyid(
  PElementID : PByte;
  Len : Longint;
  aID: PCanvasID
): TCanvasError; external 'fresnel_api' name 'canvas_getbyid';


function __fresnel_canvas_getsizes(
  aID : TCanvasID;
  aWidth: PFresnelFloat;
  aHeight: PFresnelFloat
): TCanvasError; external 'fresnel_api' name 'canvas_getsizes';

function __fresnel_canvas_setsizes(
  aID : TCanvasID;
  aWidth: TFresnelFloat;
  aHeight: TFresnelFloat
): TCanvasError; external 'fresnel_api' name 'canvas_setsizes';


function __fresnel_canvas_moveto(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat
):  TCanvasError; external 'fresnel_api' name 'canvas_moveto';

function __fresnel_canvas_lineto(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat
):  TCanvasError; external 'fresnel_api' name 'canvas_lineto';

function __fresnel_canvas_stroke(
  aID : TCanvasID
):  TCanvasError; external 'fresnel_api' name 'canvas_stroke';

function __fresnel_canvas_beginpath(
  aID : TCanvasID
):  TCanvasError; external 'fresnel_api' name 'canvas_beginpath';

function __fresnel_canvas_arc(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat;
  RadiusX : TFresnelFloat;
  RadiusY : TFresnelFloat;
  StartAngle : TFresnelFloat;
  EndAngle : TFresnelFloat;
  Rotate : TFresnelFloat;
  Flags : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_arc';


function __fresnel_canvas_fillrect(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat;
  Width : TFresnelFloat;
  Height : TFresnelFloat
):  TCanvasError; external 'fresnel_api' name 'canvas_fillrect';

function __fresnel_canvas_strokerect(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat;
  Width : TFresnelFloat;
  Height : TFresnelFloat
):  TCanvasError; external 'fresnel_api' name 'canvas_strokerect';

function __fresnel_canvas_roundrect(
  aID : TCanvasID;
  aFlags : longint;
  aData : PCanvasRoundRectData
):  TCanvasError; external 'fresnel_api' name 'canvas_roundrect';

function __fresnel_canvas_clearrect(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat;
  Width : Longint;
  Height : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_clearrect';

function __fresnel_canvas_stroketext(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat;
  aText : PByte;
  aTextLen : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_stroketext';

function __fresnel_canvas_filltext(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat;
  aText : PByte;
  aTextLen : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_filltext';

function __fresnel_canvas_set_fillstyle(
  aID : TCanvasID;
  aRed : Longint;
  aGreen: Longint;
  aBlue : Longint;
  aAlpha : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_set_fillstyle';

function __fresnel_canvas_clear(
  aID : TCanvasID;
  aRed : Longint;
  aGreen: Longint;
  aBlue : Longint;
  aAlpha : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_clear';


function __fresnel_canvas_set_strokestyle(
  aID : TCanvasID;
  aRed : Longint;
  aGreen: Longint;
  aBlue : Longint;
  aAlpha : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_set_strokestyle';

function __fresnel_canvas_set_linewidth(
  aID : TCanvasID;
  aWidth : TCanvasLineWidth
):  TCanvasError; external 'fresnel_api' name 'canvas_set_linewidth';

function __fresnel_canvas_set_linecap(
  aID : TCanvasID;
  aWidth : TCanvasLinecap
):  TCanvasError; external 'fresnel_api' name 'canvas_set_linecap';

function __fresnel_canvas_set_linejoin(
  aID : TCanvasID;
  aWidth : TCanvasLineJoin
):  TCanvasError; external 'fresnel_api' name 'canvas_set_linejoin';

function __fresnel_canvas_set_linemiterlimit(
  aID : TCanvasID;
  aWidth : TCanvasLineMiterLimit
):  TCanvasError; external 'fresnel_api' name 'canvas_set_linemiterlimit';

function __fresnel_canvas_set_textbaseline(
  aID : TCanvasID;
  aWidth : TCanvasTextBaseLine
):  TCanvasError; external 'fresnel_api' name 'canvas_set_textbaseline';


function __fresnel_canvas_set_linedash(  aID : TCanvasID;
  aOffset : TFresnelFLoat;
  aPatternCount : longint;
  aPattern : PLineDashPatternData
):  TCanvasError; external 'fresnel_api' name 'canvas_set_linedash';

function __fresnel_canvas_set_font(
  aID : TCanvasID;
  aFontName : PByte;
  aFontNameLen : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_set_font';

function __fresnel_canvas_measure_text(
  aID : TCanvasID;
  aText : PByte;
  aTextLen : Longint;
  aWidth : PFresnelFLoat;
  aHeight : PFresnelFLoat
):  TCanvasError; deprecated 'use float versions instead';

function __fresnel_canvas_measure_text(
  aID : TCanvasID;
  aText : PByte;
  aTextLen : Longint;
  Out aWidth,aHeight,aAscender,aDescender : TFresnelFloat
):  TCanvasError;


function __fresnel_canvas_measure_text(
  aID : TCanvasID;
  aText : PByte;
  aTextLen : Longint;
  aData : PCanvasMeasureTextData
):  TCanvasError; external 'fresnel_api' name 'canvas_measure_text';


function __fresnel_canvas_set_textshadow_params (aID : TCanvasID;
    aOffsetX,aOffsetY,aRadius : TFresnelFLoat;
    aRed,aGreen,aBlue,aAlpha : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_set_textshadow_params';

function __fresnel_canvas_linear_gradient_fillstyle(aID : TCanvasID;
    aStartX,aStartY,aEndX,aEndY : TFresnelFLoat;
    aColorPointCount : longint;
    aColorPoints : PGradientColorPoints
):  TCanvasError; external 'fresnel_api' name 'canvas_linear_gradient_fillstyle';

function __fresnel_canvas_image_fillstyle(aID : TCanvasID;
    aFlags,aImageWidth,aImageHeight : Longint;
    aImageData : PByte
):  TCanvasError; external 'fresnel_api' name 'canvas_image_fillstyle';

// Image in RGBA
function __fresnel_canvas_draw_image(
  aID : TCanvasID;
  aX : TFresnelFLoat;
  aY : TFresnelFLoat;
  aWidth : TFresnelFLoat;
  aHeight : TFresnelFLoat;
  aImageWidth: Longint;
  aImageHeight: Longint;
  aImageData : PByte
):  TCanvasError; external 'fresnel_api' name 'canvas_draw_image';

function __fresnel_canvas_draw_image_ex(
  aID : TCanvasID;
  aDrawData : PFresnelFloat;
  aImageData : PByte
):  TCanvasError; external 'fresnel_api' name 'canvas_draw_image_ex';

function __fresnel_canvas_point_in_path(
  aID : TCanvasID;
  aX : TFresnelFLoat;
  aY : TFresnelFLoat;
  aPointCount : Longint;
  aPointData : PFresnelFLoat;
  aRes : PByte
):  TCanvasError; external 'fresnel_api' name 'canvas_point_in_path';

function __fresnel_canvas_draw_path(
  aID : TCanvasID;
  aFlags : Longint;
  aPointCount : Integer;
  aPointData : PFresnelFloat
):  TCanvasError; external 'fresnel_api' name 'canvas_draw_path';


function __fresnel_canvas_set_transform(
 aID : TCanvasID;
 Flags : Longint;
 m11,m12,m21,m22,m31,m32 : TFresnelFLoat) : TCanvasError; external 'fresnel_api' name 'canvas_set_transform';

function __fresnel_canvas_get_viewport_sizes(
 aFlags : Longint;
 aWidth,aHeight : PFresnelFLoat) : TCanvasError; external 'fresnel_api' name 'canvas_get_viewport_sizes';

function __fresnel_canvas_set_title(
  aID : TCanvasID;
  aTitle : PByte;
  aTitleLen : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_set_title';

function __fresnel_canvas_save_state(
  aID : TCanvasID;
  Flags : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_save_state';

function __fresnel_canvas_restore_state(
  aID : TCanvasID;
  Flags : Longint
):  TCanvasError; external 'fresnel_api' name 'canvas_restore_state';

function __fresnel_canvas_clip_add_rect(
  aID : TCanvasID;
  X : TFresnelFloat;
  Y : TFresnelFloat;
  Width : TFresnelFloat;
  Height : TFresnelFloat
):  TCanvasError; external 'fresnel_api' name 'canvas_clip_add_rect';


{ ---------------------------------------------------------------------
  Timer API
  ---------------------------------------------------------------------}

function __fresnel_timer_allocate(ainterval : longint; userdata: pointer) : TTimerID; external 'fresnel_api' name 'timer_allocate';

procedure __fresnel_timer_deallocate(timerid: TTimerID); external 'fresnel_api' name 'timer_deallocate';

{ ---------------------------------------------------------------------
  Event API
  ---------------------------------------------------------------------}

// note that the events are for a single canvas !

function __fresnel_event_get(
  aID : PCanvasID;
  aMsg : PCanvasMessageID;
  aData : PCanvasMessageData
):  TCanvasError; external 'fresnel_api' name 'event_get';

function __fresnel_event_count(
  aCount : PLongint
):  TCanvasError; external 'fresnel_api' name 'event_count';

function __fresnel_event_set_special_keymap(
  aMap : PKeyMap;
  aCount : Longint
):  TCanvasError; external 'fresnel_api' name 'event_set_special_keymap';

Type

  TFresnelLogLevel = (fllTrace, fllDebug, fllInfo, fllWarning, fllError, fllCritical);
  TFresnelLogLevels = set of TFresnelLogLevel;

  TFresnelTickEvent = Procedure(aCurrent,aPrevious : Double) of Object;
  TFresnelProcessMessageEvent = TFresnelTickEvent;
  TFresnelTimerTickEvent = Procedure (aTimerID : TTimerID; userdata : pointer; var aContinue : Boolean);
  TFresnelLogHook = procedure (const Msg : string) of object;
  TFresnelLogLevelHook = procedure (Level : TFresnelLogLevel; const Msg : string) of object;
  TFresnelMenuClickEvent = procedure(aMenuID : TMenuID; aData : Pointer) of object;

{ ---------------------------------------------------------------------
  Menu API
  ---------------------------------------------------------------------}


function __fresnel_menu_add_item(
  aID : TCanvasID;
  aParent : TMenuID;
  aCaption : PByte;
  aCaptionLen : Longint;
  aClickData : PByte;
  aFlags : Longint;
  aShortCut : Longint;
  aMenuID : PMenuID
) : TCanvasError; external 'fresnel_api' name 'menu_add_item';

function __fresnel_menu_remove_item(
  aID : TCanvasID;
  aMenuID : TMenuID
) : TCanvasError; external 'fresnel_api' name 'menu_remove_item';

function __fresnel_menu_update_item(
  aID : TCanvasID;
  aMenuID : TMenuID;
  aFlags : longint;
  aShortCut : Longint
) : TCanvasError; external 'fresnel_api' name 'menu_update_item';


var
  OnFresnelWasmTick : TFresnelTickEvent;
  OnFresnelProcessMessage : TFresnelProcessMessageEvent;
  OnFresnelTimerTick : TFresnelTimerTickEvent;
  OnFresnelLog : TFresnelLogHook deprecated 'Use OnFresnelLogLevel';
  OnFresnelLogLevel : TFresnelLogLevelHook;
  OnFresnelMenuClick : TFresnelMenuClickEvent;

{ Exported functions }

procedure __fresnel_tick (aCurrent,aPrevious : double);
procedure __fresnel_process_message (aCurrent,aPrevious : double);
function __fresnel_timer_tick(timerid: TTimerID; userdata : pointer) : boolean;
procedure __fresnel_menu_click(menuid: TMenuID; userdata : pointer);


procedure __fresnel_log(aLevel : TFresnelLogLevel; Const Msg : string);
procedure __fresnel_log(aLevel : TFresnelLogLevel; Const Fmt : string; args : Array of const);
// These use fllInfo:
procedure __fresnel_log(Const Msg : string); inline;
procedure __fresnel_log(Const Fmt : string; args : Array of const); inline;

function __fresnel_scale(len : single) : longint;

implementation

{$IFDEF FPC_DOTTEDUNITS}
uses System.SysUtils;
{$ELSE}
uses SysUtils;
{$ENDIF}

function __fresnel_scale(len : single) : longint;

begin
  Result:=Round(len*FresnelScaleFactor);
end;

function __fresnel_canvas_measure_text(aID: TCanvasID; aText: PByte; aTextLen: Longint; aWidth: PFresnelFloat; aHeight: PFresnelFloat
  ): TCanvasError;

var
  Data : TCanvasMeasureTextData;

begin
  Data:=Default(TCanvasMeasureTextData);
  Result:=__fresnel_canvas_measure_text(aID,aText,aTextLen,@Data);
  if Result=ECANVAS_SUCCESS then
    begin
    aWidth^:=Data[WASMMEASURE_WIDTH];
    aHeight^:=Data[WASMMEASURE_HEIGHT];
    end;
end;

function __fresnel_canvas_measure_text(aID: TCanvasID; aText: PByte; aTextLen: Longint; out aWidth, aHeight, aAscender,
  aDescender: Single): TCanvasError;
var
  Data : TCanvasMeasureTextData;

begin
  Data:=Default(TCanvasMeasureTextData);
  Result:=__fresnel_canvas_measure_text(aID,aText,aTextLen,@Data);
  if Result=ECANVAS_SUCCESS then
    begin
    aWidth:=Data[WASMMEASURE_WIDTH];
    aHeight:=Data[WASMMEASURE_HEIGHT];
    aAscender:=Data[WASMMEASURE_ASCENDER];
    aDescender:=Data[WASMMEASURE_DESCENDER];
    end;
end;

procedure __fresnel_tick (aCurrent,aPrevious : double);

begin
  if assigned(OnFresnelWasmTick) then
    OnFresnelWasmTick(aCurrent,aPrevious);
end;

procedure __fresnel_process_message (aCurrent,aPrevious : double);

begin
  if assigned(OnFresnelProcessMessage) then
    OnFresnelProcessMessage(aCurrent,aPrevious);
end;

procedure __fresnel_log(aLevel : TFresnelLogLevel; Const Msg : string);

begin
  {$IFNDEF NOFRESNELLOG}
  if Assigned(OnFresnelLoglevel) then
    OnFresnelLogLevel(aLevel,Msg)
  else if Assigned(OnFresnelLog) then
    OnFresnelLog(Msg)
  {$ENDIF}
end;

procedure __fresnel_log(Const Msg : string);

begin
  {$IFNDEF NOFRESNELLOG}
  __fresnel_log(fllInfo,Msg);
  {$ENDIF}
end;

procedure __fresnel_log(aLevel : TFresnelLogLevel; Const Fmt : string; args : Array of const);

begin
  {$IFNDEF NOFRESNELLOG}
  __fresnel_log(aLevel,SafeFormat(Fmt,Args));
  {$ENDIF}
end;

procedure __fresnel_log(Const Fmt : string; args : Array of const);
begin
  {$IFNDEF NOFRESNELLOG}
  __fresnel_log(fllInfo,Fmt,Args);
  {$ENDIF}
end;

function __fresnel_timer_tick(timerid: TTimerID; userdata : pointer) : boolean;

begin
  Result:=True;
  if assigned(OnFresnelTimerTick) then
    OnFresnelTimerTick(timerid,userdata,Result)
  else
    Result:=False;
end;

procedure __fresnel_menu_click(menuid: TMenuID; userdata : pointer);

begin
  if assigned(OnFresnelMenuClick) then
    OnFresnelMenuClick(menuid,userdata)
end;


exports
  __fresnel_process_message,
  __fresnel_timer_tick,
  __fresnel_menu_click,
  __fresnel_tick;

end.

