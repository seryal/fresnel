program CustomSlider;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Fresnel, // this includes the Fresnel widgetset
  Fresnel.Forms, MainUnit;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFresnelSliderForm, FresnelSliderForm);
  Application.Run;
end.

