unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.DOM, Fresnel.Controls, Fresnel.DemoSlider;

type

  { TFresnelSliderForm }

  TFresnelSliderForm = class(TFresnelForm)
    procedure FresnelSliderFormCreate(Sender: TObject);
  private
  public
  end;

var
  FresnelSliderForm: TFresnelSliderForm;

implementation

{$R *.lfm}

{ TFresnelSliderForm }

procedure TFresnelSliderForm.FresnelSliderFormCreate(Sender: TObject);
var
  Slider: TDemoSlider;
begin
  Slider:=TDemoSlider.Create(Self);
  with Slider do begin
    Name:='Slider';
    Style:='width: 150px';
    MinPosition:=0;
    MaxPosition:=100;
    SliderPosition:=30;
    Parent:=Self;
  end;
end;

end.

