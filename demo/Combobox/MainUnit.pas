unit MainUnit;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.Controls, Fresnel.Events,
  FCL.Events, Fresnel.DemoCombobox;

type

  { TMainForm }

  TMainForm = class(TFresnelForm)
    Body1: TBody;
    Div1: TDiv;
    Label1: TLabel;
    procedure MainFormCreate(Sender: TObject);
  private
    procedure OnCombobox1Change(Sender: TObject);
  public
    Combobox1: TDemoCombobox;
  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.MainFormCreate(Sender: TObject);
begin
  Stylesheet.Add(''
    +'body {'
    +'  font-size: 15px;'
    +'}'
    +'#Div1 {'
    +'  background:linear-gradient(#ededed, #bab1ba);'
    +'  border:7px solid #18ab29;'
    +'  padding:16px 31px;'
    +'  font-size:15px; font-family:Arial; font-weight:bold;'
    +'  text-shadow: 0 1 1 #333;'
    +'  color:#fff;'
    +'};');
  Div1.Style:='display: none;';

  Combobox1:=TDemoCombobox.Create(Self);
  Combobox1.Name:='Combobox1';
  Combobox1.Parent:=Body1;
  Combobox1.Items.Add('First');
  Combobox1.Items.Add('Second');
  Combobox1.Items.Add('Third');
  Combobox1.OnChange:=@OnCombobox1Change;
end;

procedure TMainForm.OnCombobox1Change(Sender: TObject);
begin
  Application.Log(etInfo,'TMainForm.OnCombobox1Change ItemIndex=%d',[Combobox1.ItemIndex]);
end;

end.

