program ComboboxNative;

uses
  UTF8Utils,
  dl,
  Fresnel, // initializes the widgetset
  Fresnel.App, MainUnit;

begin
  writeln();
  FresnelApplication.HookFresnelLog:=true;
  FresnelApplication.Initialize;
  FresnelApplication.CreateForm(TMainForm,MainForm);
  FresnelApplication.Run;
end.

