program fresnelhost;

{$mode objfpc}

uses
  BrowserConsole, BrowserApp, JS, Classes, SysUtils, Web, wasihostapp, wasienv, fresnel.pas2js.wasmapi,
  wasizenfs, libzenfs, libzenfsdom, types;

var
  wasmFilename : string; external name 'wasmFilename';

type

  { TFresnelHostApplication }

  TFresnelHostApplication = class(TBrowserWASIHostApplication)
  Private
    FFresnelAPI : TWasmFresnelApi;
    FS :TWASIZenFS;
    procedure DoShowConsole(Event: TJSEvent);
    procedure OnAfterStart(Sender: TObject; aDescriptor: TWebAssemblyStartDescriptor);
  Public
    constructor Create(aOwner : TComponent); override;
    procedure DoRun; override;
    procedure RunWasm ; async;
    function LoadFiles: Integer; async;
    Property FresnelAPI : TWasmFresnelApi Read FFresnelAPI;
  end;

{ TFresnelHostApplication }

procedure TFresnelHostApplication.OnAfterStart(Sender: TObject; aDescriptor: TWebAssemblyStartDescriptor);
begin
  Writeln('Start process loop');
  FFresnelApi.ProcessMessages;
  Writeln('Started process loop');
end;

procedure TFresnelHostApplication.DoShowConsole(Event: TJSEvent);

begin
  GetHTMLElement('divConsole').classList.toggle('is-hidden');
end;

constructor TFresnelHostApplication.Create(aOwner: TComponent);
begin
  inherited Create(aOwner);
  FFresnelApi:=TWasmFresnelApi.Create(WasiEnvironment);
  FFresnelAPI.LogAPICalls:=False;
  FFresnelAPI.CanvasParent:=TJSHTMLElement(document.getElementById('desktop'));
  RunEntryFunction:='_initialize';
  GetHTMLElement('cbconsole').AddEventListener('click',@DoShowConsole);
end;

procedure TFresnelHostApplication.DoRun;

begin
  RunWasm;
end;

function TFresnelHostApplication.LoadFiles: Integer;

const
  Files : TStringDynArray =
       ('image.png','style1.css','style2.css');
var
  Res: TPreLoadFilesResult;
  I : Integer;
begin
  result:=-1;
  try
    Res:=await (PreloadFilesIntoDirectory('/',Files));
  except
    on E : Exception do
      Writeln('Exception while loading files: ',E.Message);
    on JE : TJSError do
      Writeln('Exception while loading files: ',JE.Message);
  end;
  For I:=0 to Length(Res.failedurls)-1 do
    With Res.failedurls[i] do
      Writeln('Failed to preload file: ',url,' : ',error);
  Result:=res.loadcount;
end;

procedure TFresnelHostApplication.RunWasm;

var
  WasmModule : string;
  aCount : Integer;

begin
  // Your code here
  Terminate;
  await(tjsobject,  ZenFS.configure(
    new(
      ['mounts', new([
        '/', DomBackends.WebStorage
       ])
      ])
    )
  );
  FS:=TWASIZenFS.Create;
  WasiEnvironment.FS:=FS;
  aCount:=await(LoadFiles);
  Writeln('Loaded ',aCount,' files.');
  if isString(wasmFilename) then
    WasmModule:=wasmFilename
  else
    begin
    WasmModule:=ParamStr(1);
    if WasmModule='' then
      WasmModule:='demo.wasm';
    end;
  Writeln('Loading and starting Webassembly module: ',WasmModule);
  StartWebAssembly(WasmModule,true,Nil,@OnAfterStart);
end;

var
  Application : TFresnelHostApplication;
begin
  ConsoleStyle:=DefaultCRTConsoleStyle;
  HookConsole;
  Application:=TFresnelHostApplication.Create(nil);
  Application.Initialize;
  Application.Run;
end.
