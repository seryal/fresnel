program EditDemo;

uses
  {$IFDEF Unix}
  cwstring,
  cthreads,
  {$ENDIF}
  Fresnel, // initializes the widgetset
  Fresnel.App,
  MainUnit;

begin
  FresnelApplication.HookFresnelLog:=true;
  FresnelApplication.Initialize;
  FresnelApplication.CreateForm(TMainForm,MainForm);
  FresnelApplication.Run;
end.

