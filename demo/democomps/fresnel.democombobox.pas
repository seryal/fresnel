unit Fresnel.DemoCombobox;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, System.UITypes, FPReadPNG, Fresnel.DOM, Fresnel.Controls,
  Fresnel.Classes, FCL.Events, Fresnel.Events, fpCSSTree;

type

  { TCustomDemoCombobox }

  TCustomDemoCombobox = class(TDiv,IFPObserver)
  private
    FItemIndex: integer;
    FItems: TStrings;
    FOnChange: TNotifyEvent;
    function GetCaption: string;
  protected
    procedure DoCaptionDivClick(Event: TAbstractEvent); virtual;
    procedure DoChange; virtual;
    procedure DoItemClick(Event: TAbstractEvent); virtual;
    procedure FPOObservedChanged(ASender: TObject; {%H-}Operation: TFPObservedOperation; {%H-}Data: Pointer); override;
    procedure Loaded; override;
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetCaption(const AValue: string); virtual;
    procedure SetItemIndex(const AValue: integer); virtual;
    procedure SetItems(const AValue: TStrings); virtual;
    procedure SetName(const NewName: TComponentName); override;
  public
    // default styles
    const
      cStyle = ''
        +'.Combobox {'+LineEnding
        +'  position: relative;'+LineEnding
        +'}'+LineEnding
        +'.ComboboxCaption {'+LineEnding
        +'  padding: 6px;'+LineEnding
        +'  border: 1px solid #5080e0;'+LineEnding
        +'  border-radius: 5px;'+LineEnding
        +'  background-color: #b6d6f0;'+LineEnding
        +'  display: flex;'+LineEnding
        +'  justify-content: center;'+LineEnding
        +'}'+LineEnding
        +'.ComboboxCaption:hover {'+LineEnding
        +'  background-color: #a6e6ff;'+LineEnding
        +'}'+LineEnding
        +'.ComboboxCaptionLabel {'+LineEnding
        +'}'+LineEnding
        +'.ComboboxCaptionIcon {'+LineEnding
        +'  width: 16px;'+LineEnding
        +'  height: 16px;'+LineEnding
        +'  padding-left: 8px;'+LineEnding
        +'}'+LineEnding
        +'.ComboboxMenu {'+LineEnding
        +'  position: absolute;'+LineEnding
        +'  padding-top: 2px;'+LineEnding
        +'  border-radius: 5px;'+LineEnding
        +'  border: 1px solid #5080e0;'+LineEnding
        +'  background-color: #b6d6f0;'+LineEnding
        +'}'+LineEnding
        +'.ComboboxContent {'+LineEnding
        +'  padding: 5px 0;'+LineEnding
        +'}'+LineEnding
        +'.ComboboxItem {'+LineEnding
        +'  padding: 5px 20px;'+LineEnding
        +'}'+LineEnding
        +'.ComboboxItem:hover {'+LineEnding
        +'  background-color: #a6e6ff;'+LineEnding
        +'}'+LineEnding
        +'.ComboboxSelected {'+LineEnding
        +'  background-color: #a6d6ff;'+LineEnding
        +'}'+LineEnding;
  public
    CaptionDiv: TDiv;
    CaptionLabel: TLabel;
    CaptionIcon: TImage;
    Menu: TDiv;
    Content: TDiv;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure Clear; virtual;
    class function GetCSSTypeStyle: TCSSString; override;
    procedure UpdateItems; virtual;
    procedure UpdateCaption; virtual;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
    property Items: TStrings read FItems write SetItems;
    property ItemIndex: integer read FItemIndex write SetItemIndex;
    property Caption: string read GetCaption write SetCaption;
  end;

  { TDemoCombobox }

  TDemoCombobox = class(TCustomDemoCombobox)
  published
    property OnChange;
    property Items;
    property ItemIndex;
    property Caption;
  end;

implementation

{$I fresnel.democombobox.inc}

{ TCustomDemoCombobox }

procedure TCustomDemoCombobox.SetItems(const AValue: TStrings);
begin
  if (FItems=AValue) or FItems.Equals(AValue) then Exit;
  FItems.Assign(AValue);
  UpdateItems;
end;

procedure TCustomDemoCombobox.DoChange;
begin
  if Assigned(OnChange) then
    OnChange(Self);
end;

procedure TCustomDemoCombobox.SetName(const NewName: TComponentName);
var
  WasCaption: Boolean;
begin
  WasCaption:=Caption=Name;
  inherited SetName(NewName);
  if WasCaption then
    Caption:=NewName;
end;

procedure TCustomDemoCombobox.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if AComponent=CaptionDiv then
      CaptionDiv:=nil;
    if AComponent=CaptionLabel then
      CaptionLabel:=nil;
    if AComponent=CaptionIcon then
      CaptionIcon:=nil;
    if AComponent=Content then
      Content:=nil;
    if AComponent=Menu then
      Menu:=nil;
  end;
end;

procedure TCustomDemoCombobox.Loaded;
begin
  inherited Loaded;
  if FItemIndex>=Content.NodeCount then
    FItemIndex:=-1;
  UpdateCaption;
  if (FItemIndex>=0) then
    Content.Nodes[FItemIndex].AddCSSClass('ComboboxSelected');
  UpdateCaption;
end;

procedure TCustomDemoCombobox.FPOObservedChanged(ASender: TObject; Operation: TFPObservedOperation;
  Data: Pointer);
begin
  if ASender=FItems then
  begin
    UpdateItems;
  end;
end;

procedure TCustomDemoCombobox.SetItemIndex(const AValue: integer);
begin
  if FItemIndex=AValue then Exit;
  if (csDestroying in ComponentState) or (Content=nil) then exit;
  if csLoading in ComponentState then
  begin
    FItemIndex:=AValue;
    exit;
  end;
  if (AValue>=Content.NodeCount) then
    exit;
  if (FItemIndex>=0) then
    Content.Nodes[FItemIndex].RemoveCSSClass('ComboboxSelected');
  FItemIndex:=AValue;
  if (FItemIndex>=0) then
    Content.Nodes[FItemIndex].AddCSSClass('ComboboxSelected');
  UpdateCaption;
end;

function TCustomDemoCombobox.GetCaption: string;
begin
  if CaptionLabel<>nil then
    Result:=CaptionLabel.Caption
  else
    Result:='';
end;

procedure TCustomDemoCombobox.DoItemClick(Event: TAbstractEvent);
var
  Item: TFresnelElement;
begin
  Item:=Event.Sender as TFresnelElement;
  while Item.Parent<>Content do
    Item:=Item.Parent;
  if ItemIndex<>Item.Tag then
  begin
    ItemIndex:=Item.Tag;
    DoChange;
  end;
  Menu.SetStyleAttr('display','none');
end;

procedure TCustomDemoCombobox.DoCaptionDivClick(Event: TAbstractEvent);
begin
  if Event=nil then ;
  if Menu.GetStyleAttr('display')='none' then
    Menu.SetStyleAttr('display','')
  else
    Menu.SetStyleAttr('display','none');
end;

procedure TCustomDemoCombobox.SetCaption(const AValue: string);
begin
  if CaptionLabel<>nil then
    CaptionLabel.Caption:=AValue;
end;

constructor TCustomDemoCombobox.Create(AOwner: TComponent);
var
  aStream: TMemoryStream;
begin
  inherited Create(AOwner);
  FItemIndex:=-1;
  FItems:=TStringList.Create;
  FItems.FPOAttachObserver(Self);

  CSSClasses.Add('Combobox');

  CaptionDiv:=TDiv.Create(Self);
  with CaptionDiv do begin
    Name:='CaptionDiv';
    CSSClasses.Add('ComboboxCaption');
    Parent:=Self;
    AddEventListener(evtClick,@DoCaptionDivClick);
  end;

  CaptionLabel:=TLabel.Create(Self);
  with CaptionLabel do begin
    Name:='CaptionLabel';
    Caption:=Self.Name;
    CSSClasses.Add('ComboboxCaptionLabel');
    Parent:=CaptionDiv;
  end;

  CaptionIcon:=TImage.Create(Self);
  with CaptionIcon do begin
    Name:='CaptionIcon';
    //Caption:=#$E2#$8C#$84; // Down Arrowhead ⌄ , needs font that has it
    CSSClasses.Add('ComboboxCaptionIcon');
    Parent:=CaptionDiv;
  end;
  aStream:=TMemoryStream.Create;
  try
    aStream.WriteBuffer(ArrowDownImage[1],length(ArrowDownImage));
    aStream.Position:=0;
    CaptionIcon.Image.LoadFromStream(aStream);
  finally
    aStream.Free;
  end;

  Menu:=TDiv.Create(Self);
  with Menu do begin
    Name:='Menu';
    CSSClasses.Add('ComboboxMenu');
    SetStyleAttr('display','none');
    Parent:=Self;
  end;

  Content:=TDiv.Create(Self);
  with Content do begin
    Name:='Content';
    CSSClasses.Add('ComboboxContent');
    Parent:=Menu;
  end;
end;

destructor TCustomDemoCombobox.Destroy;
begin
  FItemIndex:=-1;
  FreeAndNil(FItems);
  inherited Destroy;
end;

procedure TCustomDemoCombobox.Clear;
begin
  FItemIndex:=-1;
  FItems.Clear;
  while Content.NodeCount>FItems.Count do
    Content.Nodes[Content.NodeCount-1].Free;
  SetCaption(' ');
end;

class function TCustomDemoCombobox.GetCSSTypeStyle: TCSSString;
begin
  Result:=cStyle;
end;

procedure TCustomDemoCombobox.UpdateItems;
var
  i: Integer;
  ItemCaption: String;
  ItemDiv: TDiv;
  aLabel: TLabel;
begin
  if FItemIndex>FItems.Count then
    FItemIndex:=-1;

  for i:=0 to FItems.Count-1 do
  begin
    ItemCaption:=FItems[i];
    if i>=Content.NodeCount then
    begin
      ItemDiv:=TDiv.Create(Self);
      ItemDiv.Name:='ItemDiv'+IntToStr(i);
      ItemDiv.CSSClasses.Add('ComboboxItem');
      ItemDiv.Tag:=i;

      aLabel:=TLabel.Create(Self);
      aLabel.Name:='ItemLabel'+IntToStr(i);
      aLabel.Caption:=ItemCaption;
      aLabel.Parent:=ItemDiv;
      ItemDiv.Parent:=Content;

      ItemDiv.AddEventListener(evtClick,@DoItemClick);
    end else begin
      ItemDiv:=Content.Nodes[i] as TDiv;
      aLabel:=ItemDiv.Nodes[0] as TLabel;
      aLabel.Caption:=ItemCaption;
    end;
    if i=ItemIndex then
      ItemDiv.AddCSSClass('ComboboxSelected')
    else
      ItemDiv.RemoveCSSClass('ComboboxSelected');
  end;
  while Content.NodeCount>FItems.Count do
    Content.Nodes[Content.NodeCount-1].Free;
  UpdateCaption;
end;

procedure TCustomDemoCombobox.UpdateCaption;
begin
  if (FItemIndex>=0) then
    SetCaption(FItems[FItemIndex])
  else
    SetCaption(' ');
end;

end.

