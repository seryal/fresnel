unit DemoButtonGenerator;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.DOM, Fresnel.Controls, Fresnel.Classes, fpCSSTree, Fresnel.DemoSlider,
  Fresnel.DemoCheckBox;

type

  { TDemoButtonGenerator }

  TDemoButtonGenerator = class(TDiv)
  private
    procedure OnPropChange(Sender: TObject);
  public
    const
      BackgroundColor1 ='#79bbff';
      BackgroundColor2 ='#378de5';
      BorderColor ='#337bc4';
      cStyle =
        '#ButtonDiv {'+LineEnding
        +'background:'+BackgroundColor1+';'+LineEnding
        +'border:1px solid '+BorderColor+';'+LineEnding
        +'padding:16px 31px;'+LineEnding
        +'font-size:15px; font-family:Arial; font-weight:bold;'+LineEnding
        +'color:#fff;'+LineEnding
        +'}'+LineEnding
        +'.CheckBox {'+LineEnding
        +'  margin: 6px;'+LineEnding
        +'}'+LineEnding
        +'.Slider {'+LineEnding
        +'  margin: 6px;'+LineEnding
        +'  width: 150px;'+LineEnding
        +'}'+LineEnding;
    var
    ButtonDiv: TDiv;
    ButtonLabel: TLabel;
    FontSizeSlider: TDemoSlider;
    PaddingVerticalSlider: TDemoSlider;
    GradientChkBox: TDemoCheckBox;
    BorderWidthSlider: TDemoSlider;
    BorderRadiusDiv: TDiv;
    BorderRadiusLabel: TLabel;
    BorderRadiusTopLeftChkBox: TDemoCheckBox;
    BorderRadiusTopRightChkBox: TDemoCheckBox;
    BorderRadiusBottomLeftChkBox: TDemoCheckBox;
    BorderRadiusBottomRightChkBox: TDemoCheckBox;
    BorderRadiusSlider: TDemoSlider;
    TextShadowChkBox: TDemoCheckBox;
    TextShadowVertPosSlider: TDemoSlider;
    TextShadowHorzPosSlider: TDemoSlider;
    TextShadowBlurRadiusSlider: TDemoSlider;
    constructor Create(AOwner: TComponent); override;
    class function GetCSSTypeStyle: TCSSString; override;
    procedure UpdateButton;
  end;

implementation

{ TDemoButtonGenerator }

procedure TDemoButtonGenerator.OnPropChange(Sender: TObject);
begin
  UpdateButton;
end;

constructor TDemoButtonGenerator.Create(AOwner: TComponent);

  function AddSlider(aName, aCaption: string; aParent: TFresnelElement = nil): TDemoSlider;
  begin
    if aParent=nil then aParent:=Self;
    Result:=TDemoSlider.Create(Self);
    with Result do begin
      Name:=aName;
      Caption:=aCaption;
      ValueFormat:='%.0fpx';
      Parent:=aParent;
      OnChange:=@OnPropChange;
    end;
  end;

  function AddCheckBox(aName, aCaption: string; aParent: TFresnelElement = nil): TDemoCheckBox;
  begin
    if aParent=nil then aParent:=Self;
    Result:=TDemoCheckBox.Create(Self);
    with Result do begin
      Name:=aName;
      Caption:=aCaption;
      Checked:=true;
      Parent:=aParent;
      OnChange:=@OnPropChange;
    end;
  end;

begin
  inherited Create(AOwner);

  // font-size slider
  FontSizeSlider:=AddSlider('FontSizeSlider','Font Size:');
  with FontSizeSlider do begin
    MinPosition:=5;
    MaxPosition:=40;
    SliderPosition:=12;
  end;

  // padding vertical slider
  PaddingVerticalSlider:=AddSlider('PaddingVerticalSlider','Vertical Padding:');
  with PaddingVerticalSlider do begin
    MinPosition:=0;
    MaxPosition:=50;
    SliderPosition:=16;
  end;

  // gradient
  GradientChkBox:=AddCheckBox('GradientChkBox','Gradient');

  // border-width slider
  BorderWidthSlider:=AddSlider('BorderWidthSlider','Border Width:');
  with BorderWidthSlider do begin
    MinPosition:=0;
    MaxPosition:=10;
    SliderPosition:=1;
  end;

  // border-radius corners: four checkboxes
  BorderRadiusDiv:=TDiv.Create(Self);
  with BorderRadiusDiv do begin
    Name:='BorderRadiusDiv';
    Style:='margin: 6px;';
    Parent:=Self;
  end;

  BorderRadiusLabel:=TLabel.Create(Self);
  with BorderRadiusLabel do begin
    Name:='BorderRadiusLabel';
    Caption:='Border Corner Radius:';
    Parent:=BorderRadiusDiv;
  end;

  BorderRadiusTopLeftChkBox:=AddCheckBox('BorderRadiusTopLeftChkBox','Top Left',BorderRadiusDiv);

  BorderRadiusTopRightChkBox:=AddCheckBox('BorderRadiusTopRightChkBox','Top Right',BorderRadiusDiv);

  BorderRadiusBottomLeftChkBox:=AddCheckBox('BorderRadiusBottomLeftChkBox','Bottom Left',BorderRadiusDiv);

  BorderRadiusBottomRightChkBox:=AddCheckBox('BorderRadiusBottomRightChkBox','Bottom Right',BorderRadiusDiv);

  // slider for border radius
  BorderRadiusSlider:=AddSlider('BorderRadiusSlider','Border Radius:');
  with BorderRadiusSlider do begin
    MinPosition:=0;
    MaxPosition:=50;
    SliderPosition:=16;
  end;

  // text-shadow checkbox
  TextShadowChkBox:=AddCheckBox('TextShadowChkBox','Text Shadow');

  // slider for text-shadow vertical position
  TextShadowVertPosSlider:=AddSlider('TextShadowVertPosSlider','Vertical Position:');
  with TextShadowVertPosSlider do begin
    MinPosition:=-30;
    MaxPosition:=30;
    SliderPosition:=1;
  end;

  // slider for text-shadow horizontal position
  TextShadowHorzPosSlider:=AddSlider('TextShadowHorzPosSlider','Horizontal Position:');
  with TextShadowHorzPosSlider do begin
    MinPosition:=-30;
    MaxPosition:=30;
    SliderPosition:=1;
  end;

  // slider for text-shadow horizontal position
  TextShadowBlurRadiusSlider:=AddSlider('TextShadowBlurRadiusSlider','Blur Radius:');
  with TextShadowBlurRadiusSlider do begin
    ValueFormat:='%.1fpx';
    MinPosition:=0;
    MaxPosition:=5;
    SliderPosition:=0;
  end;

  // the Button
  ButtonDiv:=TDiv.Create(Self);
  with ButtonDiv do begin
    Name:='ButtonDiv';
    Parent:=Self;
  end;
  ButtonLabel:=TLabel.Create(Self);
  with ButtonLabel do begin
    Name:='ButtonLabel';
    Caption:='Button Text';
    Parent:=ButtonDiv;
  end;

  UpdateButton;
end;

class function TDemoButtonGenerator.GetCSSTypeStyle: TCSSString;
begin
  Result:=cStyle;
end;

procedure TDemoButtonGenerator.UpdateButton;
var
  s, NewStyle, Radius: String;
begin
  if ButtonDiv=nil then exit;
  NewStyle:=
     'font-size:'+FloatToCSSPx(FontSizeSlider.SliderPosition)+';'
    +'padding: '+FloatToCSSPx(PaddingVerticalSlider.SliderPosition)+' 31px;'
    +'border-width:'+FloatToCSSPx(BorderWidthSlider.SliderPosition)+';';

  if GradientChkBox.Checked then
    NewStyle+='background-image:linear-gradient('+BackgroundColor1+','+BackgroundColor2+');';

  Radius:=FloatToCSSPx(BorderRadiusSlider.SliderPosition);
  s:='';
  if BorderRadiusTopLeftChkBox.Checked then
    s:=Radius
  else
    s:='0';
  if BorderRadiusTopRightChkBox.Checked then
    s+=' '+Radius
  else
    s+=' 0';
  if BorderRadiusBottomLeftChkBox.Checked then
    s+=' '+Radius
  else
    s+=' 0';
  if BorderRadiusBottomRightChkBox.Checked then
    s+=' '+Radius
  else
    s+=' 0';
  NewStyle+='border-radius:'+s+';';

  s:='';
  if TextShadowChkBox.Checked then
  begin
    s:=FloatToCSSStr(TextShadowHorzPosSlider.SliderPosition)
      +' '+FloatToCSSStr(TextShadowVertPosSlider.SliderPosition)
      +' '+FloatToCSSStr(TextShadowBlurRadiusSlider.SliderPosition)
      +' #2f6627';
  end;
  NewStyle+='text-shadow:'+s+';';

  ButtonDiv.Style:=NewStyle;
end;

end.

