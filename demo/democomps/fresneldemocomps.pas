{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FresnelDemoComps;

{$warn 5023 off : no warning about unused units}
interface

uses
  Fresnel.DemoCombobox, Fresnel.DemoCheckbox, Fresnel.DemoSlider, DemoButtonGenerator, 
  LazarusPackageIntf;

implementation

procedure Register;
begin
end;

initialization
  RegisterPackage('FresnelDemoComps', @Register);
end.
