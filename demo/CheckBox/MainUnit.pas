unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.DOM, Fresnel.Controls, Fresnel.DemoCheckbox;

type

  { TFresnelCheckBoxForm }

  TFresnelCheckBoxForm = class(TFresnelForm)
    procedure FresnelCheckBoxFormCreate(Sender: TObject);
  private
  public
  end;

var
  FresnelCheckBoxForm: TFresnelCheckBoxForm;

implementation

{$R *.lfm}

{ TFresnelCheckBoxForm }

procedure TFresnelCheckBoxForm.FresnelCheckBoxFormCreate(Sender: TObject);
var
  CheckBox: TDemoCheckBox;
begin
  CheckBox:=TDemoCheckBox.Create(Self);
  with CheckBox do begin
    Name:='CheckBox';
    Style:='width: 150px';
    Caption:='CheckBox Text';
    Parent:=Self;
  end;
end;

end.

