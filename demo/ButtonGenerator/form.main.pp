unit form.main;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.DOM, Fresnel.Controls,
  DemoButtonGenerator;

type

  { TFresnelForm1 }

  TFresnelForm1 = class(TFresnelForm)
  public
    constructor createNew(aOwner : TComponent); override;
  end;

var
  FresnelForm1: TFresnelForm1;

implementation


{ TFresnelForm1 }

constructor TFresnelForm1.createNew(aOwner : TComponent);
var
  ButtonGenerator: TDemoButtonGenerator;
begin
  Inherited;
  Stylesheet.Add(TDemoButtonGenerator.cStyle);
  ButtonGenerator:=TDemoButtonGenerator.Create(Self);
  with ButtonGenerator do begin
    Name:='ButtonGenerator';
    Parent:=Self;
  end;
end;

end.

