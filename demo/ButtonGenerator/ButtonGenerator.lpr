program ButtonGenerator;

{$mode objfpc}{$H+}

{$IFDEF Windows}
  {$AppType Console}
{$ENDIF}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Fresnel, // this includes the Fresnel widgetset
  Fresnel.Forms, MainUnit;

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFresnelForm1, FresnelForm1);
  Application.Run;
end.

