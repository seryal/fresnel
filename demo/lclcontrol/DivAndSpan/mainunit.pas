unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls,
  ExtCtrls, Fresnel.Controls, Fresnel.DOM, Fresnel.Events, fcl.Events,
  Fresnel.LCLControls;

type
  THookEvent = (heClick,heMouseMove,heMouseUp,heMouseDown,heMouseEnter,heMouseLeave,heFocus,heFocusIn,heFocusOut);
  THookEvents = set of THookEvent;

  { TMainForm }

  TMainForm = class(TForm)
    CGEVents: TCheckGroup;
    CBUsePublished: TCheckBox;
    MLog: TMemo;
    PageControl1: TPageControl;
    pnlEventOptions: TPanel;
    TSEvents: TTabSheet;
    TSLog: TTabSheet;
    procedure CGEventsItemClick(Sender: TObject; {%H-}Index: integer);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    FHookEvents : THookEvents;
    Fresnel1: TFresnelLCLControl;
    procedure DisplayEventsTohook;
    procedure DoAdditionalClickEvent(Event: TAbstractEvent);
    procedure DoClick(Event: TAbstractEvent);
    procedure DoFresnelLog(aType: TEventType; const Msg: UTF8String);
    procedure DoGeneralEvent(Event: TAbstractEvent);
    procedure DoLog(const Msg: String);
    procedure DoLog(const Fmt: String; Args: array of const);
    procedure DoMouseMove(Event: TFresnelMouseEvent);
    procedure HookAllFresnelComponents;
    procedure HookEvents(aEl: TFresnelELement; Publ: Boolean);
    procedure LogEventData(Event: TAbstractEvent);
    procedure LogMouseEvent(Event: TFresnelMouseEvent; LogData: Boolean);
  public

  end;

var
  MainForm: TMainForm;

const
  HookEventNames : Array [THookEvent] of string
    = ('Click','MouseMove','MouseUp','MouseDown','MouseEnter','MouseLeave','Focus','FocusIn','FocusOut');

implementation


uses
  {$IFDEF USE_DEBUGMSG}
  dbugintf,
  {$ENDIF}
  Fresnel.Classes, TypInfo;

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);
var
  Body1: TBody;
  Div1, Div2: TDiv;
  Label1: TLabel;
  Img1 : TImage;
  ViewPort: TFresnelViewport;
  Span1: TSpan;
  HE : THookEvent;

begin
  {$IFDEF USE_DEBUGMSG}
  RaiseExceptionOnSendError:=False;
  {$ENDIF}
  TFresnelComponent._LogHook:=@DoFresnelLog;
  With CGEvents.Items do
    begin
      BeginUpdate;
      For HE in THookEvent do
        Add(HookEventNames[HE]);
      EndUpdate;
    end;
  Fresnel1:=TFresnelLCLControl.Create(Self);
  with Fresnel1 do
  begin
    Name:='Fresnel1';
    Align:=alClient;
    Viewport.Stylesheet.Text:='div { padding: 2px; border: 3px; margin: 6px; }';
    Parent:=Self;
  end;
  ViewPort:=Fresnel1.Viewport;

  Body1:=TBody.Create(Self);
  with Body1 do begin
    Name:='Body1';
    Parent:=ViewPort;
    Style:='border: 2px; border-color: blue;';
  end;

  Div1:=TDiv.Create(Self);
  with Div1 do begin
    Name:='Div1';
    Parent:=Body1;
    Style:='background-color: blue; border-color: black; height:50px;';
  end;

  Span1:=TSpan.Create(Self);
  with Span1 do begin
    Name:='Span1';
    Parent:=Body1;
    Style:='width: 50px; height:70px; background-color: red; border: 3px; border-color: black; margin: 3px;';
  end;

  Label1:=TLabel.Create(Self);
  with Label1 do
  begin
    Name:='Label1';
    Caption:='Label1Caption';
    Parent:=Body1;
    Style:='background-color: green; ';
  end;

  Div2:=TDiv.Create(Self);
  with Div2 do begin
    Name:='Div2';
    Parent:=Body1;
    Style:='border-color: black; height:50px; position: absolute; left: 30px; top: 100px; width: 50px; height: 60px;';
  end;

  Img1:=TImage.Create(Self);
  with Img1 do begin
    Name:='Img1';
    Parent:=Body1;
    Style:='border-color: red; border: 3px, height:50px; position: absolute; left: 150px; top: 200px; width: 48px; height: 48px;';
    Image.LoadFromFile('image.png');
  end;

  FHookEvents:=[heClick];
  DisplayEventsToHook;
  HookAllFresnelComponents;
end;

procedure TMainForm.FormShow(Sender: TObject);
var
  P : TPoint;
begin
  P:=ClientToScreen(Point(0,0));
  DoLog('Form is at screen origin (%d,%d) - (%d,%d)',[Left,Top,P.X,P.Y]);
  P:=Fresnel1.ClientToScreen(Point(0,0));
  With P do
    DoLog('LCLFresnelcontrol is at screen origin (%d,%d)',[X,Y]);

end;

procedure TMainForm.CGEventsItemClick(Sender: TObject; Index: integer);
Var
  HE: THookEvent;
  Events : THookEvents;
begin
  EVents:=[];
  For HE in THookEvent do
    if CGEVents.Checked[Ord(He)] then
      Include(Events,HE);
  FHookEvents:=Events;
  HookAllFresnelComponents;
end;

procedure TMainForm.FormActivate(Sender: TObject);
var
  P : TPoint;
begin
  P:=ClientToScreen(Point(0,0));
  DoLog('Form is at screen origin (%d,%d) - (%d,%d)',[Left,Top,P.X,P.Y]);
  P:=Fresnel1.ClientToScreen(Point(0,0));
  With P do
    DoLog('LCLFresnelcontrol is at screen origin (%d,%d)',[X,Y]);
end;

procedure TMainForm.DisplayEventsTohook;

Var
  HE: THookEvent;

begin
  For HE in THookEvent do
    CGEVents.Checked[Ord(He)]:=HE in FHookEvents;
end;

procedure TMainForm.HookAllFresnelComponents;

Var
  C : TComponent;
  I : Integer;

begin
  HookEvents(Fresnel1.Viewport,CBUsePublished.Checked);
  For I:=0 to ComponentCount-1 do
    begin
    C:=Components[I];
    if C is TFresnelElement then
      HookEvents(C as TFresnelElement,CBUsePublished.Checked);
    end;
end;

procedure TMainForm.DoLog(Const Fmt : String; Args : Array of const);
begin
  DoLog(SafeFormat(Fmt,Args));
end;

procedure TMainForm.DoMouseMove(Event: TFresnelMouseEvent);

begin
  LogMouseEvent(Event,True)
end;

procedure TMainForm.LogMouseEvent(Event: TFresnelMouseEvent; LogData : Boolean);

var
  Btn,Btns : String;
begin
  If LogData then
    LogEventData(Event);
  Btn:=GetEnumName(TypeInfo(TMouseButton),Ord(Event.Button)) ;
  Btns:=SetToString(PTypeInfo(TypeInfo(TMouseButtons)),Longint(EVent.Buttons),True);
  DoLog('Mouse Event (X: %f, Y: %f, Button: %s, Buttons: %s) ', [Event.ControlX,Event.ControlY,Btn,Btns]);
end;

procedure TMainForm.DoLog(const Msg : String);
begin
  MLog.Lines.Add(Msg);
  {$IFDEF USE_DEBUGMSG}
  SendDebug(Msg);
  {$ENDIF}
end;

procedure TMainForm.DoAdditionalClickEvent(Event: TAbstractEvent);
begin
  DoLog('We repeat: You clicked '+(Event.sender as TComponent).Name);
end;

procedure TMainForm.DoClick(Event: TAbstractEvent);
begin
  DoLog('You clicked '+(Event.Sender as TComponent).Name);
end;

procedure TMainForm.DoFresnelLog(aType: TEventType; const Msg: UTF8String);

var
  S : String;

begin
  Str(aType,S);
  DoLog('Fresnel log [%s] : %s',[S,Msg]);
end;

procedure TMainForm.DoGeneralEvent(Event: TAbstractEvent);
begin
  LogEventData(Event);
  If Event is TFresnelMouseEvent then
    LogMouseEvent(Event as TFresnelMouseEvent,False);
end;

procedure TMainForm.LogEventData(Event: TAbstractEvent);

var
//  E : TFresnelEvent absolute Event;
//  EUI : TFresnelUIEvent absolute Event;
  S : String;

begin
  if Event.Sender=Nil then
    S:='(Nil)'
  else
    begin
    S:=Event.Sender.ClassName;
    if Event.Sender is TComponent then
      S:=TComponent(EVent.Sender).Name+' ('+S+')';
    end;
  DoLog('Event class %s type: %s, sender : %s',[Event.ClassName, Event.EventName, S]);
end;

procedure TMainForm.HookEvents(aEl: TFresnelELement; Publ : Boolean);

begin
  DoLog('Hooking events for %s',[aEl.Name]);
  if heClick in FHookEvents then
    begin
    if Publ then
      aEl.OnClick:=@DoClick
    else
      aEl.AddEventListener('click',@DoGeneralEvent);
    DoLog(aEl.Name+'.OnClick');
    end;
  if heMouseMove in FHookEvents then
    if Publ then
      aEl.OnMouseMove:=@DoMouseMove
    else
      aEl.AddEventListener('mousemove',@DoGeneralEvent);
  if heMouseEnter in FHookEvents then
    if Publ then
      aEl.OnMouseEnter:=@DoMouseMove
    else
      aEl.AddEventListener('mouseenter',@DoGeneralEvent);
  if heMouseLeave in FHookEvents then
    if Publ then
      aEl.OnMouseLeave:=@DoMouseMove
    else
      aEl.AddEventListener('mousemove',@DoGeneralEvent);
  if heFocus in FHookEvents then
    aEl.AddEventListener('focus',@DoGeneralEvent);
  if heFocus in FHookEvents then
    aEl.AddEventListener('focus',@DoGeneralEvent);
end;

end.

