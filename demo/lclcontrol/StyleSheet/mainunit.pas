unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, TypInfo, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls, EditBtn, Fresnel.Controls, Fresnel.DOM, Fresnel.Events, fcl.Events,
  Fresnel.LCLControls;

type

  { TMainForm }

  TMainForm = class(TForm)
    FECSS: TFileNameEdit;
    LblStyleSheet: StdCtrls.TLabel;
    Panel1: TPanel;
    procedure FECSSEditingDone(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    Body1: TBody;
    Div1, Div2: TDiv;
    Img1 : TImage;
    Span1: TSpan;
    Fresnel1: TFresnelLCLControl;
    Label1: Fresnel.Controls.TLabel;
    procedure CreateControls(ViewPort: TFresnelViewport);
  public
  end;

var
  MainForm: TMainForm;

implementation

{$IFDEF USE_DEBUGMSG}
uses
  dbugintf;
{$ENDIF}

{$R *.lfm}

{ TMainForm }

procedure TMainForm.FormCreate(Sender: TObject);

begin
  {$IFDEF USE_DEBUGMSG}
  RaiseExceptionOnSendError:=False;
  {$ENDIF}
  Fresnel1:=TFresnelLCLControl.Create(Self);
  with Fresnel1 do
  begin
    Name:='Fresnel1';
    Align:=alClient;
    Viewport.Stylesheet.LoadFromFile('style1.css');
    Parent:=Self;
  end;
  CreateControls(Fresnel1.Viewport);
end;

Procedure TMainForm.CreateControls(ViewPort : TFresnelViewport);

  Function CreateControl(aClass : TFresnelElementClass;
                         aName : String;
                         aParent : TFresnelElement = nil) : TFresnelElement;
  begin
    if aParent=Nil then
      aparent:=Body1;
    Result:=aClass.Create(Self);
    Result.Name:=aName;
    Result.parent:=aParent;
  end;

begin
  Body1:=TBody(CreateControl(TBody,'Body1',ViewPort));
  Div1:=TDiv(CreateControl(TDiv,'Div1'));
  Span1:=TSpan(CreateControl(TSpan,'Span1'));
  Label1:=TLabel(CreateControl(TLabel,'Label1'));
  Label1.Caption:='Label1Caption';
  Div2:=TDiv(CreateControl(TDiv,'Div2'));
  Img1:=TImage(CreateControl(TImage,'Img1'));
  Img1.Image.LoadFromFile('image.png');
end;

procedure TMainForm.FECSSEditingDone(Sender: TObject);

var
  L : TStrings;

begin
  if FECSS.FileName<>'' then
    begin
    L:=TstringList.Create;
    L.LoadFromFile(FECSS.FileName);
    Fresnel1.ViewPort.Stylesheet:=L;
    L.Free;
    Fresnel1.ViewPort.ApplyCSS;
    end;
end;

end.

