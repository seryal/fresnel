unit MainUnit;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.Controls, Fresnel.Events,
  FCL.Events;

type

  { TMainForm }

  TMainForm = class(TFresnelForm)
    Body1: TBody;
    Div1: TDiv;
    Label1: TLabel;
    procedure Div1MouseDown(Event: TFresnelMouseEvent);
    procedure Div1MouseMove(Event: TFresnelMouseEvent);
    procedure Label1Click(Event: TAbstractEvent);
    procedure Label1MouseDown(Event: TFresnelMouseEvent);
    procedure Label1MouseMove(Event: TFresnelMouseEvent);
    procedure Label1MouseUp(Event: TFresnelMouseEvent);
    procedure MainFormCreate(Sender: TObject);
  private
  public
  end;

var
  MainForm: TMainForm;

implementation

{$R *.lfm}

{ TMainForm }

procedure TMainForm.Label1Click(Event: TAbstractEvent);
begin
  writeln('TMainForm.Label1Click ',Event.EventID);
end;

procedure TMainForm.Div1MouseDown(Event: TFresnelMouseEvent);
begin
  writeln('TMainForm.Div1MouseDown ',Event.ControlX,',',Event.ControlY);
end;

procedure TMainForm.Div1MouseMove(Event: TFresnelMouseEvent);
begin
  writeln('TMainForm.Div1MouseMove ',Event.ControlX,',',Event.ControlY);
end;

procedure TMainForm.Label1MouseDown(Event: TFresnelMouseEvent);
begin
  writeln('TMainForm.Label1MouseDown ',Event.ControlX,',',Event.ControlY);
end;

procedure TMainForm.Label1MouseMove(Event: TFresnelMouseEvent);
begin
  writeln('TMainForm.Label1MouseMove ',Event.ControlX,',',Event.ControlY);
end;

procedure TMainForm.Label1MouseUp(Event: TFresnelMouseEvent);
begin
  writeln('TMainForm.Label1MouseUp ',Event.ControlX,',',Event.ControlY);
end;

procedure TMainForm.MainFormCreate(Sender: TObject);
begin
  Stylesheet.Add('div {'
    //+'  background:#44cc66;'
    +'  background:linear-gradient(#ededed, #bab1ba);'
    +'  border:7px solid #18ab29;'
    +'  padding:16px 31px;'
    +'  font-size:15px; font-family:Arial; font-weight:bold;'
    +'  text-shadow: 0 1 1 #333;'
    +'  color:#fff;'
    +'}'
    +'div:hover {'
    +'  background:#88bb22;'
    +'}');
  Div1.Style:='';
end;

end.

