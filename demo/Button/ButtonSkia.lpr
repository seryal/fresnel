program ButtonSkia;

uses
  {$IFDEF Unix}
  cthreads,
  {$ENDIF}
  Fresnel, // initializes the widgetset
  Fresnel.App, MainUnit;

begin
  FresnelApplication.HookFresnelLog:=true;
  FresnelApplication.Initialize;
  FresnelApplication.CreateForm(TMainForm,MainForm);
  FresnelApplication.Run;
end.

