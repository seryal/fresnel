unit frmedit;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.Controls, Fresnel.Events,
  FCL.Events, Fresnel.Edit, Fresnel.Classes;

type

  { TEditForm }

  TEditForm = class(TFresnelForm)
    Body1: TBody;
    Div1: TDiv;
    Label1: TLabel;
    Edit1 : TEdit;
    procedure Label1Click(Event: TAbstractEvent);
  private
    FBlocked : Boolean;
  public
    constructor CreateNew(aOwner: TComponent); override;
  end;

var
  EditForm: TEditForm;

implementation

{ TEditForm }

procedure TEditForm.Label1Click(Event: TAbstractEvent);
begin
  FBlocked:=Not FBlocked;
  if FBlocked then
    Label1.Caption:='Allow typing'
  else
    Label1.Caption:='Block typing';
end;

constructor TEditForm.CreateNew(aOwner: TComponent);
begin
  inherited CreateNew(aOWner);

  Caption:='Edit demo';
  FormBounds:=BoundsRectFre(450,300,350,255);
  Stylesheet.Text:=
    'div {'+
    '  padding: 3px; '+
    '  border: 2px solid black; '+
    '  margin: 6px;'+
    '}';

  Body1:=TBody.Create(Self);
  Body1.Parent:=Self;
  Body1.Style:='border: 2px solid blue;'#10;
  Body1.Name:='Body1';

  Div1:=TDiv.Create(Self);
  Div1.Parent:=Body1;
  Div1.Style:='background-color: blue;'#10'border-color: black;'#10'height:30px;';
  Div1.Name:='Div1';

  Label1:=TLabel.Create(Self);
  Label1.Parent:=Div1;
  With Label1 do
    begin
    Name:='Label1';
    Style := 'color: red;';
    Caption := 'Block typing';
    OnClick := @Label1Click;
    end;
  Stylesheet.Add('div {'
    //+'  background:#44cc66;'
    +'  background:linear-gradient(#ededed, #bab1ba);'
    +'  border:7px solid #18ab29;'
    +'  padding:16px 31px;'
    +'  font-size:15px; '
    +'  font-family:Arial; '
    + ' font-weight:bold;'
    +'  text-shadow: 0 1 1 #333;'
    +'  color:#fff;'
    +'}'
    +'div:hover {'
    +'  background:#88bb22;'
    +'};');

  Edit1:=TEdit.Create(Self);
  Edit1.Name:='Edit1';
  Edit1.Value:='Edit1';
  Edit1.Parent:=Body1;
  Edit1.Style:='font-size:15px; family:Arial; background-color: yellow;'#10
     +'border-color: black;'#10; // 'height:30px;width: 100px;';
  Edit1.Focus;
  Caption:='Fresnel Edit Demo';
end;

end.
