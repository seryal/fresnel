{
  This is the HostForm of the LCL application.
  Once it is shown, it automatically opens the fresnel form.
}
unit FrmHost;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, frmEdit;

type

  { THostForm }

  THostForm = class(TForm)
    procedure FormPaint(Sender: TObject);
  private
    FQueued: boolean;
    procedure ShowFresnelForm({%H-}Data: PtrInt);
  public
  end;

var
  HostForm: THostForm;

implementation

uses Fresnel.LCL;

{$R *.lfm}

{ THostForm }

procedure THostForm.FormPaint(Sender: TObject);
begin
  if FQueued then exit;
  FQueued:=true;
  Application.QueueAsyncCall(@ShowFresnelForm,0)
end;

procedure THostForm.ShowFresnelForm(Data: PtrInt);
begin
  EditForm:=TEditForm.CreateNew(Self);
  EditForm.Show;
end;

end.

