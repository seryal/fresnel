program EditDemo;

uses
  {$IFDEF Unix}
  cthreads,
  {$ENDIF}
  Forms,
  Interfaces,
  Fresnel,
  frmedit,
  frmhost;

begin
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(THostForm,HostForm);
  Application.Run;
end.

