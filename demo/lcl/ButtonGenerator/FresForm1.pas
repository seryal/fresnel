unit FresForm1;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.DOM,
    Fresnel.Controls, DemoButtonGenerator;

type

  { TFresnelForm1 }

  TFresnelForm1 = class(TFresnelForm)
    procedure FresnelForm1Create(Sender: TObject);
  private
  public
    ButtonGenerator: TDemoButtonGenerator;
  end;

var
  FresnelForm1: TFresnelForm1;

implementation

{$R *.lfm}

{ TFresnelForm1 }

procedure TFresnelForm1.FresnelForm1Create(Sender: TObject);
begin
  ButtonGenerator:=TDemoButtonGenerator.Create(Self);
  with ButtonGenerator do begin
    Name:='ButtonGenerator';
    Parent:=Self;
  end;
end;

end.

