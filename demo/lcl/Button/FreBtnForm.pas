{
  A Fresnel form inside an LCL application.
  It requires the project (lpr) to use unit Fresnel from package FresnelLCL
  in order to initialize the Fresnel-LCL widgetset.
}
unit FreBtnForm;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.Controls, Fresnel.Events,
  FCL.Events, LazLogger;

type

  { TFresnelButtonForm }

  TFresnelButtonForm = class(TFresnelForm)
    Body1: TBody;
    Div1: TDiv;
    Label1: TLabel;
    procedure Label1Click(Event: TAbstractEvent);
    procedure Label1MouseDown(Event: TFresnelMouseEvent);
    procedure Label1MouseMove(Event: TFresnelMouseEvent);
    procedure Label1MouseUp(Event: TFresnelMouseEvent);
  public
  end;

var
  FresnelButtonForm: TFresnelButtonForm;

implementation

{$R *.lfm}

{ TFresnelButtonForm }

procedure TFresnelButtonForm.Label1Click(Event: TAbstractEvent);
begin
  DebugLn(['TFresnelButtonForm.Label1Click ',Event.EventID]);
end;

procedure TFresnelButtonForm.Label1MouseDown(Event: TFresnelMouseEvent);
begin
  debugln(['TFresnelButtonForm.Label1MouseDown ',Event.ControlX,',',Event.ControlY]);
end;

procedure TFresnelButtonForm.Label1MouseMove(Event: TFresnelMouseEvent);
begin
  debugln(['TFresnelButtonForm.Label1MouseMove ',Event.ControlX,',',Event.ControlY]);
end;

procedure TFresnelButtonForm.Label1MouseUp(Event: TFresnelMouseEvent);
begin
  debugln(['TFresnelButtonForm.Label1MouseUp ',Event.ControlX,',',Event.ControlY]);
end;

end.

