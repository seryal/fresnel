unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, Unit1, Fresnel.CSSView;

type

  { TForm1 }

  TForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
  private
    procedure OnQueued({%H-}Data: PtrInt);

  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

{ TForm1 }

procedure TForm1.FormCreate(Sender: TObject);
begin
  Application.QueueAsyncCall(@OnQueued,0);
end;

procedure TForm1.OnQueued(Data: PtrInt);
begin
  FresnelCSSView:=TFresnelCSSView.Create(Self);
  FresnelCSSView.Show;
  FresnelCSSView.TargetViewPort:=FresnelForm1;
end;

end.

