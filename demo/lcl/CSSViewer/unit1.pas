unit Unit1;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Classes, Fresnel.Forms, Fresnel.DOM, Fresnel.Controls;

type

  { TFresnelForm1 }

  TFresnelForm1 = class(TFresnelForm)
    Body1: TBody;
    Div1: TDiv;
    Label1: TLabel;
  private

  public

  end;

var
  FresnelForm1: TFresnelForm1;

implementation

{$R *.lfm}

end.

