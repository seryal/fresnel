unit Fresnel.CSSView;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Classes, Fresnel.Forms, Fresnel.DOM, Fresnel.Controls;

type

  { TFresnelCSSView }

  TFresnelCSSView = class(TFresnelForm)
  private
    FTargetViewPort: TFresnelViewport;
    procedure SetTargetViewPort(const AValue: TFresnelViewport);
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure UpdateTree; virtual;
  public
    type

      { TElementNode }

      TElementNode = class(TComponent)
      private
        FElement: TFresnelElement;
        procedure SetElement(const AValue: TFresnelElement);
      protected
        procedure Notification(AComponent: TComponent; Operation: TOperation); override;
      public
        property Element: TFresnelElement read FElement write SetElement;
      end;

  public
    MainDiv: TDiv;
    ElementsTree: TDiv;
    ElementStyles: TDiv;
    TargetRoot: TElementNode;
    constructor Create(AOwner: TComponent); override;
    property TargetViewPort: TFresnelViewport read FTargetViewPort write SetTargetViewPort;
  end;

var
  FresnelCSSView: TFresnelCSSView;

implementation

{$R *.lfm}

{ TFresnelCSSView }

procedure TFresnelCSSView.SetTargetViewPort(const AValue: TFresnelViewport);
begin
  if FTargetViewPort=AValue then Exit;
  if FTargetViewPort<>nil then
    RemoveFreeNotification(FTargetViewPort);
  FTargetViewPort:=AValue;
  if FTargetViewPort<>nil then
    FreeNotification(FTargetViewPort);
  UpdateTree;
end;

procedure TFresnelCSSView.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FTargetViewPort=AComponent then
      FTargetViewPort:=nil;
  end;
end;

procedure TFresnelCSSView.UpdateTree;
begin
  if TargetViewPort=nil then
  begin
    FreeAndNil(TargetRoot);
    exit;
  end;

end;

constructor TFresnelCSSView.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  MainDiv:=TDiv.Create(Self);
  with MainDiv do begin
    Name:='MainDiv';
    Style:='display: flex;';
    Parent:=Self;
  end;

  ElementsTree:=TDiv.Create(Self);
  with ElementsTree do begin
    Name:='ElementsTree';
    Style:='justify-self: 50%; border: 1px solid black; padding: 6px; flex-grow: 1;';
    Parent:=MainDiv;
  end;

  ElementStyles:=TDiv.Create(Self);
  with ElementStyles do begin
    Name:='ElementStyles';
    Style:='justify-self: 50%; border: 1px solid black; padding: 6px; flex-grow: 1;';
    Parent:=MainDiv;
  end;
end;

{ TFresnelCSSView.TElementNode }

procedure TFresnelCSSView.TElementNode.SetElement(const AValue: TFresnelElement);
begin
  if FElement=AValue then Exit;
  if FElement<>nil then
    RemoveFreeNotification(FElement);
  FElement:=AValue;
  if FElement<>nil then
    FreeNotification(FElement);
end;

procedure TFresnelCSSView.TElementNode.Notification(AComponent: TComponent; Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FElement=AComponent then
      FElement:=nil;
  end;
end;

end.

