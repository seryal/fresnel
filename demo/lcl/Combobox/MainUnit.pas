unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, FrForm1;

type

  { TLCLForm1 }

  TLCLForm1 = class(TForm)
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private

  public

  end;

var
  LCLForm1: TLCLForm1;

implementation

{$R *.lfm}

{ TLCLForm1 }

procedure TLCLForm1.FormCreate(Sender: TObject);
begin

end;

procedure TLCLForm1.FormShow(Sender: TObject);
begin
  if FresnelForm1=nil then
    FresnelForm1:=TFresnelForm1.Create(Self);
  FresnelForm1.Show;
end;

end.

