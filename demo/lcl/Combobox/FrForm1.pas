unit FrForm1;

{$mode ObjFPC}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Classes, Fresnel.Forms, Fresnel.DOM, Fresnel.Controls,
  Fresnel.DemoCombobox;

type

  { TFresnelForm1 }

  TFresnelForm1 = class(TFresnelForm)
    DemoCombobox1: TDemoCombobox;
    procedure FresnelForm1Create(Sender: TObject);
  private

  public

  end;

var
  FresnelForm1: TFresnelForm1;

implementation

{$R *.lfm}

{ TFresnelForm1 }

procedure TFresnelForm1.FresnelForm1Create(Sender: TObject);
begin

end;

end.

