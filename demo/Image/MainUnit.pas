unit MainUnit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.Forms, Fresnel.DOM, Fresnel.Controls, FPReadPNG;

type

  { TFresnelImageForm }

  TFresnelImageForm = class(TFresnelForm)
    procedure FresnelImageFormCreate(Sender: TObject);
  private
  public
    Image1: TImage;
    Image2: TImage;
  end;

var
  FresnelImageForm: TFresnelImageForm;

implementation

{$R *.lfm}

{ TFresnelImageForm }

procedure TFresnelImageForm.FresnelImageFormCreate(Sender: TObject);
begin
  Stylesheet.Text:='';

  Image1:=TImage.Create(Self);
  with Image1 do begin
    Name:='Image1';
    Style:='width: 100px; height: 100px; border: 1px solid black;';
    Parent:=Self;
  end;

  Image2:=TImage.Create(Self);
  with Image2 do begin
    Name:='Image2';
    Style:='border: 1px solid black;';
    Parent:=Self;
  end;

  Image1.Image.LoadFromFile('powered_by.png');
  Image2.Image.LoadFromFile('powered_by.png');
end;

end.

