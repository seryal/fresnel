program DemoImage;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}
  cthreads,
  {$ENDIF}
  {$IFDEF HASAMIGA}
  athreads,
  {$ENDIF}
  Fresnel, // this includes the Fresnel widgetset
  Fresnel.Forms, MainUnit
  { you can add units after this };

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TFresnelImageForm, FresnelImageForm);
  Application.Run;
end.

