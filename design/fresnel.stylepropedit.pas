unit Fresnel.StylePropEdit;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ButtonPanel, StdCtrls,
  PropEdits, ObjInspStrConsts, IDEWindowIntf, TextTools, SynEdit,
  SynHighlighterCss, LazUTF8, LazLoggerBase;

type

  { TStylePropEditDialog }

  TStylePropEditDialog = class(TForm)
    BtnPanel: TButtonPanel;
    ClearButton: TButton;
    SaveButton: TButton;
    ApplyButton: TButton;
    SaveDialog1: TSaveDialog;
    StatusLabel: TLabel;
    SortButton: TButton;
    TextGroupBox: TGroupBox;
    CSSSynEdit: TSynEdit;
    SynCssSyn1: TSynCssSyn;
    procedure ApplyButtonClick(Sender: TObject);
    procedure ClearButtonClick(Sender: TObject);
    procedure FormClose(Sender: TObject; var {%H-}CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CSSSynEditChange(Sender: TObject);
    procedure SaveButtonClick(Sender: TObject);
    procedure SortButtonClick(Sender: TObject);
  private
    FEditor: TPropertyEditor;
    procedure SetEditor(const AValue: TPropertyEditor);
  public
    property Editor: TPropertyEditor read FEditor write SetEditor;
    procedure Apply; virtual;
  end;

var
  StylePropEditDialog: TStylePropEditDialog;

implementation

{$R *.lfm}

{ TStylePropEditDialog }

procedure TStylePropEditDialog.ClearButtonClick(Sender: TObject);
begin
  CSSSynEdit.Clear;
end;

procedure TStylePropEditDialog.ApplyButtonClick(Sender: TObject);
begin
  Apply;
end;

procedure TStylePropEditDialog.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  IDEDialogLayoutList.SaveLayout(Self);
end;

procedure TStylePropEditDialog.FormCreate(Sender: TObject);
begin
  StatusLabel.Caption := ois0Lines0Chars;
  SortButton.Caption := oisSort;
  ClearButton.Caption := oisClear;

  CSSSynEdit.Font.Name := SynDefaultFontName;
  CSSSynEdit.Font.Height := SynDefaultFontHeight;
  CSSSynEdit.Font.Pitch := SynDefaultFontPitch;
  CSSSynEdit.Font.Quality := SynDefaultFontQuality;

  IDEDialogLayoutList.ApplyLayout(Self);
end;

procedure TStylePropEditDialog.CSSSynEditChange(Sender: TObject);
var
  NumChars: Integer;
  I: Integer;
begin
  NumChars := 0;
  for I := 0 to CSSSynEdit.Lines.Count - 1 do
    Inc(NumChars, UTF8Length(CSSSynEdit.Lines[I]));

  if CSSSynEdit.Lines.Count = 1 then
    StatusLabel.Caption := Format(ois1LineDChars, [NumChars])
  else
    StatusLabel.Caption := Format(oisDLinesDChars, [CSSSynEdit.Lines.Count, NumChars]);
end;

procedure TStylePropEditDialog.SaveButtonClick(Sender: TObject);
begin
  SaveDialog1.Title:=sccsSGEdtSaveDialog;
  if SaveDialog1.Execute then
    CSSSynEdit.Lines.SaveToFile(SaveDialog1.FileName);
end;

procedure TStylePropEditDialog.SortButtonClick(Sender: TObject);
var
  OldText, NewSortedText: String;
  SortOnlySelection: Boolean;
begin
  if not Assigned(ShowSortSelectionDialogFunc) then
  begin
    SortButton.Enabled := False;
    Exit;
  end;

  SortOnlySelection := True;
  OldText := CSSSynEdit.SelText;
  if OldText = '' then
  begin
    SortOnlySelection := False;
    OldText := CSSSynEdit.Lines.Text;
  end;

  NewSortedText:='';
  if ShowSortSelectionDialogFunc(OldText, nil, NewSortedText) <> mrOk then Exit;
  if SortOnlySelection then
    CSSSynEdit.SelText := NewSortedText
  else
    CSSSynEdit.Lines.Text := NewSortedText;
end;

procedure TStylePropEditDialog.SetEditor(const AValue: TPropertyEditor);
begin
  if FEditor=AValue then Exit;
  FEditor:=AValue;
  if Editor is TStringPropertyEditor then
  else if Editor is TClassPropertyEditor then
  else
    raise Exception.Create('TStylePropEditDialog.SetEditor '+DbgSName(Editor));
end;

procedure TStylePropEditDialog.Apply;
var
  AString: String;
  LineEndPos: SizeInt;
  aList: TStrings;
begin
  AString := CSSSynEdit.Text;
  LineEndPos := Length(AString) - Length(LineEnding) + 1;
  // erase the last lineending if any
  if Copy(AString, LineEndPos, Length(LineEnding)) = LineEnding then
    Delete(AString, LineEndPos, Length(LineEnding));

  if Editor is TStringPropertyEditor then
    Editor.SetStrValue(AString)
  else if Editor is TClassPropertyEditor then
  begin
    aList := TStrings(Editor.GetObjectValue);
    aList.Text:=AString;
    Editor.Modified;
  end;
end;

end.

