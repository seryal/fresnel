unit DemoCompsReg;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Fresnel.DemoCheckBox, Fresnel.DemoSlider, Fresnel.DemoCombobox;

procedure Register;

implementation

{$R DemoCompsReg.res}

procedure Register;
begin
  RegisterComponents('Fresnel',[TDemoCheckBox,TDemoSlider,TDemoCombobox]);
end;

end.

