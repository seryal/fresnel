unit Fresnel.DsgnOptions;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, LazFileCache, LazConfigStorage, BaseIDEIntf;

const
  FresnelDsgnOptsFile = 'fresneldsgnoptions.xml';

type

  { TFresnelDsgnOptions }

  TFresnelDsgnOptions = class(TComponent)
  private
    FChangeStamp: int64;
    FPositionAbsolute: boolean;
    FSavedStamp: int64;
    function GetModified: boolean;
    procedure SetModified(const AValue: boolean);
    procedure SetPositionAbsolute(const AValue: boolean);
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    procedure IncreaseChangeStamp; inline;
    procedure Load;
    procedure Save;
    procedure LoadFromConfig(Cfg: TConfigStorage);
    procedure SaveToConfig(Cfg: TConfigStorage);
  public
    property ChangeStamp: int64 read FChangeStamp;
    property Modified: boolean read GetModified write SetModified;
    property PositionAbsolute: boolean read FPositionAbsolute write SetPositionAbsolute; // true = when putting a new element onto a form use position absolute
  end;

var
  FresnelOptions: TFresnelDsgnOptions;

implementation

{ TFresnelDsgnOptions }

procedure TFresnelDsgnOptions.SetPositionAbsolute(const AValue: boolean);
begin
  if FPositionAbsolute=AValue then Exit;
  FPositionAbsolute:=AValue;
end;

constructor TFresnelDsgnOptions.Create(AOwner: TComponent);
begin
  inherited;
  FChangeStamp:=LUInvalidChangeStamp64;
  FPositionAbsolute:=true;
end;

destructor TFresnelDsgnOptions.Destroy;
begin
  inherited Destroy;
end;

procedure TFresnelDsgnOptions.IncreaseChangeStamp;
begin
  LUIncreaseChangeStamp64(FChangeStamp);
end;

procedure TFresnelDsgnOptions.Load;
var
  Cfg: TConfigStorage;
begin
  Cfg:=GetIDEConfigStorage(FresnelDsgnOptsFile,true);
  try
    LoadFromConfig(Cfg);
  finally
    Cfg.Free;
  end;
end;

procedure TFresnelDsgnOptions.Save;
var
  Cfg: TConfigStorage;
begin
  Cfg:=GetIDEConfigStorage(FresnelDsgnOptsFile,false);
  try
    SaveToConfig(Cfg);
  finally
    Cfg.Free;
  end;
end;

Const
  KeyPositionAbsolute = 'positionabsolute/value';

procedure TFresnelDsgnOptions.LoadFromConfig(Cfg: TConfigStorage);
begin
  PositionAbsolute:=Cfg.GetValue(KeyPositionAbsolute,true);

  Modified:=false;
end;

procedure TFresnelDsgnOptions.SaveToConfig(Cfg: TConfigStorage);
begin
  Cfg.SetDeleteValue(KeyPositionAbsolute,PositionAbsolute,true);

  Modified:=false;
end;

function TFresnelDsgnOptions.GetModified: boolean;
begin
  Result:=FSavedStamp<>FChangeStamp;
end;

procedure TFresnelDsgnOptions.SetModified(const AValue: boolean);
begin
  if AValue then
    IncreaseChangeStamp
  else
    FSavedStamp:=FChangeStamp;
end;

procedure DoneFresnelOptions;
begin
  if FresnelOptions<>nil then
  begin
    try
      if FresnelOptions.Modified then
        FresnelOptions.Save;
    except
    end;
    FreeAndNil(FresnelOptions);
  end;
end;

finalization
  DoneFresnelOptions;

end.

