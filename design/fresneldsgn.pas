{ This file was automatically created by Lazarus. Do not edit!
  This source is only used to compile and install the package.
 }

unit FresnelDsgn;

{$warn 5023 off : no warning about unused units}
interface

uses
  Fresnel.Register, Fresnel.StylePropEdit, Fresnel.DsgnStrConsts, Fresnel.DsgnOptsFrame, 
  Fresnel.DsgnOptions, DemoCompsReg, LazarusPackageIntf;

implementation

procedure Register;
begin
  RegisterUnit('Fresnel.Register', @Fresnel.Register.Register);
  RegisterUnit('DemoCompsReg', @DemoCompsReg.Register);
end;

initialization
  RegisterPackage('FresnelDsgn', @Register);
end.
