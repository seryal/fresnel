{
 Copyright (C) 2024 Mattias Gaertner mattias@freepascal.org

*****************************************************************************
 This file is part of the Fresnel project.

 See the file COPYING.modifiedLGPL.txt, included in this distribution,
 for details about the license.
*****************************************************************************
}
unit Fresnel.Register;

{$mode objfpc}{$H+}

interface

uses
  LCLProc, LCLType, Classes, SysUtils, FormEditingIntf, PropEdits, LazIDEIntf,
  ComponentEditors, IDEOptEditorIntf, LCLIntf, Graphics, Controls, Forms, ProjectIntf,
  PackageIntf, IDEOptionsIntf, LazLoggerBase, CodeToolManager, CodeCache,
  StdCodeTools, Fresnel.DOM, Fresnel.Controls, Fresnel.Forms,
  Fresnel.Renderer, Fresnel.Classes, Fresnel.LCLApp, Fresnel.LCL,
  Fresnel.DsgnStrConsts, Fresnel.StylePropEdit, Fresnel.DsgnOptsFrame, Fresnel.DsgnOptions;

const
  ProjDescNameFresnelApplication = 'Fresnel Application';
  FresnelPkgName = 'Fresnel';
  FresnelLCLPkgName = 'FresnelLCL';
  FresnelBasePkgName = 'FresnelBase';
  FresnelDesignPkgName = 'FresnelDsgn';

type

  { TFresnelFormMediator - mediator for TFresnelForm }

  TFresnelFormMediator = class(TDesignerMediator,IFresnelFormDesigner)
  private
    FDsgnForm: TFresnelForm;
    FRenderer: TFresnelLCLRenderer;
  protected
    procedure Notification(AComponent: TComponent; Operation: TOperation); override;
    procedure SetLCLForm(const AValue: TForm); override;
  public
    // needed by the Lazarus form editor
    class function CreateMediator(TheOwner, aForm: TComponent): TDesignerMediator;
      override;
    class function FormClass: TComponentClass; override;
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    function ComponentAtPos(p: TPoint; MinClass: TComponentClass;
                                Flags: TDMCompAtPosFlags): TComponent; override;
    function ComponentIsIcon(AComponent: TComponent): boolean; override;
    function ComponentIsVisible(AComponent: TComponent): Boolean; override;
    function GetComponentOriginOnForm(AComponent: TComponent): TPoint; override;
    function ParentAcceptsChild(Parent: TComponent;
                                ChildClass: TComponentClass): boolean; override;
    procedure GetBounds(AComponent: TComponent; out CurBounds: TRect); override;
    procedure GetClientArea(AComponent: TComponent; out
                      CurClientArea: TRect; out ScrollOffset: TPoint); override;
    procedure InitComponent(AComponent, NewParent: TComponent; NewBounds: TRect); override;
    procedure Paint; override;
    procedure SetBounds(AComponent: TComponent; NewBounds: TRect); override;
  public
    // needed by Fresnel
    procedure InvalidateRect(Sender: TObject; ARect: TRect; Erase: boolean); virtual;
    procedure SetDesignerFormBounds(Sender: TObject; NewBounds: TRect); virtual;
    function GetDesignerClientHeight: integer; virtual;
    function GetDesignerClientWidth: integer; virtual;
    function GetRenderer: TFresnelRenderer; virtual;
    property DsgnForm: TFresnelForm read FDsgnForm;
    property Renderer: TFresnelLCLRenderer read FRenderer;
  end;

  { TFileDescFresnelForm }

  TFileDescFresnelForm = class(TFileDescPascalUnitWithResource)
  public
    constructor Create; override;
    function Init(var NewFilename: string; NewOwner: TObject;
      var NewSource: string; Quiet: boolean): TModalResult; override;
    function Initialized(NewFile: TLazProjectFile): TModalResult; override;
    function GetInterfaceUsesSection: string; override;
    function GetLocalizedName: string; override;
    function GetLocalizedDescription: string; override;
  end;

  { TProjDescFresnelApplication }

  TProjDescFresnelApplication = class(TProjectDescriptor)
  public
    constructor Create; override;
    function GetLocalizedName: string; override;
    function GetLocalizedDescription: string; override;
    function InitProject(AProject: TLazProject): TModalResult; override;
    function CreateStartFiles({%H-}AProject: TLazProject): TModalResult; override;
  end;

  { TFresnelStylePropertyEditor }

  TFresnelStylePropertyEditor = class(TStringPropertyEditor)
  public
    function GetAttributes: TPropertyAttributes; override;
    procedure Edit; override;
  end;

  { TFresnelStyleSheetPropertyEditor }

  TFresnelStyleSheetPropertyEditor = class(TClassPropertyEditor)
  public
    procedure Edit; override;
    function GetAttributes: TPropertyAttributes; override;
  end;

  { TFresnelComponentRequirements }

  TFresnelComponentRequirements = class(TComponentRequirements)
  public
    procedure RequiredPkgs(Pkgs: TStrings); override;
  end;

var
  FileDescFresnelForm: TFileDescFresnelForm;
  ProjDescFresnelApplication: TProjDescFresnelApplication;

var
  FresnelOptionsFrameID: integer = 1000;

procedure Register;

implementation

{$R fresneldsgnimg.res}

procedure Register;
begin
  FresnelOptions:=TFresnelDsgnOptions.Create(nil);

  // register mediator for designer forms
  FormEditingHook.RegisterDesignerMediator(TFresnelFormMediator);
  FormEditingHook.SetDesignerBaseClassCanAppCreateForm(TFresnelCustomForm,true);

  // register elements
  RegisterComponents('Fresnel',[TDiv,TSpan,TLabel,TButton,TImage,TBody]);
  RegisterComponentRequirements([TDiv,TSpan,TLabel,TButton,TImage,TBody],TFresnelComponentRequirements);

  // register fresnel form as new file type
  FileDescFresnelForm:=TFileDescFresnelForm.Create;
  RegisterProjectFileDescriptor(FileDescFresnelForm,FileDescGroupName);

  // register fresnel application as new project type
  ProjDescFresnelApplication:=TProjDescFresnelApplication.Create;
  RegisterProjectDescriptor(ProjDescFresnelApplication);

  // register property editors
  RegisterPropertyEditor(TypeInfo(String), TFresnelElement, 'Style', TFresnelStylePropertyEditor);
  RegisterPropertyEditor(TypeInfo(String), TFresnelCustomForm, 'Style', THiddenPropertyEditor);
  RegisterPropertyEditor(TypeInfo(TStrings), TFresnelCustomForm, 'Stylesheet', TFresnelStyleSheetPropertyEditor);

  // register IDE options frame
  FresnelOptionsFrameID:=RegisterIDEOptionsEditor(GroupEnvironment,TFresnelOptionsFrame,
                                                  FresnelOptionsFrameID)^.Index;
end;

{ TFresnelFormMediator }

procedure TFresnelFormMediator.Notification(AComponent: TComponent;
  Operation: TOperation);
begin
  inherited Notification(AComponent, Operation);
  if Operation=opRemove then
  begin
    if FDsgnForm=AComponent then
    begin
      FDsgnForm.Designer:=nil;
      FDsgnForm:=nil;
    end;
    if FRenderer=AComponent then
    begin
      FRenderer.Canvas:=nil;
      FRenderer:=nil;
    end;
  end;
end;

procedure TFresnelFormMediator.SetLCLForm(const AValue: TForm);
begin
  if LCLForm=AValue then exit;
  inherited SetLCLForm(AValue);
  if FDsgnForm<>nil then
  begin
    if FRenderer<>nil then
      FRenderer.Canvas:=LCLForm.Canvas;
    TFresnelLCLFontEngine(FDsgnForm.FontEngine).Canvas:=LCLForm.Canvas;
  end else begin
    if FRenderer<>nil then
      FRenderer.Canvas:=nil;
    TFresnelLCLFontEngine(FDsgnForm.FontEngine).Canvas:=nil;
  end;
end;

class function TFresnelFormMediator.CreateMediator(TheOwner, aForm: TComponent
  ): TDesignerMediator;
var
  Mediator: TFresnelFormMediator;
  aFresnelForm: TFresnelForm;
begin
  Result:=inherited CreateMediator(TheOwner,aForm);
  Mediator:=TFresnelFormMediator(Result);
  aFresnelForm:=aForm as TFresnelForm;
  Mediator.FDsgnForm:=aFresnelForm;
  aFresnelForm.Designer:=Mediator;
  Mediator.FreeNotification(aForm);

  aFresnelForm.FontEngine:=TFresnelLCLFontEngine.Create(Mediator);
end;

class function TFresnelFormMediator.FormClass: TComponentClass;
begin
  Result:=TFresnelForm;
end;

procedure TFresnelFormMediator.GetBounds(AComponent: TComponent; out
  CurBounds: TRect);
var
  El: TFresnelElement;
  aBox: TFresnelRect;
begin
  if AComponent=FDsgnForm then
  begin
    CurBounds:=FDsgnForm.FormBounds.GetRect;
  end else if AComponent is TFresnelElement then
  begin
    // return borderbox
    El:=TFresnelElement(AComponent);
    aBox:=El.RenderedBorderBox;
    FresnelRectToRect(aBox,CurBounds);
  end else
    inherited GetBounds(AComponent,CurBounds);
  //debugln(['TFresnelFormMediator.GetBounds ',DbgSName(AComponent),' ',dbgs(CurBounds)]);
end;

procedure TFresnelFormMediator.SetBounds(AComponent: TComponent;
  NewBounds: TRect);
var
  El: TFresnelElement;
  OldStyle: String;
  NewBorderBox: TFresnelRect;
  NewLeft, NewTop, NewWidth, NewHeight: TFresnelLength;
begin
  //debugln(['TFresnelFormMediator.SetBounds ',DbgSName(AComponent),' ',dbgs(NewBounds)]);
  if AComponent=FDsgnForm then
  begin
    FDsgnForm.WSResize(TFresnelRect.Create(NewBounds),NewBounds.Width,NewBounds.Height);
  end else if AComponent is TFresnelElement then
  begin
    // an element (bounds are controlled by CSS)
    El:=TFresnelElement(AComponent);
    if El.ComputedPosition in [CSSRegistry.kwAbsolute,CSSRegistry.kwFixed] then
    begin
      // NewBounds is borderbox
      with El.LayoutNode do begin
        NewBorderBox.SetRect(NewBounds);
        NewLeft:=NewBorderBox.Left-MarginLeft;
        NewTop:=NewBorderBox.Top-MarginTop;
        NewWidth:=NewBorderBox.Width;
        NewHeight:=NewBorderBox.Height;

        // todo: if parent position is static, use the nearest parent with non static
        // todo: right and bottom aligned

        OldStyle:=El.Style;
        case El.ComputedBoxSizing of
        CSSRegistry.kwBorderBox:
          begin

          end;
        CSSRegistry.kwPaddingBox:
          begin
            NewWidth:=NewWidth-BorderLeft-BorderRight;
            NewHeight:=NewHeight-BorderTop-BorderBottom;
          end;
        CSSRegistry.kwContentBox:
          begin
            NewWidth:=NewWidth-BorderLeft-BorderRight-PaddingLeft-PaddingRight;
            NewHeight:=NewHeight-BorderTop-BorderBottom-PaddingTop-PaddingBottom;
          end;
        end;
      end;

      if El.GetStyleAttr('left')<>'' then
        El.SetStyleAttr('left',FloatToCSSPx(NewLeft));
      if El.GetStyleAttr('top')<>'' then
        El.SetStyleAttr('top',FloatToCSSPx(NewTop));
      if El.GetStyleAttr('width')<>'' then
        El.SetStyleAttr('width',FloatToCSSPx(NewWidth));
      if El.GetStyleAttr('height')<>'' then
        El.SetStyleAttr('height',FloatToCSSPx(NewHeight));
      debugln(['TFresnelFormMediator.SetBounds AComponent=',DbgSName(AComponent),' OldStyle=[',OldStyle,'] OldBorderBox=',FloatToCSSStr(El.RenderedBorderBox.Left),',',FloatToCSSStr(El.RenderedBorderBox.Top),' w=',FloatToCSSStr(El.RenderedBorderBox.Width),',h=',FloatToCSSStr(El.RenderedBorderBox.Height),' box-sizing=',CSSRegistry.Keywords[El.ComputedBoxSizing],' NewLeft,Top=',FloatToCSSStr(NewLeft),',',FloatToCSSStr(NewTop),' NewWH=',FloatToCSSStr(NewWidth),'x',FloatToCSSStr(NewHeight)]);
    end;
  end else begin
    inherited SetBounds(AComponent, NewBounds);
  end;
end;

procedure TFresnelFormMediator.GetClientArea(AComponent: TComponent; out
  CurClientArea: TRect; out ScrollOffset: TPoint);
var
  El: TFresnelElement;
  Box, BorderBox: TFresnelRect;
begin
  if AComponent=FDsgnForm then
  begin
    CurClientArea:=Rect(0,0,round(FDsgnForm.Width),round(FDsgnForm.Height));
    ScrollOffset:=Point(0,0);
  end else if AComponent is TFresnelElement then begin
    // return contentbox inside the borderbox
    El:=TFresnelElement(AComponent);
    BorderBox:=El.RenderedBorderBox;
    Box:=El.RenderedContentBox;
    Box.Offset(-BorderBox.Left,-BorderBox.Top);
    FresnelRectToRect(Box,CurClientArea);
  end else
    inherited;
end;

procedure TFresnelFormMediator.InitComponent(AComponent, NewParent: TComponent; NewBounds: TRect);
var
  El: TFresnelElement;
  BorderBox: TFresnelRect;
begin
  if AComponent is TFresnelElement then
  begin
    // set parentcomponent, needed for streaming
    TFresnelFormMediator(AComponent).SetParentComponent(NewParent);
    El:=TFresnelElement(AComponent);
    debugln(['TFresnelFormMediator.InitComponent AComponent=',DbgSName(AComponent),' NewParent=',DbgSName(NewParent),' Bounds=',dbgs(NewBounds)]);
    if FresnelOptions.PositionAbsolute then
    begin
      // todo: if parent position is static, use the nearest parent with non static
      BorderBox.SetRect(NewBounds);
      // todo: compute margins via resolver
      El.SetStyleAttr('position','absolute');
      El.SetStyleAttr('box-sizing','border-box');
      El.SetStyleAttr('left',FloatToCSSPx(BorderBox.Left));
      El.SetStyleAttr('top',FloatToCSSPx(BorderBox.Top));
      if not (El is TReplacedElement) then
      begin
        if (BorderBox.Width<=0) and (El.NodeCount=0) then
          BorderBox.Width:=50;
        if (BorderBox.Height<=0) and (El.NodeCount=0) then
          BorderBox.Height:=50;
        if BorderBox.Width>0 then
          El.SetStyleAttr('width',FloatToCSSPx(BorderBox.Width));
        if BorderBox.Height>0 then
          El.SetStyleAttr('height',FloatToCSSPx(BorderBox.Height));
      end;
    end;
  end else
    inherited;
end;

function TFresnelFormMediator.GetComponentOriginOnForm(AComponent: TComponent
  ): TPoint;
var
  El: TFresnelElement;
  BorderBox: TFresnelRect;
begin
  if AComponent=FDsgnForm then
  begin
    Result:=Point(0,0);
  end else if AComponent is TFresnelElement then
  begin
    El:=TFresnelElement(AComponent);
    if not El.Rendered then
      exit(Point(0,0));
    BorderBox:=El.GetBorderBoxOnViewport;
    Result.X:=round(BorderBox.Left);
    Result.Y:=round(BorderBox.Top);
  end else
    Result:=inherited GetComponentOriginOnForm(AComponent);
end;

procedure TFresnelFormMediator.Paint;
begin
  //debugln(['TFresnelFormMediator.Paint FDsgnForm=',DbgSName(FDsgnForm)]);
  if FDsgnForm=nil then exit;
  //debugln(['TFresnelFormMediator.Paint FDsgnForm=',DbgSName(FDsgnForm),' Rednerer=',DbgSName(FDsgnForm.Renderer)]);
  FDsgnForm.Renderer.Draw(FDsgnForm);
end;

function TFresnelFormMediator.ComponentIsIcon(AComponent: TComponent): boolean;
begin
  if AComponent is TFresnelElement then
    Result:=false
  else
    Result:=inherited ComponentIsIcon(AComponent);
end;

function TFresnelFormMediator.ComponentIsVisible(AComponent: TComponent
  ): Boolean;
begin
  if AComponent=FDsgnForm then
    Result:=true
  else if AComponent is TFresnelElement then
    Result:=TFresnelElement(AComponent).Rendered
  else
    Result:=true;
end;

function TFresnelFormMediator.ComponentAtPos(p: TPoint;
  MinClass: TComponentClass; Flags: TDMCompAtPosFlags): TComponent;
var
  ElArr: TFresnelElementArray;
  El: TFresnelElement;
  i: Integer;
begin
  if Flags=[] then ;
  // skip sub elements aka return only elements owned by the lookup root,
  // which are streamed
  ElArr:=DsgnForm.GetElementsAt(p.X,p.Y);
  for i:=0 to length(ElArr)-1 do
  begin
    El:=ElArr[i];
    if (El=DsgnForm) or (El.Owner=DsgnForm) then
    begin
      if (MinClass=nil) or El.InheritsFrom(MinClass) then
        exit(El);
    end;
  end;
  Result:=nil;
end;

function TFresnelFormMediator.ParentAcceptsChild(Parent: TComponent;
  ChildClass: TComponentClass): boolean;
begin
  //debugln(['TFresnelFormMediator.ParentAcceptsChild START Parent=',DbgSName(Parent),' Child=',DbgSName(ChildClass)]);
  if ChildClass.InheritsFrom(TControl) then
    Result:=false
  else if ChildClass.InheritsFrom(TFresnelViewport) then
    Result:=false
  else if Parent is TFresnelElement then
  begin
    if Parent is TReplacedElement then
      exit(false);
    Result:=ChildClass.InheritsFrom(TFresnelElement);
  end else
    Result:=inherited ParentAcceptsChild(Parent, ChildClass);
  //debugln(['TFresnelFormMediator.ParentAcceptsChild END Parent=',DbgSName(Parent),' Child=',DbgSName(ChildClass),' Result=',Result]);
end;

constructor TFresnelFormMediator.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);
end;

destructor TFresnelFormMediator.Destroy;
begin
  if FDsgnForm<>nil then FDsgnForm.Designer:=nil;
  FDsgnForm:=nil;
  inherited Destroy;
end;

procedure TFresnelFormMediator.InvalidateRect(Sender: TObject; ARect: TRect;
  Erase: boolean);
begin
  //debugln(['TFresnelFormMediator.InvalidateRect ',DbgSName(FDsgnForm),' ',dbgs(ARect)]);
  if (LCLForm=nil) or (not LCLForm.HandleAllocated) then exit;
  LCLIntf.InvalidateRect(LCLForm.Handle,@ARect,Erase);
end;

procedure TFresnelFormMediator.SetDesignerFormBounds(Sender: TObject;
  NewBounds: TRect);
begin
  if LCLForm=nil then exit;
  LCLForm.BoundsRect:=NewBounds;
end;

function TFresnelFormMediator.GetDesignerClientHeight: integer;
begin
  if LCLForm=nil then
    Result:=round(FDsgnForm.Height)
  else
    Result:=LCLForm.ClientHeight;
end;

function TFresnelFormMediator.GetDesignerClientWidth: integer;
begin
  if LCLForm=nil then
    Result:=round(FDsgnForm.Width)
  else
    Result:=LCLForm.ClientWidth;
end;

function TFresnelFormMediator.GetRenderer: TFresnelRenderer;
begin
  if FRenderer=nil then
  begin
    FRenderer:=TFresnelLCLRenderer.Create(Self);
    Renderer.Canvas:=LCLForm.Canvas;
  end;
  Result:=FRenderer;
end;

{ TFileDescFresnelForm }

constructor TFileDescFresnelForm.Create;
begin
  inherited Create;
  Name:='FresnelForm';
  ResourceClass:=TFresnelForm;
  UseCreateFormStatements:=true;
end;

function TFileDescFresnelForm.Init(var NewFilename: string; NewOwner: TObject;
  var NewSource: string; Quiet: boolean): TModalResult;
var
  DependencyOwner, aOwner: TObject;
begin
  Result:=inherited Init(NewFilename, NewOwner, NewSource, Quiet);

  // if project uses the LCL, add dependency FresnelLCL else Fresnel
  RequiredPackages:=FresnelPkgName+';'+FresnelDesignPkgName;
  aOwner:=NewOwner;
  if aOwner=nil then
    aOwner:=LazarusIDE.ActiveProject;

  if aOwner<>nil then
  begin
    if PackageEditingInterface.IsOwnerDependingOnPkg(aOwner,'LCL',DependencyOwner) then
      RequiredPackages:=FresnelLCLPkgName+';'+FresnelDesignPkgName;
  end;
end;

function TFileDescFresnelForm.Initialized(NewFile: TLazProjectFile
  ): TModalResult;
var
  aProject: TLazProject;
  MainFilename: String;
  Code: TCodeBuffer;
  DependencyOwner: TObject;
  NamePos, InPos: integer;
begin
  Result:=inherited Initialized(NewFile);
  aProject:=LazarusIDE.ActiveProject;
  if aProject=nil then begin
    debugln(['Warning: TFileDescFresnelForm.Initialized: not adding uses Fresnel, because ActiveProject=nil']);
    exit;
  end;
  if aProject.MainFile=nil then begin
    debugln(['Warning: TFileDescFresnelForm.Initialized: not adding uses Fresnel, because ActiveProject.MainFile=nil']);
    exit;
  end;
  if PackageEditingInterface.IsOwnerDependingOnPkg(aProject,'LCL',DependencyOwner) then
  begin
    // a lcl app -> add Fresnel behind 'interfaces'
    MainFilename:=aProject.MainFile.Filename;
    Code:=CodeToolBoss.LoadFile(MainFilename,true,false);
    if not CodeToolBoss.AddUnitToMainUsesSectionIfNeeded(Code,'Fresnel','',[aufLast]) then
    begin
      if CodeToolBoss.FindUnitInAllUsesSections(Code,'Fresnel',NamePos,InPos) then
      begin
        debugln(['Warning: TFileDescFresnelForm.Initialized: failed adding uses Fresnel to "',MainFilename,'"']);
        exit;
      end;
    end;
  end;
end;

function TFileDescFresnelForm.GetInterfaceUsesSection: string;
begin
  Result:='Classes, SysUtils, Fresnel.Classes, Fresnel.Forms, Fresnel.DOM, Fresnel.Controls';
end;

function TFileDescFresnelForm.GetLocalizedName: string;
begin
  Result:='Fresnel Form';
end;

function TFileDescFresnelForm.GetLocalizedDescription: string;
begin
  Result:='Create a new Fresnel form';
end;

{ TProjDescFresnelApplication }

constructor TProjDescFresnelApplication.Create;
begin
  inherited Create;
  Name:=ProjDescNameFresnelApplication;
  Flags:=Flags+[pfUseDefaultCompilerOptions];
end;

function TProjDescFresnelApplication.GetLocalizedName: string;
begin
  Result:=frsFresnelApplication;
end;

function TProjDescFresnelApplication.GetLocalizedDescription: string;
begin
  Result:=frsFresnelApplicationDesc;
end;

function TProjDescFresnelApplication.InitProject(AProject: TLazProject
  ): TModalResult;
var
  NewSource: String;
  MainFile: TLazProjectFile;
begin
  Result:=inherited InitProject(AProject);

  MainFile:=AProject.CreateProjectFile('project1.lpr');
  MainFile.IsPartOfProject:=true;
  AProject.AddFile(MainFile,false);
  AProject.MainFileID:=0;
  AProject.UseAppBundle:=true;
  AProject.UseManifest:=true;
  AProject.Scaled:=true;
  // ToDo: AProject.ProjResources.XPManifest.DpiAware := xmdaTrue;
  AProject.LoadDefaultIcon;

  // create program source
  NewSource:='program Project1;'+LineEnding
    +LineEnding
    +'{$mode objfpc}{$H+}'+LineEnding
    +LineEnding
    +'uses'+LineEnding
    +'  {$IFDEF UNIX}'+LineEnding
    +'  cthreads,'+LineEnding
    +'  {$ENDIF}'+LineEnding
    +'  {$IFDEF HASAMIGA}'+LineEnding
    +'  athreads,'+LineEnding
    +'  {$ENDIF}'+LineEnding
    +'  Fresnel, // this includes the Fresnel widgetset'+LineEnding
    +'  Fresnel.Forms'+LineEnding
    +'  { you can add units after this };'+LineEnding
    +LineEnding
    +'begin'+LineEnding
    +'  Application.Initialize;'+LineEnding
    +'  Application.Run;'+LineEnding
    +'end.'+LineEnding
    +LineEnding;
  AProject.MainFile.SetSourceText(NewSource,true);

  // add Fresnel package dependency
  AProject.AddPackageDependency('Fresnel');
  AProject.LazCompilerOptions.Win32GraphicApp:=true;
  AProject.LazCompilerOptions.UnitOutputDirectory:='lib'+PathDelim+'$(TargetCPU)-$(TargetOS)';
  AProject.LazCompilerOptions.TargetFilename:='project1';
end;

function TProjDescFresnelApplication.CreateStartFiles(AProject: TLazProject
  ): TModalResult;
begin
  Result:=LazarusIDE.DoNewEditorFile(FileDescFresnelForm,'','',
                         [nfIsPartOfProject,nfOpenInEditor,nfCreateDefaultSrc]);
end;

{ TFresnelStylePropertyEditor }

function TFresnelStylePropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paDialog, paRevertable];
end;

procedure TFresnelStylePropertyEditor.Edit;
var
  TheDialog : TStylePropEditDialog;
  AString: String;
begin
  AString := GetStrValue; // read first to get nicer error messages

  TheDialog := TStylePropEditDialog.Create(nil);
  try
    TheDialog.Caption := 'CSS Inline Style Editor';
    TheDialog.Editor := Self;
    TheDialog.CSSSynEdit.Text := AString;
    TheDialog.CSSSynEditChange(nil);
    if (TheDialog.ShowModal = mrOK) then
    begin
      TheDialog.Apply;
    end;
  finally
    TheDialog.Free;
  end;
end;

{ TFresnelStyleSheetPropertyEditor }

procedure TFresnelStyleSheetPropertyEditor.Edit;
var
  TheDialog : TStylePropEditDialog;
  aList: TStrings;
begin
  aList := TStrings(GetObjectValue);

  TheDialog := TStylePropEditDialog.Create(nil);
  try
    TheDialog.Caption := 'CSS Stylesheet Editor';
    TheDialog.Editor := Self;
    TheDialog.CSSSynEdit.Text := aList.Text;
    TheDialog.CSSSynEditChange(nil);
    if (TheDialog.ShowModal = mrOK) then
    begin
      TheDialog.Apply;
    end;
  finally
    TheDialog.Free;
  end;
end;

function TFresnelStyleSheetPropertyEditor.GetAttributes: TPropertyAttributes;
begin
  Result := [paMultiSelect, paDialog, paRevertable, paReadOnly];
end;

{ TFresnelComponentRequirements }

procedure TFresnelComponentRequirements.RequiredPkgs(Pkgs: TStrings);

  procedure RemoveFresnelBase;
  var
    i: Integer;
  begin
    i:=Pkgs.IndexOf('FresnelBase');
    if i<0 then exit;
    Pkgs.Delete(i);
  end;

var
  aProject: TLazProject;
  DependencyOwner: TObject;
begin
  // the Fresnel components are part of package FresnelBase, but the project
  // actually needs a Fresnel backend.

  // if project is using the LCL then use FresnelLCL else Fresnel
  aProject:=LazarusIDE.ActiveProject;
  if aProject<>nil then
  begin
    RemoveFresnelBase;
    if PackageEditingInterface.IsOwnerDependingOnPkg(aProject,'LCL',DependencyOwner) then
    begin
      Pkgs.Add('FresnelLCL');
    end else begin
      Pkgs.Add('Fresnel');
    end;
  end;
end;

end.

