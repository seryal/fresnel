unit Fresnel.DsgnStrConsts;

{$mode objfpc}{$H+}

interface

resourcestring
  frsFresnelApplication = 'Fresnel Application';
  frsFresnelApplicationDesc = 'A graphical Free Pascal application using'
    +' the cross-platform Fresnel library for its GUI.';
  frsWhenPuttingANewElementOntoAFormPositionItAbsoluteU = 'When putting a new element onto a form, '
    +'position it absolute using the mouse coordinates';

implementation

end.

