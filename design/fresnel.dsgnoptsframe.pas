{ IDE options frame for Fresnel options

  Author: Mattias Gaertner
}
unit Fresnel.DsgnOptsFrame;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils,
  // LCL
  Forms, StdCtrls, Dialogs,
  // IdeIntf
  IDEOptionsIntf, IDEOptEditorIntf,
  // Fresnel
  Fresnel.DsgnStrConsts, Fresnel.DsgnOptions;

type

  { TFresnelOptionsFrame }

  TFresnelOptionsFrame = class(TAbstractIDEOptionsEditor)
    PositionAbsoluteCheckBox: TCheckBox;
  private
  public
    function GetTitle: String; override;
    procedure Setup({%H-}ADialog: TAbstractOptionsEditorDialog); override;
    procedure ReadSettings({%H-}AOptions: TAbstractIDEOptions); override;
    procedure WriteSettings({%H-}AOptions: TAbstractIDEOptions); override;
    class function SupportedOptionsClass: TAbstractIDEOptionsClass; override;
  end;

implementation

{$R *.lfm}

{ TFresnelOptionsFrame }

function TFresnelOptionsFrame.GetTitle: String;
begin
  Result:='Fresnel';
end;

procedure TFresnelOptionsFrame.Setup(ADialog: TAbstractOptionsEditorDialog);
begin
  PositionAbsoluteCheckBox.Caption:='Position absolute'; // CSS keywords dont need to be translated
  PositionAbsoluteCheckBox.Hint:=frsWhenPuttingANewElementOntoAFormPositionItAbsoluteU;
end;

procedure TFresnelOptionsFrame.ReadSettings(AOptions: TAbstractIDEOptions);
begin
  PositionAbsoluteCheckBox.Checked:=FresnelOptions.PositionAbsolute;
end;

procedure TFresnelOptionsFrame.WriteSettings(AOptions: TAbstractIDEOptions);
begin
  FresnelOptions.PositionAbsolute:=PositionAbsoluteCheckBox.Checked;
end;

class function TFresnelOptionsFrame.SupportedOptionsClass: TAbstractIDEOptionsClass;
begin
  Result:=IDEEditorGroups.GetByIndex(GroupEnvironment)^.GroupClass;
end;

end.

